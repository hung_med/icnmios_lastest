//
//  ProfileDoctor+Network.swift
//  ProfileDoctor
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension ProfileDoctor {
    @discardableResult
    func getUserDoctorByID(userID:Int, userViewID:Int, success:@escaping(ProfileDoctor?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {

        return API.requestJSON(endpoint: .getUserDoctorByID(userID, userViewID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let profileDoctor = ProfileDoctor(JSON: item) {
                        success(profileDoctor)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
