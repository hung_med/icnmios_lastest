//
//  UserTag+Network.swift
//  iCNM
//
//  Created by Quang Hung on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension UserTag {
    
    @discardableResult
    static func getUserTagByUserIDFix(userID:Int, success:@escaping(UserTag?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getUserTagByUserIDFix(userID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let userTag = UserTag(JSON: item) {
                        success(userTag)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func updateUserTags(userID:Int, userTag:String, success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        var parameterDic = [String: Any]()
        parameterDic = ["UserID":userID,
                        "Tag":userTag]
        
        return API.requestJSON(endpoint: .updateUserTags(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
