//
//  Education.swift
//  iCNM
//
//  Created by Medlatec on 5/16/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import ObjectMapper
import RealmSwift

class Education:Object,Mappable {
    dynamic var id = 0
    dynamic var schoolName:String?
    dynamic var major:String?
    dynamic var startDate:Date?
    dynamic var endDate:Date?
    dynamic var summary:String?
    dynamic var doctorID = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
//        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        id <- map["EducatedID"]
        schoolName <- map["SchoolName"]
        major <- map["Major"]
        startDate <- (map["StartDate"],DateFormatterTransform(dateFormatter:dateFormatter))
        endDate <- (map["EndDate"],DateFormatterTransform(dateFormatter:dateFormatter))
        summary <- map["Summary"]
        doctorID <- map["DoctorID"]
        
    }
    
}

