//
//  UserRegister+Network.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire


extension MedicalDetailsByGroups {

    @discardableResult
    func getMedicalDetailsByGroups(diseaseGroupID:Int, success:@escaping([MedicalDetailsByGroups]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        //let parameterDic:String = "?symptonID=\(symptonID)"
        return API.requestJSON(endpoint: .getMedicalDetailsByGroups(diseaseGroupID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [MedicalDetailsByGroups]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let medicalDictDetailbyGroup = MedicalDetailsByGroups(JSON: item) {
                            data.append(medicalDictDetailbyGroup)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }

    
//    @discardableResult
//    func getMedicalDetailsByGroups(diseaseGroupID:Int, success:@escaping(MedicalDetailsByGroups?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
//        return API.requestJSON(endpoint: .getMedicalDetailsByGroups(diseaseGroupID), completionHandler: { (response) in
//            switch response.result {
//            case .success(let value):
//                if let item = value as? [String:Any] {
//                    if let medicalDictDetailbyGroup = MedicalDetailsByGroups(JSON: item) {
//                        success(medicalDictDetailbyGroup)
//                    }
//                } else {
//                    success(nil)
//                }
//            case .failure(let error):
//                fail(error,response)
//            }
//        })
//    }
}
