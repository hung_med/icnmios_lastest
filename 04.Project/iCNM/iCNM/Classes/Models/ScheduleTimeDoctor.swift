//
//  ScheduleTimeDoctor.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class ScheduleTimeDoctor:Mappable {
    var doctorTime:String? = nil
    var scheduleTimeID:Int = 0
    var isFull:Bool? = false
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            doctorTime <- map["DoctorTime"]
            scheduleTimeID <- map["ScheduleTimeID"]
            isFull <- map["IsFull"]
        }
    }
}
