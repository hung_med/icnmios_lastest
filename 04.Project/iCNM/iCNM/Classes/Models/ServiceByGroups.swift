//
//  ServiceByGroups.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class ServiceByGroups:Mappable {
    var service:Service?
    var provinceName:String = ""
    var services = ""
    var location = ""
    var serviceGroupsTest = ""
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            service <- map["Service"]
            provinceName <- map["ProvinceName"]
            services <- map["Services"]
            location <- map["Location"]
            serviceGroupsTest <- map["ServiceGroupsTest"]
        }
    }
}
