//
//  TrackingNewsID.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class TrackingNewsID:Mappable {
    var userID:Int? = 0
    var newsID:Int? = 0
    var dateCreate:Date? = nil
    var typeDevice:String? = nil
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .toJSON {
            userID <- map["UserID"]
            newsID <- map["NewsID"]
            dateCreate <- map["DateCreate"]
            typeDevice <- map["TypeDevice"]
        }
    }
}
