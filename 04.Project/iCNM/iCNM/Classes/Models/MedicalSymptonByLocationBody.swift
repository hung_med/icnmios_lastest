//
//  UserRegister.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class MedicalSymptonByLocationBody:Mappable {
    
    var active:Bool = false
    var dateCreate = ""
    var description = ""
    var descriptionPictures = ""
    var diagnosis = ""
    var locationBodyIDStr = ""
    var mean = ""
    var medicalSymptonID:Int = 0
    var name = ""
    var pictures = ""
    var reason = ""
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
      if map.mappingType == .fromJSON {
            active <- map["Active"]
            dateCreate <- map["DateCreate"]
            description <- map["Description"]
            descriptionPictures <- map["DescriptionPictures"]
            diagnosis <- map["Diagnosis"]
            locationBodyIDStr <- map["LocationBodyIDStr"]
            mean <- map["Mean"]
            medicalSymptonID <- map["MedicalSymptonID"]
            name <- map["Name"]
            pictures <- map["Pictures"]
            reason <- map["Reason"]
        }
    }
}
