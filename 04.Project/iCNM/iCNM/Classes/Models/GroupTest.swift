//
//  GroupTest.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class GroupTest:Mappable {
    var groupTestID:Int = 0
    var name = ""
    var active:Bool = false
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            groupTestID <- map["GroupTestID"]
            name <- map["Name"]
            active <- map["Active"]
        }
    }
}
