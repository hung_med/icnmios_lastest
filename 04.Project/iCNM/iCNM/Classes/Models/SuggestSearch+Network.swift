//
//  SuggestSearch+Network.swift
//  iCNM
//
//  Created by Hoang Van Trung on 8/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import Alamofire

extension SuggestSearch {
    @discardableResult
    func getSuggestSearch(param: [String: Any], success:@escaping([SuggestSearch]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getSuggestSearch(param), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [SuggestSearch]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let lookUpResult = SuggestSearch(JSON: item) {
                            data.append(lookUpResult)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }

}
