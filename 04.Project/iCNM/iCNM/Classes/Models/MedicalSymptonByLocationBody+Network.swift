//
//  UserRegister+Network.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension MedicalSymptonByLocationBody {
    
    @discardableResult
    func getMedicalSymptonByLocationBody(locationBodyID:Int, success:@escaping([MedicalSymptonByLocationBody]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        //let parameterDic:String = "?symptonID=\(symptonID)"
        return API.requestJSON(endpoint: .getMedicalSymptonByLocationBody(locationBodyID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [MedicalSymptonByLocationBody]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let medicalSymptonObj = MedicalSymptonByLocationBody(JSON: item) {
                            data.append(medicalSymptonObj)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
