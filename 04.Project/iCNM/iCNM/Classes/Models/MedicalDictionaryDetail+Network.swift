//
//  UserRegister+Network.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension MedicalDictionaryDetail {
    
    @discardableResult
    func getMedicalDictionaryDetail(medicalDicID:Int, success:@escaping(MedicalDictionaryDetail?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getMedicalDictionaryDetail(medicalDicID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let medicalDictDetail = MedicalDictionaryDetail(JSON: item) {
                        success(medicalDictDetail)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
//    @discardableResult
//    func getMedicalDictionaryDetail(groupID:Int, success:@escaping([MedicalDictionaryDetail]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
//        let parameterDic:String = "\(groupID)"
//        return API.requestJSON(endpoint: .getMedicalDictionaryDetail(parameterDic), completionHandler: { (response) in
//            switch response.result {
//            case .success(let value):
//                var data = [MedicalDictionaryDetail]()
//                if let array = value as? [[String:Any]]  {
//                    for item in array {
//                        if let medicalDictDetail = MedicalDictionaryDetail(JSON: item) {
//                            data.append(medicalDictDetail)
//                        }
//                    }
//                }
//                success(data)
//            case .failure(let error):
//                fail(error,response)
//            }
//        })
//    }
}
