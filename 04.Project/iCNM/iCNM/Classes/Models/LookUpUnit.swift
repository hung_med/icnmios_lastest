//
//  LookUpUnit.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class LookUpUnit:Mappable {
    var traSau = ""
    var email = ""
    var soTK = ""
    var bank = ""
    var soHD = ""
    var ns = ""
    var nguoiTao = ""
    var dc1 = ""
    var maBS = ""
    var nguoiDaiDien = ""
    var ngayCapCMND = ""
    var noiCapCMND = ""
    var cmnd = ""
    var chiNhanh = ""
    var maCD = ""
    var maChucDanh = ""
    var stt = ""
    var maTinh = ""
    var maTinh2 = ""
    var maHuyen = ""
    var maHuyen2 = ""
    var dc2 = ""
    var tenChuTK = ""
    var chuyenKhoa = ""
    var tenBS = ""
    var mobi = ""
    var hTCKDoiTuong = ""
    var ngayEdit = ""
    var maDTCTV = ""
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            traSau <- map["TraSau"]
            email <- map["Email"]
            soTK <- map["SoTK"]
            bank <- map["Bank"]
            soHD <- map["SoHD"]
            ns <- map["NS"]
            nguoiTao <- map["NguoiTao"]
            dc1 <- map["DC1"]
            maBS <- map["MaBS"]
            nguoiDaiDien <- map["NguoiDaiDien"]
            ngayCapCMND <- map["NgayCapCMND"]
            noiCapCMND <- map["NoiCapCMND"]
            cmnd <- map["CMND"]
            chiNhanh <- map["ChiNhanh"]
            maCD <- map["MaCD"]
            maChucDanh <- map["MaChucDanh"]
            stt <- map["Stt"]
            maTinh <- map["MaTinh"]
            maTinh2 <- map["MaTinh2"]
            maHuyen <- map["MaHuyen"]
            maHuyen2 <- map["MaHuyen2"]
            dc2 <- map["DC2"]
            tenChuTK <- map["TenChuTK"]
            chuyenKhoa <- map["ChuyenKhoa"]
            tenBS <- map["TenBS"]
            mobi <- map["Mobi"]
            hTCKDoiTuong <- map["HTCKDoiTuong"]
            ngayEdit <- map["NgayEdit"]
            maDTCTV <- map["MaDTCTV"]
        }
    }
    
}
