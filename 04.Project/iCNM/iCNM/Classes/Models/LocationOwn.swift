//
//  LocationOwn.swift
//  iCNM
//
//  Created by Hoang Van Trung on 11/24/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import ObjectMapper

class LocationOwn: Mappable {
    var LocationID: Int = 0
    var Name: String = ""
    var Address: String = ""
    var CName: String = ""
    var Lat = 0.0
    var Long = 0.0
    var Mobile: String = ""
    var Service: String = ""
    var LocationTimeID: String =  ""
    var WorkingTimeStr: String = ""
    var ImagesStr: String = ""
    var ImagesThumbnailStr: String = ""
    var WorkingTime: String = ""
    var SpecialistStr: String = ""
    var Ctype: Bool = false
    var DayStr: String = ""
    var LocationTypeID: Int = 1
    var District: Int = 0
    var City: String = ""
    var Specialist: String = ""
    var AverageRating: Double = 0.0
    var Intro: String = ""
    var TinhThanhID: String = ""
    var QuanHuyenID: String = ""
    var Geog: String =  ""
    var KeySearchName: String = ""
    var UserUpdate: String = ""
    var DateUpdate: String = ""
    var Active = false
    var CountLike: Int = 0
    var VerifyPhone: Bool = false
    var FromVicare: String = ""
    var NeedReview: String = ""
    var DoctorID: Int = 0
    var DoctorCode: String = ""
    var Email = false
    var Duyet: Int = 0
    var KeySearchAddress: Bool = false
    var KeySearchAll: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        LocationID <- map["LocationID"]
        Name <- map["Name"]
        Address <- map["Address"]
        CName <- map["CName"]
        Lat <- map["Lat"]
        Long <- map["Long"]
        Mobile <- map["Mobile"]
        Service <- map["Service"]
        LocationTimeID <- map["LocationTimeID"]
        WorkingTimeStr <- map["WorkingTimeStr"]
        ImagesStr <- map["ImagesStr"]
        ImagesThumbnailStr <- map["ImagesThumbnailStr"]
        WorkingTime <- map["WorkingTime"]
        SpecialistStr <- map["SpecialistStr"]
        Ctype <- map["Ctype"]
        DayStr <- map["DayStr"]
        LocationTypeID <- map["LocationTypeID"]
        District <- map["District"]
        City <- map["City"]
        Specialist <- map["Specialist"]
        AverageRating <- map["AverageRating"]
        Intro <- map["Intro"]
        TinhThanhID <- map["TinhThanhID"]
        QuanHuyenID <- map["QuanHuyenID"]
        Geog <- map["Geog"]
        KeySearchName <- map["KeySearchName"]
        UserUpdate <- map["UserUpdate"]
        DateUpdate <- map["DateUpdate"]
        Active <- map["Active"]
        CountLike <- map["CountLike"]
        VerifyPhone <- map["VerifyPhone"]
        FromVicare <- map["FromVicare"]
        NeedReview <- map["NeedReview"]
        DoctorID <- map["DoctorID"]
        DoctorCode <- map["DoctorCode"]
        Email <- map["Email"]
        Duyet <- map["Duyet"]
        KeySearchAddress <- map["KeySearchAddress"]
        KeySearchAll <- map["KeySearchAll"]
    }
}
