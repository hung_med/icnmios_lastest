//
//  HealthRecords.swift
//  iCNM
//
//  Created by Ta Quang Hung on 20/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper

class HealthRecords:Mappable {
    var medicalProfile:MedicalProfile?
    var userMedicalProfile:UserMedicalProfile?
    
    // PRAGMA - ObjectMapper
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            medicalProfile <- map["MedicalProfile"]
            userMedicalProfile <- map["User_MedicalProfile"]
        } else {
            medicalProfile <- map["MedicalProfile"]
            userMedicalProfile <- map["User_MedicalProfile"]
        }
    }
    
}
