//
//  Service.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class Service:Mappable {
    var serviceID:Int = 0
    var locationID:Int = 0
    var provinceID:Int = 0
    var serviceName = ""
    var serviceDes = ""
    var cost:Int = 0
    var serviceContent = ""
    var img:String = ""
    var discount:Double = 0
    var desImg = ""
    var keysearchName = ""
    var active:Bool = false
    var costFe:Int = 0
    var discountFe:Double = 0.0
    var idGoi:Int = 0
    var maGoi:String = ""
    var lowAgeLimit:Int = 0
    var hightAgeLimit:Int = 0
    var sex:Int = 0
    var averageRating:Float = 0.0
    var countRate:Int = 0
    var conditionDate = ""
    var personNumService:Int = 0
    var isServey:Int = 0
    var serviceType:Int = 0
    var timeBefore:Int = 0
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            serviceID <- map["ServiceID"]
            locationID <- map["LocationID"]
            provinceID <- map["ProvinceID"]
            serviceName <- map["ServiceName"]
            serviceDes <- map["ServiceDes"]
            cost <- map["Cost"]
            serviceContent <- map["ServiceContent"]
            img <- map["Img"]
            discount <- map["Discount"]
            desImg <- map["DesImg"]
            keysearchName <- map["KeysearchName"]
            active <- map["Active"]
            costFe <- map["CostFe"]
            discountFe <- map["DiscountFe"]
            idGoi <- map["IdGoi"]
            maGoi <- map["MaGoi"]
            lowAgeLimit <- map["LowAgeLimit"]
            hightAgeLimit <- map["HightAgeLimit"]
            sex <- map["Sex"]
            averageRating <- map["AverageRating"]
            countRate <- map["CountRate"]
            conditionDate <- map["ConditionDate"]
            personNumService <- map["PersonNumService"]
            isServey <- map["IsServey"]
            serviceType <- map["ServiceType"]
            timeBefore <- map["TimeBefore"]
        }
    }
}
