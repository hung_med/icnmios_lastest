//
//  UserMedicalProfile.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class UserMedicalProfile:Mappable {
    var active:Bool = false
    var dateCreate:Date? = nil
    var isCreate:Bool = false
    var isShare:Bool = false
    var medicalFor = ""
    var medicalProfileID:Int = 0
    var userID:Int = 0
    var userMedicalProfileID:Int = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
      if map.mappingType == .fromJSON {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            active <- map["Active"]
            dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
            isCreate <- map["IsCreate"]
            isShare <- map["IsShare"]
            medicalFor <- map["MedicalFor"]
            medicalProfileID <- map["MedicalProfileID"]
            userID <- map["UserID"]
            userMedicalProfileID <- map["UserMedicalProfileID"]
        }
    }
}
