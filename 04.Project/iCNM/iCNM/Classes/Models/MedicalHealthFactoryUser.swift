//
//  MedicalHealthFactoryUser.swift
//  iCNM
//
//  Created by Ta Quang Hung on 20/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper

class MedicalHealthFactoryUser:Mappable {
    
    static let sharedInstance = MedicalHealthFactoryUser()
    // Anything else goes here
    var medicalHealFactoriesObj = MedicalHealthFactory()
    var medicalCilinnicalObj = MedicalCilinnical()
    
    var medicalHealFactory:MedicalHealthFactory?
    var medicalProfile:MedicalProfile?
    var userID:Int = 0
    var medicalFor = ""
    var medicalVaccinsTreEm = [MedicalVaccin]()
    var medicalVaccinsTCMR = [MedicalVaccin]()
    var medicalVaccinsUonVan = [MedicalVaccin]()
    var medicalCilinicals = [MedicalCilinnical]()
    
    // PRAGMA - ObjectMapper
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .toJSON{
            medicalHealFactory <- map["MedicalHealFactory"]
            medicalProfile <- map["MedicalProfile"]
            userID <- map["UserID"]
            medicalFor <- map["MedicalFor"]
            medicalVaccinsTreEm <- map["MedicalVaccin"]
            medicalVaccinsTCMR <- map["MedicalVaccin"]
            medicalVaccinsUonVan <- map["MedicalVaccin"]
            medicalCilinicals <- map["MedicalCilinnical"]
        }
    }
}
