//
//  AddNewResult.swift
//  iCNM
//
//  Created by Thanh Huyen on 4/14/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper

class ResultCustom: Mappable {
    var resultPatient:ResultPatient? = nil
    var imageResult:[ImageResult]? = nil
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .toJSON {
            resultPatient <- map["ResultPatient"]
            imageResult <- map["Images"]
        }
    }
}
