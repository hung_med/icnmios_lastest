//
//  AddNewResult.swift
//  iCNM
//
//  Created by Thanh Huyen on 4/14/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper

class PatientResultCustom: Mappable {
    var patient:Patient? = nil
    var resultCustom:[ResultCustom]? = nil
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .toJSON {
            patient <- map["Patient"]
            resultCustom <- map["ResultCustom"]
        }
    }
}
