//
//  MedicalVaccin.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class MedicalVaccin:Mappable {

    var active:Bool = false
    var monthPreg:Int = 0
    var vaccinDate = ""
    var vaccinID:Int = 0
    var vaccinName = ""
    var vaccinReact = ""
    var vaccinSchedule = ""
    var vaccinTypeID:Int = 0
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
      if map.mappingType == .fromJSON {
            active <- map["Active"]
            monthPreg <- map["MonthPreg"]
            vaccinDate <- map["VaccinDate"]
            vaccinID <- map["VaccinID"]
            vaccinName <- map["VaccinName"]
            vaccinReact <- map["VaccinReact"]
            vaccinSchedule <- map["VaccinSchedule"]
            vaccinTypeID <- map["VaccinTypeID"]
        }
    }
}
