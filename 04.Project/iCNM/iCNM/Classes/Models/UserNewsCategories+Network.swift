//
//  UserNewsCategories+Network.swift
//  iCNM
//
//  Created by Len Pham on 9/2/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

extension UserNewsCategories {
    
    @discardableResult
    static func getAllNewsCategory(userId:Int, success:@escaping(Bool) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        var stringParam: String = String()
        stringParam = stringParam.appendingFormat("%d",userId)
        return API.requestJSON(endpoint: .allNewsCategoriesOfUser(stringParam), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var userCategories = [NewsCategoryModel]()
                var categories = [NewsCategoryModel]()

                if let stringResult = value as? [String:Any] {
                    
                    if let userNewsCategories =  UserNewsCategories(JSON: stringResult) {
                       
                        if let array = userNewsCategories.categories {
                            let realm = try! Realm()
                            
                            for item in array {
                                
                                try! realm.write {
                                    item.userID = userId
                                    
                                    if userNewsCategories.userCategories != nil {
                                        item.selected = false
                                    }
                                    else {
                                         item.selected = true
                                    }
                                    
                                    categories.append(realm.create(NewsCategoryModel.self, value: item, update: true))
                                }
                            }
                        }
                        
                        if let array = userNewsCategories.userCategories {
                            let realm = try! Realm()
                            for item in array {
                                try! realm.write {
                                    item.userID = userId
                                    item.selected = true
                                    userCategories.append(realm.create(NewsCategoryModel.self, value: item, update: true))
                                }
                            }
                        }
                    }
                }
        
                success(true)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    static func postUserCategories(userId:Int, categoryIds:String , success:@escaping(String) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        let userIDStr = String(format: "%d",userId)
        let paramDic:[String:Any] = [
            "UserID": userIDStr,
            "CategoryIDStr":categoryIds
        ]
        return API.requestJSON(endpoint: .postUserCategory(paramDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let data = value as? String {
                    success(data)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    
    @discardableResult
    static func getCategoryByNewsID(categoryID:Int, success:@escaping([NewsCategoryModel]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getCategoryByNewsID(categoryID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [NewsCategoryModel]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let newsCategoryModel = NewsCategoryModel(JSON: item) {
                            data.append(newsCategoryModel)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }

}
