//
//  Prize.swift
//  iCNM
//
//  Created by Medlatec on 5/17/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import ObjectMapper
import RealmSwift

class Prize:Object,Mappable {
    dynamic var id = 0
    dynamic var doctorID = 0
    dynamic var name:String?
    dynamic var prizeTime:Date?
    dynamic var prizePlace:String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
//        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        id <- map["PrizeID"]
        doctorID <- map["DoctorID"]
        name <- map["PrizeName"]
        prizeTime <- (map["PrizeTime"],DateFormatterTransform(dateFormatter:dateFormatter))
        prizePlace <- map["PrizePlace"]
    }
    
}
