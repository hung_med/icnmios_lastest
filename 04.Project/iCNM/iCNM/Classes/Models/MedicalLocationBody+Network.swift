//
//  MedicalLocationBody+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension MedicalLocationBody {
    
    @discardableResult
    static func getMedicalLocationBody(success:@escaping([MedicalLocationBody]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getMedicalLocationBody, completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [MedicalLocationBody]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let medicalLocation = MedicalLocationBody(JSON: item) {
                            data.append(medicalLocation)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
