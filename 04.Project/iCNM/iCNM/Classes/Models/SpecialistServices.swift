//
//  SpecialistServices.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class SpecialistServices:Mappable {
    var active:Bool = false
    var specialName = ""
    var dateCreate = ""
    var orderNum:Int = 0
    var specialistID:Int = 0
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            active <- map["Active"]
            specialName <- map["SpecialName"]
            dateCreate <- map["DateCreate"]
            orderNum <- map["OrderNum"]
            specialistID <- map["SpecialistID"]
        }        
    }
}
