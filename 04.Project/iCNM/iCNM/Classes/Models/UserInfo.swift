//
//  UserInfo.swift
//  iCNM
//
//  Created by Medlatec on 5/16/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import ObjectMapper
import RealmSwift

class UserInfo:Object,Mappable {
    dynamic var id = 0
    dynamic var doctorID = 0
    dynamic var doctorIDCode:String?
    dynamic var userTypeID = 0
    dynamic var name:String?
    dynamic var email:String?
    dynamic var phone:String?
    dynamic var password:String?
    dynamic var avatar:String?
    dynamic var gender:String?
    dynamic var birthday:Date?
    dynamic var address:String?
    dynamic var createdDate:Date?
    dynamic var editedDate:Date?
    dynamic var verifyEmail = false
    dynamic var verifyPhone = false
    dynamic var active = false
    dynamic var districtID:String?
    dynamic var provinceID:String?
    dynamic var salt:String?
    dynamic var lastLogin:Date?
    dynamic var lastLogout:Date?
    dynamic var lastUpdate:Date?
    dynamic var education:String?
    dynamic var aboutMe:String?
    dynamic var job:String?
    dynamic var workplace:String?
    dynamic var keySearchName:String?
    dynamic var userName:String?
    dynamic var userPass:String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
//        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
//        dateFormatter2.timeZone = TimeZone(secondsFromGMT: 0)
        id <- map["UserID"]
        doctorID <- map["DoctorID"]
        doctorIDCode <- map["DoctorIDCode"]
        userTypeID <- map["UserTypeID"]
        name <- map["Name"]
        email <- map["Email"]
        phone <- map["Phone"]
        password <- map["Password"]
        avatar <- map["Avatar"]
        gender <- map["Gender"]
        birthday <- (map["BirthDay"],DateFormatterTransform(dateFormatter:dateFormatter))
        address <- map["Address"]
        createdDate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
        if createdDate == nil {
                createdDate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter2))
        }
        editedDate <- (map["DateEdit"],DateFormatterTransform(dateFormatter:dateFormatter))
        if editedDate == nil {
            editedDate <- (map["DateEdit"],DateFormatterTransform(dateFormatter:dateFormatter2))
        }
        verifyEmail <- map["VerifyEmail"]
        verifyPhone <- map["VerifyPhone"]
        active <- map["Active"]
        districtID <- map["DistrictID"]
        provinceID <- map["ProvinceID"]
        salt <- map["Salt"]
        lastLogin <- (map["LastLogin"],DateFormatterTransform(dateFormatter:dateFormatter2))
        lastLogout <- (map["LastLogout"],DateFormatterTransform(dateFormatter:dateFormatter2))
        lastUpdate <- (map["LastUpdate"],DateFormatterTransform(dateFormatter:dateFormatter2))
        education <- map["Education"]
        aboutMe <- map["AboutMe"]
        job <- map["Job"]
        workplace <- map["WorkPlace"]
        keySearchName <- map["KeySearchName"]
        userName <- map["UserName"]
        userPass <- map["UserPass"]
    }
    
    func getAvatarURL() -> URL? {
        if let avatarString = avatar?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines){
            
            if avatarString.contains("http") {
                //facebook avatar
                return URL(string: avatarString)
            } else {
                let avatarURL = API.baseURLImage + "\(API.iCNMImage)" + "\(id)" + "/\(avatarString)"
                if let otherAvatarURL = URL(string: avatarURL) {
                    return otherAvatarURL
                }
            }
        }
        return nil
    }
    
}
