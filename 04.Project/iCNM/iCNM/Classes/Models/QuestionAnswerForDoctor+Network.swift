//
//  QuestionAnswerForDoctor+Network.swift
//  iCNM
//
//  Created by Quang Hung on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension QuestionAnswerForDoctor {
    
    @discardableResult
    func getQuestionNoAnswerForDoctor(doctorID:Int, pageNumber:Int, success:@escaping([QuestionAnswerForDoctor]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getQuestionNoAnswerForDoctor(doctorID, pageNumber), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [QuestionAnswerForDoctor]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let questionAnswerForDoctor = QuestionAnswerForDoctor(JSON: item) {
                            data.append(questionAnswerForDoctor)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func getQuestionRecivedForDoctor(doctorID:Int, pageNumber:Int, success:@escaping([QuestionAnswerForDoctor]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getQuestionRecivedForDoctor(doctorID, pageNumber), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [QuestionAnswerForDoctor]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let questionAnswerForDoctor = QuestionAnswerForDoctor(JSON: item) {
                            data.append(questionAnswerForDoctor)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func getQuestionAnswerForDoctor(doctorID:Int, pageNumber:Int, success:@escaping([QuestionAnswerForDoctor]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getQuestionAnswerForDoctor(doctorID, pageNumber), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [QuestionAnswerForDoctor]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let questionAnswerForDoctor = QuestionAnswerForDoctor(JSON: item) {
                            data.append(questionAnswerForDoctor)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func registerAnswerQuestion(questionID:Int, doctorID:Int, success:@escaping(QuestionAnswerForDoctor?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .registerAnswerQuestion(questionID, doctorID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let questionAnswerForDoctor = QuestionAnswerForDoctor(JSON: item) {
                        success(questionAnswerForDoctor)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
