//
//  ScheduleDoctorOfDay.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension ScheduleDoctorOfDay {
    
    @discardableResult
    static func getListScheduleDoctorOfDay(pageNumber:Int, doctorID:Int, success:@escaping([ScheduleDoctorOfDay]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getListScheduleDoctorOfDay(pageNumber, doctorID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [ScheduleDoctorOfDay]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let scheduleDoctorOfDay = ScheduleDoctorOfDay(JSON: item) {
                            data.append(scheduleDoctorOfDay)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
