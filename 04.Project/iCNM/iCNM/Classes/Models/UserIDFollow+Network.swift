//
//  UserIDFollow+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension UserIDFollow {
    @discardableResult
    func getUserFollowAction(userID:Int, userIDFllow:Int, followID:Int, success:@escaping(UserIDFollow?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {

        return API.requestJSON(endpoint: .getUserFollowAction(userID, userIDFllow, followID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let userIDFollow = UserIDFollow(JSON: item) {
                        success(userIDFollow)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
