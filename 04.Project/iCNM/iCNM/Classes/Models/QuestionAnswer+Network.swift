//
//  QuestionAnswer+Network.swift
//  iCNM
//
//  Created by Medlatec on 6/20/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire

extension QuestionAnswer {
    private enum ThankLikeShareMode:Int {
        case Check = 0, Add, Remove
    }
    
    enum InteractionObjectType:Int {
        case Question = 0, Answer
    }
    
    @discardableResult
    static func getAllQuestionAnswer(pageNumber:Int, specialistIDs:[Int], success:@escaping([QuestionAnswer]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        let parameterDic = [
            "pageNumber" : pageNumber,
            "specialistStr" : (specialistIDs.map{String($0)}).joined(separator: ",")
            ] as [String : Any]
        return API.requestJSON(endpoint: .GetQuestionAnswer(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [QuestionAnswer]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let questionAnswer = QuestionAnswer(JSON: item) {
                            data.append(questionAnswer)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func getComments(success:@escaping([UserQuestionComment]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .GetCommentQuestion(question!.id), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [UserQuestionComment]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let userQuestionComment = UserQuestionComment(JSON: item) {
                            data.append(userQuestionComment)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func getUserInteractionInfo(userID:Int, type:InteractionObjectType, success:@escaping() -> Void, fail:@escaping(_ error:Error, _ response:DataResponse<Any>)->Void) -> Request? {
        if isLoadingUserInteractionInfo == false && userInteractionInfoLoaded == false {
            isLoadingUserInteractionInfo = true
            var parameterDic:[String:Any] = [
                "userID":userID,
                "answerID":self.answer?.id ?? 0,
                "questionID":self.question?.id ?? 0,
                "mode":ThankLikeShareMode.Check.rawValue
            ]
            if type == .Answer {
                parameterDic["questionID"] = 0
            } else {
                parameterDic["answerID"] = 0
            }
            
            return API.requestJSON(endpoint: .QAThankLikeShare(parameterDic) , completionHandler: { (response) in
                self.isLoadingUserInteractionInfo = false
                switch response.result {
                case .success(let value):
                    self.userInteractionInfoLoaded = true
                    if let data = value as? [String:Any] {
                        if let like = data["Like"] as? Bool {
                            self.liked = like
                        }
                        if let share = data["Share"] as? Bool {
                            self.shared = share
                        }
                        if let thank = data["Thank"] as? Bool {
                            self.thanked = thank
                        }
                    }
                    success()
                case .failure(let error):
                    fail(error,response)
                }
            })
        }
        return nil
    }
    
    @discardableResult
    func toggleLikeThank(userID:Int, type:InteractionObjectType, success:@escaping() -> Void, fail:@escaping(_ error:Error, _ response:DataResponse<Any>)->Void) -> Request? {
        if  userInteractionInfoLoaded == true{
            isLoadingUserInteractionInfo = true
            let parameterDic:[String:Any] = [
                "userID":userID,
                "answerID":0,
                "questionID":self.question?.id ?? 0,
                "mode": self.liked ? ThankLikeShareMode.Remove.rawValue : ThankLikeShareMode.Add.rawValue
            ]
            return API.requestJSON(endpoint: .QAThankLikeShare(parameterDic) , completionHandler: { (response) in
                switch response.result {
                case .success(let value):
                    if self.liked {
                        self.question?.countLike -= 1
                    } else {
                        self.question?.countLike += 1
                    }
                    if let dic = value as? [String:Any] {
                    if let didLike = dic["Like"] as? Bool {
                        self.liked = didLike
                    }
                    }
                    success()
                case .failure(let error):
                    fail(error,response)
                }
            })
        }
        return nil
    }
    
    @discardableResult
    func postRatingAnswer(numRate:Float, userID:Int, answerID:Int, success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        let paramDic:[String:Any] = [
            "NumRate": numRate,
            "UserID": userID,
            "AnswerID": answerID,
        ]
        return API.requestJSON(endpoint: .postRatingAnswer(paramDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func getAnswerIDFromQuestionID(questionID:Int, success:@escaping(QuestionAnswer?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getAnswerIDFromQuestionID(questionID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let answer = QuestionAnswer(JSON: item) {
                        success(answer)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func getMyQuestions(pageID:Int, userID:Int, success:@escaping([QuestionAnswer]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getMyQuestions(pageID, userID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [QuestionAnswer]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let questionAnswer = QuestionAnswer(JSON: item) {
                            data.append(questionAnswer)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func postAnswerQuestion(answer:Answer, success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        let paramDic:[String:Any] = [
            "QuestionID": answer.questionID,
            "AnswerContent": answer.content!,
            "DoctorID": answer.doctorID,
            "Approver": answer.approver,
            "AnswerRate": answer.answerRate,
            "CountLike": answer.countLike,
            "CountShare":answer.countShare,
            "CountThank":answer.countThank,
            "Comment": answer.comment!,
            "Active": answer.active
        ]
        return API.requestJSON(endpoint: .postAnswerQuestion(paramDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
