//
//  NewsCategoryModel.swift
//  iCNM
//
//  Created by Len Pham on 9/2/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class NewsCategoryModel: Object,Mappable {
    dynamic var id = 0
    dynamic var name:String?
    dynamic var userID = 0
    dynamic var selected = false

    // PRAGMA - ObjectMapper
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            id <- map["CategoryID"]
            name <- map["CategoryName"]
            userID <- map["UserID"]
        }
    }
}
