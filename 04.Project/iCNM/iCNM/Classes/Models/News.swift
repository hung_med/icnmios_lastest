//
//  News.swift
//  iCNM
//
//  Created by Hoang Van Trung on 8/3/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import ObjectMapper

class News: Mappable {
    var newsID: Int = 0
    var categoryID: Int = 0
    var title: String = ""
    var description: String = ""
    var newsContent: String = ""
    var img: String = ""
    var img1: String = ""
    var img2: String = ""
    var imgOrd: String = ""
    var imgOrd1: String = ""
    var imgOrd2: String = ""
    var userCreate:Date?
    var dateCreate:Date?
    var ordUrl: String = ""
    var keyWord: String = ""
    var countRead: Int = 0
    var lang: String = ""
    var configFile: String = ""
    var edit: String = ""
    var imgAct: String = ""
    var active: Bool = false
    var crawlerAgaint: String = ""
    var countShare: Int = 0
    var countLike: Int = 0
    var tacgia: String = ""
    var userEdit: String = ""
    var dateEdit: Date?
    var type: String = ""
    var userDuyet: String = ""
    var dateDuyet: Date?
    var duyet: String = ""
    var userNhan: String = ""
    var dateNhan: Date?
    var userPhan: String = ""
    var datePhan: Date?
    var keysearchTitle: String = ""
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            newsID <- map["NewsID"]
            categoryID <- map["CategoryID"]
            title <- map["Title"]
            description <- map["Description"]
            newsContent <- map["NewsContent"]
            img <- map["Img"]
            img1 <- map["Img1"]
            img2 <- map["Img2"]
            imgOrd <- map["ImgOrd"]
            imgOrd1 <- map["ImgOrd1"]
            imgOrd2 <- map["ImgOrd2"]
            userCreate <- map["UserCreate"]
            dateCreate <- map["DateCreate"]
            ordUrl <- map["OrdUrl"]
            keyWord <- map["KeyWord"]
            countRead <- map["CountRead"]
            lang <- map["Lang"]
            configFile <- map["ConfigFile"]
            edit <- map["Edit"]
            imgAct <- map["ImgAct"]
            active <- map["Active"]
            crawlerAgaint <- map["CrawlerAgaint"]
            countShare <- map["CountShare"]
            countLike <- map["CountLike"]
            tacgia <- map["Tacgia"]
            userEdit <- map["UserEdit"]
            dateEdit <- map["DateEdit"]
            type <- map["Type"]
            userDuyet <- map["UserDuyet"]
            dateDuyet <- map["DateDuyet"]
            duyet <- map["Duyet"]
            userNhan <- map["UserNhan"]
            dateNhan <- map["DateNhan"]
            userPhan <- map["UserPhan"]
            datePhan <- map["DatePhan"]
            keysearchTitle <- map["KeysearchTitle"]

        }
    }
}
