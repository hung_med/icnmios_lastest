//
//  QuestionAnswer.swift
//  iCNM
//
//  Created by Medlatec on 6/20/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class QuestionAnswer:Mappable {
    var userAsk:UserInfo?
    var userOfDoctor:UserInfo?
    var question:Question?
    var answer:Answer?
    var doctor:Doctor?
    var specialist:Specialist?
    var thanked = false
    var liked = false
    var shared = false
    var userInteractionInfoLoaded = false
    var isLoadingUserInteractionInfo = false
    
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        userAsk <- map["UserAsk"]
        userOfDoctor <- map["UserOfDoctor"]
        question <- map["Question"]
        answer <- map["Answer"]
        doctor <- map["Doctor"]
        specialist <- map["Specialist"]
        
    }
    
}
