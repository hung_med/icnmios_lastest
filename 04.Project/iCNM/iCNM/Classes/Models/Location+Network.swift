//
//  Location+Network.swift
//  iCNM
//
//  Created by Medlatec on 7/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Alamofire

extension Location {

    static func getMedicalUnitDetail(locationID:Int, completionHandler: @escaping(Location?, Error?) -> ()) {
        
        let urlStr = Constant.apiBaseUrl + "api/locations/" + "\(locationID)"
        
        let escapedUrl = urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        Alamofire.request(escapedUrl!, method: .get, parameters: nil,
                          encoding: URLEncoding.default).validate().responseJSON
            { response in
                switch response.result {
                case .success(let value):
    
                    let data = value as? [String:Any]
                    let location = Location(JSON: data!)
                    completionHandler(location, nil)
                    
                case .failure(let error):
                    print("Error000: \(error)")
                
                }
        }
        
    }
    
}
