//
//  GetPatientUpload+Network.swift
//  iCNM
//
//  Created by Thanh Huyen on 4/24/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import Foundation
import Alamofire

extension GetPatientUpload {
    @discardableResult
    static func getPatientUpload(params:[String:Any], success:@escaping([Patient]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getPatientUpload(params), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [Patient]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let resultPatient = Patient(JSON: item) {
                            data.append(resultPatient)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
