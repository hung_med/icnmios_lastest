//
//  GetPatientUpload.swift
//  iCNM
//
//  Created by Thanh Huyen on 4/24/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper

class GetPatientUpload: Mappable {
    var pid = ""
    var organizeID = 0
    var medicalProfileID = 0
    var doctorID = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .toJSON {
            pid <- map["pid"]
            organizeID <- map["organizeID"]
            medicalProfileID <- map["medicalProfileID"]
            doctorID <- map["doctorID"]
        }
    }
}
