//
//  FeaturedNews+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension FeaturedNews {
   
    @discardableResult
    static func getAllFeaturedNews(success:@escaping([FeaturedNews]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getFeaturedNews, completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [FeaturedNews]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let featuredNews = FeaturedNews(JSON: item) {
                            data.append(featuredNews)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    static func getRelatedNewsTK(stringRequest:String, success:@escaping([FeaturedNews])->Void, fail:@escaping(_ error:Error, _ response:DataResponse<Any>)->Void)->Request {
        return API.requestJSON(endpoint: .getRelatedNewsTK(stringRequest), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [FeaturedNews]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let featuredNews = FeaturedNews(JSON: item) {
                            data.append(featuredNews)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
