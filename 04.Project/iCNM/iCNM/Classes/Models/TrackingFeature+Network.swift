//
//  TrackingFeature+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension TrackingFeature {
    @discardableResult
    func checkTrackingFeature(fcm:String, featureName:String, categoryID:Int, success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        var parameterDic:[String:Any]?
        if categoryID != 0{
            parameterDic = [
                "FCM": fcm,
                "FeatName": featureName,
                "CategoryID": categoryID
            ]
        }else{
            parameterDic = [
                "FCM": fcm,
                "FeatName": featureName
            ]
        }
        return API.requestJSON(endpoint: .checkTrackingFeature(parameterDic!), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
