//
//  MedicalLocationBody.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class MedicalLocationBody:Mappable {
    var active:Bool = false
    var dateCreate = ""
    var locationBodyID = 0
    var locationBodyName = ""
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            active <- map["Active"]
            dateCreate <- map["DateCreate"]
            locationBodyID <- map["LocationBodyID"]
            locationBodyName <- map["LocationBodyName"]
        }        
    }
}
