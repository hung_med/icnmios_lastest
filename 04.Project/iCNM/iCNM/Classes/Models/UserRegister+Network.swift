//
//  UserRegister+Network.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire

extension UserRegister {
    @discardableResult
    func sendVerifyPhone(code:String,areaCode:String, success:@escaping(String) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<String>) -> Void) -> Request {
        
        let parameterDic:[String:Any] = [
            "Mes":code,
            "PhoneNum":areaCode + (self.phoneNumber.hasPrefix("0") ? self.phoneNumber.substring(from: self.phoneNumber.index(phoneNumber.startIndex,offsetBy:1)) : self.phoneNumber)
        ]
        
        return API.requestString(endpoint: .SendMessage(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func register(success:@escaping(UserInfo) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<UserInfo>) -> Void) -> Request {
        
        let parameterDic = self.toJSON()
    
        return API.requestObjectNoRealm(type: UserInfo.self, endpoint: .Register(parameterDic) , success: { (userInfo) in
            success(userInfo)
        }, fail: { (error, response) in
            fail(error,response)
        })

    }
}
