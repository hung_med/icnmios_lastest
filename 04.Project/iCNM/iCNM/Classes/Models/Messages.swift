//
//  Messages.swift
//  iCNM
//
//  Created by Thành Trung on 10/04/2018.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import Foundation

class Messages {
    
    var Text : String
    var SenderId : String
    var ReceiverId : String
    var Time : NSNumber
    var Status : String?
    var ChatId : String?
    var PhotoUrl : String?
    var ThumnailUrl : String?
    var VideoUrl : String?
    var NewMess : String?
    
    init(TextString : String,SenderIdString : String, ReceiverIdString : String,TimeNumber : NSNumber) {
        Text = TextString
        SenderId = SenderIdString
        ReceiverId = ReceiverIdString
        Time = TimeNumber
    }
    
}
