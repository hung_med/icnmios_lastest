//
//  FeaturedDoctor+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension FeaturedDoctor {
        
    @discardableResult
    static func getAllFeaturedDoctor(success:@escaping([FeaturedDoctor]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getFeaturedDoctor, completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [FeaturedDoctor]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let featuredDoctor = FeaturedDoctor(JSON: item) {
                            data.append(featuredDoctor)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func getDoctorRatedHomeFixByPage(pageNumber:Int, success:@escaping([FeaturedDoctor]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getDoctorRatedHomeFixByPage(pageNumber), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [FeaturedDoctor]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let featuredDoctor = FeaturedDoctor(JSON: item) {
                            data.append(featuredDoctor)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
