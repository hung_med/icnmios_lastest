//
//  NewsCategory.swift
//  iCNM
//
//  Created by Len Pham on 7/30/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class NewsCategory: Mappable {
    dynamic var id = 0
    dynamic var orderNum:Int = 0
    dynamic var name:String?

    var listNews : [NewsModel] = [NewsModel]()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            id <- map["Category.CategoryID"]
            orderNum <- map["Category.OrderNum"]
            name <- map["Category.CategoryName"]
            listNews <- map["News"]
        }
    }
}
