//
//  LookupResultObject.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class LookupResultObject:Mappable {
    var type:String? = nil
    var lookUpResult = [Patient]()
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            type <- map["type"]
            lookUpResult <- map["listResult"]
           
        }
    }
}
