//
//  Search+Network.swift
//  iCNM
//
//  Created by Hoang Van Trung on 8/7/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire

extension Search {
    @discardableResult
    func getSearchSuggest(param: [String: Any], success:@escaping(Search?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getSearchSuggest(param), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let searchResult = Search(JSON: item) {
                        success(searchResult)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    func getSearch(param: [String: Any], success:@escaping(Search?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getSearch(param), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let searchResult = Search(JSON: item) {
                        success(searchResult)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
