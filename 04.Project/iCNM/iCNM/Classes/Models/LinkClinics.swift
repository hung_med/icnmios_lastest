//
//  LinkClinics.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class LinkClinics:Mappable {
    var serDisID = 0
    var serviceID = 0
    var serviceName = ""
    var organizeName = ""
    var image = ""
    var active:Bool = false
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            serDisID <- map["SerDisID"]
            serviceID <- map["ServiceID"]
            serviceName <- map["ServiceName"]
            organizeName <- map["OrganizeName"]
            image <- map["Image"]
            active <- map["Active"]
        }        
    }
}
