//
//  UserTag.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class UserTag:Mappable {
    var keywordTag = [KeyWordTag]()
    var userKeywordTags = [UserKeyWord]()
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            keywordTag <- map["KeyWorld"]
            userKeywordTags <- map["UserKeyWorld"]
        }
    }
}
