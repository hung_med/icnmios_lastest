//
//  CheckPatient.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class CheckPatient:Mappable {
    var userID:String = ""
    var password:String = ""
    var doctorID:String = ""
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            userID <- map["UserID"]
            password <- map["Password"]
            doctorID <- map["DoctorID"]
        }
    }
}
