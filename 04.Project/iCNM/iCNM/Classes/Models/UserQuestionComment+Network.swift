//
//  UserQuestionComment+Network.swift
//  iCNM
//
//  Created by Medlatec on 6/28/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import Alamofire

extension UserQuestionComment {
    @discardableResult
    func addUpdateComment(success:@escaping(UserQuestionComment?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        let paramDic = self.toJSON()
        return API.requestJSON(endpoint: .AddUpdateCommentQuestion(paramDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let questionComment = UserQuestionComment(JSON: item) {
                        success(questionComment)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
