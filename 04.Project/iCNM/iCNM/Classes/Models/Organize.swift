//
//  Organize.swift
//  iCNM
//
//  Created by Medlatec on 7/27/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Organize:Object,Mappable {
    dynamic var id = 0
    dynamic var organizeCode:String?
    dynamic var organizeName:String?
    dynamic var organizeAddress:String?
    dynamic var active = false
    dynamic var imgString:String?
    dynamic var isShowResult = false
    let timeRanges = List<ScheduleTime>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "HH:mm:ss"
        id <- map["OrganizeID"]
        organizeCode <- map["OrganizeCode"]
        organizeName <- map["OrganizeName"]
        organizeAddress <- map["OrganizeAddress"]
        active <- map["Active"]
        imgString <- map["ImageStr"]
        isShowResult <- map["IsShowResult"]
        let realm = try! Realm()
        if let currentObject = realm.object(ofType: Organize.self, forPrimaryKey:id) {
            timeRanges.append(objectsIn: currentObject.timeRanges)
        }
    }
    
}
