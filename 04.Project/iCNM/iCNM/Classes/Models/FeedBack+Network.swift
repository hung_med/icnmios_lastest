//
//  FeedBack+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension FeedBack {
    @discardableResult
    func sendFeedBack(userID:Int, name:String, email:String, phoneNumber:String, content:String, success:@escaping(FeedBack?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
       
        let parameterDic:[String:Any] = [
            "UserID": userID,
            "Active": true,
            "Name": name,
            "Email": email,
            "Phone": phoneNumber,
            "FbContent":content
        ]
        return API.requestJSON(endpoint: .sendFeedBack(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let fb = FeedBack(JSON: item) {
                        success(fb)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
