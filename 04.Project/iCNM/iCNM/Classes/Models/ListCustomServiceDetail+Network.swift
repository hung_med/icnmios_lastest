//
//  ListCustomServiceDetail+Network.swift
//  iCNM
//
//  Created by Quang Hung on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension ListCustomServiceDetail {
    @discardableResult
    func getFindService(provinceID:Int, gender:Int, age:Int, price:Int, success:@escaping([ListCustomServiceDetail]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getFindService(provinceID, gender, age, price), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [ListCustomServiceDetail]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let listCustomServiceObj = ListCustomServiceDetail(JSON: item) {
                            data.append(listCustomServiceObj)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
