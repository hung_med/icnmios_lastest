//
//  ServiceRating.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class ServiceRating:Mappable {
    var serRateID:Int = 0
    var serRateContent:String = ""
    var serRateNum:Float = 0.0
    var dateCreate:Date? = nil
    var dateEdit:Date? = nil
    var serviceID:Int = 0
    var userID:Int = 0
    var active:Bool? = false
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        if map.mappingType == .fromJSON {
            serRateID <- map["SerRateID"]
            serRateContent <- map["SerRateContent"]
            serRateNum <- map["SerRateNum"]
            dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
            if dateCreate == nil {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
            }
            dateEdit <- map["DateEdit"]
            serviceID <- map["ServiceID"]
            userID <- map["UserID"]
            active <- map["Active"]
        }
    }
}
