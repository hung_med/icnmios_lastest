//
//  NewsModel+Network.swift
//  iCNM
//
//  Created by Len Pham on 8/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import Alamofire

extension NewsModel {
    @discardableResult
    static func getListNewsOfCategory(pageNumber:Int, countNumber:Int ,specialistID:Int, success:@escaping([NewsModel]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        var stringParam: String = String()
        stringParam = stringParam.appendingFormat("%d,%d,%d",pageNumber,countNumber,specialistID)
        return API.requestJSON(endpoint: .getNewsOfCategory(stringParam), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [NewsModel]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let newsModel = NewsModel(JSON: item) {
                            data.append(newsModel)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    static func getRelatedNews(newsId:Int ,specialistID:Int, success:@escaping([NewsModel]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        var stringParam: String = String()
        stringParam = stringParam.appendingFormat("%d,%d",newsId,specialistID)
        
        return API.requestJSON(endpoint: .getRelatedNews(stringParam), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [NewsModel]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let newsModel = NewsModel(JSON: item) {
                            data.append(newsModel)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    static func getNewsDetail(newsId:Int, success:@escaping(NewsModel) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        var stringParam: String = String()
        stringParam = stringParam.appendingFormat("%d",newsId)
        
        return API.requestJSON(endpoint: .getNewsDetail(stringParam), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var newsModelResponse: NewsModel = NewsModel()
                if let valueDict = value as? [String:Any] {
                        if let newsModel = NewsModel(JSON: valueDict) {
                           newsModelResponse = newsModel
                        }
                }
                success(newsModelResponse)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    static func postUpdateRead(newsId:Int, success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .postUpdateRead(newsId), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    static func postLikeNews(newsId:Int, userId:Int, success:@escaping(Bool) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        var stringParam: String = String()
        stringParam = stringParam.appendingFormat("%d,%d",newsId,userId)
        
        return API.requestJSON(endpoint: .postLikeNews(stringParam), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let data = (value as AnyObject).boolValue {
                    success(data)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    static func getLikeNews(newsId:Int, userId:Int, success:@escaping(Bool) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        var stringParam: String = String()
        stringParam = stringParam.appendingFormat("%d,%d",newsId,userId)
        
        return API.requestJSON(endpoint: .getLikeNews(stringParam), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                let result = (value as AnyObject).doubleValue
                if result == 2{
                    success(false)
                }else{
                    success((value as AnyObject).boolValue)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}

