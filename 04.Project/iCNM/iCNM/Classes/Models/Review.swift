//
//  Review.swift
//  iCNM
//
//  Created by Nguyen Van Dung on 8/12/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import ObjectMapper

class Review: Mappable {
    
    var rateContent:String = ""
    var general:Double = 0.0
    var medicalEthics:Double = 0.0
    var technique:Double = 0.0
    var userID:Int = 0
    var locationID:Int = 0
    var sumRate:Double = 0.0

    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        rateContent <- map["RateContent"]
        general <- map["General"]
        medicalEthics <- map["MedicalEthics"]
        technique <- map["Technique"]
        userID <- map["UserID"]
        locationID <- map["LocationID"]
        sumRate <- map["SumRate"]
    }
}
