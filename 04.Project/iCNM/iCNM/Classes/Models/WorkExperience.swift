//
//  WorkExperience.swift
//  iCNM
//
//  Created by Medlatec on 5/17/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import ObjectMapper
import RealmSwift

class WorkExperience:Object,Mappable {
    dynamic var id = 0
    dynamic var doctorID = 0
    dynamic var company:String?
    dynamic var position:String?
    dynamic var startDate:Date?
    dynamic var endDate:Date?
    dynamic var summary:String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
//        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        id <- map["WorkExperienceID"]
        doctorID <- map["DoctorID"]
        company <- map["Company"]
        position <- map["Position"]
        startDate <- (map["StartDate"],DateFormatterTransform(dateFormatter:dateFormatter))
        endDate <- (map["EndDate"],DateFormatterTransform(dateFormatter:dateFormatter))
        summary <- map["SummaryWork"]
    }
    
}
