//
//  FCM.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class FCM:Mappable {
    var key = ""
    var userID:Int = 0
    var doctorID:Int = 0
    var doctorIDCode = ""
    var dateCreate = ""
    var dateEdit = ""
    var lastLogin = ""
    var lastLogout = ""
    var deviceType:Int = 0
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            key <- map["FCMKey"]
            userID <- map["UserID"]
            doctorID <- map["DoctorID"]
            doctorIDCode <- map["DoctorIDCode"]
            dateCreate <- map["DateCreate"]
            dateEdit <- map["DateEdit"]
            lastLogin <- map["LastLogin"]
            lastLogout <- map["LastLogout"]
            deviceType <- map["DeviceType"]
        }
    }
}
