//
//  ResultAds.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class ResultAds:Mappable {
    var adsID:Int = 0
    var link:String = ""
    var orgName:String = ""
    var img:String = ""
    var widthDivHeight:Int = 0
    var adsType:String = ""
    var serviceID:String = ""
    var active:Bool? = false
    var countView:Int = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            adsID <- map["AdsID"]
            link <- map["Link"]
            orgName <- map["OrgName"]
            widthDivHeight <- map["OrgName"]
            img <- map["Img"]
            adsType <- map["AdsType"]
            serviceID <- map["ServiceID"]
            active <- map["Active"]
            countView <- map["CountView"]
       }
    }
}
