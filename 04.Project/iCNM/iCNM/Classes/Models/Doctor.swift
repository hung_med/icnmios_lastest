//
//  Doctor.swift
//  iCNM
//
//  Created by Medlatec on 5/11/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import ObjectMapper
import RealmSwift

class Doctor:Object,Mappable {
    dynamic var id = 0
    dynamic var userID = 0
    dynamic var startTime:Date?
    dynamic var summary:String?
    dynamic var showPhone = false
    dynamic var answer = false
    dynamic var verify = false
    dynamic var block = false
    dynamic var rating:Double = 0
    dynamic var specialistStr:String?
    dynamic var timeWorkStr:String?
    dynamic var doctorName:String?
    dynamic var image:String?
    dynamic var showHome = false
    dynamic var position:String?
    dynamic var specialistRegister:String?
    dynamic var timeRegister:String?
    dynamic var isCheckRegister:Date?
    dynamic var rejectAnswer:Date?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["DoctorID"]
        userID <- map["UserID"]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        startTime <- (map["StartTime"],DateFormatterTransform(dateFormatter:dateFormatter))
        summary <- map["Summary"]
        showPhone <- map["ShowPhone"]
        answer <- map["Answer"]
        verify <- map["Verify"]
        block <- map["Block"]
        rating <- map["Rating"]
        specialistStr <- map["SpecialistStr"]
        timeWorkStr <- map["TimeWorkStr"]
        doctorName <- map["DoctorName"]
        image <- map["Img"]
        showHome <- map["ShowHome"]
        position <- map["Position"]
        specialistRegister <- map["SpecialistRegister"]
        timeRegister <- map["TimeRegister"]
        isCheckRegister <- map["IsCheckRegister"]
        rejectAnswer <- map["RejectAnswer"]
    }
    
}
