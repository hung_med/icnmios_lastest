//
//  CustomGroupTest.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class CustomGroupTest:Mappable {
    var groupTest:GroupTest?
    var testCodes: [Test] = [Test]()
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            groupTest <- map["GroupTest"]
            testCodes <- map["TestCodes"]
        }
    }
}
