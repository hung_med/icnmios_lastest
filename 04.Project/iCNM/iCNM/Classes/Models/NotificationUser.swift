//
//  NotificationUser.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class NotificationUser:Mappable {
    
    var notificationID:Int = 0
    var userID:Int = 0
    var doctorID:Int = 0
    var doctorIDCode:Int = 0
    var title = ""
    var description = ""
    var data = ""
    var organizeID:Int = 0
    var dateCreate = ""
    var send:Bool? = false
    var img = ""
    var listNewsID = ""
    var isRead:Bool? = false
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
      if map.mappingType == .fromJSON {
            notificationID <- map["NotificationID"]
            userID <- map["UserID"]
            doctorID <- map["DoctorID"]
            doctorIDCode <- map["DoctorIDCode"]
            title <- map["Title"]
            description <- map["Description"]
            data <- map["Data"]
            organizeID <- map["OrganizeID"]
            dateCreate <- map["DateCreate"]
            img <- map["Img"]
            send <- map["Send"]
            isRead <- map["IsRead"]
            listNewsID <- map["RelatedNew"]
        }
    }
}
