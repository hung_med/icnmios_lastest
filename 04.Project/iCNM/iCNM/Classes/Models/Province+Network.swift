//
//  Province+Network.swift
//  iCNM
//
//  Created by Medlatec on 5/11/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension Province {
    @discardableResult static func requestAllProvinces(success:@escaping([Province]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>) -> Void) -> Request {
        return API.requestJSON(endpoint: .GetProvinces, completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [Province]()
                if let array = value as? [[String:Any]] {
                    let realm = try! Realm()
                    for item in array {
                        if let province = Province(JSON: item) {
                            try! realm.write {
                                data.append(realm.create(Province.self, value: province, update: true))
                            }
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func requestAllDistrict(success:@escaping([District]) -> Void, fail:@escaping(_ error:Error, _ response:DataResponse<Any>) -> Void) -> Request {
        return API.requestJSON(endpoint: .GetDistrictByProvinceID(self.id!), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [District]()
                if let array = value as? [[String:Any]] {
                    let realm = try! Realm()
                    for item in array {
                        if let district = District(JSON: item) {
                            try! realm.write {
                                let ds = realm.create(District.self, value: district, update: true)
                                self.districts.append(ds)
                                data.append(ds)
                            }
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
