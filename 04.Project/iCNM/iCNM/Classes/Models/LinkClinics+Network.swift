//
//  LinkClinics+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/18.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension LinkClinics {
    
    @discardableResult
    static func getAllLinkClinics(success:@escaping([LinkClinics]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getAllLinkClinics, completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [LinkClinics]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let linkClinic = LinkClinics(JSON: item) {
                            data.append(linkClinic)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
