//
//  ProfileDoctor.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class ProfileDoctor:Mappable {
    var id = 0
    var userInfo:UserInfo?
    var doctor:Doctor?
    var user:User?
    var doctorInformation:DoctorInformation?
    var userFollow:UserIDFollow?
    var followNum:Int = 0
    var followedNum:Int = 0
    var rateNum:Int = 0
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            id <- map["User.UserID"]
        }
        userInfo <- map["User"]
        doctor <- map["Doctor"]
        doctorInformation <- map["DoctorInfomation"]
        userFollow <- map["UserFllow"]
        followNum <- map["FollowNum"]
        followedNum <- map["FollowedNum"]
        rateNum <- map["RateNum"]
    }
}
