//
//  UserRegister.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class AnswerQuestion:Mappable {
    var userID = ""
    var avatar = ""
    var questionId = ""
    var questionTitle = ""
    var questionContent = ""
    var dateCreate:Date?
    var fullName_Ques = ""
    
    var doctorID = ""
    var name_doctor = ""
    var avatar_doctor = ""
    var dateCreate_doctor:Date?
    var specialName = ""
    var answer = ""
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormatter2.timeZone = TimeZone(secondsFromGMT: 0)
        if map.mappingType == .fromJSON {
            userID <- map["UserAsk.UserID"]
            answer <- map["Answer.AnswerContent"]
            specialName <- map["Specialist.SpecialName"]
            
            doctorID <- map["UserOfDoctor.UserID"]
            name_doctor <- map["UserOfDoctor.Name"]
            avatar_doctor <- map["UserOfDoctor.Avatar"]
            dateCreate_doctor <- (map["Answer.DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter2))
            questionId <- map["Question.QuestionID"]
            questionTitle <- map["Question.QuestionTitle"]
            fullName_Ques <- map["Question.FullName"]
            questionContent <- map["Question.QuestionContent"]
            dateCreate <- (map["Question.DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter2))
        }        
    }
}
