//
//  MedicalHealthFactoryUser+Network.swift
//  iCNM
//
//  Created by Ta Quang Hung on 20/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import Alamofire

extension MedicalHealthFactoryUser {
    
    @discardableResult
    func addEditUserMedicalProfiles(success:@escaping(MedicalHealthFactoryUser?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        //var parameterDic:[String:Any]?
        let paramDic = self.toJSON()
        return API.requestJSON(endpoint: .addEditUserMedicalProfiles(paramDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let medicalHealthFactoryUser = MedicalHealthFactoryUser(JSON: item) {
                        success(medicalHealthFactoryUser)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func addEditMedicalHealthFactory(success:@escaping(MedicalHealthFactoryUser?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        var medicalHealthFactory = [String: Any]()
        medicalHealthFactory = (self.medicalHealFactory?.toJSON())!
        var medicalProfile = [String: Any]()
        medicalProfile = (self.medicalProfile?.toJSON())!
        var paramDic = [String: Any]()
        paramDic = ["MedicalFor": self.medicalFor,
                    "MedicalHealthFactory":medicalHealthFactory,
                    "MedicalProfile":medicalProfile,
                    "UserID":self.userID]
        
        return API.requestJSON(endpoint: .addEditMedicalHealthFactory(paramDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let medicalHealthFactoryUser = MedicalHealthFactoryUser(JSON: item) {
                        success(medicalHealthFactoryUser)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func updateAvatarHSSK(parameterDic:[String:Any],success:@escaping(String)->Void, fail:@escaping (_ error:Error, _ response:DataResponse<String>) -> Void) -> Request {
        return API.requestString(endpoint: .UpdateAvatarHSSK(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
