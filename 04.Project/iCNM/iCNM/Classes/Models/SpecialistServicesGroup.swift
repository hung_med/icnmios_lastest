//
//  SpecialistServicesGroup.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class SpecialistServicesGroup:Mappable {
    var active:Bool = false
    var keySearchName = ""
    var meanSocial = ""
    var meanSpecial = ""
    var name = ""
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            active <- map["Active"]
            keySearchName <- map["KeySearchName"]
            meanSocial <- map["MeanSocial"]
            meanSpecial <- map["MeanSpecial"]
            name <- map["Name"]
        }        
    }
}
