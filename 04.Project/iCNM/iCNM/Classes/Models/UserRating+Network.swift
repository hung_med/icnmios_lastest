//
//  UserRating+Network.swift
//  iCNM
//
//  Created by Nguyen Van Dung on 8/5/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire

extension UserRating {
    
    @discardableResult
    static func addEditRatingUserDoctor(isUpdate:Bool, rateID:Int, rateContent:String, technique:Double, medicalEthics:Double, general:Double, sumRate:Float, doctorID:Int, userID:Int, success:@escaping(Rating?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {

        var parameterDic:[String:Any]?
        if isUpdate{
            parameterDic = [
                "rateID": rateID,
                "RateContent": rateContent,
                "Technique": technique,
                "General": general,
                "MedicalEthics": medicalEthics,
                "SumRate":sumRate,
                "DoctorID":doctorID,
                "UserID": userID
            ]
        } else{
            parameterDic = [
                "RateContent": rateContent,
                "Technique": technique,
                "General": general,
                "MedicalEthics": medicalEthics,
                "SumRate":sumRate,
                "DoctorID":doctorID,
                "UserID": userID
            ]
        }

        return API.requestJSON(endpoint: .addEditRatingUserDoctor(parameterDic!), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let rating = Rating(JSON: item) {
                        success(rating)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    static func getUserRating(locationID: Int, completionHandler: @escaping([UserRating]?, Error?) -> ()) {
        
        let urlStr = Constant.apiBaseUrl + "api/ratings/LocationID/\(locationID)"
        
        let escapedUrl = urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        Alamofire.request(escapedUrl!, method: .get, parameters: nil,
                          encoding: URLEncoding.default).validate().responseJSON { response in
                            switch response.result {
                            case .success(let value):
                                
                                var data = [UserRating]()
                                
                                if let array = value as? [[String:Any]] {
                                    for item in array {
                                        let userRating:UserRating = UserRating()
                                        
                                        let userInfor = UserInfo(JSON: item["User"] as! [String:Any])
                                        let ratingInfor = Rating(JSON: item["Rating"] as! [String:Any])
                                        
                                        userRating.user = userInfor
                                        userRating.rating = ratingInfor
                                        
                                        data.append(userRating)
                                    }
                                }
                                
                                completionHandler(data, nil)
                                
                            case .failure(let error):
                                print("GetUserRating Error : \(error.localizedDescription)")
                            }
        }
        
    }
    
    static func getUserDoctorByID(doctorID:Int, pageNumber:Int, completionHandler: @escaping([UserRating]?, Error?) -> ()) {
        let urlStr = Constant.apiBaseUrl + "api/User/getRatingDoctorByPage?doctorID=\(doctorID)&pageNumber=\(pageNumber)"
        let escapedUrl = urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        Alamofire.request(escapedUrl!, method: .get, parameters: nil,
                          encoding: URLEncoding.default).validate().responseJSON { response in
                            switch response.result {
                            case .success(let value):
                                var data = [UserRating]()
                                if let array = value as? [[String:Any]] {
                                    for item in array {
                                        let userRating:UserRating = UserRating()
                                        
                                        let userInfor = UserInfo(JSON: item["User"] as! [String:Any])
                                        let ratingInfor = Rating(JSON: item["Rating"] as! [String:Any])
                                        
                                        userRating.user = userInfor
                                        userRating.rating = ratingInfor
                                        data.append(userRating)
                                    }
                                }
                                
                            completionHandler(data, nil)
                            case .failure(let error):
                                print("GetUserRating Error : \(error.localizedDescription)")
                            }
        }
    }
}
