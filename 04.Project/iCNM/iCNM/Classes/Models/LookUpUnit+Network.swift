//
//  LookUpUnit+Network.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension LookUpUnit {
    
    @discardableResult
    func checkLookUpUnit(user:String, password:String, organizeID:String, success:@escaping(LookUpUnit?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        var parameterDic:[String:Any]?
        
        parameterDic = [
                "UserWeb": user,
                "PassWeb": password,
                "OrganizeID": organizeID
        ]
        
        return API.requestJSON(endpoint: .getLookUpUnit(parameterDic!), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let lookUpObject = LookUpUnit(JSON: item) {
                        success(lookUpObject)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
