//
//  ServiceDetail.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class ServiceDetail:Mappable {
 //   var service:Service? = nil
    var serviceDetailID:Int = 0
    var serviceID:Int = 0
    var testCode:String = ""
    var testName:String = ""
    var price:Float = 0.0
    var groupTestName:String = ""
    var orderNumber:String = ""
//    var provinceName = ""
//    var services = ""
//    var location = ""
//    var serviceGroupsTest = ""
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        serviceDetailID <- map["ServiceDetailID"]
        serviceID <- map["ServiceID"]
        testCode <- map["TestCode"]
        testName <- map["TestName"]
        price <- map["Price"]
        groupTestName <- map["GroupTestName"]
        orderNumber <- map["OrderNumber"]
//        service <- map["Service"]
//        provinceName <- map["ProvinceName"]
//        services <- map["Services"]
//        location <- map["Location"]
//        serviceGroupsTest <- map["ServiceGroupsTest"]
    }
}
