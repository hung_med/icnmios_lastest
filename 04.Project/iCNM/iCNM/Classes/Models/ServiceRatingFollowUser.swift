//
//  ServiceRatingFollowUser.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class ServiceRatingFollowUser:Mappable {
    var serviceRating:ServiceRating? = nil
    var user:Users? = nil
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            serviceRating <- map["ServiceRating"]
            user <- map["User"]
        }
    }
}
