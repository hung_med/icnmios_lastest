//
//  QuestionComment.swift
//  iCNM
//
//  Created by Medlatec on 6/28/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class QuestionComment:Mappable {
    var id = 0
    var questionID = 0
    var userID = 0
    var comment = ""
    var dateCreate:Date?
    var dateEdit:Date?
    var active = false
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
//        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        id <- map["QuestionCommentID"]
        questionID <- map["QuestionID"]
        userID <- map["UserID"]
        comment <- map["Comment"]
        dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
        dateEdit <- (map["DateEdit"],DateFormatterTransform(dateFormatter:dateFormatter))
        active <- map["Active"]
        if dateCreate == nil {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
        }
        if dateEdit == nil {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateEdit <- (map["DateEdit"],DateFormatterTransform(dateFormatter:dateFormatter))
        }
    }
    
}
