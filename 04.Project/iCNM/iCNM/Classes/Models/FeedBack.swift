//
//  FeedBack.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class FeedBack:Mappable {
    var userID:Int = 0
    var active:Bool? = false
    var name = ""
    var email = ""
    var phone = ""
    var content = ""
    var dateCreate = ""
    var userTypeID:Int = 0
    var locationID:Int = 0
    var answerID = ""
    var answerContent = ""
    var answerDate = ""
    var type:Int = 0
    var typeName = ""
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            userID <- map["UserID"]
            active <- map["Active"]
            name <- map["Name"]
            email <- map["Email"]
            phone <- map["Phone"]
            content <- map["FbContent"]
            dateCreate <- map["DateCreate"]
            userTypeID <- map["UserTypeID"]
            locationID <- map["LocationID"]
            answerID <- map["AnswerID"]
            answerContent <- map["AnswerContent"]
            answerDate <- map["AnswerDate"]
            type <- map["Type"]
            typeName <- map["TypeName"]
        }
    }
}
