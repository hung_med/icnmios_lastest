//
//  Charity+Network.swift
//  iCNM
//
//  Created by Quang Hung on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension Charity {
    
    @discardableResult
    func sendCharity(userID:Int, success:@escaping(Charity?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        let parameterDic:[String:Any] = [
            "UserID": userID
        ]
        return API.requestJSON(endpoint: .sendCharity(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let charity = Charity(JSON: item) {
                        success(charity)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func getBannerCharity(userID:Int, success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getBannerCharity(userID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
