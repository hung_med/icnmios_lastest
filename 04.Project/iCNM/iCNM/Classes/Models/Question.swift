//
//  Question.swift
//  iCNM
//
//  Created by Medlatec on 6/20/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class Question:Mappable {
    var id = 0
    var title:String?
    var content:String?
    var specialistID = 0
    var userID = 0
    var fullname:String?
    var phone:String?
    var email:String?
    var address:String?
    var gender:String?
    var birthday:String?
    var reader = 0
    var countRead = 0
    var countAnswer = 0
    var active = false
    var doctorID = 0
    var expireTime:Date?
    var imgStr:String?
    var imgThumbnailStr:String?
    var dateCreate:Date?
    var countLike = 0
    var countComment = 0
    var countShare = 0
    var questionImage:String?
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
//        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        if map.mappingType == .fromJSON {
            id <- map["QuestionID"]
            reader <- map["Reader"]
            countRead <- map["CountRead"]
            countAnswer <- map["CountAnswer"]
            questionImage <- map["QuestionImage"]
            active <- map["Active"]
            expireTime <- (map["ExpireTime"],DateFormatterTransform(dateFormatter:dateFormatter))
            imgThumbnailStr <- map["ImgThumbnailStr"]
            dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
            countLike <- map["CountLike"]
            countComment <- map["CountComment"]
            countShare <- map["CountShare"]
            if dateCreate == nil {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
            }
            if expireTime == nil {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                expireTime <- (map["ExpireTime"],DateFormatterTransform(dateFormatter:dateFormatter))
            }            
        }
        title <- map["QuestionTitle"]
        content <- map["QuestionContent"]
        specialistID <- map["SpecialistID"]
        userID <- map["UserID"]
        fullname <- map["FullName"]
        phone <- map["Phone"]
        email <- map["Email"]
        address <- map["Address"]
        doctorID <- map["DoctorID"]
        gender <- map["Gender"]
       // birthday <- map["BirthYear"]
        imgStr <- map["ImgStr"]
        questionImage <- map["QuestionImage"]
      //  countLike <- map["CountLike"]
      //  countComment <- map["CountComment"]
      //  countShare <- map["CountShare"]
    }
    
}
