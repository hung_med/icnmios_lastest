//
//  Answer.swift
//  iCNM
//
//  Created by Medlatec on 6/20/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class Answer:Mappable {
    var id = 0
    var questionID = 0
    var content:String?
    var dateCreate:Date?
    var doctorID = 0
    var approver = false
    var approveTime:Date?
    var answerRate = 0
    var comment:String?
    var keyword:String?
    var countLike = 0
    var countShare = 0
    var active = false
    var countThank = 0
    var countRate:Int = 0
    var sumRate:Float = 0.0

    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
//        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        id <- map["AnswerID"]
        questionID <- map["QuestionID"]
        content <- map["AnswerContent"]
        dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
        doctorID <- map["DoctorID"]
        approver <- map["Approver"]
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        approveTime <- (map["ApproveTime"],DateFormatterTransform(dateFormatter:dateFormatter))
        answerRate <- map["AnswerRate"]
        comment <- map["Comment"]
        keyword <- map["KeyWord"]
        countLike <- map["CountLike"]
        countShare <- map["CountShare"]
        active <- map["Active"]
        countThank <- map["CountThank"]
        countRate <- map["CountRate"]
        sumRate <- map["SumRate"]
        if dateCreate == nil {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
        }
        if approveTime == nil {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            approveTime <- (map["ExpireTime"],DateFormatterTransform(dateFormatter:dateFormatter))
        }
    }
    
}

