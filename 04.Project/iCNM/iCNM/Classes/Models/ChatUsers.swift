//
//  ChatUsers.swift
//  iCNM
//
//  Created by Thành Trung on 10/04/2018.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import Foundation

class ChatUsers {
    var UserId : String
    var Email : String
    var Name : String
    
    init(UserIdString : String, EmailString : String, NameString : String) {
        UserId = UserIdString
        Email = EmailString
        Name = NameString
    }
}
