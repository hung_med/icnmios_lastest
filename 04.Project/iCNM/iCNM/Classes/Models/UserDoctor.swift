//
//  User.swift
//  iCNM
//
//  Created by Medlatec on 5/11/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import ObjectMapper
import RealmSwift


class UserDoctor:Object,Mappable {
    dynamic var id = 0
    dynamic var userInfo:UserInfo?
    dynamic var doctor:Doctor?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            id <- map["User.UserID"]
        }
        userInfo <- map["User"]
        doctor <- map["Doctor"]
    }
    
}

