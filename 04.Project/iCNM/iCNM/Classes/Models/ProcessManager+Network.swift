//
//  ProcessManager+Network.swift
//  iCNM
//
//  Created by Quang Hung on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension ProcessManager {
    
    @discardableResult
    static func customRatingEmployee(scheduleID:Int, sumRate:Float, noiDung:String, success:@escaping(Process?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        var parameterDic:[String:Any]?
        parameterDic = [
            "ScheduleID":scheduleID,
            "DiemDanhGia":sumRate,
            "NoiDungDanhGia":noiDung
        ]
        
        return API.requestJSON(endpoint: .customRatingEmployee(parameterDic!), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let process = Process(JSON: item) {
                        success(process)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    
    @discardableResult
    func getAllProcessManager(scheduleID:Int, success:@escaping(ProcessManager?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
       
        return API.requestJSON(endpoint: .getAllProcessManager(scheduleID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let processManager = ProcessManager(JSON: item) {
                        success(processManager)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
