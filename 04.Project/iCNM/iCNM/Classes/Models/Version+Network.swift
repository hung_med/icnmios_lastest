//
//  Version+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension Version {
    @discardableResult
    func getVersionApp(deviceType:Int, success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getVersionApp(deviceType), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
