//
//  CheckPatient+Network.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension CheckPatient {

    @discardableResult
    func checkPatientResult(type:Int, organizeID:String, pid:String, sid:String, password:String, success:@escaping(CheckPatient?)->Void, fail:@escaping(_ error:Error, _ response:DataResponse<Any>)->Void)->Request {
        var stringParam: String = String()
        if (type == 0){
            stringParam = stringParam.appendingFormat("%@,%@,%@,%d",organizeID,pid,password,type)
        } else if type == 1{
            stringParam = stringParam.appendingFormat("%@,%@,%@,%d",organizeID,sid,password,type)
        }else if type == 2{
            stringParam = stringParam.appendingFormat("%@,%@,%@,%d",organizeID,pid,password,type)
        }
        
        return API.requestJSON(endpoint: .checkPatientResult(stringParam), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let checkPatient = CheckPatient(JSON: item) {
                        success(checkPatient)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
