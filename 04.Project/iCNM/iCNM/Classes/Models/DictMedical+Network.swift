//
//  DictMedical+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire

extension DictMedical {
    
    @discardableResult
    static func getAllDictMedical(success:@escaping([DictMedical]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getDictMedical, completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [DictMedical]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let medicalUtil = DictMedical(JSON: item) {
                            data.append(medicalUtil)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
