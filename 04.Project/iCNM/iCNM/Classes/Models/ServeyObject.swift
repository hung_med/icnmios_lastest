//
//  ServeyObject.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class ServeyObject:Mappable {
    var serveyStatus:String? = nil
    var listServey = [Servey]()
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            serveyStatus <- map["status"]
            listServey <- map["listServey"]
           
        }
    }
}
