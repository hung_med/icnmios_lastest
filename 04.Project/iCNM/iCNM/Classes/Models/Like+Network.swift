//
//  Like+Network.swift
//  iCNM
//
//  Created by Nguyen Van Dung on 8/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Alamofire

class Like: NSObject {

    static func getLikeStatus(locationID: Int, userID:Int, completionHandler: @escaping(Int?, Error?) -> ()) {
        
        let urlStr = Constant.apiBaseUrl + "api/locations/Status/\(locationID),\(userID)"
        let escapedUrl = urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        Alamofire.request(escapedUrl!, method: .get, parameters: nil,
                          encoding: URLEncoding.default).validate().responseJSON { response in
                            switch response.result {
                            case .success(let value):
                                
                                let status = value as! Int
                                completionHandler(status, nil)
                                
                            case .failure(let error):
                                print("GetLikeStatus Error : \(error.localizedDescription)")
                            }
        }
        
    }
    
    static func updateLikeStatus(locationID: Int, userID:Int, completionHandler: @escaping(Int?, Error?) -> ()) {
        
        let urlStr = Constant.apiBaseUrl + "api/locations/Status/\(locationID),\(userID)"
        let escapedUrl = urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        Alamofire.request(escapedUrl!, method: .post, parameters: nil,
                          encoding: URLEncoding.default).validate().responseJSON { response in
                            switch response.result {
                            case .success(let value):
                                
                                if let v = value as? String {
                                    let status = Int(v)
                                    completionHandler(status, nil)
                                }
                                
                            case .failure(let error):
                                print("UpdateLikeStatus Error : \(error.localizedDescription)")
                            }
        }
        
    }

    
}
