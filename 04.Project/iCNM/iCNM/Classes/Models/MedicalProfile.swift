//
//  MedicalProfile.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class MedicalProfile:Mappable {
    var active:Bool = false
    var address = ""
    var birthDay = ""
    var bloodGroupStr = ""
    var dateCreate = ""
    var dateEdit = ""
    var districtID:Int? = 0
    var email = ""
    var ethnic = ""
    var gender = ""
    var healthInsuranceID = ""
    var height = ""
    var idNumber = ""
    var idNumberDate = ""
    var idNumberPlaceID = ""
    var imgUrl = ""
    var job = ""
    var medicalHealthFactoryID:Int = 0
    var medicalProfileID:Int = 0
    var patientName = ""
    var mobilePhone = ""
    var organizeID:Int? = 0
    var provinceID:Int? = 0
    var pID = ""
    var weight:Int? = 0
    var height2:Int? = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
      //if map.mappingType == .fromJSON {
      //  let dateFormatter = DateFormatter()
      //  dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            active <- map["Active"]
            address <- map["Address"]
            birthDay <- map["BirthDay"]
        //birthDay <- (map["BirthDay"],DateFormatterTransform(dateFormatter:dateFormatter))
            bloodGroupStr <- map["BloodGroupStr"]
            dateCreate <- map["DateCreate"]
            dateEdit <- map["DateEdit"]
            districtID <- map["DistrictID"]
            email <- map["Email"]
            ethnic <- map["Ethnic"]
            gender <- map["Gender"]
            healthInsuranceID <- map["HealthInsuranceID"]
            height <- map["Height"]
            idNumber <- map["IDNumber"]
            idNumberDate <- map["IDNumberDate"]
            idNumberPlaceID <- map["IDNumberPlaceID"]
            imgUrl <- map["ImgUrl"]
            job <- map["Job"]
            medicalHealthFactoryID <- map["MedicalHealthFactoryID"]
            medicalProfileID <- map["MedicalProfileID"]
            patientName <- map["PatientName"]
            mobilePhone <- map["MobilePhone"]
            organizeID <- map["OrganizeID"]
            provinceID <- map["ProvinceID"]
            pID <- map["PID"]
            weight <- map["Weight"]
            height2 <- map["Height"] 
        }
 //   }
}
