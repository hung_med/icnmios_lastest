//
//  LoginDoctorUnit.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class LoginDoctorUnit:Mappable {
    var maBS:String = ""
    var tenBS:String = ""
    var userWeb:String = ""
    var passWeb:String = ""
  
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            maBS <- map["MaBS"]
            tenBS <- map["TenBS"]
            userWeb <- map["UserWeb"]
            passWeb <- map["PassWeb"]
        }
    }
}
