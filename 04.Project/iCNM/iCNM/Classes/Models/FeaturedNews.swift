//
//  FeaturedNews.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class FeaturedNews:Mappable {
    var title = ""
    var dateCreate:Date?
    var datePhan:Date?
    var categoryName = ""
    var img = ""
    var newsContent = ""
    var categoryID:Int = 0
    var countLike:Int = 0
    var countRead:Int = 0
    var countShare:Int = 0
    var description = ""
    var newsModel = NewsModel()
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormatter2.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone;
       // let utcTimeZoneStr = formatter.stringFromDate(date);
        
        if map.mappingType == .fromJSON {
            categoryID <- map["Category.CategoryID"]
            categoryName <- map["Category.CategoryName"]
            newsModel <- map["News"]
            
            img <- map["News.Img"]
            dateCreate <- (map["DateCreate"], DateFormatterTransform(dateFormatter:dateFormatter2))
            countLike <- map["News.CountLike"]
            countRead <- map["News.CountRead"]
            countShare <- map["News.CountShare"]
            title <- map["News.Title"]
            newsContent <- map["News.NewsContent"]
            description <- map["News.Description"]
            datePhan <- map["News.DatePhan"]
        }
    }
}
