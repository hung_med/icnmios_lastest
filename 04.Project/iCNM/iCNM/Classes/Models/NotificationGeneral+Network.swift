//
//  NotificationGeneral+Network.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension NotificationGeneral {
    
    @discardableResult
    func getAllNotificationGeneral(success:@escaping([NotificationGeneral]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getAllNotificationGeneral, completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [NotificationGeneral]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let notificationGeneral = NotificationGeneral(JSON: item) {
                            data.append(notificationGeneral)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
