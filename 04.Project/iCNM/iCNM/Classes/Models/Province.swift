//
//  Province.swift
//  iCNM
//
//  Created by Medlatec on 5/11/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

class Province:Object,Mappable {
    dynamic var id:String?
    dynamic var name:String?
    dynamic var shortName:String?
    dynamic var active = false
    dynamic var rowGUID:String?
    dynamic var createdDate:Date?
    var districts = List<District>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        id <- map["TinhThanh_ID"]
        name <- map["TinhThanh"]
        shortName <- map["TenVietTat"]
        active <- map["KichHoat"]
        rowGUID <- map["rowguid"]
        createdDate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
    }
    
}
