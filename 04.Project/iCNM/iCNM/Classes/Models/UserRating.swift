//
//  UserRating.swift
//  iCNM
//
//  Created by Nguyen Van Dung on 8/5/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import ObjectMapper

class UserRating: NSObject {
    
    var user:UserInfo!
    var rating: Rating!
    
}
