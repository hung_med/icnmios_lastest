//
//  MedicalUnit.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class MedicalUnit:Mappable {
    var organizeID = 0
    var organizeCode = ""
    var organizeName = ""
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            organizeID <- map["OrganizeID"]
            organizeCode <- map["OrganizeCode"]
            organizeName <- map["OrganizeName"]
        }        
    }
}
