//
//  Schedule+Network.swift
//  iCNM
//
//  Created by Medlatec on 8/3/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire

extension Schedule {
    @discardableResult static func bookAnAppointment(parameter:[String:Any], success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .BookAnAppointment(parameter), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult static func bookAnAppointmentDoctor(parameter:[String:Any], success:@escaping(GeneralString?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .BookAnAppointmentDoctor(parameter), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let generalString = GeneralString(JSON: item) {
                        success(generalString)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult static func getSchedule(stringRequest:String, success:@escaping([Schedule])->Void, fail:@escaping(_ error:Error, _ response:DataResponse<Any>)->Void)->Request {
        print(stringRequest)
        return API.requestJSON(endpoint: .GetSchedule(stringRequest), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [Schedule]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let schedule = Schedule(JSON: item) {
                                data.append(schedule)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult static func deleteSchedule(scheduleID:Int, success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .DeleteSchedule(scheduleID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
