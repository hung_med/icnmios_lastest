//
//  ServiceDetailGroups+Network.swift
//  iCNM
//
//  Created by Quang Hung on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension ServiceDetailGroups {
    @discardableResult
    
    func getAllServiceDetailGroups(serviceID:Int, success:@escaping(ServiceDetailGroups?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getAllServiceDetailGroups(serviceID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let serviceDetailGroups = ServiceDetailGroups(JSON: item) {
                        success(serviceDetailGroups)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
