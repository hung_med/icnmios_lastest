//
//  Process.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class Process:Mappable {
    var processID:Int = 0
    var maLichHen:Int = 0
    var scheduleID:Int = 0
    var hoVaTen:String? = nil
    var diaChi:String? = nil
    var ngayTao:Date? = nil
    var ngayDatLich:Date? = nil
    var khungGio:String? = nil
    var userXacNhan:String? = nil
    var tGXacNhan:Date? = nil
    var ngayXacNhan:Date? = nil
    var khungGioXacNhan:String? = nil
    var userPhanLich:String? = nil
    var tGPhanLich:Date? = nil
    var userNhanLich:String? = nil
    var daNhanLich:Bool? = false
    var tgNhanLich:Date? = nil
    var tGDenDiaChi:Date? = nil
    var thucHienDichVu:Bool? = false
    var sID:String? = nil
    var tGVaoSo:Date? = nil
    var tGHenTraKQ:Date? = nil
    var diemDanhGia:Float = 0.0
    var noiDungDanhGia:String? = nil
    var tGBanGiaoMau:Date? = nil
    var userBanGiaoMau:String? = nil
    var tGNhanMau:Date? = nil
    var userNhanMau:String? = nil
    var tGChayXN:Date? = nil
    var fullResult:Bool? = false
    var fullResultTime:Date? = nil
    var valid:Bool? = false
    var validTime:Date? = nil
    var userValid:String? = nil
    var sendNotificationKQ:Bool? = false
    var tuVan:Bool? = false
    var tGTuVan:Date? = nil
    var userTuVan:String? = nil
    var tGHenTaiKham:Date? = nil
    var traKQ:Bool? = false
    var sendNotificationKhaoSat:Bool? = false
    var diemKhaoSat:Float = 0.0
    var kQKhaoSat:String? = nil
    var tGThucKhaoSat:Date? = nil
    var sendNotificationTaiKham:Bool? = false
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        if map.mappingType == .fromJSON {
            processID <- map["ProcessID"]
            maLichHen <- map["MaLichHen"]
            scheduleID <- map["ScheduleID"]
            hoVaTen <- map["HoVaTen"]
            diaChi <- map["DiaChi"]
            ngayTao <- (map["NgayTao"],DateFormatterTransform(dateFormatter:dateFormatter))
            ngayDatLich <- (map["NgayDatLich"],DateFormatterTransform(dateFormatter:dateFormatter2))
            khungGio <- map["KhungGio"]
            userXacNhan <- map["userXacNhan"]
            tGXacNhan <- (map["TGXacNhan"],DateFormatterTransform(dateFormatter:dateFormatter))
            ngayXacNhan <- (map["NgayXacNhan"],DateFormatterTransform(dateFormatter:dateFormatter2))
            khungGioXacNhan <- map["KhungGioXacNhan"]
            userPhanLich <- map["UserPhanLich"]
            tGPhanLich <- (map["TGPhanLich"],DateFormatterTransform(dateFormatter:dateFormatter))
            userNhanLich <- map["UserNhanLich"]
            daNhanLich <- map["DaNhanLich"]
            tgNhanLich <- (map["TGNhanLich"],DateFormatterTransform(dateFormatter:dateFormatter))
            tGDenDiaChi <- (map["TGDenDiaChi"],DateFormatterTransform(dateFormatter:dateFormatter))
            thucHienDichVu <- map["ThucHienDichVu"]
            sID <- map["SID"]
            tGVaoSo <- map["TGVaoSo"]
            tGHenTraKQ <- map["TGHenTraKQ"]
            diemDanhGia <- map["DiemDanhGia"]
            noiDungDanhGia <- map["NoiDungDanhGia"]
            tGBanGiaoMau <- (map["TGBanGiaoMau"],DateFormatterTransform(dateFormatter:dateFormatter))
            userBanGiaoMau <- map["UserBanGiaoMau"]
            tGNhanMau <- (map["TGNhanMau"],DateFormatterTransform(dateFormatter:dateFormatter))
            userNhanMau <- map["UserNhanMau"]
            tGChayXN <- (map["TGChayXN"],DateFormatterTransform(dateFormatter:dateFormatter))
            fullResult <- map["FullResult"]
            fullResultTime <- (map["FullResultTime"],DateFormatterTransform(dateFormatter:dateFormatter))
            valid <- map["Valid"]
            validTime <- (map["ValidTime"],DateFormatterTransform(dateFormatter:dateFormatter))
            userValid <- map["UserValid"]
            sendNotificationKQ <- map["SendNotificationKQ"]
            tuVan <- map["TuVan"]
            tGTuVan <- (map["TGTuVan"],DateFormatterTransform(dateFormatter:dateFormatter))
            userTuVan <- map["UserTuVan"]
            tGHenTaiKham <- (map["TGHenTaiKham"],DateFormatterTransform(dateFormatter:dateFormatter2))
            traKQ <- map["TraKQ"]
            sendNotificationKhaoSat <- map["SendNotificationKhaoSat"]
            diemKhaoSat <- map["DiemKhaoSat"]
            kQKhaoSat <- map["KQKhaoSat"]
            tGThucKhaoSat <- (map["TGThucKhaoSat"],DateFormatterTransform(dateFormatter:dateFormatter))
            sendNotificationTaiKham <- map["SendNotificationTaiKham"]
        }
    }
}
