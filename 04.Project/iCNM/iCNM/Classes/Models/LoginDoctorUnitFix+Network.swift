//
//  LoginDoctorUnit+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension LoginDoctorUnit {
    @discardableResult
    func getDoctorIDUnit(userWeb:String, passWeb:String, organizeID:Int, success:@escaping(LoginDoctorUnit?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        let parameterDic:[String:Any] = [
            "UserWeb": userWeb,
            "PassWeb": passWeb,
            "OrganizeID": organizeID
        ]
        
        return API.requestJSON(endpoint: .getDoctorIDUnit(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let loginDoctorUnit = LoginDoctorUnit(JSON: item) {
                        success(loginDoctorUnit)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
