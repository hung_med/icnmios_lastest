//
//  MedicalUnit+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension MedicalUnit {
    
    @discardableResult
    static func getAllMedicalUnit(success:@escaping([MedicalUnit]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getMedicalUnit, completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [MedicalUnit]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let medicalUtil = MedicalUnit(JSON: item) {
                            data.append(medicalUtil)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
