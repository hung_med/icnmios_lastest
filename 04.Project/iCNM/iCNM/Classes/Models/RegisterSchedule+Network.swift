//
//  RegisterSchedule+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension RegisterSchedule {
    @discardableResult
    func sendRegisterSchedule(idGoi:Int, maGoi:String, tenGK:String, name:String, phone:String, address:String, maNV:String, email:String, confirm:Bool, noikham:String, ngaykham:String, giohen1:String, giohen2:String, personNumService:Int, timeBefore:Int, success:@escaping(GeneralString?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        let parameterDic:[String:Any] = [
            "IDGoi": idGoi,
            "MaGoi": maGoi,
            "TenGoi": tenGK,
            "MaNV": maNV,
            "NguoiMua": name,
            "Tel": phone,
            "DiaChi": address,
            "Email": email,
            "IsTN": confirm,
            "NoiKham": noikham,
            "NgayKham": ngaykham,
            "GioHen1": giohen1,
            "GioHen2": giohen2,
            "PersonNumService": personNumService,
            "TimeBefore": timeBefore
        ]
        
        return API.requestJSON(endpoint: .sendRegisterSchedule(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let generalString = GeneralString(JSON: item) {
                        success(generalString)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func sendRegisterSchedulePK(idGoi:Int, maGoi:String, tenGK:String, name:String, nguonban:String, phone:String, address:String, maNV:String, email:String, confirm:Bool, noikham:String, ngaykham:String, giohen1:String, giohen2:String, success:@escaping(GeneralID?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        let parameterDic:[String:Any] = [
            "IDGoi": idGoi,
            "MaGoi": maGoi,
            "TenGoi": tenGK,
            "MaNV": maNV,
            "NguoiMua": name,
            "NguonBan": nguonban,
            "Tel": phone,
            "DiaChi": address,
            "Email": email,
            "IsTN": confirm,
            "NoiKham": noikham,
            "NgayKham": ngaykham,
            "GioHen1": giohen1,
            "GioHen2": giohen2,
            
        ]
        
        return API.requestJSON(endpoint: .sendRegisterSchedulePK(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let generalID = GeneralID(JSON: item) {
                        success(generalID)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func sendNotificationSuccessService(serviceID:Int, userID:Int, magenGK:String, success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .sendNotificationSuccessService(serviceID, userID, magenGK), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    
    @discardableResult
    func postVNPayment(vnp_Amount:Int, vnp_BankCode:String, vnp_Command:String, vnp_CreateDate:String, vnp_CurrCode:String, vnp_IpAddr:String, vnp_Locale:String, vnp_Merchant:String, vnp_OrderInfo:String, vnp_OrderType:String, vnp_ReturnUrl:String, vnp_TmnCode:String, vnp_TxnRef:String, vnp_Version:Int, vnp_SecureHashType:String, vnp_SecureHash:String, success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .postVNPayment(vnp_Amount, vnp_BankCode, vnp_Command, vnp_CreateDate, vnp_CurrCode, vnp_IpAddr, vnp_Locale, vnp_Merchant, vnp_OrderInfo, vnp_OrderType, vnp_ReturnUrl, vnp_TmnCode, vnp_TxnRef, vnp_Version, vnp_SecureHashType, vnp_SecureHash), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
