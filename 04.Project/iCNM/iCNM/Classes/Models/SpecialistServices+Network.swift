//
//  SpecialistServices+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire

extension SpecialistServices {
    
    @discardableResult
    static func getSpecialistServices(success:@escaping([SpecialistServices]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getSpecialistServices, completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [SpecialistServices]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let specialistServices = SpecialistServices(JSON: item) {
                            data.append(specialistServices)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
