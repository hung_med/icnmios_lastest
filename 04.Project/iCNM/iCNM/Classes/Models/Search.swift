//
//  Search.swift
//  iCNM
//
//  Created by Hoang Van Trung on 8/1/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import ObjectMapper

class Search: Mappable {
    var dictionaries = [Dictionaries]()
    var symptons = [Symptons]()
    var meanning = [Meanning]()
    var newsCategories = [NewsCategories]()
    var questions = [Question]()
    var usersDoctors = [FeaturedDoctor]()
    var locations = [LocationNearBy]()
    var users = [Users]()
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        dictionaries <- map["Dictionaries"]
        symptons <- map["Symptons"]
        meanning <- map["Meanning"]
        newsCategories <- map["NewsCategories"]
        questions <- map["Questions"]
        usersDoctors <- map["UsersDoctors"]
        locations <- map["Locations"]
        users <- map["Users"]
    }
}

class Dictionaries: Mappable {
    var medicalDicID: Int = 0
    var diseaseGroupID: Int = 0
    var medicalSymptonIDStr: Float = 0.0
    var name: String = ""
    var mean: String = ""
    var overview: String = ""
    var reason: String = ""
    var symptom: String = ""
    var classify: String =  ""
    var prevention: String = ""
    var descriptionPictures: String = ""
    var pictures: String = ""
    var keySearchName: String = ""
    var dateCreate: String = ""
    var active: Bool = false
    var viewCount: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        medicalDicID <- map["MedicalDicID"]
        diseaseGroupID <- map["DiseaseGroupID"]
        medicalSymptonIDStr <- map["MedicalSymptonIDStr"]
        name <- map["Name"]
        mean <- map["Mean"]
        overview <- map["Overview"]
        reason <- map["Reason"]
        symptom <- map["Symptom"]
        classify <- map["Classify"]
        prevention <- map["Prevention"]
        descriptionPictures <- map["DescriptionPictures"]
        pictures <- map["Pictures"]
        keySearchName <- map["KeySearchName"]
        dateCreate <- map["DateCreate"]
        active <- map["Active"]
        viewCount <- map["ViewCount"]
    }
}

class Symptons: Mappable {
    
    var medicalSymptonID: Int = 0
    var name: String = ""
    var mean: String = ""
    var reason: String = ""
    var diagnosis: String = ""
    var keySearchName: String = ""
    var viewCount: Int = 0
    var imgStrs: String = ""
    var active: Bool = false
    var dateCreate: Date?
    var locationBodyIDStr: String = ""
    var descriptionPictures: String = ""
    var pictures: String = ""
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        medicalSymptonID <- map["MedicalSymptonID"]
        name <- map["Name"]
        mean <- map["Mean"]
        reason <- map["Reason"]
        diagnosis <- map["Diagnosis"]
        keySearchName <- map["KeySearchName"]
        viewCount <- map["ViewCount"]
        imgStrs <- map["ImgStrs"]
        active <- map["Active"]
        dateCreate <- map["DateCreate"]
        locationBodyIDStr <- map["LocationBodyIDStr"]
        descriptionPictures <- map["DescriptionPictures"]
        pictures <- map["Pictures"]
    }
}

class Meanning: Mappable {
    var id = 0
    var testCode: String = ""
    var testName:String = ""
    var type: String = ""
    var normalRange: String = ""
    var meaning1: String = ""
    var time: String = ""
    var price: String = ""
    var category: String = ""
    var cateNam: String = ""
    var active: String = ""
    var meaning2: String = ""
    var indexSearch: Int = 0
    var keySearch: String = ""
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            id <- map["ID"]
            testCode <- map["TestCode"]
            testName <- map["TestName"]
            type <- map["Type"]
            normalRange <- map["NormalRange"]
            meaning1 <- map["Meaning1"]
            time <- map["Time"]
            price <- map["Price"]
            category <- map["Category"]
            cateNam <- map["CateNam"]
            active <- map["Active"]
            meaning2 <- map["Meaning2"]
            indexSearch <- map["IndexSearch"]
            keySearch <- map["KeySearchTestName"]
        }
        
    }
}

class NewsCategories: Mappable {
    var news = News()
    var category = Category()
    var dateCreate: Date?
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        news <- map["News"]
        category <- map["Category"]
        dateCreate <- map["DateCreate"]
    }
}

class Questions: Mappable {
    var questionID: Int = 0
    var questionTitle: String = ""
    var questionContent: String = ""
    var SpecialistID: Int = 0
    var userID: Int = 0
    var fullName: String = ""
    var phone: String = ""
    var email: String = ""
    var address: String = ""
    var reader: Int = 0
    var countRead: Int = 0
    var countAnswer: Int = 0
    var active: Bool = false
    var doctorID: Int = 0
    var exprireTine: String = ""
    var imgStr: String = ""
    var imgThumbnailStr: String = ""
    var dateCreate: String = ""
    var countLike: Int = 0
    var countComment: Int = 0
    var countShare: Int = 0
    var keySearchTitle: String = ""

    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            questionID <- map["QuestionID"]
            questionTitle <- map["QuestionTitle"]
            questionContent <- map["QuestionContent"]
            SpecialistID <- map["SpecialistID"]
            userID <- map["UserID"]
            fullName <- map["FullName"]
            phone <- map["Phone"]
            email <- map["Email"]
            address <- map["Address"]
            reader <- map["Reader"]
            countRead <- map["CountRead"]
            countAnswer <- map["CountAnswer"]
            active <- map["Active"]
            doctorID <- map["DoctorID"]
            exprireTine <- map["ExprireTime"]
            imgStr <- map["ImgStr"]
            imgThumbnailStr <- map["ImgThumbnailStr"]
            dateCreate <- map["DateCreate"]
            countLike <- map["CountLike"]
            countComment <- map["CountComment"]
            countShare <- map["CountShare"]
            keySearchTitle <- map["KeySearchTitle"]
        }
    }
}

class UsersDoctors: Mappable {
    var user:Users?
    var doctor:Doctor?
    var specialists: String = ""
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        user <- map["User"]
        doctor <- map["Doctor"]
        specialists <- map["Specialists"]
    }
}

class Locations: Mappable {
    var locationID: Int = 0
    var latitude: Float = 0.0
    var longitude: Float = 0.0
    var address: String = ""
    var mobile: Double = 0
    var service: String = ""
    var locationTimeID: Date?
    var workingTimeStr: String = ""
    var imagesStr: String = ""
    var imagesThumbnailStr: String = ""
    var workingTime: String = ""
    var cName: String = ""
    var cType: Int = 0
    var dayStr: String = ""
    var locationTypeID: Int = 0
    var district: String = ""
    var city: String = ""
    var specialist: String = ""
    var name: String = ""
    var averageRating: Float = 0.0
    var intro: String = ""
    var tinhThanhID: String = ""
    var quanHuyenID: String = ""
    var geog: Float = 0.0
    var keySearchName: String = ""
    var userUpdate: Date?
    var dateUpdate: Date?
    var active: Bool = false
    var countLike: Int = 0
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            locationID <- map["LocationID"]
            latitude <- map["Lat"]
            longitude <- map["Long"]
            address <- map["Address"]
            mobile <- map["Mobile"]
            service <- map["Service"]
            locationTimeID <- map["LocationTimeID"]
            workingTimeStr <- map["WorkingTimeStr"]
            imagesStr <- map["ImagesStr"]
            imagesThumbnailStr <- map["ImagesThumbnailStr"]
            workingTime <- map["WorkingTime"]
            cName <- map["CName"]
            cType <- map["CType"]
            dayStr <- map["DayStr"]
            locationTypeID <- map["LocationTypeID"]
            district <- map["District"]
            city <- map["City"]
            specialist <- map["Specialist"]
            name <- map["Name"]
            averageRating <- map["AverageRating"]
            intro <- map["Intro"]
            tinhThanhID <- map["TinhThanhID"]
            quanHuyenID <- map["QuanHuyenID"]
            geog <- map["Geog"]
            keySearchName <- map["KeySearchName"]
            userUpdate <- map["UserUpdate"]
            dateUpdate <- map["DateUpdate"]
            active <- map["Active"]
            countLike <- map["CountLike"]
        }
    }
}

class Users: Mappable {
    var userID: Int = 0
    var doctorID: Int = 0
    var userTypeID: Int = 0
    var name: String = ""
    var email:String = ""
    var phone: String = ""
    var password: String = ""
    var avatar: String = ""
    var gender: String = ""
    var birthDay: String = ""
    var address: String = ""
    var dateCreate: String = ""
    var dateEdit: String = ""
    var verifyEmail: Bool = false
    var verifyPhone: Bool = false
    var active: Bool = false
    var districtID: String = ""
    var provinceID: String = ""
    var salt: String = ""
    var lastLogin: String = ""
    var lastLogout: String = ""
    var lastUpdate: String = ""
    var education: String = ""
    var aboutMe: String = ""
    var job: String = ""
    var workPlace: String = ""
    var keySearchName: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            userID <- map["UserID"]
            doctorID <- map["DoctorID"]
            userTypeID <- map["UserTypeID"]
            name <- map["Name"]
            email <- map["Email"]
            phone <- map["Phone"]
            password <- map["Password"]
            avatar <- map["Avatar"]
            gender <- map["Gender"]
            birthDay <- map["BirthDay"]
            address <- map["Address"]
            dateCreate <- map["DateCreate"]
            dateEdit <- map["DateEdit"]
            verifyEmail <- map["VerifyEmail"]
            verifyPhone <- map["VerifyPhone"]
            active <- map["Active"]
            districtID <- map["DistrictID"]
            provinceID <- map["ProvinceID"]
            salt <- map["Salt"]
            lastLogin <- map["LastLogin"]
            lastLogout <- map["LastLogout"]
            lastUpdate <- map["LastUpdate"]
            education <- map["Education"]
            aboutMe <- map["AboutMe"]
            job <- map["Job"]
            workPlace <- map["WorkPlace"]
            keySearchName <- map["KeySearchName"]
        }
    }
}

class Category: Mappable {
    var categoryID:Int = 0
    var categoryName: String = ""
    var dateCreate: String = ""
    var img: String = ""
    var orderNum: Int = 0
    var active: Bool = false
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        categoryID <- map["CategoryID"]
        categoryName <- map["CategoryName"]
        dateCreate <- map["DateCreate"]
        img <- map["Img"]
        orderNum <- map["OrderNum"]
        active <- map["Active"]
    }
}


