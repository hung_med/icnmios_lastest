//
//  DictMedical.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class DictMedical:Mappable {
    var ID = 0
    var keySearchTestName = ""
    var meaning1 = ""
    var meaning2 = ""
    var testName = ""
    var normalRange = ""
    var time = ""
    var price:Float = 0.0
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            ID <- map["ID"]
            keySearchTestName <- map["KeySearchTestName"]
            meaning1 <- map["Meaning1"]
            meaning2 <- map["Meaning2"]
            testName <- map["TestName"]
            normalRange <- map["NormalRange"]
            time <- map["Time"]
            price <- map["Price"]
        }        
    }
}
