//
//  NewsModel.swift
//  iCNM
//
//  Created by Len Pham on 8/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import ObjectMapper

class NewsModel: Mappable {
    
    var newsID = 0
    var categoryID = 0
    var title = ""
    var datePhan = ""
    var description = ""
    var img = ""
    var newsContent = ""
    var dateCreate = ""
    var number = 0
    var linkfb = ""
    var checkNoti = ""
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            newsID <- map["NewsID"]
            categoryID <- map["CategoryID"]
            title <- map["Title"]
            description <- map["Description"]
            img <- map["Img"]
            dateCreate <- map["DateCreate"]
            datePhan <- map["DatePhan"]
            newsContent <- map["NewsContent"]
            number <- map["NUMBER"]
            linkfb <- map["LinkFB"]
            checkNoti <- map["CheckNoti"]
        }
    }

}
