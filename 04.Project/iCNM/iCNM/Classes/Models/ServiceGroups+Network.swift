//
//  ServiceGroups+Network.swift
//  iCNM
//
//  Created by Quang Hung on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension ServiceGroups {
    @discardableResult
    func getAllServiceGroups(success:@escaping([ServiceGroups]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getAllServiceGroups, completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [ServiceGroups]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let serviceGroups = ServiceGroups(JSON: item) {
                            data.append(serviceGroups)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
