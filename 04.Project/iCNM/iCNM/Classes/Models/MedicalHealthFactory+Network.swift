//
//  MedicalHealthFactory+Network.swift
//  iCNM
//
//  Created by Ta Quang Hung on 20/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import Alamofire

extension MedicalHealthFactory {
    
    @discardableResult
    func getMedicalHealthFactory(factoryID:Int, success:@escaping(MedicalHealthFactory?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getMedicalHealthFactory(factoryID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let medicalHealthFact = MedicalHealthFactory(JSON: item) {
                        success(medicalHealthFact)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
