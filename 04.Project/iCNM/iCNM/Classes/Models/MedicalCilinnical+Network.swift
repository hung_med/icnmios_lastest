//
//  MedicalCilinnical+Network.swift
//  iCNM
//
//  Created by Ta Quang Hung on 20/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import Alamofire

extension MedicalCilinnical {
    
    //post data Medical Cilinnical
    @discardableResult
    func addEditMedicalCilinnical(medicalCilinnical:MedicalCilinnical?, medicalProfileID:Int, success:@escaping(MedicalCilinnical?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        var parameterDic = [String: Any]()
        let dicData = [
            "CilinnicID":medicalCilinnical?.cilinnicID as Any,
            "DateCilin":medicalCilinnical?.dateCilin as Any,
            "DateSchedule":medicalCilinnical?.dateSchedule as Any,
            "HistoryCilin":medicalCilinnical?.historyCilin as Any,
            "Mach":medicalCilinnical?.mach as Any,
            "NhietDo": medicalCilinnical?.nhietDo as Any,
            "HA": medicalCilinnical?.ha as Any,
            "NhipTho": medicalCilinnical?.nhipTho as Any,
            "Weight": medicalCilinnical?.weight as Any,
            "Height": medicalCilinnical?.height as Any,
            "BMI": medicalCilinnical?.bMI as Any,
            "VongBung": medicalCilinnical?.vongBung as Any,
            "LeftEye": medicalCilinnical?.leftEye as Any,
            "RightEye": medicalCilinnical?.rightEye as Any,
            "LeftEyeGlass": medicalCilinnical?.leftEyeGlass as Any,
            "RightEyeGlass": medicalCilinnical?.rightEyeGlass as Any,
            "DaNiemMac": medicalCilinnical?.daNiemMac as Any,
            "ToanThanNote": medicalCilinnical?.toanThanNote as Any,
            "TimMach": medicalCilinnical?.timMach as Any,
            "HoHap": medicalCilinnical?.hoHap as Any,
            "TieuHoa": medicalCilinnical?.tieuHoa as Any,
            "TietNieu": medicalCilinnical?.tietNieu as Any,
            "CoXuongKhop": medicalCilinnical?.coXuongKhop as Any,
            "NoiTiet": medicalCilinnical?.noiTiet as Any,
            "ThanKinh": medicalCilinnical?.thanKinh as Any,
            "TamThan": medicalCilinnical?.tamThan as Any,
            "NgoaiKhoa": medicalCilinnical?.ngoaiKhoa as Any,
            "SanPhuKhoa": medicalCilinnical?.sanPhuKhoa as Any,
            "TaiMuiHong": medicalCilinnical?.taiMuiHong as Any,
            "RangHamMat": medicalCilinnical?.rangHamMat as Any,
            "Mat": medicalCilinnical?.mat as Any,
            "DaLieu": medicalCilinnical?.daLieu as Any,
            "DinhDuong": medicalCilinnical?.dinhDuong as Any,
            "VanDong": medicalCilinnical?.vanDong as Any,
            "DanhGia": medicalCilinnical?.danhGia as Any,
            "CoQuanKhac": medicalCilinnical?.coQuanKhac as Any,
            "HuyetHoc": medicalCilinnical?.huyetHoc as Any,
            "SinhHoaMau": medicalCilinnical?.sinhHoaMau as Any,
            "SinhHoaNuocTieu": medicalCilinnical?.sinhHoaNuocTieu as Any,
            "SieuAmOBung": medicalCilinnical?.sieuAmOBung as Any,
            "ChanDoan": medicalCilinnical?.chanDoan as Any,
            "TuVan": medicalCilinnical?.tuVan as Any,
            "BacSiName": medicalCilinnical?.bacSiName as Any,
            "Active":medicalCilinnical?.active as Any]

        parameterDic = ["MedicalCilinnical": dicData,
                        "MedicalProfileID": medicalProfileID]
        
        return API.requestJSON(endpoint: .addEditMedicalCilinnical(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let medicalCilinnical = MedicalCilinnical(JSON: item) {
                        success(medicalCilinnical)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func getCilinicalProfile(medicalProfileID:Int, success:@escaping([MedicalCilinnical]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getCilinical(medicalProfileID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [MedicalCilinnical]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let medicalCilinnical = MedicalCilinnical(JSON: item) {
                            data.append(medicalCilinnical)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
