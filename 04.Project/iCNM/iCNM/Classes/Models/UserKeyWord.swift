//
//  UserKeyWord.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class UserKeyWord:Mappable {
    var userID:Int = 0
    var tagID:Int = 0
    var active:Bool? = false
    var dateCreate:Date?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        userID <- map["UserID"]
        tagID <- map["TagID"]
        active <- map["Active"]
        dateCreate <- (map["DateCreate"], DateFormatterTransform(dateFormatter:dateFormatter))
        if dateCreate == nil {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateCreate <- (map["DateCreate"], DateFormatterTransform(dateFormatter:dateFormatter))
        }
    }
}
