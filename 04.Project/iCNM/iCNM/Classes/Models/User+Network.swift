//
//  User+Network.swift
//  iCNM
//
//  Created by Medlatec on 5/11/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension User {
    @discardableResult
    static func doLogin(phoneNumber:String, password:String, success:@escaping(String) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<String>) -> Void) -> Request {
        let parameterDic = [
            "phone":phoneNumber,
            "password":password
        ]
        return API.requestString(endpoint: .Login(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value)
            case .failure(let error):
                fail(error,response)
            }
        })
//        return API.requestObjectNoRealm(type: UserDoctor.self, endpoint: .Login(parameterDic), success: { (userDoctor) in
//            success(userDoctor)
//        }, fail: { (error, response) in
//            fail(error,response)
//        })
    }
    
    @discardableResult
    static func doLoginDoctorUnit(userWeb:String, passWeb:String, success:@escaping(String) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<String>)->Void) -> Request {
        let parameterDic = [
            "UserWeb": userWeb,
            "PassWeb": passWeb
        ]
        return API.requestString(endpoint: .doLoginDoctorUnit(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value)
            case .failure(let error):
                fail(error, response)
            }
        })
    }
    
    @discardableResult
    static func doLoginSocial(parameters:[String:Any], success:@escaping(UserDoctor) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<UserDoctor>) -> Void) -> Request {
        return API.requestObjectNoRealm(type: UserDoctor.self, endpoint: .GetAllUserByEmail(parameters), success: { (userDoctor) in
            success(userDoctor)
        }, fail: { (error, response) in
            fail(error,response)
        })
    }
    
    @discardableResult
    static func getUserByPhone(phone:String, success:@escaping(String) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<String>) -> Void) -> Request {
        return API.requestString(endpoint: .GetUserByPhone(phone), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func requestDoctorInformation(success:@escaping(DoctorInformation) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<DoctorInformation>) -> Void) -> Request {
        return API.requestObjectNoRealm(type: DoctorInformation.self, endpoint: .GetDoctorInformation(self.doctorID), success: { (doctorInformation) in
            doctorInformation.id = self.doctorID
            let realm = try! Realm()
            try! realm.write {
                self.doctorInformation = realm.create(DoctorInformation.self, value: doctorInformation, update: true)
            }
            success(doctorInformation)
        }, fail: { (error, response) in
            fail(error,response)
        })
        
    }
    
    @discardableResult
    func requestUserInformation(success:@escaping(UserInfo) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<UserInfo>) -> Void) -> Request {
        return API.requestObjectNoRealm(type: UserInfo.self, endpoint: .GetUserById(self.id), success: { (userInfo) in
            let realm = try! Realm()
            try! realm.write {
                self.userDoctor?.userInfo = realm.create(UserInfo.self, value: userInfo, update: true)
                self.doctorID = userInfo.doctorID
            }
                
            success(userInfo)
        }, fail: { (error, response) in
            fail(error,response)
        })
    }
    
    @discardableResult
    func updateUser(success:@escaping()->Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>) -> Void) -> Request {
        return API.requestJSON(endpoint: .UpdateUserDoctor(self.toJSON()), completionHandler: { (response) in
            switch response.result {
            case .success:
                success()
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func updateAvatar(parameterDic:[String:Any],success:@escaping(String)->Void, fail:@escaping (_ error:Error, _ response:DataResponse<String>) -> Void) -> Request {
        return API.requestString(endpoint: .UpdateAvatar(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
//    @discardableResult
//    func updatePassword(userID:Int, password:String, success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
//        let paramDic:[String:Any] = [
//            "UserID": userID,
//            "Password": password
//        ]
//        return API.requestJSON(endpoint: .updatePassword(paramDic), completionHandler: { (response) in
//            switch response.result {
//            case .success(let value):
//                success(value as? String)
//            case .failure(let error):
//                fail(error,response)
//            }
//        })
//    }
    
    @discardableResult
    func updatePassword(userID:Int, password:String, success:@escaping(UserInfo?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        let paramDic:[String:Any] = [
            "UserID": userID,
            "Password": password
        ]
        return API.requestJSON(endpoint: .updatePassword(paramDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let userInfo = UserInfo(JSON: item) {
                        success(userInfo)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func checkPassword(userID:Int, oldPassword:String, success:@escaping(UserInfo?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .checkPassword(userID, oldPassword), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let userInfo = UserInfo(JSON: item) {
                        success(userInfo)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}

