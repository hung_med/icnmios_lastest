//
//  Specialist+Network.swift
//  iCNM
//
//  Created by Medlatec on 5/11/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

extension Specialist {
    @discardableResult
    static func getAllSpecialist(success:@escaping([Specialist]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>) -> Void) -> Request {
        return API.requestJSON(endpoint: .GetAllSpecialist, completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [Specialist]()
                if let array = value as? [[String:Any]] {
                    let realm = try! Realm()
                    for item in array {
                        if let specialist = Specialist(JSON: item) {
                            try! realm.write {
                                data.append(realm.create(Specialist.self, value: specialist, update: true))
                            }
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
