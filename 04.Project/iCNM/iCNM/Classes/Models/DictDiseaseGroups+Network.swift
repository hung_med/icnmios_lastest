//
//  DictDiseaseGroups+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire

extension DictDiseaseGroups {
    
    @discardableResult
    static func getDictDiseaseGroups(success:@escaping([DictDiseaseGroups]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getDictDiseaseGroups, completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [DictDiseaseGroups]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let diseaseGroups = DictDiseaseGroups(JSON: item) {
                            data.append(diseaseGroups)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
