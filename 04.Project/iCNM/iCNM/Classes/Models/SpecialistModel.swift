//
//  SpecialistModel.swift
//  iCNM
//
//  Created by Medlatec on 5/29/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

class SpecialistModel:NSObject,Selectable {
    var specialist:Specialist!
    var title: String
    var isSelected: Bool = false
    var isUserSelectEnable: Bool = true
    
    init(title:String,isSelected:Bool,isUserSelectEnable:Bool, specialist:Specialist) {
        self.title = title
        self.isSelected = isSelected
        self.isUserSelectEnable = isUserSelectEnable
        self.specialist = specialist
    }
}
