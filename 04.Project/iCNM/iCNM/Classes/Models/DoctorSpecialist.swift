//
//  DoctorSpecialist.swift
//  iCNM
//
//  Created by Medlatec on 5/16/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//


import ObjectMapper
import RealmSwift

class DoctorSpecialist:Object,Mappable {
    dynamic var id = 0
    dynamic var doctorID = 0
    dynamic var orderNum = 0
    dynamic var specialID = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["DoctorSpecialistID"]
        doctorID <- map["DoctorID"]
        orderNum <- map["OrderNum"]
        specialID <- map["SpecialID"]
    }
    
}

