//
//  QuestionAnswerForDoctor.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class QuestionAnswerForDoctor:Mappable {
    var number:Int = 0
    var id = 0
    var title:String?
    var content:String?
    var specialistID = 0
    var specialName:String?
    var userID = 0
    var fullname:String?
    var avatar:String?
    var phone:String?
    var email:String?
    var address:String?
    var reader = 0
    var countRead = 0
    var countAnswer = 0
    var active = false
    var doctorID = 0
    var expireTime:Date?
    var imgStr:String?
    var imgThumbnailStr:String?
    var dateCreate:Date?
    var countLike = 0
    var countComment = 0
    var countShare = 0
    var answerID = 0
    var answerContent:String?
    var answerDateCreate:Date?
    var approver:Bool? = false
    var approverTime:Date?
    var answerRate:Int = 0
    var aCountLike:Int = 0
    var aCountThank:Int = 0
    var ACountShare:Int = 0
    var questionImage:String?
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        number <- map["NUMBER"]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"

        //dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        if map.mappingType == .fromJSON {
            id <- map["QuestionID"]
            reader <- map["Reader"]
            countRead <- map["CountRead"]
            countAnswer <- map["CountAnswer"]
            active <- map["Active"]
            questionImage <- map["QuestionImage"]
            expireTime <- (map["ExprireTime"],DateFormatterTransform(dateFormatter:dateFormatter))
            approverTime <- (map["ApproveTime"],DateFormatterTransform(dateFormatter:dateFormatter))
            imgThumbnailStr <- map["ImgThumbnailStr"]
            dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
            answerDateCreate <- (map["AnswerDateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
            countLike <- map["CountLike"]
            countComment <- map["CountComment"]
            countShare <- map["CountShare"]
            if dateCreate == nil {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
            }
            if expireTime == nil {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                expireTime <- (map["ExpireTime"],DateFormatterTransform(dateFormatter:dateFormatter))
            }
            if answerDateCreate == nil {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                answerDateCreate <- (map["AnswerDateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
            }
            if approverTime == nil {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                approverTime <- (map["ApproveTime"],DateFormatterTransform(dateFormatter:dateFormatter))
            }
            
        }
        title <- map["QuestionTitle"]
        content <- map["QuestionContent"]
        specialistID <- map["SpecialistID"]
        specialName <- map["SpecialName"]
        userID <- map["UserID"]
        fullname <- map["FullName"]
        avatar <- map["Avatar"]
        phone <- map["Phone"]
        email <- map["Email"]
        address <- map["Address"]
        doctorID <- map["DoctorID"]
        imgStr <- map["ImgStr"]
        answerID <- map["AnswerID"]
        answerContent <- map["AnswerContent"]
        approver <- map["Approver"]
        answerRate <- map["AnswerRate"]
        aCountLike <- map["ACountLike"]
        aCountThank <- map["ACountThank"]
        ACountShare <- map["ACountShare"]
        questionImage <- map["QuestionImage"]
    }
}
