//
//  LocationNearBy+Network.swift
//  iCNM
//
//  Created by Nguyen Van Dung on 7/29/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Alamofire

extension LocationNearBy {
    
    static func getLocationNearBy(lat: String, long: String, specialist:String, distance:Int, pageNumber:Int, pageRow:Int, type: String, rate: Int, completionHandler: @escaping([LocationNearBy]?, Error?) -> ()) {
        
        let urlStr = Constant.apiBaseUrl + "api/locations/LocationFix/" + "\(lat),\(long),\(specialist),\(distance),\(pageNumber),\(pageRow),\(type),\(rate)"
        
        let escapedUrl = urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        Alamofire.request(escapedUrl!, method: .get, parameters: nil,
                          encoding: URLEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let value):

                var data = [LocationNearBy]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let locationNearBy = LocationNearBy(JSON: item) {
                            data.append(locationNearBy)
                        }
                    }
                }
                completionHandler(data, nil)

            case .failure(let error):
                print("GetLocationNearBy Error : \(error.localizedDescription)")
            }
        }

    }
    
}
