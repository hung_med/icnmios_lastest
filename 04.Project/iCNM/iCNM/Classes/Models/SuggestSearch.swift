//
//  SuggestSearch.swift
//  iCNM
//
//  Created by Hoang Van Trung on 8/3/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import ObjectMapper

class SuggestSearch: Mappable {
    var searchID: Int = 0
    var searchContent: String = ""
    var dateCreate:String = ""
    var count: Int = 0
    var isValidSpell = false
    var dateUpdate: String = ""
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            searchID <- map["SearchID"]
            searchContent <- map["SearchContent"]
            dateCreate <- map["DateCreate"]
            count <- map["Count"]
            isValidSpell <- map["IsValidSpell"]
            dateUpdate <- map["DateUpdate"]
        }
    }
}
