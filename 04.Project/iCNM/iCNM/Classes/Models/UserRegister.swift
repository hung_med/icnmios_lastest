//
//  UserRegister.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class UserRegister:Mappable {
    var name = ""
    var phoneNumber = ""
    var userTypeID = 0
    var gender = "M"
    var password = ""
    var birthday = ""
    var verifyPhone = false
    var active = true
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .toJSON {
            name <- map["Name"]
            phoneNumber <- map["Phone"]
            userTypeID <- map["UserTypeID"]
            gender <- map["Gender"]
            password <- map["Password"]
            birthday <- map["BirthDay"]
            verifyPhone <- map["VerifyPhone"]
            active <- map["Active"]
        }
    }
    
}
