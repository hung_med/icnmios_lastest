//
//  Organize+Network.swift
//  iCNM
//
//  Created by Medlatec on 7/27/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension Organize {
    @discardableResult static func GetAllOrganizedSchedule(success:@escaping([Organize]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void)->Request {
        return API.requestJSON(endpoint: .GetAllOrganizedSchedule, completionHandler: { (response) in
            switch (response.result) {
            case .success(let result):
                var data = [Organize]()
                if let array = result as? [[String:Any]] {
                    let realm = try! Realm()
                    for item in array {
                        if let organize = Organize(JSON: item) {
                            data.append(organize)
                        }
                    }
                    try! realm.write {
                        realm.add(data, update: true)
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult static func getAllOrganizes(success:@escaping([Organize]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void)->Request {
        return API.requestJSON(endpoint: .GetAllOrganize, completionHandler: { (response) in
            switch (response.result) {
            case .success(let result):
                var data = [Organize]()
                if let array = result as? [[String:Any]] {
                    let realm = try! Realm()
                    for item in array {
                        if let organize = Organize(JSON: item) {
                            data.append(organize)
                        }
                    }
                    try! realm.write {
                        realm.add(data, update: true)
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult static func getAllOrganizeFollowSpecialistID(specialistID:Int, success:@escaping([Organize]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void)->Request {
        return API.requestJSON(endpoint: .getAllOrganizeFollowSpecialistID(specialistID), completionHandler: { (response) in
            switch (response.result) {
            case .success(let result):
                var data = [Organize]()
                if let array = result as? [[String:Any]] {
                    for item in array {
                        if let organize = Organize(JSON: item) {
                            data.append(organize)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }

    @discardableResult func getAllTimeRange(success:@escaping([ScheduleTime]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void)->Request {
        return API.requestJSON(endpoint: .GetOrganizeScheduleTime(self.id) , completionHandler: { (response) in
            switch (response.result) {
            case .success(let result):
                var data = [ScheduleTime]()
                if let array = result as? [[String:Any]] {
                    let realm = try! Realm()
                    for item in array {
                        if let timeRange = ScheduleTime(JSON: item) {
                            data.append(timeRange)
                        }
                    }
                    try! realm.write {
                        realm.add(data, update: true)
                        self.timeRanges.removeAll()
                        self.timeRanges.append(objectsIn: data)
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult static func getOrganizeByID(serviceID:Int, success:@escaping([Organize]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void)->Request {
        return API.requestJSON(endpoint: .getOrganizeByID(serviceID), completionHandler: { (response) in
            switch (response.result) {
            case .success(let result):
                var data = [Organize]()
                if let array = result as? [[String:Any]] {
                    for item in array {
                        if let organize = Organize(JSON: item) {
                            data.append(organize)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}

