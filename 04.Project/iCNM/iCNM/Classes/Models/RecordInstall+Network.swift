//
//  RecordInstall+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension RecordInstall {
    @discardableResult
    func checkRecordInstallTN(userID:Int, insDevice:Int, phone:String, success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        let parameterDic:[String:Any] = [
            "UserID": userID,
            "InsDevice": insDevice,
            "Phone": phone
        ]
        
        return API.requestJSON(endpoint: .checkRecordInstallTN(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func getRecordInstallTN(phone:String, success:@escaping(RecordInstall?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getRecordInstallTN(phone), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let recordInstall = RecordInstall(JSON: item) {
                        success(recordInstall)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func sendPromotionalCreditCard(numberCode:String, success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .sendPromotionalCreditCard(numberCode), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
