//
//  User.swift
//  iCNM
//
//  Created by Medlatec on 5/19/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper
import RealmSwift

@objc public enum LoginType:Int {
    case Normal = 0, Facebook, Google
}

class User:Object,Mappable {
    dynamic var id = 0
    dynamic var doctorID = 0
    dynamic var userDoctor:UserDoctor?
    dynamic var doctorInformation:DoctorInformation?
    dynamic var loginType:LoginType = .Normal
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .toJSON {
            userDoctor <- map["UserDoctor"]
            doctorInformation <- map["DoctorInfomation"]
        }
    }
    
}
