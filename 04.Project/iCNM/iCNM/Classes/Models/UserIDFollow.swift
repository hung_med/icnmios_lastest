//
//  UserIDFollow.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class UserIDFollow:Mappable {
    var flowID = 0
    var userID:Int?
    var userIDFllow:Int?
    var active:Bool? = false
    var dateCreate:Date? = nil
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            flowID <- map["FlowID"]
            userID <- map["UserID"]
            userIDFllow <- map["UserIDFllow"]
            active <- map["Active"]
            dateCreate <- map["DateCreate"]
        }
    }
}
