//
//  FeaturedDoctor.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class FeaturedDoctor:Mappable {
    
    var users:Users?
    var userInfo:UserInfo?
    var doctor:Doctor?
    //var specialists:Specialist?
    var specialists:[Specialist]?
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            users <- map["User"]
            userInfo <- map["User"]
            doctor <- map["Doctor"]
            specialists <- map["Specialists"]
            
        }
    }
}
