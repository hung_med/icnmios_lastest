//
//  LookUpChart+Network.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension LookUpChart {
    
    @discardableResult
    static func getDataChartDetailLookUp(pId:String, testCode:String, success:@escaping([LookUpChart])->Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>) -> Void) -> Request {
        var dictionary =  [String:String]()
        dictionary .updateValue(testCode, forKey: "TestCode")
        let parameterDic:[String:Any] = [
            "PID": pId,
            "Result": dictionary
        ]
       
        return API.requestJSON(endpoint: .getDataChartDetailLookUpByPID(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [LookUpChart]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let lookUpResult = LookUpChart(JSON: item) {
                            data.append(lookUpResult)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
