//
//  TestMedicalSchedule+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire

extension TestMedicalSchedule {
    
    @discardableResult
    static func getTestMedicalSchedule(success:@escaping([TestMedicalSchedule]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getTestMedicalSchedule(), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [TestMedicalSchedule]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let testMedicalSchedule = TestMedicalSchedule(JSON: item) {
                            data.append(testMedicalSchedule)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
