//
//  ProcessManager.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class ProcessManager:Mappable {
    var process:Process? = nil
    var schedule:Schedule? = nil
    var userLab:UserLab? = nil
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        process <- map["Process"]
        schedule <- map["Schedule"]
        userLab <- map["UserLab"]
    }
}
