//
//  ListCustomServiceDetail.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class ListCustomServiceDetail:Mappable {
    var provinceName:String = ""
    var location:Location? = nil
    var service:Service? = nil
    var listCustomServiceDetail:ListCustomServiceDetail?
    var serviceDetail:ServiceDetail?
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        provinceName <- map["ProvinceName"]
        location <- map["Location"]
        service <- map["Service"]
        listCustomServiceDetail <- map["ListCustomServiceDetail"]
        serviceDetail <- map["ServiceDetail"]
    }
}
