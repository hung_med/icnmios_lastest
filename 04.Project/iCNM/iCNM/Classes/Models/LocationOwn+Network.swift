//
//  LocationOwn+Network.swift
//  iCNM
//
//  Created by Hoang Van Trung on 11/24/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import Alamofire

extension LocationOwn {
    @discardableResult
    func getLocationOwn(param: Int, success:@escaping([LocationOwn]?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getLocationOwn(param), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [LocationOwn]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let lookUpResult = LocationOwn(JSON: item) {
                            data.append(lookUpResult)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func getLocationInfo(param: [String: Any], success:@escaping(LocationOwn?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getLocationInfo(param), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    success(LocationOwn(JSON: item))
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func updateAvatarLocation(parameterDic:[String:Any],success:@escaping(String)->Void, fail:@escaping (_ error:Error, _ response:DataResponse<String>) -> Void) -> Request {
        return API.requestString(endpoint: .updateAvatarLocation(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func addUpdateListImageLocation(parameterDic:[String:Any],success:@escaping(String)->Void, fail:@escaping (_ error:Error, _ response:DataResponse<String>) -> Void) -> Request {
        return API.requestString(endpoint: .addUpdateListImageLocation(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func addUpdateImageLocation(parameterDic:[String:Any],success:@escaping(String)->Void, fail:@escaping (_ error:Error, _ response:DataResponse<String>) -> Void) -> Request {
        return API.requestString(endpoint: .addUpdateImageLocation(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func addUpdateLocation(parameterDic:[String:Any],success:@escaping(String)->Void, fail:@escaping (_ error:Error, _ response:DataResponse<String>) -> Void) -> Request {
        return API.requestString(endpoint: .updateLocation(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
