//
//  FWSpeechBubble.swift
//  iCNM
//
//  Created by Medlatec on 6/19/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit


class FWSpeechBubble: UIView {
    
    @IBInspectable var bubbleImage:UIImage? {
        didSet {
            updateImageView()
        }
    }
    @IBInspectable var topCapInset:CGFloat = 0.0 {
        didSet {
            updateImageView()
        }
    }
    @IBInspectable var leftCapInset:CGFloat = 0.0 {
        didSet {
            updateImageView()
        }
    }
    @IBInspectable var bottomCapInset:CGFloat = 0.0 {
        didSet {
            updateImageView()
        }
    }
    @IBInspectable var rightCapInset:CGFloat = 0.0 {
        didSet {
            updateImageView()
        }
    }
    
    @IBInspectable var bubbleColor:UIColor = .lightGray {
        didSet {
            updateImageView()
        }
    }
    
    private var bubbleImageView = UIImageView()
    
    private func updateImageView() {
        bubbleImageView.image = bubbleImage?.resizableImage(withCapInsets: UIEdgeInsetsMake(topCapInset, leftCapInset, bottomCapInset, rightCapInset)).withRenderingMode(.alwaysTemplate)
        bubbleImageView.tintColor = bubbleColor
    }
    
    private func setup() {
        bubbleImageView = UIImageView(frame: self.bounds)
        updateImageView()
        self.insertSubview(bubbleImageView, at: 0)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        bubbleImageView.frame = self.bounds
    }
}
