//
//  FWTextFieldValidator.swift
//  ezCloudHotelManager
//
//  Created by Medlatec on 5/9/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

/// Popup class to be displayed
fileprivate class ValidatorPopup:UIView {
    fileprivate var showForTextField:FWTextFieldValidator!
    fileprivate var fieldFrame:CGRect?
    fileprivate var message:String = ""
    fileprivate var popUpColor = UIColor.clear
    fileprivate var arrowColor = UIColor.clear
    fileprivate var paddingInErrorPopup:CGFloat = 5.0
    fileprivate var errorFont = UIFont.boldSystemFont(ofSize: 15.0)
    fileprivate var errorFontColor = UIColor.red
    
    override func draw(_ rect: CGRect) {
        let showOnRect = showForTextField.convert(showForTextField.rightView!.frame, to: showForTextField.presentInView)
        let fieldFrame = showForTextField.superview?.convert(showForTextField.frame, to: showForTextField.presentInView)
        let maximumWidthForPopup = fieldFrame!.origin.x + fieldFrame!.size.width

        let triangleLine:UIBezierPath = UIBezierPath()
        let startX:CGFloat = showOnRect.origin.x + (showOnRect.size.width-30)/2.0
        let startY:CGFloat = showOnRect.origin.y + showOnRect.size.height - 2.0
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width:30, height:13.0), false, UIScreen.main.scale);
        let ctx = UIGraphicsGetCurrentContext();
        ctx?.setFillColor(arrowColor.cgColor)
        triangleLine.move(to: CGPoint(x:15,y:5))
        triangleLine.addLine(to: CGPoint(x:30,y:25))
        triangleLine.addLine(to: CGPoint(x:0,y:25))
        triangleLine.close()
        triangleLine.fill()
        let triangleImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        
        let attributedString = NSAttributedString(string: message, attributes: [NSFontAttributeName:errorFont, NSForegroundColorAttributeName:errorFontColor])
        let size = attributedString.sizeWithConstrainedWidth(width:(maximumWidthForPopup-(paddingInErrorPopup*2)))
        
        arrowColor.setStroke()
        let topLine = UIBezierPath()
        topLine.lineWidth = 2
        topLine.move(to: CGPoint(x:maximumWidthForPopup - (size.width+paddingInErrorPopup*2), y:startY+13.0))
        topLine.addLine(to: CGPoint(x:maximumWidthForPopup, y:startY+13.0))
        popUpColor.setFill()
        let context = UIGraphicsGetCurrentContext()
        context?.saveGState()
        context?.setShadow(offset: CGSize(width:0, height:2), blur: 7.0,  color: UIColor.black.cgColor)
        let rectangleBox:UIBezierPath = UIBezierPath(rect: CGRect(x:maximumWidthForPopup - (size.width+(paddingInErrorPopup*2)), y:startY+14.0, width: size.width + (paddingInErrorPopup * 2), height: size.height + (paddingInErrorPopup * 2)))
        rectangleBox.fill()
        context?.restoreGState()
        attributedString.draw(in: CGRect(x:maximumWidthForPopup - (size.width+paddingInErrorPopup), y:startY+14.0+paddingInErrorPopup, width:size.width, height:size.height))
        arrowColor.setStroke()
        triangleImage?.draw(in: CGRect(x:startX, y:startY, width:30, height: 13.0))
        topLine.stroke()
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        self.removeFromSuperview()
        return false
    }
}


fileprivate class TextFieldValidatorSupport:NSObject,UITextFieldDelegate {
    fileprivate weak var delegate:UITextFieldDelegate?
    fileprivate var validateOnCharacterChanged:Bool = true
    fileprivate var validateOnResign:Bool = true
    fileprivate weak var popUp:ValidatorPopup?
    
    fileprivate func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return delegate?.textFieldShouldBeginEditing?(textField) ?? true
    }
    
    fileprivate func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.textFieldDidBeginEditing?(textField)
    }
    
    fileprivate func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return delegate?.textFieldShouldEndEditing?(textField) ?? true
    }
    
    fileprivate func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.textFieldDidEndEditing?(textField)
        popUp?.removeFromSuperview()
        if validateOnResign {
            let txtField = textField as! FWTextFieldValidator
            txtField.validate()
        }
    }
    
    fileprivate func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let txtField = textField as! FWTextFieldValidator
        txtField.dismissPopup()
        if validateOnCharacterChanged {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                txtField.validate()
            })
        } else {
            txtField.rightView = nil
        }
        return delegate?.textField?(textField, shouldChangeCharactersIn: range, replacementString: string) ?? true
    }
    
    fileprivate func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return delegate?.textFieldShouldClear?(textField) ?? true
    }
    
    fileprivate func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return delegate?.textFieldShouldReturn?(textField) ?? true
    }
}


class FWTextFieldValidator: UITextField {
    private var bottomLayer:CALayer?
    
    @IBInspectable var bottomBorderOnly:Bool = false {
        didSet {
            layer.setNeedsLayout()
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.setNeedsLayout()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.setNeedsLayout()
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var paddingLeft: CGFloat = 0
    @IBInspectable var paddingRight: CGFloat = 0
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x:bounds.origin.x + paddingLeft, y:bounds.origin.y,
                      width:bounds.size.width - paddingLeft - paddingRight - (errorImage == nil ? 0:20), height:bounds.size.height);
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.rightViewRect(forBounds: bounds)
        textRect.origin.x -= paddingRight
        return textRect
    }
    
    @IBInspectable var errorImage:UIImage?
    var errorFont = UIFont.boldSystemFont(ofSize: 14.0)
    @IBInspectable var popUpColor:UIColor = UIColor.clear
    @IBInspectable var arrowColor:UIColor = UIColor.clear
    @IBInspectable var errorFontColor = UIColor.red
    @IBInspectable var paddingInErrorPopup:CGFloat = 5.0
    @IBInspectable var messageForValidatingLength:String = "This field cannot be blank This field cannot be blank This field cannot be blank This field cannot be blank This field cannot be blank"
    @IBInspectable var isMandatory:Bool = true
    private var supportObj:TextFieldValidatorSupport = TextFieldValidatorSupport()
    @IBInspectable var validateOnCharacterChanged:Bool = false {
        didSet {
            supportObj.validateOnCharacterChanged = validateOnCharacterChanged
        }
    }
    
    @IBInspectable var validateOnResign:Bool = false {
        didSet {
            supportObj.validateOnResign = validateOnResign
        }
    }
    
    @IBInspectable var autoShowPopup:Bool = false
    
    @IBOutlet weak var presentInView:UIView?
    
    private var arrRegx = [Dictionary<String,Any>]()
    private weak var popUp:ValidatorPopup?
    private var strMessage = ""
    
    override var delegate: UITextFieldDelegate? {
        didSet {
            supportObj.delegate = delegate
            super.delegate = supportObj
        }
    }
    
    // MARK: Default Methods of UIView
    func setup() {
        supportObj.validateOnCharacterChanged = validateOnCharacterChanged
        supportObj.validateOnResign = validateOnResign
        NotificationCenter.default.addObserver(self, selector: #selector(didHideKeyboard), name: .UIKeyboardWillHide, object: nil)
        setupBorder()
    }
    
    func setupBorder() {
        bottomLayer?.removeFromSuperlayer()
        bottomLayer = nil
        if !bottomBorderOnly {
            layer.borderColor = borderColor.cgColor
            layer.borderWidth = borderWidth
        } else {
            layer.borderWidth = 0
            bottomLayer = CALayer()
            bottomLayer?.backgroundColor = isFirstResponder ? borderColor.cgColor:UIColor.gray.cgColor
            layer.addSublayer(bottomLayer!)
            bottomLayer?.frame = CGRect(x: 0, y: bounds.size.height - borderWidth, width: bounds.size.width, height: isFirstResponder ? borderWidth:1.0)
            bottomLayer?.backgroundColor = isFirstResponder ? borderColor.cgColor:UIColor.gray.cgColor
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func removeAllRegx() {
        arrRegx.removeAll()
    }
    
    // MARK: Public Method
    
    /**
     Use to add regex for validating textfield text, you need to specify all your regex in queue that you want to validate and their messages respectively that will show when any regex validation will fail.
     - parameter strRegx:   Regex string.
     - parameter errorMsg:  Message string to be displayed when given regex fail.
     */
    func addRegx(strRegx:String, errorMsg:String) {
        let dic = ["regx":strRegx, "msg":errorMsg]
        arrRegx.append(dic)
    }
    
    /**
     Use to add validation for validating confirm password
     - parameter txtPassword: Hold reference of password textfield from which they will check text equality.
     - parameter withMessage: Message string to be displayed when password does not match
     */
    func addConfirmValidationTo(txtPassword:FWTextFieldValidator, withMessage:String) {
        let dic = ["confirm":txtPassword, "msg":withMessage] as [String : Any]
        arrRegx.append(dic)
    }
    /**
     Use to perform validation.If you want to apply validation on all fields simultaneously then refer below code which will be make it easy to handle validations
     ```swift
     if([txtField1 validate] & [txtField2 validate]){
     // Success operation
     }
     ```
     - Returns: Bool,It will return **true** if all provided regex validation will pass else return **false**
     */
    @discardableResult
    func validate()->Bool {
        if isMandatory {
            if let length = self.text?.characters.count {
                if length == 0 {
                    self.showErrorIconForMessage(message: messageForValidatingLength)
                    return false
                }
            } else {
                self.showErrorIconForMessage(message: messageForValidatingLength)
                return false
            }
        }
        for dic in arrRegx {
            if let txtConfirm = dic["confirm"] as? FWTextFieldValidator {
                if let password = txtConfirm.text, let confirmPassword = self.text {
                    if password != confirmPassword {
                        self.showErrorIconForMessage(message: (dic["msg"] as! String))
                        return false
                    }
                }
            } else if let regx = dic["regx"] as? String, regx.characters.count > 0 {
                if let text = self.text, text.characters.count > 0 {
                    if !self.validateString(stringToSearch: text, withRegex: regx) {
                        self.showErrorIconForMessage(message: (dic["msg"] as! String))
                        return false
                    }
                }
            }
        }
        self.rightView = nil
        return true;
    }
    
    /**
     Use to dismiss error popup.
     */
    func dismissPopup() {
        popUp?.removeFromSuperview()
    }
    
    // MARK: Internal Methods
    
    /**
     Handle tap on error icon event
     */
    @objc private func didHideKeyboard(aNotification:Notification) {
        popUp?.removeFromSuperview()
    }
    
    @objc private func tapOnErrorIcon() {
        self.showErrorWithMessage(message: strMessage)
    }
    
    private func validateString(stringToSearch:String, withRegex:String) -> Bool {
        let regex = NSPredicate(format: "SELF MATCHES %@", withRegex)
        return regex.evaluate(with: stringToSearch)
    }
    
    private func showErrorIconForMessage(message:String) {
        let btnError = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        btnError.addTarget(self, action:#selector(tapOnErrorIcon), for: .touchUpInside)
        btnError.setBackgroundImage(errorImage, for: .normal)
        if errorImage == nil {
            btnError.isEnabled = false
        }
        self.rightView = btnError
        self.rightViewMode = .always
        strMessage = message
        if autoShowPopup {
            showErrorWithMessage(message: message)
        }
    }
    
    // TODO: Implement Popup
    private func showErrorWithMessage(message:String) {
        if let presentView = presentInView {
            let thePopUp = ValidatorPopup(frame: CGRect.zero)
            thePopUp.message = message
            thePopUp.popUpColor = popUpColor
            thePopUp.backgroundColor = UIColor.clear
            thePopUp.translatesAutoresizingMaskIntoConstraints = false
            thePopUp.showForTextField = self
            thePopUp.paddingInErrorPopup = paddingInErrorPopup
            thePopUp.errorFont = errorFont
            thePopUp.errorFontColor = errorFontColor
            thePopUp.contentMode = .redraw
            thePopUp.arrowColor = arrowColor
            presentView.addSubview(thePopUp)
            let dict = ["popUp":thePopUp]
            presentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[popUp]-20-|", options: .directionLeadingToTrailing, metrics: nil, views: dict))
            presentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(-12)-[popUp]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: dict))
            supportObj.popUp = thePopUp
            popUp = thePopUp
        }
    }
    
    deinit {
        print("deinit textfield")
        NotificationCenter.default.removeObserver(self)
    }
    
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        setupBorder()        
    }
}


class FWTextFieldValidatorFloatLabel:FWTextFieldValidator {
    let animationDuration = 0.3
    var title = UILabel()
    override var accessibilityLabel:String? {
        get {
            if let txt = text , txt.isEmpty {
                return title.text
            } else {
                return text
            }
        }
        set {
            self.accessibilityLabel = newValue
        }
    }
    
    override var placeholder:String? {
        didSet {
            title.text = placeholder
            title.sizeToFit()
        }
    }
    
    override var attributedPlaceholder:NSAttributedString? {
        didSet {
            title.text = attributedPlaceholder?.string
            title.sizeToFit()
        }
    }
    
    var titleFont:UIFont = UIFont.systemFont(ofSize: 12.0) {
        didSet {
            title.font = titleFont
            title.sizeToFit()
        }
    }
    
    @IBInspectable var hintYPadding:CGFloat = 0.0
    
    @IBInspectable var titleYPadding:CGFloat = 0.0 {
        didSet {
            var r = title.frame
            r.origin.y = titleYPadding
            title.frame = r
        }
    }
    
    @IBInspectable var titleTextColour:UIColor = UIColor.gray {
        didSet {
            if !isFirstResponder {
                title.textColor = titleTextColour
            }
        }
    }
    
    @IBInspectable var titleActiveTextColour:UIColor! {
        didSet {
            if isFirstResponder {
                title.textColor = titleActiveTextColour
            }
        }
    }
    
    // MARK:- Init
    required init?(coder aDecoder:NSCoder) {
        super.init(coder:aDecoder)
        setupFloatLabel()
    }
    
    override init(frame:CGRect) {
        super.init(frame:frame)
        setupFloatLabel()
    }
    
    // MARK:- Overrides
    override func layoutSubviews() {
        super.layoutSubviews()
        setTitlePositionForTextAlignment()
        let isResp = isFirstResponder
        if let txt = text , !txt.isEmpty && isResp {
            title.textColor = titleActiveTextColour
        } else {
            title.textColor = titleTextColour
        }
        // Should we show or hide the title label?
        if let txt = text , txt.isEmpty {
            // Hide
            hideTitle(isResp)
        } else {
            // Show
            showTitle(isResp)
        }
    }
    
    override func textRect(forBounds bounds:CGRect) -> CGRect {
        var r = super.textRect(forBounds: bounds)
        var top = ceil(title.font.lineHeight + hintYPadding)
        top = min(top, maxTopInset())
        r = UIEdgeInsetsInsetRect(r, UIEdgeInsetsMake(top, 0.0, 0.0, 0.0))
        return r
    }
    
    override func editingRect(forBounds bounds:CGRect) -> CGRect {
        return self.textRect(forBounds: bounds)
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var r = super.rightViewRect(forBounds: bounds)
        print(r)
        var top = ceil(title.font.lineHeight + hintYPadding)
        top = min(top, maxTopInset())
        r = CGRect(x:r.origin.x, y:r.origin.y + (top * 0.5), width:r.size.width, height:r.size.height)
        print(r)        
        return r
    }
    
    // MARK:- Public Methods
    
    // MARK:- Private Methods
    fileprivate func setupFloatLabel() {
        borderStyle = UITextBorderStyle.none
        titleActiveTextColour = tintColor
        // Set up title label
        title.alpha = 0.0
        title.font = titleFont
        title.textColor = titleTextColour
        if let str = placeholder , !str.isEmpty {
            title.text = str
            title.sizeToFit()
        }
        self.addSubview(title)
    }
    
    fileprivate func maxTopInset()->CGFloat {
        if let fnt = font {
            return max(0, floor(bounds.size.height - fnt.lineHeight - 4.0))
        }
        return 0
    }
    
    fileprivate func setTitlePositionForTextAlignment() {
        let r = textRect(forBounds: bounds)
        var x = r.origin.x
        if textAlignment == NSTextAlignment.center {
            x = r.origin.x + (r.size.width * 0.5) - title.frame.size.width
        } else if textAlignment == NSTextAlignment.right {
            x = r.origin.x + r.size.width - title.frame.size.width
        }
        title.frame = CGRect(x:x, y:title.frame.origin.y, width:title.frame.size.width, height:title.frame.size.height)
    }
    
    fileprivate func showTitle(_ animated:Bool) {
        let dur = animated ? animationDuration : 0
        UIView.animate(withDuration: dur, delay:0, options: [UIViewAnimationOptions.beginFromCurrentState, UIViewAnimationOptions.curveEaseOut], animations:{
            // Animation
            self.title.alpha = 1.0
            var r = self.title.frame
            r.origin.y = self.titleYPadding
            self.title.frame = r
        }, completion:nil)
    }
    
    fileprivate func hideTitle(_ animated:Bool) {
        let dur = animated ? animationDuration : 0
        UIView.animate(withDuration: dur, delay:0, options: [UIViewAnimationOptions.beginFromCurrentState, UIViewAnimationOptions.curveEaseIn], animations:{
            // Animation
            self.title.alpha = 0.0
            var r = self.title.frame
            r.origin.y = self.title.font.lineHeight + self.hintYPadding
            self.title.frame = r
        }, completion:nil)
    }
    
}
