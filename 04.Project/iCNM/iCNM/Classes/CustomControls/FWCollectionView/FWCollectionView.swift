//
//  FWCollectionView.swift
//  iCNM
//
//  Created by Medlatec on 7/27/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class FWCollectionView: UICollectionView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override var contentSize: CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override public var intrinsicContentSize: CGSize {
        get {
            return self.contentSize
        }
    }
}
