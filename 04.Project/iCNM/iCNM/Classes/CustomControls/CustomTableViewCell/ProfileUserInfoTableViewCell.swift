//
//  ProfileUserInfoTableViewCell.swift
//  iCNM
//
//  Created by Medlatec on 5/16/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class ProfileUserInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    @IBOutlet weak var profileImageView: PASImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
