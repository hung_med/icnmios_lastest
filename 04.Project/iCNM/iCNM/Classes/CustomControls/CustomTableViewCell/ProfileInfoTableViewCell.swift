//
//  ProfileInfoTableViewCell.swift
//  iCNM
//
//  Created by Medlatec on 5/16/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class ProfileInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var infoTitleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
