//
//  ProfileFooterTableViewCell.swift
//  iCNM
//
//  Created by Medlatec on 9/20/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class ProfileFooterTableViewCell: UITableViewCell {
    @IBOutlet weak var footerLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
