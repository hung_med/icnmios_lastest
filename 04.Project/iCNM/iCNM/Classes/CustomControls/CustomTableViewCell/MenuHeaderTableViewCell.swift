//
//  MenuHeaderTableViewCell.swift
//  iCNM
//
//  Created by Anh Nguyen on 10/24/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class MenuHeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var headerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
