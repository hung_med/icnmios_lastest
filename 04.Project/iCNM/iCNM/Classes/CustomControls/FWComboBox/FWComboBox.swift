//
//  FWComboBox.swift
//  iCNM
//
//  Created by Medlatec on 5/23/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import UIKit
import DropDown

@objc protocol FWComboBoxDelegate : class {
    @objc optional func fwComboBox(comboBox:FWComboBox, didSelectAtIndex index:Int)
    @objc optional func fwComboBoxWillShow(comboBox:FWComboBox)
}


class FWComboBox: UIView, UIGestureRecognizerDelegate {
    
    private lazy var dropDown = DropDown()
    private let titleLabel = UILabel()
    
    weak var delegate:FWComboBoxDelegate?
    private var paddingLeftConstraint:NSLayoutConstraint!
    private var paddingRightConstraint:NSLayoutConstraint!
    
    @IBInspectable var defaultTitle:String = "Chọn"
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var paddingLeft: CGFloat = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var paddingRight: CGFloat = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var bottomOffset: CGFloat = 0 {
        didSet {
            dropDown.bottomOffset = CGPoint(x: 0, y:bottomOffset)
        }
    }
    
    @IBInspectable var topOffset: CGFloat = 0 {
        didSet {
            dropDown.topOffset = CGPoint(x: 0, y:-topOffset)
        }
    }
    
    @IBInspectable var showArrow = true {
        didSet {
            setNeedsLayout()
        }
    }
    
    var comboTextAlignment = NSTextAlignment.left {
        didSet {
            titleLabel.textAlignment = comboTextAlignment
        }
    }
    
    var direction:DropDown.Direction = .any {
        didSet {
            dropDown.direction = direction
        }
    }
    
    var dataSource = [String]() {
        didSet {
            dropDown.dataSource = dataSource
            self.titleLabel.text = dropDown.selectedItem ?? defaultTitle
        }
    }
    
    var currentRow:Int? {
        get {
            return dropDown.indexForSelectedRow
        }
    }
    
    var arrowLabel:UILabel = {
        var label = UILabel()
        label.text = "▼"
        label.font = UIFont.systemFont(ofSize: 15.0)
        label.sizeToFit()
        return label
    }()
    
    func selectRow(at:Int?) {
        dropDown.selectRow(at: at)
        self.titleLabel.text = dropDown.selectedItem ?? defaultTitle
    }
    
    func setup() {
        self.clipsToBounds = true
        dropDown.anchorView = self;
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            if self != nil {
                self!.delegate?.fwComboBox?(comboBox: self!, didSelectAtIndex: index)
                self!.titleLabel.text = item
            }
        }
        titleLabel.font = UIFont.systemFont(ofSize: 15);
        titleLabel.textAlignment = comboTextAlignment
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(titleLabel)
        titleLabel.frame = CGRect(x: paddingLeft, y: 0, width: bounds.size.width - paddingLeft - paddingRight - (showArrow ? arrowLabel.bounds.size.width + paddingRight : 0), height: bounds.size.height)
        if showArrow {
            arrowLabel.frame = CGRect(x: bounds.size.width - paddingRight - arrowLabel.bounds.size.width, y: (bounds.size.height - arrowLabel.bounds.size.height)/2.0, width: arrowLabel.bounds.size.width, height: arrowLabel.bounds.size.height)
            self.addSubview(arrowLabel)
        }
        titleLabel.text = defaultTitle
        self.isUserInteractionEnabled = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(touchedSelf))
        tapGestureRecognizer.delegate = self
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    @objc private func touchedSelf(gestureRecognizer: UIGestureRecognizer) {
        if gestureRecognizer.state == .ended {
            delegate?.fwComboBoxWillShow?(comboBox: self)
            dropDown.show()
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        titleLabel.frame = CGRect(x: paddingLeft, y: 0, width: bounds.size.width - paddingLeft - paddingRight  - (showArrow ? arrowLabel.bounds.size.width + paddingRight : 0), height: bounds.size.height)
        if showArrow {
            arrowLabel.frame = CGRect(x: bounds.size.width - paddingRight - arrowLabel.bounds.size.width, y: (bounds.size.height - arrowLabel.bounds.size.height)/2.0, width: arrowLabel.bounds.size.width, height: arrowLabel.bounds.size.height)
            if arrowLabel.superview != self {
                self.addSubview(arrowLabel)
            }
        } else {
            arrowLabel.removeFromSuperview()
        }
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
    }
    
    deinit {
        print("deinit combobox")
    }
    
}
