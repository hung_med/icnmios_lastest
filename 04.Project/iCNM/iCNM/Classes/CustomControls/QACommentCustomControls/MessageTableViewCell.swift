//
//  MessageTableViewCell.swift
//  iCNM
//
//  Created by Medlatec on 6/28/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import SlackTextViewController

class MessageTableViewCell: UITableViewCell {
    
    static var minimumHeight:CGFloat = 60.0
    static var avatarHeight:CGFloat = 50.0
    static var commentCellIdentifier = "CommentCellIdentifier"
    var indexPath:IndexPath = IndexPath(row: 0, section: 0)
    
    var titleLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        label.isUserInteractionEnabled = false
        label.numberOfLines = 0
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: MessageTableViewCell.defaultFontSize())
        return label
    }()
    
    var statusLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = .clear
        label.isUserInteractionEnabled = false
        label.numberOfLines = 0
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    var editButton:UIButton = {
        let button = UIButton(type:.custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitle("Sửa", for: .normal)
        button.setTitleColor(UIColor(hex:"1D6EDC"), for: .normal)
        return button
    }()

    var deleteButton:UIButton = {
        let button = UIButton(type:.custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setTitle("Xoá", for: .normal)
        button.setTitleColor(UIColor(hex:"EC4B4B"), for: .normal)
        return button
    }()
    
    var bodyLabel:UILabel = {
        let label = FWTagLabel()
        label.horizontalPadding = 10
        label.cornerRadius = 10
        label.verticalPadding = 5
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = UIColor(hex:"F3F3F5")
        label.isUserInteractionEnabled = false
        label.numberOfLines = 0
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: MessageTableViewCell.defaultFontSize())
        return label
    }()
    
    var thumbnailView:PASImageView = {
        let imageView = PASImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isUserInteractionEnabled = false
        imageView.backgroundProgressColor = UIColor(hex:"1D6EDC")
        imageView.progressColor = UIColor(hex:"2196F3")
        imageView.placeHolderImage = UIImage(asset: Asset.avatarHolder)
        return imageView
    }()
    
    static func defaultFontSize()->CGFloat {
        var pointSize:CGFloat = 16.0
        let contentSizeCategory = UIApplication.shared.preferredContentSizeCategory
        pointSize += SLKPointSizeDifferenceForCategory(contentSizeCategory.rawValue)
        return pointSize
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.backgroundColor = .white
        let f = contentView.frame
        let fr = UIEdgeInsetsInsetRect(f, UIEdgeInsetsMake(15, 15, 15, 15))
        contentView.frame = fr
        addShadow(cell: self)
        configureSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configureSubviews() {
        self.contentView.addSubview(thumbnailView)
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(bodyLabel)
        self.contentView.addSubview(statusLabel)
        self.contentView.addSubview(editButton)
        self.contentView.addSubview(deleteButton)
        let views = ["thumbnailView":thumbnailView,
                     "titleLabel":titleLabel,
                     "bodyLabel":bodyLabel,
                     "statusLabel":statusLabel,
                     "editButton":editButton,
                     "deleteButton":deleteButton] as [String : Any]
        let metrics = ["tumbSize":MessageTableViewCell.avatarHeight,
                       "padding":15,
                       "right":10,
                       "left":5]
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-left-[thumbnailView(tumbSize)]-right-[titleLabel(>=0)]-right-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metrics, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-left-[thumbnailView(tumbSize)]-right-[bodyLabel(>=0)]-right-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metrics, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-right-[thumbnailView(tumbSize)]-(>=0)-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metrics, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-right-[titleLabel(20)]-left-[bodyLabel(>=0@999)]-left-[statusLabel(>=0@999)]-left-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metrics, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-left-[thumbnailView(tumbSize)]-right-[statusLabel(>=0)]-left-[editButton(50)]-right-[deleteButton(50)]-right-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metrics, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-right-[titleLabel(20)]-left-[bodyLabel(>=0@999)]-left-[editButton(>=0@999)]-left-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metrics, views: views))
                self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-right-[titleLabel(20)]-left-[bodyLabel(>=0@999)]-left-[deleteButton(>=0@999)]-left-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metrics, views: views))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func addShadow(cell:UITableViewCell) {
        cell.layer.cornerRadius = 4
        cell.layer.masksToBounds = true
        
        cell.layer.masksToBounds = false
        cell.layer.shadowOffset = CGSize(width: 0, height:0)
        cell.layer.shadowColor = UIColor.darkGray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowRadius = 2
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.selectionStyle = .none
        let pointSize = MessageTableViewCell.defaultFontSize()
        self.titleLabel.font = UIFont.boldSystemFont(ofSize: pointSize)
        self.bodyLabel.font = UIFont.systemFont(ofSize: pointSize)
        self.titleLabel.text = ""
        self.bodyLabel.text = ""
    }
    
}
