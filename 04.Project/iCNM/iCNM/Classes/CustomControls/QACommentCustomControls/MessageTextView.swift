//
//  MessageTextView.swift
//  iCNM
//
//  Created by Medlatec on 6/28/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import SlackTextViewController

class MessageTextView: SLKTextView {
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        self.backgroundColor = .white
        self.placeholder = "Nội dung"
        self.placeholderColor = .lightGray
        self.layer.borderColor = UIColor(red: 217.0/255.0, green: 217.0/255.0, blue: 217.0/255.0, alpha: 1.0).cgColor
    }
}
