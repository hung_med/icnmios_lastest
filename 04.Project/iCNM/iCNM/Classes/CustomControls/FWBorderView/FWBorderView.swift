//
//  FWBorderView.swift
//  iCNM
//
//  Created by Medlatec on 7/26/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit


class FWBorderView: UIControl {

    @IBInspectable var borderColor:UIColor = UIColor.gray {
        didSet {
            topBorder.backgroundColor = borderColor
            bottomBorder.backgroundColor = borderColor
        }
    }
    private let topBorder = UIView()
    private let bottomBorder = UIView()
    
    private func setupTopAndBottomBorder() {
        topBorder.frame = CGRect(x: 0.0, y: 0.0, width: self.bounds.width, height: 1.0)
        bottomBorder.frame = CGRect(x: 0.0, y: self.bounds.height-1.0, width: self.bounds.width, height: 1.0)
        topBorder.backgroundColor = borderColor
        bottomBorder.backgroundColor = borderColor
        self.addSubview(topBorder)
        self.addSubview(bottomBorder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupTopAndBottomBorder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupTopAndBottomBorder()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        topBorder.frame = CGRect(x: 0.0, y: 0.0, width: self.bounds.width, height: 1.0)
        bottomBorder.frame = CGRect(x: 0.0, y: self.bounds.height-1.0, width: self.bounds.width, height: 1.0)
    }
}
