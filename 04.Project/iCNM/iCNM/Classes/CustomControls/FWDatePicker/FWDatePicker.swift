//
//  FWDatePicker.swift
//  iCNM
//
//  Created by Medlatec on 5/20/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Toaster
@objc
protocol FWDatePickerDelegate: class {
    @objc optional func change(_ picker:FWDatePicker, toDate:Date)
    @objc optional func willShow(_ picker:FWDatePicker)
}


class FWDatePicker: UIView, MDDatePickerDialogDelegate {
    weak var delegate:FWDatePickerDelegate?
    var conditionDate:String? = nil
    var minimumDate:Date? {
        didSet {
            if let minimumD = minimumDate {
                if currentDate.compare(minimumD) == .orderedAscending {
                    currentDate = minimumD
                }
                let currentMonth = Calendar.current.component(.month, from: currentDate)
                let minimumMonth = Calendar.current.component(.month, from: minimumD)
                let currentYear = Calendar.current.component(.year, from: currentDate)
                let minimumYear = Calendar.current.component(.year, from: minimumD)
                if currentMonth == minimumMonth && minimumYear == currentYear {
                    leftButton?.isEnabled = false
                    //                    leftButton?.isHidden = true
                } else {
                    leftButton?.isEnabled = true
                    //                    leftButton?.isHidden = false
                }
                middleButton.setTitle(formatter.string(from: currentDate).capitalized, for: .normal)
            }
        }
    }
    
    var maximumDate:Date? {
        didSet {
            if let maximumD = maximumDate {
                if currentDate.compare(maximumD) == .orderedDescending {
                    currentDate = maximumD
                }
                let currentMonth = Calendar.current.component(.month, from: currentDate)
                let maximumMonth = Calendar.current.component(.month, from: maximumD)
                let currentYear = Calendar.current.component(.year, from: currentDate)
                let maximumYear = Calendar.current.component(.year, from: maximumD)
                if currentMonth == maximumMonth && currentYear == maximumYear {
                    rightButton?.isEnabled = false
                    //                    leftButton?.isHidden = true
                } else {
                    rightButton?.isEnabled = true
                    //                    leftButton?.isHidden = false
                }
                middleButton.setTitle(formatter.string(from: currentDate).capitalized, for: .normal)
            }
        }
    }
    
    var currentDate = Date() {
        didSet {
            if let minimumD = minimumDate {
                if currentDate.compare(minimumD) == .orderedAscending {
                    currentDate = minimumD
                }
                let currentMonth = Calendar.current.component(.month, from: currentDate)
                let minimumMonth = Calendar.current.component(.month, from: minimumD)
                let currentYear = Calendar.current.component(.year, from: currentDate)
                let minimumYear = Calendar.current.component(.year, from: minimumD)
                if currentMonth == minimumMonth && minimumYear == currentYear {
                    leftButton?.isEnabled = false
                    //                    leftButton?.isHidden = true
                } else {
                    leftButton?.isEnabled = true
                    //                    leftButton?.isHidden = false
                }
                middleButton.setTitle(formatter.string(from: currentDate).capitalized, for: .normal)
            }
            if let maximumD = maximumDate {
                if currentDate.compare(maximumD) == .orderedDescending {
                    currentDate = maximumD
                }
                let currentMonth = Calendar.current.component(.month, from: currentDate)
                let maximumMonth = Calendar.current.component(.month, from: maximumD)
                let currentYear = Calendar.current.component(.year, from: currentDate)
                let maximumYear = Calendar.current.component(.year, from: maximumD)
                if currentMonth == maximumMonth && currentYear == maximumYear {
                    rightButton?.isEnabled = false
                    //                    leftButton?.isHidden = true
                } else {
                    rightButton?.isEnabled = true
                    //                    leftButton?.isHidden = false
                }
                middleButton.setTitle(formatter.string(from: currentDate).capitalized, for: .normal)
            }
            middleButton.setTitle(formatter.string(from: currentDate).capitalized, for: .normal)
        }
    }
    var formatter = DateFormatter() {
        didSet {
            middleButton.setTitle(formatter.string(from: currentDate).capitalized, for: .normal)
        }
    }
    
    var titleFont:UIFont = UIFont.systemFont(ofSize: 15) {
        didSet {
            middleButton.titleLabel?.font = titleFont
        }
    }
    
    @IBInspectable var dateImage:UIImage? {
        didSet {
            middleButton.setImage(dateImage, for: .normal)
        }
    }
    
    @IBInspectable var showArrow:Bool = false {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable var leftArrowImage:UIImage? {
        didSet {
            self.setNeedsLayout()
        }
    }
    @IBInspectable var rightArrowImage:UIImage? {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable var imageTitleSpacing:CGFloat = 0 {
        didSet {
            middleButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, imageTitleSpacing)
            middleButton.titleEdgeInsets = UIEdgeInsetsMake(0, imageTitleSpacing, 0, 0)
        }
    }
    
    @IBInspectable var textColor:UIColor = UIColor.gray {
        didSet {
            middleButton.setTitleColor(textColor, for: .normal)
        }
    }
    
    
    private var leftButton:UIButton?
    private var rightButton:UIButton?
    public let middleButton = UIButton(type: .system)
    
    var contentHorizontalAlignment:UIControlContentHorizontalAlignment = .left {
        didSet {
            middleButton.contentHorizontalAlignment = contentHorizontalAlignment
        }
    }
    
    var bottomBorder:CALayer?
    
    @IBInspectable var showBottomBorder:Bool = true {
        didSet {
            bottomBorder?.isHidden = !showBottomBorder
        }
    }
    
    private func setup() {
        self.isUserInteractionEnabled = true
        formatter.dateStyle = .short
        middleButton.setTitle(formatter.string(from: currentDate).capitalized, for: .normal)
        middleButton.addTarget(self, action: #selector(showDatePicker), for: .touchUpInside)
        middleButton.setTitleColor(UIColor.darkGray, for: .normal)
        middleButton.titleLabel?.font = titleFont
        middleButton.setImage(dateImage, for: .normal)
        middleButton.setTitle(formatter.string(for: currentDate), for: .normal)
        middleButton.contentHorizontalAlignment = contentHorizontalAlignment
        addSubview(middleButton)
        bottomBorder = CALayer()
        bottomBorder?.frame = CGRect(x: 0.0, y: self.bounds.size.height-1.0, width: self.bounds.size.width, height: 1.0)
        bottomBorder?.backgroundColor = UIColor(hex: "D7D7D7").cgColor
        self.layer.addSublayer(bottomBorder!)
        if showArrow {
            leftButton = UIButton(type: .system)
            leftButton?.tintColor = UIColor.black
            leftButton?.addTarget(self, action: #selector(touchPreviousMonth), for: .touchUpInside)
            rightButton = UIButton(type: .system)
            rightButton?.tintColor = UIColor.black
            rightButton?.addTarget(self, action: #selector(touchNextMonth), for: .touchUpInside)
            rightButton?.setImage(rightArrowImage, for: .normal)
            leftButton?.setImage(leftArrowImage, for: .normal)
            leftButton?.frame = CGRect(x: 0, y: 0, width: 35, height: self.bounds.size.height)
            rightButton?.frame = CGRect(x: self.bounds.size.width - 35, y: 0, width: 35, height: self.bounds.size.height)
            middleButton.frame = CGRect(x: 35, y: 0, width: self.bounds.size.width - 70, height: self.bounds.size.height)
            self.addSubview(leftButton!)
            self.addSubview(rightButton!)
        } else {
            middleButton.frame = CGRect(x:0, y:0, width:self.bounds.size.width, height:self.bounds.size.height)
        }
    }
    
    override init(frame:CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        setup()
    }
    
    @objc
    private func showDatePicker() {
        delegate?.willShow?(self)
        let datePicker = MDDatePickerDialog()
        datePicker.delegate = self
        if let minDate = minimumDate {
            datePicker.minimumDate = minDate
        }
        if let maxDate = maximumDate {
            datePicker.maximumDate = maxDate
        }
 //       datePicker.selectedDate = currentDate
        datePicker.show()
    }
    
    @objc
    private func touchPreviousMonth() {
        var dateComponents = DateComponents()
        dateComponents.month = -1
        let previousDate = Calendar.current.date(byAdding: dateComponents, to: currentDate)!
        if let minDate = minimumDate {
            if Calendar.current.compare(previousDate, to: minDate, toGranularity: .day) == .orderedAscending {
                currentDate = minDate
                delegate?.change?(self, toDate: currentDate)
                return
            }
        }
        currentDate = previousDate
        delegate?.change?(self, toDate: currentDate)
    }
    
    @objc
    private func touchNextMonth() {
        var dateComponents = DateComponents()
        dateComponents.month = 1
        let nextDate = Calendar.current.date(byAdding: dateComponents, to: currentDate)!
        if let maxDate = maximumDate {
            if Calendar.current.compare(nextDate, to: maxDate, toGranularity: .day) == .orderedDescending {
                currentDate = maxDate
                delegate?.change?(self, toDate: currentDate)
                return
            }
        }
        currentDate = Calendar.current.date(byAdding: dateComponents, to: currentDate)!
        delegate?.change?(self, toDate: currentDate)
    }
    
    func datePickerDialogDidSelect(_ date: Date) {
        if conditionDate != nil{
            if let dayNumberOfWeek = date.dayNumberOfWeek(){
                var convertStr = String(dayNumberOfWeek)
                if dayNumberOfWeek == 7{
                    convertStr = "0"
                }
                if (conditionDate?.contains(convertStr))!{
                    currentDate = date
                    delegate?.change?(self, toDate: date)
                }else{
                    if let modified = conditionDate?.replace(target: "0", withString: "7").replace(target: "1", withString: "Chủ nhật"){
                        Toast(text: "Gói khám chỉ được đặt vào các ngày \(modified). Vui lòng chọn đúng ngày để đặt lịch").show()
                        showDatePicker()
                    }
                }
            }
        }else{
            currentDate = date
            delegate?.change?(self, toDate: date)
        }
    }
    
    deinit {
        print("deinit datepicker")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if showArrow {
            if leftButton == nil {
                leftButton = UIButton(type: .system)
                leftButton?.tintColor = UIColor.black
                leftButton?.addTarget(self, action: #selector(touchPreviousMonth), for: .touchUpInside)
                self.addSubview(leftButton!)
            }
            if rightButton == nil {
                rightButton = UIButton(type: .system)
                rightButton?.tintColor = UIColor.black
                rightButton?.addTarget(self, action: #selector(touchNextMonth), for: .touchUpInside)
                self.addSubview(rightButton!)
            }
            leftButton?.frame = CGRect(x: 0, y: 0, width: 35, height: self.bounds.size.height)
            rightButton?.frame = CGRect(x: self.bounds.size.width - 35, y: 0, width: 35, height: self.bounds.size.height)
            middleButton.frame = CGRect(x: 35, y: 0, width: self.bounds.size.width - 70, height: self.bounds.size.height)
            leftButton?.setImage(leftArrowImage, for: .normal)
            rightButton?.setImage(rightArrowImage, for: .normal)
            if let minimumD = minimumDate {
                let currentMonth = Calendar.current.component(.month, from: currentDate)
                let minimumMonth = Calendar.current.component(.month, from: minimumD)
                let currentYear = Calendar.current.component(.year, from: currentDate)
                let minimumYear = Calendar.current.component(.year, from: minimumD)
                if currentMonth == minimumMonth && minimumYear == currentYear {
                    leftButton?.isEnabled = false
                    //                    leftButton?.isHidden = true
                } else {
                    leftButton?.isEnabled = true
                    //                    leftButton?.isHidden = false
                }
            }
            if let maximumD = minimumDate {
                let currentMonth = Calendar.current.component(.month, from: currentDate)
                let maximumMonth = Calendar.current.component(.month, from: maximumD)
                let currentYear = Calendar.current.component(.year, from: currentDate)
                let maximumYear = Calendar.current.component(.year, from: maximumD)
                if currentMonth == maximumMonth && currentYear == maximumYear {
                    rightButton?.isEnabled = false
                    //                    leftButton?.isHidden = true
                } else {
                    rightButton?.isEnabled = true
                    //                    leftButton?.isHidden = false
                }
            }
        } else {
            middleButton.frame = CGRect(x:0, y:0, width:self.bounds.size.width, height:self.bounds.size.height)
            leftButton?.removeFromSuperview()
            leftButton = nil
            rightButton?.removeFromSuperview()
            rightButton = nil
        }
        bottomBorder?.frame = CGRect(x: 0.0, y: self.bounds.size.height-1.0, width: self.bounds.size.width, height: 1.0)
    }
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
