//
//  API.swift
//  iCNM
//
//  Created by Medlatec on 5/11/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import RealmSwift

public class API {
    public static let baseURL: String = "http://icnmservice.icnm.vn/"
    public static let baseURLImage:String = "http://imgservice.icnm.vn/"
    public static let iCNMImage:String = "iCNMImage/"
    public static let serviceImage:String = "ServiceImage/"
    public static let organizeImage:String = "OrganizeImage/"
    public static let baseURLHelp:String = "http://icnmservice.icnm.vn/html/find_ios.html"
    public static let baseURL_VNPay:String = "http://icnm.vn/VnPAY/Default.aspx?"
    private static let alamofireManager:SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        return Alamofire.SessionManager(configuration: configuration)
    }()
    
    public static var authorizationCode = ""
    public static var registrationToken = ""
    
    private static let contentType =  "application/json"
    
    public enum Endpoints {
        //  Đăng nhập
        case Login([String:Any])
        //  Đăng nhập, đăng ký qua facebook và gmail
        case GetAllUserByEmail([String:Any])
        //  Đăng ký
        case Register([String:Any])
        //  Xem thông tin bác sĩ
        case GetDoctorInformation(Int)
        //  Cập nhật thông tin bác sĩ
        case UpdateUserDoctor([String:Any])
        //  Cập nhật ảnh đại diện
        case UpdateAvatar([String:Any])
        //Cập nhật ảnh đại diện hssk
        case UpdateAvatarHSSK([String:Any])
        //  Lấy thông tin user theo số điện thoại
        case GetUserById(Int)
        case GetUserByPhone(String)
        //  Gửi mã xác thực
        case SendMessage([String:Any])
        //  Lấy thông tin chuyên khoa phục vụ chỉnh sửa thông tin bác sĩ
        case GetAllSpecialist
        //  Lấy tất cả tỉnh thành
        case GetProvinces
        //  Lấy thông tin quận huyện theo tỉnh thành
        case GetDistrictByProvinceID(String)
        //  Lấy các câu hỏi đáp
        case GetQuestionAnswer([String:Any])
        //  Lấy các bình luận của hỏi đáp
        case GetCommentQuestion(Int)
        //  Thêm hoặc sửa bình luận
        case AddUpdateCommentQuestion([String:Any])
        //  Xoá bình luận
        case RemoveCommentQuestion([String:Any])
        //  Thao tác liên quan đến cảm ơn, thích, chia sẻ
        case QAThankLikeShare([String:Any])
        //  Gửi câu hỏi
        case CreateQuestion([String:Any])
        //  Upload ảnh câu hỏi
        case UploadQuestionImage([String:Any])
        //  Lấy danh sách cơ sở
        case GetAllOrganize
        // Lấy danh sách cơ sở - Dat lich
        case GetAllOrganizedSchedule
        //  Lấy danh sách cơ sở theo chuyên khoa
        case getAllOrganizeFollowSpecialistID(Int)
        //  Lấy danh sách các khung giờ của một cơ sở
        case GetOrganizeScheduleTime(Int)
        //  Đặt lịch
        case BookAnAppointment([String:Any])
        // Xoá lich
        case BookAnAppointmentDoctor([String:Any])
        //  Lấy các đặt lịch cuả tài khoản
        case GetSchedule(String)
        // xoá lịch
        case DeleteSchedule(Int)
        //  Lấy tin tức nổi bật từ trang chủ
        case getFeaturedNews
        //  Lấy tin bài liên quan trong chức năng thông báo tái khám
        case getRelatedNewsTK(String)
        //Lấy các câu hỏi đáp
        case getAnswerQuestion
        //Lấy thông tin bác sỹ nổi bật từ trang chủ
        case getFeaturedDoctor
        // Lấy tất cả các phòng khám
        case getAllLinkClinics
        //Lấy thông tin đơn vị y tế
        case getMedicalUnit
        //Get thông tin lich theo ngày của bs
        case getListScheduleDoctorOfDay(Int, Int)
        // update thông tin mã bệnh nhân, mật khẩu
        case checkUpdatePID(String)
        //check thông tin mã bệnh nhân, mật khẩu
        case checkPatientResult(String)
        //get danh sách thông tin tra cứu mã bệnh nhân theo PID, SID
        case getListLookUpResultByPID(String, String)
        case getListDetailLookUpResultBySID(String, String)
        // get danh sách thông tin tra cứu mã bệnh nhân theo Phone
        case getListDetailLookUpResultByPhone(String)
        //get danh sách thông tin tra cứu mã bệnh nhân theo Đơn vị
        case getListLookUpResultUnit(String, String, String, String)
        case getDataChartDetailLookUpByPID([String:Any])
        // get thông tin từ điển xét nghiệm
        case getDictMedical
        //check thông tin tra cứu mã bệnh nhân theo Đơn vị
        case getLookUpUnit([String:Any])
        // Lấy danh mục nhóm bệnh
        case getDictDiseaseGroups
        // Lấy tất cả nhóm triệu chứng theo cơ thể
        case getMedicalLocationBody
        // Lấy chi tiết 1 từ điển bệnh
        case getMedicalDictionaryDetail(Int)
        // Lấy chi tiết triệu chứng
        case getMedicalSymptonByLocationBody(Int)
        // Lấy chi tiết triệu chứng
        case getMedicalSymptonByID(Int)
        //Lấy tất cả danh sách bệnh trong một nhóm bệnh
        case getMedicalDetailsByGroups(Int)
        //Lấy danh sách MedicalProfile
        case getMedicalProfileByUserID(Int)
        // Xóa 1 profile
        case addEditUserMedicalProfiles([String: Any])
        // add/edit user medical profiles
        case addEditMedicalHealthFactory([String: Any])
        // Lấy tất cả các chuyên khoa của dịch vụ
        case getSpecialistServices
        //Lấy danh sách dịch vụ theo chuyên khoa
        case getSpecialistServicesGroup(Int)
        //Get object MedicalHealthFactory
        case getMedicalHealthFactory(Int)
        //get vacxin tre em
        case getVaccinTreEm(Int)
        //get vacxin TCMR
        case getVaccinTCMR(Int)
        //get vacxin Uon van
        case getVaccinUonVan(Int)
        //khám lam san
        case getCilinical(Int)
        // Thêm mới 1 mũi tiêm
        case addEditMedicalVaccin([String: Any])
        // Thêm lần khám Lam sàng và cận lâm sàng
        case addEditMedicalCilinnical([String: Any])
        // Lấy thông báo riêng của mỗi user
        case getAllNotificationUser(Int, Int)
        // Lấy các thông báo chung ứng dụng
        case getAllNotificationGeneral
        // Đăng ký mã GCM và update UserID vào mã GCM
        case addFCM([String:Any])
        
        //get answerID from questionID
        case getAnswerIDFromQuestionID(Int)
        //Tìm kiếm
        case getSearchSuggest([String: Any])
        case getSuggestSearch([String: Any])
        case getSearch([String: Any])
        
        case userAddReview([String:Any])
        
        //News
        // get news of category by page
        case getNewsOfCategory(String)
        // get related new
        case getRelatedNews(String)
        //get news content
        case getNewsDetail(String)
        // get comment of news
        case getListCommentOfNews(String)
        // post new comment
        case addNewsComment([String: Any])
        // post like
        case postLikeNews(String)
        // get like
        case getLikeNews(String)
        //get all news
        case getNewsOfAllCategory(String)
        
        //get all categories
        case allNewsCategoriesOfUser(String)
        // post user category
        case postUserCategory([String:Any])
        // Lấy thông tin người dùng
        case getUserDoctorByID(Int, Int)
        // Theo doi/Bo theo doi
        case getUserFollowAction(Int, Int, Int)
        //Thêm hoặc chỉnh sửa Rating
        case addEditRatingUserDoctor([String: Any])
        //Đánh giá nhân viên lấy mẫu
        case customRatingEmployee([String: Any])
        // gửi 1 góp ý
        case sendFeedBack([String:Any])
        // send Charity
        case sendCharity([String:Any])
        // get all progress Manager
        case getAllProcessManager(Int)
        //get banner Charity
        case getBannerCharity(Int)
        //get banner
        case getAllBanner
        // get meaning by code
        case getMeaningByCode(String)
        //Lấy danh sách các dịch vụ
        case getAllServiceGroups
        //Lấy chi tiết trong nhóm dịch vụ
        case getServiceByGroups(Int)
        //Lấy chi tiết dịch vụ / gói khám
        case getAllServiceDetailGroups(Int)
        //Đặt lịch dịch vụ
        case sendRegisterSchedule([String:Any])
        //Đặt lịch dịch vụ PK
        case sendRegisterSchedulePK([String:Any])
        // get CategoryName
        case getCategoryByNewsID(Int)
        // Lọc dịch vụ/Gói khám
        case getFindService(Int, Int, Int, Int)
        // Get câu hỏi của tôi
        case getMyQuestions(Int, Int)
        // Lấy thời gian hiện tại từ server
        case getCurrentDateTime
        // Lấy câu hỏi đang chờ nhận trả lời trong
        //chuyên khoa bác sĩ đăng ký nhận câu hỏi
        case getQuestionNoAnswerForDoctor(Int, Int)
        //Lấy danh sách câu hỏi đã nhận câu trả lời
        //Đang chờ trả lời
        case getQuestionRecivedForDoctor(Int, Int)
        //Lấy câu hỏi đa trả lời của bác sĩ
        case getQuestionAnswerForDoctor(Int, Int)
        //Đăng ký nhận câu hỏi
        case registerAnswerQuestion(Int, Int)
        //post câu trả lời
        case postAnswerQuestion([String:Any])
        //rating answer
        case postRatingAnswer([String:Any])
        //Đổi mật khẩu
        case updatePassword([String:Any])
        // check password
        case checkPassword(Int, String)
        // getServey
        case getServey(Int)
        // tracking newsID
        case sendTrackingNewsID([String: Any])
        // get list doctor follow specialist
        case getDoctorFollowSpecialist(Int, Int, String)
        
        //get all servey new follow
        case getAllServey(Int, Int)
        
        //send servey answer
        case SendServeyAnswer([String: Any])
        //
        case getLocationOwn(Int)
        //
        case getLocationInfo([String: Any])
        // update location
        case updateLocation([String: Any])
        // update avatar location
        case updateAvatarLocation([String: Any])
        // add and update list image
        case addUpdateListImageLocation([String: Any])
        // edit and delete list image
        case addUpdateImageLocation([String: Any])
        //dang ky chuyen khoa nhan cau tra loi
        case registerAnswer(Int, String)
        // get tag by UserID
        case getUserTagByUserIDFix(Int)
        // post user tag
        case updateUserTags([String: Any])
        // get ads
        case getResultAds(Int)
        // check tracking
        case checkTrackingFeature([String: Any])
        // check record install
        case checkRecordInstallTN([String: Any])
        // get record install
        case getRecordInstallTN(String)
        // login doctor unit
        case doLoginDoctorUnit([String: Any])
        // get doctorID Unit
        case getDoctorIDUnit([String: Any])
        // get version
        case getVersionApp(Int)
        // get OrganizeID
        case getOrganizeByID(Int)
        // get service rating
        case getAllServiceRating(Int, Int)
        // post service rating
        case postServiceRating([String: Any])
        // put service rating
        case putServiceRating([String: Any])
        // get list more doctor
        case getDoctorRatedHomeFixByPage(Int)
        case sendNotificationSuccessService(Int, Int, String)
        // get test point medical
        case getTestMedicalSchedule()
        //thống kê lượt đọc 1 bài viết
        case postUpdateRead(Int)
        // gửi thông tin code number
        case sendPromotionalCreditCard(String)
        // update status read notification
        case updateStatusReadNotification(Int, Int)
        // show list hồ sơ sức khoẻ
        case getListUserProfile(Int)
        // get all result type for hssk
        case getAllResultType()
        // add patient result
        case addPatientResult([String:Any])
        case getPatientUpload([String:Any])
        case getMedicalProfileOneUser(Int, Int)
        
        // post data to VNPay
        case postVNPayment(Int, String, String, String, String, String, String, String, String, String,  String, String, String, Int, String, String)
        
        public var method: Alamofire.HTTPMethod {
            switch self {
            case .Login:
                return .post
            case .Register:
                return .post
            case .UpdateUserDoctor:
                return .post
            case .UpdateAvatar:
                return .post
            case .UpdateAvatarHSSK:
                return .post
            case .SendMessage:
                return .post
            case .AddUpdateCommentQuestion:
                return .post
            case .RemoveCommentQuestion:
                return .post
            case .getLookUpUnit:
                return .post
            case .getDataChartDetailLookUpByPID:
                return .post
            case .CreateQuestion:
                return .post
            case .UploadQuestionImage:
                return .post
            case .BookAnAppointment:
                return .post
            case .BookAnAppointmentDoctor:
                return .post
            case .userAddReview:
                return .post
            case .getSearchSuggest:
                return .get
            case .getSuggestSearch:
                return .get
            case .addNewsComment:
                return .post
            case .addEditUserMedicalProfiles:
                return .post
            case .addEditMedicalHealthFactory:
                return .post
            case .addFCM:
                return .post
            // post like
            case .postLikeNews:
                return .post
            case .addEditMedicalVaccin:
                return .post
            case .addEditMedicalCilinnical:
                return .post
            case .postUserCategory:
                return .post
            case .addEditRatingUserDoctor:
                return .post
            case .customRatingEmployee:
                return .post
            case .sendFeedBack:
                return .post
            case .sendCharity:
                return .post
            case .sendRegisterSchedule:
                return .post
            case .sendRegisterSchedulePK:
                return .post
            case .postAnswerQuestion:
                return .post
            case .postRatingAnswer:
                return .post
            case .updatePassword:
                return .put
            case .SendServeyAnswer:
                return .post
            case .sendTrackingNewsID:
                return .post
            case .updateLocation:
                return .post
            case .updateAvatarLocation:
                return .post
            case .addUpdateListImageLocation:
                return .post
            case .addUpdateImageLocation:
                return .post
            case .updateUserTags:
                return .post
            case .checkTrackingFeature:
                return .post
            case .checkRecordInstallTN:
                return .post
            case .doLoginDoctorUnit:
                return .post
            case .getDoctorIDUnit:
                return .post
            case .postServiceRating:
                return .post
            case .putServiceRating:
                return .put
            case .addPatientResult:
                return .post
            case .getPatientUpload:
                return .post
            default:
                return .get
            }
        }
        
        public var path:String {
            switch self {
            case .Login:
                return baseURL + "api/User/UserLogin"
            case .GetAllUserByEmail:
                return baseURLImage + "api/User/GetAllUserByEmail"
            case .Register:
                return baseURL + "api/User/InSertUser"
            case .GetDoctorInformation:
                return baseURL + "api/Doctor/GetInfomationDoctor"
            case .UpdateUserDoctor:
                return baseURL + "api/User/UpdateUserDoctor"
            case .UpdateAvatar:
                return baseURLImage + "api/User/UpdateAvatar"
            case .UpdateAvatarHSSK:
                return baseURLImage + "api/UserMedicalProfile/UpdateAvatar"
            case .GetUserById:
                return baseURL + "api/User/GetUser"
            case .GetUserByPhone:
                return baseURL + "api/User/GetUserByPhone"
            case .SendMessage:
                return baseURL + "api/User/SendMessage"
            case .GetAllSpecialist:
                return baseURL + "api/Specialist/Get"
            case .GetProvinces:
                return baseURL + "api/Province/Get"
            case .GetDistrictByProvinceID:
                return baseURL + "api/District/GetByProvinceID/"
            case .GetQuestionAnswer:
                return baseURL + "api/Question/getQuestionByPageJoin"
            case .GetCommentQuestion:
                return baseURL + "api/Question/getCommentQuestion"
            case .AddUpdateCommentQuestion:
                return baseURL + "api/Question/addUpdateCommentQuestion"
            case .RemoveCommentQuestion:
                return baseURL + "api/Question/RemoveCommentQuestion"
            case .QAThankLikeShare:
                return baseURL + "api/Answer/ThankLikeShareAQ"
            case .CreateQuestion:
                return baseURL + "api/Questions/PostQuestion"
            case .UploadQuestionImage:
                return baseURLImage + "api/Question/AskQuestionImageFix"
            case .GetAllOrganizedSchedule:
                return baseURL + "api/Organize/GetOrganizedSchedule"
            case .GetAllOrganize:
                return baseURL + "api/Organize"
            case .getAllOrganizeFollowSpecialistID(let specialistID):
                return baseURL + "api/Organize/GetBySpecID/\(specialistID)"
            case .GetOrganizeScheduleTime(let organizeID):
                return baseURL + "api/ScheduleTime/OrganizeID/\(organizeID)"
            case .BookAnAppointment:
                //return baseURL + "api/Schedule/InsertScheduleFixDichVu"
                return baseURL + "api/Schedule/InsertScheduleFixTienTrinh"
            case .BookAnAppointmentDoctor:
                return baseURL + "api/Schedule/InsertScheduleFixDoctorNew"
            case .GetSchedule(let parameterString):
                return baseURL + "api/Schedule/AllSchedule/" + parameterString
            case .DeleteSchedule(let scheduleID):
                return baseURL + "api/Schedule/HuyLich/\(scheduleID)"
            case .getFeaturedNews:
                return baseURL + "api/News/GetRatedNewsCategories"
            case .getRelatedNewsTK(let parameterString):
                return baseURL + "api/News/GetNewTaiKham?listID=" + parameterString
            case .getAnswerQuestion:
                return baseURL + "api/Answer/GetAnswerQuestionRated"
            case .getFeaturedDoctor:
                return baseURL + "api/Doctor/GetDoctorRatedHomeFix"
            case .getAllLinkClinics:
                return baseURL + "api/Organize/GetALlSerDiscount"
            case .getMedicalUnit:
                return baseURL + "api/Organize/GetOrganizeResult"
            case .getListScheduleDoctorOfDay(let pageNumber, let doctorID):
                return baseURL + "api/Schedule/GetAllScheduleDoctor/\(pageNumber),\(doctorID)"
            case .checkUpdatePID(let parameterString):
                return baseURL + "/api/AllNews/GetUserPassFix/" + parameterString
            case .checkPatientResult(let parameterString):
                return baseURL + "/api/AllNews/GetUserPass/" + parameterString
            case .getListLookUpResultByPID:
                return baseURL + "/api/Patient/GetResultByPIDFixFinal"
            case .getListDetailLookUpResultBySID:
                return baseURL + "/api/Patient/GetResultBySIDFixFinal"
            case .getListDetailLookUpResultByPhone(let phone):
                return baseURL + "/api/Patient/GetResultByPhone?Phone=\(phone)"
            case .getListLookUpResultUnit:
                return baseURL + "api/Patient/GetPatientOfDoctorKSK"
            case .getLookUpUnit:
                return baseURL + "/api/DMBS/LoginDoctorUnitFix"
            case .getDataChartDetailLookUpByPID:
                return baseURL + "/api/Patient/GetChart"
            case .userAddReview:
                return baseURL + "api/ratings/rating"
            case .getDictMedical:
                return baseURL + "/api/Meaning"
            case .getSuggestSearch:
                return baseURL + "api/Search/getSuggestSearch"
            case .getSearchSuggest:
                return baseURL + "api/Search/GetSearchSuggest"
            case .getDictDiseaseGroups:
                return baseURL + "api/MedicalDictionary/getMedicalDictionGroups"
            case .getMedicalLocationBody:
                return baseURL + "api/MedicalDictionary/getMedicalLocationBody"
            case .getMedicalDictionaryDetail(let medicalDicID):
                return baseURL + "api/MedicalDictionary/Get/\(medicalDicID)"
            case .getMedicalSymptonByLocationBody(let locationBodyID):
                return baseURL + "api/MedicalDictionary/getMedicalSymptonByLocationBody?locationBodyID=\(locationBodyID)&PageNumber=1"
            case .getMedicalSymptonByID(let symptonID):
                return baseURL + "api/MedicalDictionary/getMedicalSymptonByID?symptonID=\(symptonID)"
            case .getMedicalDetailsByGroups(let diseaseGroupID):
                return baseURL + "api/MedicalDictionary/getMedicalDetailsByGroups?DiseaseGroupID=\(diseaseGroupID)&PageNumber=1"
            case .getMedicalProfileByUserID(let userID):
                return baseURL + "api/UserMedicalProfile/GetMedicalProfileByUserID?userID=\(userID)"
            case .addEditUserMedicalProfiles:
                return baseURL + "api/UserMedicalProfile/AddEditUserMedicalProfiles"
            case .addEditMedicalHealthFactory:
                return baseURL + "api/UserMedicalProfile/AddEditMedicalHealthFactory"
            case .addEditMedicalVaccin:
                return baseURL + "api/UserMedicalProfile/AddEditMedicalVaccin"
            case .addEditMedicalCilinnical:
                return baseURL + "api/UserMedicalProfile/AddEditMedicalCilinical"
            case .getAllNotificationUser(let userID, let doctorID):
                return baseURL + "api/Notification/GetNotificationUserFix?userID=\(userID)&doctorID=\(doctorID)&deviceTypeID=2"
            case .getAllNotificationGeneral:
                return baseURL + "api/Notification/Get?deviceTypeID=2"
            case .getSpecialistServices:
                return baseURL + "api/Specialist/Get"
            case .getSpecialistServicesGroup(let specialistID):
                return baseURL + "api/ServiceMeaning/GetServiceGroups?specialistID=\(specialistID)"
            case .getMedicalHealthFactory(let factoryID):
                return baseURL + "api/UserMedicalProfile/GetMedicalHealthFactory?id=\(factoryID)"
            case .getVaccinTreEm(let factoryID):
                return baseURL + "api/UserMedicalProfile/GetMedicalVaccin?id=\(factoryID)&typeID=1"
            case .getVaccinTCMR(let factoryID):
                return baseURL + "api/UserMedicalProfile/GetMedicalVaccin?id=\(factoryID)&typeID=2"
            case .getVaccinUonVan(let factoryID):
                return baseURL + "api/UserMedicalProfile/GetMedicalVaccin?id=\(factoryID)&typeID=3"
            case .getCilinical(let medicalProfileID):
                return baseURL + "api/UserMedicalProfile/GetMedicalCilinnical?id=\(medicalProfileID)"
            case .getNewsOfCategory(let stringParam):
                return baseURL + "api/AllNews/" + stringParam
            case .getRelatedNews(let stringParam):
                return baseURL + "api/AllNews/RelatedNewID/" + stringParam
            case .getNewsDetail(let stringParam):
                return baseURL + "api/AllNews/" + stringParam
            case .getListCommentOfNews(let stringParam):
                return baseURL + "api/newsComment/" + stringParam
            case .addNewsComment(let stringParam):
                let newBaseUrl = baseURL + "/api/NewsComment/PostComment"
                return newBaseUrl.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
            case .getNewsOfAllCategory(let stringParam):
                return baseURL + "api/AllNews/FixGetNews/" + stringParam
            case .postLikeNews(let stringParam):
                return baseURL + "api/NewsComment/Status/" + stringParam
            case .getLikeNews(let stringParam):
                return baseURL + "api/NewsComment/Status/" + stringParam
            case .allNewsCategoriesOfUser(let stringParam):
                return baseURL + "api/UserCategory/" + stringParam
            case .getSearch:
                return baseURL + "api/Search/Find"
            case .postUserCategory:
                return baseURL + "api/UserCategory/PostUserCategory"
            case .addFCM:
                return baseURL + "api/Notification/UpdateFCMFix"
            case .getAnswerIDFromQuestionID(let questionID):
                return baseURL + "api/Answer/GetAnswerQuestionDetails?questionID=\(questionID)"
            case .getUserDoctorByID(let userID, let userViewID):
                return baseURL + "api/User/GetUserDoctorByID?userID=\(userID)&userViewID=\(userViewID)"
            case .getUserFollowAction(let userID, let userIDFollow, let follow):
                return baseURL + "api/user/UserFollowAction?userID=\(userID)&userIDFllow=\(userIDFollow)&follow=\(follow)"
            case .addEditRatingUserDoctor:
                return baseURL + "api/User/addEditRatingUserDoctor"
            case .customRatingEmployee:
                return baseURL + "api/Process/Rating/InsertRating"
            case .sendFeedBack:
                return baseURL + "api/Feedback/Insert"
            case .sendCharity:
                return baseURL + "api/NServey/InsertCharity"
            case .getAllProcessManager(let scheduleID):
                return baseURL + "api/Process/ScheduleID/\(scheduleID)"
            case .getAllBanner:
                return baseURL + "api/Banner/getBanner"
            case .getBannerCharity(let userID):
                return baseURL + "api/NServey/GetAllCharityImage/\(userID)"
            case .getMeaningByCode(let testCode):
                return baseURL + "api/Meaning/GetMeaningByCode?TestCode=\(testCode)"
            case .getAllServiceGroups:
                return baseURL + "api/Service/GetAllServiceGroups"
            case .getServiceByGroups(let serviceGroupID):
                return baseURL + "api/Service/GetServiceByGroups?serviceGroupID=\(serviceGroupID)"
            case .getAllServiceDetailGroups(let serviceID):
                return baseURL + "api/Service/getAllServiceDetail?ServiceID=\(serviceID)"
            case .sendRegisterSchedule:
                return baseURL + "api/GoiKhamTD/InsertGoiKhamAllCondi"
            case .sendRegisterSchedulePK:
                return baseURL + "api/GoiKhamTD/InsertGoiKhamPK"
            case .getCategoryByNewsID(let categoryID):
                return baseURL + "api/CategoryNews/CategoryID/\(categoryID)"
            case .getFindService(let provinceID, let gender, let age, let price):
                return baseURL + "api/Service/GetFindServiceFix?ProvinceID=\(provinceID)&Gender=\(gender)&Age=\(age)&Price=\(price)"
            case .getMyQuestions(let pageID, let userID):
                return baseURL + "api/Questions/User/\(pageID),\(userID)"
            case .getCurrentDateTime:
                return baseURL + "api/Question/getCurrentDateTime"
            case .getQuestionNoAnswerForDoctor(let doctorID, let pageNumber):
                return baseURL + "api/Question/GetQuestionNoAnswerForDoctor?doctorID=\(doctorID)&pageNumber=\(pageNumber)"
            case .getDoctorRatedHomeFixByPage(let pageNumber):
                return baseURL + "api/Doctor/GetDoctorRatedHomeFixByPage?pageNumber=\(pageNumber)"
            case .getQuestionRecivedForDoctor(let doctorID, let pageNumber):
                return baseURL + "api/Question/GetQuestionRecivedForDoctor?doctorID=\(doctorID)&pageNumber=\(pageNumber)"
            case .getQuestionAnswerForDoctor(let doctorID, let pageNumber):
                return baseURL + "api/Question/GetQuestionAnswerForDoctor?doctorID=\(doctorID)&pageNumber=\(pageNumber)"
            case .registerAnswerQuestion(let questionID, let doctorID):
                return baseURL + "api/Question/RegisterAnswerQuestion?questionID=\(questionID)&doctorID=\(doctorID)"
            case .postAnswerQuestion:
                return baseURL + "api/Answer/AnswerQuestionFix"
            case .postRatingAnswer:
                return baseURL + "api/AnswerRating/InsertRate"
            case .updatePassword:
                return baseURL + "api/User/UpdatePassword"
            case .checkPassword(let userID, let oldPassword):
                return baseURL + "api/User/CheckPassword?userID=\(userID)&oldpass=\(oldPassword)"
            case .getServey(let serveyID):
                return baseURL + "api/Servey/GetServey?serveyID=\(serveyID)"
            case .getDoctorFollowSpecialist(let organizeID, let specialistID, let currentDate):
                return baseURL + "api/Schedule/GetLichBySpecIDAnd/\(organizeID),\(specialistID),\(currentDate)"
            case .getAllServey(let userID, let serveyQuestionID):
                return baseURL + "api/NServey/GetAllNServey/\(userID),\(serveyQuestionID)"
            case .SendServeyAnswer:
                return baseURL + "api/NServey/InsertServey"
            case .sendTrackingNewsID:
                return baseURL + "api/Process/TrackingNew/InsertTracking"
            case .getLocationOwn(let DoctorID):
                return baseURL + "api/locations/LocationOwn/\(DoctorID)"
            case .getLocationInfo:
                return baseURL + "api/LocationChange/Get"
            case .updateLocation:
                return baseURL + "api/LocationChange/AddEditLocation"
            case .updateAvatarLocation:
                return baseURLImage + "api/LocationChange/UpdateAvatar"
            case .addUpdateListImageLocation:
                return baseURLImage + "api/LocationChange/AddUpdateImageList"
            case .addUpdateImageLocation:
                return baseURLImage + "api/LocationChange/AddUpdateImage"
            case .registerAnswer(let doctorID, let specialistStr):
                return baseURL + "api/Doctor/RegisterAnswer?doctorID=\(doctorID)&specialistStr=\(specialistStr)"
            case .getUserTagByUserIDFix(let userID):
                return baseURL + "api/KeyWorld/GetAllKey/\(userID)"
            case .updateUserTags:
                return baseURL + "api/KeyWorld/PostKeyUserNew"
            case .getResultAds(let typeID):
                return baseURL + "api/Ads/getResultAdsFix?typeID=\(typeID)"
            case .checkTrackingFeature:
                return baseURL + "api/Track/TrackingFeatFix"
            case .checkRecordInstallTN:
                return baseURL + "api/InstallTN/InsertTNFix"
            case .getRecordInstallTN(let phone):
                return baseURL + "api/InstallTN/\(phone)"
            case .doLoginDoctorUnit:
                return baseURL + "api/DMBS/InitUserDoctorToICNM"
            case .getDoctorIDUnit:
                return baseURL + "api/DMBS/LoginDoctorUnitFix"
            case .getVersionApp(let deviceType):
                return baseURL + "api/VersionApp/\(deviceType)"
            case .getAllServiceRating(let pageID, let serviceID):
                return baseURL + "api/ServiceRating/SerRatePaging/\(pageID),\(serviceID)"
            case .postServiceRating:
                return baseURL + "api/ServiceRating/rating"
            case .putServiceRating:
                return baseURL + "api/ServiceRating/updateRatingFix"
            case .sendNotificationSuccessService(let serviceID, let userID, let magenGoi):
                return baseURL + "api/Notification/SendNotifieSuccessService?serviceID=\(serviceID)&&userID=\(userID)&&Magengoi=\(magenGoi)"
            case .getTestMedicalSchedule:
                return baseURL + "api/Schedule/GetAllTestSchedule"
            case .postUpdateRead(let newsID):
                return baseURL + "api/AllNews/UpdateRead/\(newsID)"
            case .sendPromotionalCreditCard(let codeNumber):
                return baseURL + "api/GoiKhamCT/KichHoatThe/\(codeNumber)"
            case .updateStatusReadNotification(let notificationID, let userID):
                return baseURL + "api/Notification/UpdateStatus?NotificationID=\(notificationID)&UserID=\(userID)"
            case .getOrganizeByID(let serviceID):
                return baseURL + "api/Organize/GetByID/\(serviceID)"
            case .postVNPayment(let vnp_Amount, let vnp_BankCode, let vnp_Command, let vnp_CreateDate, let vnp_CurrCode, let vnp_IpAddr, let vnp_Locale, let vnp_Merchant, let vnp_OrderInfo, let vnp_OrderType, let vnp_ReturnUrl, let vnp_TmnCode, let vnp_TxnRef, let vnp_Version, let vnp_SecureHashType, let vnp_SecureHash):
                return baseURL_VNPay + "?vnp_Amount=\(vnp_Amount)&vnp_BankCode=\(vnp_BankCode)&vnp_Command=\(vnp_Command)&vnp_CreateDate=\(vnp_CreateDate)&vnp_CurrCode=\(vnp_CurrCode)&vnp_IpAddr=\(vnp_IpAddr)&vnp_Locale=\(vnp_Locale)&vnp_Merchant=\(vnp_Merchant)&vnp_OrderInfo=\(vnp_OrderInfo)&vnp_OrderType=\(vnp_OrderType)&vnp_ReturnUrl=\(vnp_ReturnUrl)&vnp_TmnCode=\(vnp_TmnCode)&vnp_TxnRef=\(vnp_TxnRef)&vnp_Version=\(vnp_Version)&vnp_SecureHashType=\(vnp_SecureHashType)&vnp_SecureHash=\(vnp_SecureHash)"
            case .getListUserProfile(let userId):
                return baseURL + "api/UserMedicalProfile/GetListUserProfile?UserID=\(userId)"
            case .getAllResultType:
                return baseURL + "api/PatientResultIcnm/GetAllResultType"
            case .addPatientResult:
                return baseURLImage + "api/PatientResultIcnm/AddResult"
            case .getPatientUpload:
                return baseURL + "api/Patient/GetPatientUpload"
            case .getMedicalProfileOneUser(let userID, let medicalProfileID):
                return baseURL + "api/UserMedicalProfile/GetMedicalProfileOneUser?userID=\(userID)&medicalProfileID=\(medicalProfileID)"
            }
        }
        
        public var parameters: [String : Any]? {
            switch self {
            case .Login(let paramDic):
                return paramDic
            case .GetAllUserByEmail(let paramDic):
                return paramDic
            case .GetDoctorInformation(let doctorID):
                return ["doctorID":doctorID]
            case .UpdateUserDoctor(let paramDic):
                return paramDic
            case .UpdateAvatar(let paramDic):
                return paramDic
            case .UpdateAvatarHSSK(let paramDic):
                return paramDic
            case .addPatientResult(let paramDict):
                return paramDict
            case .getPatientUpload(let paramDict):
                return paramDict
            case .GetUserById(let userID):
                return ["id":userID]
            case .GetUserByPhone(let phoneString):
                return ["phone":phoneString]
            case .SendMessage(let paramDic):
                return paramDic
            case .Register(let paramDic):
                return paramDic
            case .GetDistrictByProvinceID(let provinceID):
                return ["provinceID":provinceID.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)]
            case .GetQuestionAnswer(let paramDic):
                return paramDic
            case .GetCommentQuestion(let questionID):
                return ["questionID":questionID]
            case .BookAnAppointment(let paramDic):
                return paramDic
            case .BookAnAppointmentDoctor(let paramDic):
                return paramDic
            case .AddUpdateCommentQuestion(let paramDic):
                return paramDic
            case .RemoveCommentQuestion(let paramDic):
                return paramDic
            case .QAThankLikeShare(let paramDic):
                return paramDic
            case .CreateQuestion(let paramDic):
                return paramDic
            case .UploadQuestionImage(let paramDic):
                return paramDic
//            case .checkLookUpResult(let paramDic):
//                return paramDic
            case .getLookUpUnit(let paramDic):
                return paramDic
            case .getListLookUpResultByPID(let pid, let organizeID):
                return ["pid":pid, "organizeID":organizeID]
            case .getListLookUpResultUnit(let maBS, let currentMonth, let currentYear, let organizeID):
                return ["doctorID":maBS, "month": currentMonth, "year": currentYear, "organizeID":organizeID]
            case .getListDetailLookUpResultBySID(let sid, let organizeID):
                return ["sid":sid, "organizeID":organizeID]
            case .getDataChartDetailLookUpByPID(let paramDic):
                return paramDic
            case .addEditUserMedicalProfiles(let paramDic):
                return paramDic
            case .addEditMedicalHealthFactory(let paramDic):
                return paramDic
            case .userAddReview(let paramDic):
                return paramDic
            case .getSearchSuggest(let paramDic):
                return paramDic
            case .getSuggestSearch(let paramDic):
                return paramDic
            case .getSearch(let paramDic):
                return paramDic
            case .addEditMedicalVaccin(let paramDict):
                return paramDict
            case .addEditMedicalCilinnical(let paramDict):
                return paramDict
            case .postUserCategory(let paramDict):
                return paramDict
            case .addNewsComment(let paramDict):
                return paramDict
            case .addFCM(let paramDict):
                return paramDict
            case .addEditRatingUserDoctor(let paramDict):
                return paramDict
            case .customRatingEmployee(let paramDict):
                return paramDict
            case .sendFeedBack(let paramDict):
                return paramDict
            case .sendCharity(let paramDict):
                return paramDict
            case .sendRegisterSchedule(let paramDict):
                return paramDict
            case .sendRegisterSchedulePK(let paramDict):
                return paramDict
            case .postAnswerQuestion(let paramDict):
                return paramDict
            case .postRatingAnswer(let paramDict):
                return paramDict
            case .updatePassword(let paramDict):
                return paramDict
            case .SendServeyAnswer(let paramDic):
                return paramDic
            case .sendTrackingNewsID(let paramDic):
                return paramDic
            case .getLocationInfo(let paramDict):
                return paramDict
            case .updateLocation(let paramDict):
                return paramDict
            case .updateAvatarLocation(let paramDict):
                return paramDict
            case .addUpdateListImageLocation(let paramDict):
                return paramDict
            case .addUpdateImageLocation(let paramDict):
                return paramDict
            case .updateUserTags(let paramDict):
                return paramDict
            case .checkTrackingFeature(let paramDic):
                return paramDic
            case .checkRecordInstallTN(let paramDic):
                return paramDic
            case .doLoginDoctorUnit(let paramDict):
                return paramDict
            case .getDoctorIDUnit(let paramDict):
                return paramDict
            case .postServiceRating(let paramDict):
                return paramDict
            case .putServiceRating(let paramDict):
                return paramDict
            default:
                return nil
            }
        }
        
        public var paramaterEncoding: ParameterEncoding {
            switch self {
            case .Login:
                return JSONEncoding.default
            case .UpdateUserDoctor:
                return JSONEncoding.default
            case .UpdateAvatar:
                return JSONEncoding.default
            case .UpdateAvatarHSSK:
                return JSONEncoding.default
            case .addPatientResult:
                return JSONEncoding.default
            case .getPatientUpload:
                return JSONEncoding.default
            case .SendMessage:
                return JSONEncoding.default
            case .Register:
                return JSONEncoding.default
            case .AddUpdateCommentQuestion:
                return JSONEncoding.default
            case .RemoveCommentQuestion:
                return JSONEncoding.default
            case .CreateQuestion:
                return JSONEncoding.default
            case .UploadQuestionImage:
                return JSONEncoding.default
            case .getLookUpUnit:
                return JSONEncoding.default
            case .getDataChartDetailLookUpByPID:
                return JSONEncoding.default
            case .addEditUserMedicalProfiles:
                return JSONEncoding.default
            case .addEditMedicalHealthFactory:
                return JSONEncoding.default
            case .userAddReview:
                return JSONEncoding.default
            case .addEditMedicalVaccin:
                return JSONEncoding.default
            case .addEditMedicalCilinnical:
                return JSONEncoding.default
            case .postUserCategory:
                return JSONEncoding.default
            case .addNewsComment:
                return JSONEncoding.default
            case .BookAnAppointment:
                return JSONEncoding.default
            case .BookAnAppointmentDoctor:
                return JSONEncoding.default
            case .addFCM:
                return JSONEncoding.default
            case .addEditRatingUserDoctor:
                return JSONEncoding.default
            case .customRatingEmployee:
                return JSONEncoding.default
            case .sendFeedBack:
                return JSONEncoding.default
            case .sendCharity:
                return JSONEncoding.default
            case .sendRegisterSchedule:
                return JSONEncoding.default
            case .sendRegisterSchedulePK:
                return JSONEncoding.default
            case .postAnswerQuestion:
                return JSONEncoding.default
            case .postRatingAnswer:
                return JSONEncoding.default
            case .updatePassword:
                return JSONEncoding.default
            case .SendServeyAnswer:
                return JSONEncoding.default
            case .sendTrackingNewsID:
                return JSONEncoding.default
            case .updateAvatarLocation:
                return JSONEncoding.default
            case .addUpdateListImageLocation:
                return JSONEncoding.default
            case .addUpdateImageLocation:
                return JSONEncoding.default
            case .updateLocation:
                return JSONEncoding.default
            case .updateUserTags:
                return JSONEncoding.default
            case .checkTrackingFeature:
                return JSONEncoding.default
            case .checkRecordInstallTN:
                return JSONEncoding.default
            case .doLoginDoctorUnit:
                return JSONEncoding.default
            case .getDoctorIDUnit:
                return JSONEncoding.default
            case .postServiceRating:
                return JSONEncoding.default
            case .putServiceRating:
                return JSONEncoding.default
            case .postLikeNews:
                return JSONEncoding.default
            default:
                return URLEncoding.default
            }
        }
    }
    
    @discardableResult
    public static func requestString(
        endpoint: API.Endpoints,
        completionHandler: @escaping (DataResponse<String>) -> Void)
        -> Request {
            let headers:HTTPHeaders = [
                "Authorization":authorizationCode,
                "Content-Type":contentType,
                "Accept":contentType
            ]
            
            let request = alamofireManager.request(endpoint.path, method: endpoint.method, parameters: endpoint.parameters, encoding: endpoint.paramaterEncoding, headers: headers).validate(statusCode: 200..<300).responseString { (response: DataResponse<String>) in
                completionHandler(response)
            }
            return request
    }
    
    @discardableResult
    public static func requestObject <T: Object> (type: T.Type, endpoint: API.Endpoints, success:@escaping (T) -> Void,
                                                  fail:@escaping (_ error:Error, _ response:DataResponse<T>)->Void) -> Request where T:Mappable {
        let headers:HTTPHeaders = [
            "Authorization":authorizationCode,
            "Content-Type":contentType,
            "Accept":contentType
        ]
        
        let request = alamofireManager.request(endpoint.path, method: endpoint.method, parameters: endpoint.parameters, encoding: endpoint.paramaterEncoding, headers: headers).validate(statusCode: 200..<300).responseObject { (response:DataResponse<T>) in
            switch response.result {
            case .success(let item):
                autoreleasepool {
                    do {
                        let realm = try Realm()
                        try realm.write {
                            realm.add(item, update: true)
                        }
                    } catch let error {
                        fail(error, response)
                    }
                }
                success(item)
            case .failure(let error):
                fail(error, response)
                
            }
        }
        return request
    }
    
    @discardableResult
    public static func requestObjectNoRealm <T: Mappable> (type: T.Type, endpoint: API.Endpoints, success:@escaping (T) -> Void,
                                                           fail:@escaping (_ error:Error, _ response:DataResponse<T>)->Void) -> Request {
        let headers:HTTPHeaders = [
            "Authorization":authorizationCode,
            "Content-Type":contentType,
            "Accept":contentType
        ]
        
        let request = alamofireManager.request(endpoint.path, method: endpoint.method, parameters: endpoint.parameters, encoding: endpoint.paramaterEncoding, headers: headers).validate(statusCode: 200..<300).responseObject { (response:DataResponse<T>) in
            switch response.result {
            case .success(let item):
                success(item)
            case .failure(let error):
                fail(error, response)
                
            }
            }.responseString { (response) in
                //switch response.result {
                //case .success(let string):
                    //print(string)
                //case .failure(let error):
                    //print(error.localizedDescription)
               // }
        }
        return request
    }
    
    @discardableResult
    public static func requestJSON(endpoint:API.Endpoints, completionHandler: @escaping (DataResponse<Any>) -> Void) -> Request {
        let headers:HTTPHeaders = [
            "Authorization":authorizationCode,
            "Content-Type":contentType,
            "Accept":contentType
        ]
        let request = alamofireManager.request(endpoint.path, method: endpoint.method, parameters: endpoint.parameters, encoding: endpoint.paramaterEncoding, headers: headers).validate(statusCode: 200..<300).responseJSON { (response:DataResponse<Any>) in
            completionHandler(response)
            }.responseString { (response) in
                //switch response.result {
                //case .success(let string):
                    //print(string)
                //case .failure(let error):
                    //print(error.localizedDescription)
                //}
        }
        return request
    }
}

