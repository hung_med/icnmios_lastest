//
//  Utils.swift
//  iCNM
//
//  Created by Mac osx on 7/11/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import PopupDialog

public let monthAgo =  " tháng trước"
public let weekAgo =  " tuần trước"
public let daysAgo =  " ngày trước"
public let hoursAgo = " giờ trước"
public let minAgo =  " phút trước"
public let secAgo = " giây trước"
public let second = 1000 // milliseconds
public let minute = 60
public let hour = minute * 60
public let day = hour * 24
public let week = day * 7
public let month = day * 30
public let year = month * 12

func dateDifferenceFix(fromDate:Int, ms2:Int)->String
{
    var diff:Int = 0;
    diff = ms2 - fromDate;
    
    let diffInSec:Int = Swift.abs((Int) (diff / (second)))
    var difference = "";
    if(diffInSec < minute)
    {
        difference = "\(diffInSec) \(secAgo)"
    }
    else if((diffInSec / hour) < 1)
    {
        difference = "\(diffInSec/minute) \(minAgo)"
    }
    else if((diffInSec / day) < 1)
    {
        difference = "\(diffInSec/hour) \(hoursAgo)"
    }
    else if((diffInSec / week) < 1)
    {
        difference = "\(diffInSec/day) \(daysAgo)"
    }
    else if((diffInSec/month) < 1)
    {
        difference = "\(diffInSec/week) \(weekAgo)"
    }
    else if((diffInSec/year) < 1)
    {
        difference = "\(diffInSec / month) \(monthAgo)"
    }
    else
    {
        // return date
        let date: Date = Date()
        let cal: Calendar = Calendar(identifier: .gregorian)
        let newDate: Date = cal.date(bySettingHour: 0, minute: 0, second: 0, of: date)!
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let date2: Date? = dateFormatterGet.date(from: newDate.timeAgoSinceNow())
        difference = dateFormatterGet.string(from: date2!);
        
    }
    return difference;
}

extension String {
    func htmlDecode() -> String {
        if let encodedData = self.data(using: String.Encoding.unicode) {
            if let attributedString = try? NSAttributedString(data: encodedData, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.unicode.rawValue], documentAttributes: nil) {
                return attributedString.string
            }
        }
        return self
    }
}

extension UIColor {
    
    convenience init(hexColor: Int, alpha:Float) {
        let components = (
            R: CGFloat((hexColor >> 16) & 0xff) / 255,
            G: CGFloat((hexColor >> 08) & 0xff) / 255,
            B: CGFloat((hexColor >> 00) & 0xff) / 255
        )
        self.init(red: components.R, green: components.G, blue: components.B, alpha: CGFloat(alpha))
    }
}

func createUILabel()->UILabel{
    let lblWarning = UILabel(frame: CGRect(x: screenSizeWidth/2-100, y: screenSizeHeight/2-10, width: 200, height: 20))
    lblWarning.textAlignment = .center
    lblWarning.text = "Không có dữ liệu"
    lblWarning.font = UIFont.systemFont(ofSize: 14.0)
    lblWarning.textColor = UIColor.darkGray
    return lblWarning
}

func setWidthFollowSizeScreen()->Int{
    return Int(screenSizeWidth/2)
}

func resultDateFormatStandard(str:String)->String{
    
    let partText = str.components(separatedBy: "T")[0].components(separatedBy: "-")
    if partText.count > 1{
        let yyyy = partText[0]
        let mm = partText[1]
        let dd = partText[2]
        return "\(dd)/\(mm)/\(yyyy)"
    }else{
        let convertStr = str.replacingOccurrences(of: "-", with: "/")
        return convertStr
    }
}

func splitString2(str:String)->String
{
    if str != "" && !str.isEmpty{
        let data = str.components(separatedBy: ";").last
        let organizeID = data?.components(separatedBy: ":")[1]
        return organizeID!
    }
    return ""
}

func splitString(str:String)->[String]
{
    var result = [String]()
    if str != "" && !str.isEmpty{
        let partText = str.components(separatedBy: ";")
        let pID = partText[1].components(separatedBy: ":")[1]
        result.append(pID)
        
        let medicalProfileID = partText[2].components(separatedBy: ":")[1]
        result.append(medicalProfileID)
        
        let sID = partText[3].components(separatedBy: ":")[1]
        result.append(sID)
        
        let organizeID = partText[4].components(separatedBy: ":")[1]
        result.append(organizeID)
    }
    return result
}

func splitDataRemindToString(str:String)->[String]
{
    //SID:020517-10001005226;OrganizeID:1
    var result = [String]()
    if str != "" && !str.isEmpty{
        let partText = str.components(separatedBy: ";")
        let sID = partText[1].components(separatedBy: ":")[1]
        result.append(sID)
        
        let organizeID = partText[2].components(separatedBy: ":")[1]
        result.append(organizeID)
    }
    return result
}

func splitImageLocationToString(str:String)->String
{
    //68490_locationImg_20180312_111917.jpg
    var result = String()
    if str != "" && !str.isEmpty{
        let partText = str.components(separatedBy: "_")[0]
        result.append(partText)
    }
    return result
}

func splitDataToString(str:String)->[String]
{
    //Service;serviceID:23
    var result = [String]()
    if str != "" && !str.isEmpty{
        let partText = str.components(separatedBy: ";")
        result.append(partText[0])
        
        let serviceID = partText[1].components(separatedBy: ":")[1]
        result.append(serviceID)
    }
    return result
}


func splitDataNewsToString(str:String)->[String]
{
    var result = [String]()
    if str != "" && !str.isEmpty{
        let partText = str.components(separatedBy: ";")
        let news = partText[0]
        result.append(news)
        
        let newsID = partText[1].components(separatedBy: ":")[1]
        result.append(newsID)
        
        let categoryIDNews = partText[2].components(separatedBy: ":")[1]
        
        result.append(categoryIDNews)
    }
    return result
}

func formatAttributeString(value:Int)->NSMutableAttributedString{
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    formatter.groupingSeparator = "."
    formatter.decimalSeparator = "."
    formatter.usesGroupingSeparator = true
    formatter.minimumFractionDigits = 2
    let number: NSNumber = NSNumber(value:value)
    let formattedString = formatter.string(for: number)
    var attributeString: NSMutableAttributedString? = nil
    if let result = formattedString{
        let format = String(format: "%@0đ", result)
        attributeString =  NSMutableAttributedString(string: format)
        attributeString?.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, (attributeString?.length)!))
    }
    return attributeString!
}

func formatString(value:Float)->String{
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    formatter.groupingSeparator = "."
    formatter.decimalSeparator = "."
    formatter.usesGroupingSeparator = true
    formatter.minimumFractionDigits = 2
    let number: NSNumber = NSNumber(value:value)
    let formattedString = formatter.string(for: number)
    if let result = formattedString{
        let format = String(format: "%@0đ", result)
        return format
    }else{
        return ""
    }
}

func formatString2(value:Float)->String{
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    formatter.groupingSeparator = "."
    formatter.decimalSeparator = "."
    formatter.usesGroupingSeparator = true
    formatter.minimumFractionDigits = 0
    let number: NSNumber = NSNumber(value:value)
    let formattedString = formatter.string(for: number)
    if let result = formattedString{
        let format = String(format: "%@đ", result)
        return format
    }else{
        return ""
    }
}

func setSearchButtonText(text:String,searchBar:UISearchBar) {
    for subview in searchBar.subviews {
        for innerSubViews in subview.subviews {
            if let cancelButton = innerSubViews as? UIButton {
                cancelButton.setTitleColor(UIColor(hex:"1976D2"), for: .normal)
                cancelButton.setTitle(text, for: .normal)
            }
        }
    }
}

func showAlertView(title:String, view:UIViewController){
    let alert = UIAlertController(title: "Thông báo", message: title, preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "Thoát", style: .default, handler: { action in
        
    }))
    view.present(alert, animated: true, completion: nil)
}

func filterKeywords(listKeywords:[DictMedical])->[String: [DictMedical]]{
    var result = [String:[DictMedical]]()
    for string in listKeywords {
        if let firstCharacter = string.testName.characters.first {
            let firstCharacterStr = "\(firstCharacter)"
            if result[firstCharacterStr] == nil {
                result[firstCharacterStr] = [DictMedical]()
            }
            result[firstCharacterStr]?.append(string)
        }
    }
    
    return result
}

func groupInTimeSection(lookUpObj:[Patient])->[String: [Patient]]{
    var result = [String:[Patient]]()
    for i in 0..<Int((lookUpObj.count)){
        let cvTime = lookUpObj[i].InTime.components(separatedBy: "T")[0]
        if result[cvTime] == nil {
            result[cvTime] = [Patient]()
        }
        result[cvTime]?.append(lookUpObj[i])
    }
    return result
}

func groupInGNameSection(lookUpObj:[MedicalDetailsByGroups])->[String: [MedicalDetailsByGroups]]{
    var result = [String:[MedicalDetailsByGroups]]()
    for i in 0..<Int((lookUpObj.count)){
        let dGName = lookUpObj[i].name
        if result[dGName] == nil {
            result[dGName] = [MedicalDetailsByGroups]()
        }
        result[dGName]?.append(lookUpObj[i])
    }
    return result
}

func groupNgayKhamSection(ngayKham:[ScheduleDoctorOfDay])->[String: [ScheduleDoctorOfDay]]{
    var result = [String:[ScheduleDoctorOfDay]]()
    for i in 0..<Int((ngayKham.count)){
        let ngaykham = ngayKham[i].ngaykham
        if result[ngaykham] == nil {
            result[ngaykham] = [ScheduleDoctorOfDay]()
        }
        result[ngaykham]?.append(ngayKham[i])
    }
    return result
}

func showProgress(view:UIView, color:UIColor? = nil) -> MKActivityIndicator? {
    //if let viewToAdd = view {
    let loadingProgress = MKActivityIndicator(frame: CGRect(x:0,y:0,width: 28,height:28))
    if let color = color {
        loadingProgress.color = color
    }
    loadingProgress.center = view.center
    loadingProgress.lineWidth = 3
    loadingProgress.autoresizingMask = [.flexibleTopMargin, .flexibleLeftMargin, .flexibleBottomMargin, .flexibleRightMargin]
    view.addSubview(loadingProgress)
    loadingProgress.startAnimating()
    return loadingProgress
    // }
    // return nil
}

func hideProgress(_ loadingProgress:MKActivityIndicator?) {
    loadingProgress?.stopAnimating()
    loadingProgress?.removeFromSuperview()
}

func confirmLogin(myView:UIViewController){
    // Prepare the popup assets
    let title = "Thông báo"
    let message = "Bạn cần đăng nhập để thực hiện chức năng này"
    // Create the dialog
    let popup = PopupDialog(title: title, message: message, image: nil)
    // Create buttons
    let loginButton = DefaultButton(title: "Đăng nhập") {
        let loginNavigationController = StoryboardScene.LoginRegister.loginNavigationController.instantiate()
        myView.present(loginNavigationController, animated: true, completion: nil)
    }
    
    // Add buttons to dialog
    popup.addButton(loginButton)
    
    // Present dialog
    myView.present(popup, animated: true, completion: nil)
}

func setTextAttributedString(result:String)->NSMutableAttributedString{
    let defineFont = [NSForegroundColorAttributeName: UIColor(hex:"1976D2")]
    let myAttributedString = NSMutableAttributedString()
    for letter in result.unicodeScalars {
        var myLetter : NSAttributedString
        if CharacterSet.decimalDigits.contains(letter){
            myLetter = NSAttributedString(string: "\(letter)", attributes: defineFont)
        } else {
            myLetter = NSAttributedString(string: "\(letter)")
        }
        myAttributedString.append(myLetter)
    }
    return myAttributedString
}

func convertToDays(weekDay: String) -> String{
    switch weekDay {
    case "Sunday":
        return "Chủ nhật"
    case "Monday":
        return "Thứ 2"
    case "Tuesday":
        return "Thứ 3"
    case "Wednesday":
        return "Thứ 4"
    case "Thursday":
        return "Thứ 5"
    case "Friday":
        return "Thứ 6"
    case "Saturday":
        return "Thứ 7"
    default:
        return ""
    }
}

class Common: NSObject {
    
    static let sharedInstance = Common()
    
    func setRatingStar(imgViewStar1: UIImageView, imgViewStar2: UIImageView, imgViewStar3: UIImageView, imgViewStar4: UIImageView, imgViewStar5: UIImageView, ratingScore:Double) -> Void {
        
        let nonStarImage = UIImage(named: "non-star")
        let starImage = UIImage(named: "star")
        let halfStar = UIImage(named: "ic_half_star")
        
        if (ratingScore >= 0 && ratingScore < 0.5) {
            imgViewStar1.image = nonStarImage
            imgViewStar2.image = nonStarImage
            imgViewStar3.image = nonStarImage
            imgViewStar4.image = nonStarImage
            imgViewStar5.image = nonStarImage
        } else if (ratingScore >= 0.5 && ratingScore < 1) {
            imgViewStar1.image = halfStar
            imgViewStar2.image = nonStarImage
            imgViewStar3.image = nonStarImage
            imgViewStar4.image = nonStarImage
            imgViewStar5.image = nonStarImage
        }
        else if (ratingScore >= 1 && ratingScore < 1.5) {
            imgViewStar1.image = starImage
            imgViewStar2.image = nonStarImage
            imgViewStar3.image = nonStarImage
            imgViewStar4.image = nonStarImage
            imgViewStar5.image = nonStarImage
        }
        else if (ratingScore >= 1.5 && ratingScore < 2) {
            imgViewStar1.image = starImage
            imgViewStar2.image = halfStar
            imgViewStar3.image = nonStarImage
            imgViewStar4.image = nonStarImage
            imgViewStar5.image = nonStarImage
        }
        else if (ratingScore >= 2 && ratingScore < 2.5) {
            imgViewStar1.image = starImage
            imgViewStar2.image = starImage
            imgViewStar3.image = nonStarImage
            imgViewStar4.image = nonStarImage
            imgViewStar5.image = nonStarImage
        }
        else if (ratingScore >= 2.5 && ratingScore < 3) {
            imgViewStar1.image = starImage
            imgViewStar2.image = starImage
            imgViewStar3.image = halfStar
            imgViewStar4.image = nonStarImage
            imgViewStar5.image = nonStarImage
        }
        else if (ratingScore >= 3 && ratingScore < 3.5) {
            imgViewStar1.image = starImage
            imgViewStar2.image = starImage
            imgViewStar3.image = starImage
            imgViewStar4.image = nonStarImage
            imgViewStar5.image = nonStarImage
        }else if (ratingScore >= 3.5 && ratingScore < 4) {
            imgViewStar1.image = starImage
            imgViewStar2.image = starImage
            imgViewStar3.image = starImage
            imgViewStar4.image = halfStar
            imgViewStar5.image = nonStarImage
        }
        else if (ratingScore >= 4 && ratingScore < 4.5) {
            imgViewStar1.image = starImage
            imgViewStar2.image = starImage
            imgViewStar3.image = starImage
            imgViewStar4.image = starImage
            imgViewStar5.image = nonStarImage
        }
        else if (ratingScore >= 4.5 && ratingScore < 5) {
            imgViewStar1.image = starImage
            imgViewStar2.image = starImage
            imgViewStar3.image = starImage
            imgViewStar4.image = starImage
            imgViewStar5.image = halfStar
        }
        else if (ratingScore == 5) {
            imgViewStar1.image = starImage
            imgViewStar2.image = starImage
            imgViewStar3.image = starImage
            imgViewStar4.image = starImage
            imgViewStar5.image = starImage
        }
    }
    
    func setTextLabelCommentCount(locationID: Int, label: UILabel) -> Void {
        UserRating.getUserRating(locationID: locationID) { (data, error) in
            label.text = "(\((data?.count)!) Đánh giá)"
        }
    }
    
    func stringFromTimeInterval(interval: String) -> String {
        let endingDate = Date()
        if let timeInterval = TimeInterval(interval) {
            let startingDate = endingDate.addingTimeInterval(-timeInterval)
            let calendar = Calendar.current
            
            var componentsNow = calendar.dateComponents([.hour, .minute, .second], from: startingDate, to: endingDate)
            if let hour = componentsNow.hour, let minute = componentsNow.minute, let seconds = componentsNow.second {
                return "\(hour):\(minute):\(seconds)"
            } else {
                return "00:00:00"
            }
            
        } else {
            return "00:00:00"
        }
    }
    
    func stringFromTimeInterval2(interval: Int) -> NSString {
        let hours = Int((interval / (1000 * 60 * 60)) % 24)
        let minutes = Int((interval / (1000 * 60)) % 60)
        let seconds = Int((interval / 1000) % 60)
        
        return NSString(format: "%0.2d:%0.2d:%0.2d",hours,minutes,seconds)
    }
}

