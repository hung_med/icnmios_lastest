//
//  VerifyPhoneViewController.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class VerifyPhoneViewController: BaseViewControllerNoSearchBar,UITextFieldDelegate {

    var userRegister:UserRegister!
    private var verifyCode:String = ""
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var countTime: UILabel!
    @IBOutlet weak var scrollView: FWCustomScrollView!
    @IBOutlet weak var verifyCodeTextField: FWFloatingLabelTextField!
    private var currentTextField:UITextField?
    var timer:Timer? = nil
    var countTimer:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = true
        verifyCodeTextField.becomeFirstResponder()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Thoát", style: .plain, handler: { (button) in
            let alertViewController = UIAlertController(title: "Thông báo", message: "Hủy bỏ quá trình đăng ký", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Đồng ý", style: UIAlertActionStyle.default, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
            let noAction = UIAlertAction(title: "Huỷ bỏ", style: UIAlertActionStyle.default, handler:nil)
            alertViewController.addAction(noAction)
            alertViewController.addAction(yesAction)
            self.present(alertViewController, animated: true, completion: nil)
        })
        verifyCodeTextField.delegate = self
        descriptionLabel.text = "Hãy cho iCNM biết số điện thoại này là của bạn. Nhập mã trong SMS đã gửi đến\n84" + (userRegister.phoneNumber.hasPrefix("0") ? userRegister.phoneNumber.substring(from: userRegister.phoneNumber.index(userRegister.phoneNumber.startIndex, offsetBy:1)) : userRegister.phoneNumber)
        verifyCodeTextField.addTarget(self, action: #selector(verifyCodeTextFieldChanged), for: .editingChanged)
        registerForNotifications()
        self.sendSMS()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        countTimer = 300 * 1000
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(VerifyPhoneViewController.updateRealTimer), userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
    }
    
    func updateRealTimer() {
        if(countTimer > 0) {
            countTimer -= 1000
            let result = Common.sharedInstance.stringFromTimeInterval2(interval: countTimer)
            self.countTime.text = "\(result)"
            self.countTime.textColor = UIColor(hex:"1976D2")
        }else{
            timer?.invalidate()
            timer = nil
            let alertViewController = UIAlertController(title: "Thông báo", message: "Đã hết thời gian nhận mã xác thực iCNM", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Gửi lại SMS", style: UIAlertActionStyle.default, handler: { (action) in
                self.sendSMS()
                self.countTimer = 300 * 1000
                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(VerifyPhoneViewController.updateRealTimer), userInfo: nil, repeats: true)
            })
            alertViewController.addAction(yesAction)
            self.present(alertViewController, animated: true, completion: nil)
        }
    }

    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func continueButtonTouch(_ sender: Any) {
        if verifyCodeTextField.validate() {
            self.timer?.invalidate()
            self.timer = nil
            self.perform(segue: StoryboardSegue.LoginRegister.showNameInputViewController, sender: self)
        }
    }

    @IBAction func resendSMSButtonTouch(_ sender: Any) {
        self.sendSMS()
    }
    
    @IBAction func changePhoneNumberButtonTouch(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func sendSMS() {
        let progress = self.showProgress()
        let code = 1000 + arc4random_uniform(8000)
        verifyCodeTextField.removeAllRegx()
        verifyCodeTextField.addRegx(strRegx: "^\(code)$", errorMsg: "Mã xác nhận không chính xác")
        verifyCode = "\(code)"
        print(verifyCode)
        let verifyMessage = "Mã xác nhận iCNM của bạn là:\(code)"
        userRegister.sendVerifyPhone(code:verifyMessage,areaCode: "84",success: { (string) in
            self.hideProgress(progress)
            if string.range(of: "Successfull") == nil {
                if string == "" || string == "null" {
                    let alertViewController = UIAlertController(title: "Thông báo", message: "Có lỗi từ máy chủ gửi tin nhắn", preferredStyle: .alert)
                    let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                    alertViewController.addAction(noAction)
                    self.present(alertViewController, animated: true, completion: nil)
                } else {
                    self.timer?.invalidate()
                    self.timer = nil
                    let alertViewController = UIAlertController(title: "Thông báo", message: string, preferredStyle: .alert)
                    let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                    alertViewController.addAction(noAction)
                    self.present(alertViewController, animated: true, completion: nil)
                }
            }
        }) { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        }
    }
    
    @objc private func keyboardWillShow(aNotification:Notification) {
        if let curTextField = currentTextField {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            scrollView.contentInset = contentInset
            scrollView.scrollIndicatorInsets = contentInset
            let rect = curTextField.frame
            let test = view.bounds.height - (rect.size.height + rect.origin.y + scrollView.contentInset.top) - 40.0
            let different = keyboardFrame.size.height - test
            print(different)
            if different > 0 {
                scrollView.contentOffset.y = different - self.scrollView.contentInset.top
            }
        }
        
    }
    
    @objc private func keyboardWillHide(aNotification:Notification) {
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = 0
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTextField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == verifyCodeTextField {
            textField.resignFirstResponder()
        }
        return true
    }
    
    @IBAction func verifyCodeTextFieldChanged(_ sender: Any) {
        if let textField = sender as? FWFloatingLabelTextField {
            if textField.validate() {
                perform(segue: StoryboardSegue.LoginRegister.showNameInputViewController, sender: self)
            }
        }
    }
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegue.LoginRegister.showNameInputViewController.rawValue {
            userRegister.verifyPhone = true
            let destinationVC = segue.destination as! NameInputViewController
            destinationVC.userRegister = self.userRegister
        }
     }
    
    @IBAction func touchScreen(_ sender: Any) {
        currentTextField?.resignFirstResponder()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
