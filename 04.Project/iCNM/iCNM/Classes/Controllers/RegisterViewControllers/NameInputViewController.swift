//
//  NameInputViewController.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class NameInputViewController: UIViewController,UITextFieldDelegate {
    
    var userRegister:UserRegister!
    
    
    @IBOutlet weak var scrollView: FWCustomScrollView!
    @IBOutlet weak var nameTextField: FWFloatingLabelTextField!
    private var currentTextField:UITextField?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem?.title = " "
        self.navigationItem.hidesBackButton = true
        nameTextField.addRegx(strRegx: "^.{6,}$", errorMsg: "Họ tên của bạn quá ngắn")
        registerForNotifications()
        nameTextField.delegate = self
        nameTextField.becomeFirstResponder()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Thoát", style: .plain, handler: { (button) in
            let alertViewController = UIAlertController(title: "Thông báo", message: "Hủy bỏ quá trình đăng ký", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Đồng ý", style: UIAlertActionStyle.default, handler: { (action) in
                if let array = self.navigationController?.viewControllers, array.count > 1 {
                    self.navigationController?.popToViewController(array[1], animated: true)
                }
            })
            let noAction = UIAlertAction(title: "Huỷ bỏ", style: UIAlertActionStyle.default, handler:nil)
            alertViewController.addAction(noAction)
            alertViewController.addAction(yesAction)
            self.present(alertViewController, animated: true, completion: nil)
        })
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Cập nhật Tên"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc private func keyboardWillShow(aNotification:Notification) {
        if let curTextField = currentTextField {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            scrollView.contentInset = contentInset
            scrollView.scrollIndicatorInsets = contentInset
            let rect = curTextField.frame
            let test = view.bounds.height - (rect.size.height + rect.origin.y + scrollView.contentInset.top) - 40.0
            let different = keyboardFrame.size.height - test
            print(different)
            if different > 0 {
                scrollView.contentOffset.y = different - self.scrollView.contentInset.top
            }
        }
        
    }
    
    @objc private func keyboardWillHide(aNotification:Notification) {
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = 0
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTextField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @IBAction func touchScreen(_ sender: Any) {
        currentTextField?.resignFirstResponder()
    }
    
    @IBAction func touchContinueButton(_ sender: Any) {
        if nameTextField.validate() {
            userRegister.name = nameTextField.text!
            perform(segue: StoryboardSegue.LoginRegister.showInputBirthdayViewController, sender: self)
        }
    }
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegue.LoginRegister.showInputBirthdayViewController.rawValue {
            let destinationVC = segue.destination as! BirthdayInputViewController
            destinationVC.userRegister = self.userRegister
        }
     }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
