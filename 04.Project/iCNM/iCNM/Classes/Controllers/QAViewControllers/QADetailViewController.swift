//
//  QADetailViewController.swift
//  iCNM
//
//  Created by Medlatec on 6/27/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import Social
import Cosmos
import Toaster
class QADetailViewController: UIViewController, UIGestureRecognizerDelegate, UIScrollViewDelegate {
    var questionAnswer:QuestionAnswer!
    var isCheck:Bool? = false
    private var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    @IBOutlet weak var userAskNameLabel: UILabel!
    @IBOutlet weak var timeAskLabel: UILabel!
    @IBOutlet weak var userAskAvatar: PASImageView!
    @IBOutlet weak var specialistLabel: FWTagLabel!
    @IBOutlet weak var questionTitleLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var userAnswerNameLabel: UILabel!
    @IBOutlet weak var timeanswerLabel: UILabel!
    @IBOutlet weak var answerContentLabel: UILabel!
    @IBOutlet weak var userAnswerAvatar: PASImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var answerBubble: FWSpeechBubble!
    @IBOutlet weak var questionBubble: FWSpeechBubble!
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet var ratingView: CosmosView!
    @IBOutlet weak var btnQuestion: UIButton!
    @IBOutlet weak var spaceLeft: NSLayoutConstraint!
    @IBOutlet weak var spaceRight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(QADetailViewController.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
            
            // right bar button - navigation to ask question
            let newQAButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(askNewQA))
            self.navigationItem.rightBarButtonItem = newQAButton
        }
        
        self.btnQuestion.titleLabel?.font = UIFont.fontAwesome(ofSize: 18.0)
        let titleBtn = String.fontAwesomeIcon(name: .questionCircleO) +  " Đặt câu hỏi tư vấn"
        self.btnQuestion.setTitle(titleBtn, for: .normal)
        self.btnQuestion.addTarget(self, action:#selector(askNewQA), for: .touchUpInside)
        myScrollView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(handleLoginSuccess), name: Constant.NotificationMessage.loginSuccess, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleLogout), name: Constant.NotificationMessage.logout, object: nil)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.showProfileCurrentUser(sender:)))
        tap.delegate = self // This is not required
        userAnswerAvatar.addGestureRecognizer(tap)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.showProfileCurrentUser2(sender:)))
        tap2.delegate = self // This is not required
        userAskAvatar.addGestureRecognizer(tap2)
        
        self.title = questionAnswer.question?.title
        userAskNameLabel.text = questionAnswer.question?.fullname
        questionTitleLabel.text = questionAnswer.question?.title
        questionLabel.text = questionAnswer.question?.content
        if let createdDate = questionAnswer.question?.dateCreate {
            let timeString = "\u{f017} " + createdDate.timeAgoSinceNow()
            let timeAttributedString = NSMutableAttributedString(string: timeString)
            timeAttributedString.addAttributes([NSFontAttributeName:UIFont(font: FontFamily.FontAwesome.regular, size: 15.0)], range: NSMakeRange(0, 1))
            timeAskLabel.attributedText = timeAttributedString
        }
        if let askUser = questionAnswer.userAsk, let avatarURL = askUser.getAvatarURL() {
            userAskAvatar.imageURL(URL: avatarURL)
        }
        
        if let specialistName = questionAnswer.specialist?.name {
            specialistLabel.text = specialistName
        }
        
        if let answer = questionAnswer.answer {
            if let answerHTML = try! answer.content?.convertHtmlSymbols() {
                answerContentLabel.text = answerHTML
            }
            if let createdDate = answer.dateCreate {
                let answered = "\u{f058}  Đã trả lời " + createdDate.timeAgoSinceNow()
                let timeAttributedString = NSMutableAttributedString(string: answered)
                timeAttributedString.addAttributes([NSForegroundColorAttributeName:UIColor(hex:"39b54a"), NSFontAttributeName:UIFont(font: FontFamily.FontAwesome.regular, size: 15.0)], range: NSMakeRange(0, 1))                
                timeanswerLabel.attributedText = timeAttributedString
            }
            if let doctor = questionAnswer.userOfDoctor {
                userAnswerNameLabel.text = doctor.name ?? "Bác sĩ"
            }
            if let userDoctor = questionAnswer.userOfDoctor, let avatarURL = userDoctor.getAvatarURL() {
                userAnswerAvatar.imageURL(URL: avatarURL)
            }
        } else {
            userAnswerAvatar.removeFromSuperview()
            answerBubble.removeFromSuperview()
            timeanswerLabel.removeFromSuperview()
            userAnswerNameLabel.removeFromSuperview()
            NSLayoutConstraint(item: commentButton, attribute: .top, relatedBy: .equal, toItem: questionBubble, attribute: .bottom, multiplier: 1.0, constant: 16.0).isActive = true
        }
        
        if let user = self.currentUser{
            if user.id == questionAnswer.userAsk?.id || user.doctorID == questionAnswer.doctor?.id{
                self.ratingView.isUserInteractionEnabled = true
            }else{
                self.ratingView.isUserInteractionEnabled = false
            }
        }
    }
    
    func askNewQA() {
        if currentUser != nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let createAQuestionVC = storyboard.instantiateViewController(withIdentifier: "CreateQuestionVC") as! CreateQuestionViewController
            self.navigationController?.pushViewController(createAQuestionVC, animated: true)
        }else{
            confirmLogin(myView: self)
        }
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
        let rating = self.ratingView.rating
        if rating > 0{
            if let currentRating = questionAnswer.answer?.sumRate{
                if rating != Double(currentRating){
                    if let answerID = questionAnswer.answer?.id, let userID =  questionAnswer.question?.userID{
                        QuestionAnswer().postRatingAnswer(numRate:Float(rating), userID:userID, answerID:answerID, success: { (data) in
                            if data != "" {
                                Toast(text: "Cảm ơn bạn đã để lại đánh giá").show()
                            }else {
                                
                            }
                        }, fail: { (error, response) in
                            print(error)
                        })
                    }
                }
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.myScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let liked = questionAnswer.liked
        let countComment = questionAnswer.question?.countComment ?? 0
        let countLike = questionAnswer.question?.countLike ?? 0
        let countShare = questionAnswer.question?.countShare ?? 0
        if self.isCheck!{
            if questionAnswer.liked == false {
                self.likeButton.setAttributedTitle(nil, for: .normal)
                self.likeButton.setTitleColor(UIColor(hex:"9D9D9D"), for: .normal)
                self.likeButton.setTitle("  Thích" + (countLike>0 ? " (\(countLike))":""), for: .normal)
            } else {
                let attributedString = NSMutableAttributedString(string: "  Thích" + (countLike>0 ? " (\(countLike))":""))
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor(hex:"1d6edc"), range: NSMakeRange(0, 1))
                self.likeButton.setAttributedTitle(attributedString, for: .normal)
            }
            
            if let userID = currentUser?.id, questionAnswer.userInteractionInfoLoaded == false && questionAnswer.isLoadingUserInteractionInfo == false {
                questionAnswer.getUserInteractionInfo(userID: userID, type: .Question, success: {
                    if self.questionAnswer.liked {
                        let cLike = self.questionAnswer.question?.countLike ?? 0
                        let attributedString = NSMutableAttributedString(string: "  Thích" + (cLike>0 ? " (\(cLike))":""))
                        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor(hex:"1d6edc"), range: NSMakeRange(0, 1))
                        self.likeButton.setAttributedTitle(attributedString, for: .normal)
                    }
                }, fail: { (error, response) in
                    
                })
            }
        }else{
            if liked == false {
                likeButton.setAttributedTitle(nil, for: .normal)
                likeButton.setTitleColor(UIColor(hex:"9D9D9D"), for: .normal)
                likeButton.setTitle("  Thích" + (countLike>0 ? " (\(countLike))":""), for: .normal)
            } else {
                let attributedString = NSMutableAttributedString(string: "  Thích" + (countLike>0 ? " (\(countLike))":""))
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor(hex:"1d6edc"), range: NSMakeRange(0, 1))
                likeButton.setAttributedTitle(attributedString, for: .normal)
            }
        }
        
        if let sumRate = questionAnswer.answer?.sumRate{
            self.ratingView.rating = Double(sumRate)
            if sumRate != 0{
                self.ratingView.text = "(\(Double(round(10*self.ratingView.rating)/10)))"
            }
        }
        
        if let user = self.currentUser{
            if user.id == questionAnswer.userAsk?.id || user.doctorID == questionAnswer.doctor?.id{
                commentButton.isHidden = false
            }else{
                commentButton.isHidden = true
            }
        }else{
            commentButton.isHidden = true
        }
        
        commentButton.setTitle("  Bình luận" + (countComment>0 ? " (\(countComment))":""), for: .normal)
        commentButton.addTarget(self, action: #selector(self.handleUserComment(_:)), for: .touchUpInside)
        shareButton.setTitle("  Chia sẻ" + (countShare>0 ? " (\(countShare))":""), for: .normal)
        
        if IS_IPHONE_5{
            commentButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 13.0)
            likeButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 13.0)
            shareButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 13.0)
        }
    }
    
    @objc func handleUserComment(_ sender: UIButton){
        let commentVC = self.storyboard?.instantiateViewController(withIdentifier: "QAComment") as! QACommentViewController
        commentVC.questionAnswer = questionAnswer
        self.navigationController?.pushViewController(commentVC, animated: true)
    }
    
    func openLinkAds(sender : UITapGestureRecognizer){
        let image = sender.view as! UIImageView?
        let link = image?.accessibilityHint
        guard let url = URL(string: link!) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        
     }
     
    @IBAction func touchLikeButton(_ sender: Any) {
        if let userID = currentUser?.id, !currentUser!.isInvalidated {
            questionAnswer.toggleLikeThank(userID: userID, type: .Question, success: {
                let countLike = self.questionAnswer.question?.countLike ?? 0
                if self.questionAnswer.liked == false {
                    self.likeButton.setAttributedTitle(nil, for: .normal)
                    self.likeButton.setTitleColor(UIColor(hex:"9D9D9D"), for: .normal)
                    self.likeButton.setTitle("  Thích" + (countLike>0 ? " (\(countLike))":""), for: .normal)
                } else {
                    let attributedString = NSMutableAttributedString(string: "  Thích" + (countLike>0 ? " (\(countLike))":""))
                    attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor(hex:"1d6edc"), range: NSMakeRange(0, 1))
                    self.likeButton.setAttributedTitle(attributedString, for: .normal)
                }
            }, fail: { (error, response) in
                
            })
        } else {
            confirmLogin(myView: self)
        }
    }
    
    func handleLoginSuccess(aNotification:Notification) {
        let realm = try! Realm()
        currentUser = realm.currentUser()
    }
    
    func handleLogout(aNotification:Notification) {
        currentUser = nil
    }
    
    @IBAction func touchShareButton(_ sender: Any) {
        if let composeVC = SLComposeViewController(forServiceType: SLServiceTypeFacebook), let title = questionAnswer.question?.title, let questionID = questionAnswer.question?.id{
            let result = title.replacingOccurrences(of: " ", with: "-")
            let urlString = Constant.baseURLWeb + "/\(questionID)" + "/\(result).aspx"
            let urlApp = URL(string: urlString)
            composeVC.add(urlApp)
            composeVC.setInitialText("Viết gì đó...")
            self.present(composeVC, animated: true, completion: nil)
        }
    }
    
    @objc func showProfileCurrentUser(sender: UITapGestureRecognizer? = nil){
        let profileDoctorVC = ProfileDoctorVC(nibName: "ProfileDoctorVC", bundle: nil)
        let featuredDoctor = FeaturedDoctor()
        featuredDoctor.userInfo = questionAnswer.userOfDoctor
        featuredDoctor.doctor = questionAnswer.doctor
        featuredDoctor.specialists?[0] = questionAnswer.specialist!
        profileDoctorVC.featuredDoctor = featuredDoctor
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(profileDoctorVC, animated: true)
    }
    
    @objc func showProfileCurrentUser2(sender: UITapGestureRecognizer? = nil){
        let profileDoctorVC = ProfileDoctorVC(nibName: "ProfileDoctorVC", bundle: nil)
        let featuredDoctor = FeaturedDoctor()
        featuredDoctor.userInfo = questionAnswer.userAsk
        profileDoctorVC.featuredDoctor = featuredDoctor
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(profileDoctorVC, animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5, animations: {
            self.btnQuestion.alpha = 1
        }, completion: {
            finished in
            self.btnQuestion.isHidden = false
        })
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5, animations: {
            self.btnQuestion.alpha = 0
        }, completion: {
            finished in
            self.btnQuestion.isHidden = true
        })
    }
}
