//
//  QACommentViewController.swift
//  iCNM
//
//  Created by Medlatec on 6/28/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import SlackTextViewController
import RealmSwift
import Alamofire

class QACommentViewController: SLKTextViewController {
    
    private let indicatorSize:CGFloat = 28.0
    private let loadingProgressLineWidth:CGFloat = 3.0
    fileprivate var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    var questionAnswer:QuestionAnswer!
    var comments = [UserQuestionComment]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    var editingComment = UserQuestionComment()
    var editingIndex = -1
    fileprivate var currentCommentRequest:Request?
    fileprivate var currentRemoveCommentRequest:Request?
    
    override var tableView: UITableView {
        get {
            return super.tableView!
        }
    }
    
    // MARK: - Initialisation
    
    override class func tableViewStyle(for decoder: NSCoder) -> UITableViewStyle {
        
        return .plain
    }
    
    func commonInit() {
        
        NotificationCenter.default.addObserver(self.tableView, selector: #selector(UITableView.reloadData), name: NSNotification.Name.UIContentSizeCategoryDidChange, object: nil)
    }
    
    override func viewDidLoad() {
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(QACommentViewController.back(sender:)), for: UIControlEvents.touchUpInside)
            
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        // Register a SLKTextView subclass, if you need any special appearance and/or behavior customisation.
        self.registerClass(forTextView: MessageTextView.classForCoder())
        
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = MessageTableViewCell.minimumHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.commonInit()
        self.automaticallyAdjustsScrollViewInsets = true
        // SLKTVC's configuration
        self.bounces = true
        self.shouldScrollToBottomAfterKeyboardShows = true
        self.isInverted = false
        
        self.rightButton.setTitle(NSLocalizedString("Gửi", comment: ""), for: UIControlState())
        
        self.textInputbar.autoHideRightButton = true
        self.textInputbar.maxCharCount = 256
        self.textInputbar.counterStyle = .split
        self.textInputbar.counterPosition = .top
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleLoginSuccess), name: Constant.NotificationMessage.loginSuccess, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleLogout), name: Constant.NotificationMessage.logout, object: nil)
        
        self.textInputbar.editorTitle.textColor = UIColor.darkGray
        self.textInputbar.editorLeftButton.tintColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
        self.textInputbar.editorRightButton.tintColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
        self.textInputbar.editorTitle.text = "Sửa bình luận"
        self.textInputbar.editorLeftButton.setTitle("Huỷ", for: .normal)
        self.textInputbar.editorRightButton.setTitle("Lưu", for: .normal)
        self.tableView.separatorStyle = .none
        self.tableView.register(MessageTableViewCell.classForCoder(), forCellReuseIdentifier: MessageTableViewCell.commentCellIdentifier)
        if currentUser == nil {
            self.textView.placeholder = "Bạn cần đăng nhập để bình luận";
        } else {
            self.textView.placeholder = "Nhập nội dung bình luận";
        }
        
        self.requestData()
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func requestData() {
        let progress = showProgress()
        currentCommentRequest?.cancel()
        currentCommentRequest = questionAnswer.getComments(success: { (comments) in
            self.hideProgress(progress)
            self.comments = comments
            self.comments = self.comments.sorted(by: { ($0.questionComment?.id)! < $1.questionComment!.id })
        }) { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        }
    }
    
    // MARK: - Lifeterm
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        questionAnswer.question?.countComment = comments.count
    }
    
    func showProgress(color:UIColor? = nil) -> MKActivityIndicator? {
        let loadingProgress = MKActivityIndicator(frame: CGRect(x:0,y:0,width: indicatorSize,height:indicatorSize))
        if let color = color {
            loadingProgress.color = color
        }
        loadingProgress.center = self.tableView.center
        loadingProgress.lineWidth = loadingProgressLineWidth
        loadingProgress.autoresizingMask = [.flexibleTopMargin, .flexibleLeftMargin, .flexibleBottomMargin, .flexibleRightMargin]
        tableView.addSubview(loadingProgress)
        loadingProgress.startAnimating()
        return loadingProgress
    }
    
    func hideProgress(_ loadingProgress:MKActivityIndicator?) {
        loadingProgress?.stopAnimating()
        loadingProgress?.removeFromSuperview()
    }
}

extension QACommentViewController {
    
    // MARK: - Example's Configuration
    
    
    func didLongPressCell(_ gesture: UIGestureRecognizer) {
        
        guard let view = gesture.view else {
            return
        }
        
        if gesture.state != .began {
            return
        }
        
        if #available(iOS 8, *) {
            var alertController:UIAlertController? = nil
            if IS_IPAD{
                alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
            }else{
                alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            }
            
            alertController?.modalPresentationStyle = .popover
            alertController?.popoverPresentationController?.sourceView = view.superview
            alertController?.popoverPresentationController?.sourceRect = view.frame
            alertController?.addAction(UIAlertAction(title: "Sửa bình luận", style: .default, handler: { [unowned self] (action) -> Void in
                self.editCellMessage(gesture)
            }))
            
            alertController?.addAction(UIAlertAction(title: "Huỷ", style: .cancel, handler: nil))
            
            self.navigationController?.present(alertController!, animated: true, completion: nil)
        }
        else {
            self.editCellMessage(gesture)
        }
    }
    
    func editCellMessage(_ gesture: UIGestureRecognizer) {
        
        guard let cell = gesture.view as? MessageTableViewCell else {
            return
        }
        if let indexPath = tableView.indexPath(for: cell) {
            self.editingComment = self.comments[indexPath.row]
            self.editingIndex = indexPath.row
            self.editText(self.editingComment.questionComment?.comment ?? "")
            self.tableView.scrollToRow(at: cell.indexPath, at: .bottom, animated: true)
            
        }
    }
}

extension QACommentViewController {
    
    // MARK: - Overriden Methods
    
    override func ignoreTextInputbarAdjustment() -> Bool {
        return super.ignoreTextInputbarAdjustment()
    }
    
    override func forceTextInputbarAdjustment(for responder: UIResponder!) -> Bool {
        
        if #available(iOS 8.0, *) {
            guard let _ = responder as? UIAlertController else {
                // On iOS 9, returning YES helps keeping the input view visible when the keyboard if presented from another app when using multi-tasking on iPad.
                return UIDevice.current.userInterfaceIdiom == .pad
            }
            return true
        }
        else {
            return UIDevice.current.userInterfaceIdiom == .pad
        }
    }
    
    // Notifies the view controller that the keyboard changed status.
    override func didChangeKeyboardStatus(_ status: SLKKeyboardStatus) {
        switch status {
        case .willShow:
            print("Will Show")
        case .didShow:
            print("Did Show")
        case .willHide:
            print("Will Hide")
        case .didHide:
            print("Did Hide")
        }
    }
    
    // Notifies the view controller that the text will update.
    override func textWillUpdate() {
        super.textWillUpdate()
    }
    
    // Notifies the view controller that the text did update.
    override func textDidUpdate(_ animated: Bool) {
        super.textDidUpdate(animated)
    }
    
    // Notifies the view controller when the left button's action has been triggered, manually.
    override func didPressLeftButton(_ sender: Any!) {
        super.didPressLeftButton(sender)
        
        self.dismissKeyboard(true)
        self.performSegue(withIdentifier: "Push", sender: nil)
    }
    
    // Notifies the view controller when the right button's action has been triggered, manually or by using the keyboard return key.
    override func didPressRightButton(_ sender: Any!) {
        
        // This little trick validates any pending auto-correction or auto-spelling just after hitting the 'Send' button
        //self.textView.refreshFirstResponder()
        self.textInputbar.endEditing(true)
        if currentUser != nil {
            let userComment = UserQuestionComment()
            if let uInfo = currentUser?.userDoctor?.userInfo {
                userComment.user = UserInfo(value: uInfo)
            }
            let questionComment = QuestionComment()
            questionComment.userID = currentUser!.userDoctor?.id ?? 0
            questionComment.questionID = questionAnswer.question?.id ?? 0
            questionComment.comment = self.textView.text
            userComment.questionComment = questionComment
            
            let progress = showProgress()
            userComment.addUpdateComment(success: {  (userQuestionComment) in
                self.hideProgress(progress)
                if let new = userQuestionComment {
                    let indexPath = IndexPath(row: self.comments.count, section: 0)
                    let rowAnimation: UITableViewRowAnimation = self.isInverted ? .bottom : .top
                    let scrollPosition: UITableViewScrollPosition = self.isInverted ? .bottom : .top
                    self.tableView.beginUpdates()
                    self.comments.insert(new, at: self.comments.count)
                    self.tableView.insertRows(at: [indexPath], with: rowAnimation)
                    self.tableView.endUpdates()
                    
                    self.tableView.scrollToRow(at: indexPath, at: scrollPosition, animated: true)
                    
                    // Fixes the cell from blinking (because of the transform, when using translucent cells)
                    // See https://github.com/slackhq/SlackTextViewController/issues/94#issuecomment-69929927
                    self.tableView.reloadRows(at: [indexPath], with: .automatic)
                } else {
                    
                }
            }, fail: { (error, response) in
                self.hideProgress(progress)
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
        
        super.didPressRightButton(sender)
    }
    
    // Notifies the view controller when tapped on the right "Accept" button for commiting the edited text
    override func didCommitTextEditing(_ sender: Any) {
        
        self.editingComment.questionComment?.comment = self.textView.text
        let progress = showProgress()
        self.editingComment.addUpdateComment(success: { (userQuestionComment) in
            self.hideProgress(progress)
            if let new = userQuestionComment {
                self.comments[self.editingIndex] = new
                self.tableView.reloadData()
            } else {
                
            }
        }) { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        }
        super.didCommitTextEditing(sender)
    }
    
    // Notifies the view controller when tapped on the left "Cancel" button
    override func didCancelTextEditing(_ sender: Any) {
        super.didCancelTextEditing(sender)
    }
    
    override func canPressRightButton() -> Bool {
        return super.canPressRightButton()
    }
    
    override func canShowTypingIndicator() -> Bool {
        return false
    }
    
}

extension QACommentViewController {
    
    // MARK: - UITableViewDataSource Methods
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tableView {
            return self.comments.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.messageCellForRowAtIndexPath(indexPath)
    }
    
    func messageCellForRowAtIndexPath(_ indexPath: IndexPath) -> MessageTableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: MessageTableViewCell.commentCellIdentifier) as! MessageTableViewCell
        
        let comment = self.comments[(indexPath as NSIndexPath).row]
        let avatar = comment.user?.avatar
        cell.titleLabel.text = comment.user?.name
        cell.bodyLabel.text = comment.questionComment?.comment
        if avatar == nil{
            cell.thumbnailView.update(image: UIImage(named:"avatar_default"), animated: true)
        }else{
            if let avatarURL = comment.user?.getAvatarURL() {
                cell.thumbnailView.imageURL(URL: avatarURL)
            }
        }
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapAvatar))
        gestureRecognizer.numberOfTapsRequired = 1
        cell.thumbnailView.tag = indexPath.row
        cell.thumbnailView.isUserInteractionEnabled = true
        cell.thumbnailView.addGestureRecognizer(gestureRecognizer)
        
        if let currentUserID = currentUser?.id,let commentUserID = comment.user?.id, currentUserID == commentUserID {
            if cell.gestureRecognizers?.count == nil {
                let longPress = UILongPressGestureRecognizer(target: self, action: #selector(QACommentViewController.didLongPressCell(_:)))
                cell.addGestureRecognizer(longPress)
            }
            cell.editButton.isHidden = false
            cell.deleteButton.isHidden = false
            cell.editButton.tag = (indexPath as NSIndexPath).row
            cell.deleteButton.tag = (indexPath as NSIndexPath).row
            cell.editButton.addTarget(self, action: #selector(touchEditComment(sender:)), for: .touchUpInside)
            cell.deleteButton.addTarget(self, action: #selector(touchDeleteComment(sender:)), for: .touchUpInside)
        } else {
            cell.editButton.isHidden = true
            cell.deleteButton.isHidden = true
            cell.editButton.removeTarget(self, action: #selector(touchEditComment(sender:)), for: .touchUpInside)
            cell.deleteButton.removeTarget(self, action: #selector(touchDeleteComment(sender:)), for: .touchUpInside)
        }
        
        if let editedDate = comment.questionComment?.dateEdit {
            cell.statusLabel.text = "Đã chỉnh sửa \(editedDate.timeAgoSinceNow())"
        } else if let createdDate = comment.questionComment?.dateCreate {
            cell.statusLabel.text = createdDate.timeAgoSinceNow()
        }
        // Cells must inherit the table view's transform
        // This is very important, since the main table view may be inverted
        cell.transform = self.tableView.transform
        
        return cell
    }
    
    func handleTapAvatar(_ sender: UITapGestureRecognizer) {
        if let tag = sender.view?.tag{
            let comment = self.comments[tag]
            if let userInfo = comment.user{
                self.showProfileCurrentUser(userInfo: userInfo)
            }
        }
    }
    
    func showProfileCurrentUser(userInfo:UserInfo){
        let profileDoctorVC = ProfileDoctorVC(nibName: "ProfileDoctorVC", bundle: nil)
        let featuredDoctor = FeaturedDoctor()
        featuredDoctor.userInfo = userInfo
        featuredDoctor.doctor = nil
        featuredDoctor.specialists = nil
        profileDoctorVC.featuredDoctor = featuredDoctor
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(profileDoctorVC, animated: true)
    }
    
    func touchEditComment(sender:UIButton) {
        self.editingComment = self.comments[sender.tag]
        self.editingIndex = sender.tag
        self.editText(self.editingComment.questionComment?.comment ?? "")
        let indexPath = IndexPath(row:sender.tag,section:self.comments.count)
        self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
    
    func touchDeleteComment(sender:UIButton) {
        self.textInputbar.endEditing(true)
        if let currentUserID = currentUser?.id {
            let user = ["UserID":currentUserID]
            let comment = self.comments[sender.tag]
            if let commentID = comment.questionComment?.id {
                let questionComment = ["QuestionCommentID":commentID]
                let parameterDic = ["User":user,
                                    "QuestionComment":questionComment]
                let alertViewController = UIAlertController(title: "Thông báo", message: "Bạn có chắc chắn muốn xoá bình luận này hay không?", preferredStyle: .alert)
                let yesAction = UIAlertAction(title: "Có", style: UIAlertActionStyle.cancel, handler: { (action) in
                    self.currentRemoveCommentRequest?.cancel()
                    let progress = self.showProgress()
                    self.currentRemoveCommentRequest = Question.removeComment(paramDic: parameterDic, success: { (resultString) in
                        self.hideProgress(progress)
                        if resultString == "null" {
                            self.comments.remove(at: sender.tag)
                            self.textInputbar.endEditing(true)
                            self.tableView.reloadData()
                        } else {
                            let alertViewController = UIAlertController(title: "Thông báo", message: "Có lỗi khi xoá bình luận", preferredStyle: .alert)
                            let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                            alertViewController.addAction(noAction)
                            self.present(alertViewController, animated: true, completion: nil)
                        }
                    }, fail: { (error, response) in
                        self.hideProgress(progress)
                        self.handleNetworkError(error: error, responseData: response.data)
                    })
                    
                })
                alertViewController.addAction(yesAction)
                let noAction = UIAlertAction(title: "Không", style: UIAlertActionStyle.default, handler:nil)
                self.textInputbar.endEditing(true)
                alertViewController.addAction(noAction)
                self.present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    func handleLoginSuccess(aNotification:Notification) {
        let realm = try! Realm()
        currentUser = realm.currentUser()
        let userDefault = UserDefaults.standard
        userDefault.set(true, forKey: "agreedTerms")
        userDefault.synchronize()
    }
    
    func handleLogout(aNotification:Notification) {
        currentUser = nil
        let userDefault = UserDefaults.standard
        if userDefault.bool(forKey: "agreedTerms") {
            userDefault.removeObject(forKey: "agreedTerms")
        }
    }
}

extension QACommentViewController {
    
    // MARK: - UIScrollViewDelegate Methods
    
    // Since SLKTextViewController uses UIScrollViewDelegate to update a few things, it is important that if you override this method, to call super.
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
    }
    
}

extension QACommentViewController {
    
    // MARK: - UITextViewDelegate Methods
    
    override func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if currentUser == nil {
            confirmLogin(myView: self)
            return false
        }
        if #available(iOS 12.0, *) {
            self.tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        }
        return true
    }
    
    override func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        // Since SLKTextViewController uses UIScrollViewDelegate to update a few things, it is important that if you override this method, to call super.
        return true
    }
    
    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return super.textView(textView, shouldChangeTextIn: range, replacementText: text)
    }
    
}
