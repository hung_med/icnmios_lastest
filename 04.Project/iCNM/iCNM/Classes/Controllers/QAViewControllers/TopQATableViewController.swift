//
//  TopQATableViewController.swift
//  iCNM
//
//  Created by Medlatec on 6/19/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import SideMenu
import RealmSwift
import ESPullToRefresh
import Alamofire
import Social
import Toaster

class TopQATableViewController: BaseTableViewController {
    var currentIndex = 0
    var notificationToken: NotificationToken? = nil
    let selectedSpecialists:Results<Specialist> = {
        let realm = try! Realm()
        return realm.objects(Specialist.self).filter("selected == true")
    }()
    
    private var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    private var pageNumber = 1
    
    var questionData = [QuestionAnswer]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    private var selectedIndex = -1
    private var headerScrollView = ESRefreshHeaderAnimator(frame:CGRect.zero)
    private var footerScrollView = ESRefreshFooterAnimator(frame:CGRect.zero)
    
    let specialistSelectVC = StoryboardScene.Main.specialistMenuNavigationController.instantiate()
    
    private var currentRequest:Request?
    private var currentQARequest:Request?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if #available(iOS 11.0, *) {
//            tableView.contentInsetAdjustmentBehavior = .never
//        }
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(TopQATableViewController.back(sender:)), for: UIControlEvents.touchUpInside)
            
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        let realm = try! Realm()
        currentUser = realm.currentUser()
        NotificationCenter.default.addObserver(self, selector: #selector(handleLoginSuccess), name: Constant.NotificationMessage.loginSuccess, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleLogout), name: Constant.NotificationMessage.logout, object: nil)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        NotificationCenter.default.addObserver(self, selector: #selector(updateSelected), name: Constant.NotificationMessage.specialistSelectedChanged, object: nil)
        headerScrollView.pullToRefreshDescription = "Kéo để làm mới"
        headerScrollView.releaseToRefreshDescription = "Thả để làm mới"
        headerScrollView.loadingDescription = "Đang tải..."
        footerScrollView.loadingDescription = "Đang tải..."
        footerScrollView.loadingMoreDescription = "Tải thêm"
        footerScrollView.noMoreDataDescription = "Đã hết dữ liệu"
        
        self.tableView.es.addInfiniteScrolling(animator:footerScrollView) { [weak self] in
            if let weakSelf = self {
                weakSelf.pageNumber = weakSelf.pageNumber + 1
                QuestionAnswer.getAllQuestionAnswer(pageNumber: weakSelf.pageNumber, specialistIDs: [self!.currentIndex], success: { (data) in
                    self?.tableView.es.stopLoadingMore()
                    if data.count > 0 {
                        self?.questionData.append(contentsOf: data)
                        self?.tableView.reloadData()
                    } else {
                        self?.tableView.es.noticeNoMoreData()
                    }
                }) { (error, response) in
                    self?.tableView.es.stopLoadingMore()
                    self?.handleNetworkError(error: error, responseData: response.data)
                }
            }
        }
        self.tableView.es.addPullToRefresh(animator: headerScrollView) { [weak self] in
            if let weakSelf = self {
                self?.pageNumber = 1
                self?.currentQARequest?.cancel()
                self?.currentQARequest = QuestionAnswer.getAllQuestionAnswer(pageNumber: weakSelf.pageNumber, specialistIDs: [self!.currentIndex], success: { (data) in
                    self?.questionData = data
                    self?.tableView.reloadData()
                    self?.tableView.es.stopPullToRefresh()
                }) { (error, response) in
                    self?.tableView.es.stopPullToRefresh()
                    self?.handleNetworkError(error: error, responseData: response.data)
                }
            }
        }
        tableView.tableFooterView = UIView()
        requestData()
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Hỏi đáp - Chủ đề tâm điểm"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        SideMenuManager.menuRightNavigationController = nil
    }
    
    override func requestData() {
        currentRequest?.cancel()
        let progressHUD = self.showProgress()
        currentRequest = Specialist.getAllSpecialist(success: { (data) in
            self.hideProgress(progressHUD)
            self.getQuestionAnswer()
        }) { (error, response) in
            self.handleNetworkError(error: error, responseData: response.data)
            self.hideProgress(progressHUD)
        }
    }
    
    @objc func updateSelected(aNotification: Notification) {
        getQuestionAnswer()
    }
    
    func getQuestionAnswer() {
        currentQARequest?.cancel()
        pageNumber = 1
        let progress = showProgress()
        currentQARequest = QuestionAnswer.getAllQuestionAnswer(pageNumber: pageNumber, specialistIDs: [self.currentIndex], success: { (data) in
            self.hideProgress(progress)
            self.questionData = data
        }) { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return questionData.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell", for: indexPath) as! QuestionTableViewCell
        cell.avatarImageView.reset()
        cell.nameLabel.text = ""
        cell.timeLabel.text = ""
        cell.specialistTag.text = ""
        cell.questionLabel.text = ""
        cell.questionTitleLabel.text = ""
        let questionAnswer = questionData[indexPath.section]
        if let askUser = questionAnswer.userAsk, let avatarURL = askUser.getAvatarURL() {
            cell.avatarImageView.imageURL(URL: avatarURL)
        }
        
        if let sumRate = questionAnswer.answer?.sumRate{
            if sumRate != 0{
                cell.moreButton.isHidden = false
                cell.moreButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 18.0)
                let titleBtn = String.fontAwesomeIcon(name: .star)
                cell.moreButton.setTitle(titleBtn, for: UIControlState.normal
                )
            }else{
                cell.moreButton.isHidden = true
            }
        }else{
            cell.moreButton.isHidden = true
        }
        
        if let question = questionAnswer.question {
            cell.nameLabel.text = question.fullname
            cell.questionTitleLabel.text = question.title
            cell.questionLabel.text = question.content
            if let createdDate = question.dateCreate {
                let timeString = "\u{f017} " + createdDate.timeAgoSinceNow()
                let timeAttributedString = NSMutableAttributedString(string: timeString)
                timeAttributedString.addAttributes([NSFontAttributeName:UIFont(font: FontFamily.FontAwesome.regular, size: 14.0)], range: NSMakeRange(0, 1))
                if  questionAnswer.answer != nil {
                    let space = "   "
                    let answered = space + "\u{f058}  Đã trả lời"
                    let answerAttributedString = NSMutableAttributedString(string: answered)
                    answerAttributedString.addAttributes([NSForegroundColorAttributeName:UIColor(hex:"39b54a"), NSFontAttributeName:UIFont(font: FontFamily.FontAwesome.regular, size: 14.0)], range: NSMakeRange(space.count, 1))
                    timeAttributedString.append(answerAttributedString)
                }
                cell.timeLabel.attributedText = timeAttributedString
            }
        }
        if let specialistName = questionAnswer.specialist?.name {
            cell.specialistTag.text = specialistName
        }
        
        let countComment = questionAnswer.question?.countComment ?? 0
        let countLike = questionAnswer.question?.countLike ?? 0
        let countShare = questionAnswer.question?.countShare ?? 0
        
        if let user = self.currentUser{
            if user.id == questionAnswer.userAsk?.id || user.doctorID == questionAnswer.doctor?.id{
                cell.commentButton.isHidden = false
            }else{
                cell.commentButton.isHidden = true
            }
        }else{
            cell.commentButton.isHidden = true
        }
        cell.commentButton.tag = indexPath.section
        cell.commentButton.addTarget(self, action:#selector(handleComment), for: .touchUpInside)
        cell.commentButton.setTitle("  Bình luận" + (countComment>0 ? " (\(countComment))":""), for: .normal)
        cell.shareButton.setTitle("  Chia sẻ" + (countShare>0 ? " (\(countShare))":""), for: .normal)
        cell.commentButton.tag = indexPath.section
        cell.likeBUtton.tag = indexPath.section
        cell.shareButton.tag = indexPath.section
        
        if questionAnswer.liked == false {
            cell.likeBUtton.setAttributedTitle(nil, for: .normal)
            cell.likeBUtton.setTitleColor(UIColor(hex:"9D9D9D"), for: .normal)
            cell.likeBUtton.setTitle("  Thích" + (countLike>0 ? " (\(countLike))":""), for: .normal)
        } else {
            let attributedString = NSMutableAttributedString(string: "  Thích" + (countLike>0 ? " (\(countLike))":""))
            attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor(hex:"1d6edc"), range: NSMakeRange(0, 1))
            cell.likeBUtton.setAttributedTitle(attributedString, for: .normal)
        }
        
        if IS_IPHONE_5{
            cell.moreButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 13.0)
            cell.likeBUtton.titleLabel?.font = UIFont.fontAwesome(ofSize: 13.0)
            cell.shareButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 13.0)
        }
        
        if let userID = currentUser?.id, questionAnswer.userInteractionInfoLoaded == false && questionAnswer.isLoadingUserInteractionInfo == false {
            questionAnswer.getUserInteractionInfo(userID: userID, type: .Question, success: {
                if cell.likeBUtton.tag == indexPath.section {
                    if questionAnswer.liked {
                        let cLike = questionAnswer.question?.countLike ?? 0
                        let attributedString = NSMutableAttributedString(string: "  Thích" + (cLike>0 ? " (\(cLike))":""))
                        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor(hex:"1d6edc"), range: NSMakeRange(0, 1))
                        cell.likeBUtton.setAttributedTitle(attributedString, for: .normal)
                    }
                }
            }, fail: { (error, response) in
                
            })
        }
        
        return cell
    }
    
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let qaDetailVC = StoryboardScene.Main.qaDetail.instantiate()
        let questionAnswer = questionData[indexPath.section]
        qaDetailVC.questionAnswer = questionAnswer
       self.navigationController?.pushViewController(qaDetailVC, animated: true)
    }
    
    func handleComment(sender: UIButton){
        let questionAnswer = questionData[sender.tag]
        selectedIndex = sender.tag
        let questionComment = StoryboardScene.Main.qaComment.instantiate()
        questionComment.questionAnswer = questionAnswer
        self.navigationController?.pushViewController(questionComment, animated: true)
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        self.title = ""
        if segue.identifier == StoryboardSegue.Main.showQuestionAnswerDetail.rawValue {
            if let destinationVC = segue.destination as? QADetailViewController {
                if let cell = sender as? QuestionTableViewCell {
                    if let indexPath = tableView.indexPath(for: cell) {
                        let questionAnswer = questionData[indexPath.section]
                        destinationVC.questionAnswer = questionAnswer
                    }
                }
            }
        }
    }
    
//    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
//        if identifier == StoryboardSegue.Main.showQuestionComments.rawValue {
//                if let user = currentUser, !user.isInvalidated {
//                    return true
//                } else {
//                    return false
//                }
//        }
//        return true
//    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.00001
        }
        return 5.0
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5.0
    }
    
    
    @IBAction func touchSpecialistSelect(_ sender: Any) {
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    deinit {
        notificationToken?.invalidate()
    }
    @IBAction func touchLikeButton(_ sender: Any) {
        if let userID = currentUser?.id, !currentUser!.isInvalidated {
            let index = (sender as! UIButton).tag
            let question = questionData[index]
            question.toggleLikeThank(userID: userID, type: .Question, success: {
                let indexPath = IndexPath(item: 0, section: index)
                self.tableView.reloadRows(at: [indexPath], with: .none)
            }, fail: { (error, response) in
                
            })
        } else {
            confirmLogin(myView: self)
        }
    }
    @IBAction func touchShareButton(_ sender: Any) {
        if let composeVC = SLComposeViewController(forServiceType: SLServiceTypeFacebook) {
            let urlApp = URL(string: Constant.linkAppStore)
            composeVC.add(urlApp)
            composeVC.setInitialText("Viết gì đó...")
            self.present(composeVC, animated: true, completion: nil)
        }
    }
    
    func handleLoginSuccess(aNotification:Notification) {
        let realm = try! Realm()
        currentUser = realm.currentUser()
        tableView.reloadData()
    }
    
    func handleLogout(aNotification:Notification) {
        currentUser = nil
        tableView.reloadData()
    }
    
    @IBAction func touchReveal(_ sender: Any) {
       present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
}
