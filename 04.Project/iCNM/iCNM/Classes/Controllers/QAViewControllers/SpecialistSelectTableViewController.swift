//
//  SpecialistSelectTableViewController.swift
//  iCNM
//
//  Created by Medlatec on 6/26/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift

class SpecialistSelectTableViewController: UITableViewController {
    var notificationToken: NotificationToken? = nil
    let specialists:Results<Specialist> = {
        let realm = try! Realm()
        return realm.objects(Specialist.self).sorted(byKeyPath: "name", ascending: true)
    }()
    
    var newUnCheckBox : UIBarButtonItem!
    var newCheckBox : UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // title navigation
        navigationItem.title = "Tất cả chuyên mục"
        
        // left bar button item - delete all selected check box - uncheck box
        let unCheckBox: UIButton = UIButton(type: .system)
        unCheckBox.titleLabel?.font = UIFont.fontAwesome(ofSize: 28.0)
        let unCheckBoxTitle = String.fontAwesomeIcon(name: .checkSquare)
        unCheckBox.setTitle(unCheckBoxTitle, for: .normal)
        unCheckBox.addTarget(self, action: #selector(delAllSelection), for: .touchUpInside)
        newUnCheckBox = UIBarButtonItem(customView: unCheckBox)
        
        // left bar button item - tick all selected check box - check box
        let checkBox: UIButton = UIButton(type: .system)
        checkBox.titleLabel?.font = UIFont.fontAwesome(ofSize: 28.0)
        let checkBoxTitle = String.fontAwesomeIcon(name: .square)
        checkBox.setTitle(checkBoxTitle, for: .normal)
        checkBox.addTarget(self, action: #selector(tickAllSelection), for: .touchUpInside)
        newCheckBox = UIBarButtonItem(customView: checkBox)
        
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableViewAutomaticDimension
        notificationToken = specialists.observe { [weak self] (changes) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                tableView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                // Query results have changed, so apply them to the UITableView
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.endUpdates()
            case .error(let error):
                fatalError("\(error)")
            }
        }
        
        // set button for left bar
        if specialists.count > 0 {
            self.navigationItem.leftBarButtonItems = [newCheckBox]
        } else {
            self.navigationItem.leftBarButtonItems = [newUnCheckBox]
        }
    }
    
    func delAllSelection() {
        self.navigationItem.setLeftBarButtonItems([newCheckBox], animated: false)
        for specialist in specialists {
            let realm = try! Realm()
            try! realm.write {
                specialist.selected = false
            }
            NotificationCenter.default.post(Notification(name:Constant.NotificationMessage.specialistSelectedChanged))
        }
    }
    
    func tickAllSelection() {
        self.navigationItem.setLeftBarButtonItems([newUnCheckBox], animated: false)
        for specialist in specialists {
            let realm = try! Realm()
            try! realm.write {
                specialist.selected = true
            }
            NotificationCenter.default.post(Notification(name:Constant.NotificationMessage.specialistSelectedChanged))
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return specialists.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SpecialistCell", for: indexPath) as! SpecialistSelectTableViewCell
        let specialist = specialists[indexPath.row]
        cell.specialistTitle.text = specialist.name
        if specialist.selected {
            cell.selectButton.setImage(UIImage(asset: Asset.checkBoxChecked), for: .normal)
        } else {
            cell.selectButton.setImage(UIImage(asset: Asset.checkBoxOutline), for: .normal)
        }
        cell.selectButton.tag = indexPath.row
        cell.selectButton.addTarget(self, action: #selector(selectSpecialist), for: .touchUpInside)
        // Configure the cell...

        return cell
    }
    
    deinit {
        notificationToken?.invalidate()
    }

    func selectSpecialist(_ sender:UIButton) {
        let specialist = specialists[sender.tag]
        let realm = try! Realm()
        try! realm.write {
            specialist.selected = !specialist.selected
        }
        NotificationCenter.default.post(Notification(name:Constant.NotificationMessage.specialistSelectedChanged))
    }
}
