//
//  StepFourViewController.swift
//  iCNM
//
//  Created by Medlatec on 6/7/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class StepFourViewController: UIViewController {
    
    @IBOutlet var txtContent: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func  viewDidLayoutSubviews() {
         self.txtContent.setContentOffset(CGPoint.zero, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchAgreeButton(_ sender: Any) {
        if let window = UIApplication.shared.keyWindow {
            let mainViewController =  StoryboardScene.Main.mainTabBarController.instantiate()
            window.rootViewController = mainViewController
        }
    }
}
