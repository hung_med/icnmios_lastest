//
//  StepTwoViewController.swift
//  iCNM
//
//  Created by Medlatec on 6/7/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class StepTwoViewController: UIViewController {

    @IBOutlet var btnRegister: UIButton!
    @IBOutlet var btnLogin: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnLogin?.layer.cornerRadius = 4
        btnLogin?.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func handleToMainView(_ sender: Any) {
        if let window = UIApplication.shared.keyWindow {
            let mainViewController =  StoryboardScene.Main.mainTabBarController.instantiate()
            window.rootViewController = mainViewController
        }
    }
    
    @IBAction func handleRegister(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pushRegisterVC"), object:nil)
        
    }
    
    @IBAction func handleLogin(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pushLoginVC"), object:nil)
    }
    
}
