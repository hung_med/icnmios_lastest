//
//  OnboardPageViewController.swift
//  iCNM
//
//  Created by Medlatec on 6/7/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
class OnboardPageViewController: UIPageViewController {
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    var index = 0
    lazy var VCArray : [UIViewController] = {
        return [StoryboardScene.OnBoard.stepOne.instantiate(),
                StoryboardScene.OnBoard.stepTwo.instantiate(),
                StoryboardScene.OnBoard.stepThree.instantiate()]
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        dataSource = self
        view.backgroundColor = UIColor.white
        // Do any additional setup after loading the view.
        //setViewControllers([getStepOne()], direction: .forward, animated: false, completion: nil)
        guard let currentViewController = self.VCArray.first  else { return }
        guard let nextViewController = dataSource?.pageViewController(self, viewControllerAfter: currentViewController) else { return }
        setViewControllers([nextViewController], direction: .forward, animated: true, completion: nil)
    
        NotificationCenter.default.addObserver(self, selector: #selector(handleLoginSuccess), name: Constant.NotificationMessage.loginSuccess2, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(pushRegisterVC), name: NSNotification.Name(rawValue: "pushRegisterVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(pushLoginVC), name: NSNotification.Name(rawValue: "pushLoginVC"), object: nil)
        
        Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(moveToNextIntroPage), userInfo: nil, repeats: true)
        
    }
    
    func pushRegisterVC(){
        if let window = UIApplication.shared.delegate?.window {
            if let rootViewController = window?.rootViewController {
                let registerVC = StoryboardScene.LoginRegister.registerViewController.instantiate()
                registerVC.isCheck = true
                let navController = UINavigationController(rootViewController: registerVC)
                rootViewController.present(navController, animated: true, completion: nil)
            }
        }
    }
    
    func pushLoginVC(){
        if let window = UIApplication.shared.delegate?.window {
            if let rootViewController = window?.rootViewController {
                let loginVC = StoryboardScene.LoginRegister.loginViewController.instantiate()
                loginVC.isCheck = true
                let navController = UINavigationController(rootViewController: loginVC)
                if let alreadySomeOneThere = rootViewController.presentedViewController {
                    alreadySomeOneThere.present(navController, animated: true, completion: nil)
                }else {
                    rootViewController.present(navController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func handleLoginSuccess(aNotification:Notification) {
        let realm = try! Realm()
        currentUser = realm.currentUser()
        let userDefault = UserDefaults.standard
        userDefault.set(true, forKey: "agreedTerms")

        if let window = UIApplication.shared.keyWindow {
            let mainViewController =  StoryboardScene.Main.mainTabBarController.instantiate()
            window.rootViewController = mainViewController
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func moveToNextIntroPage(){
        index += 1
        if index < self.VCArray.count {
            setViewControllers([VCArray[index]], direction: .forward, animated: true, completion: nil)
        }
        else {
            index = 0
            setViewControllers([VCArray[0]], direction: .forward, animated: true, completion: nil)
        }
    }
    
    func getStepOne() -> StepOneViewController {
        return StoryboardScene.OnBoard.stepOne.instantiate()
    }
    
    func getStepTwo() -> StepTwoViewController {
        return StoryboardScene.OnBoard.stepTwo.instantiate()
    }
    
    func getStepThree() -> StepThreeViewController {
        return StoryboardScene.OnBoard.stepThree.instantiate()
    }
    
}

// MARK: - UIPageViewControllerDataSource methods

extension OnboardPageViewController : UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if viewController.isKind(of: StepThreeViewController.self) {
            // 1 -> 0
            return getStepTwo()
        } else if viewController.isKind(of: StepTwoViewController.self){
            return getStepOne()
        } else {
            // 0 -> end of the road
            return nil
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if viewController.isKind(of: StepOneViewController.self) {
            // 0 -> 1
            return getStepTwo()
        } else if viewController.isKind(of: StepTwoViewController.self) {
            // 1 -> 2
            return getStepThree()
        } else {
            // 2 -> end of the road
            return nil
        }
    }
    
    // Enables pagination dots
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return 3
    }
    
    // This only gets called once, when setViewControllers is called
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
}

// MARK: - UIPageViewControllerDelegate methods

extension OnboardPageViewController : UIPageViewControllerDelegate {
    
}
