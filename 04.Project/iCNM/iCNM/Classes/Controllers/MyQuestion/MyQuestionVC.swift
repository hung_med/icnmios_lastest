//
//  MyQuestionVC.swift
//  iCNM
//
//  Created by Mac osx on 11/6/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import FontAwesome_swift
import ESPullToRefresh
import PhotoSlider

class MyQuestionVC: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var btnSchedule: UIButton!
    @IBOutlet weak var lblMessage: UILabel!
    var myQuestion: [QuestionAnswer]?
    var listSection:[Int: [String]] = [:]
    var listSpecialistName:[String: [QuestionAnswer]] = [:]
    var storedOffsets = [Int: CGFloat]()
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    fileprivate var pageNumber = 1
    fileprivate var footerScrollView = ESRefreshFooterAnimator(frame:CGRect.zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(MyQuestionVC.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
            
            // right bar button - navigation to ask question
            let newQuestionBtn = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(questionNewQA))
            self.navigationItem.rightBarButtonItem = newQuestionBtn
        }
        title = "Lịch sử câu hỏi"
        myQuestion = [QuestionAnswer]()
        listSection = [Int: [String]]()
        btnSchedule.layer.cornerRadius = 6.0
        btnSchedule.layer.masksToBounds = true
        btnSchedule.isHidden = true
        lblMessage.isHidden = true
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 200
        myTableView.rowHeight = UITableViewAutomaticDimension
        let nib = UINib(nibName: "MyQuestionCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        
        if currentUser != nil {
            if let user = currentUser {
                getMyQuestions(pageID:1, userID: user.id)
            }
        }
        
        footerScrollView.loadingMoreDescription = "Tải thêm"
        self.myTableView.es.addInfiniteScrolling(animator:footerScrollView) { [weak self] in
            
            if let weakSelf = self {
                weakSelf.pageNumber = weakSelf.pageNumber + 1
                QuestionAnswer().getMyQuestions(pageID:weakSelf.pageNumber, userID:(self?.currentUser?.id)!, success: { (data) in
                    weakSelf.myTableView.es.stopLoadingMore()
                    if data.count > 0 {
                        self?.myQuestion?.append(contentsOf: data)
                        self?.listSection = (self?.resultListImageURL(questionAnswer: (self?.myQuestion!)!))!
                        self?.myTableView.reloadData()
                    } else {
                        self?.footerScrollView.loadingMoreDescription = "Đã hết dữ liệu"
                        if self?.myQuestion?.count == 0{
                            self?.footerScrollView.loadingMoreDescription = "Không có bình luận nào"
                        }
                    }
                }, fail: { (error, response) in
                    
                })
            }
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func questionNewQA() {
        if currentUser != nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let createAQuestionVC = storyboard.instantiateViewController(withIdentifier: "CreateQuestionVC") as! CreateQuestionViewController
            self.navigationController?.pushViewController(createAQuestionVC, animated: true)
        }else{
            confirmLogin(myView: self)
        }
    }
    
    @IBAction func scheduleHandler(_ sender: Any) {
        if currentUser != nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let createAQuestionVC = storyboard.instantiateViewController(withIdentifier: "CreateQuestionVC") as! CreateQuestionViewController
            self.navigationController?.pushViewController(createAQuestionVC, animated: true)
        }else{
            confirmLogin(myView: self)
        }
    }
    
    func getMyQuestions(pageID:Int, userID:Int){
        let progress = self.showProgress()
        QuestionAnswer().getMyQuestions(pageID:pageID, userID:userID, success: { (data) in
            if data.count > 0 {
                self.myQuestion?.append(contentsOf: data)
                self.listSection = self.resultListImageURL(questionAnswer: self.myQuestion!)
                self.myTableView.reloadData()
                self.hideProgress(progress)
                self.btnSchedule.isHidden = true
                self.lblMessage.isHidden = true
            } else {
                self.hideProgress(progress)
                self.btnSchedule.isHidden = false
                self.lblMessage.isHidden = false
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (self.myQuestion?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MyQuestionCell
        cell.collectionView.register(UINib(nibName: "MyQuestionThumbCell", bundle: nil), forCellWithReuseIdentifier: "ThumbCell")
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.collectionView.viewWithTag(indexPath.section)
        
        if let str = self.listSection[indexPath.row]{
            if str[0].isEmpty{
                cell.heightConstraint.constant = 0.0
            }else{
                cell.heightConstraint.constant = 65.0
            }
        }else{
            cell.heightConstraint.constant = 0.0
        }
        
        let myQuestionObj = self.myQuestion![indexPath.row]
        if let title = myQuestionObj.question?.title?.trim(){
            cell.lblTitle.text = title
        }

        if let content = myQuestionObj.question?.content?.trim(){
            cell.lblContent.text = content.components(separatedBy: "\n\n").filter { $0 != "" }.joined(separator: "\n")
            cell.lblContent.lineBreakMode = .byWordWrapping
            cell.lblContent.numberOfLines = 0
        }
        if let specialistName = myQuestionObj.specialist?.name{
            cell.lblSpecialName.text = specialistName
        }else{
            cell.lblSpecialName.text = "Chưa cập nhật"
        }
        
        if let dateCreate = myQuestionObj.question?.dateCreate{
            let str = dateCreate.timeAgoSinceNow()
            cell.lblQuestionTo.text = "Hỏi cho tôi " + "\(str)"
            if let userName = myQuestionObj.userAsk?.name, let phone = myQuestionObj.userAsk?.phone, let userName2 = myQuestionObj.question?.fullname, let phone2 = myQuestionObj.question?.phone{
                if userName != userName2 || phone != phone2{
                    cell.lblQuestionTo.text = "Hỏi cho người khác " + "\(str)"
                }else{
                    cell.lblQuestionTo.text = "Hỏi cho tôi " + "\(str)"
                }
            }
        }
    
        if let doctor = myQuestionObj.doctor, let approve = myQuestionObj.answer?.approver, let doctorName = doctor.doctorName, let active_Answer = myQuestionObj.answer?.active{
//            if !active{
//                cell.lblApprove.text = "Câu hỏi đang được duyệt"
//                cell.lblApprove.textColor = UIColor.orange
//            }else
            if myQuestionObj.answer != nil && !approve{
                cell.lblApprove.text = "Câu hỏi đã được duyệt Đang chờ bác sĩ nhận"
                cell.lblApprove.textColor = UIColor.orange
                cell.lblApprove.lineBreakMode = .byWordWrapping
                cell.lblApprove.numberOfLines = 0
            }else if myQuestionObj.question?.expireTime != nil{
                cell.lblApprove.text = "Đang được trả lời bởi bác sĩ \(doctorName)"
                cell.lblApprove.textColor = UIColor.orange
                cell.lblApprove.lineBreakMode = .byWordWrapping
                cell.lblApprove.numberOfLines = 0
            }else if myQuestionObj.answer != nil && approve && !active_Answer{
                cell.lblApprove.text = "Bác sĩ " + doctorName + "đã trả lời\nĐang duyệt câu trả lời"
                cell.lblApprove.textColor = UIColor.orange
                cell.lblApprove.lineBreakMode = .byWordWrapping
                cell.lblApprove.numberOfLines = 0
            }else if myQuestionObj.answer != nil && active_Answer{
                cell.lblApprove?.font = UIFont.fontAwesome(ofSize: 13.0)
                if let approveTime = myQuestionObj.answer?.approveTime{
                    cell.lblApprove.text = String.fontAwesomeIcon(name: .checkCircle) + " Đã có câu trả lời \(approveTime.timeAgoSinceNow())"
                }else{
                    cell.lblApprove.text = String.fontAwesomeIcon(name: .checkCircle) +  " Đã có câu trả lời"
                }
                
                cell.lblApprove.textColor = UIColor.blue
                cell.lblApprove.lineBreakMode = .byWordWrapping
                cell.lblApprove.numberOfLines = 0
            }
        }else{
            cell.lblApprove.text = "Câu hỏi đang được duyệt"
            cell.lblApprove.textColor = UIColor.orange
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myQuestionObj = self.myQuestion![indexPath.row]
        if let approve = myQuestionObj.answer?.approver{
            if approve && myQuestionObj.answer != nil{
                if let questionID = myQuestionObj.question?.id{
                    let questions = QuestionAnswer()
                    questions.getAnswerIDFromQuestionID(questionID: questionID, success: {  (result) in
                        if result == nil{
                            return
                        }else{
                            questions.question = result?.question
                            questions.answer = result?.answer
                            questions.userAsk = result?.userAsk
                            questions.userOfDoctor = result?.userOfDoctor
                            questions.doctor = result?.doctor
                            questions.specialist = result?.specialist
                            questions.thanked = (result?.thanked)!
                            questions.liked = (result?.liked)!
                            questions.shared = (result?.shared)!
                            
                            let qADetailVC = StoryboardScene.Main.qaDetail.instantiate()
                            qADetailVC.questionAnswer = questions
                            self.navigationController?.pushViewController(qADetailVC, animated: true)
                        }
                    }, fail: { (error, response) in
                        
                    })
                }
            }else{
                showAlertView(title: "Câu hỏi của bạn đang ở trạng thái chờ bác sĩ trả lời!", view: self)
                return
            }
        }else{
            showAlertView(title: "Câu hỏi của bạn đang ở trạng thái chờ bác sĩ trả lời!", view: self)
            return
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? MyQuestionCell else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? MyQuestionCell else { return }
        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
    
    func resultListImageURL(questionAnswer:[QuestionAnswer])->[Int: [String]]{
        var result = [Int: [String]]()
        for i in 0..<Int(questionAnswer.count){
            let urlString = questionAnswer[i].question?.questionImage?.components(separatedBy: ";")
            if urlString != nil{
                for url in urlString! {
                    if !url.isEmpty || url != ""{
                        let urlString = url.trimmingCharacters(in: .whitespaces)
                        if let questionID = questionAnswer[i].question?.id{
                            let urlBase = API.baseURLImage + "\(API.iCNMImage)" +
                                "QuestionImage/\(questionID)/" + "\(urlString)"
                            if result[i] == nil {
                                result[i] = [String]()
                            }
                            result[i]?.append(urlBase)
                        }
                    }
                }
            }
        }
        return result
        
    }
}

extension MyQuestionVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        let tag = collectionView.tag
        if tag >= self.listSection.count{
            return 0
        }
        if let result = listSection[tag]{
            return result.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThumbCell", for: indexPath) as! MyQuestionThumbCell
       // cell.contentView.layer.cornerRadius = 4.0
        cell.contentView.layer.masksToBounds = true
        cell.contentView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell.contentView.layer.borderWidth = 1.0
        
        let tag = collectionView.tag
        let imgStr = self.listSection[tag]![indexPath.row]
        cell.imageThumb.contentMode = .scaleToFill
        if imgStr.range(of:".jpg") != nil {
            cell.imageThumb.loadImageUsingUrlString(urlString: imgStr)
        }else{
            cell.imageThumb.loadImageUsingUrlString(urlString: imgStr + ".jpg")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let tag = collectionView.tag
        let urlString = self.listSection[tag]![indexPath.row]
//        let imageView = CustomImageView()
        let url = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if url?.range(of:".jpg") != nil {
            if let imgUrl = URL(string: url!) {
//                imageView.loadImageUsingUrlString(urlString: url!)
                let photoSlider = PhotoSlider.ViewController(imageURLs: [imgUrl])
                photoSlider.modalPresentationStyle = .overCurrentContext
                photoSlider.modalTransitionStyle = .crossDissolve
                present(photoSlider, animated: true, completion: nil)
            }
        } else{
            if let imgUrl = URL(string: url! + ".jpg") {
//                imageView.loadImageUsingUrlString(urlString: url! + ".jpg")
                let photoSlider = PhotoSlider.ViewController(imageURLs: [imgUrl])
                photoSlider.modalPresentationStyle = .overCurrentContext
                photoSlider.modalTransitionStyle = .crossDissolve
                present(photoSlider, animated: true, completion: nil)
            }
        }
        
//        let frame = CGRect(x: 0, y: 0, width: screenSizeWidth, height: screenSizeHeight)
//        imageView.frame = frame
//        imageView.contentMode = .scaleAspectFit
//        imageView.backgroundColor = .black
//        imageView.isUserInteractionEnabled = true
//        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
//        imageView.addGestureRecognizer(tap)
//        var height:CGFloat = 0.0
//        if IS_IPHONE_X{
//            height = 76.0
//        }else{
//            height = CGFloat((self.navigationController?.navigationBar.frame.size.height)!)
//        }
//        let imageClose = UIImageView(frame: CGRect(x: screenSizeWidth-40, y: height+25, width: 25, height: 25))
//        let image = UIImage(named: "icon_close")?.maskWithColor(color: UIColor(hexColor: 0xFF1E17, alpha: 1.0))
//        imageClose.image = image
//        imageView.addSubview(imageClose)
//        self.navigationController?.navigationBar.layer.zPosition = -1
////        self.view.addSubview(imageView)
    }
    
    // Use to back from full mode
    func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let view = sender.view!
        view.removeFromSuperview()
        self.navigationController?.navigationBar.layer.zPosition = 0
    }
}
