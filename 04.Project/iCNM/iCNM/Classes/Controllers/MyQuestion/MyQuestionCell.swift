//
//  MyQuestionCell.swift
//  iCNM
//
//  Created by Mac osx on 11/22/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class MyQuestionCell: UITableViewCell {

    @IBOutlet weak var imageThumb:CustomImageView!
    @IBOutlet weak var lblTitle: FWTagLabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblSpecialName: FWTagLabel!
    @IBOutlet weak var lblQuestionTo: UILabel!
    @IBOutlet weak var lblApprove: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
     @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let f = contentView.frame
        let fr = UIEdgeInsetsInsetRect(f, UIEdgeInsetsMake(15, 15, 15, 15))
        contentView.frame = fr
        addShadow(cell: self)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func addShadow(cell:UITableViewCell) {
        cell.layer.cornerRadius = 4
        cell.layer.masksToBounds = true
        
        cell.layer.masksToBounds = false
        cell.layer.shadowOffset = CGSize(width: 0, height:0)
        cell.layer.shadowColor = UIColor.darkGray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowRadius = 2
    }
}

extension MyQuestionCell {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.layer.cornerRadius = 4.0
        collectionView.layer.cornerRadius = 4.0
        collectionView.backgroundColor = UIColor.white
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
}
