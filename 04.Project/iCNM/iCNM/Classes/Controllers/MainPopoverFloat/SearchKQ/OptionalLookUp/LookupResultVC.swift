//
//  LookupResultVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Alamofire

protocol LookupResultVCDelegate {
    func gobackMainView()
    func dismissResultVC()
    func showListLookUpResultbyPID(pid:String, organizeID:String)
    func showListLookUpResultbyPhone(phone:String)
    func showListLookUpResultBySID(pid:String, sid:String, organizeID:String)
    func showListLookUpUnit(user:String, maBS:String, currentMonth:String, currentYear:String, organizeID:String)
}

class LookupResultVC: BaseViewController, UITextFieldDelegate,SSRadioButtonControllerDelegate,CustomComboxBoxListDelegate, UIPopoverPresentationControllerDelegate, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var lblMessageText: UILabel!
    @IBOutlet weak var lblPassword: UITextField!
    @IBOutlet weak var lblPID: UITextField!
    
    @IBOutlet weak var buttonExit: UIButton!
    @IBOutlet weak var buttonDone: UIButton!
    @IBOutlet weak var lblWarning: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var myView: UIView!
    
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var btnLookupPhone: UIButton!
    @IBOutlet weak var lblSelectDay: UILabel!
    @IBOutlet weak var formatDate: UIButton!
    @IBOutlet weak var comboboxBtn: UIButton!
    @IBOutlet weak var btnRecent: UIButton!
    @IBOutlet weak var btnRecentLabel: UIButton!
    @IBOutlet weak var btnHelp: UIButton!
    var radioButtonController: SSRadioButtonsController?
    var organizeID:Int?=0
    var currentSelectType:Int?=1
    var delegate:LookupResultVCDelegate?
    var currentUser:User? = nil
    private var currentRequest:Request?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        myView.layer.cornerRadius = 10
        myView.layer.borderWidth = 1
        myView.layer.masksToBounds = true
        buttonDone.layer.cornerRadius = 4
        buttonDone.layer.masksToBounds = true
        buttonExit.layer.cornerRadius = 4
        buttonExit.layer.masksToBounds = true
        comboboxBtn.layer.borderWidth = 1
        comboboxBtn.layer.borderColor = UIColor.lightGray.cgColor
        comboboxBtn.layer.cornerRadius = 4
        comboboxBtn.layer.masksToBounds = true
        
        if IS_IPHONE_5{
            self.lblTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        }
        self.lblPID.delegate = self
        self.lblPassword.delegate = self
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        lblMessageText.lineBreakMode = .byWordWrapping
        lblMessageText.numberOfLines=0
        lblMessageText.sizeToFit()
        lblMessageText.adjustsFontSizeToFitWidth = true
        lblMessageText.text = Constant.DEFIND_PID
        self.organizeID = 1
        self.lblPID.placeholder = "Nhập mã thẻ KH"
        radioButtonController = SSRadioButtonsController(buttons: button1, button2, button3)
        radioButtonController!.delegate = self
        radioButtonController!.shouldLetDeSelect = true
        lblSelectDay.isHidden = true
        formatDate.isHidden = true
        self.btnLookupPhone.titleLabel?.font = UIFont.fontAwesome(ofSize: 15.0)
        let title = String.fontAwesomeIcon(name: .phoneSquare) + "  Tra cứu theo số điện thoại"
        btnLookupPhone.setTitle(title, for: .normal)
        self.searchBar.isHidden = true
        if IS_IPHONE_5{
            self.comboboxBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12.0)
            self.btnLookupPhone.titleLabel?.font = UIFont.fontAwesome(ofSize: 13.0)
        }
        self.comboboxBtn.setTitle("BVĐK MEDLATEC - 42 Nghĩa Dũng", for: UIControlState.normal)
        
        let userDefaults = UserDefaults.standard
        if let currentSelectType:Int = userDefaults.value(forKey: "currentSelectType") as? Int{
            btnRecentLabel.isHidden = false
            if currentSelectType == 1{
                let pid = userDefaults.value(forKey: "pid") as! String
                btnRecent.setTitle(" PID: \(pid)", for: UIControlState.normal)
            }else if currentSelectType == 2{
                let sid = userDefaults.value(forKey: "sid") as! String
                btnRecent.setTitle(" SID: \(sid)", for: UIControlState.normal)
            }else if currentSelectType == 3{
                let user = userDefaults.value(forKey: "user") as! String
                btnRecent.setTitle(" Đơn vị: \(user)", for: UIControlState.normal)
            }
        }
        else{
            btnRecentLabel.isHidden = true
            btnRecent.setTitle("", for: UIControlState.normal)
        }
    }
    
    @IBAction func showListUnit(_ sender: Any) {

    }
    
    func gobackView(organizeId:Int, name:String){
        organizeID = organizeId
        comboboxBtn.setTitle(name, for: UIControlState.normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if IS_IPAD{
            let heightConstraint = NSLayoutConstraint(
                item: myView,
                attribute: NSLayoutAttribute.height,
                relatedBy: NSLayoutRelation.equal,
                toItem: nil,
                attribute: NSLayoutAttribute.height,
                multiplier: 1,
                constant: 600
            )
            self.view.addConstraint(heightConstraint)
            
            let widthConstraint = NSLayoutConstraint(
                item: myView,
                attribute: NSLayoutAttribute.width,
                relatedBy: NSLayoutRelation.equal,
                toItem: nil,
                attribute: NSLayoutAttribute.width,
                multiplier: 1,
                constant: 480
            )
            self.view.addConstraint(widthConstraint)
            
            let leftConstraint = NSLayoutConstraint(
                item: myView,
                attribute: NSLayoutAttribute.left,
                relatedBy: NSLayoutRelation.equal,
                toItem: self.view,
                attribute: NSLayoutAttribute.left,
                multiplier: 1,
                constant: screenSizeWidth/2 - 240
            )
            self.view.addConstraint(leftConstraint)
            
            let bottomConstraint = NSLayoutConstraint(
                item: myView,
                attribute: NSLayoutAttribute.bottom,
                relatedBy: NSLayoutRelation.equal,
                toItem: self.view,
                attribute: NSLayoutAttribute.bottom,
                multiplier: 1,
                constant: screenSizeHeight/2-600
            )
            self.view.addConstraint(bottomConstraint)
            
        }else{
            if IS_IPHONE_5{
                button1.translatesAutoresizingMaskIntoConstraints = false
                button2.translatesAutoresizingMaskIntoConstraints = false
                button3.translatesAutoresizingMaskIntoConstraints = false
                let heightConstraint0 = NSLayoutConstraint(
                    item: myView,
                    attribute: NSLayoutAttribute.height,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: nil,
                    attribute: NSLayoutAttribute.height,
                    multiplier: 1,
                    constant: 450
                )
                self.view.addConstraint(heightConstraint0)
                
                let topConstraint0 = NSLayoutConstraint(
                    item: myView,
                    attribute: NSLayoutAttribute.top,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: self.view,
                    attribute: NSLayoutAttribute.top,
                    multiplier: 1,
                    constant: 60
                )
                self.view.addConstraint(topConstraint0)
                
                
                let heightConstraint = NSLayoutConstraint(
                    item: comboboxBtn,
                    attribute: NSLayoutAttribute.height,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: nil,
                    attribute: NSLayoutAttribute.height,
                    multiplier: 1,
                    constant: 30
                )
                myView.addConstraint(heightConstraint)
                
                let topConstraint = NSLayoutConstraint(
                    item: button1,
                    attribute: NSLayoutAttribute.top,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: comboboxBtn,
                    attribute: NSLayoutAttribute.top,
                    multiplier: 1,
                    constant: 50
                )
                myView.addConstraint(topConstraint)
                
                let topConstraint_1 = NSLayoutConstraint(
                    item: button2,
                    attribute: NSLayoutAttribute.top,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: comboboxBtn,
                    attribute: NSLayoutAttribute.top,
                    multiplier: 1,
                    constant: 50
                )
                myView.addConstraint(topConstraint_1)
                
                let topConstraint_2 = NSLayoutConstraint(
                    item: button3,
                    attribute: NSLayoutAttribute.top,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: comboboxBtn,
                    attribute: NSLayoutAttribute.top,
                    multiplier: 1,
                    constant: 50
                )
                myView.addConstraint(topConstraint_2)
                
                let widthConstraint0 = NSLayoutConstraint(
                    item: button1,
                    attribute: NSLayoutAttribute.width,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: myView,
                    attribute: NSLayoutAttribute.width,
                    multiplier: 1,
                    constant: 40
                )
                myView.addConstraint(widthConstraint0)
                
                let leftConstraint = NSLayoutConstraint(
                    item: button2,
                    attribute: NSLayoutAttribute.left,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: myView,
                    attribute: NSLayoutAttribute.left,
                    multiplier: 1,
                    constant: 110
                )
                myView.addConstraint(leftConstraint)
                
                let widthConstraint = NSLayoutConstraint(
                    item: button2,
                    attribute: NSLayoutAttribute.width,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: myView,
                    attribute: NSLayoutAttribute.width,
                    multiplier: 1,
                    constant: 40
                )
                myView.addConstraint(widthConstraint)
                
                let leftConstraint1 = NSLayoutConstraint(
                    item: button3,
                    attribute: NSLayoutAttribute.left,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: myView,
                    attribute: NSLayoutAttribute.left,
                    multiplier: 1,
                    constant: 180
                )
                myView.addConstraint(leftConstraint1)
                
                let widthConstraint1 = NSLayoutConstraint(
                    item: button3,
                    attribute: NSLayoutAttribute.width,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: myView,
                    attribute: NSLayoutAttribute.width,
                    multiplier: 1,
                    constant: 55
                )
                myView.addConstraint(widthConstraint1)
                
                let bottomConstraint1 = NSLayoutConstraint(
                    item: btnRecent,
                    attribute: NSLayoutAttribute.top,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: myView,
                    attribute: NSLayoutAttribute.top,
                    multiplier: 1,
                    constant: 380
                )
                myView.addConstraint(bottomConstraint1)
                
                let bottomConstraint2 = NSLayoutConstraint(
                    item: btnRecentLabel,
                    attribute: NSLayoutAttribute.top,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: myView,
                    attribute: NSLayoutAttribute.top,
                    multiplier: 1,
                    constant: 360
                )
                myView.addConstraint(bottomConstraint2)
                
                let leftConstraint3 = NSLayoutConstraint(
                    item: buttonExit,
                    attribute: NSLayoutAttribute.left,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: myView,
                    attribute: NSLayoutAttribute.left,
                    multiplier: 1,
                    constant: 110
                )
                myView.addConstraint(leftConstraint3)
                
                let rightConstraint3 = NSLayoutConstraint(
                    item: buttonDone,
                    attribute: NSLayoutAttribute.right,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: myView,
                    attribute: NSLayoutAttribute.right,
                    multiplier: 1,
                    constant: -15
                )
                myView.addConstraint(rightConstraint3)
                
                let widthConstraint3 = NSLayoutConstraint(
                    item: buttonDone,
                    attribute: NSLayoutAttribute.width,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: myView,
                    attribute: NSLayoutAttribute.width,
                    multiplier: 1/7,
                    constant: 30
                )
                myView.addConstraint(widthConstraint3)
                
                let widthConstraint4 = NSLayoutConstraint(
                    item: buttonExit,
                    attribute: NSLayoutAttribute.width,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: myView,
                    attribute: NSLayoutAttribute.width,
                    multiplier: 1/8,
                    constant: 30
                )
                myView.addConstraint(widthConstraint4)
                
                let leftConstraint5 = NSLayoutConstraint(
                    item: btnHelp,
                    attribute: NSLayoutAttribute.left,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: myView,
                    attribute: NSLayoutAttribute.left,
                    multiplier: 1,
                    constant: 0
                )
                myView.addConstraint(leftConstraint5)
                
                let widthConstraint5 = NSLayoutConstraint(
                    item: btnHelp,
                    attribute: NSLayoutAttribute.width,
                    relatedBy: NSLayoutRelation.equal,
                    toItem: myView,
                    attribute: NSLayoutAttribute.width,
                    multiplier: 1/5,
                    constant: 64
                )
                myView.addConstraint(widthConstraint5)
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.lblPID.resignFirstResponder()
        self.lblPassword.resignFirstResponder()
        return true
    }
    
    func didSelectButton(selectedButton: UIButton?)
    {
      //  NSLog(" \(String(describing: selectedButton?.tag))" )
        if selectedButton?.tag == 0 {
            lblSelectDay.isHidden = true
            formatDate.isHidden = true
            btnLookupPhone.isHidden = false
            currentSelectType = 1
            self.lblPID.placeholder = "Nhập mã thẻ KH"
            self.lblMessageText.text = Constant.DEFIND_PID
            self.lblWarning.text = ""
        }else if selectedButton?.tag == 1{
            lblSelectDay.isHidden = false
            formatDate.isHidden = false
            btnLookupPhone.isHidden = true
            currentSelectType = 2
            self.lblPID.placeholder = "Nhập mã tra cứu"
            self.lblMessageText.text = Constant.DEFIND_SID
            self.lblWarning.text = ""
        }else{
            lblSelectDay.isHidden = true
            formatDate.isHidden = true
            btnLookupPhone.isHidden = true
            currentSelectType = 3
            self.lblPID.placeholder = "Nhập mã đơn vị"
            self.lblMessageText.text = Constant.DEFIND_Donvi
            self.lblWarning.text = ""
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSelectDay(_ sender: Any) {
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.year = -100
        let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        DatePickerDialog().show("Chọn ngày", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .date) { (date) in
            if let dateString = date {
                let formatDateStr = formatter.string(from: dateString)
                self.formatDate .setTitle(formatDateStr, for: UIControlState.normal)
            }
        }
    }
    
    @IBAction func btnDone(_ sender: Any) {
        
        if organizeID == 0{
            self.lblWarning.text = "Vui lòng chọn đơn vị y tế"
            self.lblWarning.lineBreakMode = .byWordWrapping
            self.lblWarning.numberOfLines = 0
            self.lblWarning.adjustsFontSizeToFitWidth = true
            return
        }else if currentSelectType == 2{
            if (self.formatDate.title(for: UIControlState.normal) == "dd/MM/yyyy"){
                self.lblWarning.text = "Vui lòng chọn ngày để tra cứu kết quả"
                return
            }else{
                if let pid = self.lblPID.text, pid.isEmpty{
                    self.lblWarning.text = "Vui lòng chọn và nhập đầy đủ thông tin"
                    self.lblWarning.lineBreakMode = .byWordWrapping
                    self.lblWarning.numberOfLines = 0
                    self.lblWarning.adjustsFontSizeToFitWidth = true
                    return
                } else if let pwd = self.lblPassword.text, pwd.isEmpty {
                    self.lblWarning.text = "Vui lòng chọn và nhập đầy đủ thông tin"
                    self.lblWarning.lineBreakMode = .byWordWrapping
                    self.lblWarning.numberOfLines = 0
                    self.lblWarning.adjustsFontSizeToFitWidth = true
                    return
                }else {
                    if let organizeID = organizeID, let sid = self.lblPID.text, let pwd = self.lblPassword.text, let date = self.formatDate.title(for: UIControlState.normal){
                        let splitText = date.components(separatedBy: "/")
                        let year: String = (splitText[2] as NSString).substring(from: 2)
                        let sIDPlusString = "\(splitText[0])\(splitText[1])\(year)\("-")\(sid)"
                        
                        self.checkPatientResult(type:currentSelectType!, organizeID: organizeID, pid: "", sid:sIDPlusString, pwd: pwd)
                    }
                }
            }                
        } else if currentSelectType == 1{
            if let pid = self.lblPID.text, pid.isEmpty{
                self.lblWarning.text = "Vui lòng chọn và nhập đầy đủ thông tin"
                self.lblWarning.lineBreakMode = .byWordWrapping
                self.lblWarning.numberOfLines = 0
                self.lblWarning.adjustsFontSizeToFitWidth = true
                return
            } else if let pwd = self.lblPassword.text, pwd.isEmpty {
                self.lblWarning.text = "Vui lòng chọn và nhập đầy đủ thông tin"
                self.lblWarning.lineBreakMode = .byWordWrapping
                self.lblWarning.numberOfLines = 0
                self.lblWarning.adjustsFontSizeToFitWidth = true
                return
            }  else {
                if let organizeID = organizeID, let pid = self.lblPID.text, let pwd = self.lblPassword.text{
                    self.checkPatientResult(type:currentSelectType!, organizeID: organizeID, pid: pid, sid: "", pwd: pwd)
                }
            }
        }else{
            if let pid = self.lblPID.text, pid.isEmpty{
                self.lblWarning.text = "Vui lòng chọn và nhập đầy đủ thông tin"
                self.lblWarning.lineBreakMode = .byWordWrapping
                self.lblWarning.numberOfLines = 0
                self.lblWarning.adjustsFontSizeToFitWidth = true
                return
            } else if let pwd = self.lblPassword.text, pwd.isEmpty {
                self.lblWarning.text = "Vui lòng chọn và nhập đầy đủ thông tin"
                self.lblWarning.lineBreakMode = .byWordWrapping
                self.lblWarning.numberOfLines = 0
                self.lblWarning.adjustsFontSizeToFitWidth = true
                return
            }  else {
                if let organizeID = organizeID, let user = self.lblPID.text, let pwd = self.lblPassword.text{
                    
                    self.checkPatientResult(type:currentSelectType!, organizeID: organizeID, pid: user, sid: "", pwd: pwd)
                }
            }
        }
    }
    
    func checkPatientResult(type:Int, organizeID:Int, pid:String, sid:String, pwd:String){
        let progress = self.showProgress()
        let checkPatient = CheckPatient()
        currentRequest?.cancel()
        currentRequest = checkPatient.checkPatientResult(type:type-1, organizeID:String(organizeID), pid: pid, sid: sid, password: pwd, success: { (result) in
            self.hideProgress(progress)
            if (result?.doctorID.isEmpty)! && result?.doctorID == ""{
                self.lblWarning.text = "Mã bệnh nhân của bạn không tồn tại trên hệ thống iCNM"
                self.lblWarning.lineBreakMode = .byWordWrapping
                self.lblWarning.numberOfLines = 0
                self.lblWarning.adjustsFontSizeToFitWidth = true
                return
            }else{
                self .dismiss(animated: true) {
                    let userDefaults = UserDefaults.standard
                    if self.currentSelectType == 1{
                        self.delegate?.showListLookUpResultbyPID(pid: pid, organizeID: String(organizeID))
                    
                        if (userDefaults.object(forKey: "currentSelectType") != nil){
                            userDefaults.removeObject(forKey: "currentSelectType")
                        }
                        userDefaults.set(self.currentSelectType, forKey: "currentSelectType")
                    
                        if (userDefaults.object(forKey: "organizeID") != nil){
                            userDefaults.removeObject(forKey: "organizeID")
                        }
                        userDefaults.set(String(organizeID), forKey: "organizeID")
                    
                        if (userDefaults.object(forKey: "pid") != nil){
                            userDefaults.removeObject(forKey: "pid")
                        }
                        userDefaults.set(pid, forKey: "pid")
                    
                    }else if self.currentSelectType == 2{
                        self.delegate?.showListLookUpResultBySID(pid: pid, sid: sid, organizeID: String(organizeID))
                    
                        if (userDefaults.object(forKey: "currentSelectType") != nil){
                            userDefaults.removeObject(forKey: "currentSelectType")
                        }
                        userDefaults.set(self.currentSelectType, forKey: "currentSelectType")
                    
                        if (userDefaults.object(forKey: "organizeID") != nil){
                            userDefaults.removeObject(forKey: "organizeID")
                        }
                        userDefaults.set(String(organizeID), forKey: "organizeID")
                    
                        if (userDefaults.object(forKey: "pid") != nil){
                            userDefaults.removeObject(forKey: "pid")
                        }
                        userDefaults.set(pid, forKey: "pid")
                    
                        if (userDefaults.object(forKey: "sid") != nil){
                            userDefaults.removeObject(forKey: "sid")
                        }
                        userDefaults.set(sid, forKey: "sid")
                    }else{
                        let doctorID = result?.doctorID
                        let userID = result?.userID
                        let date = Date()
                        let calendar = Calendar.current
                        let month = calendar.component(.month, from: date)
                        let year = calendar.component(.year, from: date)
                        
                        self.delegate?.showListLookUpUnit(user:userID!, maBS:doctorID!, currentMonth: String(month), currentYear: String(year), organizeID: String(organizeID))
                        
                        let userDefaults = UserDefaults.standard
                        if (userDefaults.object(forKey: "currentSelectType") != nil){
                            userDefaults.removeObject(forKey: "currentSelectType")
                        }
                        if (userDefaults.object(forKey: "user") != nil){
                            userDefaults.removeObject(forKey: "user")
                        }
                        if (userDefaults.object(forKey: "maBS") != nil){
                            userDefaults.removeObject(forKey: "maBS")
                        }
                        if (userDefaults.object(forKey: "month") != nil){
                            userDefaults.removeObject(forKey: "month")
                        }
                        if (userDefaults.object(forKey: "year") != nil){
                            userDefaults.removeObject(forKey: "year")
                        }
                        if (userDefaults.object(forKey: "organizeID") != nil){
                            userDefaults.removeObject(forKey: "organizeID")
                        }
                        userDefaults.set(self.currentSelectType, forKey: "currentSelectType")
                        userDefaults.set(pid, forKey: "user")
                        userDefaults.set(doctorID, forKey: "maBS")
                        userDefaults.set(String(month), forKey: "month")
                        userDefaults.set(String(year), forKey: "year")
                        userDefaults.set(String(organizeID), forKey: "organizeID")
                    }
                }
            }
            
        }) { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        }
    }
    
    @IBAction func btnExit(_ sender: Any) {
        self .dismiss(animated: true) { 
            self.delegate?.gobackMainView()
        }
    }
    
    @IBAction func btnRecentClick(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = true
        let userDefaults = UserDefaults.standard
        let currentSelectType:Int = userDefaults.value(forKey: "currentSelectType") as! Int
        if currentSelectType == 1{
            self .dismiss(animated: true) {
                let organizeID = userDefaults.value(forKey: "organizeID") as! String
                let pID = userDefaults.value(forKey: "pid") as! String
                self.delegate?.showListLookUpResultbyPID(pid: pID, organizeID: organizeID)
            }
        }else if currentSelectType == 2{
            self .dismiss(animated: true) {
                let organizeID = userDefaults.value(forKey: "organizeID") as! String
                let pID = userDefaults.value(forKey: "pid") as! String
                let sID = userDefaults.value(forKey: "sid") as! String
                self.delegate?.showListLookUpResultBySID(pid: pID, sid: sID, organizeID:organizeID)
            }
        }else if currentSelectType == 3{
            self .dismiss(animated: true) {
                let date = Date()
                let calendar = Calendar.current
                let organizeID = userDefaults.value(forKey: "organizeID") as! String
                let user = userDefaults.value(forKey: "user") as! String
                let month = calendar.component(.month, from: date)
                let year = calendar.component(.year, from: date)
                let maBS = userDefaults.value(forKey: "maBS") as! String
                self.delegate?.showListLookUpUnit(user:user, maBS:maBS, currentMonth:String(month), currentYear:String(year), organizeID: organizeID)
            }
        }
    }
    
    @IBAction func btnHelp(_ sender: Any) {
        let helpPageVC = HelpPageViewController(nibName: "HelpPageViewController", bundle: nil)
        helpPageVC.modalPresentationStyle = .custom
        helpPageVC.transitioningDelegate = self
        self.present(helpPageVC, animated: true, completion: nil)
    }
    
    @IBAction func btnLookupResultPhoneNumber(_ sender: Any) {
        if currentUser == nil{
            self .dismiss(animated: true) {
                self.delegate?.dismissResultVC()
            }
        }else{
            self .dismiss(animated: true) {
                if let phone = self.currentUser?.userDoctor?.userInfo?.phone{
                    self.delegate?.showListLookUpResultbyPhone(phone: phone)
                }
            }
        }
    }
    
    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController!, sourceViewController source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController: presented, presenting: presentingViewController)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}
