//
//  CustomComboxBoxList.swift
//  iCNM
//
//  Created by Mac osx on 8/4/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

protocol CustomComboxBoxListDelegate {
    func gobackView(organizeId:Int, name:String)
   // func getDefaultOrganizeName(name:String)
}

class CustomComboxBoxList: UITableViewController {
    
    @IBOutlet var myTableView: UITableView!
    var medicalUnits: [MedicalUnit]?
    var medicalUnits2: [String]?
    var organizeID:Int?=0
    var numberRow:Int? = 0
    var delegate:CustomComboxBoxListDelegate?
    var id:Int? = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        myTableView.delegate = self
        myTableView.dataSource = self
        let nib = UINib(nibName: "CustomComboxCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        if id == 1{
            getMedicalUnit()
        }else if id == 9{
            medicalUnits2 = ["BCG", "VGB Sơ sinh", "DPT-VGB-Hib 1", "DPT-VGB-Hib 2", "DPT-VGB-Hib 3", "Bại liệt 1", "Bại liệt 2", "Bại liệt 3", "Sởi 1", "Sởi 2", "DPT4", "VNNB B1", "VNNB B2", "VNNB B3"]
        }else if id == 10{
            medicalUnits2 = ["Tả 1", "Tả 2", "Quai bị 1", "Quai bị 2", "Quai bị 3", "Cúm 1", "Cúm 2", "Cúm 3", "Thương hàn", "HPV 1", "HPV 2", "HPV 3", "Vắc xin phế cầu khuẩn"]
        }else if id == 11{
            medicalUnits2 = ["UV1", "UV2", "UV3", "UV4", "UV5"]
        }else{
            
        }
    }
    
    func getMedicalUnit(){
        self.medicalUnits = [MedicalUnit]()
        MedicalUnit.getAllMedicalUnit(success: { (data) in
            if data.count > 0 {
                self.medicalUnits?.append(contentsOf: data)
                self.numberRow = self.medicalUnits?.count
                self.myTableView.reloadData()
                
                if (self.medicalUnits?.count)! > 0{
                    self.organizeID = self.medicalUnits?[0].organizeID
                }
            } else {
                //self?.tableView.es_noticeNoMoreData()
            }
        }, fail: { (error, response) in
            // self.handleNetworkError(error: error, responseData: response.data)
        })
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if id == 1{
            return (self.medicalUnits?.count)!
        }else{
            return (self.medicalUnits2?.count)!
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomComboxCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        if id == 1{
            let medicalUnit = self.medicalUnits?[indexPath.row]
            cell.lblName.text = medicalUnit?.organizeName
        }else{
            let name = self.medicalUnits2?[indexPath.row]
            cell.lblName.text = name
        }
        
        cell.lblName.font = UIFont.systemFont(ofSize: 13)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if id == 1{
            if let medicalUnit = self.medicalUnits?[indexPath.row]{
                self.delegate?.gobackView(organizeId: medicalUnit.organizeID, name: medicalUnit.organizeName)
                self.dismiss(animated: true, completion: nil)
            }
        }else{
            if let name = self.medicalUnits2?[indexPath.row]{
                self.delegate?.gobackView(organizeId: 0, name: name)
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 30
    }
}
