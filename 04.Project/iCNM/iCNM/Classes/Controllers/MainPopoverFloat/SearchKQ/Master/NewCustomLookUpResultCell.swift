//
//  CustomLookUpResultCell.swift
//  iCNM
//
//  Created by Mac osx on 7/14/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class NewCustomLookUpResultCell: UITableViewCell {

    @IBOutlet weak var lineView2: UIView!
    //@IBOutlet weak var lineView: UIImageView!
    @IBOutlet weak var groupView: UIView!
    
    @IBOutlet weak var lblLyDoKham: UILabel!
    @IBOutlet weak var lblBacSi: UILabel!
    @IBOutlet weak var lblDiaDiem: UILabel!
    @IBOutlet weak var lblKetLuan: UILabel!
    @IBOutlet weak var lblDanDo: UILabel!
    @IBOutlet weak var lblHenTaiKham: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    
    @IBOutlet weak var lblMoney: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         self.selectionStyle = .none
         let f = contentView.frame
         let fr = UIEdgeInsetsInsetRect(f, UIEdgeInsetsMake(10, 10, 10, 15))
         contentView.frame = fr
         addShadow(cell: self)
        btnDelete.layer.cornerRadius = 8
        btnDelete.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func addShadow(cell:UITableViewCell) {
        cell.layer.cornerRadius = 4
        cell.layer.masksToBounds = true
        cell.layer.shadowOffset = CGSize(width: 0, height:0)
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.20
        cell.layer.shadowRadius = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
