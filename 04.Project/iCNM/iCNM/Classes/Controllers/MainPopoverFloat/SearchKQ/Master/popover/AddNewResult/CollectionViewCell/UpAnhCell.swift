//
//  UpAnhCell.swift
//  iCNM
//
//  Created by Thanh Huyen on 4/17/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class UpAnhCell: UICollectionViewCell {
    
    @IBOutlet weak var imgAnh: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgAnh.layer.cornerRadius = 8
        imgAnh.layer.masksToBounds = true
        imgAnh.layer.borderWidth = 2
        imgAnh.layer.borderColor = #colorLiteral(red: 0.4756349325, green: 0.4756467342, blue: 0.4756404161, alpha: 1)
        
        btnDelete.layer.cornerRadius = 10
        btnDelete.layer.masksToBounds = true

    }
}
