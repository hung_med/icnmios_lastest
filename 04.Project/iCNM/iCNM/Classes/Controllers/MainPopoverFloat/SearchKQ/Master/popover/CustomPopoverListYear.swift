//
//  CustomPopoverListYear.swift
//  iCNM
//
//  Created by Mac osx on 8/4/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

protocol CustomPopoverListYearDelegate {
    func gobackView2(year:String, index:Int)
}

class CustomPopoverListYear: UITableViewController {
    
    @IBOutlet var myTableView: UITableView!
    var delegate:CustomPopoverListYearDelegate?
    var listYear:[String]?
    var currentIndexYear:Int = -1
    override func viewDidLoad() {
        super.viewDidLoad()

        myTableView.delegate = self
        myTableView.dataSource = self
        let nib = UINib(nibName: "CustomPopoverCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        self.listYear = [String]()
        
        for i in 0 ..< 3 {
            let previousYear:Date = Calendar.current.date(byAdding: .year, value: -i, to: Date())!
            let convertToString = formatter.string(from: previousYear)
            let splitString = convertToString.components(separatedBy: "-")[0]
            self.listYear?.append(splitString)
        }
        
        self.myTableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listYear!.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomPopoverCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        if let year = listYear?[indexPath.row]{
            cell.lblMonth.text = "Năm \(year)"
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == 0 && currentIndexYear == -1{
            cell.contentView.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
            cell.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        }else{
            if indexPath.row == currentIndexYear{
                cell.contentView.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
                cell.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
            }else{
                cell.contentView.backgroundColor = UIColor.white
                cell.backgroundColor = UIColor.white
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        cell?.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let year = listYear?[indexPath.row]
        self.delegate?.gobackView2(year:year!, index:indexPath.row)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 30
    }
}
