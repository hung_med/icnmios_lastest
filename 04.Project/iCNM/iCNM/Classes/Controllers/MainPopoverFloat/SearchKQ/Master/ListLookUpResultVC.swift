//
//  ListLookUpResultVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Foundation
import FontAwesome_swift
import RealmSwift

protocol ListLookUpResultVCDelegate {
    func gobackLookupVC()
}

struct LookUpResultGroup {
    var intime: String
    var items: [Patient]
    var collapsed: Bool
    
    public init(intime: String, items: [Patient], collapsed: Bool = true) {
        self.intime = intime
        self.items = items
        self.collapsed = collapsed
    }
}

class ListLookUpResultVC: BaseViewControllerNoSearchBar, CustomSectionHeaderDelegate, UISearchBarDelegate, UIPopoverPresentationControllerDelegate, CustomPopoverListMonthDelegate, CustomPopoverListYearDelegate, UITableViewDelegate, UITableViewDataSource {

    var lookUpResults: [Patient]?
    var lookUpResultsGroup: [String: [Patient]] = [:]
    var lookUpResultsFilter: [String: [Patient]] = [:]
    var listInTimes:[String]?
    var listInTimes_Filter:[String]?
    var filteredNames:[Patient]?
    var sectionsData: [LookUpResultGroup]?
    var sectionsData2: [LookUpResultGroup]?
    var medicalProfileObj:MedicalProfile?
    
    var currentUser:String?
    var currentMaBS:String?
    var currentOrganizeID:String?
    var patientPID = ""
    var patientOrganizeID = 0
    var lblWarning:UILabel?
    var isUnit:Bool? = false
    var totalMoney:Float = 0.0
    var delegate:ListLookUpResultVCDelegate?
    var filterBarButton:UIBarButtonItem? = nil
    var filterYearBarButton:UIBarButtonItem? = nil
    var newBackButton:UIBarButtonItem? = nil
    var currentIndexMonth:Int = -1
    var currentIndexYear:Int = -1
    var isSearch:Bool? = false
    var curYear:String? = nil
    var curMonth:String? = nil
    var listMonthNearest:[String]?
    var customPopoverVC:CustomPopoverListMonth? = nil
    var customPopoverVC2:CustomPopoverListYear? = nil
    var isShowMoney:Bool? = false
    var typeKSK:String? = ""
    var arrayPatient = [Patient]()
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var spaceHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var spaceMyViewHeightConstraint: NSLayoutConstraint!
    var searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 250, height: 18))
    var searchOn : UIBarButtonItem!
    var searchOff : UIBarButtonItem!
    //@IBOutlet weak var rightTopButton: UIButton!
    @IBOutlet weak var imgAddResult: UIImageView!
    
    let searchController = UISearchController(searchResultsController: nil)
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after lfoading the view.
        self.tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        myTableView.delegate = self
        myTableView.dataSource = self
        
        myTableView.estimatedRowHeight = 70
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        
        // init
        self.lookUpResults = [Patient]()
        self.listInTimes = [String]()
        self.listInTimes_Filter = [String]()
        self.sectionsData = [LookUpResultGroup]()
        
        // Setup the Search Controller
        if #available(iOS 11.0, *) {
            self.navigationItem.setHidesBackButton(true, animated: false)
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(ListLookUpResultVC.back(sender:)), for: UIControlEvents.touchUpInside)
            newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
        }else{
            let button: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            button.setImage(UIImage(named: "angle-left"), for: UIControlState.normal)
            button.addTarget(self, action: #selector(ListLookUpResultVC.back(sender:)), for: UIControlEvents.touchUpInside)
            newBackButton = UIBarButtonItem(customView: button)
        }
        
        self.navigationItem.leftBarButtonItem = newBackButton
        searchOn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(isSearchOn))
        searchOff = UIBarButtonItem(title: "Hủy", style: .plain, target: self, action: #selector(isSearchOff))
        
        if isUnit!{
            let date = Date()
            let calendar = Calendar.current
            let month = calendar.component(.month, from: date)
            let year = calendar.component(.year, from: date)
            
            listMonthNearest = [String]()
            for i in 0 ..< 4 {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let previousMonth:Date = Calendar.current.date(byAdding: .month, value: -i, to: Date())!
                let convertToString = formatter.string(from: previousMonth)
                let splitString = convertToString.components(separatedBy: "-")[1]
                if let convertMonth = Int(splitString){
                    let str = "\(convertMonth)"
                    self.listMonthNearest?.append("\(str)")
                }
            }
            
            self.customUIBarButtonItem(month: String(month), year: String(year))
            self.navigationItem.setRightBarButtonItems([searchOn, filterYearBarButton!, filterBarButton!], animated: true)
        }else{
            title = "Lịch sử khám"
            self.spaceHeightConstraint.constant = 0
            self.navigationItem.setRightBarButtonItems([searchOn], animated: true)
        }
        
        // create search bar
        self.tabBarController?.tabBar.isHidden = true
        searchBar.placeholder = "Tìm theo Tên hoặc SID"
        searchBar.tintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        searchBar.isHidden = true
        searchBar.delegate = self
       
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        
        let nib = UINib(nibName: "CustomLookUpResultCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        
        let nib2 = UINib(nibName: "NewCustomLookUpResultCell", bundle: nil)
        myTableView.register(nib2, forCellReuseIdentifier: "NewCell")
        
        let nib_header = UINib(nibName: "CustomSectionHeader", bundle: nil)
        myTableView.register(nib_header, forHeaderFooterViewReuseIdentifier: "headerCell")
        
        self.checkTrackingFeature()
        
        // add gesture for image add result
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(addNewResult))
        imgAddResult.isUserInteractionEnabled = true
        imgAddResult.addGestureRecognizer(tapGesture)
        
    }
    
    func getPatientUpload() {
        
        let currentUsers:User? = {
            let realm = try! Realm()
            return realm.currentUser()
        }()
        
        let getPatient = GetPatientUpload()
        getPatient.pid = self.patientPID
        getPatient.organizeID = self.patientOrganizeID
        if let medicalProfileID = medicalProfileObj?.medicalProfileID {
            getPatient.medicalProfileID = medicalProfileID
        }
        if let doctorId = currentUsers?.userDoctor?.userInfo?.doctorID {
            getPatient.doctorID = "\(doctorId)"
        }
        
        GetPatientUpload.getPatientUpload(params: getPatient.toJSON(), success: { (data) in
            self.lookUpResults?.append(contentsOf: data)
            self.groupByInTime(data: self.lookUpResults!)
            
        }) { (error, response) in
            print(error)
        }
    }
    
    func groupByInTime(data:[Patient]){
        for i in 0..<Int((data.count))
        {
            let lookUpObject = data[i]
            let inTime = lookUpObject.InTime.components(separatedBy: "T")[0]
            if !(self.listInTimes!.contains(inTime)) {
                self.listInTimes?.append(inTime)
            }
        }
        
        for inTime in self.listInTimes!
        {
            self.sectionsData?.append(LookUpResultGroup(intime: inTime,
                                                        items: self.lookUpResults!, collapsed: true))
        }
    }
    
    func addNewResult() {
        let addNewResultVC = AddNewResultVC.init(nibName: "AddNewResultVC", bundle: nil)
        addNewResultVC.medicalProfileObj = self.medicalProfileObj
        self.navigationController?.pushViewController(addNewResultVC, animated: true)
    }
    
    func isSearchOn() {
        self.navigationItem.setRightBarButtonItems([self.searchOff], animated: false)
        self.navigationItem.titleView = searchBar
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 40).isActive = true
        }
        searchBar.isHidden = false
        searchBar.becomeFirstResponder()
    }
    
    func isSearchOff() {
        searchBar.text = nil
        if isUnit!{
            self.navigationItem.setRightBarButtonItems([searchOn, filterYearBarButton!, filterBarButton!], animated: true)
        }else{
            self.navigationItem.setRightBarButtonItems([self.searchOn], animated: false)
        }
        
        self.navigationItem.titleView = nil
        if searchBar.text == "" {
            isSearch = false
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            isSearch = false
            view.endEditing(true)
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
        else {
            isSearch = true
            filterContentForSearchText(searchText)
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
    }
    
    func customUIBarButtonItem(month:String, year:String){
        let filterButton =  UIButton(type: .custom)
        filterButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 15.0)
        let filterBtnName = "Tháng \(month)  " + String.fontAwesomeIcon(name: .caretDown)
        filterButton.setTitle(filterBtnName, for: .normal)
        filterButton.addTarget(self, action: #selector(ListLookUpResultVC.filterBarButtonHandler), for: .touchUpInside)
        filterButton.frame = CGRect(x: screenSizeWidth-200, y: 0, width: 100, height: 70)
        filterBarButton = UIBarButtonItem(customView: filterButton)
        
        let filterYearButton =  UIButton(type: .custom)
        filterYearButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 15.0)
        let filterYearBtnName = "Năm \(year)  " + String.fontAwesomeIcon(name: .caretDown)
        filterYearButton.setTitle(filterYearBtnName, for: .normal)
        filterYearButton.addTarget(self, action: #selector(ListLookUpResultVC.filterYearBarButtonHandler), for: .touchUpInside)
        filterYearButton.frame = CGRect(x: screenSizeWidth-80, y: 0, width: 100, height: 70)
        filterYearBarButton = UIBarButtonItem(customView: filterYearButton)
        self.navigationItem.setRightBarButtonItems([searchOn, filterYearBarButton!, filterBarButton!], animated: true)
        
        let date = Date()
        let calendar = Calendar.current
        let curYear = calendar.component(.year, from: date)
        if ((listMonthNearest?.contains(month))! && year == String(curYear)) {
            self.isShowMoney = true
        }else{
            self.isShowMoney = false
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if IS_IPAD{
            if #available(iOS 11.0, *) {
                // Running iOS 11 OR NEWER
                self.myTableView.frame = CGRect(x: 0, y: 108, width: myTableView.frame.size.width, height: myTableView.frame.size.height)
            }else{
                self.myTableView.frame = CGRect(x: 0, y: 40, width: myTableView.frame.size.width, height: myTableView.frame.size.height+76)
            }
            
        }else{
            if #available(iOS 11.0, *) {
                if IS_IPHONE_X{
                    self.spaceMyViewHeightConstraint.constant = 76
                }
            }
            else{
                if isUnit!{
                    self.spaceHeightConstraint.constant = 44
                    self.myTableView.frame = CGRect(x: 0, y: 44, width: myTableView.frame.size.width, height: myTableView.frame.size.height + 108)
                }else{
                    self.spaceHeightConstraint.constant = 0
                    self.myTableView.frame = CGRect(x: 0, y: 0, width: myTableView.frame.size.width, height: myTableView.frame.size.height + 76)
                }
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Tra cứu kết quả")
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
//        searchController.isActive = true
    }
    
    func checkTrackingFeature(){
            let userDefaults = UserDefaults.standard
            let token = userDefaults.object(forKey: "token") as! String
        TrackingFeature().checkTrackingFeature(fcm:token, featureName:Constant.RESULT_TEST, categoryID: 0, success: {(result) in
                if result != nil{
                    print("flow track done:", Constant.RESULT_TEST)
                }else{
                    print("flow track fail:", Constant.RESULT_TEST)
                }
            }, fail: { (error, response) in
                self.handleNetworkError(error: error, responseData: response.data)
            })
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
        if customPopoverVC != nil{
            customPopoverVC?.removeFromParentViewController()
        }
        if customPopoverVC2 != nil{
            customPopoverVC2?.removeFromParentViewController()
        }
        self.delegate?.gobackLookupVC()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if searchBar.text != "" {
            isSearchOn()
        }else{
            isSearchOff()
        }
    }
    
    func gobackView(month:String, index:Int){
        curMonth = month
        currentIndexMonth = index
        let theSubviews: Array = (self.myTableView.subviews)
        for view in theSubviews{
            if view.tag == 102{
                view.removeFromSuperview()
            }
        }
        totalMoney = 0.0
        self.listInTimes = [String]()
        self.listInTimes_Filter = [String]()
        self.myTableView.reloadData()
        var currentYear = ""
        if curYear == nil{
            let date = Date()
            let calendar = Calendar.current
            currentYear = "\(calendar.component(.year, from: date))"
        }else{
            currentYear = curYear!
        }
        self.getListLookUpResultUnit(user: currentUser!, maBS: currentMaBS!, currentMonth: month, currentYear: currentYear, organizeID: currentOrganizeID!)
        
        self.customUIBarButtonItem(month: month, year: currentYear)
    }
    
    func gobackView2(year:String, index:Int){
        curYear = year
        currentIndexYear = index
        let theSubviews: Array = (self.myTableView.subviews)
        for view in theSubviews{
            if view.tag == 102{
                view.removeFromSuperview()
            }
        }
        
        totalMoney = 0.0
        self.listInTimes = [String]()
        self.listInTimes_Filter = [String]()
        self.myTableView.reloadData()
        var currentMonth = ""
        if curMonth == nil{
            let date = Date()
            let calendar = Calendar.current
            currentMonth = "\(calendar.component(.month, from: date))"
        }else{
            currentMonth = curMonth!
        }
        self.getListLookUpResultUnit(user: currentUser!, maBS: currentMaBS!, currentMonth: currentMonth, currentYear: year, organizeID: currentOrganizeID!)
        
        self.customUIBarButtonItem(month: currentMonth, year: year)
    }
    
    func filterBarButtonHandler(_ sender: UIButton)
    {
        customPopoverVC = CustomPopoverListMonth(nibName: "CustomPopoverListMonth", bundle: nil)
        customPopoverVC?.targetIndexMonth = currentIndexMonth
        customPopoverVC?.delegate = self
        customPopoverVC?.modalPresentationStyle = UIModalPresentationStyle.popover
        customPopoverVC?.preferredContentSize = CGSize(width:120, height:360)
        let popover = customPopoverVC? .popoverPresentationController
        popover?.barButtonItem = UIBarButtonItem(customView: sender)
        popover!.delegate = self
        popover?.sourceRect = CGRect(x:0, y:0, width: 140, height: 125)
        popover?.permittedArrowDirections = UIPopoverArrowDirection.up
        self .present(customPopoverVC!, animated: true, completion: {
            
        })
    }
    
    func filterYearBarButtonHandler(_ sender: UIButton)
    {
        customPopoverVC2 = CustomPopoverListYear(nibName: "CustomPopoverListYear", bundle: nil)
        customPopoverVC2?.currentIndexYear = currentIndexYear
        customPopoverVC2?.delegate = self
        customPopoverVC2?.modalPresentationStyle = UIModalPresentationStyle.popover
        customPopoverVC2?.preferredContentSize = CGSize(width:120, height:90)
        let popover = customPopoverVC2? .popoverPresentationController
        popover?.barButtonItem = UIBarButtonItem(customView: sender)
        popover!.delegate = self
        popover?.sourceRect = CGRect(x:0, y:0, width: 140, height: 125)
        popover?.permittedArrowDirections = UIPopoverArrowDirection.up
        self .present(customPopoverVC2!, animated: true, completion: {
            
        })
    }

    func getListLookUpResultByPID(pid:String, organizeID:String) {
        let progress = self.showProgress()
        
        LookUpResult.getListLookUpResultByPID(pID: pid, organizeID: organizeID, success: { (data) in
            if data.count > 0 {
                self.patientPID = pid
                self.patientOrganizeID = Int(organizeID)!
                self.lookUpResults?.append(contentsOf: data)
                self.groupByInTime(data: self.lookUpResults!)
                self.myTableView.reloadData()
                self.hideProgress(progress)
            } else {
                
                self.lblWarning = createUILabel()
                self.myTableView.addSubview(self.lblWarning!)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func getListLookUpResultUnit(user:String, maBS:String, currentMonth:String, currentYear:String, organizeID:String){
        isUnit = true
        
        let progress = self.showProgress()
        self.lookUpResults = [Patient]()
        self.listInTimes = [String]()
        self.listInTimes_Filter = [String]()
        self.sectionsData = [LookUpResultGroup]()
        currentUser = user
        currentMaBS = maBS
        currentOrganizeID = organizeID
    
        LookupResultObject.getListLookUpResultUnit(maBS: maBS, currentMonth:currentMonth, currentYear:currentYear, organizeID: organizeID, success: { (data) in
            if data != nil {
                if let type = data?.type{
                    self.typeKSK = type
                }else{
                    self.typeKSK = ""
                }
                self.lookUpResults = data?.lookUpResult
                if self.lookUpResults?.count != 0{
                    for i in 0..<Int((self.lookUpResults?.count)!)
                    {
                        let lookUpObject = self.lookUpResults?[i]
                        let inTime = lookUpObject?.InTime.components(separatedBy: "T")[0]
                        if !(self.listInTimes!.contains(inTime!)) {
                            self.listInTimes?.append(inTime!)
                        }
                        for inTime in self.listInTimes!
                        {
                            self.sectionsData?.append(LookUpResultGroup(intime: inTime,
                                                                        items: self.lookUpResults!, collapsed: true))
                        }
                        if self.isUnit!{
                           // self.myLabel.isHidden = false
                            self.spaceHeightConstraint.constant = 44
                            if self.typeKSK == "KSK"{
                                self.searchBar.placeholder = "Tìm theo Tên hoặc SĐT"
                                if let count = self.lookUpResults?.count{
                                    self.myLabel.text = "Tổng nhân sự: \(count)"
                                }else{
                                    self.myLabel.text = "Tổng nhân sự: 0"
                                }
                            }else{
                                self.searchBar.placeholder = "Tìm theo Tên hoặc SID"
                                if let sumMoney = lookUpObject?.SumMoney{
                                    self.totalMoney += sumMoney
                                    let formatter = NumberFormatter()
                                    formatter.numberStyle = .decimal
                                    formatter.groupingSeparator = "."
                                    let number: NSNumber = NSNumber(value: self.totalMoney)
                                    let formattedString = formatter.string(for: number)
                                    if let result = formattedString{
                                        let format = String(format: "%@.000đ", result)
                                        if self.isShowMoney!{
                                            if let count = self.lookUpResults?.count{
                                                self.myLabel.text = "Số BN: \(count) | Tổng tiền: \(format)"
                                            }
                                        }else{
                                            self.myLabel.text = "Tổng tiền: Chỉ hiển thị 4 tháng gần nhất"
                                        }
                                    }
                                }
                            }
                        }else{
                            self.spaceHeightConstraint.constant = 0
                        }
                    }
                }else{
                    self.hideProgress(progress)
                    let lblWarning = createUILabel()
                    lblWarning.tag = 102
                    self.myTableView.addSubview(lblWarning)
                    self.spaceHeightConstraint.constant = 0
                    self.myLabel.text = ""
                }
                self.myTableView.reloadData()
                self.hideProgress(progress)
                
            } else {
                //self?.tableView.es_noticeNoMoreData()
                self.hideProgress(progress)
                let lblWarning = createUILabel()
                lblWarning.tag = 102
                self.myTableView.addSubview(lblWarning)
                self.spaceHeightConstraint.constant = 0
                self.myLabel.text = ""
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        if searchBar.text != "" {
            self.sectionsData2 = [LookUpResultGroup]()
            self.listInTimes_Filter = [String]()
            
            if self.filteredNames!.count == 0{
                return 0
            }
            for i in 0..<Int((self.filteredNames?.count)!)
            {
                let inTime = self.filteredNames![i].InTime.components(separatedBy: "T")[0]
                if !(self.listInTimes_Filter!.contains(inTime)) {
                    self.listInTimes_Filter?.append(inTime)
                }
            }
            
            for inTime in self.listInTimes_Filter!
            {
                self.sectionsData2?.append(LookUpResultGroup(intime: inTime,
                                    items: self.filteredNames!, collapsed: true))
            }
            
            return (self.listInTimes_Filter?.count)!
        }
        return (self.listInTimes?.count)!
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if searchBar.text != "" {
            lookUpResultsFilter = groupInTimeSection(lookUpObj:self.sectionsData2![section].items)
            return !sectionsData2![section].collapsed ? 0 : lookUpResultsFilter[self.listInTimes_Filter![section]]!.count
        }
        
        lookUpResultsGroup = groupInTimeSection(lookUpObj:self.sectionsData![section].items)
        if section == 0{
            return !sectionsData![section].collapsed ? 0 : lookUpResultsGroup[self.listInTimes![section]]!.count
        }else{
            return sectionsData![section].collapsed ? 0 : lookUpResultsGroup[self.listInTimes![section]]!.count
        }
    }
    
    func toggleSection(_ header: CustomSectionHeader, section: Int) {
        if searchBar.text != "" {
            let collapsed = !(self.sectionsData2?[section].collapsed)!
            // Toggle collapse when search active
            self.sectionsData2?[section].collapsed = collapsed
            header.setCollapsed(collapsed)
        }else{
            let collapsed = !(self.sectionsData?[section].collapsed)!
            // Toggle collapse
            self.sectionsData?[section].collapsed = collapsed
            header.setCollapsed(collapsed)
        }
        
        myTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerCell") as? CustomSectionHeader ?? CustomSectionHeader(reuseIdentifier: "headerCell")
        
        if searchBar.text != "" {
            headerView.setCollapsed((sectionsData2?[section].collapsed)!)
            let inTime = self.listInTimes_Filter?[section]
            headerView.inTime.text = inTime
        }else{
            headerView.setCollapsed((sectionsData?[section].collapsed)!)
            let inTime = self.listInTimes?[section]
            headerView.inTime.text = inTime
        }
        headerView.lblArrow.font = UIFont.fontAwesome(ofSize: 18)
        headerView.lblArrow.text = String.fontAwesomeIcon(name: .angleRight)
        headerView.section = section
        headerView.delegate = self
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (lookUpResults![indexPath.row].SID.contains("-")){
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomLookUpResultCell
            cell.backgroundColor = .clear
            
            if searchBar.text != "" {
                if let lookUpObject = lookUpResultsFilter[self.listInTimes_Filter![indexPath.section]]?[indexPath.row]{
                    cell.lblPatientName.text = lookUpObject.PatientName.uppercased()
                    cell.lblPID.text = lookUpObject.SID.components(separatedBy: "-")[1]
                    if isUnit!{
                        cell.lblMoney.isHidden = false
                        cell.lblAge.isHidden = false
                        if let sumMoney = lookUpObject.SumMoney{
                            let formatter = NumberFormatter()
                            formatter.numberStyle = .decimal
                            formatter.groupingSeparator = "."
                            let number: NSNumber = NSNumber(value: sumMoney)
                            let formattedString = formatter.string(for: number)
                            if let result = formattedString{
                                let format = String(format: "%@.000đ", result)
                                cell.lblMoney.text = format
                            }
                            if isShowMoney!{
                                if self.typeKSK == "KSK"{
                                    cell.lblMoney.isHidden = false
                                    cell.lblAge.isHidden = false
                                    cell.lblMoney.textColor = UIColor.black
                                    cell.lblAge.textColor = UIColor.black
                                    if !lookUpObject.Phone.trim().isEmpty && lookUpObject.Phone.trim() != "."{
                                        cell.lblMoney.text = "Số ĐT: \(lookUpObject.Phone.trim())"
                                    }else{
                                        cell.lblMoney.text = "Số ĐT: Chưa cập nhật"
                                    }
                                    if lookUpObject.Age != 0{
                                        cell.lblAge.text = "N.sinh: \(lookUpObject.Age)"
                                    }else{
                                        cell.lblAge.text = "N.sinh: Chưa cập nhật"
                                    }
                                }else{
                                    cell.lblMoney.isHidden = false
                                    cell.lblAge.isHidden = true
                                }
                            }else{
                                if self.typeKSK == "KSK"{
                                    cell.lblMoney.isHidden = false
                                    cell.lblAge.isHidden = false
                                    cell.lblMoney.textColor = UIColor.black
                                    cell.lblAge.textColor = UIColor.black
                                    if !lookUpObject.Phone.trim().isEmpty && lookUpObject.Phone.trim() != "."{
                                        cell.lblMoney.text = "Số ĐT: \(lookUpObject.Phone.trim())"
                                    }else{
                                        cell.lblMoney.text = "Số ĐT: Chưa cập nhật"
                                    }
                                    if lookUpObject.Age != 0{
                                        cell.lblAge.text = "N.sinh: \(lookUpObject.Age)"
                                    }else{
                                        cell.lblAge.text = "N.sinh: Chưa cập nhật"
                                    }
                                }else{
                                    cell.lblMoney.isHidden = true
                                    cell.lblAge.isHidden = true
                                }
                            }
                        }
                    }else{
                        cell.lblMoney.isHidden = true
                        cell.lblMoney.isHidden = true
                    }
                }
            } else{
                if let lookUpObj = lookUpResultsGroup[self.listInTimes![indexPath.section]]?[indexPath.row]{
                    cell.lblPatientName.text = lookUpObj.PatientName.uppercased()
                    if isUnit!{
                        cell.lblMoney.isHidden = false
                        cell.lblAge.isHidden = false
                        if let sumMoney = lookUpObj.SumMoney{
                            let formatter = NumberFormatter()
                            formatter.numberStyle = .decimal
                            formatter.groupingSeparator = "."
                            let number: NSNumber = NSNumber(value: sumMoney)
                            let formattedString = formatter.string(for: number)
                            if let result = formattedString{
                                let format = String(format: "%@.000đ", result)
                                cell.lblMoney.text = format
                            }
                            if isShowMoney!{
                                if self.typeKSK == "KSK"{
                                    cell.lblMoney.isHidden = false
                                    cell.lblAge.isHidden = false
                                    cell.lblMoney.textColor = UIColor.black
                                    cell.lblAge.textColor = UIColor.black
                                    if !lookUpObj.Phone.trim().isEmpty && lookUpObj.Phone.trim() != "."{
                                        cell.lblMoney.text = "Số ĐT: \(lookUpObj.Phone.trim())"
                                    }else{
                                        cell.lblMoney.text = "Số ĐT: Chưa cập nhật"
                                    }
                                    if lookUpObj.Age != 0{
                                        cell.lblAge.text = "N.sinh: \(lookUpObj.Age)"
                                    }else{
                                        cell.lblAge.text = "N.sinh: Chưa cập nhật"
                                    }
                                }else{
                                    cell.lblMoney.isHidden = false
                                    cell.lblAge.isHidden = true
                                }
                            }else{
                                if self.typeKSK == "KSK"{
                                    cell.lblMoney.isHidden = false
                                    cell.lblAge.isHidden = false
                                    cell.lblMoney.textColor = UIColor.black
                                    cell.lblAge.textColor = UIColor.black
                                    if !lookUpObj.Phone.trim().isEmpty && lookUpObj.Phone.trim() != "."{
                                        cell.lblMoney.text = "Số ĐT: \(lookUpObj.Phone.trim())"
                                    }else{
                                        cell.lblMoney.text = "Số ĐT: Chưa cập nhật"
                                    }
                                    if lookUpObj.Age != 0{
                                        cell.lblAge.text = "N.sinh: \(lookUpObj.Age)"
                                    }else{
                                        cell.lblAge.text = "N.sinh: Chưa cập nhật"
                                    }
                                }else{
                                    cell.lblMoney.isHidden = true
                                    cell.lblAge.isHidden = true
                                }
                            }
                        }
                    }else{
                        cell.lblMoney.isHidden = true
                        cell.lblMoney.isHidden = true
                    }
                    
                    cell.lblPID.text = lookUpObj.SID.components(separatedBy: "-")[1]
                }
            }
            
            return cell
        } else if (lookUpResults![indexPath.row].SID.contains("")) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewCell", for: indexPath) as! NewCustomLookUpResultCell
            cell.backgroundColor = .clear
            let patientObj:Patient = lookUpResults![indexPath.row]
            cell.lblLyDoKham.text = patientObj.Reason
            cell.lblKetLuan.text = patientObj.Conclude
            cell.lblDiaDiem.text = patientObj.Place
            cell.lblDanDo.text = patientObj.Note
            return cell
        }
        
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let viewController = UIStoryboard(name: "DetailLookUp", bundle: nil).instantiateViewController(withIdentifier: "DetailLookUpResultVC") as? DetailLookUpResultVC {
            self.navigationItem.titleView = nil
            let lookUpObject: Patient?
            if searchBar.text != "" {
                isSearch = true
                lookUpObject = lookUpResultsFilter[self.listInTimes_Filter![indexPath.section]]?[indexPath.row]
                if let sID = lookUpObject?.SID, let pID = lookUpObject?.PID, let organizeID = lookUpObject?.OrganizeID{
                    viewController.getListLookUpResultBySID(sid: sID, pID: pID, organizeID: String(organizeID))
                }
                
            } else{
                isSearch = false
                lookUpObject = lookUpResultsGroup[self.listInTimes![indexPath.section]]?[indexPath.row]
                if let sID = lookUpObject?.SID, let pID = lookUpObject?.PID, let organizeID = lookUpObject?.OrganizeID{
                    viewController.getListLookUpResultBySID(sid: sID, pID: pID, organizeID: String(organizeID))
                }
            }
            self.navigationController!.pushViewController(viewController, animated: true)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if isUnit!{
            return 88.0
        }else{
            if (lookUpResults![indexPath.row].SID.contains("-")){
                return 90.0
            }else{
                return UITableViewAutomaticDimension
            }
        }
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredNames = lookUpResults?.filter({( lookUp : Patient) -> Bool in
            let patientName = lookUp.PatientName.folding(options: .diacriticInsensitive, locale: .autoupdatingCurrent)
            let keySearch = searchText.folding(options: .diacriticInsensitive, locale: .autoupdatingCurrent)
            return patientName.lowercased().contains(keySearch.lowercased())
                || lookUp.SID.components(separatedBy: "-")[1].lowercased().contains(searchText.lowercased())
                || lookUp.InTime.components(separatedBy: "T")[0].lowercased().contains(searchText.lowercased())
                || lookUp.Phone.lowercased().contains(searchText.lowercased())
                || String(describing: lookUp.SumMoney).lowercased().contains(searchText.lowercased())
        })
    }

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
