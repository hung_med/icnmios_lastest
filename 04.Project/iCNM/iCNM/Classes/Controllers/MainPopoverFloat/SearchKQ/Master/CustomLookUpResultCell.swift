//
//  CustomLookUpResultCell.swift
//  iCNM
//
//  Created by Mac osx on 7/14/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class CustomLookUpResultCell: UITableViewCell {

    @IBOutlet weak var lineView2: UIView!
    //@IBOutlet weak var lineView: UIImageView!
    @IBOutlet weak var groupView: UIView!
    @IBOutlet weak var lblPatientName: UILabel!
    @IBOutlet weak var lblPID: UILabel!
    @IBOutlet weak var lblMoney: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.groupView.layer.cornerRadius = 4
        self.groupView.layer.borderWidth = 1
        self.groupView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.groupView.layer.masksToBounds = true
        self.groupView.layer.shadowOffset = CGSize(width: 0, height:2)
        self.groupView.layer.shadowColor = UIColor.groupTableViewBackground.cgColor
        self.groupView.layer.shadowOpacity = 0.23
        self.groupView.layer.shadowRadius = 1
        self.lineView2.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        
        self.selectionStyle = .none
        let f = contentView.frame
        let fr = UIEdgeInsetsInsetRect(f, UIEdgeInsetsMake(10, 10, 10, 15))
        contentView.frame = fr
        addShadow(cell: self)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func addShadow(cell:UITableViewCell) {
        cell.layer.cornerRadius = 4
        cell.layer.masksToBounds = true
        cell.layer.shadowOffset = CGSize(width: 0, height:0)
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.20
        cell.layer.shadowRadius = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
