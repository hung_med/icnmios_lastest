//
//  CustomPopoverListMonth.swift
//  iCNM
//
//  Created by Mac osx on 8/4/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

protocol CustomPopoverListMonthDelegate {
    func gobackView(month:String, index:Int)
}

class CustomPopoverListMonth: UITableViewController {
    
    @IBOutlet var myTableView: UITableView!
    var delegate:CustomPopoverListMonthDelegate?
    var listMonth:[String]?
    var currentIndexMonth:Int = -1
    var targetIndexMonth:Int = -1
    override func viewDidLoad() {
        super.viewDidLoad()

        myTableView.delegate = self
        myTableView.dataSource = self
        let nib = UINib(nibName: "CustomPopoverCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        self.listMonth = [String]()
        for i in 0 ..< 12 {
            let month = "\(i+1)"
            self.listMonth?.append(month)
        }
        
        let date = Date()
        let calendar = Calendar.current
        let month = calendar.component(.month, from: date)
        if targetIndexMonth != -1{
            currentIndexMonth = targetIndexMonth+1
        }else{
           currentIndexMonth = month
        }
        
        
        self.myTableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listMonth!.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomPopoverCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        if let month = listMonth?[indexPath.row]{
            cell.lblMonth.text = "Tháng \(month)"
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == 0 && currentIndexMonth == -1{
            cell.contentView.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
            cell.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        }else{
            if indexPath.row == currentIndexMonth-1{
                cell.contentView.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
                cell.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
            }else{
                cell.contentView.backgroundColor = UIColor.white
                cell.backgroundColor = UIColor.white
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        cell?.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let month = listMonth?[indexPath.row]
        self.delegate?.gobackView(month:month!, index:indexPath.row)
        self.dismiss(animated: true, completion: nil)
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 30
    }
}
