//
//  DetailLookUpCustomHeader.swift
//  iCNM
//
//  Created by Mac osx on 7/18/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
protocol CustomSectionHeaderDelegate {
    func toggleSection(_ header: CustomSectionHeader, section: Int)
}

class CustomSectionHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var lineView2: UIView!
    //@IBOutlet weak var lineView: UIImageView!
    @IBOutlet weak var inTime: UILabel!
    @IBOutlet weak var bgInTime: UILabel!
    @IBOutlet weak var lblArrow: UILabel!
    @IBOutlet weak var spaceLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var pointView: UIView!
    @IBOutlet weak var pointView2: UIView!
    var delegate: CustomSectionHeaderDelegate?
    var section: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.bgInTime.layer.cornerRadius = 4
        self.bgInTime.layer.masksToBounds = true
       // self.lineView.isHidden=true
        self.lineView2.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        
        self.pointView.layer.cornerRadius = 11
        self.pointView.layer.masksToBounds = true
        self.pointView.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 0.3)
        
        self.pointView2.layer.cornerRadius = 6
        self.pointView2.layer.masksToBounds = true
        self.pointView2.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        
        self.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CustomSectionHeader.tapHeader(_:))))
    }
    
    func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? CustomSectionHeader else {
            return
        }
        
        delegate?.toggleSection(self, section: cell.section)
    }
    
    func setCollapsed(_ collapsed: Bool) {
        self.lblArrow.rotate(collapsed ? 0.0 : .pi/2)
    }
}
