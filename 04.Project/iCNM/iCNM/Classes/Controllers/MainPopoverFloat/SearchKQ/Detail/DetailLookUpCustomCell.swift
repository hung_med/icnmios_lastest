//
//  DetailLookUpCustomCell.swift
//  iCNM
//
//  Created by Mac osx on 7/15/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class DetailLookUpCustomCell: UITableViewCell {

    @IBOutlet weak var imgChart: UIImageView!
    @IBOutlet weak var lblNormalRange: UILabel!
    @IBOutlet weak var lblResultTest: UILabel!
    @IBOutlet weak var lblTestName: UILabel!
    @IBOutlet weak var lineView1: UIView!
    @IBOutlet weak var lineView2: UIView!
    @IBOutlet weak var chartView: NSLayoutConstraint!
    @IBOutlet weak var constraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.imgChart.contentMode = .scaleAspectFit
        let frame = CGRect(x: self.imgChart.frame.origin.x, y: self.imgChart.frame.origin.y, width: 24, height: 24)
        self.imgChart.frame = frame
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
