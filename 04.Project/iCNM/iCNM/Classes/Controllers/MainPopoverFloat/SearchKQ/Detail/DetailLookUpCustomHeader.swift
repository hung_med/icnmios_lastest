//
//  DetailLookUpCustomHeader.swift
//  iCNM
//
//  Created by Mac osx on 7/18/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class DetailLookUpCustomHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var lblHeaderMain: UILabel!
    @IBOutlet weak var lblHeaderNormalRange: UILabel!
    @IBOutlet weak var lblHeaderResultTest: UILabel!
    @IBOutlet weak var lblHeaderTestName: UILabel!
    
    @IBOutlet weak var lineView3: UIView!
    @IBOutlet weak var lineView2: UIView!
    @IBOutlet weak var lineView1: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       // addShadow(cell: self)
    }
    
    private func addShadow(cell:UITableViewHeaderFooterView) {
        cell.layer.shadowOffset = CGSize(width:1, height:10)
        cell.layer.shadowColor = UIColor.groupTableViewBackground.cgColor
        cell.layer.shadowRadius = 1
        cell.layer.shadowOpacity = 1
        
        cell.clipsToBounds = false
        
        let shadowFrame: CGRect = (cell.layer.bounds)
        let shadowPath: CGPath = UIBezierPath(rect: shadowFrame).cgPath
        cell.layer.shadowPath = shadowPath
    }

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
    
}
