import UIKit
import Toaster
class GraphDataViewController: BaseViewController, ChartDelegate {
    @IBOutlet weak var chart: Chart!
    
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var lblLeftTitle: UILabel!
    @IBOutlet weak var lblNormalRange: UILabel!
    
    @IBOutlet weak var lblRightTitle: UILabel!
    @IBOutlet weak var lblNormal: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblResult: UILabel!
    var currentTitle:String? = nil
    var lookUpChart: [LookUpChart]?
    var values:[Float] = []
    var values1:[Float] = []
    var values2:[Float] = []
    var dates:[String] = []
    
    override func viewDidLoad() {
        
        // Draw the chart selected from the TableViewController
        let prefs = UserDefaults.standard
        let keyValue = prefs.object(forKey:"dates")
        if keyValue != nil{
            prefs.removeObject(forKey:"dates")
        }
        self.navigationItem.hidesBackButton = true

        let button: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        button.setImage(UIImage(named: "angle-left"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(GraphDataViewController.back(sender:)), for: UIControlEvents.touchUpInside)
        
        let backBtn = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = backBtn
        
        chart.delegate = self
        setShadowView(view: leftView)
        setShadowView(view: rightView)
        
        leftView.isHidden = true
        rightView.isHidden = true
        self.searchBar.isHidden = true
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func setShadowView(view:UIView){
        view.layer.shadowRadius = 3
        view.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        view.layer.shadowOpacity = 0.4
        view.clipsToBounds = false
        view.layer.borderWidth = 2.0
    }
    
    func getDataChartDetailLookUp(pid:String, testCode:String){
        let progress = self.showProgress()
        self.lookUpChart = [LookUpChart]()
        LookUpChart.getDataChartDetailLookUp(pId:pid, testCode: testCode, success: { (data) in
            if data.count > 0 {
                self.lookUpChart?.append(contentsOf: data)
            } else {
                //self?.tableView.es_noticeNoMoreData()
                Toast(text: "Không có dữ liệu").show()
                self.hideProgress(progress)
                return
            }
            
            self.hideProgress(progress)
            
            for i in 0..<Int((self.lookUpChart?.count)!) {
                if self.lookUpChart?.count == 1{
                    if let value = self.lookUpChart?[i].lowLimit{
                        self.values1.append(Float(value))
                        self.values1.append(Float(value))
                    }
                }else{
                    if let value = self.lookUpChart?[i].lowLimit{
                        self.values1.append(Float(value))
                    }
                }
            }
            for i in 0..<Int((self.lookUpChart?.count)!) {
                if self.lookUpChart?.count == 1{
                    if let value = self.lookUpChart?[i].hightLimit{
                        self.values2.append(Float(value))
                        self.values2.append(Float(value))
                    }
                }else{
                    if let value = self.lookUpChart?[i].hightLimit{
                        self.values2.append(Float(value))
                    }
                }
            }
            
            let serie1 = ChartSeries(self.values1)
            serie1.color = ChartColors.redColor()
            let serie2 = ChartSeries(self.values2)
            serie2.color = ChartColors.redColor()
           
            for i in 0..<Int((self.lookUpChart?.count)!) {
                if self.lookUpChart?.count == 1{
                    if let value = self.lookUpChart?[i].resultText{
                        self.lblNormal.text = value
                        let v = (value as NSString).floatValue
                        self.values.append(v)
                        self.values.append(v)
                    }
                }else{
                    if let value = self.lookUpChart?[i].resultText{
                        self.lblNormal.text = value
                        let v = (value as NSString).floatValue
                        self.values.append(v)
                    }
                }
            }
            
            for i in 0..<Int((self.lookUpChart?.count)!) {
                if self.lookUpChart?.count == 1{
                    if let value = self.lookUpChart?[i].resultTime{
                        self.dates.append(value)
                        self.dates.append(value)
                    }
                }else{
                    if let value = self.lookUpChart?[i].resultTime{
                        self.dates.append(value)
                    }
                }
            }
            
            UserDefaults.standard.set(self.dates, forKey: "dates")
            UserDefaults.standard.synchronize()
            
            let series = ChartSeries(self.values)
            series.color = ChartColors.blueColor()
            series.area = true
            
            self.chart.add([series, serie1, serie2])
            
            self.leftView.isHidden = false
            self.rightView.isHidden = false
            self.lblLeftTitle.text = "Chỉ số bình thường"
            self.lblRightTitle.text = "Kết quả lần cuối"
            self.lblLeftTitle.textAlignment = NSTextAlignment.center
            self.lblRightTitle.textAlignment = NSTextAlignment.center
            self.lblLeftTitle.lineBreakMode = .byWordWrapping
            self.lblLeftTitle.numberOfLines = 0
            self.lblRightTitle.lineBreakMode = .byWordWrapping
            self.lblRightTitle.numberOfLines = 0

            if (self.lookUpChart?.count)! > 0{
                if let normalRange = self.lookUpChart?[(self.lookUpChart?.count)!-1].normalRange{
                    self.lblNormalRange.text = normalRange
                }
                
                if let resultTime = self.lookUpChart?[(self.lookUpChart?.count)!-1].resultTime{
                    self.lblDateTime.text = resultTime
                }
            }
            
            self.lblDate.text = "Thời gian"
            self.lblResult.text = "Kết quả"
            
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    

    override func viewDidLayoutSubviews() {
        self.lblResult.frame = CGRect(x: 0, y: self.lblResult.frame.origin.y, width: self.lblResult.frame.size.width, height: self.lblResult.frame.size.height)
       // self.lblResult.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
        if IS_IPHONE_5{
            let frame = CGRect(x: leftView.frame.origin.x-15, y: leftView.frame.origin.y, width: leftView.frame.size.width-15, height: leftView.frame.size.height)
            leftView.frame = frame
            
            let frame1 = CGRect(x: rightView.frame.origin.x+25, y: rightView.frame.origin.y, width: rightView.frame.size.width-15, height: rightView.frame.size.height)
            rightView.frame = frame1
        }
        title = currentTitle
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]

    }
    
    // Chart delegate
    
    func didTouchChart(_ chart: Chart, indexes: Array<Int?>, x: Float, left: CGFloat) {
        for (seriesIndex, dataIndex) in indexes.enumerated() {
            if let value = chart.valueForSeries(seriesIndex, atIndex: dataIndex) {
                let index = accurateRound(value: Double(x))
                if index == (self.lookUpChart?.count)!{
                    return
                }
                
                if let resultTime = self.lookUpChart?[index].resultTime{
                    let splitText = resultTime.components(separatedBy: "T")
                    self.lblDateTime.text = splitText[0]
                }
                if let normal = self.lookUpChart?[index].resultText{
                    self.lblNormal.text = normal
                    
                    self.lblRightTitle.text = "Kết quả lần trước"
                    if index == ((self.lookUpChart?.count)!-1){
                        self.lblRightTitle.text = "Kết quả lần cuối"
                    }
                }
            }
        }
    }
    
    func didFinishTouchingChart(_ chart: Chart) {
        
    }
    
    func didEndTouchingChart(_ chart: Chart) {
        
    }
    
    
    func accurateRound(value: Double) -> Int {
        let d : Double = value - Double(Int(value))
        if d < 0.5 {
            return Int(value)
        } else {
            return Int(value) + 1
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator)
        
        // Redraw chart on rotation
        chart.setNeedsDisplay()
        
    }
    
}
