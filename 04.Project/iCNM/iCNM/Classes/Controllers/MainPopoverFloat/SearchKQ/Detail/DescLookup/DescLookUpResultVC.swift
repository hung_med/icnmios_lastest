//
//  DescLookUpResultVC
//  iCNM
//
//  Created by Mac osx on 11/02/17.
//  Copyright © 2017 Anh Nguyen. All rights reserved.
//

import UIKit

class DescLookUpResultVC: BaseViewController {
  
    @IBOutlet weak var mySegment: UISegmentedControl!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContent: UITextView!
    @IBOutlet weak var myBtnExit: UIButton!
    @IBOutlet weak var myView: UIView!
    
    var meaning1:String? = nil
    var meaning2:String? = nil
    var testName:String? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//        myView.layer.cornerRadius = 10
//        myView.layer.borderWidth = 1
//        myView.layer.masksToBounds = true
        myBtnExit.layer.cornerRadius = 4
        myBtnExit.layer.masksToBounds = true
        mySegment.setTitle("Ý nghĩa chung", forSegmentAt: 0)
        mySegment.setTitle("Dành cho bác sỹ", forSegmentAt: 1)
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        lblContent.text = meaning1
        lblTitle.text = testName
        lblTitle.lineBreakMode = .byWordWrapping
        lblTitle.numberOfLines = 0
        self.searchBar.isHidden = true
    }
   
    @IBAction func switchSegment(_ sender: Any) {
        switch mySegment.selectedSegmentIndex {
        case 0:
            lblContent.text = meaning1
        case 1:
            lblContent.text = meaning2
        default:
            break;
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnExit(_ sender: Any) {
        self .dismiss(animated: true) {
            
        }
    }
    
}
