//
//  ConfirmAccountDoctorVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import Toaster
class ConfirmAccountDoctorVC: BaseViewController, UITextFieldDelegate, UIPopoverPresentationControllerDelegate, UIViewControllerTransitioningDelegate {

    @IBOutlet weak var txtFullName: FWFloatingLabelTextField!
    @IBOutlet weak var txtDevice: FWFloatingLabelTextField!
    @IBOutlet weak var txtPhoneNumber: FWFloatingLabelTextField!
   
    @IBOutlet weak var buttonExit: UIButton!
    @IBOutlet weak var buttonDone: UIButton!
    @IBOutlet weak var lblWarning: UILabel!
    @IBOutlet weak var myView: UIView!
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        myView.layer.cornerRadius = 10
        myView.layer.borderWidth = 1
        myView.layer.masksToBounds = true
        buttonDone.layer.cornerRadius = 4
        buttonDone.layer.masksToBounds = true
        buttonExit.layer.cornerRadius = 4
        buttonExit.layer.masksToBounds = true
        
        self.txtFullName.delegate = self
        self.txtDevice.delegate = self
        self.txtPhoneNumber.delegate = self
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        
        self.txtFullName.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
        if currentUser != nil {
            if let user = currentUser {
                if let phoneNumber = user.userDoctor?.userInfo?.phone{
                    self.txtDevice.placeholder = "Hệ điều hành"
                    self.txtDevice.text = "iOS"
                    self.txtPhoneNumber.text = phoneNumber
                    self.txtDevice.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                    self.txtPhoneNumber.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                    self.getRecordInstallTN(phone: phoneNumber)
                }else{
                    self.txtPhoneNumber.isEnabled = true
                }
            }
        }
    }
    
    func getRecordInstallTN(phone:String){
        RecordInstall().getRecordInstallTN(phone:phone, success: { (data) in
            if data != nil {
                self.txtFullName.text = data?.userID
                self.txtFullName.isUserInteractionEnabled = false
                self.txtFullName.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                self.buttonDone.isUserInteractionEnabled = false
            } else {
                self.txtFullName.text = ""
                self.txtFullName.isUserInteractionEnabled = true
                self.buttonDone.isUserInteractionEnabled = true
            }
        }, fail: { (error, response) in
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let frame = CGRect(x: screenSizeWidth/2 - self.myView.frame.width/2, y: screenSizeHeight/2 - self.myView.frame.height/2, width: self.myView.frame.width, height: self.myView.frame.height)
        self.myView.frame = frame
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtFullName.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnDone(_ sender: Any) {
        if let fullName = self.txtFullName.text, fullName.isEmpty{
            self.lblWarning.text = "Bạn vui lòng nhập mã nhân viên"
            self.lblWarning.lineBreakMode = .byWordWrapping
            self.lblWarning.numberOfLines = 0
            self.lblWarning.adjustsFontSizeToFitWidth = true
            return
        }else{
            if currentUser != nil {
                let userID = Int(self.txtFullName.text!)
                let phoneNumber = self.txtPhoneNumber.text
                self.checkRecordInstallTN(userID: userID!, insDevice:2, phone: phoneNumber!)
            }
        }
    }
    
    func checkRecordInstallTN(userID:Int, insDevice:Int, phone:String){
        let progress = self.showProgress()
        RecordInstall().checkRecordInstallTN(userID:userID, insDevice:insDevice, phone:phone, success: { (data) in
            if (data?.contains("Ok"))! {
                self.hideProgress(progress)
                Toast(text: "Xác nhận cài đặt thành công!").show()
                self .dismiss(animated: true) {}
            } else {
                self.hideProgress(progress)
                self.lblWarning.text = data
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
   
    @IBAction func btnExit(_ sender: Any) {
        self .dismiss(animated: true) { 
            
        }
    }
    
    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController!, sourceViewController source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController: presented, presenting: presentingViewController)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
}
