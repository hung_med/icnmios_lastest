//
//  PromotionalCreditCard.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import Toaster
class PromotionalCreditCard: BaseViewController, UITextFieldDelegate, UIPopoverPresentationControllerDelegate, UIViewControllerTransitioningDelegate {

    @IBOutlet weak var txtPromotionalCode: FWFloatingLabelTextField!
    @IBOutlet weak var buttonExit: UIButton!
    @IBOutlet weak var buttonDone: UIButton!
    @IBOutlet weak var lblWarning: UILabel!
    @IBOutlet weak var myView: UIView!
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        myView.layer.cornerRadius = 10
        myView.layer.borderWidth = 1
        myView.layer.masksToBounds = true
        buttonDone.layer.cornerRadius = 4
        buttonDone.layer.masksToBounds = true
        buttonExit.layer.cornerRadius = 4
        buttonExit.layer.masksToBounds = true
    
        self.txtPromotionalCode.delegate = self
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let frame = CGRect(x: screenSizeWidth/2 - self.myView.frame.width/2, y: screenSizeHeight/2 - self.myView.frame.height/2, width: self.myView.frame.width, height: self.myView.frame.height)
        self.myView.frame = frame
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnDone(_ sender: Any) {
        if let promotionalCode = self.txtPromotionalCode.text?.trim(), promotionalCode.isEmpty{
            self.lblWarning.text = "Bạn vui lòng nhập mã thẻ khuyến mại"
            self.lblWarning.lineBreakMode = .byWordWrapping
            self.lblWarning.numberOfLines = 0
            self.lblWarning.adjustsFontSizeToFitWidth = true
            return
        }else{
            let numberCode = self.txtPromotionalCode.text!.trim()
            self.sendPromotionalCode(numberCode: numberCode)
        }
    }
    
    func sendPromotionalCode(numberCode:String){
        let progress = self.showProgress()
        RecordInstall().sendPromotionalCreditCard(numberCode:numberCode, success: { (data) in
           self.lblWarning.text = data
           self.hideProgress(progress)
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
   
    @IBAction func btnExit(_ sender: Any) {
        self .dismiss(animated: true) { 
            
        }
    }
    
    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController!, sourceViewController source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController: presented, presenting: presentingViewController)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
}
