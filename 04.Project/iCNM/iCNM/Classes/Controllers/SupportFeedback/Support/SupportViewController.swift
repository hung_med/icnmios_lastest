//
//  SupportViewController.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import Toaster
class SupportViewController: BaseViewController, UITextFieldDelegate, UITextViewDelegate, UIPopoverPresentationControllerDelegate, UIViewControllerTransitioningDelegate {

    @IBOutlet weak var txtMaBS: FWFloatingLabelTextField!
    @IBOutlet weak var txtFullName: FWFloatingLabelTextField!
    @IBOutlet weak var txtEmail: FWFloatingLabelTextField!
    @IBOutlet weak var txtPhoneNumber: FWFloatingLabelTextField!
    @IBOutlet weak var txtContent: UITextView!
    @IBOutlet weak var lblContent: UILabel!
    
    @IBOutlet weak var buttonExit: UIButton!
    @IBOutlet weak var buttonDone: UIButton!
    @IBOutlet weak var lblWarning: UILabel!
    @IBOutlet weak var myView: UIView!
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        myView.layer.cornerRadius = 10
        myView.layer.borderWidth = 1
        myView.layer.masksToBounds = true
        buttonDone.layer.cornerRadius = 4
        buttonDone.layer.masksToBounds = true
        buttonExit.layer.cornerRadius = 4
        buttonExit.layer.masksToBounds = true
        
        self.txtMaBS.delegate = self
        self.txtFullName.delegate = self
        self.txtEmail.delegate = self
        self.txtPhoneNumber.delegate = self
        self.txtContent.delegate = self
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        
        self.txtContent.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        self.txtContent.layer.borderWidth = 1.0
        self.txtContent.layer.cornerRadius = 4
        self.txtContent.layer.masksToBounds = true
        
        if currentUser != nil {
            if let user = currentUser {
                self.txtMaBS.isUserInteractionEnabled = false
                if let doctorID = user.userDoctor?.userInfo?.doctorID{
                    if doctorID != 0{
                        self.txtMaBS.text = "\(doctorID)"
                        
                        self.txtMaBS.isUserInteractionEnabled = false
                    }
                }
                
                if let doctorIDCode = user.userDoctor?.userInfo?.doctorIDCode{
                    if !doctorIDCode.isEmpty{
                        self.txtMaBS.text = "\(doctorIDCode)"
                        self.txtMaBS.isUserInteractionEnabled = false
                    }
                }
                self.txtFullName.text = user.userDoctor?.userInfo?.name
                self.txtEmail.text = user.userDoctor?.userInfo?.email
                self.txtPhoneNumber.text = user.userDoctor?.userInfo?.phone
            }
        }else{
            //self.lblFullName.isHidden = true
            //self.lblEmail.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let frame = CGRect(x: screenSizeWidth/2 - self.myView.frame.width/2, y: screenSizeHeight/2 - self.myView.frame.height/2, width: self.myView.frame.width, height: self.myView.frame.height)
        self.myView.frame = frame
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtFullName.resignFirstResponder()
        self.txtEmail.resignFirstResponder()
        self.txtPhoneNumber.resignFirstResponder()
        //self.txtContent.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnDone(_ sender: Any) {
        if let fullName = self.txtFullName.text, fullName.isEmpty{
            self.lblWarning.text = "Bạn vui lòng nhập họ tên"
            self.lblWarning.lineBreakMode = .byWordWrapping
            self.lblWarning.numberOfLines = 0
            self.lblWarning.adjustsFontSizeToFitWidth = true
            return
        } else if let email = self.txtEmail.text, email.isEmpty {
            self.lblWarning.text = "Bạn vui lòng nhập địa chỉ email"
            self.lblWarning.lineBreakMode = .byWordWrapping
            self.lblWarning.numberOfLines = 0
            self.lblWarning.adjustsFontSizeToFitWidth = true
            return
        }else if let phoneNumber = self.txtPhoneNumber.text, phoneNumber.isEmpty {
            self.lblWarning.text = "Bạn vui lòng nhập số điện thoại"
            self.lblWarning.lineBreakMode = .byWordWrapping
            self.lblWarning.numberOfLines = 0
            self.lblWarning.adjustsFontSizeToFitWidth = true
            return
        }else if let content = self.txtContent.text, content.trim().isEmpty {
            self.lblWarning.text = "Bạn vui lòng nhập nội dung góp ý"
            self.lblWarning.lineBreakMode = .byWordWrapping
            self.lblWarning.numberOfLines = 0
            self.lblWarning.adjustsFontSizeToFitWidth = true
            return
        }else{
            //if currentUser != nil {
            let name = self.txtFullName.text
            let email = self.txtEmail.text
            let phoneNumber = self.txtPhoneNumber.text
            let content = self.txtContent.text.trim()
            if let userID = currentUser?.id{
                self.sendFeedBack(userID: userID, name: name!, email: email!, phoneNumber: phoneNumber!, content: content)
            }else{
                self.sendFeedBack(userID: 0, name: name!, email: email!, phoneNumber: phoneNumber!, content: content)
            }
        }
    }
    
    func sendFeedBack(userID:Int, name:String, email:String, phoneNumber:String, content:String){
        let progress = self.showProgress()
        FeedBack().sendFeedBack(userID:userID, name:name, email:email, phoneNumber:phoneNumber, content:content, success: { (data) in
            if data != nil {
                self.hideProgress(progress)
                Toast(text: "Đã gửi góp ý thành công!").show()
                self .dismiss(animated: true) {
                    
                }
            } else {
                self.hideProgress(progress)
                Toast(text: "Gửi góp thất bại!").show()
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
   
    @IBAction func btnExit(_ sender: Any) {
        self .dismiss(animated: true) { 
            
        }
    }
    
    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController!, sourceViewController source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController: presented, presenting: presentingViewController)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if let result = textView.text {
            if !result.isEmpty{
                self.lblContent.isHidden = false
            }else{
                //self.lblContent.isHidden = true
                self.txtContent.text = ""
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtMaBS{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }else if textField == txtFullName{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == txtEmail{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == txtPhoneNumber{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 12
        }else{
            return true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = txtContent.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        return changedText.count <= 100
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
}
