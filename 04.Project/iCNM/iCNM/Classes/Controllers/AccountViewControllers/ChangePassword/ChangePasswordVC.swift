//
//  ChangePasswordVC
//  iCNM
//
//  Created by Mac osx on 11/02/17.
//  Copyright © 2017 Quang Hung. All rights reserved.
//

import UIKit
import Toaster
import RealmSwift

protocol ChangePasswordVCDelegate {
    func reloadDataTemplate(content:String)
}
class ChangePasswordVC: BaseViewController, UITextFieldDelegate {
  
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var oldPassword: FWFloatingLabelTextField!
    @IBOutlet weak var newPassword: FWFloatingLabelTextField!
    @IBOutlet weak var remindPassword: FWFloatingLabelTextField!
    @IBOutlet weak var lblWarning: UILabel!
    @IBOutlet weak var myBtnExit: UIButton!
    @IBOutlet weak var myBtnDone: UIButton!
    @IBOutlet weak var myView: UIView!
    var delegate:ChangePasswordVCDelegate?
    private var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        oldPassword.addRegx(strRegx: "^.{6,}$", errorMsg: "Mật khẩu không được ít hơn 6 ký tự")
        oldPassword.addRegx(strRegx: "^\\S*$", errorMsg: "Mật khẩu không được chứa khoảng trắng")
        newPassword.addRegx(strRegx: "^.{6,}$", errorMsg: "Mật khẩu không được ít hơn 6 ký tự")
        newPassword.addRegx(strRegx: "^\\S*$", errorMsg: "Mật khẩu không được chứa khoảng trắng")
        remindPassword.addRegx(strRegx: "^.{6,}$", errorMsg: "Mật khẩu không được ít hơn 6 ký tự")
        remindPassword.addRegx(strRegx: "^\\S*$", errorMsg: "Mật khẩu không được chứa khoảng trắng")
        myView.layer.cornerRadius = 10
        myView.layer.borderWidth = 1
        myView.layer.masksToBounds = true
        myBtnExit.layer.cornerRadius = 4
        myBtnExit.layer.masksToBounds = true
        myBtnDone.layer.cornerRadius = 4
        myBtnDone.layer.masksToBounds = true
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        self.searchBar.isHidden = true
        oldPassword.delegate = self
        newPassword.delegate = self
        remindPassword.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnExit(_ sender: Any) {
        self .dismiss(animated: true) {
            
        }
    }
    
    @IBAction func btnDone(_ sender: Any) {
        if let oldPasswordValue = self.oldPassword.text, oldPasswordValue.isEmpty{
            self.lblWarning.text = "Vui lòng nhập đầy đủ thông tin"
            return
        } else if let newPass = self.newPassword.text, newPass.isEmpty {
            self.lblWarning.text = "Vui lòng nhập đầy đủ thông tin"
            return
        }else if let remindPass = self.remindPassword.text, remindPass.isEmpty {
            self.lblWarning.text = "Vui lòng nhập đầy đủ thông tin"
            return
        }else{
            if self.newPassword.validate() && self.remindPassword.validate() && self.oldPassword.validate(){
                if let newPass = self.newPassword.text, let remindPass = self.remindPassword.text, let userID = currentUser?.id, let oldPassword = self.oldPassword.text{
                    if newPass==remindPass{
                        let progress = self.showProgress()
                        User().checkPassword(userID: userID, oldPassword:oldPassword, success: { (data) in
                            if data != nil {
                                self.hideProgress(progress)
                                self.updatePassword(userID: userID, password: newPass)
                            } else {
                                self.hideProgress(progress)
                                self.lblWarning.text = "Bạn nhập không đúng mật khẩu cũ!"
                            }
                        }, fail: { (error, response) in
                            self.hideProgress(progress)
                            self.handleNetworkError(error: error, responseData: response.data)
                        })
                    }else{
                        self.lblWarning.text = "Xác nhận lại mật khẩu mới không đúng!"
                    }
                }
            }
        }
    }
    
    func updatePassword(userID:Int, password:String){
        let progress = self.showProgress()
        User().updatePassword(userID: userID, password:password, success: { (data) in
            if data != nil {
                self.hideProgress(progress)
                self .dismiss(animated: true) {
                    Toast(text: "Cập nhật mật khẩu thành công!").show()
                }
            } else {
                self.hideProgress(progress)
                Toast(text: "Cập nhật mật khẩu thất bại").show()
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
