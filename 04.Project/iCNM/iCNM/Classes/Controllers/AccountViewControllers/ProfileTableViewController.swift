//
//  ProfileTableViewController.swift
//  iCNM
//
//  Created by Medlatec on 5/12/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift

class ProfileTableViewController: BaseTableViewController, SpecialistPopupViewControllerDelegate {
    
    private var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var avatarImageView: PASImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    private let titleKey = "Title"
    private let dataKey = "Data"
    private var dataArray = [[String:String]]()
    private var specialListSelect = [SpecialistModel]()
    private var specialistSelected = [SpecialistModel]()
    var arrChuyenKhoa = [String]()
    var userDefaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.allowsSelection = false
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        }
        updateAvatar()
        myTableView.register(UINib(nibName: "CustomMedicalBodyHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "headerCell")
    }
    
    private func updateAvatar() {
        if let user = currentUser, let userInfo = currentUser?.userDoctor?.userInfo {
            nameLabel.text = userInfo.name
            if user.userDoctor?.doctor != nil {
                if userInfo.doctorID == currentUser?.doctorID{
                    if currentUser?.userDoctor?.doctor?.verify == true{
                        roleLabel.text = "(Bác sĩ)"
                    }else{
                        roleLabel.text = "(Bác sĩ - Chưa xác thực)"
                    }
                }
            } else {
                roleLabel.text = "(Người dùng)"
            }
            if let avatarURL = userInfo.getAvatarURL() {
                avatarImageView.imageURL(URL: avatarURL)
            }
        } else {
            avatarImageView.reset()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        requestData()
    }
    
    override func requestData() {
        if let user = currentUser {
            let progress = showProgress()
            user.requestUserInformation(success: { (newUserInfo) in
                if newUserInfo.doctorID > 0 {
                    user.requestDoctorInformation(success: { (doctorInformation) in
                        self.hideProgress(progress)
                        self.prepareData()
                        self.updateAvatar()
                        self.tableView.reloadData()
                    }, fail: { (error, response) in
                        self.hideProgress(progress)
                        self.handleNetworkError(error: error, responseData: response.data)
                    })
                    
                } else {
                    self.hideProgress(progress)
                    self.prepareData()
                    self.updateAvatar()
                    self.tableView.reloadData()
                }
            }, fail: { (error, response) in
                self.hideProgress(progress)
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
    }
    
    private func prepareData() {
        dataArray = [[String:String]]()
        if let userInfo = currentUser?.userDoctor?.userInfo {
            var contactContent = ""
            
            contactContent += "\u{f098}\t" + (userInfo.phone ?? "chưa cập nhật")
            contactContent += "\n\u{f0e0}\t" + (userInfo.email ?? "chưa cập nhật")
            if let birthday = userInfo.birthday {
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = .short
                dateFormatter.timeStyle = .none
                dateFormatter.dateFormat = "dd/MM/yyyy"
                contactContent += "\n\u{f1fd}\t"+dateFormatter.string(from: birthday)
            } else {
                contactContent += "\n\u{f1fd}\tchưa cập nhật"
            }
            if let gender = userInfo.gender {
                if gender == "M" {
                    contactContent += "\n \u{f183}\tNam"
                } else {
                    contactContent += "\n \u{f182}\tNữ"
                }
            } else {
                contactContent += "\n\u{f228}\tchưa cập nhật"
            }
            
            contactContent += "\n\u{f015}\t" + (userInfo.address ?? "chưa cập nhật")
            
            let contactInformation:[String:String] = [
                titleKey: "Thông tin liên hệ",
                dataKey:contactContent
            ]
            dataArray.append(contactInformation)
            
            let currentJobInformation:[String:String] = [
                titleKey: "Nghề nghiệp hiện tại",
                dataKey:" \t" + (userInfo.job ?? "chưa cập nhật")
            ]
            dataArray.append(currentJobInformation)
            
            let workplaceInformation:[String:String] = [
                titleKey: "Nơi làm việc hiện tại",
                dataKey:" \t" + (userInfo.workplace ?? "chưa cập nhật")
            ]
            dataArray.append(workplaceInformation)
            
            let educationInformation:[String:String] = [
                titleKey: "Trình độ học vấn",
                dataKey: " \t" + (userInfo.education ?? "chưa cập nhật")
            ]
            dataArray.append(educationInformation)
            
            let aboutMeInformation:[String:String] = [
                titleKey: "Sơ lược bản thân",
                dataKey:" \t" + (userInfo.aboutMe ?? "chưa cập nhật")
            ]
            dataArray.append(aboutMeInformation)
            
        }
        if let doctorInformation = currentUser?.doctorInformation {
            let doctorSpecialists = doctorInformation.doctorSpecialists
            let specialists = doctorInformation.specialists
            var specialistString = ""
            if doctorSpecialists.count > 0 && specialists.count > 0 {
                
                let specialistIDs:[Int] = doctorSpecialists.map({ (doctorSpecialist) -> Int in
                    return doctorSpecialist.specialID
                })
                if specialistIDs.count > 0 {
                    let filterdSpecialists = specialists.filter(NSPredicate(format: "id IN %@", specialistIDs))
                    var count = 0
                    for specialist in filterdSpecialists {
                        if specialist.active {
                            if let specialistName = specialist.name {
                                if count > 0 {
                                    specialistString += "\n"
                                }
                                specialistString += " \t" + specialistName
                                count = count + 1
                            }
                        }
                    }
                }
            }
            
            let specialistInforamtion:[String:String] = [
                titleKey: "Chuyên khoa",
                dataKey: specialistString.count > 0 ? specialistString:" \tchưa cập nhật"
            ]
            dataArray.append(specialistInforamtion)
            
            //Work Experiences
            let workExperiences = doctorInformation.workExperiences
            var workExperiencesString = ""
            for (index,workExperience) in workExperiences.enumerated() {
                if index > 0 {
                    workExperiencesString += "\n\n"
                }
                if let position = workExperience.position {
                    workExperiencesString += " \t" + position + " "
                }
                if let company = workExperience.company {
                    workExperiencesString += "tại " + company
                }
                if let startDate = workExperience.startDate {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy"
                    workExperiencesString += "\n \tTừ " + dateFormatter.string(from: startDate)
                }
                if let endDate = workExperience.endDate {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy"
                    workExperiencesString += " đến " + dateFormatter.string(from: endDate)
                }
                if let summary = workExperience.summary {
                    workExperiencesString += "\n \t"+summary
                }
            }
            
            let workExperiencesInformation:[String:String] = [
                titleKey: "Kinh nghiệm",
                dataKey:workExperiencesString.count > 0 ? workExperiencesString:" \tchưa cập nhật"
            ]
            dataArray.append(workExperiencesInformation)
            //Education
            let educateds = doctorInformation.educateds
            var educationString = ""
            for (index,education) in educateds.enumerated() {
                if index > 0 {
                    educationString += "\n\n"
                }
                if let major = education.major {
                    educationString += " \tHọc " + major + " "
                }
                if let schoolName = education.schoolName {
                    educationString += "tại " + schoolName
                }
                if let startDate = education.startDate {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy"
                    educationString += "\n \tTừ " + dateFormatter.string(from: startDate)
                }
                if let endDate = education.endDate {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy"
                    educationString += " đến " + dateFormatter.string(from: endDate)
                }
                if let summary = education.summary {
                    educationString += "\n \t"+summary
                }
            }
            let educationInformation:[String:String] = [
                titleKey: "Đào tạo",
                dataKey: educationString.count > 0 ? educationString:" \tchưa cập nhật"
            ]
            dataArray.append(educationInformation)
            
            //Prizes
            let prizes = doctorInformation.prizes
            var prizesString = ""
            for (index,prize) in prizes.enumerated() {
                if index > 0 {
                    prizesString += "\n\n"
                }
                if let name = prize.name {
                    prizesString += " \tĐạt được " + name + " "
                }
                if let prizePlace = prize.prizePlace {
                    prizesString += "\n \tTại " + prizePlace
                }
                if let date = prize.prizeTime {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy"
                    prizesString += "\n \t" + dateFormatter.string(from: date)
                }
            }
            let prizesInformation:[String:String] = [
                titleKey: "Giải thưởng",
                dataKey:prizesString.count > 0 ? prizesString : " \tchưa cập nhật"
            ]
            dataArray.append(prizesInformation)
        }
    }
    
    @IBAction func touchRegisterSpecialist(_ sender: Any) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    
    // MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerCell") as! CustomMedicalBodyHeader
        headerView.lblNameSection.textColor = UIColor(hex:"1976D2")
        headerView.lblNameSection.textAlignment = NSTextAlignment.center
        headerView.lblNameSection.font = UIFont.fontAwesome(ofSize: 15.0)
        headerView.lblNameSection.text = String.fontAwesomeIcon(name: .medkit) + "  Bác sĩ đăng ký chuyên khoa"
        //headerView.lblNameSection.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        //headerView.lblNameSection.layer.borderWidth = 0.5
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapSectionName))
        headerView.isUserInteractionEnabled = true
        headerView.addGestureRecognizer(tapGesture)
        headerView.contentView.backgroundColor = UIColor.white
        return headerView
    }
    
    func handleTapSectionName(sender: UITapGestureRecognizer) {
        if let user = currentUser {
            if !(user.userDoctor?.doctor?.verify)!{
                showAlertView(title: "Tài khoản Bác sĩ của bạn chưa được iCNM xác thực. Vui lòng chờ hệ thống iCNM xác thực cho bạn hoặc liên hệ trực tiếp. Xin cảm ơn!", view: self)
                return
            }else{
                self.getSpecialList()
            }
        }
    }
    
    func getSpecialList() {
        if userDefaults.object(forKey: "registerAnswerQuestion") != nil{
            showAlertView(title: "Tài khoản Bác sĩ của bạn chưa được iCNM xác thực. Vui lòng chờ hệ thống iCNM xác thực cho bạn hoặc liên hệ trực tiếp. Xin cảm ơn!", view: self)
            return
        }else{
            Specialist.getAllSpecialist(success: { (results) in
                if !results.isEmpty {
                    for result in results {
                        self.specialListSelect.append(SpecialistModel(title: result.name!, isSelected: self.arrChuyenKhoa.contains(result.name!), isUserSelectEnable: true, specialist: result))
                    }
                    self.specialistSelected = self.specialListSelect.filter({ (specialistModel) -> Bool in
                        return specialistModel.isSelected
                    })
                    
                    self.showSpecialListSelect()
                    
                } else {
                    
                }
            }, fail: { (error, response) in
                
            })
        }
    }
    
    func showSpecialListSelect() {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let specialistPopup = storyboard.instantiateViewController(withIdentifier: "SpecialistPopup") as! SpecialistPopupViewController
        specialistPopup.modalPresentationStyle = .custom
        specialistPopup.dataArray = self.specialListSelect
        specialistPopup.delegate = self
        self.present(specialistPopup, animated: true, completion: nil)
    }
    
    //Delegate from popup select specialist
    func specialistDidUpdate(specialistPopup: SpecialistPopupViewController) {
        specialistSelected = self.specialListSelect.filter({ (specialistModel) -> Bool in
            return specialistModel.isSelected
        })
        self.arrChuyenKhoa.removeAll()
        for itemSelected in specialistSelected {
            let strID = "\(itemSelected.specialist.id)"
            self.arrChuyenKhoa.append(strID)
        }
        
        let specialistStr = self.arrChuyenKhoa.joined(separator: ",")
        if let user = self.currentUser{
            if self.arrChuyenKhoa.count != 0{
                self.registerAnswerQuestion(doctorID: user.doctorID, specialistStr: specialistStr)
            }else{
                showAlertView(title: "Bạn vui lòng chọn chuyên khoa đăng ký nhận trả lời câu hỏi", view: self)
                return
            }
        }
    }
    
    func registerAnswerQuestion(doctorID:Int, specialistStr:String){
        Doctor().registerAnswer(doctorID:doctorID, specialistStr:specialistStr, success: { (data) in
            if data != nil {
                showAlertView(title: "Bạn đã đăng ký trả lời câu hỏi trên iCNM thành công. Vui lòng chờ hệ thống iCNM liên hệ và xác thực thông tin. Xin cảm ơn!", view: self)
                self.userDefaults.set(true, forKey: "registerAnswerQuestion")
                self.userDefaults.synchronize()
            } else {
                showAlertView(title: "Đăng ký chuyên khoa trả lời câu hỏi thất bại!", view: self)
            }
        }, fail: { (error, response) in
            
        })
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if let user = self.currentUser{
            if user.userDoctor?.doctor?.id == nil{
                return 0
            }else{
                if (user.userDoctor?.doctor?.answer)!{
                    return 0
                }else{
                    return 40
                }
            }
        }else{
            return 40
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count + 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 && indexPath.row == dataArray.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FooterCell") as! ProfileFooterTableViewCell
            cell.selectionStyle = .none
            if let createdDate = currentUser?.userDoctor?.userInfo?.createdDate {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy"
                cell.footerLabel.text = "Đã tham gia iCNM vào \(formatter.string(from: createdDate))"
            }
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InformationCell", for: indexPath) as! ProfileInfoTableViewCell
        cell.infoTitleLabel.textAlignment = .left
        let cellData = dataArray[indexPath.row]
        cell.infoTitleLabel.text = cellData[titleKey]
        if let content = cellData[dataKey] {
            let contentAttributedString = NSMutableAttributedString(string: content)
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 5
            contentAttributedString.addAttribute(NSParagraphStyleAttributeName, value: style, range: NSRange(location: 0, length: content.count))
            cell.contentLabel.attributedText = contentAttributedString
        }
        // Configure the cell...
        
        return cell
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    @IBAction func touchBackButton(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
}
