//
//  ForgetPasswordVC.swift
//  iCNM
//
//  Created by Mac osx on 12/5/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Alamofire

class ForgetPasswordVC: BaseViewControllerNoSearchBar, UITextFieldDelegate {
    @IBOutlet weak var scrollView: FWCustomScrollView!
    @IBOutlet weak var phoneNumberTextField: FWTextFieldValidator!
    @IBOutlet weak var continueButton: UIButton!
    private var userRegister = UserRegister()
    private var userId = 0
    private var checkUserPhoneRequest:Request?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        // Setup the Search Controller
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(ForgetPasswordVC.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        title = "Lấy lại mật khẩu"
        
        phoneNumberTextField.addRegx(strRegx: "^[0-9]{10,11}$", errorMsg: "Số điện thoại không đúng định dạng")
        phoneNumberTextField.messageForValidatingLength = "Số điện thoại không được bỏ trống"
        phoneNumberTextField.delegate = self

        // Do any additional setup after loading the view.
        //self.navigationController?.isNavigationBarHidden = true
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        self.navigationController?.isNavigationBarHidden = true
//    }

    private func disableControls() {
        phoneNumberTextField.isEnabled = false
        continueButton.isEnabled = false
    }
    
    private func enableControls() {
        phoneNumberTextField.isEnabled = true
        continueButton.isEnabled = true
    }
    
    @IBAction func touchContinueHandler(_ sender: Any) {
        checkUserPhoneRequest?.cancel()
        if phoneNumberTextField.validate() {
            if let phoneNumber = phoneNumberTextField.text {
                userRegister.phoneNumber =  phoneNumber.hasPrefix("0") ? phoneNumber: "0"+phoneNumber
                disableControls()
                let progress = showProgress()
                checkUserPhoneRequest = User.getUserByPhone(phone: userRegister.phoneNumber, success: { (responseString) in
                    self.hideProgress(progress)
                    if responseString == "null" {
                        let alertViewController = UIAlertController(title: "Thông báo", message: "Số điện thoại '\(phoneNumber)' không tồn tại trên hệ thống iCNM", preferredStyle: .alert)
                        let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                        alertViewController.addAction(noAction)
                        self.present(alertViewController, animated: true, completion: nil)
                    } else {
                        let dict = self.convertToDictionary(text: responseString)
                        if let userID = dict!["UserID"]{
                            self.userId = userID as! Int
                            self.perform(segue: StoryboardSegue.LoginRegister.confirmCodePasswordVC, sender: self)
                        }
                    }
                    self.enableControls()
                }, fail: { (error, response) in
                    self.hideProgress(progress)
                    self.handleNetworkError(error: error, responseData: response.data)
                    self.enableControls()
                })
            }
        }else{
            showAlertView(title: "Số điện thoại không được bỏ trống", view: self)
            return
        }
    }
    
    @IBAction func touchBackButton(_ sender: Any) {
        if let navigationController = self.navigationController {
            checkUserPhoneRequest?.cancel()
            navigationController.dismiss(animated: true, completion: nil)
        }
        //self.backCurrentView()
    }
    
    @IBAction func touchScreen(_ sender: Any) {
        phoneNumberTextField.resignFirstResponder()
        //passwordTextField.resignFirstResponder()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == phoneNumberTextField {
            phoneNumberTextField.resignFirstResponder()
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegue.LoginRegister.confirmCodePasswordVC.rawValue {
            let destinationVC = segue.destination as! ConfirmCodePasswordVC
            destinationVC.userRegister = self.userRegister
            destinationVC.userID = userId
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
