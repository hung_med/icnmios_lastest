//
//  ConfirmPasswordVC.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import Toaster

class ConfirmPasswordVC: BaseViewControllerNoSearchBar, UITextFieldDelegate {
    var userRegister:UserRegister!
    var userID:Int = 0
    @IBOutlet weak var scrollView: FWCustomScrollView!
    @IBOutlet weak var passwordTextField: FWFloatingLabelTextField!
    private var currentTextField:UITextField?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem?.title = " "
        passwordTextField.addRegx(strRegx: "^.{6,}$", errorMsg: "Mật khẩu không được ít hơn 6 ký tự")
        passwordTextField.addRegx(strRegx: "^\\S*$", errorMsg: "Mật khẩu không được chứa khoảng trắng")
        registerForNotifications()
        passwordTextField.becomeFirstResponder()
        passwordTextField.delegate = self
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        // Do any additional setup after loading the view.
    }
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Cập nhật mật khẩu"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchScreen(_ sender: Any) {
        currentTextField?.resignFirstResponder()
    }
    
    @IBAction func touchContinueButton(_ sender: Any) {
        if passwordTextField.validate() {
            let password = passwordTextField.text!
            let progress = self.showProgress()
            User().updatePassword(userID: self.userID, password:password, success: { (data) in
                if data != nil {
                    self.hideProgress(progress)
                    self .dismiss(animated: true) {
                        Toast(text: "Cập nhật mật khẩu thành công!").show()
                    }
                } else {
                    self.hideProgress(progress)
                    Toast(text: "Cập nhật mật khẩu thất bại").show()
                }
            }, fail: { (error, response) in
                self.hideProgress(progress)
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
    }
    
    @objc private func keyboardWillShow(aNotification:Notification) {
        if let curTextField = currentTextField {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            scrollView.contentInset = contentInset
            scrollView.scrollIndicatorInsets = contentInset
            let rect = curTextField.frame
            let test = view.bounds.height - (rect.size.height + rect.origin.y + scrollView.contentInset.top) - 40.0
            let different = keyboardFrame.size.height - test
            if different > 0 {
                scrollView.contentOffset.y = different - self.scrollView.contentInset.top
            }
        }
        
    }
    
    @objc private func keyboardWillHide(aNotification:Notification) {
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = 0
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTextField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

