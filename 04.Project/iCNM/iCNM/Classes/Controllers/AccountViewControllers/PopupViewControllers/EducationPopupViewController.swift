//
//  EducationPopupViewController.swift
//  iCNM
//
//  Created by Medlatec on 5/23/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

@objc protocol EducationPopupViewControllerDelegate : class {
    @objc optional func educationDidUpdate(educationPopup:EducationPopupViewController, education:Education?)
}

class EducationPopupViewController: UIViewController, UITextFieldDelegate {

    var education:Education?
    weak var delegate:EducationPopupViewControllerDelegate?
    
    @IBOutlet weak var schoolNameTextField: FWFloatingLabelTextField!
    @IBOutlet weak var majorTextField: FWFloatingLabelTextField!
    @IBOutlet weak var summaryTextField: FWFloatingLabelTextField!
    @IBOutlet weak var startYearTextField: FWFloatingLabelTextField!
    @IBOutlet weak var endYearTextField: FWFloatingLabelTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        startYearTextField.delegate = self
        endYearTextField.delegate = self
        schoolNameTextField.delegate = self
        majorTextField.delegate = self
        summaryTextField.delegate = self
        if let edu = education {
            schoolNameTextField.text = edu.schoolName
            majorTextField.text = edu.major
            summaryTextField.text = edu.summary
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            startYearTextField.text = edu.startDate != nil ? dateFormatter.string(from: edu.startDate!) : ""
            endYearTextField.text = edu.endDate != nil ? dateFormatter.string(from: edu.endDate!) : ""
        }
        
        startYearTextField.addRegx(strRegx: "^[0-9]{4}$", errorMsg: "Năm bạn nhập chưa đúng định dạng")
        endYearTextField.addRegx(strRegx: "^[0-9]{4}$", errorMsg: "Năm bạn nhập chưa đúng định dạng")
        startYearTextField.validateBlock = {
            [weak self]() -> (String?) in
            if let startText = self?.startYearTextField.text, let value = Int(startText) {
                let currentYear = Calendar.current.component(.year, from: Date())
                if value > currentYear || value < 1951 {
                    return "Bạn chỉ được nhập năm trong khoảng từ 1951 đến \(currentYear)"
                }
                
                if let endText = self?.endYearTextField.text, let value2 = Int(endText) {
                    if value > value2 {
                        return "Năm bắt đầu không được lớn hơn năm kết thúc"
                    }
                }
                
                return nil
            } else {
                return "Năm bạn nhập chưa đúng định dạng"
            }
        }
        
        endYearTextField.validateBlock = {
            [weak self]() -> (String?) in
            if let endText = self?.endYearTextField.text, let value = Int(endText) {
                let currentYear = Calendar.current.component(.year, from: Date())
                if value > currentYear || value < 1951 {
                    return "Bạn chỉ được nhập năm trong khoảng từ 1951 đến \(currentYear)"
                }
                
                if let startText = self?.startYearTextField.text, let value2 = Int(startText) {
                    if value < value2 {
                        return "Năm kết thúc không được nhỏ hơn năm bắt đầu"
                    }
                }
                
                return nil
            } else {
                return "Năm bạn nhập chưa đúng định dạng"
            }
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchDoneButton(_ sender: Any) {
        if (schoolNameTextField.validate() && majorTextField.validate() && summaryTextField.validate() && startYearTextField.validate() && endYearTextField.validate()) {
        if let education = education {
            education.schoolName = schoolNameTextField.text?.trim()
            education.major = majorTextField.text?.trim()
            education.summary = summaryTextField.text?.trim()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            education.startDate = dateFormatter.date(from: startYearTextField.text!.trim())
            education.endDate = dateFormatter.date(from: endYearTextField.text!.trim())
            delegate?.educationDidUpdate?(educationPopup: self, education: nil)
        } else {
            let newEducation = Education()
            newEducation.schoolName = schoolNameTextField.text?.trim()
            newEducation.major = majorTextField.text?.trim()
            newEducation.summary = summaryTextField.text?.trim()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            newEducation.startDate = dateFormatter.date(from: startYearTextField.text!.trim())
            newEducation.endDate = dateFormatter.date(from: endYearTextField.text!.trim())
            delegate?.educationDidUpdate?(educationPopup: self, education: newEducation)
        }
        dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func touchCancleButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == schoolNameTextField {
            schoolNameTextField.resignFirstResponder()
        }

        if textField == majorTextField {
            majorTextField.resignFirstResponder()
        }

        if textField == summaryTextField {
            summaryTextField.resignFirstResponder()
        }

        if textField == startYearTextField {
            startYearTextField.resignFirstResponder()
        }

        if textField == endYearTextField {
            endYearTextField.resignFirstResponder()
        }

        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == startYearTextField {
            endYearTextField.validate()
        }
        
        if textField == endYearTextField {
            startYearTextField.validate()
        }
    }
    
    @IBAction func touchScreen(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func touchPopupBackground(_ sender: Any) {
        view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == schoolNameTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 100
        }else if textField == majorTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == summaryTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == startYearTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == endYearTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else{
            return true
        }
    }
}
