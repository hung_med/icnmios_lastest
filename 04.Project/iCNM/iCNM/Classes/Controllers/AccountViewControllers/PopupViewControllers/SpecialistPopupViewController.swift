//
//  SpecialistPopupViewController.swift
//  iCNM
//
//  Created by Medlatec on 5/29/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

@objc protocol SpecialistPopupViewControllerDelegate : class {
    @objc optional func specialistDidUpdate(specialistPopup:SpecialistPopupViewController)
}

class SpecialistPopupViewController: UIViewController {
    
    var dataArray:[SpecialistModel]!
    var dataArray2:[TestMedicalScheduleModel]!
    weak var delegate:SpecialistPopupViewControllerDelegate?
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var amChoiceView: AMChoice!
    override func viewDidLoad() {
        super.viewDidLoad()
        amChoiceView.selectionType = .multiple
        amChoiceView.data = dataArray
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchDoneButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        delegate?.specialistDidUpdate?(specialistPopup: self)
    }
    
    @IBAction func touchBackground(_ sender: Any) {
        dismiss(animated: true, completion: nil)        
    }
    @IBAction func touchDeleteButton(_ sender: Any) {
        dataArray.forEach { (specialistModel) in
            specialistModel.isSelected = false;
        }
        dismiss(animated: true, completion: nil)
        amChoiceView.tableView.reloadData()
    }
    
}
