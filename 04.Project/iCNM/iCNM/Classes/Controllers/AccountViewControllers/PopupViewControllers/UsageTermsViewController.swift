//
//  UsageTermsViewController.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class UsageTermsViewController: UIViewController,UIWebViewDelegate {
    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        let url = URL(string: Constant.usageTermsURL)
        let urlRequest = URLRequest(url: url!)
        webView.loadRequest(urlRequest)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchAgreeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
