//
//  PrizePopupViewController.swift
//  iCNM
//
//  Created by Medlatec on 5/23/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

@objc protocol PrizePopupViewControllerDelegate : class {
    @objc optional func prizePopupDidUpdate(prizePopup:PrizePopupViewController, prize:Prize?)
}

class PrizePopupViewController: UIViewController, UITextFieldDelegate{

    var prize:Prize?
    weak var delegate:PrizePopupViewControllerDelegate?
    
    @IBOutlet weak var prizeNameTextField: FWFloatingLabelTextField!
    @IBOutlet weak var prizePlaceTextField: FWFloatingLabelTextField!
    @IBOutlet weak var prizeYearTextField: FWFloatingLabelTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        prizeNameTextField.delegate = self
        prizePlaceTextField.delegate = self
        prizeYearTextField.delegate = self
        if let prize = prize {
            prizeNameTextField.text = prize.name
            prizePlaceTextField.text = prize.prizePlace
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            prizeYearTextField.text = prize.prizeTime != nil ? dateFormatter.string(from: prize.prizeTime!) : ""
        }
        
        prizeYearTextField.addRegx(strRegx: "^[0-9]{4}$", errorMsg: "Năm bạn nhập chưa đúng định dạng")
        prizeYearTextField.validateBlock = {
            [weak self]() -> (String?) in
            if let startText = self?.prizeYearTextField.text, let value = Int(startText) {
                let currentYear = Calendar.current.component(.year, from: Date())
                if value > currentYear || value < 1951 {
                    return "Bạn chỉ được nhập năm trong khoảng từ 1951 đến \(currentYear)"
                }
                
                return nil
            } else {
                return "Năm bạn nhập chưa đúng định dạng"
            }
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchDoneButton(_ sender: Any) {
        if (prizeNameTextField.validate() && prizePlaceTextField.validate() && prizeYearTextField.validate()) {
        if let prize = prize {
            prize.name = prizeNameTextField.text?.trim()
            prize.prizePlace = prizePlaceTextField.text?.trim()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            prize.prizeTime = dateFormatter.date(from: prizeYearTextField.text!.trim())
            delegate?.prizePopupDidUpdate?(prizePopup: self, prize: nil)
        } else {
            let newPrize = Prize()
            newPrize.name = prizeNameTextField.text?.trim()
            newPrize.prizePlace = prizePlaceTextField.text?.trim()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            newPrize.prizeTime = dateFormatter.date(from: prizeYearTextField.text!.trim())
            delegate?.prizePopupDidUpdate?(prizePopup: self, prize: newPrize)
        }
        dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func touchCancleButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == prizeNameTextField {
            prizeNameTextField.resignFirstResponder()
        }
        
        if textField == prizePlaceTextField {
            prizePlaceTextField.resignFirstResponder()
        }
        
        if textField == prizeYearTextField {
            prizeYearTextField.resignFirstResponder()
        }
        
        return true
    }
    @IBAction func touchPopupBackground(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func touchScreen(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == prizeNameTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 100
        }else if textField == prizePlaceTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 100
        }else if textField == prizeYearTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else{
            return true
        }
    }

}
