//
//  BirthdayInputViewController.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class BirthdayInputViewController: UIViewController, FWDatePickerDelegate {

    var userRegister:UserRegister!
    @IBOutlet weak var birthdayPicker: FWDatePicker!
    @IBOutlet weak var scrollView: FWCustomScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        birthdayPicker.contentHorizontalAlignment = .center
        birthdayPicker.maximumDate = Date()
        self.navigationItem.backBarButtonItem?.title = " "
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Thoát", style: .plain, handler: { (button) in
            let alertViewController = UIAlertController(title: "Thông báo", message: "Hủy bỏ quá trình đăng ký", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Đồng ý", style: UIAlertActionStyle.default, handler: { (action) in
                if let array = self.navigationController?.viewControllers, array.count > 1 {
                    self.navigationController?.popToViewController(array[1], animated: true)
                }
            })
            let noAction = UIAlertAction(title: "Huỷ bỏ", style: UIAlertActionStyle.default, handler:nil)
            alertViewController.addAction(noAction)
            alertViewController.addAction(yesAction)
            self.present(alertViewController, animated: true, completion: nil)
        })
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Cập nhật Ngày sinh"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func touchContinueButton(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        userRegister.birthday = dateFormatter.string(from: birthdayPicker.currentDate)
        perform(segue: StoryboardSegue.LoginRegister.showInputGenderViewController, sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegue.LoginRegister.showInputGenderViewController.rawValue {
            let destinationVC = segue.destination as! GenderInputViewController
            destinationVC.userRegister = self.userRegister
        }
    }

}
