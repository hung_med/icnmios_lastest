//
//  RegisterViewController.swift
//  iCNM
//
//  Created by Medlatec on 5/15/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Alamofire
import GoogleSignIn
import FBSDKLoginKit
import RealmSwift

class RegisterViewController: BaseViewControllerNoSearchBar,UITextFieldDelegate,GIDSignInUIDelegate,GIDSignInDelegate {
    
    @IBOutlet weak var scrollView: FWCustomScrollView!
    @IBOutlet weak var phoneTextField: FWFloatingLabelTextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var facebookLoginButton: UIButton!
    @IBOutlet weak var googleLoginButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var termsButton: UIButton!
    
    private var currentTextField:UITextField?
    private var userRegister = UserRegister()
    private var currentRequest:Request?
    private var checkUserPhoneRequest:Request?
    private var googleProgress:MKActivityIndicator?
    var isCheck:Bool? = false
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneTextField.delegate = self
        registerForNotifications()
        phoneTextField.addRegx(strRegx: "^[0-9]{10,11}$", errorMsg: "Số điện thoại không đúng định dạng!")
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    private func disableControls() {
        phoneTextField.isEnabled = false
        registerButton.isEnabled = false
        facebookLoginButton.isEnabled = false
        googleLoginButton.isEnabled = false
        termsButton.isEnabled = false
        loginButton.isEnabled = false
    }
    
    private func enableControls() {
        phoneTextField.isEnabled = true
        registerButton.isEnabled = true
        facebookLoginButton.isEnabled = true
        googleLoginButton.isEnabled = true
        termsButton.isEnabled = true
        loginButton.isEnabled = true
    }
    
    @IBAction func touchContinueButton(_ sender: Any) {
        checkUserPhoneRequest?.cancel()
        if phoneTextField.validate() {
            if let phoneNumber = phoneTextField.text {
                userRegister.phoneNumber =  phoneNumber.hasPrefix("0") ? phoneNumber: "0"+phoneNumber
                disableControls()
                let progress = showProgress()
                checkUserPhoneRequest = User.getUserByPhone(phone: userRegister.phoneNumber, success: { (responseString) in
                    self.hideProgress(progress)
                    if responseString == "null" {
                        var userTypeActionSheet:UIAlertController? = nil
                        if IS_IPAD{
                            userTypeActionSheet = UIAlertController(title: nil, message: "Chọn loại tài khoản", preferredStyle: .alert)
                        }else{
                            userTypeActionSheet = UIAlertController(title: nil, message: "Chọn loại tài khoản", preferredStyle: .actionSheet)
                        }
                        
                        let personalAccount = UIAlertAction(title: "Tài khoản cá nhân", style: .default) { (action) in
                            self.userRegister.userTypeID = 1
                            self.perform(segue: StoryboardSegue.LoginRegister.showVerifyPhoneScene, sender: self)
                        }
                        let doctorAccount = UIAlertAction(title: "Bác sĩ", style: .default) { (action) in
                            self.userRegister.userTypeID = 2
                            self.perform(segue: StoryboardSegue.LoginRegister.showVerifyPhoneScene, sender: self)
                        }
                        let cancelAction = UIAlertAction(title: "Đóng", style: .cancel, handler: nil)
                        userTypeActionSheet?.addAction(personalAccount)
                        userTypeActionSheet?.addAction(doctorAccount)
                        userTypeActionSheet?.addAction(cancelAction)
                        self.present(userTypeActionSheet!, animated: true, completion: nil)
                    } else {
                        let alertViewController = UIAlertController(title: "Thông báo", message: "Số điện thoại này đã đăng ký", preferredStyle: .alert)
                        let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                        alertViewController.addAction(noAction)
                        self.present(alertViewController, animated: true, completion: nil)
                    }
                    self.enableControls()
                }, fail: { (error, response) in
                    self.hideProgress(progress)
                    self.handleNetworkError(error: error, responseData: response.data)
                    self.enableControls()
                })
            }
        }
    }
    
    
    @IBAction func touchFacebookButton(_ sender: Any) {
        let activityIndicator = self.showProgress()
        currentRequest?.cancel()
        FBSDKLoginManager().logIn(withReadPermissions: ["public_profile","email","user_birthday"], from: self) { (result, error) in
            if error != nil {
                self.hideProgress(activityIndicator)
            } else if let r = result, r.isCancelled != true {
                FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "name, email, picture.type(large), birthday, gender"]).start(completionHandler: { (connection, result, error) in
                    if error != nil {
                        self.hideProgress(activityIndicator)
                        let alertViewController = UIAlertController(title: "Thông báo", message: "Không thể lấy thông tin user facebook!", preferredStyle: .alert)
                        let noAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
                        alertViewController.addAction(noAction)
                        self.present(alertViewController, animated: true, completion: nil)
                    } else {
                        if let userInformation = result as? [String:Any] {
                            var parameters = [String:Any]()
                            if let emailUser = userInformation["email"] as? String {
                                parameters["emailUser"] = emailUser
                            }
                            if let userBirthday = userInformation["birthday"] as? String {
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "MM/dd/yyyy"
                                if let birthday = dateFormatter.date(from: userBirthday) {
                                    dateFormatter.dateFormat = "yyyy-MM-dd"
                                    let stringBirthday = dateFormatter.string(from: birthday)
                                    parameters["birthday"] = stringBirthday
                                }
                            }
                            
                            if let gender = userInformation["gender"] as? String {
                                if gender == "male" {
                                    parameters["gender"] = 1
                                } else {
                                    parameters["gender"] = 0
                                }
                            }
                            if let name = userInformation["name"] as? String {
                                parameters["name"] = name
                            }
                            if let pictureDic = userInformation["picture"] as? [String:Any] {
                                if let data = pictureDic["data"] as? [String:Any] {
                                    if let pictureURL = data["url"] as? String {
                                        parameters["avatar"] = pictureURL
                                    }
                                }
                            }
                            self.currentRequest = User.doLoginSocial(parameters:parameters, success: { (userDoctor) in
                                self.hideProgress(activityIndicator)
                                if userDoctor.id == 0 {
                                    let alertViewController = UIAlertController(title: "Thông báo", message: "Số điện thoại không tồn tại!", preferredStyle: .alert)
                                    let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                                    alertViewController.addAction(noAction)
                                    self.present(alertViewController, animated: true, completion: nil)
                                } else {
                                    autoreleasepool {
                                        do {
                                            let realm = try Realm()
                                            let user = User()
                                            user.id = userDoctor.id
                                            if let doctorID = userDoctor.userInfo?.doctorID {
                                                user.doctorID =  doctorID
                                            }
                                            user.userDoctor = userDoctor
                                            try realm.write {
                                                realm.add(user, update: true)
                                            }
                                            NotificationCenter.default.post(name: Constant.NotificationMessage.loginSuccess, object: nil)
                                            self.navigationController?.dismiss(animated: true, completion: nil)
                                        } catch let error {
                                            let alertViewController = UIAlertController(title: "Thông báo", message: "Không thể lưu thông tin đăng nhập:" + error.localizedDescription, preferredStyle: .alert)
                                            let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                                            alertViewController.addAction(noAction)
                                            self.present(alertViewController, animated: true, completion: nil)
                                        }
                                    }
                                }
                            }, fail: { (error, response) in
                                self.hideProgress(activityIndicator)
                                self.handleNetworkError(error: error, responseData: response.data)
                            })
                        } else {
                            self.hideProgress(activityIndicator)
                        }
                    }
                })
            } else {
                self.hideProgress(activityIndicator)
            }
        }
    }
    
    @IBAction func touchGoogleButton(_ sender: Any) {
        googleProgress = showProgress()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func touchLoginButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc private func keyboardWillShow(aNotification:Notification) {
        if let curTextField = currentTextField {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            scrollView.contentInset = contentInset
            scrollView.scrollIndicatorInsets = contentInset
            let rect = curTextField.frame
            let test = view.bounds.height - (rect.size.height + rect.origin.y + scrollView.contentInset.top) - 40.0
            let different = keyboardFrame.size.height - test
            if different > 0 {
                scrollView.contentOffset.y = different - self.scrollView.contentInset.top
            }
        }
        
    }
    
    @objc private func keyboardWillHide(aNotification:Notification) {
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = 0
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTextField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == phoneTextField {
            phoneTextField.resignFirstResponder()
        }
        return true
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegue.LoginRegister.showVerifyPhoneScene.rawValue {
            let destinationVC = segue.destination as! VerifyPhoneViewController
            destinationVC.userRegister = self.userRegister
        }
    }
    
    @IBAction func touchScreen(_ sender: Any) {
        currentTextField?.resignFirstResponder()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    //GoogleSignIn Delegate
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        viewController.modalPresentationStyle = UIModalPresentationStyle.currentContext
        present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        if let err = error {
            print("Failed to log into Google: ", err)
            return
        }
        
        print("Successfully logged into Google", user)
        
        //Login to webservie
        
        var parameters = [String:Any]()
        parameters["emailUser"] = user.profile.email
        parameters["name"] = user.profile.name
        if user.profile.hasImage {
            parameters["avatar"] = user.profile.imageURL(withDimension: 200).absoluteString
        }
        
        parameters["gender"] = ""
        parameters["birthday"] = ""
        let activityIndicator = showProgress()
        self.currentRequest?.cancel()
        self.currentRequest = User.doLoginSocial(parameters:parameters, success: { (userDoctor) in
            self.hideProgress(activityIndicator)
            if userDoctor.id == 0 {
                let alertViewController = UIAlertController(title: "Thông báo", message: "Số điện thoại không tồn tại!", preferredStyle: .alert)
                let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                alertViewController.addAction(noAction)
                self.present(alertViewController, animated: true, completion: nil)
                GIDSignIn.sharedInstance().signOut()
            } else {
                autoreleasepool {
                    do {
                        let realm = try Realm()
                        let user = User()
                        user.id = userDoctor.id
                        if let doctorID = userDoctor.userInfo?.doctorID {
                            user.doctorID =  doctorID
                        }
                        user.userDoctor = userDoctor
                        try realm.write {
                            realm.add(user, update: true)
                        }
                        NotificationCenter.default.post(name: Constant.NotificationMessage.loginSuccess, object: nil)
                        self.navigationController?.dismiss(animated: true, completion: nil)
                    } catch let error {
                        let alertViewController = UIAlertController(title: "Thông báo", message: "Không thể lưu thông tin đăng nhập:" + error.localizedDescription, preferredStyle: .alert)
                        let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                        alertViewController.addAction(noAction)
                        self.present(alertViewController, animated: true, completion: nil)
                    }
                }
            }
        }, fail: { (error, response) in
            self.hideProgress(activityIndicator)
            self.handleNetworkError(error: error, responseData: response.data)
            GIDSignIn.sharedInstance().signOut()
        })
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        hideProgress(googleProgress)
        googleProgress = nil
    }
    @IBAction func touchBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated:true)
        if isCheck!{
            if let navigationController = self.navigationController {
                currentRequest?.cancel()
                navigationController.dismiss(animated: true, completion: nil)
            }
        }
    }
}
