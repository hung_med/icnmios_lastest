//
//  LoginViewController.swift
//  iCNM
//
//  Created by Medlatec on 5/11/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import Firebase
import GoogleSignIn
import FBSDKLoginKit

@objc protocol LoginViewControllerDelegate {
    @objc optional func backCurrentView()
}

class LoginViewController: BaseViewController, UITextFieldDelegate,GIDSignInUIDelegate,GIDSignInDelegate {
    @IBOutlet weak var scrollView: FWCustomScrollView!
    @IBOutlet weak var phoneNumberTextField: FWTextFieldValidator!
    @IBOutlet weak var passwordTextField: FWTextFieldValidator!
    @IBOutlet weak var loginButton: UIButton!
    private var currentTextField:UITextField?
    private var currentRequest:Request?
    private var googleProgress:MKActivityIndicator?
    var myDelegate:LoginViewControllerDelegate?
    @IBOutlet weak var btnGoogle: UIButton!
    @IBOutlet weak var btnFacebook: UIButton!
    var isCheck:Bool? = false
    override func viewDidLoad() {
        super.viewDidLoad()
        registerForNotifications()
        //        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(touchDoneButton))
        //        self.navigationItem.rightBarButtonItem = doneButton
        phoneNumberTextField.addRegx(strRegx: "^[0-9]{10,11}$", errorMsg: "Số điện thoại không đúng định dạng")
        phoneNumberTextField.messageForValidatingLength = "Số điện thoại không được bỏ trống"
        passwordTextField.addRegx(strRegx: "^.{6,}$", errorMsg:"Mật khẩu phải có tối thiểu 6 ký tự")
        passwordTextField.messageForValidatingLength = "Mật khẩu không được bỏ trống"
        phoneNumberTextField.delegate = self
        passwordTextField.delegate = self
        self.btnFacebook.layer.cornerRadius = 4
        self.btnFacebook.layer.masksToBounds = true
        self.btnFacebook.layer.borderWidth = 1
        self.btnFacebook.layer.borderColor = UIColor.darkGray.cgColor
        //   UIColor.init(hexColor: 0x3B5998, alpha: 1.0)
        self.btnGoogle.layer.cornerRadius = 4
        self.btnGoogle.layer.masksToBounds = true
        self.btnGoogle.layer.borderWidth = 1
        self.btnGoogle.layer.borderColor = UIColor.red.cgColor
        addDoneButtonOnKeyboard()
        self.navigationController?.isNavigationBarHidden = true
        self.searchBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    @IBAction func touchLoginButton(_ sender: UIButton) {
        currentRequest?.cancel()
        if (phoneNumberTextField.text?.isEmpty)!{
            showAlertView(title: "Số điện thoại không được bỏ trống", view: self)
            return
        }else if (passwordTextField.text?.isEmpty)!{
            showAlertView(title: "Mật khẩu không được bỏ trống", view: self)
            return
        }
        let isValidPhone = Int(phoneNumberTextField.text!)
        if isValidPhone != nil{
            if (phoneNumberTextField.validate() && passwordTextField.validate()) {
                loginButton.isEnabled = false
                let progress = showProgress()
                currentRequest = User.doLogin(phoneNumber: phoneNumberTextField.text!, password: passwordTextField.text!, success: { (response) in
                    self.hideProgress(progress)
                    self.loginButton.isEnabled = true
                    if response == "null" {
                        let alertViewController = UIAlertController(title: "Đăng Nhập", message: "Thông tin đăng nhập không chính xác", preferredStyle: .alert)
                        let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                        alertViewController.addAction(noAction)
                        self.present(alertViewController, animated: true, completion: nil)
                    } else if let userDoctor = UserDoctor(JSONString: response) {
                        if userDoctor.id == 0 {
                            let alertViewController = UIAlertController(title: "Thông báo", message: "Số điện thoại không tồn tại", preferredStyle: .alert)
                            let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                            alertViewController.addAction(noAction)
                            self.present(alertViewController, animated: true, completion: nil)
                        } else {
                            autoreleasepool {
                                do {
                                    let realm = try Realm()
                                    let user = User()
                                    user.id = userDoctor.id
                                    if let doctorID = userDoctor.userInfo?.doctorID {
                                        user.doctorID =  doctorID
                                    }
                                    user.userDoctor = userDoctor
                                    try realm.write {
                                        realm.add(user, update: true)
                                    }
                                    
                                    if self.isCheck!{
                                        NotificationCenter.default.post(name: Constant.NotificationMessage.loginSuccess2, object: nil)
                                        self.navigationController?.dismiss(animated: true, completion: nil)
                                    }else{
                                        NotificationCenter.default.post(name: Constant.NotificationMessage.loginSuccess, object: nil)
                                        self.navigationController?.dismiss(animated: true, completion: nil)
                                        self.backCurrentView()
                                    }
                                    
                                    
                                } catch let error {
                                    let alertViewController = UIAlertController(title: "Thông báo", message: "Không thể lưu thông tin đăng nhập:" + error.localizedDescription, preferredStyle: .alert)
                                    let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                                    alertViewController.addAction(noAction)
                                    self.present(alertViewController, animated: true, completion: nil)
                                }
                            }
                        }
                    } else {
                        let alertViewController = UIAlertController(title: "Đăng Nhập", message: "Có lỗi xảy ra", preferredStyle: .alert)
                        let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                        alertViewController.addAction(noAction)
                        self.present(alertViewController, animated: true, completion: nil)
                    }
                }, fail: { (error, response) in
                    self.hideProgress(progress)
                    self.handleNetworkError(error: error, responseData: response.data)
                    self.loginButton.isEnabled = true
                })
            }
        }else{
            let isCheck = phoneNumberTextField.text?.trimmingCharacters(in: .whitespaces)
            let progress = showProgress()
            if !(isCheck?.isEmpty)!{
                currentRequest = User.doLoginDoctorUnit(userWeb: phoneNumberTextField.text!, passWeb: passwordTextField.text!, success: { (response) in
                    self.hideProgress(progress)
                    self.loginButton.isEnabled = true
                    if response == "null" {
                        let alertViewController = UIAlertController(title: "Đăng Nhập", message: "Thông tin đăng nhập không chính xác", preferredStyle: .alert)
                        let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                        alertViewController.addAction(noAction)
                        self.present(alertViewController, animated: true, completion: nil)
                    }
                    else if let userDoctor = UserDoctor(JSONString: response) {
                        if userDoctor.id == 0 {
                            let alertViewController = UIAlertController(title: "Thông báo", message: "Thông tin đơn vị không tồn tại", preferredStyle: .alert)
                            let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                            alertViewController.addAction(noAction)
                            self.present(alertViewController, animated: true, completion: nil)
                        } else {
                            autoreleasepool {
                                do {
                                    let realm = try Realm()
                                    let user = User()
                                    user.id = userDoctor.id
                                    if let doctorID = userDoctor.userInfo?.doctorID {
                                        user.doctorID =  doctorID
                                    }
                                    user.userDoctor = userDoctor
                                    try realm.write {
                                        realm.add(user, update: true)
                                    }
                                    
                                    if self.isCheck!{
                                        NotificationCenter.default.post(name: Constant.NotificationMessage.loginSuccess2, object: nil)
                                        self.navigationController?.dismiss(animated: true, completion: nil)
                                    }else{
                                        NotificationCenter.default.post(name: Constant.NotificationMessage.loginSuccess, object: nil)
                                        self.navigationController?.dismiss(animated: true, completion: nil)
                                        self.backCurrentView()
                                    }
                                    
                                    
                                } catch let error {
                                    let alertViewController = UIAlertController(title: "Thông báo", message: "Không thể lưu thông tin đăng nhập:" + error.localizedDescription, preferredStyle: .alert)
                                    let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                                    alertViewController.addAction(noAction)
                                    self.present(alertViewController, animated: true, completion: nil)
                                }
                            }
                        }
                    } else {
                        let alertViewController = UIAlertController(title: "Đăng Nhập", message: "Có lỗi xảy ra", preferredStyle: .alert)
                        let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                        alertViewController.addAction(noAction)
                        self.present(alertViewController, animated: true, completion: nil)
                    }
                }, fail: { (error, response) in
                    self.hideProgress(progress)
                    self.handleNetworkError(error: error, responseData: response.data)
                    self.loginButton.isEnabled = true
                })
            }else{
                self.hideProgress(progress)
                let alertViewController = UIAlertController(title: "Thông báo", message: "Thông tin đăng nhập không chính xác", preferredStyle: .alert)
                let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                alertViewController.addAction(noAction)
                self.present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func touchScreen(_ sender: Any) {
        phoneNumberTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func keyboardWillShow(aNotification:Notification) {
        if let curTextField = currentTextField {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            scrollView.contentInset = contentInset
            scrollView.scrollIndicatorInsets = contentInset
            let rect = curTextField.frame
            let test = view.bounds.height - (rect.size.height + rect.origin.y + scrollView.contentInset.top) - 40.0
            let different = keyboardFrame.size.height - test
            if different > 0 {
                scrollView.contentOffset.y = different - self.scrollView.contentInset.top
            }
        }
        
    }
    
    @objc private func keyboardWillHide(aNotification:Notification) {
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = 0
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTextField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == phoneNumberTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            passwordTextField.resignFirstResponder()
            touchLoginButton(loginButton)
        }
        return true
    }
    
    @objc private func nextButtonAction() {
        if currentTextField == phoneNumberTextField {
            passwordTextField.becomeFirstResponder()
        }
    }
    
    private func addDoneButtonOnKeyboard()
    {
        let nextToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0,y: 0,width: 320,height: 50))
        nextToolbar.barTintColor = UIColor.white
        //        nextToolbar.tintColor = UIColor.black
        nextToolbar.isTranslucent = true
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let nextBtn: UIBarButtonItem = UIBarButtonItem(title: "Tiếp", style: UIBarButtonItemStyle.plain, target: self, action: #selector(nextButtonAction))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(nextBtn)
        
        nextToolbar.items = items
        nextToolbar.sizeToFit()
        
        phoneNumberTextField.inputAccessoryView = nextToolbar
    }
    
    @IBAction func touchFacebookLoginButton(_ sender: Any) {
        let activityIndicator = self.showProgress()
        currentRequest?.cancel()
        FBSDKLoginManager().logIn(withReadPermissions: ["public_profile","email"], from: self) { (result, error) in
            if error != nil {
                self.hideProgress(activityIndicator)
            } else if let r = result, r.isCancelled != true {
                FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id, email, name, picture, gender, birthday"]).start(completionHandler: { (connection, result, error) in
                    if error != nil {
                        self.hideProgress(activityIndicator)
                        let alertViewController = UIAlertController(title: "Thông báo", message: "Không thể lấy thông tin user facebook!", preferredStyle: .alert)
                        let noAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
                        alertViewController.addAction(noAction)
                        self.present(alertViewController, animated: true, completion: nil)
                    } else {
                        if let userInformation = result as? [String:Any] {
                            var parameters = [String:Any]()
                            if let emailUser = userInformation["email"] as? String {
                                parameters["emailUser"] = emailUser
                                print(emailUser)
                            }
                            if let userBirthday = userInformation["birthday"] as? String {
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "MM/dd/yyyy"
                                if let birthday = dateFormatter.date(from: userBirthday) {
                                    dateFormatter.dateFormat = "yyyy-MM-dd"
                                    let stringBirthday = dateFormatter.string(from: birthday)
                                    parameters["birthday"] = stringBirthday
                                }
                            }
                            
                            if let gender = userInformation["gender"] as? String {
                                print(gender)
                                if gender == "male" {
                                    parameters["gender"] = 1
                                } else {
                                    parameters["gender"] = 0
                                }
                            }
                            if let name = userInformation["name"] as? String {
                                parameters["name"] = name
                            }
                            if let id = userInformation["id"] as? String {
                                parameters["id"] = id
                            }
                            if let pictureDic = userInformation["picture"] as? [String:Any] {
                                if let data = pictureDic["data"] as? [String:Any] {
                                    if (data["url"] as? String) != nil {
                                        parameters["avatar"] = "https://graph.facebook.com/v3.2/\(parameters["id"] as! String)/picture?height=100"
                                    }
                                }
                            }
                            guard let emailUser = parameters["emailUser"] else {return}
                            guard let name = parameters["name"] else {return}
                            guard let avatar = parameters["avatar"] else {return}
                            
                            let param = ["emailUser" : emailUser, "name" : name, "avatar" : avatar, "gender" : 0, "birthday" : 0]
                            print(param)
                            self.currentRequest = User.doLoginSocial(parameters:param, success: { (userDoctor) in
                                self.hideProgress(activityIndicator)
                                if userDoctor.id == 0 {
                                    let alertViewController = UIAlertController(title: "Thông báo", message: "Số điện thoại không tồn tại!", preferredStyle: .alert)
                                    let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                                    alertViewController.addAction(noAction)
                                    self.present(alertViewController, animated: true, completion: nil)
                                } else {
                                    autoreleasepool {
                                        do {
                                            let realm = try Realm()
                                            let user = User()
                                            user.id = userDoctor.id
                                            if let doctorID = userDoctor.userInfo?.doctorID {
                                                user.doctorID =  doctorID
                                            }
                                            user.userDoctor = userDoctor
                                            user.loginType = .Facebook
                                            try realm.write {
                                                realm.add(user, update: true)
                                            }
                                            if self.isCheck!{
                                                NotificationCenter.default.post(name: Constant.NotificationMessage.loginSuccess2, object: nil)
                                                self.navigationController?.dismiss(animated: true, completion: nil)
                                            }else{
                                                NotificationCenter.default.post(name: Constant.NotificationMessage.loginSuccess, object: nil)
                                                self.navigationController?.dismiss(animated: true, completion: nil)
                                                self.backCurrentView()
                                            }
                                        } catch let error {
                                            let alertViewController = UIAlertController(title: "Thông báo", message: "Không thể lưu thông tin đăng nhập:" + error.localizedDescription, preferredStyle: .alert)
                                            let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                                            alertViewController.addAction(noAction)
                                            self.present(alertViewController, animated: true, completion: nil)
                                        }
                                    }
                                }
                            }, fail: { (error, response) in
                                self.hideProgress(activityIndicator)
                                self.handleNetworkError(error: error, responseData: response.data)
                                self.loginButton.isEnabled = true
                            })
                        } else {
                            self.hideProgress(activityIndicator)
                        }
                    }
                })
            } else {
                self.hideProgress(activityIndicator)
            }
        }
    }
    
    @IBAction func touchGoogleSignInButton(_ sender: Any) {
        googleProgress = showProgress()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().signIn()
    }
    
    //GoogleSignIn Delegate
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        viewController.modalPresentationStyle = UIModalPresentationStyle.currentContext
        present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        if let err = error {
            print("Failed to log into Google: ", err)
            return
        }
        
        print("Successfully logged into Google", user)
        
        //Login to webservie
        
        var parameters = [String:Any]()
        parameters["emailUser"] = user.profile.email
        parameters["name"] = user.profile.name
        if user.profile.hasImage {
            parameters["avatar"] = user.profile.imageURL(withDimension: 200).absoluteString
        }
        
        parameters["gender"] = ""
        parameters["birthday"] = ""
        let activityIndicator = showProgress()
        self.currentRequest?.cancel()
        self.currentRequest = User.doLoginSocial(parameters:parameters, success: { (userDoctor) in
            self.hideProgress(activityIndicator)
            if userDoctor.id == 0 {
                let alertViewController = UIAlertController(title: "Thông báo", message: "Số điện thoại không tồn tại", preferredStyle: .alert)
                let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                alertViewController.addAction(noAction)
                self.present(alertViewController, animated: true, completion: nil)
                GIDSignIn.sharedInstance().signOut()
            } else {
                autoreleasepool {
                    do {
                        let realm = try Realm()
                        let user = User()
                        user.id = userDoctor.id
                        if let doctorID = userDoctor.userInfo?.doctorID {
                            user.doctorID =  doctorID
                        }
                        user.userDoctor = userDoctor
                        user.loginType = .Google
                        try realm.write {
                            realm.add(user, update: true)
                        }
                        if self.isCheck!{
                            NotificationCenter.default.post(name: Constant.NotificationMessage.loginSuccess2, object: nil)
                            self.navigationController?.dismiss(animated: true, completion: nil)
                        }else{
                            NotificationCenter.default.post(name: Constant.NotificationMessage.loginSuccess, object: nil)
                            self.navigationController?.dismiss(animated: true, completion: nil)
                            self.backCurrentView()
                        }
                    } catch let error {
                        let alertViewController = UIAlertController(title: "Thông báo", message: "Không thể lưu thông tin đăng nhập:" + error.localizedDescription, preferredStyle: .alert)
                        let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                        alertViewController.addAction(noAction)
                        self.present(alertViewController, animated: true, completion: nil)
                    }
                }
            }
        }, fail: { (error, response) in
            self.hideProgress(activityIndicator)
            self.handleNetworkError(error: error, responseData: response.data)
            GIDSignIn.sharedInstance().signOut()
        })
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        hideProgress(googleProgress)
        googleProgress = nil
    }
    
    @IBAction func touchBackButton(_ sender: Any) {
        if let navigationController = self.navigationController {
            currentRequest?.cancel()
            navigationController.dismiss(animated: true, completion: nil)
        }
        self.backCurrentView()
    }
    
    override func backCurrentView(){
        self.myDelegate?.backCurrentView!()
    }
    
}
