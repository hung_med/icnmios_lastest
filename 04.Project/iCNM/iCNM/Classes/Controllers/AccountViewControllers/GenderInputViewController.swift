//
//  GenderInputViewController.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class GenderInputViewController: UIViewController {

    var userRegister:UserRegister!
    @IBOutlet weak var femaleRadioButton: ISRadioButton!
    @IBOutlet weak var maleRadioButton: ISRadioButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem?.title = " "
        maleRadioButton.isSelected = true
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Thoát", style: .plain, handler: { (button) in
            let alertViewController = UIAlertController(title: "Thông báo", message: "Hủy bỏ quá trình đăng ký", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Đồng ý", style: UIAlertActionStyle.default, handler: { (action) in
                if let array = self.navigationController?.viewControllers, array.count > 1 {
                    self.navigationController?.popToViewController(array[1], animated: true)
                }
            })
            let noAction = UIAlertAction(title: "Huỷ bỏ", style: UIAlertActionStyle.default, handler:nil)
            alertViewController.addAction(noAction)
            alertViewController.addAction(yesAction)
            self.present(alertViewController, animated: true, completion: nil)
        })
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Cập nhật Giới tính"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchGenderButton(_ sender: ISRadioButton) {
        if sender == maleRadioButton {
            maleRadioButton.iconColor = UIColor(hex: "1D6EDC")
            femaleRadioButton.iconColor = .darkGray
        } else {
            femaleRadioButton.iconColor = UIColor(hex: "1D6EDC")
            maleRadioButton.iconColor = .darkGray
        }
    }

    @IBAction func touchContinueButton(_ sender: Any) {
        if maleRadioButton.isSelected {
            userRegister.gender = "M"
        } else {
            userRegister.gender = "F"
        }
        perform(segue: StoryboardSegue.LoginRegister.showCreatePasswordViewController, sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegue.LoginRegister.showCreatePasswordViewController.rawValue {
            let destinationVC = segue.destination as! CreatePasswordViewController
            destinationVC.userRegister = self.userRegister
        }
    }

}
