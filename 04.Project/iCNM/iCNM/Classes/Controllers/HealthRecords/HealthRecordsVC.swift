//
//  ListLookUpResultVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
class HealthRecordsVC: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate, UIPopoverPresentationControllerDelegate, DetailHealthRecordsVCDelegate, AddChangeMaBNDelegate, UIViewControllerTransitioningDelegate {

    var healthRecords: [HealthRecords]?
    var isCheckNoti:Bool = false
   // var filteredNames:[LookUpResult]?
    var currentMaBS:String?
    var currentOrganizeID:String?
    var lblWarning:UILabel?
    @IBOutlet weak var btnCreateHSSK: UIButton!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var myTableView: UITableView!
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    //@IBOutlet weak var rightTopButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        // Do any additional setup after lfoading the view.
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        myTableView.delegate = self
        myTableView.dataSource = self
        
        myTableView.estimatedRowHeight = 70
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        title = "Hồ sơ sức khoẻ"
        btnCreateHSSK.layer.cornerRadius = 6.0
        btnCreateHSSK.layer.masksToBounds = true
        btnCreateHSSK.isHidden = true
        lblMessage.isHidden = true
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(HealthRecordsVC.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        let filterBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(HealthRecordsVC.filterBarButtonHandler))
        self.navigationItem.setRightBarButtonItems([filterBarButton], animated: true)
        
        let nib = UINib(nibName: "CustomHealthRecordsCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        
        self.checkTrackingFeature()
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createHSSKHandler(_ sender: Any) {
        if currentUser != nil {
            let detailHealthRecordsVC = DetailHealthRecordsVC(nibName: "DetailHealthRecordsVC", bundle: nil)
            detailHealthRecordsVC.isEdit = false
            detailHealthRecordsVC.delegate = self
            self.navigationController?.pushViewController(detailHealthRecordsVC, animated: true)
        }else{
            confirmLogin(myView: self)
        }
    }
    
    func filterBarButtonHandler(button: UIBarButtonItem)
    {
        let detailHealthRecordsVC = DetailHealthRecordsVC(nibName: "DetailHealthRecordsVC", bundle: nil)
        detailHealthRecordsVC.isEdit = false
        detailHealthRecordsVC.delegate = self
        var dependPerson = [String]()
        for health in healthRecords! {
            dependPerson.append((health.userMedicalProfile?.medicalFor)!)
        }
        detailHealthRecordsVC.dependPerson = dependPerson
        self.navigationController?.pushViewController(detailHealthRecordsVC, animated: true)
    }
    
    func reloadListHSSK(userID:Int){
        self.getMedicalProfileByUserID(userID:userID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Hồ sơ sức khoẻ"
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Hồ sơ sức khoẻ")
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    func checkTrackingFeature(){
        if currentUser != nil{
            let userDefaults = UserDefaults.standard
            let token = userDefaults.object(forKey: "token") as! String
            TrackingFeature().checkTrackingFeature(fcm:token, featureName:Constant.HEALTH_RECORDS, categoryID: 0, success: {(result) in
                if result != nil{
                    print("flow track done", Constant.HEALTH_RECORDS)
                }else{
                    print("flow track fail", Constant.HEALTH_RECORDS)
                }
            }, fail: { (error, response) in
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }else{
            return
        }
    }
    
    func getMedicalProfileByUserID(userID:Int){
        self.healthRecords = [HealthRecords]()
        let progress = self.showProgress()
        HealthRecords.getMedicalProfileByUserID(userID: userID, success: { (data) in
            if data.count > 0 {
                self.healthRecords?.append(contentsOf: data)
                self.myTableView.reloadData()
                self.btnCreateHSSK.isHidden = true
                self.lblMessage.isHidden = true
                self.hideProgress(progress)
                if self.isCheckNoti{
                    let healthRecordObj = self.healthRecords![0]
                    self.showUpdateMaBN(healthRecordsObj: healthRecordObj)
                }
            } else {
                self.hideProgress(progress)
                self.btnCreateHSSK.isHidden = false
                self.lblMessage.isHidden = false
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (self.healthRecords?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomHealthRecordsCell
        cell.backgroundColor = .clear
        cell.groupView.layer.cornerRadius = 4
        cell.groupView.layer.borderWidth = 1
        cell.groupView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell.groupView.layer.masksToBounds = true
        cell.groupView.layer.shadowOffset = CGSize(width: 0, height:5)
        cell.groupView.layer.shadowColor = UIColor.groupTableViewBackground.cgColor
        cell.groupView.layer.shadowOpacity = 0.23
        cell.groupView.layer.shadowRadius = 2
        cell.profileImage.layer.borderColor = UIColor(hexColor: 0x1D6EDC, alpha: 1.0).cgColor
        cell.profileImage.layer.borderWidth = 2.0
        cell.profileImage.layer.cornerRadius = 25
        cell.profileImage.layer.masksToBounds = true
        cell.btnMore.tag = indexPath.row
        cell.btnMore.addTarget(self, action: #selector(HealthRecordsVC.handleMore), for: .touchUpInside)
        let healthRecordsObj = self.healthRecords?[indexPath.row]
        let medicalProfile = healthRecordsObj?.medicalProfile
        let userMedicalProfile = healthRecordsObj?.userMedicalProfile
        cell.lblPID.text = "Mã BN"
        if let patientName = medicalProfile?.patientName{
            cell.lblUserName.text = patientName
        }
        
        if let pID = medicalProfile?.pID{
            if pID == ""{
                cell.lblPIDValue.text = "chưa cập nhật"
            }else{
                cell.lblPIDValue.text = pID
            }
        }
        
        
        if let medicalFor = userMedicalProfile?.medicalFor{
            cell.lblDependentPerson.layer.cornerRadius = 4
            cell.lblDependentPerson.layer.masksToBounds = true
            cell.lblDependentPerson.text = medicalFor
            list.append(medicalFor)
        }
        print(list)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        if let dateCreate = userMedicalProfile?.dateCreate{
            let strDate = formatter.string(from: dateCreate)
            cell.lblCreateDate.text = "Ngày tạo    \(strDate)"
        }
       
        if let imgUrl = medicalProfile?.imgUrl{
            if !imgUrl.isEmpty{
                let userID:Int = (userMedicalProfile?.userID)!
                let userMedicalProfileID:Int = (userMedicalProfile?.medicalProfileID)!
                let urlString = API.baseURLImage + "\(API.iCNMImage)" + "\(userID)" + "/MedicalProfiles/" + "\(userMedicalProfileID)" + "/\(imgUrl)"
                cell.profileImage.contentMode = .scaleAspectFill
                cell.profileImage.loadImageUsingUrlString(urlString: urlString)
            }else{
                cell.profileImage.image = UIImage(named: "avatar_default")
            }
        }
        return cell
    }
    var list = [String]()
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let healthRecordsObj = self.healthRecords?[indexPath.row]
        if let healthRecords = healthRecordsObj{
            let detailHealthRecordsVC = DetailHealthRecordsVC(nibName: "DetailHealthRecordsVC", bundle: nil)
            if let factoryID = healthRecordsObj?.medicalProfile?.medicalHealthFactoryID{
                detailHealthRecordsVC.isEdit = true
                detailHealthRecordsVC.delegate = self
                detailHealthRecordsVC.healthRecords = healthRecords
                detailHealthRecordsVC.getMedicalHealthFactory(id: factoryID)
            }
            
            self.navigationController?.pushViewController(detailHealthRecordsVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100.0;//Choose your custom row height
    }
    
    func handleMore(sender: UIButton){
        
        var optionMenu:UIAlertController? = nil
        if IS_IPAD{
            optionMenu = UIAlertController(title: nil, message: "Thông báo", preferredStyle: .alert)
        }else{
            optionMenu = UIAlertController(title: nil, message: "Thông báo", preferredStyle: .alert)
        }
        
        optionMenu?.addAction(UIAlertAction(title: "Xoá", style: .default, handler: { _ in
            let alert = UIAlertController(title: "Thông báo", message: "Bạn có chắc chắn muốn xoá bản ghi này không?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
                let index = sender.tag
                //call API
                let healthRecordsObj = self.healthRecords?[index]
                self.addEditUserMedicalProfiles(currentIndex: index, userMedicalProfileID:(healthRecordsObj?.userMedicalProfile?.userMedicalProfileID)!, userID:(healthRecordsObj?.userMedicalProfile?.userID)!, active:false)
            }))
            
            alert.addAction(UIAlertAction(title: "Huỷ", style: UIAlertActionStyle.default, handler: {    (action:UIAlertAction!) in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }))
        optionMenu?.addAction(UIAlertAction(title: "Xem kết quả khám", style: .default, handler: { _ in
            let index = sender.tag
            let healthRecordsObj = self.healthRecords?[index]
            if let healthRecords = healthRecordsObj{
                let medicalProfile = healthRecords.medicalProfile
                if let pID = medicalProfile?.pID, let organizeID = medicalProfile?.organizeID{
                    if !pID.isEmpty{
                        let listLookUpResultVC = ListLookUpResultVC(nibName: "ListLookUpResultVC", bundle: nil)
                        //listLookUpResultVC.delegate = self
                        listLookUpResultVC.getListLookUpResultByPID(pid: pID, organizeID:String(organizeID))
                        self.navigationController?.pushViewController(listLookUpResultVC, animated: true)
                    }else{
                        let alert = UIAlertController(title: "Thông báo", message: "Hồ sơ này không có kết quả khám. Vui lòng nhập mã bệnh nhân để xem kết quả khám", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Nhập mã", style: UIAlertActionStyle.default, handler: { alertAction in
                            self.showUpdateMaBN(healthRecordsObj:healthRecordsObj!)
                        }))
                        alert.addAction(UIAlertAction(title: "Bỏ qua", style: UIAlertActionStyle.cancel, handler: { alertAction in
                            
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    let alert = UIAlertController(title: "Thông báo", message: "Hồ sơ này không có kết quả khám. Vui lòng nhập mã bệnh nhân để xem kết quả khám", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Nhập mã", style: UIAlertActionStyle.default, handler: { alertAction in
                        self.showUpdateMaBN(healthRecordsObj:healthRecordsObj!)
                    }))
                    alert.addAction(UIAlertAction(title: "Bỏ qua", style: UIAlertActionStyle.cancel, handler: { alertAction in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }))
        
        optionMenu?.addAction(UIAlertAction(title: "Huỷ", style: UIAlertActionStyle.cancel, handler: {(action:UIAlertAction!) in
            
        }))
        
        optionMenu?.popoverPresentationController?.sourceView = self.view
        self.present(optionMenu!, animated: true, completion: nil)
    }

    func showUpdateMaBN(healthRecordsObj:HealthRecords){
        let addChangeMaBN = AddChangeMaBN(nibName: "AddChangeMaBN", bundle: nil)
        addChangeMaBN.delegate = self
        if let medicalProfile = healthRecordsObj.medicalProfile{
            addChangeMaBN.medicalProfile = medicalProfile
        }
        addChangeMaBN.maBN = ""
        addChangeMaBN.modalPresentationStyle = .custom
        addChangeMaBN.transitioningDelegate = self
        addChangeMaBN.view.frame = CGRect(x: screenSizeWidth/2-162, y: screenSizeHeight/2-194, width: 324, height: 350)
        self.present(addChangeMaBN, animated: true, completion: nil)
    }
    
    func resultChangeMaBN(maBN:String){
        showAlertView(title: "Cập nhật mã bệnh nhân thành công!", view: self)
        if let user = self.currentUser{
            self.getMedicalProfileByUserID(userID:user.id)
        }
    }
    
    func addEditUserMedicalProfiles(currentIndex:Int, userMedicalProfileID:Int, userID:Int, active:Bool){
        let progress = self.showProgress()
        let healthRecords = HealthRecords()
        healthRecords.addEditUserMedicalProfiles(userMedicalProfileID:userMedicalProfileID, userID:userID, active:active, success: {  (result) in
            self.hideProgress(progress)
            if result == nil{
                return
            }else{
                self.healthRecords?.remove(at: currentIndex)
                self.myTableView.reloadData()
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func didPresentSearchController(searchController: UISearchController) {
        searchController.searchBar.becomeFirstResponder()
    }

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
