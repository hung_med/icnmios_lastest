//
//  DetailHealthRecordsVC.swift
//  iCNM
//
//  Created by Mac osx on 8/22/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import MobileCoreServices
import FontAwesome_swift
import Alamofire
import Toaster

protocol DetailHealthRecordsVCDelegate {
    func reloadListHSSK(userID:Int)
    
}
class DetailHealthRecordsVC: BaseViewController, UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, CropViewControllerDelegate, CustomMedicalBodyHeaderDelegate, AddMuitiemVCDelegate, UIPopoverPresentationControllerDelegate, UIViewControllerTransitioningDelegate, FWComboBoxDelegate, ThutucHCCustomCellDelegate, TinhTrangLucSinhCustomCellDelegate, SuckhoeCanhanCustomCellDelegate, TiensubenhtatCustomCellDelegate, KhuyettatCustomCellDelegate, TiensuphauthuatCustomCellDelegate, TiensugiadinhCustomCellDelegate, SinhsanKHHGTCustomCellDelegate, MuitiemCustomCellDelegate, AddChangeMaBNDelegate, LamsangVCDelegate {

    @IBOutlet var createForLabel: UILabel!
    @IBOutlet var updateImageLabel: UILabel!
    @IBOutlet var profileView: UIView!
    @IBOutlet var changeBtn: UIButton!
    @IBOutlet var indexBMIBtn: UIButton!
    @IBOutlet var historyView: UIView!
    @IBOutlet var historyImage:UIImageView!
    @IBOutlet var historyLabel: UILabel!
    @IBOutlet var statusText: UILabel!
    @IBOutlet weak var medicalForComboBox: FWComboBox!
    @IBOutlet weak var profileImage: PASImageView!
    @IBOutlet weak var myTableView: UITableView!
    var sectionsData: [SectionGroup]?
    var sectionGroups:[String]?
    var isEdit:Bool?=false
    var isActive:Bool?=false
    var storedOffsets = [Int: CGFloat]()
    var healthRecords:HealthRecords?
    var medicalVaccin1:[MedicalVaccin]?
    var medicalVaccin2:[MedicalVaccin]?
    var medicalVaccin3:[MedicalVaccin]?
    var medicalProfiles:[MedicalCilinnical]?
    var patientName:String?
    var medicalHealthFactories:[MedicalHealthFactory]?
    var medicalProfileObj:MedicalProfile?
    var medicalHealFactoryObj:MedicalHealthFactory?
    var medicalHealFactory = MedicalHealthFactory()
    var delegate:DetailHealthRecordsVCDelegate?
    var dependPerson = [String]()
   // var chosenImage:PASImageView?=nil
    var maBN:String?
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    var listMedicalFor:[String]?
    var currentMedicalForIndex:Int? = 0
    var currentRequest:Request?
    var currentVacxinIdx:Int? = 0
    var imageView = UIImageView()
    private var image: UIImage?
    private var croppingStyle = CropViewCroppingStyle.default
    private var croppedRect = CGRect.zero
    private var croppedAngle = 0
    let picker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        if dependPerson.contains("Tôi") {
            listMedicalFor = ["Chồng", "Vợ", "Bố", "Mẹ", "Con", "Ông", "Bà", "Anh", "Chị", "Em"]
        } else {
            listMedicalFor = ["Tôi", "Chồng", "Vợ", "Bố", "Mẹ", "Con", "Ông", "Bà", "Anh", "Chị", "Em"]
        }
        medicalForComboBox.delegate = self
        picker.delegate = self
        
        self.setupSwipAction()
        medicalForComboBox.defaultTitle = (listMedicalFor?[currentMedicalForIndex!])!
        populateListMedicalForComboBox()
        sectionsData = [SectionGroup]()
        sectionGroups = ["Thông tin hành chính", "Tình trạng lúc sinh", "Yếu tố nguy cơ đến sức khoẻ cá nhân", "Tiền sử bệnh tật, dị ứng", "Khuyết tật", "Tiền sử phẫu thuật", "Tiền sử gia đình", "Sinh sản và kế hoạch hoá gia đình", "Vấn đề khác liên quan đến sức khoẻ", "Tiêm chủng trẻ em", "Tiêm chủng ngoài chương trình TCMR", "Tiêm chủng uốn ván(phụ nữ có thai)", "Khám lâm sàng và cận lâm sàng"]
        for s in sectionGroups!
        {
            self.sectionsData?.append(SectionGroup(name:s, items: [
                ItemInSection(name: "")]))
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(HealthRecordsVC.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        myTableView.delegate = self
        myTableView.dataSource = self
        searchBar.isHidden = true
        self.navigationItem.titleView = nil
        myTableView.estimatedRowHeight = 70
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.medicalVaccin1 = [MedicalVaccin]()
        self.medicalVaccin2 = [MedicalVaccin]()
        self.medicalVaccin3 = [MedicalVaccin]()
        self.medicalProfiles = [MedicalCilinnical]()
        self.medicalProfileObj = nil
        self.medicalHealFactoryObj = medicalHealFactory
        self.historyImage?.image = UIImage.init(named: "ic_history")
        self.updateHistoryView(isEdit: isEdit!)
        // Do any additional setup after loading the view.
        let nib_header = UINib(nibName: "CustomMedicalBodyHeader", bundle: nil)
        myTableView.register(nib_header, forHeaderFooterViewReuseIdentifier: "headerCell")
        
        let nib = UINib(nibName: "ThutucHCCustomCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        
        let nib1 = UINib(nibName: "TinhTrangLucSinhCustomCell", bundle: nil)
        myTableView.register(nib1, forCellReuseIdentifier: "Cell1")
        
        let nib2 = UINib(nibName: "SuckhoeCanhanCustomCell", bundle: nil)
        myTableView.register(nib2, forCellReuseIdentifier: "Cell2")
        
        let nib3 = UINib(nibName: "TiensubenhtatCustomCell", bundle: nil)
        myTableView.register(nib3, forCellReuseIdentifier: "Cell3")
        
        let nib4 = UINib(nibName: "KhuyettatCustomCell", bundle: nil)
        myTableView.register(nib4, forCellReuseIdentifier: "Cell4")
        
        let nib5 = UINib(nibName: "TiensuphauthuatCustomCell", bundle: nil)
        myTableView.register(nib5, forCellReuseIdentifier: "Cell5")
        
        let nib6 = UINib(nibName: "TiensugiadinhCustomCell", bundle: nil)
        myTableView.register(nib6, forCellReuseIdentifier: "Cell6")
        
        let nib7 = UINib(nibName: "SinhsanKHHGTCustomCell", bundle: nil)
        myTableView.register(nib7, forCellReuseIdentifier: "Cell7")

        let nib8 = UINib(nibName: "MuitiemCustomCell", bundle: nil)
        myTableView.register(nib8, forCellReuseIdentifier: "Cell8")
        
        let nib9 = UINib(nibName: "TiemchungTreemCustomCell", bundle: nil)
        myTableView.register(nib9, forCellReuseIdentifier: "Cell9")
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectImagetoPickerView(tapGestureRecognizer:)))
        updateImageLabel.isUserInteractionEnabled = true
        self.updateImageLabel.addGestureRecognizer(tapGestureRecognizer)
        self.medicalHealthFactories = [MedicalHealthFactory]()
        if healthRecords != nil{
            let medicalProfile = healthRecords?.medicalProfile
            let userMedicalProfile = healthRecords?.userMedicalProfile
            if let imgUrl = medicalProfile?.imgUrl{
                let userID:Int = (userMedicalProfile?.userID)!
                let userMedicalProfileID:Int = (userMedicalProfile?.medicalProfileID)!
                let urlString = API.baseURLImage + "\(API.iCNMImage)" + "\(userID)" + "/MedicalProfiles/" + "\(userMedicalProfileID)" + "/\(imgUrl)"
                
                self.profileImage.contentMode = .scaleAspectFill
                let avatarURL = URL(string: urlString)
                self.profileImage.imageURL(URL: avatarURL!)
                //self.chosenImage = self.profileImage
            }
        }else{
            self.profileImage.reset()
            //self.chosenImage = nil
        }
        self.searchBar.isHidden = true
        
        changeBtn?.titleLabel?.font = UIFont.fontAwesome(ofSize: 14.0)
        let changeLabel = " " + String.fontAwesomeIcon(name: .edit) + " Thay đổi "
        changeBtn.setTitle(changeLabel, for: .normal)
        indexBMIBtn?.titleLabel?.font = UIFont.fontAwesome(ofSize: 14.0)
        if let weight = self.medicalProfileObj?.weight, let height = self.medicalProfileObj?.height2{
            if weight != 0 && height != 0{
                self.calculatorBMI(weight: Float(weight), height: Float(height))
            }else{
                indexBMIBtn.setTitle(" Chỉ số BMI ", for: .normal)
                return
            }
        }else{
            indexBMIBtn.setTitle(" Chỉ số BMI ", for: .normal)
            return
        }
        
//        NotificationCenter.default.addObserver(self, selector: #selector(DetailHealthRecordsVC.keyboardWillShow(notification:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(DetailHealthRecordsVC.keyboardWillHide(notification:)), name: Notification.Name.UIKeyboardDidHide, object: nil)
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            self.myTableView.contentInset = UIEdgeInsetsMake(0, 0, keyboardHeight, 0)
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.2, animations: {
            // For some reason adding inset in keyboardWillShow is animated by itself but removing is not, that's why we have to use animateWithDuration here
            self.myTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        })
    }
    
    func updateHistoryView(isEdit:Bool){
        if isEdit{
            if let medicalProfile = healthRecords?.medicalProfile{
                title = medicalProfile.patientName
            }
            //medicalForComboBox.isUserInteractionEnabled = false
            self.updateStatusProfile()
            let button: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 24, height: screenSizeHeight-40))
            button.setImage(UIImage(named: "EditProfileIcon"), for: UIControlState.normal)
            button.addTarget(self, action: #selector(DetailHealthRecordsVC.editBarButtonHandler), for: UIControlEvents.touchUpInside)
            
            let editBarButton = UIBarButtonItem(customView: button)
            self.navigationItem.setRightBarButtonItems([editBarButton], animated: true)
            
        }else{
            if healthRecords != nil{
                title = "Chỉnh sửa hồ sơ"
            }else{
                title = "Tạo hồ sơ mới"
            }
            
            self.updateStatusProfile()
            let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(DetailHealthRecordsVC.doneBarButtonHandler))
            self.navigationItem.setRightBarButtonItems([doneBarButton], animated: true)
        }
        
        
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        
        self.profileImage.layer.cornerRadius = 30
        self.profileImage.layer.masksToBounds = true
    }
    
    func updateStatusProfile(){
        if healthRecords != nil{
            if !isActive!{
                self.medicalForComboBox.isHidden = true
                self.medicalForComboBox.isUserInteractionEnabled = false
                self.profileImage.isUserInteractionEnabled = false
                self.medicalProfileObj = healthRecords?.medicalProfile
                title = self.medicalProfileObj?.patientName
                if let pID = medicalProfileObj?.pID{
                        changeBtn.isHidden = false
                        indexBMIBtn.isHidden = false
                        historyView.isHidden = false
                        updateImageLabel.isHidden = true
                        historyView.layer.borderWidth = 1
                        historyView.layer.borderColor = UIColor.lightGray.cgColor
                        historyView.layer.cornerRadius = 2
                        historyView.layer.masksToBounds = true
                        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showHistoryTracuuKQ(tapGestureRecognizer:)))
                        self.historyImage.isUserInteractionEnabled = true
                        self.historyImage.addGestureRecognizer(tapGestureRecognizer)
                        self.maBN = pID
                        statusText.text = "Mã BN: \(pID)"
                        if IS_IPHONE_5{
                            statusText.font = UIFont.boldSystemFont(ofSize: 12.0)
                        }
                }
            }else{
                self.medicalForComboBox.isUserInteractionEnabled = true
                self.profileImage.isUserInteractionEnabled = true
                createForLabel.isHidden = false
                self.medicalForComboBox.isHidden = false
                
                self.medicalProfileObj = healthRecords?.medicalProfile
                if let pID = medicalProfileObj?.pID{
                    statusText.text = "Cập nhật mã bệnh nhân"
                        changeBtn.isHidden = false
                        indexBMIBtn.isHidden = false
                        historyView.isHidden = false
                        updateImageLabel?.font = UIFont.fontAwesome(ofSize: 14.0)
                        let title = String.fontAwesomeIcon(name: .plus) + "  Cập nhật ảnh"
                        updateImageLabel.text =  title
                        updateImageLabel.textColor = UIColor.darkGray
                        updateImageLabel.isHidden = false
                        historyView.layer.borderWidth = 1
                        historyView.layer.borderColor = UIColor.lightGray.cgColor
                        historyView.layer.cornerRadius = 2
                        historyView.layer.masksToBounds = true
                        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showHistoryTracuuKQ(tapGestureRecognizer:)))
                        self.historyImage.isUserInteractionEnabled = true
                        self.historyImage.addGestureRecognizer(tapGestureRecognizer)
                        self.maBN = pID
                        statusText.text = "Mã BN: \(pID)"
                       // statusText.textColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
                   // }
                }
            }
            
        }else{
            self.medicalForComboBox.isUserInteractionEnabled = true
            self.profileImage.isUserInteractionEnabled = true
            //statusText.text = "Cập nhật mã bệnh nhân"
            //statusText.textColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
            statusText.isHidden = true
            changeBtn.isHidden = true
            indexBMIBtn.isHidden = true
            historyView.isHidden = true
            createForLabel.isHidden = false
            createForLabel.textAlignment = NSTextAlignment.left
            updateImageLabel.isHidden = false
            updateImageLabel?.font = UIFont.fontAwesome(ofSize: 14.0)
            let title = String.fontAwesomeIcon(name: .plus) + "  Cập nhật ảnh"
            updateImageLabel.text =  title
            updateImageLabel.textColor = UIColor.darkGray
        }
    }
    
    func populateListMedicalForComboBox() {
        if let medicalFor = listMedicalFor {
            medicalForComboBox.dataSource = medicalFor.map({ (medicalFor) -> String in
                return medicalFor
            })
        } else {
            medicalForComboBox.selectRow(at: nil)
            medicalForComboBox.dataSource = [String]()
        }
    }
    
    func fwComboBox(comboBox: FWComboBox, didSelectAtIndex index: Int) {
        currentMedicalForIndex = index
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // get previous view controller and set title to "" or any things You want
        if let viewControllers = self.navigationController?.viewControllers {
            let previousVC: UIViewController? = viewControllers.count >= 2 ? viewControllers[viewControllers.count - 2] : nil;
            previousVC?.title = ""
        }
    }
    
    /*
     * Set up swipe gesture for view
     */
    func setupSwipAction() -> Void {
        let swipeUp = UISwipeGestureRecognizer.init(target: self, action: #selector(self.swipeUp))
        swipeUp.direction = UISwipeGestureRecognizerDirection.up;
        self.myTableView.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer.init(target: self, action: #selector(self.swipeDown))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down;
        self.myTableView.addGestureRecognizer(swipeDown)
    }
    
    // MARK: -- SWIFT LEFT, UP, DOWN
    func swipeUp(gestureRecognizer: UISwipeGestureRecognizer) {
        
        UIView.animate(withDuration: 0.5) {
            //self.constraintTop.constant = 0
            self.myTableView.layoutIfNeeded()
        }
    }
    
    func swipeDown(gestureRecognizer: UISwipeGestureRecognizer) {
        
//        if self.constraintTop.constant == 0 {
//            UIView.animate(withDuration: 0.5) {
//                self.constraintTop.constant = 270
//                self.myTableView.layoutIfNeeded()
//            }
//        } else if self.constraintTop.constant == 270 {
//            let screenHeight = self.view.frame.size.height
//            self.constraintTop.constant = screenHeight
//        }
    }
    
    func editBarButtonHandler(button: UIBarButtonItem)
    {
        //isEdit = false
        isActive = true
        self.profileImage.isUserInteractionEnabled = true
        self.medicalForComboBox.isUserInteractionEnabled = true
        self.updateHistoryView(isEdit:!isActive!)
        self.myTableView.reloadData()
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(DetailHealthRecordsVC.doneBarButtonHandler))
        self.navigationItem.setRightBarButtonItems([doneBarButton], animated: true)
    }
    
    @IBAction func showHistoryTracuuKQ(_ sender: Any) {
        self.showHistoryLookupResult()
    }
    
    func showHistoryLookupResult(){
        let medicalProfile = self.healthRecords?.medicalProfile
        if let pID = medicalProfile?.pID, let organizeID = medicalProfile?.organizeID{
            if !pID.isEmpty{
                let listLookUpResultVC = ListLookUpResultVC(nibName: "ListLookUpResultVC", bundle: nil)
                //listLookUpResultVC.delegate = self
                listLookUpResultVC.medicalProfileObj = self.medicalProfileObj
                listLookUpResultVC.getListLookUpResultByPID(pid: pID, organizeID:String(organizeID))
                self.navigationController?.pushViewController(listLookUpResultVC, animated: true)
            }else{
                let alert = UIAlertController(title: "Thông báo", message: "Hồ sơ này không có kết quả khám. Vui lòng nhập mã bệnh nhân để xem kết quả khám", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Nhập mã", style: UIAlertActionStyle.default, handler: { alertAction in
                    self.showUpdateMaBN()
                }))
                alert.addAction(UIAlertAction(title: "Bỏ qua", style: UIAlertActionStyle.cancel, handler: { alertAction in
                    
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }else{
            let alert = UIAlertController(title: "Thông báo", message: "Hồ sơ này không có kết quả khám. Vui lòng nhập mã bệnh nhân để xem kết quả khám", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Nhập mã", style: UIAlertActionStyle.default, handler: { alertAction in
                self.showUpdateMaBN()
            }))
            alert.addAction(UIAlertAction(title: "Bỏ qua", style: UIAlertActionStyle.cancel, handler: { alertAction in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showHistoryTracuuKQ(tapGestureRecognizer: UITapGestureRecognizer){
        self.showHistoryLookupResult()
    }
    
    func doneBarButtonHandler(button: UIBarButtonItem)
    {
        let patientName = self.medicalProfileObj?.patientName
        if patientName == nil || (patientName?.isEmpty)!{
            showAlertView(title: "Họ tên không được phép bỏ trống", view: self)
            return
        }
        
        let birthDay = self.medicalProfileObj?.birthDay
        if birthDay == nil || (birthDay?.isEmpty)!{
            showAlertView(title: "Ngày sinh không được phép bỏ trống", view: self)
            return
        }
       
        let gender = self.medicalProfileObj?.gender
        if gender == nil || (gender?.isEmpty)!{
            showAlertView(title: "Bạn phải chọn giới tính", view: self)
            return
        }else{
            let medicalHealthFactoryUser = MedicalHealthFactoryUser()
            medicalHealthFactoryUser.medicalHealFactory = self.medicalHealFactoryObj
            medicalHealthFactoryUser.medicalProfile =  self.medicalProfileObj
            medicalHealthFactoryUser.medicalFor = (listMedicalFor?[currentMedicalForIndex!])!
            medicalHealthFactoryUser.medicalVaccinsTreEm = self.medicalVaccin1!
            medicalHealthFactoryUser.medicalVaccinsTCMR = self.medicalVaccin2!
            medicalHealthFactoryUser.medicalVaccinsUonVan = self.medicalVaccin3!
            medicalHealthFactoryUser.medicalCilinicals = self.medicalProfiles!
            if currentUser != nil {
                if let user = currentUser {
                    medicalHealthFactoryUser.userID = user.id
                }
            }

            let progress = self.showProgress()
            medicalHealthFactoryUser.addEditMedicalHealthFactory(success: {(result) in
                if result != nil {
                    // case MedicalVaccin
                    if self.isEdit!{
                        Toast(text: "Cập nhật Hồ sơ sức khoẻ thành công!").show()
                    }else{
                        Toast(text: "Tạo Hồ sơ sức khoẻ thành công!").show()
                    }
                    
                    if let medicalProfileID = medicalHealthFactoryUser.medicalProfile?.medicalProfileID{
                        self.updateMedicalVaccin(medicalProfileID:medicalProfileID)
                    }
                    //case avatar
                    if (self.image != nil){
                        if self.isEdit!{
                            // get medicalProfileID when edit HSSK
                            let medicalProfile = self.healthRecords?.medicalProfile
                            self.updateAvatarHSSK(medicalProfileID:(medicalProfile?.medicalProfileID)!)
                        }else{
                            // get medicalProfileID when add new HSSK
                            self.getMedicalProfileByUserID(userID: medicalHealthFactoryUser.userID)
                        }
                    }else{
                        //only case only TTHC
                        self.navigationController?.popViewController(animated: true)
                        self.delegate?.reloadListHSSK(userID:medicalHealthFactoryUser.userID)
                        self.hideProgress(progress)
                    }
                    
                } else {
                    showAlertView(title: "Không thể tạo mới hồ sơ sức khoẻ!", view: self)
                }
            }, fail: { (error, response) in
                self.hideProgress(progress)
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
    }
    
    func updateMedicalVaccin(medicalProfileID:Int){
        if (self.medicalVaccin1?.count)! > 0 {
            self.addEditMedicalVaccinAndCilinnical(vaccinTypeID:1, medicalVaccin:self.medicalVaccin1, medicalProfileID: medicalProfileID)
        }
        if (self.medicalVaccin2?.count)! > 0 {
            self.addEditMedicalVaccinAndCilinnical(vaccinTypeID:2, medicalVaccin:self.medicalVaccin2, medicalProfileID: medicalProfileID)
        }
        if (self.medicalVaccin3?.count)! > 0 {
            self.addEditMedicalVaccinAndCilinnical(vaccinTypeID:3, medicalVaccin:self.medicalVaccin3, medicalProfileID: medicalProfileID)
        }
        if (self.medicalProfiles?.count)! > 0 {
            self.addEditMedicalCilinnical(medicalCilinnical:self.medicalProfiles, medicalProfileID: medicalProfileID)
        }
    }
    
    func addEditMedicalVaccinAndCilinnical(vaccinTypeID:Int, medicalVaccin:[MedicalVaccin]?, medicalProfileID:Int){
        let progress = self.showProgress()
        for medicalVaccinObj in medicalVaccin! {
            MedicalVaccin().addEditMedicalVaccin(vaccinTypeID:vaccinTypeID, medicalVaccin:medicalVaccinObj, medicalProfileID:medicalProfileID, success: {(result) in
                if result != nil{
                    self.hideProgress(progress)
                    //Toast(text: "Tạo mũi tiêm thành công!").show()
                    //upload success
                }else{
                    self.hideProgress(progress)
                    //Toast(text: "Tạo mũi thất bại!").show()
                    //upload fail
                }
            }, fail: { (error, response) in
                self.hideProgress(progress)
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
    }
    
    func addEditMedicalCilinnical(medicalCilinnical:[MedicalCilinnical]?, medicalProfileID:Int){
        let progress = self.showProgress()
        for medicalCilinnicalObj in medicalCilinnical! {
            MedicalCilinnical().addEditMedicalCilinnical(medicalCilinnical:medicalCilinnicalObj, medicalProfileID:medicalProfileID, success: {(result) in
                if result != nil{
                    self.hideProgress(progress)
                    Toast(text: "Tạo lần khám thành công!").show()
                    //upload success
                }else{
                    self.hideProgress(progress)
                    Toast(text: "Tạo lần khám thất bại!").show()
                    //upload fail
                }
            }, fail: { (error, response) in
                self.hideProgress(progress)
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
    }
    
    func getMedicalProfileByUserID(userID:Int){
        let progress = self.showProgress()
        HealthRecords.getMedicalProfileByUserID(userID: userID, success: { (data) in
            if data.count > 0 {
                if let medicalProfile = data[data.count - 1].medicalProfile{
                    self.updateAvatarHSSK(medicalProfileID:medicalProfile.medicalProfileID)
                }
                self.hideProgress(progress)
            } else {
                self.hideProgress(progress)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews(){
        super.viewDidLayoutSubviews()
        self.layoutImageView()
        let frame = CGRect(x:0, y:self.profileView.frame.size.height, width: screenSizeWidth, height:screenSizeHeight)
        self.myTableView.frame = frame
        self.myTableView.contentInset = UIEdgeInsetsMake(0, 0, 240, 0)
    }
    
    func resultMedicalProfile(medicalProfile:MedicalProfile){
        self.medicalProfileObj = medicalProfile
    }
    
    func resultGender(medicalProfile:MedicalProfile){
        self.medicalProfileObj = medicalProfile
    }
    
    func resultMedicalHealthFactory(medicalHealthFactory:MedicalHealthFactory){
        self.medicalHealFactoryObj = medicalHealthFactory
    }
     
    func getMedicalHealthFactory(id:Int){
        let progress = self.showProgress()
        MedicalHealthFactory().getMedicalHealthFactory(factoryID: id, success: { (data) in
            if let medicalHealthFactoriesObj = data {
                self.medicalHealthFactories?.append(medicalHealthFactoriesObj)
                self.myTableView.reloadData()
                self.hideProgress(progress)
                
                self.medicalProfileObj = self.healthRecords?.medicalProfile
                // get vacxin Treem
                if let medicalProfileID = self.medicalProfileObj?.medicalProfileID{
                    //get vacxin treem
                    self.getAllVaccin(typeID:1, factoryID: medicalProfileID)
                    //get vacxin TCMR
                    self.getAllVaccin(typeID:2, factoryID: medicalProfileID)
                    //get vacxin Uon van
                    self.getAllVaccin(typeID:3, factoryID: medicalProfileID)
                }
                
                if let medicalProfileID = self.medicalProfileObj?.medicalProfileID{
                    //khám ls
                    self.getCilinicalProfile(medicalProfileId: medicalProfileID)
                }
            } else {
                self.hideProgress(progress)
            }
            
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return (self.sectionsData?.count)!
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerCell") as? CustomMedicalBodyHeader ?? CustomMedicalBodyHeader(reuseIdentifier: "headerCell")
        headerView.lblNameSection.text = sectionsData?[section].name
        headerView.setCollapsed((sectionsData?[section].collapsed)!)
        headerView.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        headerView.lblArrowSection.font = UIFont.fontAwesome(ofSize: 18)
        headerView.lblArrowSection.text = String.fontAwesomeIcon(name: .angleRight)
        headerView.section = section
        headerView.delegate = self
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return !sectionsData![section].collapsed ? 0 : sectionsData![section].items.count
        }else{
            return sectionsData![section].collapsed ? 0 : sectionsData![section].items.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (self.medicalHealthFactories?.count)! > 0{
            self.medicalHealFactoryObj = self.medicalHealthFactories?[0]
        }
        
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ThutucHCCustomCell
            cell.delegate = self
            cell.populateDistrictComboBox2()
            cell.populateProvinceComboBox2()
            cell.populateGroupBloodComboBox()
            if isEdit!{
                if healthRecords != nil{
                    self.medicalProfileObj = healthRecords?.medicalProfile
                    cell.medicalProfile = (healthRecords?.medicalProfile)!
                    
                    if let address = self.medicalProfileObj?.address{
                        cell.lblAddress.text = address
                        cell.lblAddress.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblAddress.isUserInteractionEnabled = false
                        }else{
                            cell.lblAddress.isUserInteractionEnabled = true
                        }
                    }
                        
                    if let patientName = self.medicalProfileObj?.patientName{
                        cell.lblUserName.text = patientName
                        cell.lblUserName.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblUserName.isUserInteractionEnabled = false
                        }else{
                            cell.lblUserName.isUserInteractionEnabled = true
                        }
                    }
                    if let email = self.medicalProfileObj?.email{
                        cell.lblEmailAddress.text = email
                        cell.lblEmailAddress.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblEmailAddress.isUserInteractionEnabled = false
                        }else{
                            cell.lblEmailAddress.isUserInteractionEnabled = true
                        }
                    }
                    if let mobilePhone = self.medicalProfileObj?.mobilePhone{
                        cell.lblPhoneNumber.text = "\(mobilePhone)"
                        cell.lblPhoneNumber.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblPhoneNumber.isUserInteractionEnabled = false
                        }else{
                            cell.lblPhoneNumber.isUserInteractionEnabled = true
                        }
                    }
                    if let birthDay = self.medicalProfileObj?.birthDay{
                        if birthDay != ""{
                            let result = resultDateFormatStandard(str: birthDay)
                            cell.lblBirthDay.text = result
                            cell.lblBirthDay.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                            if !isActive!{
                                cell.lblBirthDay.isUserInteractionEnabled = false
                            }else{
                                cell.lblBirthDay.isUserInteractionEnabled = true
                            }
                        }else{
                            cell.lblBirthDay.text = ""
                        }
                        
                    }
                    if let idNumber = self.medicalProfileObj?.idNumber{
                        cell.lblSoCMT.text = idNumber
                        cell.lblSoCMT.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblSoCMT.isUserInteractionEnabled = false
                        }else{
                            cell.lblSoCMT.isUserInteractionEnabled = true
                        }
                    }
                    if let healthInsuranceID = self.medicalProfileObj?.healthInsuranceID{
                        cell.lblSoBHYT.text = healthInsuranceID
                        cell.lblSoBHYT.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblSoBHYT.isUserInteractionEnabled = false
                        }else{
                            cell.lblSoBHYT.isUserInteractionEnabled = true
                        }
                    }
                    
                    if let weight = self.medicalProfileObj?.weight{
                        cell.lblWeight.text = "\(weight)"
                        cell.lblWeight.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblWeight.isUserInteractionEnabled = false
                        }else{
                            cell.lblWeight.isUserInteractionEnabled = true
                        }
                    }
                    if let height2 = self.medicalProfileObj?.height2{
                        cell.lblHeight.text = "\(height2)"
                        cell.lblHeight.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblHeight.isUserInteractionEnabled = false
                        }else{
                            cell.lblHeight.isUserInteractionEnabled = true
                        }
                    }
                   
                    if let provinceID = self.medicalProfileObj?.provinceID{
                        cell.currentProvinceIndex = provinceID
                        cell.populateProvinceComboBox2()
                        
                    }
                    if let districtID = self.medicalProfileObj?.districtID{
                        cell.currentDistrictIndex = districtID
                        cell.requestDistricts()
                        cell.populateDistrictComboBox2()
                    }
                    if let bloodGroupStr = self.medicalProfileObj?.bloodGroupStr{
                        cell.bloodGroupStr = bloodGroupStr
                        cell.resultGroupBloodComboBox()
                    }
                    
                    if self.medicalProfileObj?.gender == "F"{
                        cell.radio_Nam.isSelected  = false
                        cell.radio_Nu.isSelected = true
                    }else{
                        cell.radio_Nam.isSelected  = true
                        cell.radio_Nu.isSelected = false
                    }
                    if !isActive!{
                        cell.radio_Nam.isUserInteractionEnabled  = false
                        cell.radio_Nu.isUserInteractionEnabled = false
                    }else{
                        cell.radio_Nam.isUserInteractionEnabled  = true
                        cell.radio_Nu.isUserInteractionEnabled = true
                    }
                    
                    if !isActive!{
                        cell.districtComboBox.isUserInteractionEnabled  = false
                        cell.provinceComboBox.isUserInteractionEnabled = false
                        cell.gBloodComboBox.isUserInteractionEnabled = false
                    }else{
                        cell.districtComboBox.isUserInteractionEnabled  = true
                        cell.provinceComboBox.isUserInteractionEnabled = true
                        cell.gBloodComboBox.isUserInteractionEnabled = true
                    }
                    if !isActive!{
                        cell.btnBirthday.isUserInteractionEnabled = false
                    }else{
                        cell.btnBirthday.isUserInteractionEnabled = true
                    }
                }
            }else{
               
            }
        
            return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! TinhTrangLucSinhCustomCell
            cell.delegate = self
            if isEdit!{
                if healthRecords != nil{
                    if let medicalHealFactory = self.medicalHealFactoryObj{
                        cell.medicalHealFactory = medicalHealFactory
                    }
                    
                    if let cn = self.medicalHealFactoryObj?.cnLucDe{
                        cell.lblCannang.text = "\(cn)"
                        cell.lblCannang.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblCannang.isUserInteractionEnabled = false
                        }else{
                            cell.lblCannang.isUserInteractionEnabled = true
                            
                        }
                    }
                    if let cd = self.medicalHealFactoryObj?.cdLucDe{
                        cell.lblChieudai.text = "\(cd)"
                        cell.lblChieudai.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblChieudai.isUserInteractionEnabled = false
                        }else{
                            cell.lblChieudai.isUserInteractionEnabled = true
                        }
                    }
                    if let diTatBS = self.medicalHealFactoryObj?.diTatBS{
                        cell.lblDitatBS.text = diTatBS
                        cell.lblDitatBS.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblDitatBS.isUserInteractionEnabled = false
                        }else{
                            cell.lblDitatBS.isUserInteractionEnabled = true
                        }
                    }
                    if let ttLSNote = self.medicalHealFactoryObj?.ttLSNote{
                        cell.lblKhac.text = ttLSNote
                        cell.lblKhac.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblKhac.isUserInteractionEnabled = false
                        }else{
                            cell.lblKhac.isUserInteractionEnabled = true
                        }
                    }
                    
                    if let dt = self.medicalHealFactoryObj?.deThuong{
                        cell.checkbox1.setOn(dt, animated: true)
                        if !isActive!{
                            cell.checkbox1.isUserInteractionEnabled = false
                        }else{
                            cell.checkbox1.isUserInteractionEnabled = true
                        }
                    }
                    if let dm = self.medicalHealFactoryObj?.deMo{
                        cell.checkbox2.setOn(dm, animated: true)
                        if !isActive!{
                            cell.checkbox2.isUserInteractionEnabled = false
                        }else{
                            cell.checkbox2.isUserInteractionEnabled = true
                        }
                    }
                    if let dtt = self.medicalHealFactoryObj?.deThieuThang{
                        cell.checkbox3.setOn(dtt, animated: true)
                        if !isActive!{
                            cell.checkbox3.isUserInteractionEnabled = false
                        }else{
                            cell.checkbox3.isUserInteractionEnabled = true
                        }
                    }
                    if let bn = self.medicalHealFactoryObj?.biNgat{
                        cell.checkbox4.setOn(bn, animated: true)
                        if !isActive!{
                            cell.checkbox4.isUserInteractionEnabled = false
                        }else{
                            cell.checkbox4.isUserInteractionEnabled = true
                        }
                    }
                }
            }
            return cell
        }else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath) as! SuckhoeCanhanCustomCell
            
            cell.delegate = self
            if isEdit!{
                if healthRecords != nil{
                    if let medicalHealFactory = self.medicalHealFactoryObj{
                        cell.medicalHealFactory = medicalHealFactory
                    }
                    if let ht = medicalHealFactoryObj?.hutThuoc{
                        cell.Hutthuoc.setOn(ht, animated: true)
                        if !isActive!{
                            cell.Hutthuoc.isUserInteractionEnabled = false
                        }else{
                            cell.Hutthuoc.isUserInteractionEnabled = true
                        }
                    }
                    if let tt = self.medicalHealFactoryObj?.ttHutThuoc{
                        cell.Hutthuoc_TX.setOn(tt, animated: true)
                        if !isActive!{
                            cell.Hutthuoc_TX.isUserInteractionEnabled = false
                        }else{
                            cell.Hutthuoc_TX.isUserInteractionEnabled = true
                        }
                    }
                    if let htdb = self.medicalHealFactoryObj?.boHutThuoc{
                        cell.Hutthuoc_Dabo.setOn(htdb, animated: true)
                        if !isActive!{
                            cell.Hutthuoc_Dabo.isUserInteractionEnabled = false
                        }else{
                            cell.Hutthuoc_Dabo.isUserInteractionEnabled = true
                        }
                    }
                    
                    if let rb = self.medicalHealFactoryObj?.ruouBia{
                        cell.Uongruou.setOn(rb, animated: true)
                        if !isActive!{
                            cell.Uongruou.isUserInteractionEnabled = false
                        }else{
                            cell.Uongruou.isUserInteractionEnabled = true
                        }
                    }
                    if let num = self.medicalHealFactoryObj?.ruouBiaNgay{
                        cell.uongruou_Num.text = "\(num)"
                        cell.uongruou_Num.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.uongruou_Num.isUserInteractionEnabled = false
                        }else{
                            cell.uongruou_Num.isUserInteractionEnabled = true
                        }
                    }
                    if let rbdb = self.medicalHealFactoryObj?.boRuouBia{
                        cell.uongruou_dabo.setOn(rbdb, animated: true)
                        if !isActive!{
                            cell.uongruou_dabo.isUserInteractionEnabled = false
                        }else{
                            cell.uongruou_dabo.isUserInteractionEnabled = true
                        }
                    }
                    
                    if let mt = self.medicalHealFactoryObj?.maTuy{
                        cell.matuy.setOn(mt, animated: true)
                        if !isActive!{
                            cell.matuy.isUserInteractionEnabled = false
                        }else{
                            cell.matuy.isUserInteractionEnabled = true
                        }
                    }
                    if let mttx = self.medicalHealFactoryObj?.maTuyNgay{
                        cell.mathuy_Tx.setOn(mttx, animated: true)
                        if !isActive!{
                            cell.mathuy_Tx.isUserInteractionEnabled = false
                        }else{
                            cell.mathuy_Tx.isUserInteractionEnabled = true
                        }
                    }
                    if let mtdb = self.medicalHealFactoryObj?.boMaTuy{
                        cell.matuy_dabo.setOn(mtdb, animated: true)
                        if !isActive!{
                            cell.matuy_dabo.isUserInteractionEnabled = false
                        }else{
                            cell.matuy_dabo.isUserInteractionEnabled = true
                        }
                    }
                    
                    if let td = self.medicalHealFactoryObj?.theDuc{
                        cell.Hoatdong.setOn(td, animated: true)
                        if !isActive!{
                            cell.Hoatdong.isUserInteractionEnabled = false
                        }else{
                            cell.Hoatdong.isUserInteractionEnabled = true
                        }
                    }
                    if let tdtx = self.medicalHealFactoryObj?.theDucNgay{
                        cell.Hoatdong_TX.setOn(tdtx, animated: true)
                        if !isActive!{
                            cell.Hoatdong_TX.isUserInteractionEnabled = false
                        }else{
                            cell.Hoatdong_TX.isUserInteractionEnabled = true
                        }
                    }
                    if let tddb = self.medicalHealFactoryObj?.nghiTheDuc{
                        cell.Hoatdong_Dabo.setOn(tddb, animated: true)
                        if !isActive!{
                            cell.Hoatdong_Dabo.isUserInteractionEnabled = false
                        }else{
                            cell.Hoatdong_Dabo.isUserInteractionEnabled = true
                        }
                    }
                    
                    if let hoachat = self.medicalHealFactoryObj?.duHoaChatMP{
                        cell.lblhoachat.text = hoachat
                        cell.lblhoachat.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblhoachat.isUserInteractionEnabled = false
                        }else{
                            cell.lblhoachat.isUserInteractionEnabled = true
                        }
                    }
                    if let tshoachat = self.medicalHealFactoryObj?.yeuToTXNNTime{
                        cell.lblTimeTX.text = tshoachat
                        cell.lblTimeTX.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblTimeTX.isUserInteractionEnabled = false
                        }else{
                            cell.lblTimeTX.isUserInteractionEnabled = true
                        }
                    }
                    if let loaiHoXi = self.medicalHealFactoryObj?.loaiHoXi{
                        cell.lblLoaiHX.text = loaiHoXi
                        cell.lblLoaiHX.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblLoaiHX.isUserInteractionEnabled = false
                        }else{
                            cell.lblLoaiHX.isUserInteractionEnabled = true
                        }
                    }
                    if let yeutokhac = self.medicalHealFactoryObj?.yeuToTXNN{
                        if yeutokhac.isEmpty{
                            cell.labelOther.isHidden = true
                            cell.lblOther.text = "Yếu tố khác"
                            cell.lblOther.textColor = UIColor.lightGray
                        }else{
                            cell.labelOther.isHidden = false
                            cell.lblOther.text = yeutokhac
                            cell.lblOther.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        }
                        
                        
                        if !isActive!{
                            cell.lblOther.isEditable = false
                            cell.lblOther.isUserInteractionEnabled = false
                        }else{
                            cell.lblOther.isEditable = true
                            cell.lblOther.isUserInteractionEnabled = true
                        }
                    }
                }
            }
            return cell
        }else if indexPath.section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3", for: indexPath) as! TiensubenhtatCustomCell
            if IS_IPHONE_5{
                if #available(iOS 9.0, *) {
                    cell.ckBenhTHA.leftAnchor.constraint(equalTo: cell.contentView.leftAnchor, constant: 172).isActive = true
                } else {
                    // Fallback on earlier versions
                }
            }
            
            cell.delegate = self
            if isEdit!{
                if healthRecords != nil{
                    if let medicalHealFactory = self.medicalHealFactoryObj{
                        cell.medicalHealFactory = medicalHealFactory
                    }
                    if let thuoc = self.medicalHealFactoryObj?.diUngThuoc{
                        cell.lblThuoc.text = thuoc
                        cell.lblThuoc.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblThuoc.isUserInteractionEnabled = false
                        }else{
                            cell.lblThuoc.isUserInteractionEnabled = true
                        }
                    }
                    if let diung = self.medicalHealFactoryObj?.duNote{
                        cell.lblDiung.text = diung
                        cell.lblDiung.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblDiung.isUserInteractionEnabled = false
                        }else{
                            cell.lblDiung.isUserInteractionEnabled = true
                        }
                    }
                    if let tp = self.medicalHealFactoryObj?.duThucPham{
                        cell.lblThucpham.text = tp
                        cell.lblThucpham.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblThucpham.isUserInteractionEnabled = false
                        }else{
                            cell.lblThucpham.isUserInteractionEnabled = true
                        }
                    }
                    if let hc = self.medicalHealFactoryObj?.duHoaChatMP{
                        cell.lblHoachatMP.text = hc
                        cell.lblHoachatMP.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblHoachatMP.isUserInteractionEnabled = false
                        }else{
                            cell.lblHoachatMP.isUserInteractionEnabled = true
                        }
                    }
                    if let ut = self.medicalHealFactoryObj?.ungThu{
                        cell.lblUngthu.text = ut
                        cell.lblUngthu.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblUngthu.isUserInteractionEnabled = false
                        }else{
                            cell.lblUngthu.isUserInteractionEnabled = true
                        }
                    }
                    if let lao = self.medicalHealFactoryObj?.lao{
                        cell.lblLao.text = lao
                        cell.lblLao.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblLao.isUserInteractionEnabled = false
                        }else{
                            cell.lblLao.isUserInteractionEnabled = true
                        }
                    }
                    if let bk = self.medicalHealFactoryObj?.benhNote{
                        cell.lblBenhkhac.text = bk
                        cell.lblBenhkhac.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblBenhkhac.isUserInteractionEnabled = false
                        }else{
                            cell.lblBenhkhac.isUserInteractionEnabled = true
                        }
                    }
                    if let tm = self.medicalHealFactoryObj?.benhTimMach{
                        cell.ckTimmach.setOn(tm, animated: true)
                        if !isActive!{
                            cell.ckTimmach.isUserInteractionEnabled = false
                        }else{
                            cell.ckTimmach.isUserInteractionEnabled = true
                        }
                    }
                    if let phoi = self.medicalHealFactoryObj?.benhPhoiManTinh{
                        cell.ckBenhphoi.setOn(phoi, animated: true)
                        if !isActive!{
                            cell.ckBenhphoi.isUserInteractionEnabled = false
                        }else{
                            cell.ckBenhphoi.isUserInteractionEnabled = true
                        }
                    }
                    if let dd = self.medicalHealFactoryObj?.benhDaDay{
                        cell.ckDaday.setOn(dd, animated: true)
                        if !isActive!{
                            cell.ckDaday.isUserInteractionEnabled = false
                        }else{
                            cell.ckDaday.isUserInteractionEnabled = true
                        }
                    }
                    if let dtd = self.medicalHealFactoryObj?.daiThaoDuong{
                        cell.ckDaiTD.setOn(dtd, animated: true)
                        if !isActive!{
                            cell.ckDaiTD.isUserInteractionEnabled = false
                        }else{
                            cell.ckDaiTD.isUserInteractionEnabled = true
                        }
                    }
                    if let tha = self.medicalHealFactoryObj?.tangHuyetAp{
                        cell.ckBenhTHA.setOn(tha, animated: true)
                        if !isActive!{
                            cell.ckBenhTHA.isUserInteractionEnabled = false
                        }else{
                            cell.ckBenhTHA.isUserInteractionEnabled = true
                        }
                    }
                    if let vg = self.medicalHealFactoryObj?.viemGan{
                        cell.ckBenhVG.setOn(vg, animated: true)
                        if !isActive!{
                            cell.ckBenhVG.isUserInteractionEnabled = false
                        }else{
                            cell.ckBenhVG.isUserInteractionEnabled = true
                        }
                    }
                    if let bbc = self.medicalHealFactoryObj?.benhBuouCo{
                        cell.ckBenhBuouco.setOn(bbc, animated: true)
                        if !isActive!{
                            cell.ckBenhBuouco.isUserInteractionEnabled = false
                        }else{
                            cell.ckBenhBuouco.isUserInteractionEnabled = true
                        }
                    }
                    if let hx = self.medicalHealFactoryObj?.henSuyen{
                        cell.ckHenxuyen.setOn(hx, animated: true)
                        if !isActive!{
                            cell.ckHenxuyen.isUserInteractionEnabled = false
                        }else{
                            cell.ckHenxuyen.isUserInteractionEnabled = true
                        }
                    }
                    if let dk = self.medicalHealFactoryObj?.dongKinh{
                        cell.ckDongkinh.setOn(dk, animated: true)
                        if !isActive!{
                            cell.ckDongkinh.isUserInteractionEnabled = false
                        }else{
                            cell.ckDongkinh.isUserInteractionEnabled = true
                        }
                    }
                    if let tk = self.medicalHealFactoryObj?.tuKy{
                        cell.ckTuky.setOn(tk, animated: true)
                        if !isActive!{
                            cell.ckTuky.isUserInteractionEnabled = false
                        }else{
                            cell.ckTuky.isUserInteractionEnabled = true
                        }
                    }
                    if let tt = self.medicalHealFactoryObj?.tamThan{
                        cell.ckTamthan.setOn(tt, animated: true)
                        if !isActive!{
                            cell.ckTamthan.isUserInteractionEnabled = false
                        }else{
                            cell.ckTamthan.isUserInteractionEnabled = true
                        }
                    }
                    if let tbs = self.medicalHealFactoryObj?.timBamSinh{
                        cell.ckTimBS.setOn(tbs, animated: true)
                        if !isActive!{
                            cell.ckTimBS.isUserInteractionEnabled = false
                        }else{
                            cell.ckTimBS.isUserInteractionEnabled = true
                        }
                    }
                }
            }
            return cell
        }else if indexPath.section == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell4", for: indexPath) as! KhuyettatCustomCell
            cell.delegate = self
            if isEdit!{
                if healthRecords != nil{
                    if let medicalHealFactory = self.medicalHealFactoryObj{
                        cell.medicalHealFactory = medicalHealFactory
                    }
                    if let tl = self.medicalHealFactoryObj?.khuyetTatThinhLuc{
                        cell.lblThinhluc.text = tl
                        cell.lblThinhluc.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblThinhluc.isUserInteractionEnabled = false
                        }else{
                            cell.lblThinhluc.isUserInteractionEnabled = true
                        }
                    }
                    if let kt = self.medicalHealFactoryObj?.khuyetTatNote{
                        cell.lblKhuyettat.text = kt
                        cell.lblKhuyettat.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblKhuyettat.isUserInteractionEnabled = false
                        }else{
                            cell.lblKhuyettat.isUserInteractionEnabled = true
                        }
                    }
                    if let hm = self.medicalHealFactoryObj?.kheHoMoiVM{
                        cell.lblKhehomoi.text = hm
                        cell.lblKhehomoi.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblKhehomoi.isUserInteractionEnabled = false
                        }else{
                            cell.lblKhehomoi.isUserInteractionEnabled = true
                        }
                    }
                    if let chan = self.medicalHealFactoryObj?.ktChan{
                        cell.lblChan.text = chan
                        cell.lblChan.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblChan.isUserInteractionEnabled = false
                        }else{
                            cell.lblChan.isUserInteractionEnabled = true
                        }
                    }
                    if let tay = self.medicalHealFactoryObj?.ktTay{
                        cell.lblTay.text = tay
                        cell.lblTay.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblTay.isUserInteractionEnabled = false
                        }else{
                            cell.lblTay.isUserInteractionEnabled = true
                        }
                    }
                    if let tl = self.medicalHealFactoryObj?.ktThiLuc{
                        cell.lblThiluc.text = tl
                        cell.lblThiluc.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblThiluc.isUserInteractionEnabled = false
                        }else{
                            cell.lblThiluc.isUserInteractionEnabled = true
                        }
                    }
                    if let cv = self.medicalHealFactoryObj?.congVeoCS{
                        cell.lblCongveo.text = cv
                        cell.lblCongveo.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblCongveo.isUserInteractionEnabled = false
                        }else{
                            cell.lblCongveo.isUserInteractionEnabled = true
                        }
                    }
                }
            }
            return cell
        }else if indexPath.section == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell5", for: indexPath) as! TiensuphauthuatCustomCell
            cell.delegate = self
            if isEdit!{
                if healthRecords != nil{
                    if let medicalHealFactory = self.medicalHealFactoryObj{
                        cell.medicalHealFactory = medicalHealFactory
                    }
                    if let tspt = self.medicalHealFactoryObj?.tienSuPhauThuat{
                        cell.lblTiensuPT.text = tspt
                        cell.lblTiensuPT.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblTiensuPT.isUserInteractionEnabled = false
                        }else{
                            cell.lblTiensuPT.isUserInteractionEnabled = true
                        }
                    }
                }
            }
            return cell
        }else if indexPath.section == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell6", for: indexPath) as! TiensugiadinhCustomCell
            cell.delegate = self
            if isEdit!{
                if healthRecords != nil{
                    if let medicalHealFactory = self.medicalHealFactoryObj{
                        cell.medicalHealFactory = medicalHealFactory
                    }
                    if let tm = self.medicalHealFactoryObj?.tsGDBenhTimMach{
                        cell.lblTimmach_CK.setOn(tm, animated: true)
                        if !isActive!{
                            cell.lblTimmach_CK.isUserInteractionEnabled = false
                        }else{
                            cell.lblTimmach_CK.isUserInteractionEnabled = true
                        }
                    }
                    if let tha = self.medicalHealFactoryObj?.tsGDTangHuyetAp{
                        cell.lblTHA_CK.setOn(tha, animated: true)
                        if !isActive!{
                            cell.lblTHA_CK.isUserInteractionEnabled = false
                        }else{
                            cell.lblTHA_CK.isUserInteractionEnabled = true
                        }
                    }
                    if let tt = self.medicalHealFactoryObj?.tsGDTamThan{
                        cell.lblTamthan_CK.setOn(tt, animated: true)
                        if !isActive!{
                            cell.lblTamthan_CK.isUserInteractionEnabled = false
                        }else{
                            cell.lblTamthan_CK.isUserInteractionEnabled = true
                        }
                    }
                    if let hx = self.medicalHealFactoryObj?.tsGDHenSuyen{
                        cell.lblHenxuyen_CK.setOn(hx, animated: true)
                        if !isActive!{
                            cell.lblHenxuyen_CK.isUserInteractionEnabled = false
                        }else{
                            cell.lblHenxuyen_CK.isUserInteractionEnabled = true
                        }
                    }
                    if let dtd = self.medicalHealFactoryObj?.tsGDDaiThaoDuong{
                        cell.lblDaiTD_CK.setOn(dtd, animated: true)
                        if !isActive!{
                            cell.lblDaiTD_CK.isUserInteractionEnabled = false
                        }else{
                            cell.lblDaiTD_CK.isUserInteractionEnabled = true
                        }
                    }
                    if let dk = self.medicalHealFactoryObj?.tsGDDongKinh{
                        cell.lblDongkinh_CK.setOn(dk, animated: true)
                        if !isActive!{
                            cell.lblDongkinh_CK.isUserInteractionEnabled = false
                        }else{
                            cell.lblDongkinh_CK.isUserInteractionEnabled = true
                        }
                    }
                    
                    if let tm_nm = self.medicalHealFactoryObj?.tsGDBenhTimMachName{
                        cell.lblTimmach_NM.text = tm_nm
                        cell.lblTimmach_NM.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblTimmach_NM.isUserInteractionEnabled = false
                        }else{
                            cell.lblTimmach_NM.isUserInteractionEnabled = true
                        }
                    }
                    if let tha_nm = self.medicalHealFactoryObj?.tsGDTangHuyetApName{
                        cell.lblTHA_NM.text = tha_nm
                        cell.lblTHA_NM.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblTHA_NM.isUserInteractionEnabled = false
                        }else{
                            cell.lblTHA_NM.isUserInteractionEnabled = true
                        }
                    }
                    if let tt_nm = self.medicalHealFactoryObj?.tsGDTamThanName{
                        cell.lblTamthan_NM.text = tt_nm
                        cell.lblTamthan_NM.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblTamthan_NM.isUserInteractionEnabled = false
                        }else{
                            cell.lblTamthan_NM.isUserInteractionEnabled = true
                        }
                    }
                    if let hx_nm = self.medicalHealFactoryObj?.tsGDHenSuyenName{
                        cell.lblHenxuyen_NM.text = hx_nm
                        cell.lblHenxuyen_NM.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblHenxuyen_NM.isUserInteractionEnabled = false
                        }else{
                            cell.lblHenxuyen_NM.isUserInteractionEnabled = true
                        }
                    }
                    if let dtd_nm = self.medicalHealFactoryObj?.tsGDDaiThaoDuongName{
                        cell.lblDaiTD_NM.text = dtd_nm
                        cell.lblDaiTD_NM.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblDaiTD_NM.isUserInteractionEnabled = false
                        }else{
                            cell.lblDaiTD_NM.isUserInteractionEnabled = true
                        }
                    }
                    if let dk_nm = self.medicalHealFactoryObj?.tsGDDongKinhName{
                        cell.lblDongkinh_NM.text = dk_nm
                        cell.lblDongkinh_NM.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblDongkinh_NM.isUserInteractionEnabled = false
                        }else{
                            cell.lblDongkinh_NM.isUserInteractionEnabled = true
                        }
                    }
                    
                    if let th = self.medicalHealFactoryObj?.tsGDThuoc{
                        cell.tsgd_lblThuoc.text = th
                        cell.tsgd_lblThuoc.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.tsgd_lblThuoc.isUserInteractionEnabled = false
                        }else{
                            cell.tsgd_lblThuoc.isUserInteractionEnabled = true
                        }
                    }
                    if let th_nm = self.medicalHealFactoryObj?.tsGDThuocName{
                        cell.lblThuoc_NM.text = th_nm
                        cell.lblThuoc_NM.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblThuoc_NM.isUserInteractionEnabled = false
                        }else{
                            cell.lblThuoc_NM.isUserInteractionEnabled = true
                        }
                    }
                    
                    if let thuoc = self.medicalHealFactoryObj?.tsGDThuoc{
                        
                        cell.tsgd_lblThuoc.text = thuoc
                        cell.tsgd_lblThuoc.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.tsgd_lblThuoc.isUserInteractionEnabled = false
                        }else{
                            cell.tsgd_lblThuoc.isUserInteractionEnabled = true
                        }
                    }
                    if let diung = self.medicalHealFactoryObj?.tsGDNote{
                        cell.tsgd_lblDiung.text = diung
                        cell.tsgd_lblDiung.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.tsgd_lblDiung.isUserInteractionEnabled = false
                        }else{
                            cell.tsgd_lblDiung.isUserInteractionEnabled = true
                        }
                    }
                    if let diung_nm = self.medicalHealFactoryObj?.tsGDNoteName{
                        cell.lblDiung_NM.text = diung_nm
                        cell.lblDiung_NM.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblDiung_NM.isUserInteractionEnabled = false
                        }else{
                            cell.lblDiung_NM.isUserInteractionEnabled = true
                        }
                    }
                    if let tp = self.medicalHealFactoryObj?.tsGDThucPham{
                        cell.tsgd_lblThucpham.text = tp
                        cell.tsgd_lblThucpham.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.tsgd_lblThucpham.isUserInteractionEnabled = false
                        }else{
                            cell.tsgd_lblThucpham.isUserInteractionEnabled = true
                        }
                    }
                    if let tp_nm = self.medicalHealFactoryObj?.tsGDThucPhamName{
                        cell.lblThucpham_NM.text = tp_nm
                        cell.lblThucpham_NM.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblThucpham_NM.isUserInteractionEnabled = false
                        }else{
                            cell.lblThucpham_NM.isUserInteractionEnabled = true
                        }
                    }
                    if let hcmp = self.medicalHealFactoryObj?.tsGDHoaChatMyPham{
                        cell.tsgd_lblHoachat.text = hcmp
                        cell.tsgd_lblHoachat.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.tsgd_lblHoachat.isUserInteractionEnabled = false
                        }else{
                            cell.tsgd_lblHoachat.isUserInteractionEnabled = true
                        }
                    }
                    if let hcmp_nm = self.medicalHealFactoryObj?.tsGDHoaChatMyPhamName{
                        cell.lblHoachat_NM.text = hcmp_nm
                        cell.lblHoachat_NM.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblHoachat_NM.isUserInteractionEnabled = false
                        }else{
                            cell.lblHoachat_NM.isUserInteractionEnabled = true
                        }
                    }
                    
                    if let ut = self.medicalHealFactoryObj?.tsGDUngThu{
                        cell.lblungthu.text = ut
                        cell.lblungthu.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblungthu.isUserInteractionEnabled = false
                        }else{
                            cell.lblungthu.isUserInteractionEnabled = true
                        }
                    }
                    if let ut_nm = self.medicalHealFactoryObj?.tsGDUngThuName{
                        cell.lblungthu_NM.text = ut_nm
                        cell.lblungthu_NM.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblungthu_NM.isUserInteractionEnabled = false
                        }else{
                            cell.lblungthu_NM.isUserInteractionEnabled = true
                        }
                    }
                    if let lao = self.medicalHealFactoryObj?.tsGDLao{
                        cell.lblLao.text = lao
                        cell.lblLao.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblLao.isUserInteractionEnabled = false
                        }else{
                            cell.lblLao.isUserInteractionEnabled = true
                        }
                    }
                    if let lao_nm = self.medicalHealFactoryObj?.tsGDLaoName{
                        cell.lblLao_NM.text = lao_nm
                        cell.lblLao_NM.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblLao_NM.isUserInteractionEnabled = false
                        }else{
                            cell.lblLao_NM.isUserInteractionEnabled = true
                        }
                    }
                    
                    if let another = self.medicalHealFactoryObj?.tsGDNoteAnother{
                        cell.lblKhac.text = another
                        cell.lblKhac.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblKhac.isUserInteractionEnabled = false
                        }else{
                            cell.lblKhac.isUserInteractionEnabled = true
                        }
                    }
                    if let another_nm = self.medicalHealFactoryObj?.tsGDNoteAnotherName{
                        cell.lblKhac_NM.text = another_nm
                        cell.lblKhac_NM.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblKhac_NM.isUserInteractionEnabled = false
                        }else{
                            cell.lblKhac_NM.isUserInteractionEnabled = true
                        }
                    }
                }
            }
            return cell
        }else if indexPath.section == 7{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell7", for: indexPath) as! SinhsanKHHGTCustomCell
            cell.delegate = self
            if isEdit!{
                if healthRecords != nil{
                    if let medicalHealFactory = self.medicalHealFactoryObj{
                        cell.medicalHealFactory = medicalHealFactory
                    }
                    if let ktc = self.medicalHealFactoryObj?.kyThaiCC{
                        cell.lblKythaicuoi.text = ktc
                        cell.lblKythaicuoi.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblKythaicuoi.isUserInteractionEnabled = false
                        }else{
                            cell.lblKythaicuoi.isUserInteractionEnabled = true
                        }
                    }
                    if let bpk = self.medicalHealFactoryObj?.benhPhuKhoa{
                        cell.lblBenhPhukhoa.text = bpk
                        cell.lblBenhPhukhoa.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblBenhPhukhoa.isUserInteractionEnabled = false
                        }else{
                            cell.lblBenhPhukhoa.isUserInteractionEnabled = true
                        }
                    }
                    if let num_chirl = self.medicalHealFactoryObj?.soConHienSong{
                        cell.lblSocon.text = "\(num_chirl)"
                        cell.lblSocon.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblSocon.isUserInteractionEnabled = false
                        }else{
                            cell.lblSocon.isUserInteractionEnabled = true
                        }
                    }
                    if let sldn = self.medicalHealFactoryObj?.deNon{
                        cell.lblSolanDeNon.text = "\(sldn)"
                        cell.lblSolanDeNon.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblSolanDeNon.isUserInteractionEnabled = false
                        }else{
                            cell.lblSolanDeNon.isUserInteractionEnabled = true
                        }
                    }
                    if let slddt = self.medicalHealFactoryObj?.deDuThang{
                        cell.lblSolanDeDT.text = "\(slddt)"
                        cell.lblSolanDeDT.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblSolanDeDT.isUserInteractionEnabled = false
                        }else{
                            cell.lblSolanDeDT.isUserInteractionEnabled = true
                        }
                    }
                    if let dk = self.medicalHealFactoryObj?.deKho{
                        cell.lblDekho.text = "\(dk)"
                        cell.lblDekho.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblDekho.isUserInteractionEnabled = false
                        }else{
                            cell.lblDekho.isUserInteractionEnabled = true
                        }
                    }
                    if let dm = self.medicalHealFactoryObj?.deMoNum{
                        cell.lblDemo.text = "\(dm)"
                        cell.lblDemo.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblDemo.isUserInteractionEnabled = false
                        }else{
                            cell.lblDemo.isUserInteractionEnabled = true
                        }
                    }
                    if let dt = self.medicalHealFactoryObj?.deThuongNum{
                        cell.lblDethuong.text = "\(dt)"
                        cell.lblDethuong.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblDethuong.isUserInteractionEnabled = false
                        }else{
                            cell.lblDethuong.isUserInteractionEnabled = true
                        }
                    }
                    if let num = self.medicalHealFactoryObj?.sinhDe{
                        cell.lblSolanSD.text = "\(num)"
                        cell.lblSolanSD.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblSolanSD.isUserInteractionEnabled = false
                        }else{
                            cell.lblSolanSD.isUserInteractionEnabled = true
                        }
                    }
                    if let num_pt = self.medicalHealFactoryObj?.phaThai{
                        cell.lblSolanphathai.text = "\(num_pt)"
                        cell.lblSolanphathai.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblSolanphathai.isUserInteractionEnabled = false
                        }else{
                            cell.lblSolanphathai.isUserInteractionEnabled = true
                        }
                    }
                    if let num_st = self.medicalHealFactoryObj?.sayThai{
                        cell.lblSolanxaythai.text = "\(num_st)"
                        cell.lblSolanxaythai.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblSolanxaythai.isUserInteractionEnabled = false
                        }else{
                            cell.lblSolanxaythai.isUserInteractionEnabled = true
                        }
                    }
                    if let num_ct = self.medicalHealFactoryObj?.coThai{
                        cell.lblSolancothai.text = "\(num_ct)"
                        cell.lblSolancothai.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblSolancothai.isUserInteractionEnabled = false
                        }else{
                            cell.lblSolancothai.isUserInteractionEnabled = true
                        }
                    }
                    if let bptt = self.medicalHealFactoryObj?.pPTranhThaiDD{
                        cell.lblBPTranhthai.text = bptt
                        cell.lblBPTranhthai.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.lblBPTranhthai.isUserInteractionEnabled = false
                        }else{
                            cell.lblBPTranhthai.isUserInteractionEnabled = true
                        }
                    }
                }
            }
            
            return cell
            
        }else if indexPath.section == 8{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell8", for: indexPath) as! MuitiemCustomCell
            cell.delegate = self
            if isEdit!{
                if healthRecords != nil{
                    if let medicalHealFactory = self.medicalHealFactoryObj{
                        cell.medicalHealFactory = medicalHealFactory
                    }
                    if let note = self.medicalHealFactoryObj?.note{
                        cell.txtIssueNote.text = note
                        cell.txtIssueNote.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                        if !isActive!{
                            cell.txtIssueNote.isUserInteractionEnabled = false
                        }else{
                            cell.txtIssueNote.isUserInteractionEnabled = true
                        }
                    }
                }
            }
            return cell
        }
        else if indexPath.section == 9{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell9", for: indexPath) as! TiemchungTreemCustomCell
            cell.myCollectionView.register(UINib(nibName: "VacxinCustomViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
            cell.myCollectionView.viewWithTag(9)
            cell.btnAddMuitiem.tag = 9
            cell.btnAddMuitiem.addTarget(self, action:#selector(addMuitiemAction), for: .touchUpInside)
            if isEdit!{
                cell.btnAddMuitiem.isHidden = true
                if isActive!{
                    cell.lblNumber.isUserInteractionEnabled = true
                    cell.btnAddMuitiem.isHidden = false
                }else{
                    cell.btnAddMuitiem.isHidden = true
                    cell.lblNumber.isUserInteractionEnabled = false
                }
            }else{
                cell.btnAddMuitiem.isHidden = false
            }
            
            if (self.medicalVaccin1?.count)! > 0{
                cell.myCollectionView.isHidden = false
                cell.lblNameDefault.isHidden = true
                cell.lblNumber.isHidden = false
                cell.lblNumberTitle.isHidden = false
            }else{
                cell.lblNameDefault.isHidden = false
                cell.myCollectionView.isHidden = true
                cell.lblNumber.isHidden = false
                cell.lblNumberTitle.isHidden = false
            }
            return cell
        }else if indexPath.section == 10{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell9", for: indexPath) as! TiemchungTreemCustomCell
            cell.spaceBottonView.constant = 0
            cell.myCollectionView.register(UINib(nibName: "VacxinCustomViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
            cell.myCollectionView.viewWithTag(10)
            cell.btnAddMuitiem.tag = 10
            cell.btnAddMuitiem.addTarget(self, action:#selector(addMuitiemAction), for: .touchUpInside)
            if isEdit!{
                cell.btnAddMuitiem.isHidden = true
                if isActive!{
                    cell.btnAddMuitiem.isHidden = false
                }else{
                    cell.btnAddMuitiem.isHidden = true
                }
            }else{
                cell.btnAddMuitiem.isHidden = false
            }
            if (self.medicalVaccin2?.count)! > 0{
                cell.myCollectionView.isHidden = false
                cell.lblNameDefault.isHidden = true
               // cell.lblNumber.isHidden = true
                cell.lblNumberTitle.isHidden = true
            }else{
                cell.lblNameDefault.isHidden = false
                cell.myCollectionView.isHidden = true
               // cell.lblNumber.isHidden = true
                cell.lblNumberTitle.isHidden = true
            }
            
            return cell
        }else if indexPath.section == 11{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell9", for: indexPath) as! TiemchungTreemCustomCell
            cell.spaceBottonView.constant = 0
            cell.myCollectionView.register(UINib(nibName: "VacxinCustomViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
            cell.myCollectionView.viewWithTag(11)
            cell.btnAddMuitiem.tag = 11
            cell.btnAddMuitiem.addTarget(self, action:#selector(addMuitiemAction), for: .touchUpInside)
            if isEdit!{
                cell.btnAddMuitiem.isHidden = true
                if isActive!{
                    cell.btnAddMuitiem.isHidden = false
                }else{
                    cell.btnAddMuitiem.isHidden = true
                }
            }else{
                cell.btnAddMuitiem.isHidden = false
            }
            
            if (self.medicalVaccin3?.count)! > 0{
                cell.lblNameDefault.isHidden = true
                //cell.lblNumber.isHidden = true
                cell.lblNumberTitle.isHidden = true
                cell.myCollectionView.isHidden = false
            }else{
                cell.lblNameDefault.isHidden = false
                cell.myCollectionView.isHidden = true
                //cell.lblNumber.isHidden = true
                cell.lblNumberTitle.isHidden = true
            }
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell9", for: indexPath) as! TiemchungTreemCustomCell
            cell.spaceBottonView.constant = 0
            cell.myCollectionView.register(UINib(nibName: "KhamLSCustomViewCell", bundle: nil), forCellWithReuseIdentifier: "LSCell")
            cell.myCollectionView.viewWithTag(12)
            cell.btnAddMuitiem.tag = 12
            cell.btnAddMuitiem.setTitle("Thêm lần khám", for: UIControlState.normal)
            if isEdit!{
                cell.btnAddMuitiem.isHidden = true
                if isActive!{
                    cell.btnAddMuitiem.isHidden = false
                }else{
                    cell.btnAddMuitiem.isHidden = true
                }
            }else{
                cell.btnAddMuitiem.isHidden = false
            }
            cell.btnAddMuitiem.addTarget(self, action:#selector(addMuitiemAction), for: .touchUpInside)
            if (self.medicalProfiles?.count)! > 0{
                cell.lblNameDefault.isHidden = true
                //cell.lblNumber.isHidden = true
                cell.lblNumberTitle.isHidden = true
                cell.myCollectionView.isHidden = false
            }else{
                cell.lblNameDefault.isHidden = false
                cell.myCollectionView.isHidden = true
                //cell.lblNumber.isHidden = true
                cell.lblNumberTitle.isHidden = true
            }
            return cell
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    func toggleSection(_ header: CustomMedicalBodyHeader, section: Int) {
        let collapsed = !(self.sectionsData?[section].collapsed)!
        self.sectionsData?[section].collapsed = collapsed
        // Toggle collapse
        if section == 0{
            header.setCollapsed(!collapsed)
        }else{
            header.setCollapsed(collapsed)
        }
        
        myTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
    func addMuitiemAction(sender: UIButton){
        if sender.tag == 9{
            addMuitiemVC(tag:sender.tag)
        }else if sender.tag == 10{
            addMuitiemVC(tag:sender.tag)
        }else if sender.tag == 11{
            addMuitiemVC(tag:sender.tag)
        }else{
            addLamsangVC(tag:sender.tag)
        }
    }
    
    func addMuitiemVC(tag:Int){
        let addMuitiemVC = AddMuitiemVC(nibName: "AddMuitiemVC", bundle: nil)
        addMuitiemVC.delegate = self
        addMuitiemVC.currentTag = tag
        addMuitiemVC.modalPresentationStyle = .custom
        addMuitiemVC.transitioningDelegate = self
        addMuitiemVC.view.frame = CGRect(x: screenSizeWidth/2-162, y: screenSizeHeight/2-194, width: 324, height: 388)
        self.present(addMuitiemVC, animated: true, completion: nil)
    }
    
    func addLamsangVC(tag:Int){
        let lamsangVC = LamsangVC(nibName: "LamsangVC", bundle: nil)
        lamsangVC.delegate = self
        lamsangVC.currentTag = tag
        self.navigationController?.pushViewController(lamsangVC, animated: true)
    }
    
    func resultDataMedicalCilinnical(isUpdateItem:Bool, currentIndex:Int, medicalCilinnical:MedicalCilinnical, tag:Int){
        if isUpdateItem{
            self.medicalProfiles?[currentIndex] = medicalCilinnical
        }else{
            self.medicalProfiles?.append(medicalCilinnical)
        }
        self.myTableView.reloadSections(IndexSet(integer: tag), with: .automatic)
    }
 
    func gobackDetailView(isUpdateItem:Bool, currentTag:Int, currentIndex:Int, vaccinID:Int, vaccinTypeID:Int, vaccinName:String, vaccinDate:String, vaccinSchedule:String, vaccinReact:String, monthPreg:Int, vacccinIdx:Int){
        self.currentVacxinIdx = vacccinIdx
        let medicalVaccinObj = MedicalVaccin()
        medicalVaccinObj.vaccinID = vaccinID
        medicalVaccinObj.vaccinTypeID = vaccinTypeID
        medicalVaccinObj.vaccinName = vaccinName
        medicalVaccinObj.vaccinDate = vaccinDate
        medicalVaccinObj.vaccinSchedule = vaccinSchedule
        medicalVaccinObj.vaccinReact = vaccinReact
        medicalVaccinObj.monthPreg = monthPreg
        medicalVaccinObj.active = true
        if currentTag == 9{
            if isUpdateItem{
                self.medicalVaccin1?[currentIndex] = medicalVaccinObj
                Toast(text: "Mũi tiêm cập nhật thành công").show()
            }else{
                self.medicalVaccin1?.append(medicalVaccinObj)
                Toast(text: "Mũi tiêm thêm mới thành công").show()
            }
            
        }else if currentTag == 10{
            if isUpdateItem{
                self.medicalVaccin2?[currentIndex] = medicalVaccinObj
                 Toast(text: "Tiêm chủng ngoài chương trình TCMR cập nhật thành công").show()
            }else{
                self.medicalVaccin2?.append(medicalVaccinObj)
                Toast(text: "Tiêm chủng ngoài chương trình TCMR thêm mới thành công").show()
            }
            
        }else if currentTag == 11{
            if isUpdateItem{
                self.medicalVaccin3?[currentIndex] = medicalVaccinObj
                Toast(text: "Tiêm chủng uốn ván cập nhật thành công").show()
            }else{
                self.medicalVaccin3?.append(medicalVaccinObj)
                Toast(text: "Tiêm chủng uốn ván thêm mới thành công").show()
            }
        }else{
            
        }
        
        self.myTableView.reloadSections(IndexSet(integer: currentTag), with: .automatic)
    }
    
    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController!, sourceViewController source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController2(presentedViewController: presented, presenting: presentingViewController)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.myTableView .deselectRow(at: indexPath, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch indexPath.section{
        case 0:
            return 480.0
        case 1:
            return 400.0
        case 2:
            return 650.0
        case 3:
            return 820.0
        case 4:
            return 490.0
        case 5:
            return 80.0
        case 6:
            return 880.0
        case 7:
            return 580.0
        case 8:
            return 90.0
        case 9:
            if (self.medicalVaccin1?.count)! > 0{
                return CGFloat(130*(self.medicalVaccin1!.count)+110)
            }else{
                return 110.0
            }
        case 10:
            if (self.medicalVaccin2?.count)! > 0{
                return CGFloat(130*(self.medicalVaccin2!.count)+110)
            }else{
                return 90.0
            }
        case 11:
            if (self.medicalVaccin3?.count)! > 0{
                return CGFloat(150*(self.medicalVaccin3!.count)+110)
            }else{
                return 90.0
            }
        case 12:
            if (self.medicalProfiles?.count)! > 0{
                return CGFloat(160*(self.medicalProfiles!.count)+150)
            }else{
                return 90.0
            }
            
        default:
            return 0
        }
    }
    
    func selectImagetoPickerView(tapGestureRecognizer: UITapGestureRecognizer)
    {
        var avatarActionSheet:UIAlertController? = nil
        if IS_IPAD{
            avatarActionSheet = UIAlertController(title: nil, message: "Ảnh đại diện", preferredStyle: .alert)
        }else{
            avatarActionSheet = UIAlertController(title: nil, message: "Ảnh đại diện", preferredStyle: .actionSheet)
        }
        
        let cameraAction = UIAlertAction(title: "Chụp ảnh mới", style: .default) { (action) in
            self.picker.sourceType = .camera
            self.picker.cameraCaptureMode = .photo
            self.picker.modalPresentationStyle = .fullScreen
            self.present(self.picker, animated: true, completion: nil)
        }
        let galleryAction = UIAlertAction(title: "Chọn ảnh từ thư viện", style: .default) { (action) in
            self.picker.sourceType = .photoLibrary
            self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.present(self.picker, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Huỷ", style: .cancel, handler: nil)
        avatarActionSheet?.addAction(cameraAction)
        avatarActionSheet?.addAction(galleryAction)
        avatarActionSheet?.addAction(cancelAction)
        
        present(avatarActionSheet!, animated: true, completion: nil)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        imageView.image = image
        layoutImageView()
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        if cropViewController.croppingStyle != .circular {
            imageView.isHidden = true
            
            cropViewController.dismissAnimatedFrom(self, withCroppedImage: image,
                                                   toView: imageView,
                                                   toFrame: CGRect.zero,
                                                   setup: { self.layoutImageView() },
                                                   completion: { self.imageView.isHidden = false })
            self.profileImage.update(image: imageView.image, animated: true)
        }
        else {
            self.imageView.isHidden = false
            cropViewController.dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = (info[UIImagePickerControllerOriginalImage] as? UIImage) else { return }
        let cropController = CropViewController(croppingStyle: croppingStyle, image: image)
        cropController.delegate = self
        self.image = image
        //If profile picture, push onto the same navigation stack
        if croppingStyle == .circular {
            picker.pushViewController(cropController, animated: true)
        }
        else { //otherwise dismiss, and then present from the main controller
            picker.dismiss(animated: true, completion: {
                self.present(cropController, animated: true, completion: nil)
                //self.navigationController!.pushViewController(cropController, animated: true)
            })
        }
    }
    
    
    func updateAvatarHSSK(medicalProfileID:Int){
        let progress = showProgress()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd_hhmmss"
        if let curUser = currentUser, let avatar = UIImage.resizeImage(image:self.imageView.image!, toWidth: 512), let imageData = UIImagePNGRepresentation(avatar) {
            
            let timestamp = dateFormatter.string(from: Date())
            let avatarName = "avatar_medical_profile_\(curUser.id)_\(timestamp)"
            let parameterDic:[String:Any] = [
                "Name": avatarName,
                "MedicalProfileID": medicalProfileID,
                "Base64String": imageData.base64EncodedString(),
                "UserID":curUser.id
            ]
            MedicalHealthFactoryUser().updateAvatarHSSK(parameterDic: parameterDic, success: { (responseString) in
                if responseString.contains("Done") {
                    self.navigationController?.popViewController(animated: true)
                    self.delegate?.reloadListHSSK(userID:curUser.id)
                    self.hideProgress(progress)
                    
                } else {
                    let alertViewController = UIAlertController(title: "Thông báo", message: "Có Lỗi xảy ra!", preferredStyle: .alert)
                    let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                    alertViewController.addAction(noAction)
                    self.present(alertViewController, animated: true, completion: nil)
                }
            }, fail: { (error, response) in
                self.hideProgress(progress)
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changeMaBN(_ sender: Any) {
        self.showUpdateMaBN()
    }
    
    @IBAction func showIndexBMI(_ sender: Any) {
        if let weight = self.medicalProfileObj?.weight, let height = self.medicalProfileObj?.height2{
            if weight != 0 && height != 0{
                self.showBMIAlertView(weight: Float(weight), height: Float(height))
            }else{
                showAlertView(title: "Bạn phải nhập Chiều cao và Cân nặng để có thể xem chỉ số BMI", view: self)
                return
            }
        }else{
            showAlertView(title: "Bạn phải nhập Chiều cao và Cân nặng để có thể xem chỉ số BMI", view: self)
            return
        }
    }
    
    func showBMIAlertView(weight:Float, height:Float){
        let value = height/100*height/100
        let result = Float(weight/value)
        var msg = ""
        if result < 18.5 && result > 0.0{
            msg = "Chỉ số BMI ở trên cho thấy bạn bị gầy. Bạn cần tăng cường chế độ ăn uống và bổ sung dưỡng chất."
        }else if result > 18.5 && result < 24.9{
            msg = "Chúc mừng bạn! Bạn có chỉ số BMI bình thường!"
        }else if result > 25 && result < 29.9{
            msg = "Chỉ số BMI cho thấy bạn bị thừa cân! Bạn cần có chế độ ăn uống hợp lý hơn"
        }else{
            msg = "Chỉ số BMI ở trên cho thấy bạn bị béo phì độ 1! Bạn nên hạn chế ăn đồ mỡ, đồ béo và chế độ ăn uống hợp lý"
        }
        showAlertView(title: msg, view: self)
    }
    
    func calculatorBMI(weight:Float, height:Float){
        let value = height/100*height/100
        let result = Float(weight/value)
        var msg = ""
        if result < 18.5 && result > 0.0{
            msg = String(format: " BMI: %.2f", result)
            indexBMIBtn.setTitleColor(UIColor.red, for: UIControlState.normal)
        }else if result > 18.5 && result < 24.9{
            msg = String(format: " BMI: %.2f", result)
            indexBMIBtn.setTitleColor(UIColor(hexColor: 0x1976D2, alpha: 1.0), for: UIControlState.normal)
        }else if result > 25 && result < 29.9{
            msg = String(format: " BMI: %.2f", result)
            indexBMIBtn.setTitleColor(UIColor.red, for: UIControlState.normal)
        }else{
            msg = String(format: " BMI: %.2f", result)
            indexBMIBtn.setTitleColor(UIColor.red, for: UIControlState.normal)
        }
        
        let indexBMILabel = String.fontAwesomeIcon(name: .infoCircle) + msg
        indexBMIBtn.setTitle(indexBMILabel, for: .normal)
        if IS_IPHONE_5{
            indexBMIBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12.0)
        }
    }
    
    func showUpdateMaBN(){
        let addChangeMaBN = AddChangeMaBN(nibName: "AddChangeMaBN", bundle: nil)
        addChangeMaBN.delegate = self
        if let medicalProfile = healthRecords?.medicalProfile{
            addChangeMaBN.medicalProfile = medicalProfile
        }
        addChangeMaBN.maBN = self.maBN
        addChangeMaBN.modalPresentationStyle = .custom
        addChangeMaBN.transitioningDelegate = self
        addChangeMaBN.view.frame = CGRect(x: screenSizeWidth/2-162, y: screenSizeHeight/2-194, width: 324, height: 350)
        self.present(addChangeMaBN, animated: true, completion: nil)
    }
    
    func resultChangeMaBN(maBN:String){
        self.navigationController?.popViewController(animated: true)
        if let curUser = currentUser{
             self.delegate?.reloadListHSSK(userID:curUser.id)
        }
        //self.maBN = maBN
        //statusText.text = "Mã bệnh nhân: \(maBN)"
        //statusText.textColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        //showAlertView(title: "Đã cập nhật mã bệnh nhân thành công!", view: self)
        Toast(text: "Cập nhật mã bệnh nhân thành công!").show()
    }
    
    func getCilinicalProfile(medicalProfileId:Int){
        let progress = self.showProgress()
        MedicalCilinnical().getCilinicalProfile(medicalProfileID:medicalProfileId, success: { (data) in
            if data.count > 0 {
                self.medicalProfiles?.append(contentsOf: data)
                self.myTableView.reloadData()
                self.hideProgress(progress)
            } else {
                self.hideProgress(progress)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func getAllVaccin(typeID:Int, factoryID:Int){
        let progress = self.showProgress()
        MedicalVaccin().getAllVaccin(typeID: typeID, factoryID:factoryID, success: { (data) in
            if data.count > 0 {
                if typeID == 1{
                    for vaccinObj in data{
                        if vaccinObj.active == true{
                            self.medicalVaccin1?.append(vaccinObj)
                        }
                    }
                }else if typeID == 2{
                    for vaccinObj in data{
                        if vaccinObj.active == true{
                            self.medicalVaccin2?.append(vaccinObj)
                        }
                    }
                }else{
                    for vaccinObj in data{
                        if vaccinObj.active == true{
                            self.medicalVaccin3?.append(vaccinObj)
                        }
                    }
                }
                self.myTableView.reloadData()
                self.hideProgress(progress)
            } else {
                self.hideProgress(progress)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? TiemchungTreemCustomCell else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? TiemchungTreemCustomCell else { return }
        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }

}

extension DetailHealthRecordsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 9:
            return medicalVaccin1!.count
        case 10:
            return medicalVaccin2!.count
        case 11:
            return medicalVaccin3!.count
        case 12:
            return medicalProfiles!.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch collectionView.tag {
        case 9:
            return CGSize(width: Int(screenSizeWidth), height: 120)
        case 10:
            return CGSize(width: Int(screenSizeWidth), height: 120)
        case 11:
            return CGSize(width: Int(screenSizeWidth), height: 140)
        case 12:
            return CGSize(width: Int(screenSizeWidth), height: 150)
        default:
            return CGSize(width: 0, height: 0)
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView.tag {
        case 9:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! VacxinCustomViewCell
            cell.btnMore.tag = indexPath.row
            cell.btnMore.accessibilityHint = "\(collectionView.tag)"
            cell.btnMore.addTarget(self, action: #selector(DetailHealthRecordsVC.handleDetailMore), for: .touchUpInside)
            
            let medicalVaccinObj = self.medicalVaccin1?[indexPath.row]
            
            if let type = medicalVaccinObj?.vaccinName{
                cell.lblLoaiVx.text = type
            }
            if let date = medicalVaccinObj?.vaccinDate{
                let result = resultDateFormatStandard(str: date)
                cell.lblNgaytiem.text = result
            }
            if let react = medicalVaccinObj?.vaccinReact{
                if !react.isEmpty{
                    cell.lblPUSauTiem.text = react
                }else{
                    cell.lblPUSauTiem.text = "Không có phản ứng nào"
                }
            }
            if let schedule = medicalVaccinObj?.vaccinSchedule{
                let result = resultDateFormatStandard(str: schedule)
                if !result.isEmpty{
                    cell.lblNgayHentiem.text = result
                }else{
                    cell.lblNgayHentiem.text = "Không có dữ liệu"
                }
            }
            
            return cell
        case 10:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! VacxinCustomViewCell
            cell.btnMore.tag = indexPath.row
            cell.btnMore.accessibilityHint = "\(collectionView.tag)"
            cell.btnMore.addTarget(self, action: #selector(DetailHealthRecordsVC.handleDetailMore), for: .touchUpInside)
            
            let medicalVaccinObj = self.medicalVaccin2?[indexPath.row]
            if let type = medicalVaccinObj?.vaccinName{
                cell.lblLoaiVx.text = type
            }
            if let date = medicalVaccinObj?.vaccinDate{
                let result = resultDateFormatStandard(str: date)
                cell.lblNgaytiem.text = result
            }
            if let react = medicalVaccinObj?.vaccinReact{
                if !react.isEmpty{
                    cell.lblPUSauTiem.text = react
                }else{
                    cell.lblPUSauTiem.text = "Không có phản ứng nào"
                }
            }
            if let schedule = medicalVaccinObj?.vaccinSchedule{
                let result = resultDateFormatStandard(str: schedule)
                if !result.isEmpty{
                    cell.lblNgayHentiem.text = result
                }else{
                    cell.lblNgayHentiem.text = "Không có dữ liệu"
                }
            }
            
            return cell
        case 11:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! VacxinCustomViewCell
            cell.btnMore.tag = indexPath.row
            cell.btnMore.accessibilityHint = "\(collectionView.tag)"
            cell.btnMore.addTarget(self, action: #selector(DetailHealthRecordsVC.handleDetailMore), for: .touchUpInside)
            
            let medicalVaccinObj = self.medicalVaccin3?[indexPath.row]
            if let type = medicalVaccinObj?.vaccinName{
                cell.lblLoaiVx.text = type
            }
            if let date = medicalVaccinObj?.vaccinDate{
                let result = resultDateFormatStandard(str: date)
                cell.lblNgaytiem.text = result
            }
            if let react = medicalVaccinObj?.vaccinReact{
                if !react.isEmpty{
                    cell.lblPUSauTiem.text = react
                }else{
                    cell.lblPUSauTiem.text = "Không có phản ứng nào"
                }
            }
            if let schedule = medicalVaccinObj?.vaccinSchedule{
                let result = resultDateFormatStandard(str: schedule)
                if !result.isEmpty{
                    cell.lblNgayHentiem.text = result
                }else{
                    cell.lblNgayHentiem.text = "Không có dữ liệu"
                }
            }
            if let monthPreg = medicalVaccinObj?.monthPreg{
                cell.lblThangthai.text = "\(monthPreg)"
            }
            
            return cell
        case 12:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LSCell", for: indexPath) as! KhamLSCustomViewCell
            cell.btnMore.tag = indexPath.row
            cell.btnMore.accessibilityHint = "\(collectionView.tag)"
            cell.btnMore.addTarget(self, action: #selector(DetailHealthRecordsVC.handleDetailMore), for: .touchUpInside)
            
            let medicalProfileObj = self.medicalProfiles?[indexPath.row]
            if let chanDoan = medicalProfileObj?.chanDoan{
                cell.lblChuandoan.text = chanDoan
            }
            if let dateCilin = medicalProfileObj?.dateCilin{
                let result = resultDateFormatStandard(str: dateCilin)
                if !result.isEmpty{
                    cell.lblNgaykham.text = result
                }else{
                    cell.lblNgaykham.text = "Không có dữ liệu"
                }
            }
            if let bacSiName = medicalProfileObj?.bacSiName{
                cell.lblBsKham.text = bacSiName
            }
            if let tuVan = medicalProfileObj?.tuVan{
                cell.lblTuvan.text = tuVan
            }
            if let schedule = medicalProfileObj?.dateSchedule{
                let result = resultDateFormatStandard(str: schedule)
                if !result.isEmpty{
                    cell.lblNgayhenkham.text = result
                }else{
                    cell.lblNgayhenkham.text = "Không có dữ liệu"
                }
            }
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! VacxinCustomViewCell
            return cell
       }
    }
    
    func handleDetailMore(sender: UIButton){
        var alertController:UIAlertController? = nil
        if IS_IPAD{
            alertController = UIAlertController(title: nil, message: "Tuỳ chọn", preferredStyle: .alert)
        }else{
            alertController = UIAlertController(title: nil, message: "Tuỳ chọn", preferredStyle: .actionSheet)
        }
        
        alertController?.popoverPresentationController?.sourceView = sender
        alertController?.popoverPresentationController?.sourceRect = sender.frame
        let index = sender.tag
        let section:Int = Int(sender.accessibilityHint!)!
        
        alertController?.addAction(UIAlertAction(title: "Xoá", style: .default, handler: { _ in
            let alert = UIAlertController(title: "Thông báo", message: "Bạn có chắc chắn muốn xoá bản ghi này không?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
                let medicalHealthFactoryUser = MedicalHealthFactoryUser()
                medicalHealthFactoryUser.medicalProfile =  self.medicalProfileObj
                let progress = self.showProgress()
                if section == 9{
                    let medicalVaccin = self.medicalVaccin1![index]
                    let medicalVaccinObj = MedicalVaccin()
                    medicalVaccinObj.vaccinID = medicalVaccin.vaccinID
                    medicalVaccinObj.vaccinTypeID = medicalVaccin.vaccinTypeID
                    medicalVaccinObj.vaccinName = medicalVaccin.vaccinName
                    medicalVaccinObj.vaccinDate = medicalVaccin.vaccinDate
                    medicalVaccinObj.vaccinSchedule = medicalVaccin.vaccinSchedule
                    medicalVaccinObj.vaccinReact = medicalVaccin.vaccinReact
                    medicalVaccinObj.active = false
                   
                    if let medicalProfileID = medicalHealthFactoryUser.medicalProfile?.medicalProfileID{
                        MedicalVaccin().addEditMedicalVaccin(vaccinTypeID:1, medicalVaccin:medicalVaccinObj, medicalProfileID:medicalProfileID, success: {(result) in
                            if result != nil{
                                self.hideProgress(progress)
                                self.medicalVaccin1?.remove(at: index)
                                self.myTableView.reloadSections(IndexSet(integer: section), with: .automatic)
                            }else{
                                self.hideProgress(progress)
                            }
                        }, fail: { (error, response) in
                            self.hideProgress(progress)
                            self.handleNetworkError(error: error, responseData: response.data)
                        })
                    }
                }else if section == 10{
                    let medicalVaccin = self.medicalVaccin2![index]
                    let medicalVaccinObj = MedicalVaccin()
                    medicalVaccinObj.vaccinID = medicalVaccin.vaccinID
                    medicalVaccinObj.vaccinTypeID = medicalVaccin.vaccinTypeID
                    medicalVaccinObj.vaccinName = medicalVaccin.vaccinName
                    medicalVaccinObj.vaccinDate = medicalVaccin.vaccinDate
                    medicalVaccinObj.vaccinSchedule = medicalVaccin.vaccinSchedule
                    medicalVaccinObj.vaccinReact = medicalVaccin.vaccinReact
                    medicalVaccinObj.active = false
                    
                    if let medicalProfileID = medicalHealthFactoryUser.medicalProfile?.medicalProfileID{
                        MedicalVaccin().addEditMedicalVaccin(vaccinTypeID:2, medicalVaccin:medicalVaccinObj, medicalProfileID:medicalProfileID, success: {(result) in
                            if result != nil{
                                self.hideProgress(progress)
                                self.medicalVaccin2?.remove(at: index)
                                self.myTableView.reloadSections(IndexSet(integer: section), with: .automatic)
                            }else{
                                self.hideProgress(progress)
                            }
                        }, fail: { (error, response) in
                            self.hideProgress(progress)
                            self.handleNetworkError(error: error, responseData: response.data)
                        })
                    }
                }else if section == 11{
                    let medicalVaccin = self.medicalVaccin3![index]
                    let medicalVaccinObj = MedicalVaccin()
                    medicalVaccinObj.vaccinID = medicalVaccin.vaccinID
                    medicalVaccinObj.vaccinTypeID = medicalVaccin.vaccinTypeID
                    medicalVaccinObj.vaccinName = medicalVaccin.vaccinName
                    medicalVaccinObj.vaccinDate = medicalVaccin.vaccinDate
                    medicalVaccinObj.vaccinSchedule = medicalVaccin.vaccinSchedule
                    medicalVaccinObj.vaccinReact = medicalVaccin.vaccinReact
                    medicalVaccinObj.monthPreg = medicalVaccin.monthPreg
                    medicalVaccinObj.active = false
                    
                    if let medicalProfileID = medicalHealthFactoryUser.medicalProfile?.medicalProfileID{
                        MedicalVaccin().addEditMedicalVaccin(vaccinTypeID:3, medicalVaccin:medicalVaccinObj, medicalProfileID:medicalProfileID, success: {(result) in
                            if result != nil{
                                self.hideProgress(progress)
                                self.medicalVaccin3?.remove(at: index)
                                self.myTableView.reloadSections(IndexSet(integer: section), with: .automatic)
                            }else{
                                self.hideProgress(progress)
                            }
                        }, fail: { (error, response) in
                            self.hideProgress(progress)
                            self.handleNetworkError(error: error, responseData: response.data)
                        })
                    }
                }else{
                    let medicalCilinnical = self.medicalProfiles![index]
                    let medicalCilinnicalObj = MedicalCilinnical()
                    medicalCilinnicalObj.cilinnicID = medicalCilinnical.cilinnicID
                    medicalCilinnicalObj.dateCilin = medicalCilinnical.dateCilin
                    medicalCilinnicalObj.dateSchedule = medicalCilinnical.dateSchedule
                    medicalCilinnicalObj.historyCilin = medicalCilinnical.historyCilin
                    medicalCilinnicalObj.mach = medicalCilinnical.mach
                    medicalCilinnicalObj.nhietDo = medicalCilinnical.nhietDo
                    medicalCilinnicalObj.ha = medicalCilinnical.ha
                    medicalCilinnicalObj.nhipTho = medicalCilinnical.nhipTho
                    medicalCilinnicalObj.weight = medicalCilinnical.weight
                    medicalCilinnicalObj.height = medicalCilinnical.height
                    medicalCilinnicalObj.bMI = medicalCilinnical.bMI
                    medicalCilinnicalObj.vongBung = medicalCilinnical.vongBung
                    medicalCilinnicalObj.leftEye = medicalCilinnical.leftEye
                    medicalCilinnicalObj.rightEye = medicalCilinnical.rightEye
                    medicalCilinnicalObj.leftEyeGlass = medicalCilinnical.leftEyeGlass
                    medicalCilinnicalObj.rightEyeGlass = medicalCilinnical.rightEyeGlass
                    medicalCilinnicalObj.daNiemMac = medicalCilinnical.daNiemMac
                    medicalCilinnicalObj.toanThanNote = medicalCilinnical.toanThanNote
                    medicalCilinnicalObj.timMach = medicalCilinnical.timMach
                    medicalCilinnicalObj.hoHap = medicalCilinnical.hoHap
                    medicalCilinnicalObj.tieuHoa = medicalCilinnical.tieuHoa
                    medicalCilinnicalObj.tietNieu = medicalCilinnical.tietNieu
                    medicalCilinnicalObj.coXuongKhop = medicalCilinnical.coXuongKhop
                    medicalCilinnicalObj.noiTiet = medicalCilinnical.noiTiet
                    medicalCilinnicalObj.thanKinh = medicalCilinnical.thanKinh
                    medicalCilinnicalObj.tamThan = medicalCilinnical.tamThan
                    medicalCilinnicalObj.ngoaiKhoa = medicalCilinnical.ngoaiKhoa
                    medicalCilinnicalObj.sanPhuKhoa = medicalCilinnical.sanPhuKhoa
                    medicalCilinnicalObj.taiMuiHong = medicalCilinnical.taiMuiHong
                    medicalCilinnicalObj.rangHamMat = medicalCilinnical.rangHamMat
                    medicalCilinnicalObj.mat = medicalCilinnical.mat
                    medicalCilinnicalObj.daLieu = medicalCilinnical.daLieu
                    medicalCilinnicalObj.dinhDuong = medicalCilinnical.dinhDuong
                    medicalCilinnicalObj.vanDong = medicalCilinnical.vanDong
                    medicalCilinnicalObj.danhGia = medicalCilinnical.danhGia
                    medicalCilinnicalObj.coQuanKhac = medicalCilinnical.coQuanKhac
                    medicalCilinnicalObj.huyetHoc = medicalCilinnical.huyetHoc
                    medicalCilinnicalObj.sinhHoaMau = medicalCilinnical.sinhHoaMau
                    medicalCilinnicalObj.sinhHoaNuocTieu = medicalCilinnical.sinhHoaNuocTieu
                    medicalCilinnicalObj.sieuAmOBung = medicalCilinnical.sieuAmOBung
                    medicalCilinnicalObj.chanDoan = medicalCilinnical.chanDoan
                    medicalCilinnicalObj.tuVan = medicalCilinnical.tuVan
                    medicalCilinnicalObj.bacSiName = medicalCilinnical.bacSiName
                    medicalCilinnicalObj.active = false
                    
                    if let medicalProfileID =  medicalHealthFactoryUser.medicalProfile?.medicalProfileID{
                    MedicalCilinnical().addEditMedicalCilinnical(medicalCilinnical:medicalCilinnicalObj, medicalProfileID:medicalProfileID, success: {(result) in
                            if result != nil{
                                self.hideProgress(progress)
                                self.medicalProfiles?.remove(at: index)
                                self.myTableView.reloadSections(IndexSet(integer: section), with: .automatic)
                            }else{
                                self.hideProgress(progress)
                            }
                        }, fail: { (error, response) in
                            self.hideProgress(progress)
                            self.handleNetworkError(error: error, responseData: response.data)
                        })
                    }
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "Huỷ", style: UIAlertActionStyle.default, handler: {    (action:UIAlertAction!) in
                
            }))
            
            alertController?.popoverPresentationController?.sourceView = self.view
            self.present(alert, animated: true, completion: nil)
        }))
        
        alertController?.addAction(UIAlertAction(title: "Chỉnh sửa", style: .default, handler: { _ in
            if section == 9{
                let medicalVaccinObj:MedicalVaccin = self.medicalVaccin1![index]
                self.editMuitiemInSection(section:section, index:index, medicalVaccin:medicalVaccinObj)
            }else if section == 10{
                let medicalVaccinObj:MedicalVaccin = self.medicalVaccin2![index]
                self.editMuitiemInSection(section:section, index:index, medicalVaccin:medicalVaccinObj)
            }else if section == 11{
                let medicalVaccinObj:MedicalVaccin = self.medicalVaccin3![index]
                self.editMuitiemInSection(section:section, index:index, medicalVaccin:medicalVaccinObj)
            }else{
                let medicalCilinnicalObj:MedicalCilinnical = self.medicalProfiles![index]
                MedicalHealthFactoryUser.sharedInstance.medicalCilinnicalObj = medicalCilinnicalObj
                self.editLamsangnSection(index:index, medicalCilinnical:medicalCilinnicalObj)
            }
        }))
        alertController?.addAction(UIAlertAction(title: "Huỷ", style: .cancel, handler: { _ in
            
        }))
        present(alertController!, animated: false, completion: nil)
    }
    
    func editMuitiemInSection(section:Int, index:Int, medicalVaccin:MedicalVaccin){
        let addMuitiemVC = AddMuitiemVC(nibName: "AddMuitiemVC", bundle: nil)
        addMuitiemVC.delegate = self
        addMuitiemVC.currentVacxinIdx = self.currentVacxinIdx!
        addMuitiemVC.currentTag = section
        addMuitiemVC.currentIndex = index
        addMuitiemVC.isUpdateItem = true
        addMuitiemVC.medicalVaccin = medicalVaccin
        addMuitiemVC.modalPresentationStyle = .custom
        addMuitiemVC.transitioningDelegate = self
        addMuitiemVC.view.frame = CGRect(x: screenSizeWidth/2-162, y: screenSizeHeight/2-194, width: 324, height: 388)
        self.present(addMuitiemVC, animated: true, completion: nil)
    }
    
    func editLamsangnSection(index:Int, medicalCilinnical:MedicalCilinnical){
        let lamsangVC = LamsangVC(nibName: "LamsangVC", bundle: nil)
        lamsangVC.delegate = self
        lamsangVC.currentIndex = index
        lamsangVC.isUpdateItem = true
        lamsangVC.medicalCilinnical = medicalCilinnical
        self.navigationController?.pushViewController(lamsangVC, animated: true)
    }
    
    public func layoutImageView() {
        guard imageView.image != nil else { return }
        let padding: CGFloat = 20.0
        
        var viewFrame = self.view.bounds
        viewFrame.size.width -= (padding * 2.0)
        viewFrame.size.height -= ((padding * 2.0))
        
        var imageFrame = CGRect.zero
        imageFrame.size = imageView.image!.size;
        
        if imageView.image!.size.width > viewFrame.size.width || imageView.image!.size.height > viewFrame.size.height {
            let scale = min(viewFrame.size.width / imageFrame.size.width, viewFrame.size.height / imageFrame.size.height)
            imageFrame.size.width *= scale
            imageFrame.size.height *= scale
            imageFrame.origin.x = (self.view.bounds.size.width - imageFrame.size.width) * 0.5
            imageFrame.origin.y = (self.view.bounds.size.height - imageFrame.size.height) * 0.5
            imageView.frame = imageFrame
        }
        else {
            self.imageView.frame = imageFrame;
            self.imageView.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
        }
    }
    
}

class HalfSizePresentationController2 : UIPresentationController {
    override var frameOfPresentedViewInContainerView: CGRect {
        return CGRect(x: 0, y: 0, width: 300, height: 300)
    }
}

