//
//  HealthRecordsCell.swift
//  iCNM
//
//  Created by Mac osx on 8/22/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
protocol MuitiemCustomCellDelegate {
    func resultMedicalHealthFactory(medicalHealthFactory:MedicalHealthFactory)
}
class MuitiemCustomCell: UITableViewCell, UITextFieldDelegate {
    @IBOutlet var txtIssueNote: UITextField!
    var delegate: MuitiemCustomCellDelegate?
    var medicalHealFactory = MedicalHealthFactoryUser.sharedInstance.medicalHealFactoriesObj
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtIssueNote.delegate = self
        let gesture = UITapGestureRecognizer(target: self, action: #selector(MuitiemCustomCell.didSelectCell))
        addGestureRecognizer(gesture)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

// MARK: - Actions
extension MuitiemCustomCell {
    func didSelectCell() {
        //txtIssueNote.becomeFirstResponder()
    }
    
    @IBAction func textFieldValueChanged(_ sender: UITextField) {
        if let result = sender.text {
            if sender.tag == 0{
                if !result.isEmpty{
                    medicalHealFactory.note = result.trim()
                }else{
                    medicalHealFactory.note = ""
                }
            }
            
        delegate?.resultMedicalHealthFactory(medicalHealthFactory:medicalHealFactory)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtIssueNote{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else{
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
