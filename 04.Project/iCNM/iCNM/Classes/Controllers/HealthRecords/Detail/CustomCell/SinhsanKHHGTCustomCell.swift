//
//  HealthRecordsCell.swift
//  iCNM
//
//  Created by Mac osx on 8/22/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
protocol SinhsanKHHGTCustomCellDelegate {
    func resultMedicalHealthFactory(medicalHealthFactory:MedicalHealthFactory)
}
class SinhsanKHHGTCustomCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet var lblKythaicuoi: FWFloatingLabelTextField!
    @IBOutlet var lblBenhPhukhoa: FWFloatingLabelTextField!
    @IBOutlet var lblSocon: FWFloatingLabelTextField!
    @IBOutlet var lblSolanDeNon: FWFloatingLabelTextField!
    @IBOutlet var lblSolanDeDT: FWFloatingLabelTextField!
    @IBOutlet var lblDekho: FWFloatingLabelTextField!
    @IBOutlet var lblDemo: FWFloatingLabelTextField!
    @IBOutlet var lblDethuong: FWFloatingLabelTextField!
    @IBOutlet var lblSolanSD: FWFloatingLabelTextField!
    @IBOutlet var lblSolanphathai: FWFloatingLabelTextField!
    @IBOutlet var lblSolanxaythai: FWFloatingLabelTextField!
    @IBOutlet var lblSolancothai: FWFloatingLabelTextField!
    @IBOutlet var lblBPTranhthai: FWFloatingLabelTextField!
    
    var delegate: SinhsanKHHGTCustomCellDelegate?
    var medicalHealFactory = MedicalHealthFactoryUser.sharedInstance.medicalHealFactoriesObj
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblKythaicuoi.delegate = self
        lblBenhPhukhoa.delegate = self
        lblSocon.delegate = self
        lblSolanDeNon.delegate = self
        lblSolanDeDT.delegate = self
        lblDekho.delegate = self
        lblDemo.delegate = self
        lblDethuong.delegate = self
        lblSolanSD.delegate = self
        lblSolanphathai.delegate = self
        lblSolanxaythai.delegate = self
        lblSolancothai.delegate = self
        lblBPTranhthai.delegate = self
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(SinhsanKHHGTCustomCell.didSelectCell))
        addGestureRecognizer(gesture)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

// MARK: - Actions
extension SinhsanKHHGTCustomCell {
    func didSelectCell() {
//        lblKythaicuoi.becomeFirstResponder()
//        lblBenhPhukhoa.becomeFirstResponder()
//        lblSocon.becomeFirstResponder()
//        lblSolanDeNon.becomeFirstResponder()
//        lblSolanDeDT.becomeFirstResponder()
//        lblDekho.becomeFirstResponder()
//        lblDemo.becomeFirstResponder()
//        lblDethuong.becomeFirstResponder()
//        lblSolanSD.becomeFirstResponder()
//        lblSolanphathai.becomeFirstResponder()
//        lblSolanxaythai.becomeFirstResponder()
//        lblSolancothai.becomeFirstResponder()
//        lblBPTranhthai.becomeFirstResponder()
    }
    
    @IBAction func textFieldInTableViewCell_SKCN(_ sender: UITextField) {
        if let result = sender.text {
            switch sender.tag {
            case 0:
                if !result.isEmpty{
                    medicalHealFactory.pPTranhThaiDD = result.trim()
                }else{
                    medicalHealFactory.pPTranhThaiDD = ""
                }
            case 1:
                if !result.isEmpty{
                    medicalHealFactory.kyThaiCC = result.trim()
                }else{
                    medicalHealFactory.kyThaiCC = ""
                }
            case 2:
                if !result.isEmpty{
                    let num = Int(result)
                    medicalHealFactory.coThai = num!
                    
                }else{
                    
                }
            case 3:
                if !result.isEmpty{
                    let num = Int(result)
                    medicalHealFactory.sayThai = num!
                    
                }else{
                   
                }
            case 4:
                if !result.isEmpty{
                    let num = Int(result)
                    medicalHealFactory.phaThai = num!
                    
                }else{
                    
                }
            case 5:
                if !result.isEmpty{
                    let num = Int(result)
                    medicalHealFactory.sinhDe = num!
                    
                }else{
                    
                }
            case 6:
                if !result.isEmpty{
                    let num = Int(result)
                    medicalHealFactory.deThuongNum = num!
                    
                }else{
                    
                }
            case 7:
                if !result.isEmpty{
                    let num = Int(result)
                    medicalHealFactory.deMoNum = num!
                    
                }else{
                   
                }
            case 8:
                if !result.isEmpty{
                    let num = Int(result)
                    medicalHealFactory.deKho = num!
                    
                }else{
                    
                }
            case 9:
                if !result.isEmpty{
                    let num = Int(result)
                    medicalHealFactory.deDuThang = num!
                    
                }else{
                    
                }
            case 10:
                if !result.isEmpty{
                    let num = Int(result)
                    medicalHealFactory.deNon = num!
                    
                }else{
                   
                }
            case 11:
                if !result.isEmpty{
                    let num = Int(result)
                    medicalHealFactory.soConHienSong = num!
                    
                }else{
                    
                }
            case 12:
                if !result.isEmpty{
                    medicalHealFactory.benhPhuKhoa = result.trim()
                }else{
                    medicalHealFactory.benhPhuKhoa = ""
                }
            default:
                break
            }
        delegate?.resultMedicalHealthFactory(medicalHealthFactory:medicalHealFactory)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == lblBPTranhthai{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == lblKythaicuoi{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == lblBenhPhukhoa{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == lblSocon{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 2
        }else if textField == lblSolanDeNon{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 2
        }else if textField == lblSolanDeDT{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 2
        }else if textField == lblDekho{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 2
        }else if textField == lblDemo{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 2
        }else if textField == lblDethuong{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 2
        }else if textField == lblSolanSD{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 2
        }else if textField == lblSolanphathai{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 2
        }else if textField == lblSolanxaythai{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 2
        }else if textField == lblSolancothai{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 2
        }else{
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
}

