//
//  HealthRecordsCell.swift
//  iCNM
//
//  Created by Mac osx on 8/22/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import BEMCheckBox
protocol TiensubenhtatCustomCellDelegate {
    func resultMedicalHealthFactory(medicalHealthFactory:MedicalHealthFactory)
}
class TiensubenhtatCustomCell: UITableViewCell, BEMCheckBoxDelegate, UITextFieldDelegate {

    @IBOutlet var lblThuoc: FWFloatingLabelTextField!
    @IBOutlet var lblDiung: FWFloatingLabelTextField!
    @IBOutlet var lblThucpham: FWFloatingLabelTextField!
    @IBOutlet var lblHoachatMP: FWFloatingLabelTextField!
    @IBOutlet var lblUngthu: FWFloatingLabelTextField!
    @IBOutlet var lblLao: FWFloatingLabelTextField!
    @IBOutlet var lblBenhkhac: FWFloatingLabelTextField!
    
    @IBOutlet var ckTimmach: BEMCheckBox!
    @IBOutlet var ckBenhphoi: BEMCheckBox!
    @IBOutlet var ckDaday: BEMCheckBox!
    @IBOutlet var ckDaiTD: BEMCheckBox!
    @IBOutlet var ckBenhTHA: BEMCheckBox!
    
    @IBOutlet var ckBenhVG: BEMCheckBox!
    @IBOutlet var ckBenhBuouco: BEMCheckBox!
    @IBOutlet var ckHenxuyen: BEMCheckBox!
    
    @IBOutlet var ckDongkinh: BEMCheckBox!
    @IBOutlet var ckTuky: BEMCheckBox!
    @IBOutlet var ckTamthan: BEMCheckBox!
    @IBOutlet var ckTimBS: BEMCheckBox!
    
    var delegate: TiensubenhtatCustomCellDelegate?
    var medicalHealFactory = MedicalHealthFactoryUser.sharedInstance.medicalHealFactoriesObj
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ckTimmach.delegate = self
        ckBenhphoi.delegate = self
        ckDaday.delegate = self
        ckDaiTD.delegate = self
        ckBenhTHA.delegate = self
        ckBenhVG.delegate = self
        ckBenhBuouco.delegate = self
        ckHenxuyen.delegate = self
        ckDongkinh.delegate = self
        ckTuky.delegate = self
        ckTamthan.delegate = self
        ckTimBS.delegate = self
        
        lblThuoc.delegate = self
        lblDiung.delegate = self
        lblThucpham.delegate = self
        lblHoachatMP.delegate = self
        lblUngthu.delegate = self
        lblLao.delegate = self
        lblBenhkhac.delegate = self
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(TiensubenhtatCustomCell.didSelectCell))
        addGestureRecognizer(gesture)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func didTap(_ checkBox: BEMCheckBox){
        switch checkBox.tag {
        case 4:
            medicalHealFactory.benhTimMach = checkBox.on
        case 5:
            medicalHealFactory.tangHuyetAp = checkBox.on
        case 6:
            medicalHealFactory.daiThaoDuong = checkBox.on
        case 7:
            medicalHealFactory.benhDaDay = checkBox.on
        case 8:
            medicalHealFactory.benhPhoiManTinh = checkBox.on
        case 9:
            medicalHealFactory.henSuyen = checkBox.on
        case 10:
            medicalHealFactory.benhBuouCo = checkBox.on
        case 11:
            medicalHealFactory.viemGan = checkBox.on
        case 12:
            medicalHealFactory.timBamSinh = checkBox.on
        case 13:
            medicalHealFactory.tamThan = checkBox.on
        case 14:
            medicalHealFactory.tuKy = checkBox.on
        case 15:
            medicalHealFactory.dongKinh = checkBox.on
        default:
            break
        }
        
        delegate?.resultMedicalHealthFactory(medicalHealthFactory:medicalHealFactory)
    }
}

// MARK: - Actions
extension TiensubenhtatCustomCell {
    func didSelectCell() {
//        lblThuoc.becomeFirstResponder()
//        lblDiung.becomeFirstResponder()
//        lblThucpham.becomeFirstResponder()
//        lblHoachatMP.becomeFirstResponder()
//        lblUngthu.becomeFirstResponder()
//        lblLao.becomeFirstResponder()
//        lblBenhkhac.becomeFirstResponder()
    }
    
    @IBAction func textFieldInTableViewCell_SKCN(_ sender: UITextField) {
        if let result = sender.text {
            switch sender.tag {
            case 0:
                if !result.isEmpty{
                    medicalHealFactory.diUngThuoc = result.trim()
                }else{
                   medicalHealFactory.diUngThuoc = ""
                }
            case 1:
                if !result.isEmpty{
                    medicalHealFactory.duHoaChatMP = result.trim()
                }else{
                    medicalHealFactory.duHoaChatMP = ""
                }
            case 2:
                if !result.isEmpty{
                    medicalHealFactory.duThucPham = result.trim()
                }else{
                    medicalHealFactory.duThucPham = ""
                }
            case 3:
                if !result.isEmpty{
                    medicalHealFactory.duNote = result.trim()
                }else{
                    medicalHealFactory.duNote = ""
                }
            case 16:
                if !result.isEmpty{
                    medicalHealFactory.ungThu = result.trim()
                }else{
                    medicalHealFactory.ungThu = ""
                }
            case 17:
                if !result.isEmpty{
                    medicalHealFactory.lao = result.trim()
                }else{
                   medicalHealFactory.lao = ""
                }
            case 18:
                if !result.isEmpty{
                    medicalHealFactory.benhNote = result.trim()
                }else{
                   medicalHealFactory.benhNote = ""
                }
            default:
                break
            }
            
            delegate?.resultMedicalHealthFactory(medicalHealthFactory:medicalHealFactory)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == lblThuoc{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == lblHoachatMP{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == lblThucpham{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == lblUngthu{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == lblLao{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == lblBenhkhac{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 100
        }else{
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
