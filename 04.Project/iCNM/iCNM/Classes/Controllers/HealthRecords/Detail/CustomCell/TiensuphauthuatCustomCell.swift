//
//  HealthRecordsCell.swift
//  iCNM
//
//  Created by Mac osx on 8/22/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
protocol TiensuphauthuatCustomCellDelegate {
    func resultMedicalHealthFactory(medicalHealthFactory:MedicalHealthFactory)
}
class TiensuphauthuatCustomCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet var lblTiensuPT: FWFloatingLabelTextField!
    var delegate: TiensuphauthuatCustomCellDelegate?
    var medicalHealFactory = MedicalHealthFactoryUser.sharedInstance.medicalHealFactoriesObj
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTiensuPT.delegate = self
        let gesture = UITapGestureRecognizer(target: self, action: #selector(TiensuphauthuatCustomCell.didSelectCell))
        addGestureRecognizer(gesture)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

// MARK: - Actions
extension TiensuphauthuatCustomCell {
    func didSelectCell() {
        //lblTiensuPT.becomeFirstResponder()
    }
    
    @IBAction func textFieldInTableViewCell_SKCN(_ sender: UITextField) {
        if let result = sender.text {
            if sender.tag == 0{
                if !result.isEmpty{
                    medicalHealFactory.tienSuPhauThuat = result.trim()
                }else{
                    medicalHealFactory.tienSuPhauThuat = ""
                }
            }
            
            delegate?.resultMedicalHealthFactory(medicalHealthFactory:medicalHealFactory)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == lblTiensuPT{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else{
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
