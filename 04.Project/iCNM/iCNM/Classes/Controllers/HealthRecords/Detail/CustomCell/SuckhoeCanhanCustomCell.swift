//
//  HealthRecordsCell.swift
//  iCNM
//
//  Created by Mac osx on 8/22/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import BEMCheckBox
import QuartzCore
protocol SuckhoeCanhanCustomCellDelegate {
    func resultMedicalHealthFactory(medicalHealthFactory:MedicalHealthFactory)
}

class SuckhoeCanhanCustomCell: UITableViewCell, BEMCheckBoxDelegate, UITextFieldDelegate,UITextViewDelegate {

    @IBOutlet var Hutthuoc: BEMCheckBox!
    @IBOutlet var Hutthuoc_Dabo: BEMCheckBox!
    @IBOutlet var Hutthuoc_TX: BEMCheckBox!
    
    @IBOutlet var uongruou_dabo: BEMCheckBox!
    @IBOutlet var uongruou_Num: UITextField!
    @IBOutlet var Uongruou: BEMCheckBox!
    
    @IBOutlet var matuy: BEMCheckBox!
    @IBOutlet var matuy_dabo: BEMCheckBox!
    @IBOutlet var mathuy_Tx: BEMCheckBox!
    
    @IBOutlet var Hoatdong_Dabo: BEMCheckBox!
    @IBOutlet var Hoatdong_TX: BEMCheckBox!
    @IBOutlet var Hoatdong: BEMCheckBox!
    
    @IBOutlet var lblOther: UITextView!
    @IBOutlet var lblLoaiHX: FWFloatingLabelTextField!
    @IBOutlet var lblTimeTX: FWFloatingLabelTextField!
    @IBOutlet var lblhoachat: FWFloatingLabelTextField!
    @IBOutlet var labelOther: UILabel!
    
    var delegate: SuckhoeCanhanCustomCellDelegate?
    var medicalHealFactory = MedicalHealthFactoryUser.sharedInstance.medicalHealFactoriesObj
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Hutthuoc.delegate = self
        Hutthuoc_Dabo.delegate = self
        Hutthuoc_TX.delegate = self
        uongruou_dabo.delegate = self
        Uongruou.delegate = self
        matuy.delegate = self
        matuy_dabo.delegate = self
        mathuy_Tx.delegate = self
        Hoatdong_Dabo.delegate = self
        Hoatdong_TX.delegate = self
        Hoatdong.delegate = self
        
        uongruou_Num.delegate = self
        lblhoachat.delegate = self
        lblLoaiHX.delegate = self
        lblTimeTX.delegate = self
        lblhoachat.delegate = self
        lblOther.delegate = self
        
        lblOther.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        lblOther.layer.borderWidth = 1.0
        lblOther.layer.cornerRadius = 4
        lblOther.layer.masksToBounds = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(SuckhoeCanhanCustomCell.didSelectCell))
        addGestureRecognizer(gesture)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func didTap(_ checkBox: BEMCheckBox){
        switch checkBox.tag {
        case 0:
            medicalHealFactory.hutThuoc = checkBox.on
        case 1:
            medicalHealFactory.ttHutThuoc = checkBox.on
        case 2:
            medicalHealFactory.boHutThuoc = checkBox.on
        case 3:
            medicalHealFactory.ruouBia = checkBox.on
        case 4:
            medicalHealFactory.boRuouBia = checkBox.on
        case 5:
            medicalHealFactory.maTuy = checkBox.on
        case 6:
            medicalHealFactory.maTuyNgay = checkBox.on
        case 7:
            medicalHealFactory.boMaTuy = checkBox.on
        case 8:
            medicalHealFactory.theDuc = checkBox.on
        case 9:
            medicalHealFactory.theDucNgay = checkBox.on
        case 10:
            medicalHealFactory.nghiTheDuc = checkBox.on
        default:
            break
        }
        
        delegate?.resultMedicalHealthFactory(medicalHealthFactory:medicalHealFactory)
    }
}

// MARK: - Actions
extension SuckhoeCanhanCustomCell {
    func didSelectCell() {
//        uongruou_Num.becomeFirstResponder()
//        lblLoaiHX.becomeFirstResponder()
//        lblTimeTX.becomeFirstResponder()
//        lblOther.becomeFirstResponder()
//        lblhoachat.becomeFirstResponder()
    }
    
    @IBAction func textFieldInTableViewCell(_ sender: UITextField) {
        if let result = sender.text {
            switch sender.tag {
            case 11:
                if !result.isEmpty{
                    let num = Int(result)
                    medicalHealFactory.ruouBiaNgay = num!
                }else{
                    medicalHealFactory.ruouBiaNgay = 0
                }
            case 12:
                if !result.isEmpty{
                    medicalHealFactory.duHoaChatMP = result
                }else{
                    medicalHealFactory.duHoaChatMP = ""
                }
            case 13:
                if !result.isEmpty{
                    medicalHealFactory.yeuToTXNNTime = result
                }else{
                    medicalHealFactory.yeuToTXNNTime = ""
                }
            case 14:
                if !result.isEmpty{
                    medicalHealFactory.loaiHoXi = result
                }else{
                    medicalHealFactory.loaiHoXi = ""
                }
                
            default:
                break
            }
            delegate?.resultMedicalHealthFactory(medicalHealthFactory:medicalHealFactory)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if let result = textView.text {
            if !result.isEmpty{
                medicalHealFactory.yeuToTXNN = result.trim()
                labelOther.isHidden = false
            }else{
                medicalHealFactory.yeuToTXNN = ""
                labelOther.isHidden = true
            }
        }
        
        delegate?.resultMedicalHealthFactory(medicalHealthFactory:medicalHealFactory)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == uongruou_Num{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == lblTimeTX{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == lblLoaiHX{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == lblhoachat{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == lblOther{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else{
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
