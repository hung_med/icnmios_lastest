//
//  HealthRecordsCell.swift
//  iCNM
//
//  Created by Mac osx on 8/22/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import BEMCheckBox
protocol TiensugiadinhCustomCellDelegate {
    func resultMedicalHealthFactory(medicalHealthFactory:MedicalHealthFactory)
}

class TiensugiadinhCustomCell: UITableViewCell, UITextFieldDelegate, BEMCheckBoxDelegate {

    @IBOutlet var lblTimmach_CK: BEMCheckBox!
    @IBOutlet var lblTHA_CK: BEMCheckBox!
    @IBOutlet var lblTamthan_CK: BEMCheckBox!
    @IBOutlet var lblHenxuyen_CK: BEMCheckBox!
    @IBOutlet var lblDongkinh_CK: BEMCheckBox!
    @IBOutlet var lblDaiTD_CK: BEMCheckBox!
    
    @IBOutlet var lblTimmach_NM: FWFloatingLabelTextField!
    @IBOutlet var lblTHA_NM: FWFloatingLabelTextField!
    @IBOutlet var lblTamthan_NM: FWFloatingLabelTextField!
    @IBOutlet var lblHenxuyen_NM: FWFloatingLabelTextField!
    @IBOutlet var lblDongkinh_NM: FWFloatingLabelTextField!
    @IBOutlet var lblDaiTD_NM: FWFloatingLabelTextField!
    
    @IBOutlet var lblungthu: FWFloatingLabelTextField!
    @IBOutlet var lblLao: FWFloatingLabelTextField!
    @IBOutlet var lblKhac: FWFloatingLabelTextField!
    
    @IBOutlet var lblungthu_NM: FWFloatingLabelTextField!
    @IBOutlet var lblLao_NM: FWFloatingLabelTextField!
    @IBOutlet var lblKhac_NM: FWFloatingLabelTextField!

    @IBOutlet var lblThuoc_NM: FWFloatingLabelTextField!
    @IBOutlet var lblDiung_NM: FWFloatingLabelTextField!
    @IBOutlet var lblThucpham_NM: FWFloatingLabelTextField!
    @IBOutlet var lblHoachat_NM: FWFloatingLabelTextField!
    
    @IBOutlet var tsgd_lblThuoc: FWFloatingLabelTextField!
    @IBOutlet var tsgd_lblDiung: FWFloatingLabelTextField!
    @IBOutlet var tsgd_lblThucpham: FWFloatingLabelTextField!
    @IBOutlet var tsgd_lblHoachat: FWFloatingLabelTextField!
    
    var delegate: TiensugiadinhCustomCellDelegate?
    var medicalHealFactory = MedicalHealthFactoryUser.sharedInstance.medicalHealFactoriesObj
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTimmach_NM.delegate = self
        lblTHA_NM.delegate = self
        lblTamthan_NM.delegate = self
        lblHenxuyen_NM.delegate = self
        lblDongkinh_NM.delegate = self
        lblDaiTD_NM.delegate = self
        lblungthu.delegate = self
        lblLao.delegate = self
        lblKhac.delegate = self
        lblungthu_NM.delegate = self
        lblLao_NM.delegate = self
        lblKhac_NM.delegate = self
        lblThuoc_NM.delegate = self
        lblDiung_NM.delegate = self
        lblThucpham_NM.delegate = self
        lblHoachat_NM.delegate = self
        tsgd_lblThuoc.delegate = self
        tsgd_lblDiung.delegate = self
        tsgd_lblThucpham.delegate = self
        tsgd_lblHoachat.delegate = self
        
        lblTimmach_CK.delegate = self
        lblTHA_CK.delegate = self
        lblTamthan_CK.delegate = self
        lblHenxuyen_CK.delegate = self
        lblDongkinh_CK.delegate = self
        lblDaiTD_CK.delegate = self
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(TiensugiadinhCustomCell.didSelectCell))
        addGestureRecognizer(gesture)
    }
    
    func didTap(_ checkBox: BEMCheckBox){
        switch checkBox.tag {
        case 20:
            medicalHealFactory.tsGDBenhTimMach = checkBox.on
        case 21:
            medicalHealFactory.tsGDTangHuyetAp = checkBox.on
        case 22:
            medicalHealFactory.tsGDTamThan = checkBox.on
        case 23:
            medicalHealFactory.tsGDHenSuyen = checkBox.on
        case 24:
            medicalHealFactory.tsGDDaiThaoDuong = checkBox.on
        case 25:
            medicalHealFactory.tsGDDongKinh = checkBox.on
        default:
            break
        }
        
        delegate?.resultMedicalHealthFactory(medicalHealthFactory:medicalHealFactory)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

// MARK: - Actions
extension TiensugiadinhCustomCell {
    
    func didSelectCell()
    {
//        lblTimmach_NM.becomeFirstResponder()
//        lblTHA_NM.becomeFirstResponder()
//        lblTamthan_NM.becomeFirstResponder()
//        lblHenxuyen_NM.becomeFirstResponder()
//        lblDongkinh_NM.becomeFirstResponder()
//        lblDaiTD_NM.becomeFirstResponder()
//        lblungthu.becomeFirstResponder()
//        lblLao.becomeFirstResponder()
//        lblKhac.becomeFirstResponder()
//        lblungthu_NM.becomeFirstResponder()
//        lblLao_NM.becomeFirstResponder()
//        lblKhac_NM.becomeFirstResponder()
//        lblThuoc_NM.becomeFirstResponder()
//        lblDiung_NM.becomeFirstResponder()
//        lblThucpham_NM.becomeFirstResponder()
//        lblHoachat_NM.becomeFirstResponder()
//        tsgd_lblThuoc.becomeFirstResponder()
//        tsgd_lblDiung.becomeFirstResponder()
//        tsgd_lblThucpham.becomeFirstResponder()
//        tsgd_lblHoachat.becomeFirstResponder()
    }
    
    @IBAction func textFieldValueChanged(_ sender: UITextField) {
        if let result = sender.text {
            switch sender.tag {
            case 0:
                if !result.isEmpty{
                    medicalHealFactory.tsGDThuoc = result.trim()
                }else{
                   medicalHealFactory.tsGDThuoc = ""
                }
            case 1:
                if !result.isEmpty{
                    medicalHealFactory.tsGDThuocName = result.trim()
                }else{
                    medicalHealFactory.tsGDThuocName = ""
                }
            case 2:
                if !result.isEmpty{
                    medicalHealFactory.tsGDHoaChatMyPham = result.trim()
                }else{
                    medicalHealFactory.tsGDHoaChatMyPham = ""
                }
            case 3:
                if !result.isEmpty{
                    medicalHealFactory.tsGDHoaChatMyPhamName = result.trim()
                }else{
                    medicalHealFactory.tsGDHoaChatMyPhamName = ""
                }
            case 4:
                if !result.isEmpty{
                    medicalHealFactory.tsGDThucPham = result.trim()
                    
                }else{
                    medicalHealFactory.tsGDThucPham = ""
                }
            case 5:
                if !result.isEmpty{
                    medicalHealFactory.tsGDThucPhamName = result.trim()
                }else{
                    medicalHealFactory.tsGDThucPhamName = ""
                }
            case 6:
                if !result.isEmpty{
                    medicalHealFactory.tsGDNote = result.trim()
                }else{
                    medicalHealFactory.tsGDNote = ""
                }
            case 7:
                if !result.isEmpty{
                    medicalHealFactory.tsGDNoteName = result.trim()
                }else{
                    medicalHealFactory.tsGDNoteName = ""
                }
            case 8:
                if !result.isEmpty{
                    medicalHealFactory.tsGDBenhTimMachName = result.trim()
                }else{
                    medicalHealFactory.tsGDBenhTimMachName = ""
                }
            case 9:
                if !result.isEmpty{
                    medicalHealFactory.tsGDTangHuyetApName = result.trim()
                }else{
                    medicalHealFactory.tsGDTangHuyetApName = ""
                }
            case 10:
                if !result.isEmpty{
                    medicalHealFactory.tsGDTamThanName = result.trim()
                }else{
                    medicalHealFactory.tsGDTamThanName = ""
                }
            case 11:
                if !result.isEmpty{
                    medicalHealFactory.tsGDHenSuyenName = result.trim()
                }else{
                    medicalHealFactory.tsGDHenSuyenName = ""
                }
            case 12:
                if !result.isEmpty{
                    medicalHealFactory.tsGDDaiThaoDuongName = result.trim()
                }else{
                    medicalHealFactory.tsGDDaiThaoDuongName = ""
                }
            case 13:
                if !result.isEmpty{
                    medicalHealFactory.tsGDDongKinhName = result.trim()
                }else{
                    medicalHealFactory.tsGDDongKinhName = ""
                }
            case 14:
                if !result.isEmpty{
                    medicalHealFactory.tsGDUngThu = result.trim()
                }else{
                    medicalHealFactory.tsGDUngThu = ""
                }
            case 15:
                if !result.isEmpty{
                    medicalHealFactory.tsGDUngThuName = result.trim()
                }else{
                    medicalHealFactory.tsGDUngThuName = ""
                }
            case 16:
                if !result.isEmpty{
                    medicalHealFactory.tsGDLao = result.trim()
                }else{
                    medicalHealFactory.tsGDLao = ""
                }
            case 17:
                if !result.isEmpty{
                    medicalHealFactory.tsGDLaoName = result.trim()
                }else{
                    medicalHealFactory.tsGDLaoName = ""
                }
            case 18:
                if !result.isEmpty{
                    medicalHealFactory.tsGDNoteAnother = result.trim()
                }else{
                    medicalHealFactory.tsGDNoteAnother = ""
                }
            case 19:
                if !result.isEmpty{
                    medicalHealFactory.tsGDNoteAnotherName = result.trim()
                }else{
                    medicalHealFactory.tsGDNoteAnotherName = ""
                }
            default: break
                
            }
            
            delegate?.resultMedicalHealthFactory(medicalHealthFactory:medicalHealFactory)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tsgd_lblThuoc{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == tsgd_lblThucpham{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == tsgd_lblHoachat{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == tsgd_lblDiung{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == lblungthu{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == lblLao{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == lblKhac{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == lblTimmach_NM{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }else if textField == lblTHA_NM{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }else if textField == lblDongkinh_NM{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }else if textField == lblHenxuyen_NM{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }else if textField == lblTHA_NM{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }else if textField == lblTamthan_NM{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }else if textField == lblDaiTD_NM{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }else if textField == lblungthu_NM{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }else if textField == lblLao_NM{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }else if textField == lblKhac_NM{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }else if textField == lblThuoc_NM{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }else if textField == lblDiung_NM{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }else if textField == lblThucpham_NM{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }else if textField == lblHoachat_NM{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }
        else{
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
