//
//  LookupResultVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

protocol AddMuitiemVCDelegate {
    func gobackDetailView(isUpdateItem:Bool, currentTag:Int, currentIndex:Int, vaccinID:Int, vaccinTypeID:Int, vaccinName:String, vaccinDate:String, vaccinSchedule:String, vaccinReact:String, monthPreg:Int, vacccinIdx:Int)
}

class AddMuitiemVC: BaseViewController, UITextFieldDelegate, UIPopoverPresentationControllerDelegate, FWComboBoxDelegate {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var buttonExit: UIButton!
    @IBOutlet weak var buttonDone: UIButton!
    @IBOutlet weak var spaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblThangthai: FWFloatingLabelTextField!
    @IBOutlet weak var lblPUSautiem: FWFloatingLabelTextField!
    @IBOutlet weak var lblNgaychungngua: FWFloatingLabelTextField!
    @IBOutlet weak var lblNgayhenkham: FWFloatingLabelTextField!
    @IBOutlet weak var btnNgaychungngua: UIButton!
    @IBOutlet weak var btnNgayhenkham: UIButton!
    @IBOutlet weak var comboboxListMT: FWComboBox!
    @IBOutlet weak var lblWarning: UILabel!
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var lblWarningMsg: UILabel!
    @IBOutlet weak var lblSelectDay: UILabel!
    @IBOutlet weak var formatDate: UIButton!
    @IBOutlet weak var comboboxBtn: UIButton!
    var currentTag:Int? = 0
    var currentIndex:Int? = 0
    var isUpdateItem:Bool? = false
    var medicalVaccin:MedicalVaccin? = nil
    var currentVacxinName:String? = nil
    var currentVacxinID:Int = 0
    var currentVacxinIdx:Int = 0
    var currentVaccinTypeID:Int = 0
    var delegate:AddMuitiemVCDelegate?
    var isCheckUpdate:Bool? = false
    var convertTime1:Int = 0
    var convertTime2:Int = 0
    var medicalUnits: [String]?
    //var lookUpResults: [LookUpResult]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        myView.layer.cornerRadius = 10
        myView.layer.borderWidth = 1
        myView.layer.masksToBounds = true
        buttonDone.layer.cornerRadius = 4
        buttonDone.layer.masksToBounds = true
        buttonExit.layer.cornerRadius = 4
        buttonExit.layer.masksToBounds = true
        comboboxBtn.isHidden = true
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        comboboxListMT.layer.cornerRadius = 5
        comboboxListMT.layer.masksToBounds = true
        comboboxListMT.layer.borderWidth = 0.5
        comboboxListMT.layer.borderColor = UIColor.lightGray.cgColor
        comboboxListMT.delegate = self
        comboboxListMT.comboTextAlignment = .center
        if self.currentTag == 9{
            medicalUnits = ["BCG", "VGB Sơ sinh", "DPT-VGB-Hib 1", "DPT-VGB-Hib 2", "DPT-VGB-Hib 3", "Bại liệt 1", "Bại liệt 2", "Bại liệt 3", "Sởi 1", "Sởi 2", "DPT4", "VNNB B1", "VNNB B2", "VNNB B3"]
        }else if self.currentTag == 10{
            medicalUnits = ["Tả 1", "Tả 2", "Quai bị 1", "Quai bị 2", "Quai bị 3", "Cúm 1", "Cúm 2", "Cúm 3", "Thương hàn", "HPV 1", "HPV 2", "HPV 3", "Vắc xin phế cầu khuẩn"]
        }else if self.currentTag == 11{
             medicalUnits = ["UV1", "UV2", "UV3", "UV4", "UV5"]
        }
        self.initialComboboxDatasource()
        self.lblPUSautiem.delegate = self
        self.lblNgaychungngua.delegate = self
        self.lblNgayhenkham.delegate = self
        self.lblThangthai.delegate = self
        self.btnNgaychungngua.tag = 1
        self.btnNgaychungngua.addTarget(self, action: #selector(btnSelectDay), for: .touchDown)
        self.btnNgayhenkham.tag = 2
        self.btnNgayhenkham.addTarget(self, action: #selector(btnSelectDay), for: .touchDown)
        self.searchBar.isHidden = true

        self.updateValueMuitiem(currentTag:self.currentTag!)
    }
    
    func updateValueMuitiem(currentTag:Int){
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        if self.currentTag == 9{
            self.lblThangthai.isHidden = true
            spaceConstraint.constant = -44
            self.lblTitle.text = "Tiêm chủng trẻ em"
            if self.medicalVaccin != nil{
                self.lblThangthai.isHidden = true
                if let vaccinID = self.medicalVaccin?.vaccinID{
                    currentVacxinID = vaccinID
                }
                
                if let vaccinTypeID = self.medicalVaccin?.vaccinTypeID{
                    currentVaccinTypeID = vaccinTypeID
                }
                
                if let vaccinName = self.medicalVaccin?.vaccinName{
                    self.currentVacxinName = vaccinName
                    comboboxBtn.setTitleColor(UIColor(hexColor: 0x1976D2, alpha: 1.0), for: UIControlState.normal)
                }
                
                if let vaccinReact = self.medicalVaccin?.vaccinReact{
                    self.lblPUSautiem.text = vaccinReact
                    self.lblPUSautiem.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                }
                
                if let vaccinDate = self.medicalVaccin?.vaccinDate{
                    let result = resultDateFormatStandard(str: vaccinDate)
                    self.lblNgaychungngua.text = result
                    self.lblNgaychungngua.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                }
                if let vaccinSchedule = self.medicalVaccin?.vaccinSchedule{
                    let result = resultDateFormatStandard(str: vaccinSchedule)
                    self.lblNgayhenkham.text = result
                    self.lblNgayhenkham.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                }
            }else{
                currentVaccinTypeID = 1
                currentIndex = 0
                currentVacxinID = 0
                isUpdateItem = false
                currentVacxinName = ""
                self.lblThangthai.isHidden = true
                spaceConstraint.constant = -44
            }
                
        }else if self.currentTag == 10{
            self.lblThangthai.isHidden = true
            self.lblTitle.text = "Tiêm chủng ngoài chương trình TCMR"
            if self.medicalVaccin != nil{
                self.lblThangthai.isHidden = true
                spaceConstraint.constant = -44
                if let vaccinID = self.medicalVaccin?.vaccinID{
                    currentVacxinID = vaccinID
                }
                
                if let vaccinTypeID = self.medicalVaccin?.vaccinTypeID{
                    currentVaccinTypeID = vaccinTypeID
                }
                
                if let vaccinName = self.medicalVaccin?.vaccinName{
                    self.currentVacxinName = vaccinName
                    comboboxBtn.setTitle(vaccinName, for: UIControlState.normal)
                    comboboxBtn.setTitleColor(UIColor(hexColor: 0x1976D2, alpha: 1.0), for: UIControlState.normal)
                }
                if let vaccinReact = self.medicalVaccin?.vaccinReact{
                    self.lblPUSautiem.text = vaccinReact
                    self.lblPUSautiem.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                }
                if let vaccinDate = self.medicalVaccin?.vaccinDate{
                    let result = resultDateFormatStandard(str: vaccinDate)
                    self.lblNgaychungngua.text = result
                    self.lblNgaychungngua.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                }
                if let vaccinSchedule = self.medicalVaccin?.vaccinSchedule{
                    let result = resultDateFormatStandard(str: vaccinSchedule)
                    self.lblNgayhenkham.text = result
                    self.lblNgayhenkham.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                }
            }else{
                currentVaccinTypeID = 2
                currentIndex = 0
                currentVacxinID = 0
                isUpdateItem = false
                currentVacxinName = ""
                self.lblThangthai.isHidden = true
                spaceConstraint.constant = -44
            }
        }else if self.currentTag == 11{
            self.lblThangthai.isHidden = false
            self.lblTitle.text = "Tiêm chủng uốn ván(Phụ nữ có thai)"
            if self.medicalVaccin != nil{
                self.lblThangthai.isHidden = false
                spaceConstraint.constant = 22
                if let vaccinID = self.medicalVaccin?.vaccinID{
                    currentVacxinID = vaccinID
                }
                
                if let vaccinTypeID = self.medicalVaccin?.vaccinTypeID{
                    currentVaccinTypeID = vaccinTypeID
                }
                
                if let vaccinName = self.medicalVaccin?.vaccinName{
                    self.currentVacxinName = vaccinName
                    comboboxBtn.setTitle(vaccinName, for: UIControlState.normal)
                    comboboxBtn.setTitleColor(UIColor(hexColor: 0x1976D2, alpha: 1.0), for: UIControlState.normal)
                }
                if let vaccinReact = self.medicalVaccin?.vaccinReact{
                    self.lblPUSautiem.text = vaccinReact
                    self.lblPUSautiem.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                }
                if let vaccinDate = self.medicalVaccin?.vaccinDate{
                    let result = resultDateFormatStandard(str: vaccinDate)
                    self.lblNgaychungngua.text = result
                    self.lblNgaychungngua.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                }
                if let vaccinSchedule = self.medicalVaccin?.vaccinSchedule{
                    let result = resultDateFormatStandard(str: vaccinSchedule)
                    self.lblNgayhenkham.text = result
                    self.lblNgayhenkham.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                }
                if let monthPreg = self.medicalVaccin?.monthPreg{
                    self.lblThangthai.text = "\(monthPreg)"
                    self.lblThangthai.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                }
            }else{
                currentVaccinTypeID = 3
                currentIndex = 0
                currentVacxinID = 0
                isUpdateItem = false
                currentVacxinName = ""
                spaceConstraint.constant = 22
            }
        }
    }
    
    /*
     * initial combox datasource
     */
    func initialComboboxDatasource() {
        self.comboboxListMT.dataSource = self.medicalUnits!
        if self.currentVacxinIdx == 0{
            comboboxListMT.selectRow(at: 0)
        }else{
            comboboxListMT.selectRow(at: self.currentVacxinIdx)
        }
    }
    
    func fwComboBox(comboBox: FWComboBox, didSelectAtIndex index: Int) {
        let medicalName = self.medicalUnits![index]
        self.isCheckUpdate = true
        self.currentVacxinName = medicalName
        self.currentVacxinIdx = index
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.lblPUSautiem.resignFirstResponder()
        self.lblNgaychungngua.resignFirstResponder()
        self.lblNgayhenkham.resignFirstResponder()
        return true
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func btnSelectDay(sender: UIButton) {
        let currentDate = Date()
        var dateComponents = DateComponents()
        if sender.tag == 1{
            dateComponents.year = -100
        }else if sender.tag == 2{
            dateComponents.year = 100
        }
        
        let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        DatePickerDialog().show("Chọn ngày", doneButtonTitle: "Chọn", cancelButtonTitle: "Huỷ", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .date) { (date) in
            if let dateString = date {
                let formatDateStr = formatter.string(from: dateString)
                if sender.tag == 1{
                    let timeInterval = dateString.timeIntervalSince1970
                    self.convertTime1 = Int(timeInterval)
                    self.lblNgaychungngua.text = formatDateStr
                }else if sender.tag == 2{
                    self.lblNgayhenkham.text = formatDateStr
                    let timeInterval = dateString.timeIntervalSince1970
                    self.convertTime2 = Int(timeInterval)
                }
                
                if self.convertTime1 != 0 && self.convertTime2 != 0 && (self.convertTime1 > self.convertTime2){
                    if sender.tag == 1{
                        self.lblNgaychungngua.text = ""
                    }else{
                        self.lblNgayhenkham.text = ""
                    }
                    showAlertView(title: "Ngày chủng ngừa phải nhỏ hơn ngày hẹn khám", view: self)
                    return
                }
            }
        }
    }
   
    @IBAction func btnDone2(_ sender: Any) {
        if (!(self.currentVacxinName?.isEmpty)!) && !(self.lblNgaychungngua.text?.isEmpty)!{
            if let vaccinDate = self.lblNgaychungngua.text, let vaccinSchedule = self.lblNgayhenkham.text, let vaccinReact = self.lblPUSautiem.text, let monthPreg = self.lblThangthai.text{
                self .dismiss(animated: true) {
                    var currentMonth:Int? = 0
                    if monthPreg.isEmpty{
                        currentMonth = 0
                    }else{
                        currentMonth = Int(monthPreg)
                    }
                    
                    self.delegate?.gobackDetailView(isUpdateItem:self.isUpdateItem!, currentTag: self.currentTag!, currentIndex: self.currentIndex!, vaccinID:self.currentVacxinID, vaccinTypeID: self.currentVaccinTypeID, vaccinName: self.currentVacxinName!, vaccinDate: vaccinDate, vaccinSchedule: vaccinSchedule, vaccinReact: vaccinReact.trim(), monthPreg: currentMonth!, vacccinIdx: self.currentVacxinIdx)
                }
            }
        }else{
            lblWarningMsg.text = "Bạn vui lòng nhập đầy đủ thông tin"
            return
        }
    }
    
    @IBAction func btnExit(_ sender: Any) {
        self .dismiss(animated: true) { 
            
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == lblThangthai{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 2
        }else if textField == lblPUSautiem{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else{
            return true
        }
    }
   
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}
