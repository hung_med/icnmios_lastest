//
//  LamsangCustomCell.swift
//  iCNM
//
//  Created by Mac osx on 8/22/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
protocol LamsangCustomCellDelegate {
    func resultMedicalCilinnical(medicalCilinnical:MedicalCilinnical)
}
class LamsangCustomCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet var txtNgaykham: UITextField!
    @IBOutlet var txtHenKham: UITextField!
    @IBOutlet var txtTiensu: FWFloatingLabelTextField!
    @IBOutlet var txtMach: FWFloatingLabelTextField!
    @IBOutlet var txtNhietdo: FWFloatingLabelTextField!
    @IBOutlet var txtHA: FWFloatingLabelTextField!
    @IBOutlet var txtNhiptho: FWFloatingLabelTextField!
    @IBOutlet var txtCannang: FWFloatingLabelTextField!
    @IBOutlet var txtCao: FWFloatingLabelTextField!
    @IBOutlet var txtBMI: FWFloatingLabelTextField!
    @IBOutlet var txtVongbung: FWFloatingLabelTextField!
    @IBOutlet var txtMattrai_NoK: FWFloatingLabelTextField!
    @IBOutlet var txtMatphai_NoK: FWFloatingLabelTextField!
    @IBOutlet var txtMatphai_CoK: FWFloatingLabelTextField!
    @IBOutlet var txtMattrai_CoK: FWFloatingLabelTextField!
    @IBOutlet var txtNiemmac: FWFloatingLabelTextField!
    @IBOutlet var txtKhac: FWFloatingLabelTextField!
    @IBOutlet var txtTimmach: FWFloatingLabelTextField!
    @IBOutlet var txtHohap: FWFloatingLabelTextField!
    @IBOutlet var txtTieuhoa: FWFloatingLabelTextField!
    @IBOutlet var txtTietlieu: FWFloatingLabelTextField!
    
    @IBOutlet var txtThankinh: FWFloatingLabelTextField!
    @IBOutlet var txtNoitiet: FWFloatingLabelTextField!
    @IBOutlet var txtCoXuongKhop: FWFloatingLabelTextField!
    @IBOutlet var txtNgoaikhoa: FWFloatingLabelTextField!
    @IBOutlet var txtTamthan: FWFloatingLabelTextField!
    @IBOutlet var txtSanphukhoa: FWFloatingLabelTextField!
    @IBOutlet var txtTaimuihong: FWFloatingLabelTextField!
    @IBOutlet var txtRHM: FWFloatingLabelTextField!
    @IBOutlet var txtMat: FWFloatingLabelTextField!
    @IBOutlet var txtDalieu: FWFloatingLabelTextField!
    @IBOutlet var txtDinhduong: FWFloatingLabelTextField!
    @IBOutlet var txtVandong: FWFloatingLabelTextField!
    @IBOutlet var txtOther: FWFloatingLabelTextField!
    @IBOutlet var txtDanhgia: FWFloatingLabelTextField!
    @IBOutlet var txtHuyethoc: FWFloatingLabelTextField!
    @IBOutlet var txtSinhhoamau: FWFloatingLabelTextField!
    @IBOutlet var txtSieuam: FWFloatingLabelTextField!
    @IBOutlet var txtSinhhoaNT: FWFloatingLabelTextField!
    @IBOutlet var txtKetluan: FWFloatingLabelTextField!
    @IBOutlet var txtTuvan: FWFloatingLabelTextField!
    @IBOutlet var txtBacsikham: FWFloatingLabelTextField!
    
    var delegate: LamsangCustomCellDelegate?
    //var medicalCilinnical = MedicalCilinnical()
    var medicalCilinnical = MedicalHealthFactoryUser.sharedInstance.medicalCilinnicalObj
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtNgaykham.delegate = self
        txtHenKham.delegate = self
        txtTiensu.delegate = self
        txtMach.delegate = self
        txtNhietdo.delegate = self
        txtHA.delegate = self
        txtNhiptho.delegate = self
        txtCannang.delegate = self
        txtCao.delegate = self
        txtBMI.delegate = self

        txtVongbung.delegate = self
        txtMattrai_NoK.delegate = self
        txtMatphai_NoK.delegate = self
        txtMatphai_CoK.delegate = self
        txtMattrai_CoK.delegate = self
        txtNiemmac.delegate = self
        txtKhac.delegate = self
        txtTimmach.delegate = self
        txtHohap.delegate = self
        txtTieuhoa.delegate = self
        txtTietlieu.delegate = self
       
        txtThankinh.delegate = self
        txtNoitiet.delegate = self
        txtCoXuongKhop.delegate = self
        txtNgoaikhoa.delegate = self
        txtTamthan.delegate = self
        txtSanphukhoa.delegate = self
        txtTaimuihong.delegate = self
        txtRHM.delegate = self
        txtMat.delegate = self
        txtDalieu.delegate = self
        txtDinhduong.delegate = self
        txtVandong.delegate = self
        txtOther.delegate = self
        txtDanhgia.delegate = self
        txtHuyethoc.delegate = self
        txtSinhhoamau.delegate = self
        txtSieuam.delegate = self
        txtSinhhoaNT.delegate = self
        txtKetluan.delegate = self
        txtTuvan.delegate = self
        txtBacsikham.delegate = self
       
        let gesture = UITapGestureRecognizer(target: self, action: #selector(LamsangCustomCell.didSelectCell))
        addGestureRecognizer(gesture)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func HenKhamHandler(_ sender: Any) {
        let tag = (sender as AnyObject).tag
        self.showPickerDateView(tag:tag!)
    }
    
    func showPickerDateView(tag:Int){
        let currentDate = Date()
        var dateComponents = DateComponents()
        dateComponents.year = -100
        let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        
        DatePickerDialog().show("Chọn ngày", doneButtonTitle: "Chọn", cancelButtonTitle: "Huỷ", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .date) { (date) in
            if let dateString = date {
                let formatDateStr = formatter.string(from: dateString)
                if tag == 1{
                    self.txtNgaykham.text = formatDateStr
                    self.medicalCilinnical.dateCilin = formatDateStr
                self.delegate?.resultMedicalCilinnical(medicalCilinnical:self.medicalCilinnical)
                }else if tag == 2{
                    self.txtHenKham.text = formatDateStr
                    self.medicalCilinnical.dateSchedule = formatDateStr
                self.delegate?.resultMedicalCilinnical(medicalCilinnical:self.medicalCilinnical)
                }
            }
        }
    }
}

// MARK: - Actions
extension LamsangCustomCell {
    func didSelectCell() {
//        txtBacsikham.becomeFirstResponder()
//        txtTuvan.becomeFirstResponder()
//        txtKetluan.becomeFirstResponder()
//        txtSinhhoaNT.becomeFirstResponder()
//        txtSieuam.becomeFirstResponder()
//        txtSinhhoamau.becomeFirstResponder()
//        txtHuyethoc.becomeFirstResponder()
//        txtDanhgia.becomeFirstResponder()
//        txtOther.becomeFirstResponder()
//        txtVandong.becomeFirstResponder()
//
//        txtDinhduong.becomeFirstResponder()
//        txtDalieu.becomeFirstResponder()
//        txtMat.becomeFirstResponder()
//        txtRHM.becomeFirstResponder()
//        txtTaimuihong.becomeFirstResponder()
//        txtSanphukhoa.becomeFirstResponder()
//        txtTamthan.becomeFirstResponder()
//        txtNgoaikhoa.becomeFirstResponder()
//        txtCoXuongKhop.becomeFirstResponder()
//        txtNoitiet.becomeFirstResponder()
//        txtThankinh.becomeFirstResponder()
//        txtVongbung.becomeFirstResponder()
//        txtMattrai_NoK.becomeFirstResponder()
//        txtMatphai_NoK.becomeFirstResponder()
//        txtMatphai_CoK.becomeFirstResponder()
//        txtMattrai_CoK.becomeFirstResponder()
//        txtNiemmac.becomeFirstResponder()
//        txtKhac.becomeFirstResponder()
//        txtTimmach.becomeFirstResponder()
//        txtHohap.becomeFirstResponder()
//        txtTieuhoa.becomeFirstResponder()
//        txtTietlieu.becomeFirstResponder()
//        txtTiensu.becomeFirstResponder()
//        txtMach.becomeFirstResponder()
//        txtNhietdo.becomeFirstResponder()
//        txtHA.becomeFirstResponder()
//        txtNhiptho.becomeFirstResponder()
//        txtCannang.becomeFirstResponder()
//        txtCao.becomeFirstResponder()
//        txtBMI.becomeFirstResponder()
    }

    @IBAction func textFieldValueChanged(_ sender: UITextField) {
        if let result = sender.text {
            switch sender.tag {
            case 3:
                if !result.isEmpty{
                    medicalCilinnical.historyCilin = result.trim()
                }else{
                   medicalCilinnical.historyCilin = ""
                }
            case 4:
                if !result.isEmpty{
                    let value = Int(result)
                    medicalCilinnical.mach = value!
                    
                }else{
                    
                }
            case 5:
                if !result.isEmpty{
                    let value = Int(result)
                    medicalCilinnical.nhietDo = value!
                    
                }else{
                    
                }
            case 6:
                if !result.isEmpty{
                    let value = Int(result)
                    medicalCilinnical.ha = value!
                    
                }else{
                    
                }
            case 7:
                if !result.isEmpty{
                    let value = Int(result)
                    medicalCilinnical.nhipTho = value!
                    
                }else{
                   
                }
            case 8:
                if !result.isEmpty{
                    let value = Int(result)
                    medicalCilinnical.weight = value!
                    
                }else{
                    
                }
            case 9:
                if !result.isEmpty{
                    let value = Int(result)
                    medicalCilinnical.height = value!
                    
                }else{
                    
                }
            case 10:
                if !result.isEmpty{
                    let value = Int(result)
                    medicalCilinnical.bMI = value!
                    
                }else{
                    
                }
            case 11:
                if !result.isEmpty{
                    let value = Int(result)
                    medicalCilinnical.vongBung = value!
                    
                }else{
                   
                }
            case 12:
                if !result.isEmpty{
                    let value = Int(result)
                    medicalCilinnical.leftEye = value!
                    
                }else{
                    
                }
            case 13:
                if !result.isEmpty{
                    let value = Int(result)
                    medicalCilinnical.rightEye = value!
                    
                }else{
                    
                }
            case 14:
                if !result.isEmpty{
                    let value = Int(result)
                    medicalCilinnical.leftEyeGlass = value!
                    
                }else{
                    
                }
            case 15:
                if !result.isEmpty{
                    let value = Int(result)
                    medicalCilinnical.rightEyeGlass = value!
                    
                }else{
                    
                }
            case 16:
                if !result.isEmpty{
                    medicalCilinnical.daNiemMac = result.trim()
                }else{
                    medicalCilinnical.daNiemMac = ""
                }
            case 17:
                if !result.isEmpty{
                    medicalCilinnical.toanThanNote = result.trim()
                }else{
                    medicalCilinnical.toanThanNote = ""
                }
            case 18:
                if !result.isEmpty{
                    medicalCilinnical.timMach = result.trim()
                }else{
                    medicalCilinnical.timMach = ""
                }
            case 19:
                if !result.isEmpty{
                    medicalCilinnical.hoHap = result.trim()
                }else{
                   medicalCilinnical.hoHap = ""
                }
            case 20:
                if !result.isEmpty{
                    medicalCilinnical.tieuHoa = result.trim()
                }else{
                    medicalCilinnical.tieuHoa = ""
                }
            case 21:
                if !result.isEmpty{
                    medicalCilinnical.tietNieu = result.trim()
                }else{
                    medicalCilinnical.tietNieu = ""
                }
            case 22:
                if !result.isEmpty{
                    medicalCilinnical.coXuongKhop = result.trim()
                }else{
                   medicalCilinnical.coXuongKhop = ""
                }
            case 23:
                if !result.isEmpty{
                    medicalCilinnical.noiTiet = result.trim()
                }else{
                    medicalCilinnical.noiTiet = ""
                }
            case 24:
                if !result.isEmpty{
                    medicalCilinnical.thanKinh = result.trim()
                }else{
                    medicalCilinnical.thanKinh = ""
                }
            case 25:
                if !result.isEmpty{
                    medicalCilinnical.tamThan = result.trim()
                }else{
                    medicalCilinnical.tamThan = ""
                }
            case 26:
                if !result.isEmpty{
                    medicalCilinnical.ngoaiKhoa = result.trim()
                }else{
                    medicalCilinnical.ngoaiKhoa = ""
                }
            case 27:
                if !result.isEmpty{
                    medicalCilinnical.sanPhuKhoa = result.trim()
                }else{
                    medicalCilinnical.sanPhuKhoa = ""
                }
            case 28:
                if !result.isEmpty{
                    medicalCilinnical.taiMuiHong = result.trim()
                }else{
                    medicalCilinnical.taiMuiHong = ""
                }
            case 29:
                if !result.isEmpty{
                    medicalCilinnical.rangHamMat = result.trim()
                }else{
                   medicalCilinnical.rangHamMat = ""
                }
            case 30:
                if !result.isEmpty{
                    medicalCilinnical.mat = result.trim()
                }else{
                    medicalCilinnical.mat = ""
                }
            case 31:
                if !result.isEmpty{
                    medicalCilinnical.daLieu = result.trim()
                }else{
                    medicalCilinnical.daLieu = ""
                }
            case 32:
                if !result.isEmpty{
                    medicalCilinnical.dinhDuong = result.trim()
                }else{
                    medicalCilinnical.dinhDuong = ""
                }
            case 33:
                if !result.isEmpty{
                    medicalCilinnical.vanDong = result.trim()
                }else{
                   medicalCilinnical.vanDong = ""
                }
            case 34:
                if !result.isEmpty{
                    medicalCilinnical.coQuanKhac = result.trim()
                }else{
                    medicalCilinnical.coQuanKhac = ""
                }
            case 35:
                if !result.isEmpty{
                    medicalCilinnical.danhGia = result.trim()
                }else{
                    medicalCilinnical.danhGia = ""
                }
            case 36:
                if !result.isEmpty{
                    medicalCilinnical.huyetHoc = result.trim()
                }else{
                    medicalCilinnical.huyetHoc = ""
                }
            case 37:
                if !result.isEmpty{
                    medicalCilinnical.sinhHoaNuocTieu = result.trim()
                }else{
                    medicalCilinnical.sinhHoaNuocTieu = ""
                }
            case 38:
                if !result.isEmpty{
                    medicalCilinnical.chanDoan = result.trim()
                }else{
                    medicalCilinnical.chanDoan = ""
                }
            case 39:
                if !result.isEmpty{
                    medicalCilinnical.tuVan = result.trim()
                }else{
                    medicalCilinnical.tuVan = ""
                }
            case 40:
                if !result.isEmpty{
                    medicalCilinnical.bacSiName = result.trim()
                }else{
                    medicalCilinnical.bacSiName = ""
                }
            case 41:
                if !result.isEmpty{
                    medicalCilinnical.sinhHoaMau = result.trim()
                }else{
                    medicalCilinnical.sinhHoaMau = ""
                }
            case 42:
                if !result.isEmpty{
                    medicalCilinnical.sieuAmOBung = result.trim()
                }else{
                    medicalCilinnical.sieuAmOBung = ""
                }
            default:
                break
            }
            medicalCilinnical.active = true
            delegate?.resultMedicalCilinnical(medicalCilinnical:medicalCilinnical)
        }
    }
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtMach{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == txtNhietdo{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == txtHA{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == txtNhiptho{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == txtCannang{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == txtCao{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == txtBMI{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == txtVongbung{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == txtMattrai_NoK{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == txtMatphai_NoK{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == txtMatphai_CoK{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == txtMattrai_CoK{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == txtNiemmac{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtTimmach{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtHohap{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtTieuhoa{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtTietlieu{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtCoXuongKhop{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtNoitiet{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtThankinh{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtTamthan{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtSanphukhoa{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtNgoaikhoa{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtTaimuihong{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtRHM{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtMat{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtDalieu{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtDinhduong{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtVandong{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtDanhgia{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtKhac{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtHuyethoc{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtSinhhoaNT{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtSinhhoamau{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtSieuam{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtTuvan{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == txtBacsikham{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == txtOther{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else{
            return true
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }

}

