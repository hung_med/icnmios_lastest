//
//  LookupResultVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

protocol AddChangeMaBNDelegate {
    func resultChangeMaBN(maBN:String)
}

class AddChangeMaBN: BaseViewController, UITextFieldDelegate,CustomComboxBoxListDelegate, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var buttonExit: UIButton!
    @IBOutlet weak var buttonDone: UIButton!
    
    @IBOutlet weak var txtMa: FWFloatingLabelTextField!
    @IBOutlet weak var txtPassword: FWFloatingLabelTextField!
   
    @IBOutlet weak var lblWarning: UILabel!
    @IBOutlet weak var lblMessageText: UILabel!
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var comboboxBtn: UIButton!
    
    var organizeID:Int?=0
    var maBN:String? = nil
    var medicalUnits: [MedicalUnit]?
    var medicalProfile:MedicalProfile?
    var delegate:AddChangeMaBNDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        myView.layer.cornerRadius = 10
        myView.layer.borderWidth = 1
        myView.layer.masksToBounds = true
        buttonDone.layer.cornerRadius = 4
        buttonDone.layer.masksToBounds = true
        buttonExit.layer.cornerRadius = 4
        buttonExit.layer.masksToBounds = true
        comboboxBtn.layer.borderWidth = 1
        comboboxBtn.layer.borderColor = UIColor.lightGray.cgColor
        comboboxBtn.layer.cornerRadius = 4
        comboboxBtn.layer.masksToBounds = true
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        self.txtMa.delegate = self
        self.txtPassword.delegate = self
        if IS_IPHONE_5{
            self.comboboxBtn.titleLabel?.font = UIFont.systemFont(ofSize: 13.0)
        }
        self.comboboxBtn.setTitle("BVĐK MEDLATEC - 42 Nghĩa Dũng", for: UIControlState.normal)
        self.txtMa.text = maBN
//        if let organizeID = self.medicalProfile?.organizeID{
//            self.organizeID = organizeID
//            self.getMedicalUnit(organizeID: organizeID)
//        }else{
//            organizeID = 1
//        }
        self.organizeID = 1
        lblMessageText.lineBreakMode = .byWordWrapping
        lblMessageText.numberOfLines=0
        lblMessageText.sizeToFit()
        lblMessageText.adjustsFontSizeToFitWidth = true
        lblMessageText.text = "Mã bệnh nhân là mã khám chữa bệnh được các cơ sở y tế có liên kết với ứng dụng iCNM cung cấp cho khám chữa bệnh tại đơn vị đó"
    }
    
    func getMedicalUnit(organizeID:Int){
        let progress = showProgress()
        self.medicalUnits = [MedicalUnit]()
        MedicalUnit.getAllMedicalUnit(success: { (data) in
            if data.count > 0 {
                self.medicalUnits?.append(contentsOf: data)
                for i in 0..<Int((self.medicalUnits?.count)!){
                    let organizeId = self.medicalUnits?[i].organizeID
                    if organizeID == organizeId{
                        let organizeName = self.medicalUnits?[i].organizeName
                        self.comboboxBtn.setTitle(organizeName, for: UIControlState.normal)
                        self.comboboxBtn.setTitleColor(UIColor(hexColor: 0x1976D2, alpha: 1.0), for: UIControlState.normal)
                        self.hideProgress(progress)
                        return
                    }
                }
                
            } else {
                self.hideProgress(progress)
            }
        }, fail: { (error, response) in
           
        })
        self.hideProgress(progress)
    }
    
    @IBAction func showListUnit(_ sender: Any) {
        
    }

    func gobackView(organizeId:Int, name:String){
        self.organizeID = organizeId
        comboboxBtn.setTitle(name, for: UIControlState.normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtMa.resignFirstResponder()
        self.txtPassword.resignFirstResponder()
        return true
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    @IBAction func btnDone(_ sender: Any) {
        if organizeID == 0{
            self.lblWarning.text = "Vui lòng chọn đơn vị y tế"
            self.lblWarning.lineBreakMode = .byWordWrapping
            self.lblWarning.numberOfLines = 0
            self.lblWarning.adjustsFontSizeToFitWidth = true
            return
        }else{
            if let pid = self.txtMa.text, pid.isEmpty{
                self.lblWarning.text = "Vui lòng chọn và nhập đầy đủ thông tin"
                self.lblWarning.lineBreakMode = .byWordWrapping
                self.lblWarning.numberOfLines = 0
                self.lblWarning.adjustsFontSizeToFitWidth = true
                return
            } else if let pwd = self.txtPassword.text, pwd.isEmpty {
                self.lblWarning.text = "Vui lòng chọn và nhập đầy đủ thông tin"
                self.lblWarning.lineBreakMode = .byWordWrapping
                self.lblWarning.numberOfLines = 0
                self.lblWarning.adjustsFontSizeToFitWidth = true
                return
            }  else {
                if let medicalProfileID = medicalProfile?.medicalProfileID, let pid = self.txtMa.text, let pwd = self.txtPassword.text{
                    self.checkLookUpResult(medicalProfileID:medicalProfileID, organizeID: organizeID!, pid:pid.trim(), pwd: pwd)
                }
            }
        }
    }
    
    func checkLookUpResult(medicalProfileID:Int, organizeID:Int, pid:String, pwd:String){
        let progress = self.showProgress()
        let updatePID = UpdatePID()
        updatePID.checkUpdatePID(medicalProfileID:medicalProfileID, organizeID:String(organizeID), pid: pid, sid: "", password: pwd, success: {  (result) in
            self.hideProgress(progress)
            if (result?.doctorID.isEmpty)!{
                self.lblWarning.text = "Mã bệnh nhân của bạn không tồn tại trên hệ thống iCNM"
                self.lblWarning.lineBreakMode = .byWordWrapping
                self.lblWarning.numberOfLines = 0
                self.lblWarning.adjustsFontSizeToFitWidth = true
                return
            }else{
                if let patientRes = result {
                    print("success", patientRes)
                    self .dismiss(animated: true) {
                        self.delegate?.resultChangeMaBN(maBN: pid)
                    }
                }
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtMa{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 16
        }else if textField == txtPassword{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 16
        }else{
            return true
        }
    }
    
    @IBAction func btnExit(_ sender: Any) {
        self .dismiss(animated: true) { 
            
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}
