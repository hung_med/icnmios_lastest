//
//  CustomComboxBoxList.swift
//  iCNM
//
//  Created by Mac osx on 8/4/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

protocol TemplateListComboboxDelegate {
    func gobackView(name:String)
   // func getDefaultOrganizeName(name:String)
}

class TemplateListCombobox: UITableViewController {
    
    @IBOutlet var myTableView: UITableView!
    var listNames: [String]?
    var currentName:String?
    var delegate:TemplateListComboboxDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        myTableView.delegate = self
        myTableView.dataSource = self
        let nib = UINib(nibName: "CustomComboxCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.listNames?.count)!
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomComboxCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        let name = self.listNames?[indexPath.row]
        cell.lblName.text = name
        cell.lblName.font = UIFont.systemFont(ofSize: 12)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let name = self.listNames?[indexPath.row]{
            self.delegate?.gobackView(name: name)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 25
    }
}
