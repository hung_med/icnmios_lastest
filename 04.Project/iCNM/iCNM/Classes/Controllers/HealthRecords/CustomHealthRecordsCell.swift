//
//  CustomLookUpResultCell.swift
//  iCNM
//
//  Created by Mac osx on 7/14/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class CustomHealthRecordsCell: UITableViewCell {

    @IBOutlet weak var groupView: UIView!
    @IBOutlet weak var profileImage: CustomImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var lblDependentPerson: UILabel!
    @IBOutlet weak var lblCreateDate: UILabel!
    @IBOutlet weak var lblPID: UILabel!
    @IBOutlet weak var lblPIDValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         let f = contentView.frame
         let fr = UIEdgeInsetsInsetRect(f, UIEdgeInsetsMake(10, 10, 10, 15))
         contentView.frame = fr
         addShadow(cell: self)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func addShadow(cell:UITableViewCell) {
        cell.layer.cornerRadius = 4
        cell.layer.masksToBounds = true
        
        cell.layer.masksToBounds = false
        cell.layer.shadowOffset = CGSize(width: 0, height:5)
        cell.layer.shadowColor = UIColor.groupTableViewBackground.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowRadius = 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
