//
//  GenQRCodeViewController.swift
//  iCNM
//
//  Created by Thanh Huyen on 7/27/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift

class GenQRCodeViewController: UIViewController {
    
    @IBOutlet weak var lbUserName: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lbUserId: UILabel!
    @IBOutlet weak var lbPhone: UILabel!
    @IBOutlet weak var imgQRCode: UIImageView!
    
    var currentUser : User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // custom img
        imgAvatar.layer.cornerRadius = 35
        imgAvatar.layer.masksToBounds = true
        imgAvatar.layer.borderWidth = 2.5
        imgAvatar.layer.borderColor = UIColor.init(hex:"1D6EDC").cgColor
        
        imgQRCode.layer.borderWidth = 2.5
        imgQRCode.layer.borderColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        // Setup the Search Controller
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(GenQRCodeViewController.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        // automatic gen qr code
        guard let name = currentUser?.userDoctor?.userInfo?.name else {return}
        guard let id = currentUser?.userDoctor?.userInfo?.id else {return}
        
        guard let dateBirthday = currentUser?.userDoctor?.userInfo?.birthday else {return}
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let birthday = dateFormatter.string(from: dateBirthday)
        
        guard let gender = currentUser?.userDoctor?.userInfo?.gender else {return}
        var sex = ""
        if gender == "M" {
            sex = "Nam"
        } else {
            sex = "Nữ"
        }
        
        var qrcode = ""
        lbUserName.text = "Họ tên: \(name)"
        lbUserId.text = "Mã ID: \(id)"
        // check avatar
        if let avatar = currentUser?.userDoctor?.userInfo?.avatar {
            if let url = URL(string: API.baseURLImage + API.iCNMImage + "\(id)/\(avatar)".trim()) {
                imgAvatar.sd_setImage(with: url, completed: nil)
            }
        } else {
            imgAvatar.image = UIImage(named: "avatar_default")
        }
        
        // check phone number
        if let phone = currentUser?.userDoctor?.userInfo?.phone { // login = sdt
            lbPhone.text = "Số điện thoại: \(phone)"
            qrcode = "Mã ID: \(id)\nHọ tên: \(name)\nNgày sinh: \(birthday)\nSố điện thoại: \(phone)\nGiới tính: \(sex)"
        } else { // login = fb hoac google
            lbPhone.isHidden = true
            qrcode = "Mã ID: \(id)\nHọ tên: \(name)\nNgày sinh: Chưa cập nhật\nSố điện thoại: Chưa cập nhật\nGiới tính: Chưa cập nhật"
        }
        
        let data = qrcode.data(using: .utf8, allowLossyConversion: true)
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter?.setValue(data, forKey: "inputMessage")
        
        let ciImage = filter?.outputImage
        
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let transformImage = ciImage?.applying(transform)
        
        let image = UIImage(ciImage: transformImage!)
        imgQRCode.image = image
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }

}
