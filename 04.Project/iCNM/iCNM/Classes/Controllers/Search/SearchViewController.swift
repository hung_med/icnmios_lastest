//
//  SearchViewController.swift
//  iCNM
//
//  Created by Hoang Van Trung on 7/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import ESPullToRefresh

class SearchViewController: BaseTableViewController, UISearchBarDelegate {
    var searchController: UISearchBar!
    var searchSuggest = Search()
    var searchResult = Search()
    var paramSearch = [String: Any]()
    var arrScopeTitle = ["Tất cả", "Bệnh", "Triệu chứng", "Bác sĩ", "Đơn vị y tế", "Hỏi đáp", "Tin tức", "Xét nghiệm"]
    var arrSearchHistory = [String]()
    var arrKeySearch = [String]()
    var filterStr: String = "Tất cả"
    let defaults:UserDefaults = UserDefaults.standard
    var pageNumber: Int = 1
    var txtSearchInput: String?
    @IBOutlet weak var scopeSelected: UISegmentedControl!
    private var footerScrollView = ESRefreshFooterAnimator(frame:CGRect.zero)
    
    var searchView: SuggestSearchView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.configureSearchController()
        paramSearch = ["findState":"Tất cả",
                       "pageNumber":1]
        self.getSearchSuggest(param: paramSearch)
        
//        tableView.es_addInfiniteScrolling(animator:footerScrollView) { [weak self] in
//            if let weakSelf = self {
//                weakSelf.loadMoreData()
//                weakSelf.tableView.es_stopLoadingMore()
//            }
//        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.searchController.isActive = true
        if (self.searchController.text?.isEmpty)! {
            if #available(iOS 9.0, *) {
                self.showKeyboard()
            } else {
                self.perform(#selector(self.showKeyboard), with: nil, afterDelay: 0.3)
            }
        }
    }

    func showKeyboard() {
        if self.searchController.responds(to: #selector(self.searchController.becomeFirstResponder)) {
            self.searchController.becomeFirstResponder()
        }
    }

    func configureSearchController() {
        searchController = UISearchBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 300, height: 40))
        
        searchController.delegate = self
        searchController.placeholder = "Tìm kiếm"
        searchController.autocapitalizationType = UITextAutocapitalizationType.none
        searchController.keyboardType = UIKeyboardType.default
        self.navigationItem.titleView = searchController
        navigationItem.titleView?.frame = searchController.frame
        searchController.showsBookmarkButton = false

        for subView in searchController.subviews {
            for secondSubView in subView.subviews {
                if secondSubView is UITextField {
                    secondSubView.backgroundColor = UIColor(red: 221/255, green: 221/255, blue: 221/255, alpha: 1)
                }
            }
        }
        searchController.showsSearchResultsButton = false
        if #available(iOS 9.0, *) {
            (UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self])).tintColor = UIColor.blue
        } else {

        }

        if txtSearchInput != nil {
            searchController.text = txtSearchInput
//            self.searchText(withString: txtSearchInput!)
        }
    }
    
    func showSuggestSearchView(keySearch: String) {
        if searchView == nil {
            searchView = SuggestSearchView(frame: CGRect(origin: CGPoint.zero, size: self.view.frame.size))
            self.view.addSubview(searchView!)
        }
        searchView?.showView(keySearch: keySearch)
        tableView.isScrollEnabled = false
        searchView?.executeSearchBlock = { [unowned self] (keysearch) -> Void in
            self.searchController.text = keysearch
            let param: [String: Any] = ["query":keysearch,
                                        "findState":"\(self.filterStr)",
                "pageNumber":1]
            self.getSearch(param: param)
            self.saveHistorySearch(key: keysearch)
            self.searchView?.hiddenView()
            self.tableView.isScrollEnabled = true
        }
    }
    
    func getSearchSuggest(param: [String: Any]) {
        let progress = self.showProgress()
        searchSuggest.getSearchSuggest(param:param, success: { (result) in
            if let new = result {
                self.searchSuggest = new
                self.tableView.reloadData()
            } else {
                
            }
            self.hideProgress(progress)
        }) { (error, response) in
            self.hideProgress(progress)
            
        }
    }
    
    func getSearch(param: [String: Any]) {
        let progress = self.showProgress()
        searchResult.getSearch(param:param, success: { (result) in
            if let new = result {
                self.searchSuggest = new
                self.tableView.reloadData()
            } else {
                
            }
            self.hideProgress(progress)
        }) { (error, response) in
            self.hideProgress(progress)
        }
    }
    
    func saveHistorySearch(key: String!) {
        arrKeySearch.insert(key, at: 0)
        defaults.setValue(arrKeySearch, forKey: "historySearch")
        defaults.synchronize()
    }
    
    func loadMoreData() {
        pageNumber += 1
        let param: [String: Any] = ["query":searchController.text!,
                                    "findState":"\(filterStr)",
            "pageNumber":1]
        self.getSearch(param: param)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return searchSuggest.dictionaries.count
        case 1:
            return searchSuggest.symptons.count
        case 2:
            return searchSuggest.usersDoctors.count
        case 3:
            return searchSuggest.locations.count
        case 4:
            return searchSuggest.questions.count
        case 5:
            return searchSuggest.newsCategories.count
        case 6:
            return searchSuggest.meanning.count
        default:
            return 0
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return arrScopeTitle.count - 1
    }
    
//    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return arrScopeTitle[section+1]
//    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            if searchSuggest.dictionaries.count > 0 {
                return 30
            }
        case 1:
            if searchSuggest.symptons.count > 0 {
                return 30
            }
        case 2:
            if searchSuggest.usersDoctors.count > 0 {
                return 30
            }
        case 3:
            if searchSuggest.locations.count > 0 {
                return 30
            }
        case 4:
            if searchSuggest.questions.count > 0 {
                return 30
            }
        case 5:
            if searchSuggest.newsCategories.count > 0 {
                return 30
            }
        case 6:
            if searchSuggest.meanning.count > 0 {
                return 30
            }
        default:
            return 0
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerSection = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 30))
        headerSection.backgroundColor = UIColor(red: 204/255, green: 221/255, blue: 57/255, alpha: 57/255)
        let titleSection = UILabel(frame: CGRect(x: 10, y: 5, width: UIScreen.main.bounds.width - 10, height: 20))
        titleSection.font = UIFont.boldSystemFont(ofSize: 18)
        titleSection.text = arrScopeTitle[section+1]
        headerSection.addSubview(titleSection)
        return headerSection
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        switch indexPath.section {
        case 0:
            cell.textLabel?.text = searchSuggest.dictionaries[indexPath.row].name
            break
        case 1:
            cell.textLabel?.text = searchSuggest.symptons[indexPath.row].name
            break
        case 2:
            cell.textLabel?.text = searchSuggest.usersDoctors[indexPath.row].users?.name
            break
        case 3:
            cell.textLabel?.text = searchSuggest.locations[indexPath.row].name
            break
        case 4:
            cell.textLabel?.text = searchSuggest.questions[indexPath.row].title
            break
        case 5:
            cell.textLabel?.text = searchSuggest.newsCategories[indexPath.row].news.title
            break
        case 6:
            cell.textLabel?.text = searchSuggest.meanning[indexPath.row].testName
            break
        default:
            break
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    // MARK: - UISearchBarDelegate

    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        self.showSuggestSearchView(keySearch: searchController.text!)
        txtSearchInput = nil
        searchController.showsCancelButton = true
        return true
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchController.showsCancelButton = false
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let keyword = searchBar.text
        if(keyword == "") {
            self.tableView.isHidden=false
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        if !(searchBar.text?.isEmpty)! {
            let param: [String: Any] = ["query":searchBar.text!,
                                        "findState":"\(filterStr)",
                "pageNumber":pageNumber]
            self.getSearch(param: param)
            self.saveHistorySearch(key: searchBar.text!)
        }
//        searchController.isActive = false
        self.searchView?.hiddenView()
        self.tableView.isScrollEnabled = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchController.text = searchBar.text
        self.searchView?.hiddenView()
    }

    @IBAction func filterSearch(_ sender: Any) {
        filterStr = arrScopeTitle[scopeSelected.selectedSegmentIndex]
        let param: [String: Any] = ["query":searchController.text!,
            "findState":"\(filterStr)",
            "pageNumber":1]
        self.getSearch(param: param)
    }
}
