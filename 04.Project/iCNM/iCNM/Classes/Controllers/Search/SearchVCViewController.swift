//
//  SearchVCViewController.swift
//  iCNM
//
//  Created by Hoang Van Trung on 8/21/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import FontAwesomeKit
import SVPullToRefresh
import GoogleMaps
import FontAwesome_swift
import Toaster
class SearchVCViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    private var myTableView: UITableView!
    var scopeSelected = ScrollableSegmentedControl()
    var searchSuggest = Search()
    var searchResult = Search()
    var dictionaries = [Dictionaries]()
    var symptons = [Symptons]()
    var meanning = [Meanning]()
    var newsCategories = [NewsCategories]()
    var questions = [Question]()
    var usersDoctors = [FeaturedDoctor]()
    var locations = [LocationNearBy]()
    var users = [Users]()
    private var myItemCount:Int=0
    var arrScopeTitle = ["Tất cả", "Bệnh", "Triệu chứng", "Bác sĩ", "Đơn vị y tế", "Hỏi Đáp", "Tin tức", "Xét nghiệm"]
    
    var arrSearchHistory = [String]()
    var arrKeySearch = [String]()
    var filterStr: String = "Tất cả"
    let defaults:UserDefaults = UserDefaults.standard
    var txtSearchInput: String?
    var isPushNav:Bool?=false
//    var mySearchBar: UISearchBar!
    var searchView: SuggestSearchView?
    
    private let indicatorSize:CGFloat = 28.0
    private let loadingProgressLineWidth:CGFloat = 3.0
    private var pageNumber: Int = 1
    var userDefaults = UserDefaults.standard
    var verticalTopSpace: CGFloat = 104
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // ===================================== init
        self.tabBarController?.tabBar.isHidden = true
        self.title=""
        let screenW = UIScreen.main.bounds.width
        let screenH = UIScreen.main.bounds.height
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                verticalTopSpace = 124
            default:
                print("unknown")
            }
        }
        
        if #available(iOS 11.0, *) {
            
        }else{
            verticalTopSpace = 104
        }
        
        if IS_IPHONE_X || IS_IPHONE_XS{
            verticalTopSpace = 128
        }
            
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        // Setup the Search Controller
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(SearchVCViewController.back(sender:)), for: UIControlEvents.touchUpInside)
            
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        // ===================================== create
        // ------------ mySearchBar
        searchBar.autocapitalizationType = UITextAutocapitalizationType.none
        searchBar.keyboardType = UIKeyboardType.default
        searchBar.showsBookmarkButton = false
        searchBar.showsSearchResultsButton = false
        searchBar.tintColor = UIColor.gray
        
        // ------------ TableView
        myTableView = UITableView(frame: CGRect(x: 0.0, y: verticalTopSpace, width: screenW, height: screenH - verticalTopSpace), style: .plain)
        myTableView.dataSource = self
        myTableView.delegate = self
        myTableView.contentInset=UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        myTableView.rowHeight = UITableViewAutomaticDimension
        let nib = UINib(nibName: "CustomSearchResultCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        myTableView.addInfiniteScrolling { 
            if self.scopeSelected.selectedSegmentIndex != 0 {
                self.pageNumber = self.pageNumber + 1
                if (self.searchBar.text?.trim().isEmpty)! {
                    let paramSearch: [String : Any] = ["findState":self.filterStr,
                                                       "pageNumber":self.pageNumber]
                    self.searchSuggest.getSearchSuggest(param:paramSearch, success: { (result) in
                        if let new = result {
                            if new.dictionaries.count > 0 {
                                self.dictionaries.append(contentsOf: new.dictionaries)
                            }
                            if new.symptons.count > 0 {
                                self.symptons.append(contentsOf: new.symptons)
                            }
                            if new.meanning.count > 0 {
                                self.meanning.append(contentsOf: new.meanning)
                            }
                            if new.newsCategories.count > 0 {
                                self.newsCategories.append(contentsOf: new.newsCategories)
                            }
                            if new.questions.count > 0 {
                                self.questions.append(contentsOf: new.questions)
                            }
                            if new.usersDoctors.count > 0 {
                                self.usersDoctors.append(contentsOf: new.usersDoctors)
                            }
                            if new.locations.count > 0 {
                                self.locations.append(contentsOf: new.locations)
                            }
                            self.myTableView.reloadData()
                            self.myTableView.infiniteScrollingView.stopAnimating()
                        } else {
                            self.myTableView.infiniteScrollingView.stopAnimating()
                        }
                    }) { (error, response) in
                        self.myTableView.infiniteScrollingView.stopAnimating()
                    }
                } else {
                    let param: [String: Any] = ["query":self.searchBar.text!.trim(),
                                                "findState":"\(self.filterStr)",
                        "pageNumber":self.pageNumber]
                    self.searchResult.getSearch(param:param, success: { (result) in
                        if let new = result {
                            if new.dictionaries.count > 0 {
                                self.dictionaries.append(contentsOf: new.dictionaries)
                            }
                            if new.symptons.count > 0 {
                                self.symptons.append(contentsOf: new.symptons)
                            }
                            if new.meanning.count > 0 {
                                self.meanning.append(contentsOf: new.meanning)
                            }
                            if new.newsCategories.count > 0 {
                                self.newsCategories.append(contentsOf: new.newsCategories)
                            }
                            if new.questions.count > 0 {
                                self.questions.append(contentsOf: new.questions)
                            }
                            if new.usersDoctors.count > 0 {
                                self.usersDoctors.append(contentsOf: new.usersDoctors)
                            }
                            if new.locations.count > 0 {
                                self.locations.append(contentsOf: new.locations)
                            }
                            self.myTableView.reloadData()
                            self.myTableView.infiniteScrollingView.stopAnimating()

                        } else {
                            self.myTableView.infiniteScrollingView.stopAnimating()
                        }
                    }) { (error, response) in
                        self.myTableView.infiniteScrollingView.stopAnimating()
                        print(error)
                    }
                }
            } else {
                self.myTableView.infiniteScrollingView.stopAnimating()
            }
        }
        let scopeBar = UIView(frame: CGRect(x: 0, y: 0, width: screenW, height: 114))
        scopeBar.backgroundColor = UIColor.white
        scopeSelected.frame = CGRect(x: 0, y: 0, width: 800, height: 40)
        scopeSelected.addTarget(self, action: #selector(SearchVCViewController.filterSearch), for: .valueChanged)
        var indexCate: Int = 0
        for copeTitle in arrScopeTitle {
            scopeSelected.insertSegment(withTitle: copeTitle, at: indexCate)
            indexCate += 1
        }
        scopeSelected.underlineSelected = true
        scopeSelected.segmentStyle = .textOnly
        scopeSelected.backgroundColor = UIColor.white
        scopeSelected.tintColor = UIColor(hex:"1976D2")
        if (self.searchBar.text?.trim().isEmpty)! {
            if #available(iOS 9.0, *) {
                self.showKeyboard()
            } else {
                self.perform(#selector(self.showKeyboard), with: nil, afterDelay: 0.1)
            }
        }
        // add it in a scrollView, because ill got too much categories here. Just if you need that:
        let scrollView  = UIScrollView()
        scrollView.frame = CGRect(x: 0, y: 0, width: screenW, height: 200)
        scrollView.contentSize = CGSize(width: scopeSelected.frame.size.width + 16,height: scopeSelected.frame.size.height-1)
        scrollView.showsHorizontalScrollIndicator = false;
        // set first category
        scopeSelected.selectedSegmentIndex = 0
        scrollView.addSubview(scopeSelected)
        
        scopeBar.addSubview(scrollView)
        self.view.addSubview(scopeBar)
        
        // ===================================== add
        self.view.addSubview(myTableView)
        
        if txtSearchInput != nil {
            searchBar.text = txtSearchInput
        }
        
        let paramSearch: [String : Any] = ["findState":"Tất cả",
                       "pageNumber":1]
        self.getSearchSuggest(param: paramSearch)
        
        self.getResultAds(typeID: 9)
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func showSuggestSearchView(keySearch: String) {
        if self.isPushNav!{
            return
        }
        if searchView == nil {
            let positionY = CGFloat((self.navigationController?.navigationBar.frame.origin.y)!) + CGFloat((self.navigationController?.navigationBar.frame.size.height)!)
            searchView = SuggestSearchView(frame: CGRect(origin: CGPoint(x: 0, y: positionY), size: self.view.frame.size))
            self.view.addSubview(searchView!)
        }
        searchView?.showView(keySearch: keySearch)
        searchView?.executeSearchBlock = { [unowned self] (keysearch) -> Void in
            self.searchBar.text? = keysearch
            let param: [String: Any] = ["query":keysearch,
                                        "findState":"\(self.filterStr)",
                "pageNumber":1]
            self.getSearch(param: param)
            self.saveHistorySearch(key: keysearch)
            self.searchView?.hiddenView()
            self.searchBar.resignFirstResponder()
        }
        searchView?.hiddenKeyboardBlock = { [unowned self] () -> Void in
            self.searchBar.resignFirstResponder()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.searchBar.resignFirstResponder()
    }
    
    func getResultAds(typeID:Int){
        ResultAds().getResultAds(typeID:typeID, success: { (data) in
            if data != nil {
                if let img = data?.img, let linkAds = data?.link{
                    let urlString = URL(string:img)
                    if let data = try? Data(contentsOf: urlString!){
                        DispatchQueue.main.async {
                            let image: UIImage = UIImage(data: data)!
                            let imageView = UIImageView(image: image)
                            imageView.frame = CGRect(x: 0, y: screenSizeHeight, width: screenSizeWidth, height: screenSizeHeight/10)
                            let btnClose = UIButton(frame: CGRect(x: screenSizeWidth-20, y: 0, width: 20, height: 20))
                            let imageClose = UIImage(named: "icon_close")?.maskWithColor(color: UIColor(hexColor: 0xFF1E17, alpha: 1.0))
                            btnClose.setImage(imageClose, for: .normal)
                            let gestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(SearchVCViewController.handlerCloseAds))
                            btnClose.isUserInteractionEnabled = true
                            btnClose.addGestureRecognizer(gestureRecognizer1)
                            
                            imageView.addSubview(btnClose)
                            imageView.accessibilityHint = linkAds
                            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SearchVCViewController.openLinkAds))
                            imageView.isUserInteractionEnabled = true
                            imageView.addGestureRecognizer(gestureRecognizer)
                            self.myTableView.tableHeaderView = imageView
                        }
                    }
                }
                
            } else {
                
            }
        }, fail: { (error, response) in
            
        })
    }
    
    func openLinkAds(sender : UITapGestureRecognizer){
        let image = sender.view as! UIImageView?
        let link = image?.accessibilityHint
        guard let url = URL(string: link!) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func handlerCloseAds(sender : UITapGestureRecognizer){
        self.myTableView.tableHeaderView = nil
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Call API
    func getSearchSuggest(param: [String: Any]) {
        //let progress = self.showProgress()
        searchSuggest.getSearchSuggest(param:param, success: { (result) in
            if let new = result {
                self.dictionaries = new.dictionaries
                self.symptons = new.symptons
                self.meanning = new.meanning
                self.newsCategories = new.newsCategories
                self.questions = new.questions
                self.usersDoctors = new.usersDoctors
                self.locations = new.locations
                self.myTableView.reloadData()
            } else {
                
            }
            //self.hideProgress(progress)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                let pointScroll = CGPoint(x: 0, y: 0)
                self.myTableView.setContentOffset(pointScroll, animated: true)
                Loader.removeLoaderFrom(self.myTableView)
            }
        }) { (error, response) in
            //self.hideProgress(progress)
            Loader.removeLoaderFrom(self.myTableView)
        }
    }
    
    func getSearch(param: [String: Any]) {
        let progress = self.showProgress()
        searchResult.getSearch(param:param, success: { (result) in
            if let new = result {
                self.dictionaries = new.dictionaries
                self.symptons = new.symptons
                self.meanning = new.meanning
                self.newsCategories = new.newsCategories
                self.questions = new.questions
                self.usersDoctors = new.usersDoctors
                self.locations = new.locations
                self.myTableView.reloadData()
            } else {
                
            }
            self.hideProgress(progress)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                let pointScroll = CGPoint(x: 0, y: 0)
                self.myTableView.setContentOffset(pointScroll, animated: true)
            }
        }) { (error, response) in
            self.hideProgress(progress)
        }
    }
    
    func saveHistorySearch(key: String!) {
        if !key.isEmpty {
            if !arrKeySearch.contains(key) {
                arrKeySearch.insert(key, at: 0)
            } else {
                arrKeySearch.remove(at: arrKeySearch.index(of: key)!)
                arrKeySearch.insert(key, at: 0)
            }
            defaults.setValue(arrKeySearch, forKey: "historySearch")
            defaults.synchronize()
        }
    }
    
    func showKeyboard() {
        if self.searchBar.responds(to: #selector(self.searchBar.becomeFirstResponder)) {
            self.searchBar.becomeFirstResponder()
        }
    }
    
    func configureCancelBarButton() {
        let cancelButton = UIBarButtonItem()
        cancelButton.action = #selector(self.searchBarCancelButtonClicked(_:))
        cancelButton.title = "Cancel"
        cancelButton.target = self
        self.navigationItem.setRightBarButton(cancelButton, animated: true)
    }
    // MARK: - UITableViewDelegate
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.section {
        case 0:
            let dictionaries = self.dictionaries[indexPath.row]
            let detailDictionGroupVC = DetailDictionGroupVC(nibName: "DetailDictionGroupVC", bundle: nil)
            let medicalDicID = dictionaries.medicalDicID
            detailDictionGroupVC.titleDiction = dictionaries.name
                detailDictionGroupVC.getMedicalDictionaryDetail(medicalDicID:medicalDicID)
            isPushNav = true
            self.navigationController?.pushViewController(detailDictionGroupVC, animated: true)
        case 1:
            let symptons = self.symptons[indexPath.row]
            let detailMedicalSymptonVC = DetailMedicalBodyGroup(nibName: "DetailMedicalBodyGroup", bundle: nil)
            let symptonID = symptons.medicalSymptonID
            detailMedicalSymptonVC.titleMedicalBody = symptons.name
                detailMedicalSymptonVC.getMedicalSymptonByID(symptonByID:symptonID)
            isPushNav = true
            self.navigationController?.pushViewController(detailMedicalSymptonVC, animated: true)
        case 2:
            let usersDoctors = self.usersDoctors[indexPath.row]
            let profileDoctorVC = ProfileDoctorVC(nibName: "ProfileDoctorVC", bundle: nil)
            profileDoctorVC.featuredDoctor = usersDoctors
            isPushNav = true
            self.navigationController?.pushViewController(profileDoctorVC, animated: true)
        case 3:
            let unitDetailVC = UnitDetailController(nibName: "UnitDetailController", bundle: nil)
            unitDetailVC.locationNearBy = self.locations[indexPath.row]
            isPushNav = true
            if userDefaults.object(forKey: "LAT") != nil && userDefaults.object(forKey: "LONG") != nil{
                let lat = userDefaults.object(forKey: "LAT") as! String
                let long = userDefaults.object(forKey: "LONG") as! String
                unitDetailVC.savedCameraPosition =
                    GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(long)!, zoom: 16.0)
            }
            self.navigationController?.pushViewController(unitDetailVC, animated: true)
        case 4:
            let questions = QuestionAnswer()
            questions.question = self.questions[indexPath.row]
            let questionID = questions.question?.id
            questions.getAnswerIDFromQuestionID(questionID: questionID!, success: {  (result) in
                if result == nil{
                    return
                }else{
                    questions.answer = result?.answer
                    questions.userAsk = result?.userAsk
                    questions.userOfDoctor = result?.userOfDoctor
                    questions.doctor = result?.doctor
                    questions.specialist = result?.specialist
                    questions.thanked = (result?.thanked)!
                    questions.liked = (result?.liked)!
                    questions.shared = (result?.shared)!
                    
                    let qADetailVC = StoryboardScene.Main.qaDetail.instantiate()
                    qADetailVC.questionAnswer = questions
                    self.isPushNav = true
                    self.navigationController?.pushViewController(qADetailVC, animated: true)
                }
            }, fail: { (error, response) in
                
            })
            
        case 5:
            let news = self.newsCategories[indexPath.row].news
            let newsDetailVC = StoryboardScene.Main.newsDetailViewController.instantiate()
            newsDetailVC.newsModelInput.categoryID = news.categoryID
            newsDetailVC.newsModelInput.newsID = news.newsID
            newsDetailVC.newsModelInput.title = news.title
            newsDetailVC.newsModelInput.img = news.img
            newsDetailVC.newsModelInput.description = news.description
            
            newsDetailVC.newsCategoryInput.id = self.newsCategories[indexPath.row].category.categoryID
            newsDetailVC.newsCategoryInput.name = self.newsCategories[indexPath.row].category.categoryName

            isPushNav = true
            self.navigationController?.pushViewController(newsDetailVC, animated: true)
           // break
        case 6:
            let meanning = self.meanning[indexPath.row]
            let detailDictMedicalVC = DetailDictMedicalVC(nibName: "DetailDictMedicalVC", bundle: nil)
            detailDictMedicalVC.headerString = meanning.testName
            detailDictMedicalVC.meaning1 = meanning.meaning1
            detailDictMedicalVC.meaning2 = meanning.meaning2
            isPushNav = true
            self.navigationController?.pushViewController(detailDictMedicalVC, animated: true)
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            if self.dictionaries.count > 0 {
                return 30
            }
        case 1:
            if self.symptons.count > 0 {
                return 30
            }
        case 2:
            if self.usersDoctors.count > 0 {
                return 30
            }
        case 3:
            if self.locations.count > 0 {
                return 30
            }
        case 4:
            if self.questions.count > 0 {
                return 30
            }
        case 5:
            if self.newsCategories.count > 0 {
                return 30
            }
        case 6:
            if self.meanning.count > 0 {
                return 30
            }
        default:
            return 0
        }
        return 0
    }
    // MARK: - UITableViewDatasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrScopeTitle.count - 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.dictionaries.count
        case 1:
            return self.symptons.count
        case 2:
            return self.usersDoctors.count
        case 3:
            return self.locations.count
        case 4:
            return self.questions.count
        case 5:
            return self.newsCategories.count
        case 6:
            return self.meanning.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomSearchResultCell
        //cell.lblTitle?.numberOfLines = 0
        cell.lblTitle?.sizeToFit()
        switch indexPath.section {
        case 0:
            let keyStr = searchBar.text!.trim().folding(options: .diacriticInsensitive, locale: .current)
            let parentStr = self.dictionaries[indexPath.row].name.folding(options: .diacriticInsensitive, locale: .current)
            let range = (parentStr.lowercased() as NSString).range(of: keyStr.lowercased(), options: .caseInsensitive)

            let attribute = NSMutableAttributedString.init(string: self.dictionaries[indexPath.row].name)
            attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.red , range: range)
            cell.lblTitle?.attributedText = attribute
            if let mean = try! self.dictionaries[indexPath.row].mean.convertHtmlSymbols(){
                if !mean.isEmpty{
                    cell.lblDesc.text = mean
                }else{
                    cell.lblDesc.text = "Đang cập nhật dữ liệu"
                }
            }
            cell.imagThumb.isHidden = true
            cell.titleConstraint.constant = 8
            cell.descConstraint.constant = 8
            break
        case 1:
            let keyStr = searchBar.text!.trim().folding(options: .diacriticInsensitive, locale: .current)
            let parentStr = self.symptons[indexPath.row].name.folding(options: .diacriticInsensitive, locale: .current)
            let range = (parentStr.lowercased() as NSString).range(of: keyStr.lowercased(), options: .caseInsensitive)

            let attribute = NSMutableAttributedString.init(string: self.symptons[indexPath.row].name)
            attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.red , range: range)
            cell.lblTitle?.attributedText = attribute
            if let sympton = try! self.symptons[indexPath.row].mean.convertHtmlSymbols(){
                if !sympton.isEmpty{
                    cell.lblDesc.text = sympton
                }else{
                    cell.lblDesc.text = "Đang cập nhật dữ liệu"
                }
            }
            cell.imagThumb.isHidden = true
            cell.titleConstraint.constant = 8
            cell.descConstraint.constant = 8
            break
        case 2:
            let keyStr = searchBar.text!.trim().folding(options: .diacriticInsensitive, locale: .current)
            let parentStr = self.usersDoctors[indexPath.row].users?.name.folding(options: .diacriticInsensitive, locale: .current)
            let range = (parentStr!.lowercased() as NSString).range(of: keyStr.lowercased(), options: .caseInsensitive)

            let attribute = NSMutableAttributedString.init(string: (self.usersDoctors[indexPath.row].users?.name)!)
            attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.red , range: range)
            if let avatar = self.usersDoctors[indexPath.row].users?.avatar, let userId = self.usersDoctors[indexPath.row].users?.userID {
                let urlString = avatar.trimmingCharacters(in: .whitespaces)
                if urlString.isEmpty{
                    cell.imagThumb.image = UIImage(named: "avatar_default")
                }else{
                    let url = API.baseURLImage + "\(API.iCNMImage)" + "\(userId)" + "/\(urlString)"
                    cell.imagThumb.loadImageUsingUrlString(urlString: url)
                }
            }
            cell.imagThumb.layer.borderColor = UIColor(hexColor: 0x1D6EDC, alpha: 1.0).cgColor
            cell.imagThumb.layer.borderWidth = 2.0
            cell.imagThumb?.isHidden = false
            cell.imagThumb?.layer.cornerRadius = 18
            cell.imagThumb?.layer.masksToBounds = true
            cell.lblTitle?.attributedText = attribute
            cell.lblDesc.text = self.usersDoctors[indexPath.row].users?.address
            cell.titleConstraint.constant = 52
            cell.descConstraint.constant = 52
            break
        case 3:
            let keyStr = searchBar.text!.trim().folding(options: .diacriticInsensitive, locale: .current)
            let parentStr = self.locations[indexPath.row].name.folding(options: .diacriticInsensitive, locale: .current)
            let range = (parentStr.lowercased() as NSString).range(of: keyStr.lowercased(), options: .caseInsensitive)

            let attribute = NSMutableAttributedString.init(string: self.locations[indexPath.row].name)
            attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.red , range: range)
            cell.lblTitle?.attributedText = attribute
            cell.lblDesc?.text = self.locations[indexPath.row].address
            //let distance = self.locations[indexPath.row].distance
            //let distanceKm = distance/1000
            //cell.lbDistance.text = "\(Double(distanceKm).rounded(toPlaces: 2)) km"
            let locationObj = self.locations[indexPath.row]
            var urlImage = API.baseURLImage + "iCNMImage/" + "LocationImage/" +
                "\(locationObj.locationID)/" + locationObj.imageStr
            let result = Int(splitImageLocationToString(str: locationObj.imageStr))
            if result != self.locations[indexPath.row].locationID{
                urlImage = API.baseURLImage + "iCNMImage/" + "LocationImage/" + locationObj.imageStr
                let realImageUrl = urlImage.components(separatedBy: ";")[0]
                let url = realImageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                cell.imagThumb.loadImageUsingUrlString(urlString: url!)
            }else{
                let realImageUrl = urlImage.components(separatedBy: ";")[0]
                let url = realImageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                cell.imagThumb.loadImageUsingUrlString(urlString: url!)
            }

            cell.imagThumb.layer.borderColor = UIColor.clear.cgColor
            cell.imagThumb?.isHidden = false
            cell.imagThumb?.layer.cornerRadius = 0
            cell.imagThumb?.layer.masksToBounds = true
            cell.titleConstraint.constant = 52
            cell.descConstraint.constant = 52
            break
        case 4:
            let keyStr = searchBar.text!.trim().folding(options: .diacriticInsensitive, locale: .current)
            let parentStr = self.questions[indexPath.row].title?.folding(options: .diacriticInsensitive, locale: .current)
            let range = (parentStr?.lowercased() as! NSString).range(of: keyStr.lowercased(), options: .caseInsensitive)

            let attribute = NSMutableAttributedString.init(string: self.questions[indexPath.row].title!)
            attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.red , range: range)
            cell.lblTitle?.attributedText = attribute
            cell.lblDesc?.text = self.questions[indexPath.row].content
            cell.imagThumb?.isHidden = true
            cell.titleConstraint.constant = 8
            cell.descConstraint.constant = 8
            break
        case 5:
            let keyStr = searchBar.text!.trim().folding(options: .diacriticInsensitive, locale: .current)
            let parentStr = self.newsCategories[indexPath.row].news.title.folding(options: .diacriticInsensitive, locale: .current)
            let range = (parentStr.lowercased() as NSString).range(of: keyStr.lowercased(), options: .caseInsensitive)

            let attribute = NSMutableAttributedString.init(string: self.newsCategories[indexPath.row].news.title)
            attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.red , range: range)
            cell.lblTitle?.attributedText = attribute
            cell.lblDesc?.text = self.newsCategories[indexPath.row].news.description
            let newsObj = self.newsCategories[indexPath.row].news
            let urlString = newsObj.img.trimmingCharacters(in: .whitespaces)
            cell.imagThumb.loadImageUsingUrlString(urlString: urlString)
            cell.imagThumb.layer.borderColor = UIColor.clear.cgColor
            cell.imagThumb?.isHidden = false
            cell.imagThumb?.layer.cornerRadius = 0
            cell.imagThumb?.layer.masksToBounds = true
            cell.titleConstraint.constant = 52
            cell.descConstraint.constant = 52
            break
        case 6:
            let keyStr = searchBar.text!.trim().folding(options: .diacriticInsensitive, locale: .current)
            let parentStr = self.meanning[indexPath.row].testName.folding(options: .diacriticInsensitive, locale: .current)
            let range = (parentStr.lowercased() as NSString).range(of: keyStr.lowercased(), options: .caseInsensitive)

            let attribute = NSMutableAttributedString.init(string: self.meanning[indexPath.row].testName)
            attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.red , range: range)
            cell.lblTitle?.attributedText = attribute
            if let meanning = try! self.meanning[indexPath.row].meaning1.convertHtmlSymbols(){
                if !meanning.isEmpty{
                    cell.lblDesc.text = meanning
                }else{
                    cell.lblDesc.text = "Đang cập nhật dữ liệu"
                }
            }
            cell.imagThumb.isHidden = true
            cell.titleConstraint.constant = 8
            cell.descConstraint.constant = 8
            break
        default:
            break
        }
        return cell
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerSection = UIView(frame: CGRect(x: 5, y: 0, width: UIScreen.main.bounds.width - 10, height: 30))
        headerSection.backgroundColor = UIColor(red: 244/255, green: 247/255, blue: 221/255, alpha: 1)
        let titleSection = UILabel(frame: CGRect(x: 40, y: 5, width: UIScreen.main.bounds.width - 10, height: 20))
        titleSection.font = UIFont.systemFont(ofSize: 18)
        titleSection.text = arrScopeTitle[section+1].uppercased()
        let iconSection = UIImageView()
        iconSection.frame = CGRect(x: 5, y: 5, width: 20, height: 20)
        switch section {
        case 0:
            titleSection.text = titleSection.text! + " (\(self.dictionaries.count))"
            iconSection.image = FAKFontAwesome.medkitIcon(withSize: 20).image(with: CGSize(width: 20, height: 20))
        case 1:
            titleSection.text = titleSection.text! + " (\(self.symptons.count))"
            iconSection.image = FAKFontAwesome.heartbeatIcon(withSize: 20).image(with: CGSize(width: 20, height: 20))
        case 2:
            titleSection.text = titleSection.text! + " (\(self.usersDoctors.count))"
            iconSection.image = FAKFontAwesome.userMdIcon(withSize: 20).image(with: CGSize(width: 20, height: 20))
        case 3:
            titleSection.text = titleSection.text! + " (\(self.locations.count))"
            iconSection.image = FAKFontAwesome.hospitalOIcon(withSize: 20).image(with: CGSize(width: 20, height: 20))
        case 4:
            titleSection.text = titleSection.text! + " (\(self.questions.count))"
            iconSection.image = FAKFontAwesome.commentOIcon(withSize: 20).image(with: CGSize(width: 20, height: 20))
        case 5:
            titleSection.text = titleSection.text! + " (\(self.newsCategories.count))"
            iconSection.image = FAKFontAwesome.newspaperOIcon(withSize: 20).image(with: CGSize(width: 20, height: 20))
        case 6:
            titleSection.text = titleSection.text! + " (\(self.meanning.count))"
            iconSection.image = FAKFontAwesome.ambulanceIcon(withSize: 20).image(with: CGSize(width: 20, height: 20))
        default:
            break
        }
        
        headerSection.addSubview(iconSection)
        headerSection.addSubview(titleSection)
        return headerSection
    }
    
    // MARK: - UISearchBarDelegate
    
    override func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        self.showSuggestSearchView(keySearch: searchBar.text!.trim())
        txtSearchInput = nil
        searchBar.showsCancelButton = true
        if UIDevice().model == "iPad" {
            self.configureCancelBarButton()
        }
        return true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let keyword = searchBar.text?.trim()
        if(keyword == "") {
            pageNumber = 1
        } else {
            searchView?.showView(keySearch: keyword!)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.searchView?.hiddenView()
        if UIDevice().model == "iPad" {
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard  let keyword = searchBar.text?.trim() else { return }
        self.saveHistorySearch(key: keyword)
        searchBar.resignFirstResponder()
        self.searchView?.hiddenView()
        let param: [String: Any] = ["query":searchBar.text!.trim(),
                                    "findState":"\(filterStr)",
            "pageNumber":1]
        self.getSearch(param: param)
    }
    
    func filterSearch() {
        pageNumber = 1
//        self.myTableView.es_resetNoMoreData()
        let pointScroll = CGPoint(x: 0, y: 0)
        self.myTableView.setContentOffset(pointScroll, animated: true)
        filterStr = arrScopeTitle[scopeSelected.selectedSegmentIndex]
        if (searchBar.text?.trim().isEmpty)! {
            let paramSearch: [String : Any] = ["findState":filterStr,
                                               "pageNumber":1]
            self.getSearchSuggest(param: paramSearch)
        } else {
            let param: [String: Any] = ["query":searchBar.text!.trim(),
                                        "findState":"\(filterStr)",
                "pageNumber":1]
            self.getSearch(param: param)
        }
    }
    
    override func showProgress(color:UIColor? = nil) -> MKActivityIndicator? {
        if let viewToAdd = self.navigationController?.view {
            let loadingProgress = MKActivityIndicator(frame: CGRect(x:0,y:0,width: indicatorSize,height:indicatorSize))
            if let color = color {
                loadingProgress.color = color
            }
            loadingProgress.center = self.view.center
            loadingProgress.lineWidth = loadingProgressLineWidth
            loadingProgress.autoresizingMask = [.flexibleTopMargin, .flexibleLeftMargin, .flexibleBottomMargin, .flexibleRightMargin]
            viewToAdd.addSubview(loadingProgress)
            loadingProgress.startAnimating()
            return loadingProgress
        }
        return nil
    }
    
    override func hideProgress(_ loadingProgress:MKActivityIndicator?) {
        loadingProgress?.stopAnimating()
        loadingProgress?.removeFromSuperview()
    }
}
