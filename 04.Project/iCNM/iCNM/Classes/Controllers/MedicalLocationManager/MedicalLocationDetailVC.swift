//
//  MedicalLocationDetailVC.swift
//  iCNM
//
//  Created by Hoang Van Trung on 11/22/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Alamofire
import GooglePlacePicker
import FontAwesomeKit
import RealmSwift
import Toaster
import IQKeyboardManagerSwift

class MedicalLocationDetailVC: UIViewController, SpecialistPopupViewControllerDelegate, UIGestureRecognizerDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UIPopoverControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate, CropViewControllerDelegate {
    @IBOutlet weak var viewType: UIView!
    @IBOutlet weak var viewContactInfo: UIView!
    @IBOutlet weak var viewLocationInfo: UIView!
    @IBOutlet weak var viewInfoDetail: UIView!
    @IBOutlet weak var viewUpdateimage: UIView!
    @IBOutlet weak var nameLocation: FWFloatingLabelTextView!
    @IBOutlet weak var errorMsgName: UILabel!
    @IBOutlet weak var phoneNumber: FWFloatingLabelTextField!
    @IBOutlet weak var address: FWFloatingLabelTextView!
    @IBOutlet weak var errorMsgAddress: UILabel!
    @IBOutlet weak var descriptionCommon: FWFloatingLabelTextView!
    @IBOutlet weak var department: FWFloatingLabelTextView!
    @IBOutlet weak var imgLocation: UIImageView!
    @IBOutlet weak var updateImage: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnDoctor: UIButton!
    @IBOutlet weak var btnHospital: UIButton!
    @IBOutlet weak var btnClinic: UIButton!
    @IBOutlet weak var btnPharmacy: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    
    var mapViewController: BackgroundMapViewController?
    var locationOwn: LocationOwn?
    private var specialListSelect = [SpecialistModel]()
    private var specialistSelected=[SpecialistModel]()
    var arrImgThumbnail = [String]()
    var arrImgBase64 = [String]()
    var locationID = 0
    var isUpdateAvatar = true
    var locationTypeID = 0
    var isAddNewLocation = false
    var doctorID = 0
    var arrChuyenKhoa = [String]()
    var updateAvatarBlock: (() -> Void)?
    var avatarAddNewComplete = false
    var imgUpload = [[String: Any]]()
    var locationUpdate = CLLocationCoordinate2D()
    
    var imageView = UIImageView()
    private var image: UIImage?
    private var croppingStyle = CropViewCroppingStyle.default
    private var croppedRect = CGRect.zero
    private var croppedAngle = 0
    let picker = UIImagePickerController()
    var isAddNewCompleted = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = isAddNewLocation ? "Thêm mới đơn vị y tế" : "Cập nhật đơn vị y tế"
        self.avatarAddNewComplete = isAddNewLocation ? false : true
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: FAKFontAwesome.saveIcon(withSize: 20).image(with: CGSize(width: 20, height: 20)).maskWithColor(color: .white), style: .plain, target: self, action: #selector(MedicalLocationDetailVC.updateLocation(_:)))
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(MedicalLocationDetailVC.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        picker.delegate = self
        let nib = UINib(nibName: "ImgThumbnailCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "ImgThumbnailCell")
        self.btnSave.layer.cornerRadius = 3
        self.imgLocation.contentMode = .scaleAspectFill
        viewType.dropShadow(color: .gray, opacity: 1, offSet: .zero, radius: 3)
        viewContactInfo.dropShadow(color: .gray, opacity: 1, offSet: .zero, radius: 3)
        viewLocationInfo.dropShadow(color: .gray, opacity: 1, offSet: .zero, radius: 3)
        viewInfoDetail.dropShadow(color: .gray, opacity: 1, offSet: .zero, radius: 3)
        viewUpdateimage.dropShadow(color: .gray, opacity: 1, offSet: .zero, radius: 3)
        let realm = try! Realm()
        let currentUser = realm.currentUser()
        doctorID = (currentUser?.doctorID)!
        if let locationOwn = self.locationOwn {
            self.fillInforLocation(locationOwn: locationOwn)
            self.arrChuyenKhoa = locationOwn.CName.components(separatedBy: "; ")
        } else {
            self.fillInforLocation(locationOwn: LocationOwn())
        }
        
        self.getSpecialList()
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MedicalLocationDetailVC.selectImgAvatar))
        gestureRecognizer.delegate = self
        updateImage.addGestureRecognizer(gestureRecognizer)
        
        let departmentGesture = UITapGestureRecognizer(target: self, action: #selector(MedicalLocationDetailVC.showListSpecialist))
        departmentGesture.delegate = self
        department.addGestureRecognizer(departmentGesture)
        
        let locationGesture = UITapGestureRecognizer(target: self, action: #selector(MedicalLocationDetailVC.showSelectLocation))
        locationGesture.delegate = self
        address.addGestureRecognizer(locationGesture)
        
        phoneNumber.addRegx(strRegx: "^[0-9]{10,11}$", errorMsg: "Số điện thoại không đúng định dạng")
        phoneNumber.messageForValidatingLength = "Số điện thoại không được bỏ trống"
        phoneNumber.delegate = self
        nameLocation.delegate = self
        address.delegate = self
    }

    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutImageView()
    }
    
    func fillInforLocation(locationOwn: LocationOwn) {
        locationID = locationOwn.LocationID
        nameLocation.text = locationOwn.Name
        phoneNumber.text = locationOwn.Mobile
        address.text = locationOwn.Address
        descriptionCommon.text = locationOwn.Intro
        department.text = locationOwn.CName
        locationTypeID = locationOwn.LocationTypeID
        locationUpdate.latitude = locationOwn.Lat
        locationUpdate.longitude = locationOwn.Long
        if !locationOwn.ImagesThumbnailStr.isEmpty {
            arrImgThumbnail = locationOwn.ImagesThumbnailStr.components(separatedBy: ";")
        }
        let url = URL(string: API.baseURLImage + "iCNMImage/" + "LocationImage/"
            + "\(locationOwn.LocationID)/" + "\(locationOwn.ImagesStr)")
        imgLocation.sd_setImage(with: url, placeholderImage: FAKFontAwesome.pictureOIcon(withSize: 375).image(with: CGSize(width: 512, height: 512)).maskWithColor(color: UIColor(hexColor: 0x0288D1, alpha: 1.0)))
        
        btnDoctor.layer.cornerRadius = 3
        btnHospital.layer.cornerRadius = 3
        btnClinic.layer.cornerRadius = 3
        btnPharmacy.layer.cornerRadius = 3
        switch locationOwn.LocationTypeID {
        case 1:
            self.changeButtonSelect(button: self.btnDoctor)
            self.btnDoctor.setImage(FAKFontAwesome.userMdIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: .white), for: .normal)
            self.btnHospital.setImage(FAKFontAwesome.hospitalOIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
            self.btnClinic.setImage(FAKFontAwesome.homeIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
            self.btnPharmacy.setImage(FAKFontAwesome.medkitIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
            break
        case 2:
            self.changeButtonSelect(button: self.btnHospital)
            self.btnHospital.setImage(FAKFontAwesome.hospitalOIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: .white), for: .normal)
            self.btnDoctor.setImage(FAKFontAwesome.userMdIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
            self.btnClinic.setImage(FAKFontAwesome.homeIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
            self.btnPharmacy.setImage(FAKFontAwesome.medkitIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
            break
        case 4:
            self.changeButtonSelect(button: self.btnClinic)
            self.btnClinic.setImage(FAKFontAwesome.homeIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: .white), for: .normal)
            self.btnDoctor.setImage(FAKFontAwesome.userMdIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
            self.btnHospital.setImage(FAKFontAwesome.hospitalOIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
            self.btnPharmacy.setImage(FAKFontAwesome.medkitIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
            break
        case 5:
            self.changeButtonSelect(button: self.btnPharmacy)
            self.btnPharmacy.setImage(FAKFontAwesome.medkitIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: .white), for: .normal)
            self.btnDoctor.setImage(FAKFontAwesome.userMdIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
            self.btnHospital.setImage(FAKFontAwesome.hospitalOIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
            self.btnClinic.setImage(FAKFontAwesome.homeIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
            break
        default:
            break
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let block = self.updateAvatarBlock {
            block()
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == nameLocation {
            IQKeyboardManager.sharedManager().enable = false
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == nameLocation {
            IQKeyboardManager.sharedManager().enable = true
            if textView.text.isEmpty {
                errorMsgName.text = "Tên địa điểm không được bỏ trống"
            } else {
                if textView.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                    errorMsgName.text = "Tên địa điểm không được bỏ trống"
                    nameLocation.text = ""
                } else {
                    errorMsgName.text = ""
                }
            }
        }
    }
    
    @IBAction func selectChuyenKhoa(_ sender: Any) {
        self.showListSpecialist()
    }
    
    @IBAction func selectDoctor(_ sender: Any) {
        locationTypeID = 1
        changeButtonSelect(button: self.btnDoctor)
        unselectButton(button: self.btnPharmacy)
        unselectButton(button: self.btnClinic)
        unselectButton(button: self.btnHospital)
        self.btnDoctor.setImage(FAKFontAwesome.userMdIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: .white), for: .normal)
        self.btnHospital.setImage(FAKFontAwesome.hospitalOIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
        self.btnClinic.setImage(FAKFontAwesome.homeIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
        self.btnPharmacy.setImage(FAKFontAwesome.medkitIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
    }
    
    @IBAction func selectHospital(_ sender: Any) {
        locationTypeID = 2
        changeButtonSelect(button: self.btnHospital)
        unselectButton(button: self.btnPharmacy)
        unselectButton(button: self.btnClinic)
        unselectButton(button: self.btnDoctor)
        self.btnDoctor.setImage(FAKFontAwesome.userMdIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
        self.btnHospital.setImage(FAKFontAwesome.hospitalOIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: .white), for: .normal)
        self.btnClinic.setImage(FAKFontAwesome.homeIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
        self.btnPharmacy.setImage(FAKFontAwesome.medkitIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
    }
    
    @IBAction func selectClinic(_ sender: Any) {
        locationTypeID = 4
        changeButtonSelect(button: self.btnClinic)
        unselectButton(button: self.btnPharmacy)
        unselectButton(button: self.btnDoctor)
        unselectButton(button: self.btnHospital)
        self.btnDoctor.setImage(FAKFontAwesome.userMdIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
        self.btnHospital.setImage(FAKFontAwesome.hospitalOIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
        self.btnClinic.setImage(FAKFontAwesome.homeIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: .white), for: .normal)
        self.btnPharmacy.setImage(FAKFontAwesome.medkitIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
    }
    
    @IBAction func selectPharmacy(_ sender: Any) {
        locationTypeID = 5
        changeButtonSelect(button: self.btnPharmacy)
        unselectButton(button: self.btnDoctor)
        unselectButton(button: self.btnClinic)
        unselectButton(button: self.btnHospital)
        self.btnDoctor.setImage(FAKFontAwesome.userMdIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
        self.btnHospital.setImage(FAKFontAwesome.hospitalOIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
        self.btnClinic.setImage(FAKFontAwesome.homeIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: UIColor(hexColor: 0x03A9F4, alpha: 1.0)), for: .normal)
        self.btnPharmacy.setImage(FAKFontAwesome.medkitIcon(withSize: 20).image(with: CGSize(width: 15, height: 15)).maskWithColor(color: .white), for: .normal)
    }
    
    func changeButtonSelect(button: UIButton) {
        button.layer.cornerRadius = 16
        button.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        button.setTitleColor(UIColor(hexColor: 0xFFFFFF, alpha: 1.0), for: .normal)
    }
    
    func unselectButton(button: UIButton) {
        button.layer.cornerRadius = 3
        button.backgroundColor = UIColor(hexColor: 0xEFEFF4, alpha: 1.0)
        button.setTitleColor(UIColor(hexColor: 0x0096FF, alpha: 1.0), for:.normal)
    }
    func selectImgAvatar() {
        isUpdateAvatar = true
        self.selectLibrary()
    }
    
    @IBAction func updateAvatar(_ sender: Any) {
        var avatarActionSheet:UIAlertController? = nil
        if IS_IPAD{
            avatarActionSheet = UIAlertController(title: nil, message: "Chọn ảnh đại diện", preferredStyle: .alert)
        }else{
            avatarActionSheet = UIAlertController(title: nil, message: "Chọn ảnh đại diện", preferredStyle: .actionSheet)
        }
        
        let cameraAction = UIAlertAction(title: "Chụp ảnh mới", style: .default) { (action) in
            self.picker.sourceType = .camera
            self.picker.cameraCaptureMode = .photo
            self.picker.modalPresentationStyle = .fullScreen
            self.present(self.picker, animated: true, completion: nil)
        }
        let galleryAction = UIAlertAction(title: "Chọn ảnh từ thư viện", style: .default) { (action) in
            self.picker.sourceType = .photoLibrary
            self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.present(self.picker, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Đóng", style: .cancel, handler: nil)
        avatarActionSheet?.addAction(cameraAction)
        avatarActionSheet?.addAction(galleryAction)
        avatarActionSheet?.addAction(cancelAction)
        
        present(avatarActionSheet!, animated: true, completion: nil)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        imageView.image = image
        layoutImageView()
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        if cropViewController.croppingStyle != .circular {
            imageView.isHidden = true
            
            cropViewController.dismissAnimatedFrom(self, withCroppedImage: image,
                                                   toView: imageView,
                                                   toFrame: CGRect.zero,
                                                   setup: { self.layoutImageView() },
                                                   completion: { self.imageView.isHidden = false })
            
            if let avatar = UIImage.resizeImage(image: imageView.image!, toWidth: 512), let imageData = UIImagePNGRepresentation(avatar) {
                if isAddNewLocation {
                    if isUpdateAvatar {
                        self.avatarAddNewComplete = true
                        self.imgLocation.image = UIImage(data:imageData,scale:1.0)
                    } else {
                        self.imgUpload.append(["Name":self.createImgName(locationID: self.locationID), "LocationID": self.locationID, "Base64String": imageData.base64EncodedString()])
                        self.arrImgBase64.append(imageData.base64EncodedString())
                        self.collectionView.reloadData()
                    }
                } else {
                    if isUpdateAvatar {
                        self.updateAvatar(image: imageData)
                    } else {
                        let locationID: Int = self.locationOwn != nil ? (self.locationOwn?.LocationID)! : 0
                        let param: [String : Any] = ["Name": self.createImgName(locationID: locationID), "LocationID": locationID, "Status": 1, "Base64String": imageData.base64EncodedString(options: .lineLength64Characters)]
                        self.deleteEditImageListLocation(param: param)
                    }
                }
            }
        }
        else {
            self.imageView.isHidden = false
            cropViewController.dismiss(animated: true, completion: nil)
        }
    }
    
    func selectLibrary() {
        
        var avatarActionSheet:UIAlertController? = nil
        if IS_IPAD{
            avatarActionSheet = UIAlertController(title: nil, message: "Ảnh đại diện", preferredStyle: .alert)
        }else{
            avatarActionSheet = UIAlertController(title: nil, message: "Ảnh đại diện", preferredStyle: .actionSheet)
        }

        let firstAction: UIAlertAction = UIAlertAction(title: "Chọn ảnh từ thư viện", style: .default) { action -> Void in
            self.openGallary()
        }
        
        let secondAction: UIAlertAction = UIAlertAction(title: "Chụp ảnh mới", style: .default) { action -> Void in
            self.openCamera()
        }
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Hủy", style: .cancel) { action -> Void in }
        
        // add actions
        avatarActionSheet?.addAction(firstAction)
        avatarActionSheet?.addAction(secondAction)
        avatarActionSheet?.addAction(cancelAction)
        
        // present an actionSheet...
        present(avatarActionSheet!, animated: true, completion: nil)
    }
    
    func openGallary()
    {
        self.picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(self.picker, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            self.picker.sourceType = UIImagePickerControllerSourceType.camera
            self.picker.cameraCaptureMode = .photo
            present(self.picker, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = (info[UIImagePickerControllerOriginalImage] as? UIImage) else { return }
        let cropController = CropViewController(croppingStyle: croppingStyle, image: image)
        cropController.delegate = self
        self.image = image
        
        //If profile picture, push onto the same navigation stack
        if croppingStyle == .circular {
            picker.pushViewController(cropController, animated: true)
        }
        else { //otherwise dismiss, and then present from the main controller
            picker.dismiss(animated: true, completion: {
                self.present(cropController, animated: true, completion: nil)
                //self.navigationController!.pushViewController(cropController, animated: true)
            })
        }
    }
    
    public func layoutImageView() {
        guard imageView.image != nil else { return }
        let padding: CGFloat = 20.0
        
        var viewFrame = self.view.bounds
        viewFrame.size.width -= (padding * 2.0)
        viewFrame.size.height -= ((padding * 2.0))
        
        var imageFrame = CGRect.zero
        imageFrame.size = imageView.image!.size;
        
        if imageView.image!.size.width > viewFrame.size.width || imageView.image!.size.height > viewFrame.size.height {
            let scale = min(viewFrame.size.width / imageFrame.size.width, viewFrame.size.height / imageFrame.size.height)
            imageFrame.size.width *= scale
            imageFrame.size.height *= scale
            imageFrame.origin.x = (self.view.bounds.size.width - imageFrame.size.width) * 0.5
            imageFrame.origin.y = (self.view.bounds.size.height - imageFrame.size.height) * 0.5
            imageView.frame = imageFrame
        }
        else {
            self.imageView.frame = imageFrame;
            self.imageView.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
        }
    }
    
    func getSpecialList() {
        Specialist.getAllSpecialist(success: { (results) in
            if !results.isEmpty {
                for result in results {
                    self.specialListSelect.append(SpecialistModel(title: result.name!, isSelected: self.arrChuyenKhoa.contains(result.name!), isUserSelectEnable: true, specialist: result))
                }
                self.specialistSelected = self.specialListSelect.filter({ (specialistModel) -> Bool in
                    return specialistModel.isSelected
                })
            } else {
                
            }
        }, fail: { (error, response) in
            
        })
    }
    func showListSpecialist() {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let specialistPopup = storyboard.instantiateViewController(withIdentifier: "SpecialistPopup") as! SpecialistPopupViewController
        specialistPopup.dataArray = self.specialListSelect
        specialistPopup.delegate = self
        self.present(specialistPopup, animated: true, completion: nil)
    }
    
    //Delegate from popup select specialist
    func specialistDidUpdate(specialistPopup: SpecialistPopupViewController) {
        specialistSelected = self.specialListSelect.filter({ (specialistModel) -> Bool in
            return specialistModel.isSelected
        })
        self.department.text = ""
        self.arrChuyenKhoa.removeAll()
        for itemSeelcted in specialistSelected {
            self.arrChuyenKhoa.append(itemSeelcted.title)
            self.department.text = self.department.text! + "\(itemSeelcted.title); "
        }
        department.layoutSubviews()
    }
    
    @IBAction func selectLocation(_ sender: Any) {
        self.showSelectLocation()
    }
    
    func showSelectLocation() {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        placePicker.modalPresentationStyle = .popover
        self.present(placePicker, animated: true, completion: nil)
    }
    
    @IBAction func updateLocation(_ sender: Any) {
        if isCheckField() {
            var param = [String: Any]()
            let locationID = locationOwn != nil ? locationOwn?.LocationID : 0
            let latitude = locationUpdate.latitude
            let longtitude = locationUpdate.longitude
            let doctorID = locationOwn != nil ? locationOwn?.DoctorID: self.doctorID
            param = ["LocationID": locationID!,"Name": nameLocation.text!, "Mobile": phoneNumber.text!, "Address": address.text!, "Intro": descriptionCommon.text!, "CName": department.text!, "LocationTypeID": locationTypeID, "Lat": latitude, "Long": longtitude, "DoctorID": doctorID!]
            if isAddNewLocation {
                if !isAddNewCompleted {
                    self.addNewLocation(param: param)
                }
            } else {
                self.updateLocation(param: param) { (isSuccess, str) in
                    print(str)
                }
            }
        } else {
            if !avatarAddNewComplete {
                let alertViewController = UIAlertController(title: "Thiếu thông tin", message: "Chưa chọn ảnh đại diện địa điểm", preferredStyle: .alert)
                let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                alertViewController.addAction(noAction)
                self.present(alertViewController, animated: true, completion: nil)
            }
        }
    }
    
    func addNewLocation(param: [String: Any]) {
        self.isAddNewCompleted = !self.isAddNewCompleted
        LocationOwn().addUpdateLocation(parameterDic: param, success: { (responseString) in
            if responseString.contains("oke") {
                self.isAddNewCompleted = true
                let message = self.isAddNewLocation ? "Thêm mới thành công!" : "Cập nhật thành công!"
                Toast(text: message).show()
                self.locationID = Int(responseString.westernArabicNumeralsOnly)!
                self.addNewAvatar(image: UIImagePNGRepresentation(self.imgLocation.image!)!)
            } else {
                self.isAddNewCompleted = false
                Toast(text: "Có Lỗi xảy ra!").show()
            }
        }, fail: { (error, response) in
            
        })
    }
    
    @IBAction func addListImage(_ sender: Any) {
        isUpdateAvatar = false
        if isAddNewLocation {
            if self.arrImgBase64.count < 3 {
                self.selectLibrary()
            } else {
                Toast(text: "Bạn chỉ được chọn tối đa ba bức ảnh!").show()
            }
        } else {
            if self.arrImgThumbnail.count < 3 {
                self.selectLibrary()
            } else {
                Toast(text: "Bạn chỉ được chọn tối đa ba bức ảnh!").show()
            }
        }
    }
    
    @IBAction func getAccountInfor(_ sender: Any) {
        let realm = try! Realm()
        if let currentUser = realm.currentUser()?.userDoctor?.userInfo {
            nameLocation.text = currentUser.name!
            nameLocation.layoutSubviews()
            phoneNumber.text = currentUser.phone
        }
        
    }
    
    func updateLocation(param:[String: Any], complete: @escaping(Bool, String) -> Swift.Void) {
        let loadingProgress = self.showProgress()
        LocationOwn().addUpdateLocation(parameterDic: param, success: { (responseString) in
            if responseString.contains("oke") {
                let message = self.isAddNewLocation ? "Thêm mới thành công!" : "Cập nhật thành công!"
                Toast(text: message).show()
                self.navigationController?.popViewController(animated: true)
            } else {
                Toast(text: "Có Lỗi xảy ra!").show()
            }
            self.hideProgress(loadingProgress)
        }, fail: { (error, response) in
            self.hideProgress(loadingProgress)
            
        })
    }
    
    func updateAvatar(image: Data) {
        let locationID: Int = locationOwn != nil ? (locationOwn?.LocationID)! : self.locationID
        let name: String = self.createImgName(locationID: locationID)
        let param: [String : Any] = ["Name": name, "LocationID": locationID, "Base64String": image.base64EncodedString(options: .lineLength64Characters)]
        let loadingProgress = self.showProgress()
        LocationOwn().updateAvatarLocation(parameterDic: param, success: { (responseString) in
            if responseString.contains("1") {
                self.imgLocation.image = UIImage(data:image,scale:1.0)
                Toast(text: "Update avatar thành công").show()
            } else {
                Toast(text: "Update avatar thất bại").show()
            }
            self.hideProgress(loadingProgress)
        }, fail: { (error, response) in
            
            self.hideProgress(loadingProgress)
        })
    }
    
    func addNewAvatar(image: Data) {
        let locationID: Int = locationOwn != nil ? (locationOwn?.LocationID)! : self.locationID
        let name: String = self.createImgName(locationID: locationID)
        let param: [String : Any] = ["Name": name, "LocationID": locationID, "Base64String": image.base64EncodedString(options: .lineLength64Characters)]
        let loadingProgress = self.showProgress()
        LocationOwn().updateAvatarLocation(parameterDic: param, success: { (responseString) in
            self.addListImageLocation()
            self.hideProgress(loadingProgress)
        }, fail: { (error, response) in
            self.hideProgress(loadingProgress)
        })
        
    }
    func createImgName(locationID: Int) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd_hhmmss"
        let name: String = "\(locationID)"  + "_locationImg_" +  formatter.string(from: Date())
        return name
    }
    func addListImageLocation() {
        let param: [String : Any] = ["Images": self.imgUpload, "LocationID": self.locationID]
        let loadingProgress = self.showProgress()
        LocationOwn().addUpdateListImageLocation(parameterDic: param, success: { (responseString) in
            if responseString.contains("oke") {
                self.navigationController?.popViewController(animated: true)
            } else {
                Toast(text: "Add list image bị lỗi!").show()
            }
            self.hideProgress(loadingProgress)
        }, fail: { (error, response) in
            self.hideProgress(loadingProgress)
        })
    }
    
    func deleteEditImageListLocation(param: [String: Any]) {
        let loadingProgress = self.showProgress()
        LocationOwn().addUpdateImageLocation(parameterDic: param, success: { (responseString) in
            if responseString.contains("1") {
                Toast(text: "Thực hiện thành công!").show()
                self.getLocationInfo()
            } else {
                Toast(text: "Có lỗi xảy ra").show()
            }
            self.hideProgress(loadingProgress)
        }, fail: { (error, response) in
            self.hideProgress(loadingProgress)
        })
    }
    
    func getLocationInfo() {
        let param = ["locationID": locationID]
        LocationOwn().getLocationInfo(param: param, success: { (result) in
            if let locationOwn = result {
                self.arrImgThumbnail = locationOwn.ImagesThumbnailStr.components(separatedBy: ";")
                if self.arrImgThumbnail.count == 1, (self.arrImgThumbnail.first?.isEmpty)! {
                    self.arrImgThumbnail.removeAll()
                }
                self.collectionView.reloadData()
            } else {
                
            }
        }) { (error, response) in
            print(response)
        }
    }
    
    func isCheckField() -> Bool {
        if nameLocation.text.isEmpty {
            errorMsgName.text = "Tên địa điểm không được bỏ trống"
            return false
        }
        
        if address.text.isEmpty {
            self.errorMsgAddress.text = "Bạn phải nhập địa chỉ"
            return false
        }
        
        if !phoneNumber.validate() {
            return false
        }
        
        if !avatarAddNewComplete {
            return false
        }
        
        return true
    }
}

extension MedicalLocationDetailVC : GMSPlacePickerViewControllerDelegate {
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
//        let nextScreen = PlaceDetailViewController(place: place)
        self.errorMsgAddress.text = ""
        self.address.text = place.formattedAddress
        self.address.layoutSubviews()
        self.locationUpdate = place.coordinate
//        self.splitPaneViewController?.push(viewController: nextScreen, animated: false)
        self.mapViewController?.coordinate = place.coordinate
        
        // Dismiss the place picker.
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didFailWithError error: Error) {
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
        if self.address.text.isEmpty {
            self.errorMsgAddress.text = "Bạn phải nhập địa chỉ"
        }
    }
    
    func showProgress(color:UIColor? = nil) -> MKActivityIndicator? {
        if let viewToAdd = self.navigationController?.view {
            let loadingProgress = MKActivityIndicator(frame: CGRect(x:0,y:0,width: 28,height:28))
            if let color = color {
                loadingProgress.color = color
            }
            loadingProgress.center = self.view.center
            loadingProgress.lineWidth = 3
            loadingProgress.autoresizingMask = [.flexibleTopMargin, .flexibleLeftMargin, .flexibleBottomMargin, .flexibleRightMargin]
            viewToAdd.addSubview(loadingProgress)
            loadingProgress.startAnimating()
            return loadingProgress
        }
        return nil
    }
    
    func hideProgress(_ loadingProgress:MKActivityIndicator?) {
        loadingProgress?.stopAnimating()
        loadingProgress?.removeFromSuperview()
    }
}

extension MedicalLocationDetailVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isAddNewLocation {
            return self.arrImgBase64.count
        } else {
            return self.arrImgThumbnail.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImgThumbnailCell", for: indexPath) as! ImgThumbnailCell
        if isAddNewLocation {
            if let imageData = Data(base64Encoded: self.arrImgBase64[indexPath.row]) {
                cell.imgThumbnail.image = UIImage(data: imageData)
            }
        } else {
            cell.imgName = self.arrImgThumbnail[indexPath.row]
            let url = URL(string: API.baseURLImage + "iCNMImage/" + "LocationImage/"
                + "\(locationID)/" + self.arrImgThumbnail[indexPath.row])
            cell.imgThumbnail.sd_setImage(with: url)
            cell.deleteImageBlock = { (imgName) -> Void in
                let locationID: Int = self.locationOwn != nil ? (self.locationOwn?.LocationID)! : 0
                let param: [String : Any] = ["Name": imgName.components(separatedBy: ".jpg").first!, "LocationID": locationID, "Status": 0]
                self.deleteEditImageListLocation(param: param)
            }
        }
        return cell
    }
}
