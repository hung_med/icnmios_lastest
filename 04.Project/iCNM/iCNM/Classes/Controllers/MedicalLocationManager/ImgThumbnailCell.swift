//
//  ImgThumbnailCell.swift
//  iCNM
//
//  Created by Hoang Van Trung on 12/6/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class ImgThumbnailCell: UICollectionViewCell {

    var deleteImageBlock: ((_ imgName: String) -> Void)?
    @IBOutlet weak var imgThumbnail: UIImageView!
    var imgName = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func deleteImg(_ sender: Any) {
        if let block = deleteImageBlock {
            block(self.imgName)
        }
    }
    
}
