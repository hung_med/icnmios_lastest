//
//  DetailLookUpCustomHeader.swift
//  iCNM
//
//  Created by Mac osx on 7/18/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
protocol CustomScheduleDoctorHeaderDelegate {
    func toggleSection(_ header: CustomScheduleDoctorHeader, section: Int)
}

class CustomScheduleDoctorHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var inTime: UILabel!
    @IBOutlet weak var bgInTime: UIView!
     @IBOutlet weak var lblArrow: UILabel!
    @IBOutlet weak var spaceLeftConstraint: NSLayoutConstraint!
    var delegate: CustomScheduleDoctorHeaderDelegate?
    var section: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.bgInTime.layer.cornerRadius = 4
        self.bgInTime.layer.masksToBounds = true
       // self.lineView.isHidden=true
        
        self.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CustomSectionHeader.tapHeader(_:))))
    }
    
    func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? CustomScheduleDoctorHeader else {
            return
        }
        
        delegate?.toggleSection(self, section: cell.section)
    }
    
    func setCollapsed(_ collapsed: Bool) {
        self.lblArrow.rotate(collapsed ? 0.0 : .pi/2)
    }
}
