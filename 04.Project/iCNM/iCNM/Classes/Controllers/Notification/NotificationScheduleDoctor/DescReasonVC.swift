//
//  DescLookUpResultVC
//  iCNM
//
//  Created by Mac osx on 11/02/17.
//  Copyright © 2017 Anh Nguyen. All rights reserved.
//

import UIKit

class DescReasonVC: UIViewController {
  
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContent: UITextView!
    @IBOutlet weak var myBtnExit: UIButton!
    @IBOutlet weak var myView: UIView!
    
    var strTitle:String? = nil
    var strReason:String? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        myBtnExit.layer.cornerRadius = 4
        myBtnExit.layer.masksToBounds = true
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        lblContent.text = strReason
        self.view.layer.cornerRadius = 4.0
        self.view.layer.borderWidth = 1.0
        lblTitle.text = strTitle
        lblTitle.lineBreakMode = .byWordWrapping
        lblTitle.numberOfLines = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnExit(_ sender: Any) {
        self .dismiss(animated: true) {
            
        }
    }
    
}
