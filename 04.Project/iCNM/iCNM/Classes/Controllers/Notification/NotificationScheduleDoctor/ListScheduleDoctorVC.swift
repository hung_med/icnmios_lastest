//
//  ListScheduleDoctorVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Foundation
import FontAwesome_swift
import ESPullToRefresh

struct ListScheduleDoctorVCGroup {
    var intime: String
    var items: [ScheduleDoctorOfDay]
    var collapsed: Bool
    
    public init(intime: String, items: [ScheduleDoctorOfDay], collapsed: Bool = true) {
        self.intime = intime
        self.items = items
        self.collapsed = collapsed
    }
}

class ListScheduleDoctorVC: BaseViewControllerNoSearchBar, CustomScheduleDoctorHeaderDelegate, UISearchBarDelegate, UIPopoverPresentationControllerDelegate, UITableViewDelegate, UITableViewDataSource, DetailHealthRecordsVCDelegate {
    
    func reloadListHSSK(userID: Int) {
        print(userID)
    }
    
    var scheduleDoctorOfDay: [ScheduleDoctorOfDay]?
    var scheduleDoctorOfDayGroup: [String: [ScheduleDoctorOfDay]] = [:]
    var scheduleDoctorOfDayFilter: [String: [ScheduleDoctorOfDay]] = [:]
    var listInTimes:[String]?
    var listInTimes_Filter:[String]?
    var filteredNames:[ScheduleDoctorOfDay]?
    var sectionsData: [ListScheduleDoctorVCGroup]?
    var sectionsData2: [ListScheduleDoctorVCGroup]?
    var lblWarning:UILabel?
    var newBackButton:UIBarButtonItem? = nil
    var isSearch:Bool? = false
    var doctorID:Int? = 0
    var openNumber = 1
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var tableViewPopUp: UITableView!
    var arrayPopUp = ["Tất cả", "Đã từng khám", "Khách mới khám"]
    var arrayTick = [false, true, true]
    var section = Int()
    var healthRecords = [HealthRecords]()
    var titleKham = "Tất cả"
    
    @IBOutlet weak var myLabel: UILabel!
    //@IBOutlet weak var spaceHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var spaceMyViewHeightConstraint: NSLayoutConstraint!
    var searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 250, height: 18))
    var searchOn : UIBarButtonItem!
    var searchOff : UIBarButtonItem!
    fileprivate var pageNumber = 1
    fileprivate var footerScrollView = ESRefreshFooterAnimator(frame:CGRect.zero)
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after lfoading the view.
        self.tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        myTableView.delegate = self
        myTableView.dataSource = self
        tableViewPopUp.delegate = self
        tableViewPopUp.dataSource = self
        tableViewPopUp.isHidden = true
        
        myTableView.estimatedRowHeight = 70
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        
        // Setup the Search Controller
        if #available(iOS 11.0, *) {
            self.navigationItem.setHidesBackButton(true, animated: false)
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(ListScheduleDoctorVC.back(sender:)), for: UIControlEvents.touchUpInside)
            newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
        }else{
            let button: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            button.setImage(UIImage(named: "angle-left"), for: UIControlState.normal)
            button.addTarget(self, action: #selector(ListScheduleDoctorVC.back(sender:)), for: UIControlEvents.touchUpInside)
            newBackButton = UIBarButtonItem(customView: button)
        }
        
        self.navigationItem.leftBarButtonItem = newBackButton
        searchOn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(isSearchOn))
        searchOff = UIBarButtonItem(title: "Hủy", style: .plain, target: self, action: #selector(isSearchOff))
        
        //self.spaceHeightConstraint.constant = 0
        self.navigationItem.setRightBarButtonItems([searchOn], animated: true)
        
        setTitle(title: "Quản lý lịch hẹn", subtitle: "Tất cả")
        
        // create search bar
        self.tabBarController?.tabBar.isHidden = true
        searchBar.placeholder = "Tìm lịch theo ngày"
        searchBar.tintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        searchBar.isHidden = true
        searchBar.delegate = self
        searchBar.keyboardType = UIKeyboardType.numberPad
        self.myLabel.text = "Kéo xuống để tải thêm"
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        
        let nib = UINib(nibName: "CustomScheduleDoctorCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        
        let nib_header = UINib(nibName: "CustomScheduleDoctorHeader", bundle: nil)
        myTableView.register(nib_header, forHeaderFooterViewReuseIdentifier: "headerCell")
        
        let nib_popup = UINib(nibName: "QuanLyLichHen_TableViewCell", bundle: nil)
        tableViewPopUp.register(nib_popup, forCellReuseIdentifier: "popupCell")
                
        footerScrollView.loadingMoreDescription = "Tải thêm"
        self.myTableView.es.addInfiniteScrolling(animator:footerScrollView) { [weak self] in
            if let weakSelf = self {
                self!.myLabel.isHidden = true
                weakSelf.pageNumber = weakSelf.pageNumber + 1
                ScheduleDoctorOfDay.getListScheduleDoctorOfDay(pageNumber: weakSelf.pageNumber, doctorID: (self!.doctorID)!, success: { (data) in
                    weakSelf.myTableView.es.stopLoadingMore()
                    if data.count > 0 {
                        self!.scheduleDoctorOfDay?.append(contentsOf: data)
                        self!.updateDataSheduleDoctorOfDay(scheduleDoctorOfDay: self!.scheduleDoctorOfDay!)
                    } else {
                        self?.footerScrollView.loadingMoreDescription = "Đã hết dữ liệu"
                        self!.myLabel.isHidden = true
                    }
                }, fail: { (error, response) in
                    
                })
            }
        }
    }
    
    func getMedicalProfile(userID: Int, medicalProfileID: Int) {

    }
    
    func setTitle(title: String, subtitle: String) {
        let titleButton = UIButton()
        titleButton.setTitle(title, for: .normal)
        titleButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        titleButton.setTitleColor(.white, for: .normal)
        titleButton.addTarget(self, action: #selector(showPopUp), for: .touchUpInside)
        
        let subtitleButton = UIButton()
        subtitleButton.setTitle(subtitle, for: .normal)
        subtitleButton.titleLabel?.font = .systemFont(ofSize: 12)
        subtitleButton.setTitleColor(.white, for: .normal)
        subtitleButton.addTarget(self, action: #selector(showPopUp), for: .valueChanged)
        
        let stackView = UIStackView(arrangedSubviews: [titleButton, subtitleButton])
        stackView.distribution = .equalCentering
        stackView.alignment = .center
        stackView.axis = .vertical
        
        navigationItem.titleView = stackView
    }
    
    func showPopUp() {
        openNumber += 1
        if openNumber % 2 == 0 {
            tableViewPopUp.isHidden = false
        } else {
            tableViewPopUp.isHidden = true
        }
    }
    
    func isSearchOn() {
        self.navigationItem.setRightBarButtonItems([self.searchOff], animated: false)
        self.navigationItem.titleView = searchBar
        tableViewPopUp.isHidden = true
        openNumber = 1
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 40).isActive = true
        }
        searchBar.isHidden = false
        searchBar.becomeFirstResponder()
    }
    
    func isSearchOff() {
        searchBar.text = nil
        self.navigationItem.setRightBarButtonItems([self.searchOn], animated: false)
        setTitle(title: "Quản lý lịch hẹn", subtitle: "Tất cả")
        if searchBar.text == "" {
            isSearch = false
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            isSearch = false
            view.endEditing(true)
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
        else {
            isSearch = true
            filterContentForSearchText(searchText)
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if IS_IPAD{
            if #available(iOS 11.0, *) {
                // Running iOS 11 OR NEWER
                self.myTableView.frame = CGRect(x: 0, y: 108, width: myTableView.frame.size.width, height: myTableView.frame.size.height)
            }else{
                self.myTableView.frame = CGRect(x: 0, y: 40, width: myTableView.frame.size.width, height: myTableView.frame.size.height+76)
            }
            
        }else{
            if #available(iOS 11.0, *) {
                if IS_IPHONE_X{
                    self.spaceMyViewHeightConstraint.constant = 76
                }
            }
            else{
                //self.spaceHeightConstraint.constant = 0
                self.myTableView.frame = CGRect(x: 0, y: 0, width: myTableView.frame.size.width, height: myTableView.frame.size.height + 76)
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if searchBar.text != "" {
            isSearchOn()
        }else{
            isSearchOff()
        }
    }
    
    func getListScheduleDoctorOfDay(pageNumber:Int, doctorID:Int){
        let progress = self.showProgress()
        self.scheduleDoctorOfDay = [ScheduleDoctorOfDay]()
        self.listInTimes = [String]()
        self.listInTimes_Filter = [String]()
        self.doctorID = doctorID
        ScheduleDoctorOfDay.getListScheduleDoctorOfDay(pageNumber: pageNumber, doctorID: doctorID, success: { (data) in
            if data.count > 0 {
                self.scheduleDoctorOfDay?.append(contentsOf: data)
                self.updateDataSheduleDoctorOfDay(scheduleDoctorOfDay: self.scheduleDoctorOfDay!)
                self.hideProgress(progress)
            } else {
                self.hideProgress(progress)
                self.myLabel.isHidden = true
                self.lblWarning = createUILabel()
                self.myTableView.addSubview(self.lblWarning!)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func updateDataSheduleDoctorOfDay(scheduleDoctorOfDay: [ScheduleDoctorOfDay]){
        self.sectionsData = [ListScheduleDoctorVCGroup]()
        for i in 0..<Int(scheduleDoctorOfDay.count)
        {
            let scheduleDoctorObject = scheduleDoctorOfDay[i]
            let ngaykham = scheduleDoctorObject.ngaykham
            if !(self.listInTimes!.contains(ngaykham)) {
                self.listInTimes?.append(ngaykham)
            }
        }
        
        for ngayKham in self.listInTimes!{
            self.sectionsData?.append(ListScheduleDoctorVCGroup(intime: ngayKham,
                                                                items: scheduleDoctorOfDay, collapsed: true))
        }
        self.myLabel.isHidden = false
        self.myTableView.reloadData()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == myTableView {
            if searchBar.text != "" {
                self.sectionsData2 = [ListScheduleDoctorVCGroup]()
                self.listInTimes_Filter = [String]()
                
                if self.filteredNames!.count == 0{
                    return 0
                }
                for i in 0..<Int((self.filteredNames?.count)!)
                {
                    let ngaykham = self.filteredNames![i].ngaykham
                    if !(self.listInTimes_Filter!.contains(ngaykham)) {
                        self.listInTimes_Filter?.append(ngaykham)
                    }
                }
                
                for ngayKham in self.listInTimes_Filter!
                {
                    self.sectionsData2?.append(ListScheduleDoctorVCGroup(intime: ngayKham,
                                                                         items: self.filteredNames!, collapsed: true))
                }
                
                return (self.listInTimes_Filter?.count)!
            }
            return (self.listInTimes?.count)!
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if tableView == myTableView {
            return 40
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == myTableView {
            if searchBar.text != "" {
                scheduleDoctorOfDayFilter = groupNgayKhamSection(ngayKham: self.sectionsData2![section].items)
                return !sectionsData2![section].collapsed ? 0 : scheduleDoctorOfDayFilter[self.listInTimes_Filter![section]]!.count
            }
            
            scheduleDoctorOfDayGroup = groupNgayKhamSection(ngayKham: self.sectionsData![section].items)
            if section == 0{
                return !sectionsData![section].collapsed ? 0 : (self.scheduleDoctorOfDayGroup[self.listInTimes![section]]?.count)!
            }else{
                return sectionsData![section].collapsed ? 0 : (self.scheduleDoctorOfDayGroup[self.listInTimes![section]]?.count)!
            }
        } else {
            return 3
        }
    }
    
    func toggleSection(_ header: CustomScheduleDoctorHeader, section: Int) {
        if searchBar.text != "" {
            let collapsed = !(self.sectionsData2?[section].collapsed)!
            // Toggle collapse when search active
            self.sectionsData2?[section].collapsed = collapsed
            header.setCollapsed(collapsed)
        }else{
            let collapsed = !(self.sectionsData?[section].collapsed)!
            // Toggle collapse
            self.sectionsData?[section].collapsed = collapsed
            header.setCollapsed(collapsed)
        }
        
        myTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerCell") as? CustomScheduleDoctorHeader ?? CustomScheduleDoctorHeader(reuseIdentifier: "headerCell")
        if searchBar.text != "" {
            headerView.setCollapsed((sectionsData2?[section].collapsed)!)
            let inTime = self.listInTimes_Filter?[section]
            if let scheduleDoctorOfDay = self.scheduleDoctorOfDayGroup[self.listInTimes_Filter![section]]{
                headerView.inTime.text = inTime! + " (\(scheduleDoctorOfDay.count) khách hàng)"
            }
        }else{
            headerView.setCollapsed((sectionsData?[section].collapsed)!)
            let inTime = self.listInTimes?[section]
            if let scheduleDoctorOfDay = self.scheduleDoctorOfDayGroup[self.listInTimes![section]]{
                headerView.inTime.text = inTime! + " (\(scheduleDoctorOfDay.count) khách hàng)"
            }
        }
        headerView.lblArrow.font = UIFont.fontAwesome(ofSize: 18)
        headerView.lblArrow.text = String.fontAwesomeIcon(name: .angleRight)
        headerView.section = section
        headerView.delegate = self
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) ->
        CGFloat {
            if tableView == myTableView {
                return 8
            } else {
                return 0
            }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == myTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomScheduleDoctorCell
            cell.selectionStyle = .none
            cell.groupView.layer.cornerRadius = 4.0
            cell.groupView.layer.masksToBounds = true
            cell.groupView.layer.borderWidth = 2.0
            cell.groupView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            
            var scheduleDoctorObj:ScheduleDoctorOfDay? = nil
            if searchBar.text != "" {
                scheduleDoctorObj = scheduleDoctorOfDayFilter[self.listInTimes_Filter![indexPath.section]]?[indexPath.row]
            }else{
                scheduleDoctorObj = scheduleDoctorOfDayGroup[self.listInTimes![indexPath.section]]?[indexPath.row]
            }
            
            cell.lblUserName?.font = UIFont.fontAwesome(ofSize: 16.0)
            cell.lblUserName.text = "\(String.fontAwesomeIcon(name: .userCircle))  \(scheduleDoctorObj!.name)"
            
            cell.lblPhone?.font = UIFont.fontAwesome(ofSize: 16.0)
            var genderStr = ""
            if scheduleDoctorObj!.gender == "M"{
                genderStr = "Nam"
            }else{
                genderStr = "Nữ"
            }
            cell.lblPhone.text = "\(String.fontAwesomeIcon(name: .phone))  \(scheduleDoctorObj!.phone)  |  \(String.fontAwesomeIcon(name: .intersex))  \(genderStr)"
            
            if !scheduleDoctorObj!.address.isEmpty{
                cell.lblAddress?.font = UIFont.fontAwesome(ofSize: 16.0)
                cell.lblAddress.text = "\(String.fontAwesomeIcon(name: .mapMarker))  \(scheduleDoctorObj!.address)"
                cell.lblAddress.lineBreakMode = .byWordWrapping
                cell.lblAddress.numberOfLines = 0
                cell.lblAddress.adjustsFontSizeToFitWidth = true
            }else{
                cell.lblAddress?.font = UIFont.fontAwesome(ofSize: 16.0)
                cell.lblAddress.text = "\(String.fontAwesomeIcon(name: .mapMarker))  Chưa cập nhật"
            }
            
            if !scheduleDoctorObj!.giohen.isEmpty{
                cell.lblScheduleTime?.font = UIFont.fontAwesome(ofSize: 16.0)
                cell.lblScheduleTime.text = "\(String.fontAwesomeIcon(name: .calendarPlusO))  \(scheduleDoctorObj!.giohen)"
            }else{
                cell.lblScheduleTime?.font = UIFont.fontAwesome(ofSize: 16.0)
                cell.lblScheduleTime.text = "\(String.fontAwesomeIcon(name: .calendarPlusO))   Chưa cập nhật"
            }
            
            if let lydoKham = scheduleDoctorObj!.lydoKham?.trim(){
                cell.lblReason?.font = UIFont.fontAwesome(ofSize: 16.0)
                cell.lblReason.text = "\(String.fontAwesomeIcon(name: .medkit))  \(lydoKham)"
            }else{
                cell.lblReason?.font = UIFont.fontAwesome(ofSize: 16.0)
                cell.lblReason.text = "\(String.fontAwesomeIcon(name: .medkit))  Chưa cập nhật"
            }
            
            cell.btnShow.isHidden = true
            // check khach hang moi hay cu
            if scheduleDoctorObj!.KHCu == 1 && scheduleDoctorObj?.medicalProfileID != 0 {
                cell.lblHistory.text = "Đã từng khám"
                cell.btnShow.tag = indexPath.row
                section = indexPath.section
                cell.btnShow.addTarget(self, action: #selector(goHSSK), for: .touchUpInside)
                cell.btnShow.isHidden = false
            }else{
                cell.lblHistory.text = " "
            }
            
            return cell
        } else {
            let cell = tableViewPopUp.dequeueReusableCell(withIdentifier: "popupCell") as! QuanLyLichHen_TableViewCell
            cell.lblTitle.text = arrayPopUp[indexPath.row]
            
            cell.imgTick.isHidden = arrayTick[indexPath.row]
            return cell
        }
    }
    
    func goHSSK(_ sender: UIButton) {
        var scheduleDoctorObj:ScheduleDoctorOfDay? = nil
        if searchBar.text != "" {
            scheduleDoctorObj = scheduleDoctorOfDayFilter[self.listInTimes_Filter![section]]?[sender.tag]
            let detailHealthRecordsVC = DetailHealthRecordsVC(nibName: "DetailHealthRecordsVC", bundle: nil)
            detailHealthRecordsVC.isEdit = true
            detailHealthRecordsVC.delegate = self
            self.navigationController?.pushViewController(detailHealthRecordsVC, animated: true)
        } else {
            scheduleDoctorObj = scheduleDoctorOfDayGroup[self.listInTimes![section]]?[sender.tag]
            // call api
            let userID = scheduleDoctorObj?.userID
            let medicalProfileID = scheduleDoctorObj?.medicalProfileID
            HealthRecords.getMedicalProfileOneUser(userID: userID!, medicalProfileID: medicalProfileID!, success: { (data) in
                if data != nil {
                    let detailHealthRecordsVC = DetailHealthRecordsVC(nibName: "DetailHealthRecordsVC", bundle: nil)
                    detailHealthRecordsVC.isEdit = true
                    detailHealthRecordsVC.delegate = self
                    detailHealthRecordsVC.healthRecords = data[0]
                    self.navigationController?.pushViewController(detailHealthRecordsVC, animated: true)
                }
            }) { (error, response) in
                print(response)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == myTableView {
            var scheduleDoctorObj:ScheduleDoctorOfDay? = nil
            if searchBar.text != "" {
                scheduleDoctorObj = scheduleDoctorOfDayFilter[self.listInTimes_Filter![indexPath.section]]?[indexPath.row]
            }else{
                scheduleDoctorObj = scheduleDoctorOfDayGroup[self.listInTimes![indexPath.section]]?[indexPath.row]
            }
            
            if let lydoKham = scheduleDoctorObj!.lydoKham?.trim(){
                if !lydoKham.isEmpty{
                    let descReasonVC = DescReasonVC(nibName: "DescReasonVC", bundle: nil)
                    descReasonVC.strTitle = "LÝ DO KHÁM"
                    descReasonVC.strReason = lydoKham
                    descReasonVC.modalPresentationStyle = .custom
                    descReasonVC.view.frame = CGRect(x: screenSizeWidth/2-(screenSizeWidth-60)/2, y: screenSizeHeight/2-(screenSizeHeight - 300)/2, width: screenSizeWidth-60, height: screenSizeHeight - 300)
                    self.present(descReasonVC, animated: true, completion: nil)
                }
            }
        } else {
            openNumber = 1
            tableViewPopUp.isHidden = true
            switch indexPath.row {
            case 0:
                arrayTick = [false, true, true]
                setTitle(title: "Quản lý lịch hẹn", subtitle: "Tất cả")
                self.updateDataSheduleDoctorOfDay(scheduleDoctorOfDay: scheduleDoctorOfDay!)
                self.myTableView.reloadData()
            case 1:
                arrayTick = [true, false, true]
                setTitle(title: "Quản lý lịch hẹn", subtitle: "Đã từng khám")
                titleKham = "Đã từng khám"
                self.listInTimes = [String]()
                var arrayDaKham = [ScheduleDoctorOfDay]()
                for scheduleDoctorOfDayObj in scheduleDoctorOfDay! {
                    if scheduleDoctorOfDayObj.KHCu == 1 && scheduleDoctorOfDayObj.medicalProfileID != 0 {
                        arrayDaKham.append(scheduleDoctorOfDayObj)
                    }
                }
                self.updateDataSheduleDoctorOfDay(scheduleDoctorOfDay: arrayDaKham)
                self.myTableView.reloadData()
            case 2:
                arrayTick = [true, true, false]
                setTitle(title: "Quản lý lịch hẹn", subtitle: "Khách mới khám")
                titleKham = "Khách mới khám"
                self.listInTimes = [String]()
                var arrayChuaKham = [ScheduleDoctorOfDay]()
                for scheduleDoctorOfDayObj in scheduleDoctorOfDay! {
                    if scheduleDoctorOfDayObj.KHCu == 0 {
                        arrayChuaKham.append(scheduleDoctorOfDayObj)
                    }
                }
                self.updateDataSheduleDoctorOfDay(scheduleDoctorOfDay: arrayChuaKham)
                self.myTableView.reloadData()
            default:
                break
            }
            DispatchQueue.main.async {
                self.tableViewPopUp.reloadData()
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
        tableViewPopUp.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == myTableView {
            var scheduleDoctorObj:ScheduleDoctorOfDay? = nil
            if searchBar.text != "" {
                scheduleDoctorObj = scheduleDoctorOfDayFilter[self.listInTimes_Filter![indexPath.section]]?[indexPath.row]
            }else{
                scheduleDoctorObj = scheduleDoctorOfDayGroup[self.listInTimes![indexPath.section]]?[indexPath.row]
            }
            
            if !scheduleDoctorObj!.lydoKham!.trim().isEmpty{
                if IS_IPHONE_5{
                    return 190
                }else{
                    return 186
                }
            }else{
                return 186
            }
        } else {
            return 40
        }
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredNames = scheduleDoctorOfDay?.filter({( scheduleDoctorObj : ScheduleDoctorOfDay) -> Bool in
            let ngaykham = scheduleDoctorObj.ngaykham.folding(options: .diacriticInsensitive, locale: .autoupdatingCurrent)
            let keySearch = searchText.folding(options: .diacriticInsensitive, locale: .autoupdatingCurrent)
            return ngaykham.lowercased().contains(keySearch.lowercased())
        })
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
