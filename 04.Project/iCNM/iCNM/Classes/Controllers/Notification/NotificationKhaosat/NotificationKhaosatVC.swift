//
//  NotificationKhaosatVC.swift
//  iCNM
//
//  Created by Mac osx on 11/6/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import FontAwesome_swift
import Toaster

protocol NotificationKhaosatVCDelegate {
    func gobackRegisterScheduleVC()
    func destroyListServey()
}

class NotificationKhaosatVC: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate, UIPopoverPresentationControllerDelegate, UIViewControllerTransitioningDelegate {
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var myView: UIView!
    var listItemsInSection:[String: [String]] = [:]
    var listItemsInSection2:[String: [Servey]] = [:]
    var storedOffsets = [Int: CGFloat]()
    var buttons:[Int:[ISRadioButton]] = [:]
    var result = [Int:[ISRadioButton]]()
    var serveyID:Int = 0
    var postServey:[ServeyAnswer]? = nil
    var serveyQuestionID:[Int]?
    var listSection = [Servey]()
    var listServey = [String : [Servey]]()
    var delegate:NotificationKhaosatVCDelegate?
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        self.myView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        title = "Khảo sát chất lượng BV"
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        
        let btnButton =  UIButton(type: .custom)
        btnButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 16.0)
        let buttonName = String.fontAwesomeIcon(name: .sendO)
        btnButton.setTitle(buttonName, for: .normal)
        btnButton.addTarget(self, action: #selector(NotificationKhaosatVC.btnSendKSAction), for: .touchUpInside)
        btnButton.frame = CGRect(x: screenSizeWidth-20, y: 0, width: 30, height:30)
        let filterBtnItem = UIBarButtonItem(customView: btnButton)
        self.navigationItem.setRightBarButtonItems([filterBtnItem], animated: true)
        
        buttons = [Int:[ISRadioButton]]()
        postServey = [ServeyAnswer]()
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 200
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.sectionHeaderHeight = UITableViewAutomaticDimension
        myTableView.estimatedSectionHeaderHeight = 40
        
        let nib = UINib(nibName: "KhaosatCustomCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "KhaosatCell")
       
        let nib_header = UINib(nibName: "KhaosatCustomHeader", bundle: nil)
        myTableView.register(nib_header, forHeaderFooterViewReuseIdentifier: "KhaosatHeader")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func getServey(serveyID:Int){

    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.listSection.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if self.listSection.count != 0{
            let keyObj = self.listSection[section].nServeyQuesContent
            let items = self.listServey[keyObj!]
            return items!.count
        }else{
            return 0
        }
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "KhaosatHeader") as? KhaosatCustomHeader
        if self.listSection.count != 0{
            if let nServeyQuesContent = self.listSection[section].nServeyQuesContent{
                headerView?.lblTitle.font = UIFont.fontAwesome(ofSize: 16.0)
                let iconRight = String.fontAwesomeIcon(name: .handORight)
                headerView?.lblTitle.text = "  \(iconRight) \(nServeyQuesContent)"
                headerView?.lblTitle.lineBreakMode = .byWordWrapping
                headerView?.lblTitle.numberOfLines = 0
            }
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "KhaosatCell", for: indexPath) as! KhaosatCustomCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        let keyObj = self.listSection[indexPath.section].nServeyQuesContent
        if let item = self.listServey[keyObj!]{
            let serveryAnswer = item[indexPath.row]
            if IS_IPHONE_5{
                cell.lblName.font = UIFont.systemFont(ofSize: 13.0)
            }else{
                cell.lblName.font = UIFont.systemFont(ofSize: 14.0)
            }
            cell.lblName.text = serveryAnswer.nServeyAnsContent
            cell.lblName.lineBreakMode = .byWordWrapping
            cell.lblName.numberOfLines = 0
            cell.radio_btn.accessibilityHint = "\(indexPath.section)"
            cell.radio_btn.tag = indexPath.row
            self.buttons = self.selectCurrentRadioButton(section: indexPath.section, radio: cell.radio_btn)
            cell.radio_btn.addTarget(self, action: #selector(NotificationKhaosatVC.choiceItemInSection), for: UIControlEvents.touchUpInside)
        }
        return cell
    }
    
    func selectCurrentRadioButton(section:Int, radio:ISRadioButton)->[Int:[ISRadioButton]]{
        if result[section] == nil {
            result[section] = [ISRadioButton]()
        }
        result[section]?.append(radio)
        return result
    }
    
    @IBAction func choiceItemInSection(_ sender: Any) {
        let choice = sender as! ISRadioButton
        let section = Int(choice.accessibilityHint!)
        let row = choice.tag
        
        let items = self.buttons[section!]
        for item in items! {
            item.isSelected = false
        }
        choice.isSelected = true
        
        let serveyAnswerObj = ServeyAnswer()
        let keyObj = self.listSection[section!].nServeyQuesContent
        if let item = self.listServey[keyObj!]{
            serveyAnswerObj.userID = (self.currentUser?.id)!
            serveyAnswerObj.serveyID = item[row].serveyID
            serveyAnswerObj.nServeyQuesID = item[row].nServeyQuesID
            serveyAnswerObj.nServeyAnsID = item[row].nServeyAnsID
        }
       
        if let index = self.postServey?.index(where: {$0.nServeyQuesID == serveyAnswerObj.nServeyQuesID}) {
            self.postServey![index].userID = serveyAnswerObj.userID
            self.postServey![index].serveyID = serveyAnswerObj.serveyID
            self.postServey![index].nServeyQuesID = serveyAnswerObj.nServeyQuesID
            self.postServey![index].nServeyAnsID = serveyAnswerObj.nServeyAnsID
        }else{
            self.postServey?.append(serveyAnswerObj)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if IS_IPHONE_5{
            return 40
        }else{
            return UITableViewAutomaticDimension
        }
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    @IBAction func btnSendKSAction(_ sender: Any) {
        if self.postServey?.count == self.listSection.count{
            let progress = self.showProgress()
            ServeyAnswer().SendServeyAnswer(listServey:self.postServey!, success: {(result) in
                if result != nil{
                    self.hideProgress(progress)
                    Toast(text: "Gửi câu trả lời thành công! Đặt lịch hẹn ngay").show()
                    //upload success
                    self .dismiss(animated: true) {
                        self.delegate?.destroyListServey()
                    }
                }else{
                    self.hideProgress(progress)
                    Toast(text: "Gửi câu trả lời thất bại!").show()
                    //upload fail
                    self .dismiss(animated: true) {
                        
                    }
                }
            }, fail: { (error, response) in
                self.hideProgress(progress)
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }else{
            Toast(text: "Bạn vui lòng chọn câu trả lời khi tham gia khảo sát!").show()
        }
    }
}

