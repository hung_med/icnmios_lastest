//
//  NotificationViewController.swift
//  iCNM
//
//  Created by Mac osx on 9/22/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import SideMenu
import RealmSwift
import ESPullToRefresh
import Toaster
class NotificationViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {

    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var menuAction: UIBarButtonItem!
    let segmented = YSSegmentedControl(frame: .zero, titles: [])
    var scopeSelected = ScrollableSegmentedControl()
    var serviceNames: [String]?
    var currentIndex:Int = 0
    var categoryName = ""
    var userDefaults = UserDefaults.standard
    var userDefaults1 = UserDefaults.standard
    var notificationGeneral: [NotificationGeneral]?
    var notificationUser: [NotificationUser]?
    var listStatusReaderGeneral:[Bool]?
    var listStatusReaderUser:[Bool]?
   // var arrayCurrRow: [Int] = [Int]()
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    let supportFeedBackVC = StoryboardScene.Main.supportCategoriesMenuNavigationController.instantiate()
    fileprivate var headerScrollView = ESRefreshHeaderAnimator(frame:CGRect.zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationGeneral = [NotificationGeneral]()
        notificationUser = [NotificationUser]()
        menuAction.action = #selector(revealToggle(sender:))
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 200
        myTableView.rowHeight = UITableViewAutomaticDimension
        
        let nib = UINib(nibName: "NotificationTableViewCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        let nib2 = UINib(nibName: "NotificationNewsCell", bundle: nil)
        myTableView.register(nib2, forCellReuseIdentifier: "NewsCell")
       
        serviceNames = ["Chung", "Của tôi"]
        self.reloadSegmentController(names: self.serviceNames!, currentTab: 0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleLoginSuccess), name: Constant.NotificationMessage.loginSuccess, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleLogout), name: Constant.NotificationMessage.logout, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification), name: Constant.NotificationMessage.handleNotification, object: nil)
        if userDefaults.object(forKey: "receiveNotification") != nil{
            let data = userDefaults.object(forKey: "receiveNotification") as! String
            self.receiveNotificationBecomeApp(data:data)
        }
        
        if userDefaults.object(forKey: "receiveNotificationOnBackground") != nil{
            let data = userDefaults.object(forKey: "receiveNotificationOnBackground") as! String
            self.receiveNotificationBackground(data:data)
        }
        
        self.getAllNotificationGeneral()
        
        if currentUser != nil {
            if let user = currentUser {
                self.getAllNotificationUser(userID: user.id, doctorID: user.doctorID)
            }
        }else{
            confirmLogin(myView: self)
            self.tabBarController?.tabBar.items?[4].badgeValue = nil
        }
        self.setupSwipAction()
        //}
        
        // add refresh and load more control
        //add loading text
        headerScrollView.pullToRefreshDescription = "Kéo để làm mới"
        headerScrollView.releaseToRefreshDescription = "Thả để làm mới"
        headerScrollView.loadingDescription = "Đang tải..."
        // add refresh control
        self.myTableView.es.stopPullToRefresh()
        self.myTableView.es.addPullToRefresh(animator: headerScrollView) { [weak self] in
            if let weakSelf = self {
                weakSelf.checkData()
            }
        }
    }
    
    func checkData() -> Void {
        if self.currentIndex == 0{
            self.getAllNotificationGeneral()
        }else{
            if self.currentUser != nil {
                if let user = self.currentUser {
                    self.getAllNotificationUser(userID: user.id, doctorID: user.doctorID)
                }
            }else{
                confirmLogin(myView: self)
                self.tabBarController?.tabBar.items?[4].badgeValue = nil
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        SideMenuManager.default.menuRightNavigationController = supportFeedBackVC
      //  requestData()
    }
    
    /*
     * Set up swipe gesture for view
     */
    func setupSwipAction() -> Void {
        let swipeLeft = UISwipeGestureRecognizer.init(target: self, action: #selector(self.swipeLeftAndRight))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.myTableView.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer.init(target: self, action: #selector(self.swipeLeftAndRight))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.myTableView.addGestureRecognizer(swipeRight)
    }
    
    func swipeLeftAndRight(gestureRecognizer: UISwipeGestureRecognizer) {
        if gestureRecognizer.direction == .right{
            currentIndex = 0
            if let count = self.listStatusReaderGeneral?.count{
                self.tabBarController?.tabBar.items?[4].badgeValue = "\(count)"
            }else{
                self.tabBarController?.tabBar.items?[4].badgeValue = nil
            }
        }else{
            currentIndex = 1
            if let count = self.listStatusReaderUser?.count{
                self.tabBarController?.tabBar.items?[4].badgeValue = "\(count)"
            }else{
                self.tabBarController?.tabBar.items?[4].badgeValue = nil
            }
        }
        scopeSelected.selectedSegmentIndex = currentIndex
//        if !(self.tabBarController?.tabBar.items?[4].badgeValue?.isEmpty)!{
//            if let badgeValue = Int((self.tabBarController?.tabBar.items?[4].badgeValue)!){
//                self.tabBarController?.tabBar.items?[4].badgeValue = "\(badgeValue)"
//            }
//        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if IS_IPAD{
            if #available(iOS 11.0, *) {
                
            }else{
                self.myView.frame = CGRect(x: 0, y: 0, width: myView.frame.size.width, height: myView.frame.size.height)
                self.myTableView.frame = CGRect(x: 0, y: 40, width: myTableView.frame.size.width, height: myTableView.frame.size.height+76)
            }
                
        }else{
            if #available(iOS 11.0, *) {
                
            }
            else{
                self.myView.frame = CGRect(x: 0, y: 0, width: myView.frame.size.width, height: myView.frame.size.height)
                self.myTableView.frame = CGRect(x: 0, y: 40, width: myTableView.frame.size.width, height: myTableView.frame.size.height+76)
            }
            
            if IS_IPHONE_X || IS_IPHONE_XS{
                self.myView.frame = CGRect(x: 0, y: 20, width: myView.frame.size.width, height: myView.frame.size.height)
                self.myTableView.frame = CGRect(x: 0, y: 108, width: myTableView.frame.size.width, height: myTableView.frame.size.height)
            }
        }
    }
    
    func reloadSegmentController(names:[String], currentTab:Int){
        if #available(iOS 11.0, *) {
            scopeSelected.frame = CGRect(x: 0, y: 38, width: screenSizeWidth/2*CGFloat(names.count), height: 38)
        }
        else{
            scopeSelected.frame = CGRect(x: 0, y: 34, width: screenSizeWidth/2*CGFloat(names.count), height: 30)
        }
        
        var indexCate: Int = 0
        for copeTitle in names {
            scopeSelected.insertSegment(withTitle: copeTitle, at: indexCate)
            indexCate += 1
        }
        
        scopeSelected.addTarget(self, action: #selector(NotificationViewController.switchChanged), for: .valueChanged)
        scopeSelected.segmentContentColor = UIColor.lightGray
        scopeSelected.underlineSelected = true
        scopeSelected.segmentStyle = .textOnly
        scopeSelected.backgroundColor = UIColor.clear
        scopeSelected.tintColor = UIColor(hex:"1976D2")
        // add it in a scrollView, because ill got too much categories here. Just if you need that:
    
        let scrollView  = UIScrollView()
        if #available(iOS 11.0, *) {
            scrollView.frame = CGRect(x:0, y: 34, width: screenSizeWidth, height: 100)
        }
        else{
            scrollView.frame = CGRect(x:0, y: 45, width: screenSizeWidth, height: 100)
        }
        scrollView.contentSize = CGSize(width: scopeSelected.frame.size.width + 16,height: scopeSelected.frame.size.height-1)
        scrollView.showsHorizontalScrollIndicator = false
        // set first category
        scopeSelected.selectedSegmentIndex = 0
        scrollView.addSubview(scopeSelected)
        
        myView?.addSubview(scrollView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
        if IS_IPAD{
            if #available(iOS 11.0, *) {
            }
        }else{
            if IS_IPHONE_X || IS_IPHONE_XS{
                self.myView.frame = CGRect(x: 0, y: 20, width: myView.frame.size.width, height: myView.frame.size.height)
                self.myTableView.frame = CGRect(x: 0, y: 108, width: myTableView.frame.size.width, height: myTableView.frame.size.height)
            }
        }
    }
    
    func switchChanged(_ sender: ScrollableSegmentedControl){
        currentIndex = sender.selectedSegmentIndex
        if currentIndex == 0{
            if let count = self.listStatusReaderGeneral?.count{
                self.tabBarController?.tabBar.items?[4].badgeValue = "\(count)"
            }else{
                self.tabBarController?.tabBar.items?[4].badgeValue = nil
            }
        }else{
            if let count = self.listStatusReaderUser?.count{
                self.tabBarController?.tabBar.items?[4].badgeValue = "\(count)"
            }else{
                self.tabBarController?.tabBar.items?[4].badgeValue = nil
            }
        }
        if (currentUser == nil && currentIndex == 1){
            self.notificationUser = [NotificationUser]()
            self.myTableView .reloadData()
            confirmLogin(myView: self)
            self.tabBarController?.tabBar.items?[4].badgeValue = nil
        }
        
        else if (currentIndex == 1 && self.notificationUser?.count == 0){
            Toast(text: "Không có dữ liệu").show()
        }
        
        self.myTableView .reloadData()
    }
    
    
    func receiveNotificationBackground(data:String){
        self.directionCurrentVC(data: data)
        userDefaults.removeObject(forKey: "receiveNotificationOnBackground")
    }
    
    func receiveNotificationBecomeApp(data:String){
        self.directionCurrentVC(data: data)
        userDefaults.removeObject(forKey: "receiveNotification")
    }
    
    // handleNotification
    func handleNotification(aNotification:Notification) {
        let data = aNotification.object as! String
        self.directionCurrentVC(data: data)
    }
    
    func directionCurrentVC(data:String){
        if data.contains("Result"){
            let result = splitString(str: data)
            if let viewController = UIStoryboard(name: "DetailLookUp", bundle: nil).instantiateViewController(withIdentifier: "DetailLookUpResultVC") as? DetailLookUpResultVC{
                let pid = result[0]
                let sid = result[2]
                let organizeID = result[3]
                viewController.hidesBottomBarWhenPushed = true
                viewController.getListLookUpResultBySID(sid: sid, pID: pid, organizeID:organizeID)
                self.navigationController!.pushViewController(viewController, animated: true)
            }
        }else if data.contains("TaiKham"){
            if userDefaults.object(forKey: "body") != nil && userDefaults.object(forKey: "title") != nil{
                let body = userDefaults.object(forKey: "body") as! String
                let title = userDefaults.object(forKey: "title") as! String
                let notificationRemindVC = NotificationRemindVC(nibName: "NotificationRemindVC", bundle: nil)
                notificationRemindVC.dataNotification = data
                notificationRemindVC.descNotification = body
                notificationRemindVC.titleNotification = title
                notificationRemindVC.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(notificationRemindVC, animated: true)
                userDefaults.removeObject(forKey: "body")
                userDefaults.removeObject(forKey: "title")
            }
        }else if data.contains("News"){
            let result = splitDataNewsToString(str: data)
            let featuredNewsObj = FeaturedNews()
            featuredNewsObj.newsModel.categoryID = Int(result[2])!
            self.getCategoryByNewsID(result:result, categoryID:featuredNewsObj.newsModel.categoryID, isCheck:true)
        }else if data.contains("Service"){
            /*
            let result = splitDataServiceToString(str: data)
            let serviceID = Int(result[1])!
            let servicePackageDetailVC = ServicePackageDetailVC(nibName: "ServicePackageDetailVC", bundle: nil)
            servicePackageDetailVC.isCheck = true
            servicePackageDetailVC.getAllServiceDetailGroups(serviceID:serviceID)
            self.navigationController?.pushViewController(servicePackageDetailVC, animated: true)
             */
        }else if data.contains("VerifyDoctor"){
            let title = "Xin chúc mừng bạn! Tài khoản bác sĩ của bạn đã được xác thực. Bạn vui lòng đăng nhập lại hệ thống để hoàn thành việc xác thực"
            let alert = UIAlertController(title: "Thông báo", message: title, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Đăng nhập", style: .default, handler: { action in
                NotificationCenter.default.addObserver(self, selector: #selector(self.handleLogout), name: Constant.NotificationMessage.logout, object: nil)
                let loginNavigationController = StoryboardScene.LoginRegister.loginNavigationController.instantiate()
                self.present(loginNavigationController, animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }else if data.contains("AcceptAnswerDoctor"){
            let title = "Xin chúc mừng bạn! Tài khoản bác sĩ đăng ký chuyên khoa trả lời câu hỏi của bạn đã được duyệt"
            let alert = UIAlertController(title: "Thông báo", message: title, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Đăng nhập", style: .default, handler: { action in
                NotificationCenter.default.addObserver(self, selector: #selector(self.handleLogout), name: Constant.NotificationMessage.logout, object: nil)
                let loginNavigationController = StoryboardScene.LoginRegister.loginNavigationController.instantiate()
                self.present(loginNavigationController, animated: true, completion: nil)
                
            }))
            self.present(alert, animated: true, completion: nil)
        }else if data.contains("ProcessStatus"){
            let result = splitDataToString(str: data)
            let scheduleID = Int(result[1])!
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let processManagerVC = storyboard.instantiateViewController(withIdentifier: "ProcessManagerVC") as! ProcessManagerViewController
            processManagerVC.hidesBottomBarWhenPushed = true
            processManagerVC.getDataProcessManager(scheduleID: scheduleID)
            self.navigationController?.pushViewController(processManagerVC, animated: true)
        }else if data.contains("NewQuestion"){
            let questionDoctorVC = ManagerQuestionDoctorVC(nibName: "ManagerQuestionDoctorVC", bundle: nil)
            questionDoctorVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(questionDoctorVC, animated: true)
        }else if data.contains("RemindQuestionAnswer"){
            let questionDoctorVC = ManagerQuestionDoctorVC(nibName: "ManagerQuestionDoctorVC", bundle: nil)
            questionDoctorVC.isChecked = true
            questionDoctorVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(questionDoctorVC, animated: true)
        }
        else if data.contains("UserApproverAnswer") || data.contains("DoctorApproverAnswer"){
            let alert = UIAlertController(title: "Thông báo", message: "Nội dung câu trả lời chỉ mang tính tham khảo, quý khách hàng nên đến gặp Bác sĩ trực tiếp để được tư vấn chính xác nhất!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Đồng ý", style: .cancel, handler: { action in
                self.presentedDetailQuestionAnswer(data: data)
            }))
            self.present(alert, animated: true, completion: nil)
        }else if data.contains("UpdatePID"){
            let healthRecordsVC = HealthRecordsVC(nibName: "HealthRecordsVC", bundle: nil)
            if let user = currentUser {
                healthRecordsVC.getMedicalProfileByUserID(userID:user.id)
                healthRecordsVC.isCheckNoti = true
            }
            healthRecordsVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(healthRecordsVC, animated: true)
        }else if data.contains("ScheduleDoctorOfDay"){
            let result = splitDataToString(str: data)
            let doctorID = Int(result[1])
            let scheduleDoctorVC = ListScheduleDoctorVC(nibName: "ListScheduleDoctorVC", bundle: nil)
            scheduleDoctorVC.getListScheduleDoctorOfDay(pageNumber: 1, doctorID: doctorID!)
            scheduleDoctorVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(scheduleDoctorVC, animated: true)
        }else if data.contains("ScheduleUser"){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let scheduleAnAppointmentVC = storyboard.instantiateViewController(withIdentifier: "ViewAppointmentListVC") as! ViewAppointmentListViewController
            scheduleAnAppointmentVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(scheduleAnAppointmentVC, animated: true)
        }
    }
    
    @IBAction func touchOnChangeSupportFeedback(_ sender: Any) {
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    func getAllNotificationGeneral(){
        self.notificationGeneral = [NotificationGeneral]()
        self.listStatusReaderGeneral = [Bool]()
        NotificationGeneral().getAllNotificationGeneral(success: { (data) in
            if data.count > 0 {
                self.notificationGeneral?.append(contentsOf: data)
                for i in 0..<Int((self.notificationGeneral?.count)!){
                    let isRead = self.notificationGeneral![i].isRead
                    if isRead != true{
                        self.listStatusReaderGeneral?.append(false)
                    }
                }
                self.myTableView.reloadData()
                self.myTableView.es.stopPullToRefresh()
                Loader.addLoaderTo(self.myTableView)
                Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(NotificationViewController.loaded), userInfo: nil, repeats: true)
                
                if let badgeValue = self.userDefaults.object(forKey: "badgeValue"){
                    self.tabBarController?.tabBar.items?[4].badgeValue = "\(badgeValue)"
                }
            } else {
                self.myTableView.es.stopPullToRefresh()
                Loader.removeLoaderFrom(self.myTableView)
            }
        }, fail: { (error, response) in
            self.myTableView.es.stopPullToRefresh()
            Loader.removeLoaderFrom(self.myTableView)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func loaded()
    {
        Loader.removeLoaderFrom(self.myTableView)
    }
    
    func getAllNotificationUser(userID:Int, doctorID:Int){
        self.notificationUser = [NotificationUser]()
        self.listStatusReaderUser = [Bool]()
        NotificationUser().getAllNotificationUser(userID: userID, doctorID:doctorID, success: { (data) in
            if data.count > 0 {
                self.notificationUser?.append(contentsOf: data)
                    for i in 0..<Int((self.notificationUser?.count)!){
                        let isRead = self.notificationUser![i].isRead
                        if isRead != true{
                            self.listStatusReaderUser?.append(false)
                        }
                }
                self.myTableView.reloadData()
                self.myTableView.es.stopPullToRefresh()
            } else {
                self.myTableView.es.stopPullToRefresh()
                Loader.removeLoaderFrom(self.myTableView)
            }
        }, fail: { (error, response) in
            //self.hideProgress(progress)
            self.myTableView.es.stopPullToRefresh()
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }

    func revealToggle(sender: UIBarButtonItem) {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    @IBAction func test(_ sender: Any) {
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if currentIndex == 0{
            if notificationGeneral!.count > 0{
                return notificationGeneral!.count
            }else{
                return 0
            }
        }else{
            if notificationUser!.count > 0{
                return notificationUser!.count
            }else{
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var notificationGeneral:NotificationGeneral? = nil
        var notificationUser:NotificationUser? = nil
        if currentIndex == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NotificationNewsCell
            cell.contentView.backgroundColor = UIColor.white
            cell.contentView.dropShadow(color: .groupTableViewBackground, opacity: 1, offSet: .zero, radius: 1)
            if self.notificationGeneral!.count > 0{
                notificationGeneral = self.notificationGeneral?[indexPath.row]
                if let desc = notificationGeneral?.title{
                    cell.lblDescription.text = desc
                }
                
                if let title = notificationGeneral?.description{
                    cell.lblTitle.text = title
                    cell.lblTitle.lineBreakMode = .byWordWrapping
                    cell.lblTitle.numberOfLines = 0
                }
                
                if let img = notificationGeneral?.img{
                    cell.imgThumb.contentMode = .scaleToFill
                    // cell.imgThumb.contentMode = UIViewContentMode.scaleAspectFill
                    let urlString = img.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                    cell.imgThumb.loadImageUsingUrlString(urlString: urlString!)
                }
                
                if let dateCreate = notificationGeneral?.dateCreate{
                    //get datetime server
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    var date = dateFormatter.date(from: dateCreate)
                    if date == nil {
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                        date = dateFormatter.date(from: dateCreate)
                    }
                    
                    let dateTime = date?.timeAgoSinceNow()
                    if let currentDateTime = dateTime{
                        cell.lblDateCreate.font = UIFont.fontAwesome(ofSize: 14.0)
                        let timeStr = String.fontAwesomeIcon(name: .listAlt) + "  "+currentDateTime
                        cell.lblDateCreate.text = timeStr
                    }
                }
            }
            return cell
        }
        else{
            if self.notificationUser?.count != 0{
                notificationUser = self.notificationUser?[indexPath.row]
                if (notificationUser?.data.contains("Service"))! || (notificationUser?.data.contains("ProcessStatus"))! ||
                    (notificationUser?.data.contains("ScheduleDoctorOfDay"))! ||
                    (notificationUser?.data.contains("ScheduleUser"))!{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NotificationNewsCell
                    
                    if let desc = notificationUser?.title{
                        cell.lblDescription.text = desc
                    }
                    
                    if let title = notificationUser?.description{
                        cell.lblTitle.text = title
                        cell.lblTitle.lineBreakMode = .byWordWrapping
                        cell.lblTitle.numberOfLines = 0
                    }
                    
                    if let isRead = notificationUser?.isRead{
                        if isRead{
                            cell.contentView.backgroundColor = UIColor.white
                        }
                    }else{
                        cell.contentView.backgroundColor = UIColor.init(hex: "ffe3f2fd")
                    }
                    
                    if let img = notificationUser?.img{
                        cell.imgThumb.contentMode = .scaleToFill
                        if !img.isEmpty{
                            let urlString = img.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                            cell.imgThumb.loadImageUsingUrlString(urlString: urlString!)
                        }else{
                            cell.imgThumb.image = UIImage(named: "icon_process.jpg")
                        }
                    }
                    
                    if let dateCreate = notificationUser?.dateCreate{
                        //get datetime server
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                        var date = dateFormatter.date(from: dateCreate)
                        if date == nil {
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                            date = dateFormatter.date(from: dateCreate)
                        }
                        
                        let dateTime = date?.timeAgoSinceNow()
                        if let currentDateTime = dateTime{
                            cell.lblDateCreate.font = UIFont.fontAwesome(ofSize: 14.0)
                            let timeStr = String.fontAwesomeIcon(name: .listAlt) + "  "+currentDateTime
                            cell.lblDateCreate.text = timeStr
                        }
                    }
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! NotificationTableViewCell
                    // cell.contentView.dropShadow(color: .groupTableViewBackground, opacity: 1, offSet: .zero, radius: 1)
                    
                    if let desc = notificationUser?.title{
                        cell.lblDescription.text = desc
                    }
                    
                    if let title = notificationUser?.description{
                        cell.lblTitle.text = title
                        cell.lblTitle.lineBreakMode = .byWordWrapping
                        cell.lblTitle.numberOfLines = 0
                    }
                    
                    if let isRead = notificationUser?.isRead{
                        if isRead{
                            cell.contentView.backgroundColor = UIColor.white
                        }
                    }else{
                        cell.contentView.backgroundColor = UIColor.init(hex: "ffe3f2fd")
                    }
                    
                    if let dateCreate = notificationUser?.dateCreate{
                        //get datetime server
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                        var date = dateFormatter.date(from: dateCreate)
                        if date == nil {
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                            date = dateFormatter.date(from: dateCreate)
                        }
                        
                        let dateTime = date?.timeAgoSinceNow()
                        if let currentDateTime = dateTime{
                            cell.lblDateCreate.font = UIFont.fontAwesome(ofSize: 14.0)
                            if (notificationUser?.data.contains("TaiKham"))!{
                                let timeStr = String.fontAwesomeIcon(name: .calendar) + "  "+currentDateTime
                                cell.lblDateCreate.text = timeStr
                            }else{
                                let timeStr = String.fontAwesomeIcon(name: .listAlt) + "  "+currentDateTime
                                cell.lblDateCreate.text = timeStr
                            }
                        }
                    }
                    
                    return cell
                }
            }
            return UITableViewCell.init()
        }
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var notificationGeneral:NotificationGeneral? = nil
        var notificationUser:NotificationUser? = nil
        
        if currentIndex == 0{
            notificationGeneral = self.notificationGeneral?[indexPath.row]
            if let title = notificationGeneral?.title, let desc = notificationGeneral?.description, let data = notificationGeneral?.data{
                if data.contains("News"){
                    let result = splitDataNewsToString(str: data)
                    let featuredNewsObj = FeaturedNews()
                    featuredNewsObj.newsModel.categoryID = Int(result[2])!
                    self.getCategoryByNewsID(result:result, categoryID:featuredNewsObj.newsModel.categoryID, isCheck:false)
                }else if data.contains("TaiKham"){
                    let notificationRemindVC = NotificationRemindVC(nibName: "NotificationRemindVC", bundle: nil)
                    notificationRemindVC.titleNotification = title
                    notificationRemindVC.descNotification = desc
                    notificationRemindVC.dataNotification = data
                    self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.pushViewController(notificationRemindVC, animated: true)
                }else if data.contains("Service"){
                    let result = splitDataToString(str: data)
                    let serviceID = Int(result[1])!
                    let servicePackageDetailVC = ServicePackageDetailVC(nibName: "ServicePackageDetailVC", bundle: nil)
                    servicePackageDetailVC.isCheck = true
                    servicePackageDetailVC.getAllServiceDetailGroups(serviceID:serviceID)
                    servicePackageDetailVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(servicePackageDetailVC, animated: true)
                }else{
                    return
                }
                
                if data.contains("News") || data.contains("Service"){
                    let index = IndexPath(item: indexPath.row, section: 0)
                    if let cell:NotificationNewsCell = self.myTableView.cellForRow(at: index) as? NotificationNewsCell{
                        cell.contentView.backgroundColor = UIColor.white
                        userDefaults.set(self.tabBarController?.tabBar.items?[4].badgeValue, forKey: "badgeValue")
                        userDefaults.synchronize()
                       
                    }
                }
            }
        }else{
            notificationUser = self.notificationUser?[indexPath.row]
            if let title = notificationUser?.title, let data = notificationUser?.data, let desc = notificationUser?.description, let listNewsID = notificationUser?.listNewsID{
                if data.contains("Result"){
                    self.tabBarController?.tabBar.isHidden = true
                    let result = splitString(str: data)
                    if let viewController = UIStoryboard(name: "DetailLookUp", bundle: nil).instantiateViewController(withIdentifier: "DetailLookUpResultVC") as? DetailLookUpResultVC{
                        let pid = result[0]
                        let sid = result[2]
                        let organizeID = result[3]
                        viewController.getListLookUpResultBySID(sid: sid, pID: pid, organizeID:organizeID)
                        self.navigationController!.pushViewController(viewController, animated: true)
                    }
                }else if data.contains("TaiKham"){
                    let notificationRemindVC = NotificationRemindVC(nibName: "NotificationRemindVC", bundle: nil)
                    notificationRemindVC.titleNotification = title
                    notificationRemindVC.descNotification = desc
                    notificationRemindVC.dataNotification = data
                    notificationRemindVC.listNewsID = listNewsID.trim()
                    self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.pushViewController(notificationRemindVC, animated: true)
                }else if data.contains("News"){
                    let result = splitDataNewsToString(str: data)
                    let featuredNewsObj = FeaturedNews()
                    featuredNewsObj.newsModel.categoryID = Int(result[2])!
                    self.getCategoryByNewsID(result:result, categoryID:featuredNewsObj.newsModel.categoryID, isCheck:false)
                }else if data.contains("Service"){
                    let result = splitDataToString(str: data)
                    let serviceID = Int(result[1])!
                    let servicePackageDetailVC = ServicePackageDetailVC(nibName: "ServicePackageDetailVC", bundle: nil)
                    servicePackageDetailVC.isCheck = true
                    servicePackageDetailVC.getAllServiceDetailGroups(serviceID:serviceID)
                    servicePackageDetailVC.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(servicePackageDetailVC, animated: true)
                }else if data.contains("VerifyDoctor"){
                    let title = "Xin chúc mừng bạn! Tài khoản bác sĩ của bạn đã được xác thực. Bạn vui lòng đăng nhập lại hệ thống để hoàn thành việc xác thực."
                    let alert = UIAlertController(title: "Thông báo", message: title, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Đăng nhập", style: .default, handler: { action in
                        NotificationCenter.default.addObserver(self, selector: #selector(self.handleLogout), name: Constant.NotificationMessage.logout, object: nil)
                        let loginNavigationController = StoryboardScene.LoginRegister.loginNavigationController.instantiate()
                        self.present(loginNavigationController, animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }else if data.contains("AcceptAnswerDoctor"){
                    let title = "Xin chúc mừng bạn! Tài khoản bác sĩ đăng ký chuyên khoa trả lời câu hỏi của bạn đã được duyệt."
                    let alert = UIAlertController(title: "Thông báo", message: title, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Đăng nhập", style: .default, handler: { action in
                        NotificationCenter.default.addObserver(self, selector: #selector(self.handleLogout), name: Constant.NotificationMessage.logout, object: nil)
                        let loginNavigationController = StoryboardScene.LoginRegister.loginNavigationController.instantiate()
                        self.present(loginNavigationController, animated: true, completion: nil)
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }else if data.contains("NewQuestion"){
                    let questionDoctorVC = ManagerQuestionDoctorVC(nibName: "ManagerQuestionDoctorVC", bundle: nil)
                    self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.pushViewController(questionDoctorVC, animated: true)
                }else if data.contains("RemindQuestionAnswer"){
                    let questionDoctorVC = ManagerQuestionDoctorVC(nibName: "ManagerQuestionDoctorVC", bundle: nil)
                    questionDoctorVC.isChecked = true
                    self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.pushViewController(questionDoctorVC, animated: true)
                }else if data.contains("UserApproverAnswer") || data.contains("DoctorApproverAnswer"){
                    let alert = UIAlertController(title: "Thông báo", message: "Nội dung câu trả lời chỉ mang tính tham khảo, quý khách hàng nên đến gặp Bác sĩ trực tiếp để được tư vấn chính xác nhất!", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Đồng ý", style: .cancel, handler: { action in
                        self.presentedDetailQuestionAnswer(data: data)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }else if data.contains("UpdatePID"){
                    let healthRecordsVC = HealthRecordsVC(nibName: "HealthRecordsVC", bundle: nil)
                    if let user = currentUser {
                        healthRecordsVC.getMedicalProfileByUserID(userID:user.id)
                        healthRecordsVC.isCheckNoti = true
                    }
                    self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.pushViewController(healthRecordsVC, animated: true)
                }else if data.contains("ProcessStatus"){
                    self.tabBarController?.tabBar.isHidden = true
                    let result = splitDataToString(str: data)
                    let scheduleID = Int(result[1])!
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let processManagerVC = storyboard.instantiateViewController(withIdentifier: "ProcessManagerVC") as! ProcessManagerViewController
                    processManagerVC.getDataProcessManager(scheduleID: scheduleID)
                    self.navigationController?.pushViewController(processManagerVC, animated: true)
                }else if data.contains("ScheduleDoctorOfDay"){
                    self.tabBarController?.tabBar.isHidden = true
                    let result = splitDataToString(str: data)
                    let doctorID = Int(result[1])
                    let scheduleDoctorVC = ListScheduleDoctorVC(nibName: "ListScheduleDoctorVC", bundle: nil)
                    scheduleDoctorVC.getListScheduleDoctorOfDay(pageNumber: 1, doctorID: doctorID!)
                    self.navigationController?.pushViewController(scheduleDoctorVC, animated: true)
                }else if data.contains("ScheduleUser"){
                    self.tabBarController?.tabBar.isHidden = true
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let scheduleAnAppointmentVC = storyboard.instantiateViewController(withIdentifier: "ViewAppointmentListVC") as! ViewAppointmentListViewController
                    self.navigationController?.pushViewController(scheduleAnAppointmentVC, animated: true)
                }
                
                if let userID = self.currentUser?.id, let notificationID = notificationUser?.notificationID{
                    let index = IndexPath(item: indexPath.row, section: 0)
                    if data.contains("Result") || data.contains("TaiKham") || data.contains("VerifyDoctor") || data.contains("AcceptAnswerDoctor") || data.contains("NewQuestion") || data.contains("UserApproverAnswer") || data.contains("DoctorApproverAnswer") || data.contains("UpdatePID") || data.contains("RemindQuestionAnswer") || data.contains("ProcessStatus") || data.contains("News") || data.contains("ScheduleUser") || data.contains("ScheduleDoctorOfDay"){
                        if let cell:NotificationTableViewCell = self.myTableView.cellForRow(at: index) as? NotificationTableViewCell{
                            self.updateStatusReadNotification(notificationID: notificationID, userID: userID)
                            let current_Color = cell.contentView.backgroundColor
                            if current_Color == UIColor.init(hex: "ffe3f2fd"){
                                let badgeValue = Int((self.tabBarController?.tabBar.items?[4].badgeValue)!)
                                if badgeValue! > 0{
                                    self.tabBarController?.tabBar.items?[4].badgeValue = "\(badgeValue!-1)"
                                }else{
                                    self.tabBarController?.tabBar.items?[4].badgeValue = nil
                                }
                                cell.contentView.backgroundColor = UIColor.white
                            }else{
                                return
                            }
                        }else if let cell:NotificationNewsCell = self.myTableView.cellForRow(at: index) as? NotificationNewsCell{
                            if data.contains("ProcessStatus") || data.contains("ScheduleDoctorOfDay") || data.contains("ScheduleUser"){
                                self.updateStatusReadNotification(notificationID: notificationID, userID: userID)
                                let current_Color = cell.contentView.backgroundColor
                                if current_Color == UIColor.init(hex: "ffe3f2fd"){
                                    let badgeValue = Int((self.tabBarController?.tabBar.items?[4].badgeValue)!)
                                    if badgeValue! > 0{
                                        self.tabBarController?.tabBar.items?[4].badgeValue = "\(badgeValue!-1)"
                                    }else{
                                        self.tabBarController?.tabBar.items?[4].badgeValue = nil
                                    }
                                    cell.contentView.backgroundColor = UIColor.white
                                }else{
                                    return
                                }
                            }
                        }
                    }else if data.contains("Service"){
                        if let cell:NotificationNewsCell = self.myTableView.cellForRow(at: index) as? NotificationNewsCell{
                            self.updateStatusReadNotification(notificationID: notificationID, userID: userID)
                            let current_Color = cell.contentView.backgroundColor
                            if current_Color == UIColor.init(hex: "ffe3f2fd"){
                                let badgeValue = Int((self.tabBarController?.tabBar.items?[4].badgeValue)!)
                                if badgeValue! > 0 {
                                    self.tabBarController?.tabBar.items?[4].badgeValue = "\(badgeValue!-1)"
                                }else{
                                    self.tabBarController?.tabBar.items?[4].badgeValue = nil
                                }
                            } else {
                                return
                            }
                            cell.contentView.backgroundColor = UIColor.white
                        }
                    }
                }
            }
        }
    }
    
    func presentedDetailQuestionAnswer(data:String){
        let questions = QuestionAnswer()
        //get QuestionID
        let result = splitDataToString(str: data)
        let questionID = Int(result[1])!
        questions.getAnswerIDFromQuestionID(questionID: questionID, success: {  (result) in
            if result == nil{
                return
            }else{
                questions.question = result?.question
                questions.answer = result?.answer
                questions.userAsk = result?.userAsk
                questions.userOfDoctor = result?.userOfDoctor
                questions.doctor = result?.doctor
                questions.specialist = result?.specialist
                questions.thanked = (result?.thanked)!
                questions.liked = (result?.liked)!
                questions.shared = (result?.shared)!
                
                let qADetailVC = StoryboardScene.Main.qaDetail.instantiate()
                qADetailVC.questionAnswer = questions
                self.navigationController?.pushViewController(qADetailVC, animated: true)
            }
        }, fail: { (error, response) in
            
        })
    }
    
    func updateStatusReadNotification(notificationID:Int, userID:Int){
        NotificationUser().updateStatusReadNotification(notificationID:notificationID, userID:userID, success: { (data) in
            if data != nil {
                self.currentIndex = 1
                if self.currentUser != nil {
                    if let user = self.currentUser {
                        self.getAllNotificationUser(userID: user.id, doctorID: user.doctorID)
                    }
                }else{
                    confirmLogin(myView: self)
                    self.tabBarController?.tabBar.items?[4].badgeValue = nil
                }
                self.setupSwipAction()

            } else {
                
            }
        }, fail: { (error, response) in
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    override func requestData() {
        if let user = currentUser {
            let progress = showProgress()
            user.requestUserInformation(success: { (newUserInfo) in
                self.hideProgress(progress)
                NotificationCenter.default.post(name: Constant.NotificationMessage.profileEdited, object: self)
            }, fail: { (error, response) in
                self.hideProgress(progress)
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
    }
    
    func getCategoryByNewsID(result:[String], categoryID:Int, isCheck:Bool){
        let progress = self.showProgress()
        UserNewsCategories.getCategoryByNewsID(categoryID:categoryID, success: { (data) in
            if data.count > 0 {
                self.categoryName = data[0].name!
                self.hideProgress(progress)
                let newsDetailVC = StoryboardScene.Main.newsDetailViewController.instantiate()
                let featuredNewsObj = FeaturedNews()
                featuredNewsObj.categoryName = self.categoryName
                featuredNewsObj.newsModel.categoryID = Int(result[2])!
                featuredNewsObj.newsModel.newsID = Int(result[1])!
                let newsCategory = NewsCategory()
                newsCategory.id = featuredNewsObj.newsModel.categoryID
                newsCategory.name = featuredNewsObj.categoryName
                newsDetailVC.newsCategoryInput = newsCategory
                
                let model =  featuredNewsObj.newsModel
                newsDetailVC.newsModelInput = model
                newsDetailVC.isCheck = isCheck
                self.navigationController?.pushViewController(newsDetailVC, animated: true)
            } else {
                self.hideProgress(progress)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }

    func handleLoginSuccess(aNotification:Notification) {
        let realm = try! Realm()
        currentUser = realm.currentUser()
        if currentUser != nil {
            if let user = currentUser {
                self.getAllNotificationUser(userID: user.id, doctorID: user.doctorID)
            }
        }
        self.myTableView.reloadData()
    }
    
    func handleLogout(aNotification:Notification) {
        currentUser = nil
        notificationUser = [NotificationUser]()
        self.myTableView.reloadData()
        self.tabBarController?.tabBar.items?[4].badgeValue = "0"
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5, animations: {
            self.tabBarController?.tabBar.isHidden = true
        }, completion: {
            finished in
            self.tabBarController?.tabBar.isHidden = false
        })
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5, animations: {
            self.tabBarController?.tabBar.isHidden = false
        }, completion: {
            finished in
            self.tabBarController?.tabBar.isHidden = true
        })
    }
}
