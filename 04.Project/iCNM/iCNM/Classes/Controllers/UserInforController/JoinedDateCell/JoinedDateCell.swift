//
//  JoinedDateCell.swift
//  iCNM
//
//  Created by ngvdung on 8/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class JoinedDateCell: UITableViewCell {
    
    @IBOutlet weak var lbContent1: UILabel!
    @IBOutlet weak var lbContent2: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
