//
//  DetailUserInfoCell.swift
//  iCNM
//
//  Created by Mac osx on 10/6/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class DetailUserInfoCell: UITableViewCell {

    @IBOutlet weak var iconImage:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.iconImage.contentMode = .scaleAspectFit
        let frame = CGRect(x: self.iconImage.frame.origin.x, y: self.iconImage.frame.origin.y, width: 20, height: 20)
        self.iconImage.frame = frame
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
