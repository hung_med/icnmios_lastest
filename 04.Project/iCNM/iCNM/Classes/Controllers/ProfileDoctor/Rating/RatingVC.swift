//
//  LookupResultVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Alamofire
import Cosmos
import RealmSwift

protocol RatingVCDelegate {
    func reloadRatingUser(general:Double, ratingTechnique:Double, medicalEthics:Double, noidung:String, sumRate:Double)
    func goBackProfileDoctor()
}

class RatingVC: BaseViewController, UIPopoverPresentationControllerDelegate, UIViewControllerTransitioningDelegate, UITextViewDelegate{
   
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var cosmosViewCamNhanChung: CosmosView!
    @IBOutlet weak var cosmosViewChuyenMon: CosmosView!
    @IBOutlet weak var cosmosViewThaiDo: CosmosView!
    @IBOutlet weak var tvNoiDung: UITextView!
    var listUserComment:[UserRating]? = nil
    var delegate:RatingVCDelegate?
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.myView.layer.cornerRadius = 10
        self.myView.layer.borderWidth = 1
        self.myView.layer.masksToBounds = true
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        
        self.tvNoiDung.layer.masksToBounds = true
        self.tvNoiDung.layer.borderWidth = 0.5
        self.tvNoiDung.layer.borderColor = UIColor.lightGray.cgColor
        self.tvNoiDung.layer.cornerRadius = 5
        self.tvNoiDung.delegate = self
        self.tvNoiDung.returnKeyType = UIReturnKeyType.done
        if listUserComment != nil{
            self.performRatingAction(listUserComment:listUserComment!)
        }
    }
    
    func performRatingAction(listUserComment:[UserRating]) {
        if self.currentUser == nil {
            confirmLogin(myView: self)
        } else {
            if self.currentUser != nil {
                if let user = self.currentUser{
                    for listComment in listUserComment{
                        if let userInfo = listComment.user{
                            if userInfo.id == user.id{
                                let rating = listComment.rating
                                if let rateContent = rating?.rateContent{
                                    self.tvNoiDung.text! = rateContent
                                }
                                if let general = rating?.general{
                                    self.cosmosViewCamNhanChung.rating = Double(general)
                                }
                                if let technique = rating?.technique{
                                    self.cosmosViewChuyenMon.rating = Double(technique)
                                }
                                if let medicalEthics = rating?.medicalEthics{
                                    self.cosmosViewThaiDo.rating = Double(medicalEthics)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let frame = CGRect(x: screenSizeWidth/2 - self.myView.frame.width/2, y: screenSizeHeight/2 - self.myView.frame.height/2, width: self.myView.frame.width, height: self.myView.frame.height)
        self.myView.frame = frame
    }
    
    
    // MARK: IBAction
    @IBAction func hideRatingAction(_ sender: Any) {
        self.cosmosViewCamNhanChung.rating = 0
        self.cosmosViewChuyenMon.rating = 0
        self.cosmosViewThaiDo.rating = 0
        self.tvNoiDung.text = ""
        delegate?.goBackProfileDoctor()
    }
    
    @IBAction func submitRatingAction(_ sender: Any){
        let general = self.cosmosViewCamNhanChung.rating
        let ratingTechnique = self.cosmosViewChuyenMon.rating
        let medicalEthics = self.cosmosViewThaiDo.rating
        let noidung = self.tvNoiDung.text.trim()
        
        let sumRate = (general + ratingTechnique + medicalEthics)/3
        if (general == 0 || ratingTechnique == 0 || medicalEthics == 0 || noidung.isEmpty) {
            let alert = UIAlertController(title: "Thông báo", message: "Bạn vui lòng nhập nhập đầy đủ thông tin đánh giá", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            delegate?.reloadRatingUser(general: general, ratingTechnique: ratingTechnique, medicalEthics: medicalEthics, noidung: noidung, sumRate: sumRate)
        }
    }
    
    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController!, sourceViewController source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController: presented, presenting: presentingViewController)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = tvNoiDung.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        return changedText.count <= 100
    }
}
