//
//  LookupResultVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Alamofire
import Cosmos
import RealmSwift

protocol RatingVC3Delegate {
    func reloadRatingUser(scheduleID:Int, noidung:String, sumRate:Double)
    func goBackProcessManager()
}

class RatingVC3: BaseViewController, UIPopoverPresentationControllerDelegate, UIViewControllerTransitioningDelegate, UITextViewDelegate{
   
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var rating1: CosmosView!
    @IBOutlet weak var rating2: CosmosView!
    @IBOutlet weak var rating3: CosmosView!
    @IBOutlet weak var rating4: CosmosView!
    @IBOutlet weak var tvNoiDung: UITextView!
    var scheduleID:Int = 0
    var listUserComment:[UserRating]? = nil
    var delegate:RatingVC3Delegate?
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        title = "Đánh giá nhân viên"
        self.myView.layer.cornerRadius = 10
        self.myView.layer.borderWidth = 1
        self.myView.layer.masksToBounds = true
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        
        self.tvNoiDung.layer.masksToBounds = true
        self.tvNoiDung.layer.borderWidth = 0.5
        self.tvNoiDung.layer.borderColor = UIColor.lightGray.cgColor
        self.tvNoiDung.layer.cornerRadius = 5
        self.tvNoiDung.delegate = self
        self.tvNoiDung.returnKeyType = UIReturnKeyType.done
//        if listUserComment != nil{
//            self.performRatingAction(listUserComment:listUserComment!)
//        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let frame = CGRect(x: screenSizeWidth/2 - self.myView.frame.width/2, y: screenSizeHeight/2 - self.myView.frame.height/2, width: self.myView.frame.width, height: self.myView.frame.height)
        self.myView.frame = frame
    }
    
    
    // MARK: IBAction
    @IBAction func hideRatingAction(_sender: Any) {
        self.rating1.rating = 0
        self.rating2.rating = 0
        self.rating3.rating = 0
        self.rating4.rating = 0
        self.tvNoiDung.text = ""
        delegate?.goBackProcessManager()
    }
    
    @IBAction func submitRatingAction(_ sender: Any){
        let rt1 = self.rating1.rating
        let rt2 = self.rating2.rating
        let rt3 = self.rating3.rating
        let rt4 = self.rating4.rating
        let noidung = self.tvNoiDung.text.trim()
        
        let sumRate = (rt1 + rt2 + rt3 + rt4)/4
        if (rt1 == 0 || rt2 == 0 || rt3 == 0 || rt4 == 0 || noidung.isEmpty) {
            let alert = UIAlertController(title: "Thông báo", message: "Bạn vui lòng nhập nhập đầy đủ thông tin đánh giá", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            delegate?.reloadRatingUser(scheduleID:scheduleID, noidung: noidung, sumRate: sumRate)
        }
    }
    
    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController!, sourceViewController source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController: presented, presenting: presentingViewController)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = tvNoiDung.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        return changedText.count <= 150
    }
}
