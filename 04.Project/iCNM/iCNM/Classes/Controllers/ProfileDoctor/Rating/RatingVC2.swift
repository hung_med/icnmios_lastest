//
//  LookupResultVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Alamofire
import Cosmos
import RealmSwift

protocol RatingVC2Delegate {
    func reloadRatingUser2(ratingNum:Double, noidung:String)
    func goBackServicePackage()
}

class RatingVC2: BaseViewController, UIPopoverPresentationControllerDelegate, UIViewControllerTransitioningDelegate, UITextViewDelegate{
   
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var cosmosViewRate: CosmosView!
    @IBOutlet weak var tvNoiDung: UITextView!
    var serviceRatings:[ServiceRatingFollowUser]? = nil
    var delegate:RatingVC2Delegate?
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.myView.layer.cornerRadius = 10
        self.myView.layer.borderWidth = 1
        self.myView.layer.masksToBounds = true
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        
        self.tvNoiDung.layer.masksToBounds = true
        self.tvNoiDung.layer.borderWidth = 0.5
        self.tvNoiDung.layer.borderColor = UIColor.lightGray.cgColor
        self.tvNoiDung.layer.cornerRadius = 5
        self.tvNoiDung.delegate = self
        self.tvNoiDung.returnKeyType = UIReturnKeyType.done
        self.performRatingAction(serviceRatings: self.serviceRatings!)
        
    }
    
    func performRatingAction(serviceRatings:[ServiceRatingFollowUser]) {
        if self.currentUser == nil {
            confirmLogin(myView: self)
        } else {
            if currentUser != nil {
                if let user = currentUser{
                    for serviceRating in serviceRatings{
                        if let userInfo = serviceRating.user{
                            if userInfo.userID == user.id{
                                let rating = serviceRating.serviceRating
                                if let rateContent = rating?.serRateContent{
                                    self.tvNoiDung.text! = rateContent
                                }
                                if let serRateNum = rating?.serRateNum{
                                    self.cosmosViewRate.rating = Double(serRateNum)
                                }
                            }else{
                                
                            }
                        }
                    }
                }
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let frame = CGRect(x: screenSizeWidth/2 - self.myView.frame.width/2, y: screenSizeHeight/2 - self.myView.frame.height/2, width: self.myView.frame.width, height: self.myView.frame.height)
        self.myView.frame = frame
    }
    
    
    // MARK: IBAction
    @IBAction func hideRatingAction(_ sender: Any) {
        self.cosmosViewRate.rating = 0
        self.tvNoiDung.text = ""
        delegate?.goBackServicePackage()
    }
    
    @IBAction func submitRatingAction(_ sender: Any){
        let ratingNum = self.cosmosViewRate.rating
        let noidung = self.tvNoiDung.text.trim()
        
        if (noidung.isEmpty || ratingNum == 0) {
            let alert = UIAlertController(title: "Thông báo", message: "Bạn vui lòng nhập nhập đầy đủ thông tin đánh giá", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            delegate?.reloadRatingUser2(ratingNum: ratingNum, noidung: noidung)
        }
    }
    
    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController!, sourceViewController source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController: presented, presenting: presentingViewController)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = tvNoiDung.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        return changedText.count <= 100
    }
}
