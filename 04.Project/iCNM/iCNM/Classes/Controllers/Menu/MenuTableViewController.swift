//
//  MenuTableViewController.swift
//  iCNM
//
//  Created by Medlatec on 5/11/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import GoogleSignIn
import FontAwesome_swift
import Firebase
import FirebaseDatabase

class MenuTableViewController: UITableViewController, PASImageViewDelegate, LookupResultVCDelegate, ListLookUpResultVCDelegate, DetailLookUpResultVCDelegate, SpecialistPopupViewControllerDelegate {
    
    @IBOutlet weak var avatarImageView: PASImageView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    private var specialListSelect = [SpecialistModel]()
    private var specialistSelected = [SpecialistModel]()
    var arrChuyenKhoa = [String]()
    var userDefaults = UserDefaults.standard
    
    let databaseReference = Database.database().reference()
    
    func gobackLookupVC() {
        self.tabBarController?.tabBar.isHidden = true
       // self.btnMenu?.isHidden = true
        self.showListResultVC()
    }
    
    func gobackMainView() {
        self.tabBarController?.tabBar.isHidden = false
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
    }
   
    func dismissResultVC() {
        confirmLogin(myView: self)
        return
    }
    
    func showListLookUpResultbyPID(pid: String, organizeID: String) {
        let listLookUpResultVC = ListLookUpResultVC(nibName: "ListLookUpResultVC", bundle: nil)
        listLookUpResultVC.delegate = self
        listLookUpResultVC.getListLookUpResultByPID(pid: pid, organizeID:organizeID)
        listLookUpResultVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(listLookUpResultVC, animated: true)
    }
    
    func showListLookUpResultBySID(pid: String, sid: String, organizeID: String) {
        if let viewController = UIStoryboard(name: "DetailLookUp", bundle: nil).instantiateViewController(withIdentifier: "DetailLookUpResultVC") as? DetailLookUpResultVC {
            viewController.delegate = self
            viewController.hidesBottomBarWhenPushed = true
            viewController.getListLookUpResultBySID(sid: sid, pID: pid, organizeID: organizeID)
            self.navigationController!.pushViewController(viewController, animated: true)
        }
    }
    
    func showListLookUpResultbyPhone(phone: String) {
        if let viewController = UIStoryboard(name: "DetailLookUp", bundle: nil).instantiateViewController(withIdentifier: "DetailLookUpResultVC") as? DetailLookUpResultVC {
            viewController.delegate = self
            viewController.hidesBottomBarWhenPushed = true
            viewController.getListLookUpResultByPhone(phone: phone)
            self.navigationController!.pushViewController(viewController, animated: true)
        }
    }
    
    func showListLookUpUnit(user: String, maBS: String, currentMonth: String, currentYear: String, organizeID: String) {
        let listLookUpResultVC = ListLookUpResultVC(nibName: "ListLookUpResultVC", bundle: nil)
        listLookUpResultVC.delegate = self
        listLookUpResultVC.hidesBottomBarWhenPushed = true
        listLookUpResultVC.getListLookUpResultUnit(user: user, maBS: maBS, currentMonth: currentMonth, currentYear: currentYear, organizeID: organizeID)
        self.navigationController?.pushViewController(listLookUpResultVC, animated: true)
    }
    
    private var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    private enum MenuItem:String {
        case LineSeperator = "Line Seperator"
        case ServiceMenuHeader = "Dịch vụ - Tiện ích"
        case SearchResult = "Tra cứu kết quả"
        case ScheduleAnAppointment = "Đặt lịch hẹn khám"
        case MakeAQuestion = "Đặt câu hỏi tư vấn"
        case Service = "Gói khám/Dịch vụ"
        case Chat = "Tin nhắn của tôi"
        case AccountMenuHeader = "Tài khoản"
        case ManageQuestion = "Đăng ký trả lời câu hỏi"
        case ManageQuestion_Active = "Quản lý câu hỏi"
        case ManageMedicalLocation = "Quản lý địa điểm y tế"
        case MyHealthProfile = "Hồ sơ sức khoẻ"
        case MyAppointments = "Quản lý lịch hẹn"
        case MyQuestions = "Lịch sử câu hỏi"
        case Logout = "Đăng xuất"
    }
    
    private var menuList:[MenuItem] = [
        MenuItem.ServiceMenuHeader,
        MenuItem.ScheduleAnAppointment,
        MenuItem.MakeAQuestion,
        MenuItem.MyAppointments,
        MenuItem.Chat,
        MenuItem.Logout
    ]
    //FontAwesomeIcon
    
    private func getMenuIcon(menuItem:MenuItem)->String? {
        switch menuItem {
        case .SearchResult:
            return "\u{f022}"
        case .ScheduleAnAppointment:
            return "\u{f271}"
        case .MakeAQuestion:
            return "\u{f29c}"
        case .Service:
            return "\u{f0fe}"
        case .Chat:
            return "\u{f0e5}"
        case .ManageQuestion:
            return "\u{f044}"
        case .ManageQuestion_Active:
            return "\u{f044}"
        case .ManageMedicalLocation:
            return "\u{f0f8}"
        case .MyHealthProfile:
            return "\u{f0f0}"
        case .MyAppointments:
            return "\u{f073}"
        case .MyQuestions:
            return "\u{f059}"
        case .Logout:
            return "\u{f08b}"
        default:
            return nil
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(handleLoginSuccess), name: Constant.NotificationMessage.loginSuccess, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleLogout), name: Constant.NotificationMessage.logout, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleProfileEdited), name: Constant.NotificationMessage.profileEdited, object: nil)
        reloadMenu()
        avatarImageView.delegate = self;
        updateAvatar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // tableView.layoutTableHeaderView()
        updateAvatar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return  menuList.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let indexMenu = indexPath.row
        let menuItem = menuList[indexMenu]
        if menuItem == .ServiceMenuHeader || menuItem == .AccountMenuHeader {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuHeader", for: indexPath) as! MenuHeaderTableViewCell
            cell.headerLabel.text = menuItem.rawValue
            // Configure the cell...
            
            return cell
        }
        if menuItem == .LineSeperator {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LineSeperator", for: indexPath)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuTableViewCell
        if indexPath.row < 6{
            cell.iconLabel.textColor = UIColor(hex:"1976D2")
        }else{
            cell.iconLabel.textColor = UIColor(hex:"9D9D9D")
        }
        
        cell.iconLabel.text = getMenuIcon(menuItem: menuItem)
        cell.titleLabel.text = menuItem.rawValue
        if menuItem == .Logout {
            if currentUser?.loginType == .Facebook {
                cell.iconLabel.textColor = UIColor(hex:"3b5998")
                cell.iconLabel.text = "\u{f082}"
            } else if currentUser?.loginType == .Google {
                cell.iconLabel.textColor = UIColor(hex:"dd4b39")
                cell.iconLabel.text = "\u{f0d4}"
            }
        }
        
        // Configure the cell...
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let indexMenu = indexPath.row
            let menuItem = menuList[indexMenu]
            switch menuItem {
            case .SearchResult:
                self.showListResultVC()
            case .MakeAQuestion:
                if currentUser != nil {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let createAQuestionVC = storyboard.instantiateViewController(withIdentifier: "CreateQuestionVC") as! CreateQuestionViewController
                    self.navigationController?.pushViewController(createAQuestionVC, animated: true)
                }else{
                    confirmLogin(myView: self)
                }
            case .ScheduleAnAppointment:
                if currentUser == nil{
                    confirmLogin(myView: self)
                }
                
                var optionMenu:UIAlertController? = nil
                if IS_IPAD{
                    optionMenu = UIAlertController(title: nil, message: "Tuỳ chọn", preferredStyle: .alert)
                }else{
                    optionMenu = UIAlertController(title: nil, message: "Tuỳ chọn", preferredStyle: .actionSheet)
                }
                
                let action1 = UIAlertAction(title: "Đặt lịch khám ngay", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    let sAppointmentVC = StoryboardScene.Main.scheduleAppointmentVC.instantiate()
                    self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.pushViewController(sAppointmentVC, animated: true)
                })
                let action2 = UIAlertAction(title: "Đặt lịch với Bác sĩ", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    let scheduleAppointmentVC = FormScheduleAppointmentVC(nibName: "FormScheduleAppointmentVC", bundle: nil)
                    self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.pushViewController(scheduleAppointmentVC, animated: true)
                })
                
                let cancelAction = UIAlertAction(title: "Huỷ", style: .cancel, handler: {
                    (alert: UIAlertAction!) -> Void in
                })
                
                let image_doctor =  UIImage.resizeImage(image: UIImage(named: "user-circle")!, toWidth: 20)?.maskWithColor(color: UIColor(hexColor: 0x1976D2, alpha: 1.0))
                let image_normal =  UIImage.resizeImage(image: UIImage(named: "user-md")!, toWidth: 20)?.maskWithColor(color: UIColor(hexColor: 0xFF1E17, alpha: 1.0))
                action2.setValue(image_normal?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
                action1.setValue(image_doctor?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
                
                optionMenu?.addAction(action1)
                optionMenu?.addAction(action2)
                optionMenu?.addAction(cancelAction)
                
                // show action sheet
                optionMenu?.popoverPresentationController?.sourceView = self.view
                self.present(optionMenu!, animated: true, completion: nil)
                
            case .MyAppointments:
                var SATitle:String? = nil
                var SDTitle:String? = nil
                var doctorID:Int = 0
                if let user = self.currentUser{
                    if user.userDoctor?.doctor?.id != 0 && user.userDoctor?.doctor != nil{
                        doctorID = (user.userDoctor?.doctor?.id)!
                        SATitle = "Lịch sử lịch hẹn"
                        SDTitle = "Lịch hẹn khám của tôi"
                        
                        var optionMenu:UIAlertController? = nil
                        if IS_IPAD{
                            optionMenu = UIAlertController(title: nil, message: "Tuỳ chọn", preferredStyle: .alert)
                        }else{
                            optionMenu = UIAlertController(title: nil, message: "Tuỳ chọn", preferredStyle: .actionSheet)
                        }
                        
                        let action1 = UIAlertAction(title: SATitle, style: .default, handler: {
                            (alert: UIAlertAction!) -> Void in
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let scheduleAnAppointmentVC = storyboard.instantiateViewController(withIdentifier: "ViewAppointmentListVC") as! ViewAppointmentListViewController
                            self.tabBarController?.tabBar.isHidden = true
                            self.navigationController?.pushViewController(scheduleAnAppointmentVC, animated: true)
                        })
                        let action2 = UIAlertAction(title: SDTitle, style: .default, handler: {
                            (alert: UIAlertAction!) -> Void in
                            let scheduleDoctorVC = ListScheduleDoctorVC(nibName: "ListScheduleDoctorVC", bundle: nil)
                            scheduleDoctorVC.getListScheduleDoctorOfDay(pageNumber: 1, doctorID: doctorID)
                            scheduleDoctorVC.hidesBottomBarWhenPushed = true
                            self.navigationController?.pushViewController(scheduleDoctorVC, animated: true)
                        })
                        
                        let cancelAction = UIAlertAction(title: "Huỷ", style: .cancel, handler: {
                            (alert: UIAlertAction!) -> Void in
                        })
                        
                        let image_doctor =  UIImage.resizeImage(image: UIImage(named: "ic_history")!, toWidth: 22)?.maskWithColor(color: UIColor(hexColor: 0x1976D2, alpha: 1.0))
                        let image_normal =  UIImage.resizeImage(image: UIImage(named: "icon_calendar")!, toWidth: 20)?.maskWithColor(color: UIColor(hexColor: 0xFF1E17, alpha: 1.0))
                        action2.setValue(image_normal?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
                        action1.setValue(image_doctor?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
                        
                        optionMenu?.addAction(action1)
                        optionMenu?.addAction(action2)
                        optionMenu?.addAction(cancelAction)
                        
                        // show action sheet
                        optionMenu?.popoverPresentationController?.sourceView = self.view
                        self.present(optionMenu!, animated: true, completion: nil)
                    }else{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let scheduleAnAppointmentVC = storyboard.instantiateViewController(withIdentifier: "ViewAppointmentListVC") as! ViewAppointmentListViewController
                        self.tabBarController?.tabBar.isHidden = true
                        self.navigationController?.pushViewController(scheduleAnAppointmentVC, animated: true)
                    }
                }else{
                    confirmLogin(myView: self)
                    return
                }
                
            case .MyHealthProfile:
                if currentUser != nil {
                    if let user = currentUser {
                        let healthRecordsVC = HealthRecordsVC(nibName: "HealthRecordsVC", bundle: nil)
                        self.tabBarController?.tabBar.isHidden = true
                        self.navigationController?.pushViewController(healthRecordsVC, animated: true)
                        healthRecordsVC.getMedicalProfileByUserID(userID:user.id)
                    }
                }else{
                    confirmLogin(myView: self)
                }
            case .Service:
                let servicePackageVC = ServicePackageVC(nibName: "ServicePackageVC", bundle: nil)
                self.tabBarController?.tabBar.isHidden = true
                self.navigationController?.pushViewController(servicePackageVC, animated: true)
            
            case .Chat:
                
                let id = currentUser?.userDoctor?.userInfo?.id
                let userName = currentUser?.userDoctor?.userInfo?.name
                let phone = currentUser?.userDoctor?.userInfo?.phone
                let keySearch = currentUser?.userDoctor?.userInfo?.keySearchName
                
                if let userAvatar = currentUser?.userDoctor?.userInfo?.avatar {
                    databaseReference.child("Users").child("\(id!)").setValue(["UserId": id as Any,"Name": userName as Any,"Avatar": "\(userAvatar)" as Any, "KeySearch" : keySearch as Any, "Phone" : phone as Any])
                } else {
                    databaseReference.child("Users").child("\(id!)").setValue(["UserId": id as Any,"Name": userName as Any,"Avatar": "nil" as Any, "KeySearch" : keySearch as Any, "Phone" : phone as Any])
                }
                
                let storyboard = UIStoryboard(name: "Chat", bundle: nil)
                let mediacalLocation = storyboard.instantiateViewController(withIdentifier: "chathistory_vc") as! ChatHistoryViewController
                self.tabBarController?.tabBar.isHidden = true
                self.navigationController?.pushViewController(mediacalLocation, animated: true)
 
            case .ManageMedicalLocation:
                if let user = currentUser {
                    if !(user.userDoctor?.doctor?.verify)!{
                        showAlertView(title: "Tài khoản Bác sĩ của bạn chưa được iCNM xác thực. Vui lòng chờ hệ thống iCNM xác thực cho bạn hoặc liên hệ trực tiếp. Xin cảm ơn!", view: self)
                        return
                    }else{
                        let storyboard = UIStoryboard(name: "MedicalLocation", bundle: nil)
                        let mediacalLocation = storyboard.instantiateViewController(withIdentifier: "MedicalLocation") as! MedicalLocationViewController
                        self.tabBarController?.tabBar.isHidden = true
                        self.navigationController?.pushViewController(mediacalLocation, animated: true)
                    }
                }
            case .MyQuestions:
                let myQuestionVC = MyQuestionVC(nibName: "MyQuestionVC", bundle: nil)
                self.tabBarController?.tabBar.isHidden = true
                self.navigationController?.pushViewController(myQuestionVC, animated: true)
            case .ManageQuestion:
                if let user = currentUser {
                    if !(user.userDoctor?.doctor?.verify)!{
                        showAlertView(title: "Tài khoản Bác sĩ của bạn chưa được iCNM xác thực. Vui lòng chờ hệ thống iCNM xác thực cho bạn hoặc liên hệ trực tiếp. Xin cảm ơn!", view: self)
                        return
                    }else{
                        self.getSpecialList()
                    }
                }
            case .ManageQuestion_Active:
                if let user = self.currentUser{
                    if (user.userDoctor?.doctor?.answer)!{
                        let questionDoctorVC = ManagerQuestionDoctorVC(nibName: "ManagerQuestionDoctorVC", bundle: nil)
                        self.tabBarController?.tabBar.isHidden = true
                        self.navigationController?.pushViewController(questionDoctorVC, animated: true)
                    }
                }
            case .Logout:
                autoreleasepool {
                    do {
                        let realm = try Realm()
                        try realm.write {
                            realm.delete(realm.objects(User.self))
                            realm.delete(realm.objects(UserDoctor.self))
                            realm.delete(realm.objects(UserInfo.self))
                            realm.delete(realm.objects(WorkExperience.self))
                            realm.delete(realm.objects(DoctorSpecialist.self))
                            realm.delete(realm.objects(DoctorInformation.self))
//                            realm.deleteAll()
                        }
                        NotificationCenter.default.post(name: Constant.NotificationMessage.logout, object: nil)
                    } catch let error {
                        let alertViewController = UIAlertController(title: "Thông báo", message: "Không thể thực hiện đăng xuất:" + error.localizedDescription, preferredStyle: .alert)
                        let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                        alertViewController.addAction(noAction)
                        self.present(alertViewController, animated: true, completion: nil)
                    }
                }
            default:
                break
            }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let indexMenu = indexPath.row
        let menuItem = menuList[indexMenu]
        if menuItem == .LineSeperator {
            return 1.0
        }
        return UITableViewAutomaticDimension
    }
    
    func showListResultVC(){
        if let user = self.currentUser{
            if user.userDoctor?.userInfo?.userName != nil{
                var optionMenu:UIAlertController? = nil
                if IS_IPAD{
                    optionMenu = UIAlertController(title: nil, message: "Chọn loại tài khoản", preferredStyle: .alert)
                }else{
                    optionMenu = UIAlertController(title: nil, message: "Chọn loại tài khoản", preferredStyle: .actionSheet)
                }
                
                let action1 = UIAlertAction(title: "Tra cứu kết quả", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    self.showLookupResultVC()
                })
                let action2 = UIAlertAction(title: "Xem kết quả cho bác sĩ", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    if let user = self.currentUser{
                        let userWeb = user.userDoctor?.userInfo?.userName
                        let passWeb = user.userDoctor?.userInfo?.userPass
                        self.getDoctorIDUnit(userWeb: userWeb!, passWed: passWeb!, organizeID: 5)
                    }
                })
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                    (alert: UIAlertAction!) -> Void in
                })
                
                let image =  UIImage.resizeImage(image: UIImage(named: "user-circle")!, toWidth: 20)?.maskWithColor(color: UIColor(hexColor: 0x1976D2, alpha: 1.0))
                let image2 =  UIImage.resizeImage(image: UIImage(named: "user-md")!, toWidth: 20)?.maskWithColor(color: UIColor(hexColor: 0xFF1E17, alpha: 1.0))
                
                action1.setValue(image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
                action2.setValue(image2?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
                
                optionMenu?.addAction(action1)
                optionMenu?.addAction(action2)
                optionMenu?.addAction(cancelAction)
                
                // show action sheet
                optionMenu?.popoverPresentationController?.sourceView = self.view
                self.present(optionMenu!, animated: true, completion: nil)
            }else{
                
            }
        }
        
        self.showLookupResultVC()
    }
    
    func showLookupResultVC(){
        let lookupResultVC = LookupResultVC(nibName: "LookupResultVC", bundle: nil)
        if let currUser = self.currentUser{
            lookupResultVC.currentUser = currUser
        }
        lookupResultVC.delegate = self
        self.tabBarController?.tabBar.isHidden = true
        lookupResultVC.modalPresentationStyle = .custom
        lookupResultVC.view.frame = CGRect(x: screenSizeWidth/2-(screenSizeWidth-94)/2, y: screenSizeHeight/2-(screenSizeHeight - 336)/2, width: screenSizeWidth-94, height: screenSizeHeight - 336)
        
        self.present(lookupResultVC, animated: true, completion: nil)
    }
   
    func getDoctorIDUnit(userWeb:String, passWed:String, organizeID:Int){
        LoginDoctorUnit().getDoctorIDUnit(userWeb:userWeb, passWeb:passWed, organizeID:organizeID, success: { (data) in
            if data != nil {
                let date = Date()
                let calendar = Calendar.current
                let month = calendar.component(.month, from: date)
                let year = calendar.component(.year, from: date)
                if let maBS = data?.maBS{
                    self.showListLookUpUnit(user: "", maBS: maBS, currentMonth: String(month), currentYear: String(year), organizeID: "5")
                }
            } else {
                
            }
        }, fail: { (error, response) in
            
        })
    }
    
    func getSpecialList() {
        if userDefaults.object(forKey: "registerAnswerQuestion") != nil{
            showAlertView(title: "Tài khoản Bác sĩ của bạn chưa được iCNM xác thực. Vui lòng chờ hệ thống iCNM xác thực cho bạn hoặc liên hệ trực tiếp. Xin cảm ơn!", view: self)
            return
        }else{
            Specialist.getAllSpecialist(success: { (results) in
                if !results.isEmpty {
                    for result in results {
                        self.specialListSelect.append(SpecialistModel(title: result.name!, isSelected: self.arrChuyenKhoa.contains(result.name!), isUserSelectEnable: true, specialist: result))
                    }
                    self.specialistSelected = self.specialListSelect.filter({ (specialistModel) -> Bool in
                        return specialistModel.isSelected
                    })
                    
                    self.showSpecialListSelect()
                    
                } else {
                    
                }
            }, fail: { (error, response) in
                
            })
        }
    }
    
    func showSpecialListSelect() {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let specialistPopup = storyboard.instantiateViewController(withIdentifier: "SpecialistPopup") as! SpecialistPopupViewController
        specialistPopup.modalPresentationStyle = .custom
        specialistPopup.dataArray = self.specialListSelect
        specialistPopup.delegate = self
        self.present(specialistPopup, animated: true, completion: nil)
    }
    
    //Delegate from popup select specialist
    func specialistDidUpdate(specialistPopup: SpecialistPopupViewController) {
            specialistSelected = self.specialListSelect.filter({ (specialistModel) -> Bool in
                return specialistModel.isSelected
            })
            self.arrChuyenKhoa.removeAll()
            for itemSelected in specialistSelected {
                let strID = "\(itemSelected.specialist.id)"
                self.arrChuyenKhoa.append(strID)
            }
            
            let specialistStr = self.arrChuyenKhoa.joined(separator: ",")
            if let user = self.currentUser{
                if self.arrChuyenKhoa.count != 0{
                    self.registerAnswerQuestion(doctorID: user.doctorID, specialistStr: specialistStr)
                }else{
                    showAlertView(title: "Bạn vui lòng chọn chuyên khoa đăng ký nhận trả lời câu hỏi", view: self)
                    return
                }
            }
    }
    
    func registerAnswerQuestion(doctorID:Int, specialistStr:String){
        Doctor().registerAnswer(doctorID:doctorID, specialistStr:specialistStr, success: { (data) in
            if data != nil {
                showAlertView(title: "Bạn đã đăng ký trả lời câu hỏi trên iCNM thành công. Vui lòng chờ hệ thống iCNM liên hệ và xác thực thông tin. Xin cảm ơn!", view: self)
                self.userDefaults.set(true, forKey: "registerAnswerQuestion")
                self.userDefaults.synchronize()
            } else {
                showAlertView(title: "Đăng ký chuyên khoa trả lời câu hỏi thất bại!", view: self)
            }
        }, fail: { (error, response) in
            
        })
    }
    
    @IBAction func touchUserInfoFirstButton(_ sender: UIButton) {
        if currentUser == nil {
            showLoginScreen()
        } else {
            showProfileScreen()
        }
    }
    
    @IBAction func touchUserInfoSecondButton(_ sender: UIButton) {
        if currentUser == nil {
            showLoginScreen()
        } else {
            showProfileScreen()
        }
    }
    
    private func showLoginScreen() {
        //        if let drawerController = self.drawerController {
        let loginNavigationController = StoryboardScene.LoginRegister.loginNavigationController.instantiate()
        //            loginNavigationController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        present(loginNavigationController, animated: true, completion: nil)
        //        }
    }
    
    private func showProfileScreen() {
        //        if let drawerController = self.drawerController {
        let profileNavigationController = StoryboardScene.Profile.profileNavigationController.instantiate()
        //            loginNavigationController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        present(profileNavigationController, animated: true, completion: nil)
        //        }
    }
    
    func handleLoginSuccess(aNotification:Notification) {
        let realm = try! Realm()
        currentUser = realm.currentUser()
        reloadMenu()
        updateAvatar()
        tableView.reloadData()
        
        if currentUser != nil {
            if let user = currentUser {
                let userDefaults = UserDefaults.standard
                let token = userDefaults.object(forKey: "token") as! String
                
                var doctorIDCode = user.userDoctor?.userInfo?.doctorIDCode
                if doctorIDCode == nil{
                    doctorIDCode = ""
                }
                self.addFCM(key: token, userID: user.id, doctorID: user.doctorID, doctorIDCode: doctorIDCode!)
            }
        }
    }
    
    func updateAvatar() {
        if let user = currentUser, let userInfo = currentUser?.userDoctor?.userInfo  {
            if user.userDoctor?.doctor != nil{
                if userInfo.doctorID == currentUser?.doctorID{
                    if currentUser?.userDoctor?.doctor?.verify == true{
                        loginButton.setTitle(userInfo.name, for: .normal)
                        registerButton.setTitle(userInfo.email, for: .normal)
                        if IS_IPHONE_5{
                            registerButton.titleLabel?.font = UIFont.systemFont(ofSize: 13.0)
                        }
                    }else{
                        loginButton.setTitle(userInfo.name, for: .normal)
                        registerButton.setTitle("(BS-Chưa xác thực)", for: .normal)
                    }
                }
            } else {
                loginButton.setTitle(userInfo.name, for: .normal)
                registerButton.setTitle("(Người dùng)", for: .normal)
            }
           
            if let avatarURL = userInfo.getAvatarURL() {
                avatarImageView.imageURL(URL: avatarURL)
            }
        } else {
            loginButton.setTitle("Đăng nhập", for: .normal)
            registerButton.setTitle("hoặc Đăng ký", for: .normal)
            avatarImageView.reset()
        }
    }
    
    func reloadMenu() {
        if currentUser == nil {
            menuList = [
                MenuItem.ServiceMenuHeader,
                MenuItem.SearchResult,
                MenuItem.ScheduleAnAppointment,
                MenuItem.MakeAQuestion,
                MenuItem.Service
//               MenuItem.MyAppointments,
                
            ]
        } else {
            if currentUser?.doctorID == 0{
                menuList = [
                    MenuItem.ServiceMenuHeader,
                    MenuItem.SearchResult,
                    MenuItem.ScheduleAnAppointment,
                    MenuItem.MakeAQuestion,
                    MenuItem.Service,
                    MenuItem.Chat,
                    MenuItem.LineSeperator,
                    MenuItem.AccountMenuHeader,
                    MenuItem.MyHealthProfile,
                    MenuItem.MyAppointments,
                    MenuItem.MyQuestions,
                    MenuItem.LineSeperator,
                    MenuItem.Logout
                ]
            }else{
                if let user = self.currentUser{
                    if (user.userDoctor?.doctor?.answer)!{
                        menuList = [
                            MenuItem.ServiceMenuHeader,
                            MenuItem.SearchResult,
                            MenuItem.ScheduleAnAppointment,
                            MenuItem.MakeAQuestion,
                            MenuItem.Service,
                            MenuItem.Chat,
                            MenuItem.LineSeperator,
                            MenuItem.AccountMenuHeader,
                            MenuItem.ManageQuestion_Active,
                            MenuItem.ManageMedicalLocation,
                            MenuItem.MyHealthProfile,
                            MenuItem.MyAppointments,
                            MenuItem.MyQuestions,
                            MenuItem.LineSeperator,
                            MenuItem.Logout
                        ]
                    }else{
                        menuList = [
                            MenuItem.ServiceMenuHeader,
                            MenuItem.SearchResult,
                            MenuItem.ScheduleAnAppointment,
                            MenuItem.MakeAQuestion,
                            MenuItem.Service,
                            MenuItem.Chat,
                            MenuItem.LineSeperator,
                            MenuItem.AccountMenuHeader,
                            MenuItem.ManageQuestion,
                            MenuItem.ManageMedicalLocation,
                            MenuItem.MyHealthProfile,
                            MenuItem.MyAppointments,
                            MenuItem.MyQuestions,
                            MenuItem.LineSeperator,
                            MenuItem.Logout
                        ]
                    }
                }
            }
        }
    }
    
    func handleLogout(aNotification:Notification) {
        currentUser = nil
        let userDefaults = UserDefaults.standard
        let token = userDefaults.object(forKey: "token") as! String
        self.addFCM(key: token, userID: 0, doctorID: 0, doctorIDCode: "NULL")
        
        if userDefaults.bool(forKey: "agreedTerms") {
            userDefaults.removeObject(forKey: "agreedTerms")
        }
        if GIDSignIn.sharedInstance().hasAuthInKeychain() {
            GIDSignIn.sharedInstance().signOut()
        } else {
            /* code to show your login VC */
        }
        reloadMenu()
        updateAvatar()
        tableView.reloadData()
    }
    
    func addFCM(key:String, userID:Int, doctorID:Int, doctorIDCode:String){
        FCM().addFCM(key: key, userID:userID, doctorID: doctorID, doctorIDCode:doctorIDCode, success: { (data) in
            if data != nil {
                print("register token success")
            } else {
                print("register token failed")
            }
        }, fail: { (error, response) in
            print("register token failed")
        })
    }
    
    func handleProfileEdited(aNotification:Notification) {
        tableView.reloadData()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func PAImageView(didTapped imageView: PASImageView) {
        if currentUser == nil {
            showLoginScreen()
        } else {
            showProfileScreen()
        }
    }
    
}
