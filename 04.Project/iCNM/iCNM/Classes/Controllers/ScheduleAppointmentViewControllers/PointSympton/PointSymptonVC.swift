//
//  PointSymptonVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

protocol PointSymptonVCDelegate {
    func gobackAppointmentListVC(listSelectMedicalSchedule:[TestMedicalSchedule]?, medicalSchedule:[TestMedicalSchedule]?)
}

class PointSymptonVC: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate, UIViewControllerTransitioningDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myTableView2: UITableView!
    var medicalSchedule: [TestMedicalSchedule]?
    var listKeywords: [String]?
    var listMedicalNames: [String: [TestMedicalSchedule]] = [:]
    var newBackButton:UIBarButtonItem? = nil
    var filteredNames:[TestMedicalSchedule]?
//    let searchController = UISearchController(searchResultsController: nil)
//    var searchBarButton:UIBarButtonItem? = nil
    var isSearch:Bool? = false
    @IBOutlet weak var myTableviewHeightConstraint: NSLayoutConstraint!
    var listSelectResult:[TestMedicalSchedule] = [TestMedicalSchedule]()
    var delegate:PointSymptonVCDelegate?
    
    var searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 250, height: 18))
    var searchOn : UIBarButtonItem!
    var searchOff : UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 70
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.sectionIndexBackgroundColor = UIColor.clear
        myTableView2.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        myTableView2.scrollIndicatorInsets = UIEdgeInsetsMake(64, 0, 0, 0)
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        // Setup the Search Controller
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(PointSymptonVC.back(sender:)), for: UIControlEvents.touchUpInside)
            newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }else{
            myTableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
            let button: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            button.setImage(UIImage(named: "angle-left"), for: UIControlState.normal)
            button.addTarget(self, action: #selector(PointSymptonVC.back(sender:)), for: UIControlEvents.touchUpInside)
            newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        // create right bar button item
        searchOn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(isSearchOn))
        searchOff = UIBarButtonItem(title: "Hủy", style: .plain, target: self, action: #selector(isSearchOff))
        
        self.navigationItem.rightBarButtonItems = [self.searchOn]
        
        // create search bar
        self.tabBarController?.tabBar.isHidden = true
        searchBar.placeholder = "Tìm kiếm danh mục chỉ định.."
        searchBar.tintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        searchBar.isHidden = true
        searchBar.delegate = self
        
        myTableviewHeightConstraint.constant = 0
        title = "Danh sách chỉ định"
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        
        if self.medicalSchedule?.count == 0{
            self.getListMedicalSchedule()
        }else{
            self.listKeywords = [String]()
            DispatchQueue.main.async{
                self.medicalSchedule = self.medicalSchedule?.sorted(by: { $0.categoryName < $1.categoryName })
                for i in 0..<Int((self.medicalSchedule?.count)!)
                {
                    let categoryName = self.medicalSchedule?[i].categoryName
                    if !(self.listKeywords!.contains(categoryName!)) {
                        self.listKeywords?.append(categoryName!)
                    }
                }
                
                self.myTableView.reloadData()
                self.myTableView.setEditing(true, animated: false)
            }
        }
        
        if self.listSelectResult.count != 0{
            myTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
            myTableviewHeightConstraint.constant = -200
            self.myTableView2.setEditing(true, animated: false)
            self.myTableView2.reloadData()
        }
    }
    
    func isSearchOn() {
        self.navigationItem.setRightBarButtonItems([self.searchOff], animated: false)
        self.navigationItem.titleView = searchBar
        searchBar.isHidden = false
        myTableView2.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        searchBar.becomeFirstResponder()
    }
    
    func isSearchOff() {
        searchBar.text = nil
        self.navigationItem.setRightBarButtonItems([self.searchOn], animated: false)
        self.navigationItem.titleView = nil
        if searchBar.text == "" {
            isSearch = false
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            isSearch = false
            view.endEditing(true)
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
        else {
            isSearch = true
            filterContentForSearchText(searchText)
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
        self.delegate?.gobackAppointmentListVC(listSelectMedicalSchedule: self.listSelectResult, medicalSchedule: self.medicalSchedule)
    }
    
    func getListMedicalSchedule(){
        let progress = self.showProgress()
        self.medicalSchedule = [TestMedicalSchedule]()
        self.listKeywords = [String]()
        TestMedicalSchedule.getTestMedicalSchedule(success: { (data) in
            if data.count > 0 {
                self.medicalSchedule?.append(contentsOf: data)
                self.medicalSchedule = self.medicalSchedule?.sorted(by: { $0.categoryName < $1.categoryName })
                DispatchQueue.main.async{
                    for i in 0..<Int((self.medicalSchedule?.count)!)
                    {
                        let categoryName = self.medicalSchedule?[i].categoryName
                        if !(self.listKeywords!.contains(categoryName!)) {
                            self.listKeywords?.append(categoryName!)
                        }
                    }
                    self.myTableView.reloadData()
                    self.hideProgress(progress)
                    self.myTableView.setEditing(true, animated: false)
                }
            } else {
                //self?.tableView.es_noticeNoMoreData()
                let lblWarning = createUILabel()
                self.myTableView.addSubview(lblWarning)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if tableView == myTableView{
            return self.listKeywords //Side Section title
        }else{
            return []
        }
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int
    {
        if tableView == myTableView{
            return index
        }else{
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == myTableView{
            if searchBar.text != "" {
                return 1
            }else{
                return self.listKeywords!.count
            }
        }else{
            if self.listSelectResult.count != 0{
                return 1
            }else{
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // code to check for a particular tableView.
        if tableView == myTableView{
            if searchBar.text != "" {
                return self.filteredNames!.count
            }
            listMedicalNames = self.filterKeyword(listKeywords: self.medicalSchedule!)
            return listMedicalNames[self.listKeywords![section]]!.count
        }else{
            return self.listSelectResult.count
        }
    }
    
    func filterKeyword(listKeywords:[TestMedicalSchedule])->[String: [TestMedicalSchedule]]{
        var result = [String:[TestMedicalSchedule]]()
        for string in listKeywords {
            let firstCharacterStr = "\(string.categoryName)"
            if result[firstCharacterStr] == nil {
                result[firstCharacterStr] = [TestMedicalSchedule]()
            }
            result[firstCharacterStr]?.append(string)
        }
        
        return result
    }
    
    // Section Title
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
        var total:Float = 0
        if tableView == myTableView{
            if searchBar.text != "" {
                if let count = self.filteredNames?.count{
                    return "Kết quả tìm kiếm" + " (\(count))"
                }else{
                    return "Kết quả tìm kiếm" + " (0)"
                }
            }else{
                return self.listKeywords?[section]
            }
        }else{
            if self.listSelectResult.count != 0{
                for item in self.listSelectResult{
                    total = total + item.price
                }
                let format = formatString(value: total)
                return "Chỉ định đã chọn:" + " \(self.listSelectResult.count) (\(format))"
            }else{
                return ""
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header : UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.backgroundView?.backgroundColor = UIColor.groupTableViewBackground
        //header.textLabel?.textColor = UIColor.red
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if (cell == nil) {
            cell = UITableViewCell(style:UITableViewCellStyle.subtitle, reuseIdentifier:"Cell")
        }
        if tableView == myTableView{
            let medicalScheduleObj: TestMedicalSchedule?
            if searchBar.text != "" {
                medicalScheduleObj = (self.filteredNames?[indexPath.row])!
                cell?.textLabel?.text = medicalScheduleObj?.testName
                cell?.textLabel?.font = UIFont.fontAwesome(ofSize: 14.0)
                cell?.textLabel?.lineBreakMode = .byWordWrapping
                cell?.textLabel?.numberOfLines = 0
                cell?.textLabel?.adjustsFontSizeToFitWidth = true
                if let cost = medicalScheduleObj?.price{
                    if cost == 0{
                        cell?.detailTextLabel?.text = "0đ"
                    }else{
                        let format = formatString(value: cost)
                        cell?.detailTextLabel?.text = format
                    }
                }
               // cell?.detailTextLabel?.textColor = UIColor.red
            } else {
                if let medicalScheduleObj = self.listMedicalNames[(self.listKeywords?[indexPath.section])!]?[indexPath.row]{
                    cell?.textLabel?.text = medicalScheduleObj.testName
                    cell?.textLabel?.font = UIFont.fontAwesome(ofSize: 14.0)
                    cell?.textLabel?.lineBreakMode = .byWordWrapping
                    cell?.textLabel?.numberOfLines = 0
                    cell?.textLabel?.adjustsFontSizeToFitWidth = true
                    let cost = medicalScheduleObj.price
                    if cost == 0{
                        cell?.detailTextLabel?.text = "0đ"
                    }else{
                        let format = formatString(value: cost)
                        cell?.detailTextLabel?.text = format
                    }
                }
              //  cell?.detailTextLabel?.textColor = UIColor.red
            }
        }else{
            let medicalScheduleObj = self.listSelectResult[indexPath.row]
            cell?.textLabel?.text = medicalScheduleObj.testName
            cell?.textLabel?.font = UIFont.fontAwesome(ofSize: 14.0)
            cell?.textLabel?.lineBreakMode = .byWordWrapping
            cell?.textLabel?.numberOfLines = 0
            cell?.textLabel?.adjustsFontSizeToFitWidth = true
            let cost = medicalScheduleObj.price
            if cost == 0{
                cell?.detailTextLabel?.text = "0đ"
            }else{
                let format = formatString(value: cost)
                cell?.detailTextLabel?.text = format
            }
           // cell?.detailTextLabel?.textColor = UIColor.red
        }
        
        return cell!
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredNames = medicalSchedule?.filter({( medicalScheduleObj : TestMedicalSchedule) -> Bool in
            return medicalScheduleObj.testName.lowercased().contains(searchText.lowercased())
        })
        self.myTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchBar.text != "" {
            isSearch = true
            if let medicalObj = filteredNames?[indexPath.row]{
                if !self.listSelectResult.contains(where: { $0.testCode == medicalObj.testCode}) {
                    self.listSelectResult.append(medicalObj)
                    if let idx = self.filteredNames?.index(where: {$0.testCode == medicalObj.testCode}) {
                        self.filteredNames?.remove(at: idx)
                        self.myTableView.reloadData()
                    }
                }else{
                    showAlertView(title: "Chỉ định này đã chọn", view: self)
                    return
                }
            }

        }else{
            isSearch = false
            if let medicalObj = self.listMedicalNames[(self.listKeywords?[indexPath.section])!]?[indexPath.row]{
                if !self.listSelectResult.contains(where: { $0.testCode == medicalObj.testCode}) {
                    self.listSelectResult.append(medicalObj)
                    if let idx = self.medicalSchedule?.index(where: {$0.testCode == medicalObj.testCode}) {
                       self.medicalSchedule?.remove(at: idx)
                        self.myTableView.reloadData()
                    }
                }
            }
        }
        myTableviewHeightConstraint.constant = -200
        myTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        if #available(iOS 11.0, *) {

        }else{
            self.myTableView2.contentInset = UIEdgeInsetsMake(62, 0, 0, 0)
        }
        self.myTableView2.setEditing(true, animated: false)
        self.myTableView2.reloadData()
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.listSelectResult.count-1, section: 0)
            self.myTableView2.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if searchBar.text != "" {
            if let medicalObj = self.filteredNames?[indexPath.row]{
                if let index = self.filteredNames?.index(where: {$0.testCode == medicalObj.testCode}) {
                    self.filteredNames?.remove(at: index)
                    self.myTableView2.reloadData()
                }
            }
        }else{
            if let medicalObj = self.listMedicalNames[(self.listKeywords?[indexPath.section])!]?[indexPath.row]{
                if let index = self.listSelectResult.index(where: {$0.testCode == medicalObj.testCode}) {
                    self.listSelectResult.remove(at: index)
                    self.myTableView2.reloadData()
                }
            }
        }
        
        if self.listSelectResult.count == 0{
            if #available(iOS 11.0, *) {
                myTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
            }else{
                myTableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
            }
            myTableviewHeightConstraint.constant = 0
        }
    }
    
    private func tableView(tableView: UITableView,
                           editingStyleForRowAtIndexPath indexPath: IndexPath)
        -> UITableViewCellEditingStyle {
            return UITableViewCellEditingStyle.init(rawValue: 3)!
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if tableView == myTableView2{
            
            if editingStyle == .delete{
                if searchBar.text != "" {
                    let medicalObj = self.listSelectResult[indexPath.row]
                    self.filteredNames?.insert(medicalObj, at: 0)
                    
                    self.listSelectResult.remove(at: indexPath.row)
                    if self.listSelectResult.count == 0{
                        myTableviewHeightConstraint.constant = 0
                        if #available(iOS 11.0, *) {
                            myTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
                        }else{
                            myTableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
                        }
                    }
                    self.myTableView2.reloadData()
                    self.myTableView.reloadData()
                    
                }else{
                    let medicalObj = self.listSelectResult[indexPath.row]
                    self.medicalSchedule?.insert(medicalObj, at: 0)
                    
                    self.listSelectResult.remove(at: indexPath.row)
                    if self.listSelectResult.count == 0{
                        myTableviewHeightConstraint.constant = 0
                        if #available(iOS 11.0, *) {
                            myTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
                        }else{
                            myTableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
                        }
                    }
                    self.myTableView2.reloadData()
                    self.myTableView.reloadData()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension//Choose your custom row height
    }
}
