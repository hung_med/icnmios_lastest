//
//  ScheduleAppointmentDoctorVC.swift
//  iCNM
//
//  Created by Mac osx on 11/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import FontAwesome_swift
import Toaster
class ScheduleAppointmentDoctorVC: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, FWDatePickerDelegate, FWComboBoxDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var myView2: UIView!
    @IBOutlet weak var txtNumberResult: UILabel!
    @IBOutlet weak var txtWarningMsg: UILabel!
    @IBOutlet weak var btnCalendar: UIButton!
    @IBOutlet weak var appointmentDatePicker: FWDatePicker?
    @IBOutlet weak var specialistDetailComboBox: FWComboBox!
    var currentOrganize:Organize? = nil
    var currentSpecialist:Specialist? = nil
    var storedOffsets = [Int: CGFloat]()
    var listScheduleDoctor:[ScheduleDoctor]? = nil
    var listScheduleTimeDoctor:[Int: [ScheduleTimeDoctor]]? = nil
    var listScheduleTimeDoctorFilter:[Int: [ScheduleTimeDoctor]]? = nil
    var specialistID:Int = 0
    var organizeID:Int = 0
    var filteredDoctorNames:[ScheduleDoctor] = [ScheduleDoctor]()
    private var currentSpecialistRequest:Request?
    private var specialistNotificationToken:NotificationToken?
    private let specialistResult:Results<Specialist> = {
        let realm = try! Realm()
        return realm.objects(Specialist.self).filter("active == true")
    }()
    
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    let searchController = UISearchController(searchResultsController: nil)
    var searchBarButton:UIBarButtonItem? = nil
    var isSearch:Bool? = false
    var currentRequest:Request?
    
    var isSearching = false
    var searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 250, height: 18))
    var searchOn : UIBarButtonItem!
    var searchOff : UIBarButtonItem!
    var newBackButton:UIBarButtonItem? = nil
    @IBOutlet weak var spaceTopHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        self.tabBarController?.tabBar.isHidden = true
        
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(ScheduleAppointmentDoctorVC.back(sender:)), for: UIControlEvents.touchUpInside)
            newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        if ScreenSize.SCREEN_MAX_LENGTH >= 812{
            spaceTopHeightConstraint.constant = 125
        }else{
            spaceTopHeightConstraint.constant = 110
        }
        title = "ĐẶT LỊCH BÁC SĨ"
        btnCalendar.titleLabel?.font = UIFont.fontAwesome(ofSize: 16.0)
        let titleCalendar = String.fontAwesomeIcon(name: .calendarPlusO)
        btnCalendar.setTitle(titleCalendar, for: .normal)
        myTableView.delegate = self
        myTableView.dataSource = self
        
        myTableView.estimatedRowHeight = 70
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        myView.layer.cornerRadius = 4
        myView.layer.borderWidth = 1.0
        myView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        myView2.layer.cornerRadius = 4
        myView2.layer.borderWidth = 1.0
        myView2.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        
        // create right bar button item
        searchOn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(isSearchOn))
        searchOff = UIBarButtonItem(title: "Hủy", style: .plain, target: self, action: #selector(isSearchOff))
        self.navigationItem.rightBarButtonItems = [self.searchOn]
        
        // create search bar
        searchBar.placeholder = "Tìm kiếm bác sĩ.."
        searchBar.tintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        searchBar.isHidden = true
        searchBar.delegate = self
        self.listScheduleTimeDoctorFilter = [Int: [ScheduleTimeDoctor]]()
        appointmentDatePicker?.delegate = self
        appointmentDatePicker?.minimumDate = Date()
        appointmentDatePicker?.titleFont = UIFont.boldSystemFont(ofSize: 16.0)
        appointmentDatePicker?.textColor = UIColor(hex: "1d6edc")
        appointmentDatePicker?.contentHorizontalAlignment = .right
        
        let nib = UINib(nibName: "SADoctorCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        specialistDetailComboBox.delegate = self
        specialistDetailComboBox.layer.borderWidth = 1
        specialistDetailComboBox.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        specialistDetailComboBox.layer.cornerRadius = 4
        specialistDetailComboBox.layer.masksToBounds = true
        //specialistDetailComboBox.defaultTitle = "Tất cả"
        
        specialistNotificationToken = specialistResult.observe({[weak self] (changes) in
            switch changes {
            case .initial:
                // Results are now populated and can be accessed without blocking the UI
                self?.setupSpecialistDetailComboBox()
                break
            case .update:
                // Query results have changed, so apply them to the UITableView
                self?.setupSpecialistDetailComboBox()
                break
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(error)")
                break
            }
        })
        
        requestData()
    }
    
    deinit {
        specialistNotificationToken?.invalidate()
        currentSpecialistRequest?.cancel()
        NotificationCenter.default.removeObserver(self)
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    private func setupSpecialistDetailComboBox() {
        specialistDetailComboBox.dataSource = specialistResult.map({ (specialist) -> String in
            return specialist.name ?? ""
        })
        if let curSpecialist = currentSpecialist {
            let index = specialistResult.index(where: { (specialist) -> Bool in
                specialist.id == curSpecialist.id
            })
            specialistDetailComboBox.selectRow(at: index)
        }
    }
    
    func fwComboBox(comboBox: FWComboBox, didSelectAtIndex index: Int) {
        if comboBox == specialistDetailComboBox {
            self.isSearchOff()
            self.filteredDoctorNames = [ScheduleDoctor]()
            self.listScheduleTimeDoctorFilter = [Int: [ScheduleTimeDoctor]]()
            currentSpecialist = specialistResult[index]
            if let specialistID = currentSpecialist?.id, let currentDate = appointmentDatePicker?.currentDate{
                let convertToStr = currentDate.toString(withFormat: "yyyy-MM-dd")
                self.getDoctorFollowSpecialist(organizeID: self.organizeID, specialistID: specialistID, currentDate: convertToStr)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if searchBar.text != "" {
            isSearchOn()
        }
    }
    
    func change(_ picker: FWDatePicker, toDate: Date) {
        let currentDate = picker.currentDate
        let convertToStr = currentDate.toString(withFormat: "yyyy-MM-dd")
        self.getDoctorFollowSpecialist(organizeID:self.organizeID, specialistID: self.specialistID, currentDate: convertToStr)
    }
    
    func willShow(_ picker: FWDatePicker) {
        self.view.endEditing(true)
        searchBar.resignFirstResponder()
    }
    
    func getDoctorFollowSpecialist(organizeID:Int, specialistID:Int, currentDate:String){
        self.specialistID = specialistID
        self.organizeID = organizeID
        let progress = self.showProgress()
        self.listScheduleDoctor = [ScheduleDoctor]()
        self.listScheduleTimeDoctor = [Int: [ScheduleTimeDoctor]]()
        ScheduleDoctor().getDoctorFollowSpecialist(organizeID:organizeID, currentDate:currentDate, specialistID: specialistID, success: { (data) in
            if data.count != 0{
                self.txtWarningMsg.isHidden = true
                self.listScheduleDoctor = data
                for i in 0..<Int((self.listScheduleDoctor?.count)!){
                    if let listDoctorTime = self.listScheduleDoctor![i].listLichBS{
                        self.listScheduleTimeDoctor![i] = listDoctorTime
                    }else{
                        self.listScheduleTimeDoctor![i] = [ScheduleTimeDoctor]()
                    }
                }
                if let numberOfDoctor = self.listScheduleDoctor?.count{
                    self.txtNumberResult.text = "Kết quả tìm kiếm (\(numberOfDoctor))"
                }else{
                    self.txtNumberResult.text = "Kết quả tìm kiếm (0)"
                }
                self.hideProgress(progress)
                self.myTableView.reloadData()
            } else {
                self.txtWarningMsg.isHidden = false
                self.txtWarningMsg.text = "Không có dữ liệu"
                self.hideProgress(progress)
                self.myTableView.reloadData()
                self.txtNumberResult.text = "Kết quả tìm kiếm (0)"
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if searchBar.text != "" {
            return self.filteredDoctorNames.count
        }
        return self.listScheduleDoctor!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SADoctorCell
        cell.backgroundColor = .clear
        cell.myView.layer.cornerRadius = 4
        cell.myView.layer.borderWidth = 1
        cell.myView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell.myView.layer.masksToBounds = true
        cell.myView.layer.shadowOffset = CGSize(width: 0, height:5)
        cell.myView.layer.shadowColor = UIColor.groupTableViewBackground.cgColor
        cell.myView.layer.shadowOpacity = 0.5
        cell.myView.layer.shadowRadius = 2
        
        cell.myCollectionView.register(UINib(nibName: "SAColllectionCell", bundle: nil), forCellWithReuseIdentifier: "STCell")
        cell.myCollectionView.viewWithTag(indexPath.row)
        if #available(iOS 10, *) {
            // use an api that requires the minimum version to be 10
        } else {
            cell.myCollectionView.collectionViewLayout.invalidateLayout()
        }
        cell.myCollectionView.showsHorizontalScrollIndicator = false
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapAvatar))
        gestureRecognizer.numberOfTapsRequired = 1
        cell.avatarDoctor.layer.cornerRadius = 4.0
        cell.avatarDoctor.isUserInteractionEnabled = true
        cell.avatarDoctor.tag = indexPath.row
        cell.avatarDoctor.addGestureRecognizer(gestureRecognizer)
        
        
        DispatchQueue.main.async(execute: {
            var scheduleDoctorObj:ScheduleDoctor? = nil
            if self.isSearching {
                scheduleDoctorObj = self.filteredDoctorNames[indexPath.row]
            }else{
                scheduleDoctorObj = self.listScheduleDoctor![indexPath.row]
            }
            if let avatar = scheduleDoctorObj!.avatar{
                let userId = scheduleDoctorObj!.userID
                cell.avatarDoctor.contentMode = .scaleToFill
                let urlString = avatar.trimmingCharacters(in: .whitespaces)
                if avatar != "" || !avatar.isEmpty{
                    let urlString = API.baseURLImage + "\(API.iCNMImage)" + "\(userId)" + "/\(urlString)"
                    cell.avatarDoctor.loadImageUsingUrlString(urlString: urlString)
                }else{
                    cell.avatarDoctor.image = UIImage(named: "avatar_default")
                }
            }else{
                cell.avatarDoctor.image = UIImage(named: "avatar_default")
            }
            
            if let doctorName = scheduleDoctorObj!.name{
                cell.lblName.text = doctorName
                cell.lblName.lineBreakMode = .byWordWrapping
                cell.lblName.numberOfLines = 0
            }
            if let specialistName = scheduleDoctorObj!.specialName{
                if !specialistName.isEmpty{
                    cell.lblSpecialistName.text = specialistName
                }else{
                    cell.lblSpecialistName.text = "Tất cả chuyên khoa"
                }
                cell.lblSpecialistName.lineBreakMode = .byWordWrapping
                cell.lblSpecialistName.numberOfLines = 0
            }
            
            cell.lblScheduleTime.font = UIFont.fontAwesome(ofSize: 15.0)
            if let scheduleDoctorTime = scheduleDoctorObj!.listLichBS{
                if scheduleDoctorTime.count != 0{
                    let iconStr = String.fontAwesomeIcon(name: .arrowCircleDown)
                    cell.lblScheduleTime.text = "Chọn giờ khám  " + iconStr
                    cell.lblScheduleTime.textColor = UIColor.black
                    cell.myCollectionView.isHidden = false
                }else{
                    let iconStr = String.fontAwesomeIcon(name: .timesCircleO)
                    cell.lblScheduleTime.text = "Không có khung giờ khám  " + iconStr
                    cell.lblScheduleTime.textColor = UIColor.red
                    cell.myCollectionView.isHidden = true
                }
            }else{
                let iconStr = String.fontAwesomeIcon(name: .timesCircleO)
                cell.lblScheduleTime.text = "Không có khung giờ khám  " + iconStr
                cell.lblScheduleTime.textColor = UIColor.red
                cell.myCollectionView.isHidden = true
            }
            
            if let rating = scheduleDoctorObj!.rating{
                cell.cosmosStar.rating = rating
            }
            
            let personRate = scheduleDoctorObj!.personRate
            if personRate != 0{
                cell.lblNumberRating.text = "(\(personRate) đánh giá)"
            }else{
                cell.lblNumberRating.text = "(0 đánh giá)"
            }
            
            if let price = scheduleDoctorObj!.price{
                let format = formatString(value: Float(price)) + "/lượt khám"
                cell.lblPrice.text = format
            }
        })
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let healthRecordsObj = self.healthRecords?[indexPath.row]
        //        if let healthRecords = healthRecordsObj{
        //            let detailHealthRecordsVC = DetailHealthRecordsVC(nibName: "DetailHealthRecordsVC", bundle: nil)
        //            if let factoryID = healthRecordsObj?.medicalProfile?.medicalHealthFactoryID{
        //                detailHealthRecordsVC.isEdit = true
        //                detailHealthRecordsVC.delegate = self
        //                detailHealthRecordsVC.healthRecords = healthRecords
        //                detailHealthRecordsVC.getMedicalHealthFactory(id: factoryID)
        //            }
        //            self.navigationController?.pushViewController(detailHealthRecordsVC, animated: true)
        //        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var scheduleDoctorObj:ScheduleDoctor? = nil
        if self.isSearching {
            scheduleDoctorObj = self.filteredDoctorNames[indexPath.row]
        }else{
            scheduleDoctorObj = self.listScheduleDoctor![indexPath.row]
        }
        if let scheduleDoctorTime = scheduleDoctorObj!.listLichBS{
            if scheduleDoctorTime.count != 0{
                return 240
            }else{
                return 184
            }
        }else{
            return 184
        }
    }
    
    func handleTapAvatar(_ sender: UITapGestureRecognizer) {
        let index = sender.view?.tag
        var scheduleDoctorObj:ScheduleDoctor? = nil
        if self.isSearching {
            scheduleDoctorObj = self.filteredDoctorNames[index!]
        }else{
            scheduleDoctorObj = self.listScheduleDoctor![index!]
        }
        let userInfo = UserInfo()
        userInfo.id = scheduleDoctorObj!.userID
        userInfo.doctorID = scheduleDoctorObj!.doctorID
        userInfo.avatar = scheduleDoctorObj!.avatar
        userInfo.name = scheduleDoctorObj!.name
        self.showProfileCurrentUser(user: userInfo)
    }
    
    func showProfileCurrentUser(user:UserInfo){
        let profileDoctorVC = ProfileDoctorVC(nibName: "ProfileDoctorVC", bundle: nil)
        let featuredDoctor = FeaturedDoctor()
        featuredDoctor.userInfo = user
        featuredDoctor.doctor = nil
        featuredDoctor.specialists = nil
        profileDoctorVC.featuredDoctor = featuredDoctor
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(profileDoctorVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? SADoctorCell else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? SADoctorCell else { return }
        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!)
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
        
        if searchController.isActive{
            self.navigationItem.leftBarButtonItem = nil
        }else{
            self.navigationItem.leftBarButtonItem = newBackButton
        }
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredDoctorNames = listScheduleDoctor!.filter({( scheduleDoctor : ScheduleDoctor) -> Bool in
            let doctorName = scheduleDoctor.name!.folding(options: .diacriticInsensitive, locale: .autoupdatingCurrent)
            let keySearch = searchText.folding(options: .diacriticInsensitive, locale: .autoupdatingCurrent)
            return doctorName.lowercased().contains(keySearch.lowercased())
        })
        
        if filteredDoctorNames.count != 0 && !searchText.isEmpty{
            self.txtNumberResult.text = "Kết quả tìm kiếm (\(filteredDoctorNames.count))"
            for i in 0..<Int((self.filteredDoctorNames.count)){
                if let listDoctorTime = self.filteredDoctorNames[i].listLichBS{
                    self.listScheduleTimeDoctorFilter![i] = listDoctorTime
                }else{
                    self.listScheduleTimeDoctorFilter![i] = [ScheduleTimeDoctor]()
                }
            }
            
            self.myTableView.reloadData()
        }else{
            self.filteredDoctorNames = [ScheduleDoctor]()
            self.txtNumberResult.text = "Kết quả tìm kiếm (0)"
            self.myTableView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar:UISearchBar) {
        self.navigationItem.titleView = nil
        self.navigationItem.setRightBarButtonItems([searchBarButton!], animated: true)
        self.navigationItem.leftBarButtonItem = newBackButton
        self.filteredDoctorNames = [ScheduleDoctor]()
        self.myTableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            isSearching = false
            view.endEditing(true)
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
        else {
            isSearching = true
            filterContentForSearchText(searchText)
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
    }
    
    func didPresentSearchController(searchController: UISearchController) {
        searchController.searchBar.becomeFirstResponder()
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchController.searchBar.tintColor = UIColor.gray
        return true
    }
    
    func isSearchOn() {
        self.navigationItem.setRightBarButtonItems([self.searchOff], animated: false)
        self.navigationItem.titleView = searchBar
        searchBar.isHidden = false
        searchBar.becomeFirstResponder()
    }
    
    func isSearchOff() {
        searchBar.text = nil
        self.navigationItem.setRightBarButtonItems([self.searchOn], animated: false)
        self.navigationItem.titleView = nil
        if searchBar.text == "" {
            isSearching = false
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
    }
}

extension ScheduleAppointmentDoctorVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        let tag = collectionView.tag
        var listScheduleTimeDoctorObj:[Int: [ScheduleTimeDoctor]]? = nil
        if self.isSearching {
            listScheduleTimeDoctorObj = self.listScheduleTimeDoctorFilter
        }else{
            listScheduleTimeDoctorObj = self.listScheduleTimeDoctor
        }
        
        if tag >= (listScheduleTimeDoctorObj?.count)!{
            collectionView.collectionViewLayout.invalidateLayout()
            return 0
        }
        if let result = listScheduleTimeDoctorObj![tag]{
            collectionView.collectionViewLayout.invalidateLayout()
            return result.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 110, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.collectionViewLayout.invalidateLayout()
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "STCell", for: indexPath) as! SAColllectionCell
        cell.timeButton.layer.cornerRadius = 4.0
        cell.timeButton.layer.masksToBounds = true
        cell.timeButton.layer.borderColor = UIColor.lightGray.cgColor
        cell.timeButton.layer.borderWidth = 1.0
        let tag = collectionView.tag
        var listScheduleTimeDoctorObj:[Int: [ScheduleTimeDoctor]]? = nil
        if self.isSearching {
            listScheduleTimeDoctorObj = self.listScheduleTimeDoctorFilter
        }else{
            listScheduleTimeDoctorObj = self.listScheduleTimeDoctor
        }
        
        let timeRange = listScheduleTimeDoctorObj![tag]![indexPath.row]
        if let doctorTime = timeRange.doctorTime{
            cell.timeButton.setTitle(doctorTime, for: UIControlState.normal)
            cell.timeButton.tag = tag
            cell.timeButton.accessibilityHint = "\(indexPath.row)"
            cell.timeButton.addTarget(self, action: #selector(ScheduleAppointmentDoctorVC.registerTimeSchedule(sender:)), for: UIControlEvents.touchUpInside)
        }
        
        if let isFull = timeRange.isFull{
            if isFull{
                //cell.timeButton.setTitleColor(UIColor.white, for: UIControlState.normal)
                cell.timeButton.alpha = 0.5
            }else{
                //cell.timeButton.setTitleColor(UIColor(hex:"1976D2"), for: UIControlState.normal)
                cell.timeButton.alpha = 1.0
            }
        }
        
        return cell
    }
    
    @IBAction func registerTimeSchedule(sender:UIButton) {
        let tag = sender.tag
        let index = Int(sender.accessibilityHint!)
        var scheduleDoctorObj:ScheduleDoctor? = nil
        var scheduleTimeObj:ScheduleTimeDoctor? = nil
        if self.isSearching {
            scheduleTimeObj = self.listScheduleTimeDoctorFilter![tag]![index!]
            scheduleDoctorObj = self.filteredDoctorNames[tag]
        }else{
            scheduleTimeObj = self.listScheduleTimeDoctor![tag]![index!]
            scheduleDoctorObj = self.listScheduleDoctor![tag]
        }
        
        if let isFull = scheduleTimeObj!.isFull{
            if isFull{
                Toast(text: "Không còn chỗ trống cho khung giờ này. Xin cảm ơn!").show()
                return
            }
        }
        
        let detailScheduleDoctorVC = DetailScheduleDoctorVC(nibName: "DetailScheduleDoctorVC", bundle: nil)
        if let user = self.currentUser, let currentOrganize = self.currentOrganize, let doctorName = scheduleDoctorObj!.name, let currentDate = appointmentDatePicker?.currentDate, let doctorTime = scheduleTimeObj?.doctorTime{
            detailScheduleDoctorVC.currentUser = user
            detailScheduleDoctorVC.currentOrganize = currentOrganize
            detailScheduleDoctorVC.currentSpecialist = self.currentSpecialist
            detailScheduleDoctorVC.doctorID = scheduleDoctorObj!.doctorID
            detailScheduleDoctorVC.doctorIDCode = scheduleDoctorObj!.doctorIDCode
            detailScheduleDoctorVC.doctorName = doctorName
            detailScheduleDoctorVC.currentDate = currentDate
            detailScheduleDoctorVC.doctorTime = doctorTime
            detailScheduleDoctorVC.price = scheduleDoctorObj!.price!
            detailScheduleDoctorVC.scheduleTimeID = (scheduleTimeObj?.scheduleTimeID)!
            self.navigationController?.pushViewController(detailScheduleDoctorVC, animated: true)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
        searchBar.resignFirstResponder()
    }
}
