//
//  DetailScheduleDoctorVC.swift
//  iCNM
//
//  Created by Mac osx on 8/8/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import Alamofire
import FontAwesome_swift
import Toaster

class DetailScheduleDoctorVC: BaseViewControllerNoSearchBar, FWComboBoxDelegate, UITextFieldDelegate, UITextViewDelegate, UIScrollViewDelegate, FWDatePickerDelegate {

    @IBOutlet weak var myBorderView: UIView!
    @IBOutlet weak var avatarImageView: PASImageView!
    private var currentTextField:UITextField?
    @IBOutlet weak var scrollView: FWCustomScrollView!
    @IBOutlet weak var fullNameDoctorTextField: FWFloatingLabelTextField!
    @IBOutlet weak var organizeTextField: FWFloatingLabelTextField!
    @IBOutlet weak var sheduleTimeTextField: FWFloatingLabelTextField!
    @IBOutlet weak var priceTextField: FWFloatingLabelTextField!
    @IBOutlet weak var specialistTextField: FWFloatingLabelTextField!
    @IBOutlet weak var fullNameTextField: FWFloatingLabelTextField!
    @IBOutlet weak var phoneNumberTextField: FWFloatingLabelTextField!
    @IBOutlet weak var addressTextField: FWFloatingLabelTextField!
//    @IBOutlet weak var birthdayTextField: FWFloatingLabelTextField!
    @IBOutlet weak var birthdayDatePicker: FWDatePicker!
    @IBOutlet weak var genderTextField: FWFloatingLabelTextField!
    @IBOutlet weak var lydoKham: UITextView!
    @IBOutlet weak var scheduleFor: FWComboBox!
    @IBOutlet weak var maleRadioButton: ISRadioButton!
    @IBOutlet weak var femaleRadioButton: ISRadioButton!
    @IBOutlet weak var btnRegisterSchedule: UIButton!
    
    @IBOutlet weak var lblRelation: UILabel!
    @IBOutlet weak var relationComboBox: FWComboBox!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    // HSSK
    var medicalFor = [String]()
    var arrayHealthRecords = [HealthRecords]()
    var healthRecords:HealthRecords?
    var medicalProfileObj:MedicalProfile?
    var medicalHealFactoryObj:MedicalHealthFactory?
    var medicalHealFactory = MedicalHealthFactory()
    
    var medicalVaccin1:[MedicalVaccin]?
    var medicalVaccin2:[MedicalVaccin]?
    var medicalVaccin3:[MedicalVaccin]?
    var medicalProfiles:[MedicalCilinnical]?
    
    var listMedicalFor:[String]?
    var currentMedicalForIndex:Int? = 0
    var bookForOtherPerson : Bool?
    
    var currentBookingDotorRequest:Request?
    var currentUser:User? = nil
    var currentSpecialist:Specialist? = nil
    var currentOrganize:Organize? = nil
    var doctorID:Int = 0
    var doctorIDCode:String? = nil
    var doctorName:String? = nil
    var currentDate:Date? = nil
    var doctorTime:String? = nil
    var scheduleTimeID:Int = 0
    var price:Float = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(DetailScheduleDoctorVC.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        title = "ĐẶT LỊCH BÁC SĨ"
        
        self.lydoKham.layer.cornerRadius = 2.0
        self.lydoKham.layer.masksToBounds = true
        self.lydoKham.layer.borderColor = UIColor.lightGray.cgColor
        self.lydoKham.layer.borderWidth = 0.5
      
        self.avatarImageView.layer.cornerRadius = 30
        self.avatarImageView.layer.masksToBounds = true
        
         UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        let btnButton =  UIButton(type: .custom)
        btnButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 15.0)
        let buttonName = String.fontAwesomeIcon(name: .send)
        btnButton.setTitle(buttonName, for: .normal)
        
        btnButton.addTarget(self, action: #selector(DetailScheduleDoctorVC.touchSendButton), for: .touchUpInside)
        btnButton.frame = CGRect(x: screenSizeWidth-20, y: 0, width: 25, height:25)
        let filterBtnItem = UIBarButtonItem(customView: btnButton)
        self.navigationItem.setRightBarButtonItems([filterBtnItem], animated: true)
        
        if let organizeName = self.currentOrganize?.organizeName{
            self.organizeTextField.text = "\(organizeName)"
        }
        
        if let specialistName = self.currentSpecialist?.name{
            self.specialistTextField.text = "\(specialistName)"
        }else{
            self.specialistTextField.text = "Tất cả"
        }
        
        if let doctorName = self.doctorName{
            self.fullNameDoctorTextField.text = "\(doctorName)"
        }
        
        if let doctorTime = self.doctorTime, let currentDate = self.currentDate{
            let convertToStr = currentDate.toString(withFormat: "dd-MM-yyyy")
            self.sheduleTimeTextField.text = "\(convertToStr) lúc: \(doctorTime)"
        }
    
        self.priceTextField.text = "\(self.price)00 đ"
        self.priceTextField.isUserInteractionEnabled = false
        
        if currentUser == self.currentUser{
            if let userName = currentUser?.userDoctor?.userInfo?.name{
                self.fullNameTextField.text = userName
            }
            
            if let phoneNumber = currentUser?.userDoctor?.userInfo?.phone{
                self.phoneNumberTextField.text = phoneNumber
            }
            
            if let address = currentUser?.userDoctor?.userInfo?.address{
                self.addressTextField.text = address
            }
            
//            let birthday = currentUser?.userDoctor?.userInfo?.birthday
//            let year = birthday?.toString(withFormat: "yyyy")
//            self.birthdayTextField.text = year
           
            if let gender = currentUser?.userDoctor?.userInfo?.gender {
                if gender == "M" {
                    genderTextField.text = "Nam"
                    maleRadioButton.isSelected = true
                } else {
                    genderTextField.text = "Nữ"
                    femaleRadioButton.isSelected = true
                }
            } else {
                genderTextField.text = ""
                maleRadioButton.isSelected = false
                femaleRadioButton.isSelected = false
            }
            self.genderTextField.isUserInteractionEnabled = false
        }
        
        if let avatarURL = currentUser?.userDoctor?.userInfo?.getAvatarURL() {
            avatarImageView.imageURL(URL: avatarURL)
        }
       
        phoneNumberTextField.text = currentUser?.userDoctor?.userInfo?.phone
        fullNameTextField.text = currentUser?.userDoctor?.userInfo?.name
        phoneNumberTextField.addRegx(strRegx: "^[0-9]{10,11}$", errorMsg: "Số điện thoại không đúng định dạng")
        phoneNumberTextField.messageForValidatingLength = "Số điện thoại không được bỏ trống"
        fullNameTextField.messageForValidatingLength = "Tên không được bỏ trống"
        addressTextField.messageForValidatingLength = "Địa chỉ không được bỏ trống"
        genderTextField.messageForValidatingLength = "Bạn chưa chọn giới tính"
//        birthdayTextField.messageForValidatingLength = "Năm sinh không được bỏ trống"
        
        // assign relation for patient when booking
        // show number of hssk in list
        if let id = currentUser?.userDoctor?.userInfo?.id {
            HealthRecords.getMedicalProfileByUserID(userID: id, success: { (data) in
                if data.count > 0 {
                    self.scheduleFor.dataSource.append("Tôi")
                    self.arrayHealthRecords = data
                    for medicalProfile in data {
                        self.scheduleFor.dataSource.append((medicalProfile.medicalProfile?.patientName)!)
                        if let medicalPerson = medicalProfile.userMedicalProfile?.medicalFor {
                            self.medicalFor.append(medicalPerson)
                        }
                    }
                    self.scheduleFor.dataSource.append("Người khác")
                    // check duplicate user
                    self.scheduleFor.dataSource.removeAll { $0 == self.currentUser?.userDoctor?.userInfo?.name }
                    self.scheduleFor.selectRow(at: 0)
                    self.scheduleFor.delegate = self
                    self.scheduleFor.comboTextAlignment = .center
                } else {
                    self.scheduleFor.dataSource = ["Tôi", "Người khác"]
                    self.scheduleFor.selectRow(at: 0)
                    self.scheduleFor.delegate = self
                    self.scheduleFor.comboTextAlignment = .center
                }
            }) { (error, response) in
                print(error)
            }
        }
        
        if let avatarURL = currentUser?.userDoctor?.userInfo?.getAvatarURL() {
            avatarImageView.imageURL(URL: avatarURL)
        }
        
        scheduleFor.layer.cornerRadius = 4.0
        scheduleFor.layer.masksToBounds = true
        scheduleFor.layer.borderColor = UIColor.lightGray.cgColor
        scheduleFor.layer.borderWidth = 0.5
        
        relationComboBox.layer.cornerRadius = 4.0
        relationComboBox.layer.masksToBounds = true
        relationComboBox.layer.borderColor = UIColor.lightGray.cgColor
        relationComboBox.layer.borderWidth = 0.5
        
        // HSSK
        listMedicalFor = ["Tôi", "Chồng", "Vợ", "Bố", "Mẹ", "Con", "Ông", "Bà", "Anh", "Chị", "Em"]
        self.medicalVaccin1 = [MedicalVaccin]()
        self.medicalVaccin2 = [MedicalVaccin]()
        self.medicalVaccin3 = [MedicalVaccin]()
        self.medicalProfiles = [MedicalCilinnical]()
        self.medicalProfileObj = MedicalProfile()
        self.medicalHealFactoryObj = medicalHealFactory
        
        relationComboBox.dataSource = listMedicalFor!
        relationComboBox.defaultTitle = (listMedicalFor?[currentMedicalForIndex!])!
        relationComboBox.selectRow(at: 0)
        relationComboBox.delegate = self
        relationComboBox.comboTextAlignment = .center
        
        bookForOtherPerson = false
        
        // hide combobox
        relationComboBox.isHidden = true
        lblRelation.isHidden = true
        topConstraint.constant = 41
        
        scrollView.delegate = self
        fullNameDoctorTextField.delegate = self
        fullNameDoctorTextField.isUserInteractionEnabled = false
        organizeTextField.delegate = self
        organizeTextField.isUserInteractionEnabled = false
        sheduleTimeTextField.delegate = self
        sheduleTimeTextField.isUserInteractionEnabled = false
        priceTextField.delegate = self
        priceTextField.isUserInteractionEnabled = false
        specialistTextField.delegate = self
        specialistTextField.isUserInteractionEnabled = false
        
        fullNameTextField.delegate = self
        phoneNumberTextField.delegate = self
        addressTextField.delegate = self
//        birthdayTextField.delegate = self
        birthdayDatePicker.delegate = self
        genderTextField.delegate = self
        lydoKham.delegate = self
    }
    
    func fwComboBox(comboBox: FWComboBox, didSelectAtIndex index: Int) {
        if comboBox == scheduleFor {
            if index == 0 {
                // hide combobox
                relationComboBox.isHidden = true
                lblRelation.isHidden = true
                topConstraint.constant = 41
                
                bookForOtherPerson = false
                
                if let avatarURL = currentUser?.userDoctor?.userInfo?.getAvatarURL(){
                    avatarImageView.imageURL(URL: avatarURL)
                }
                femaleRadioButton.isSelected = false
                maleRadioButton.isSelected = false
                phoneNumberTextField.text = currentUser?.userDoctor?.userInfo?.phone
                addressTextField.text = currentUser?.userDoctor?.userInfo?.address
                fullNameTextField.text = currentUser?.userDoctor?.userInfo?.name
                if let birthday = currentUser?.userDoctor?.userInfo?.birthday {
                    birthdayDatePicker.currentDate = birthday
                }
                if let gender = currentUser?.userDoctor?.userInfo?.gender {
                    if gender == "M" {
                        genderTextField.text = "Nam"
                        maleRadioButton.isSelected = true
                        femaleRadioButton.isSelected = false
                    } else {
                        genderTextField.text = "Nữ"
                        femaleRadioButton.isSelected = true
                        maleRadioButton.isSelected = false
                    }
                }
            } else if index == scheduleFor.dataSource.count - 1 {
                // show combobox
                relationComboBox.isHidden = false
                lblRelation.isHidden = false
                topConstraint.constant = 81
                relationComboBox.dataSource = listMedicalFor!
                relationComboBox.dataSource.remove(at: 0)
                relationComboBox.selectRow(at: 0)
                relationComboBox.alpha = 1
                // change to other booking
                bookForOtherPerson = true
                
                avatarImageView.reset()
                femaleRadioButton.isSelected = false
                maleRadioButton.isSelected = false
                phoneNumberTextField.text = ""
                genderTextField.text = ""
                addressTextField.text = ""
                fullNameTextField.text = ""
                birthdayDatePicker.currentDate = Date()
//                birthdayTextField.text = ""
            } else {
                // show combobox
                relationComboBox.isHidden = false
                lblRelation.isHidden = false
                topConstraint.constant = 81
                
                // custom combo box
                relationComboBox.dataSource.removeAll()
                relationComboBox.dataSource = [medicalFor[index]]
                relationComboBox.selectRow(at: 0)
                relationComboBox.reloadInputViews()
                relationComboBox.alpha = 0.5
                relationComboBox.isUserInteractionEnabled = false
                bookForOtherPerson = false
                
                bookForOtherPerson = false
                
                if let imgString = arrayHealthRecords[index].medicalProfile?.imgUrl {
                    if let id = arrayHealthRecords[index].userMedicalProfile?.userID {
                        let urlString = API.baseURLImage + API.iCNMImage + "\(id)/\(imgString)".trim()
                        if let url = URL(string: urlString) {
                            avatarImageView.imageURL(URL: url)
                        }
                    } else {
                        avatarImageView.reset()
                    }
                }
                
                phoneNumberTextField.text = arrayHealthRecords[index].medicalProfile?.mobilePhone
                addressTextField.text = arrayHealthRecords[index].medicalProfile?.address
                fullNameTextField.text = arrayHealthRecords[index].medicalProfile?.patientName
                if let dateString = arrayHealthRecords[index].medicalProfile?.birthDay {
                    let dateFormater = DateFormatter()
                    dateFormater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    birthdayDatePicker.currentDate = dateFormater.date(from: dateString)!
                }
                //maleRadioButton.isUserInteractionEnabled = false
                //femaleRadioButton.isUserInteractionEnabled = false
                if let gender = arrayHealthRecords[index].medicalProfile?.gender {
                    if gender == "M" {
                        genderTextField.text = "Nam"
                        femaleRadioButton.isSelected = false
                        maleRadioButton.isSelected = true
                    } else {
                        genderTextField.text = "Nữ"
                        femaleRadioButton.isSelected = true
                        maleRadioButton.isSelected = false
                    }
                }
            }
        } else if comboBox == relationComboBox {
            currentMedicalForIndex = index
        }
    }
    
    @IBAction func touchGenderRadioButton(_ sender: ISRadioButton) {
        if sender == maleRadioButton {
            genderTextField.text = "Nam"
            maleRadioButton.isSelected = true
            femaleRadioButton.isSelected = false
        } else {
            genderTextField.text = "Nữ"
            maleRadioButton.isSelected = false
            femaleRadioButton.isSelected = true
        }
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTextField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        currentTextField = nil
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        currentTextField = nil
        return true
    }

    
    @IBAction func touchSendButton(_ sender: Any) {
        if fullNameTextField.text?.trim().count == 0{
            fullNameTextField?.becomeFirstResponder()
        }else if phoneNumberTextField.text?.trim().count == 0{
            phoneNumberTextField?.becomeFirstResponder()
        }else if addressTextField.text?.trim().count == 0{
            addressTextField?.becomeFirstResponder()
        }else if genderTextField.text?.trim().count == 0{
            genderTextField?.becomeFirstResponder()
        }
//        else if birthdayTextField.text?.trim().count == 0{
//            birthdayTextField?.becomeFirstResponder()
//        }
        
        self.view.endEditing(true)
        if fullNameTextField.validate() && phoneNumberTextField.validate() && addressTextField.validate() && genderTextField.validate() {
//            if birthdayTextField.text?.count != 4{
//                showAlertView(title: "Năm sinh không đúng định dạng", view: self)
//                return
//            }else{
//                let date = Date()
//                let calendar = Calendar.current
//                let currentYear = Int(calendar.component(.year, from: date))
//
//                if let birthDay = Int(birthdayTextField.text!) {
//                    if birthDay > currentYear{
//                        showAlertView(title: "Năm sinh không lớn hơn năm hiện tại", view: self)
//                        return
//                    }
//                }
//            }
            // check who book
            if self.bookForOtherPerson == true {
                let alert = UIAlertController(title: "Hồ sơ sức khoẻ", message: "Bạn có muốn tạo hồ sơ sức khoẻ cho \(self.fullNameTextField.text!) không", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Đồng ý", style: .default, handler: { (action1) in
                    self.autoGenHSSK()
                    self.sendScheduleAppoint()
                })
                let action2 = UIAlertAction(title: "Không", style: .cancel) { (action2) in
                    self.sendScheduleAppoint()
                }
                alert.addAction(action1)
                alert.addAction(action2)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func sendScheduleAppoint(){
        let schedule = Schedule()
        schedule.userID = currentUser!.id
        schedule.userName = fullNameTextField.text?.trim()
        schedule.organizeID = currentOrganize!.id
        schedule.organizeCode = currentOrganize?.organizeCode
        schedule.typeID = 5
        schedule.timeID = self.scheduleTimeID
        schedule.phone = phoneNumberTextField.text?.trim()
        schedule.address = addressTextField.text?.trim()
        schedule.gender = maleRadioButton.isSelected ? "M":"F"
        schedule.doctorID = self.doctorID
        schedule.doctorIDCode = self.doctorIDCode
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy"
        schedule.birthYear = Int(dateFormater.string(from: birthdayDatePicker.currentDate))!
        schedule.medicalProfileID = medicalProfileObj?.medicalProfileID
//        if let birthYear = birthdayTextField.text?.trim(){
//            schedule.birthYear = Int(birthYear)!
//        }else{
//            schedule.birthYear = 0
//        }
        schedule.dateSchedule = self.currentDate
        schedule.noiKham = currentOrganize?.organizeName
        if let specialistID = self.currentSpecialist?.id{
            schedule.specialistID = specialistID
        }
        
        if let lydoKham = self.lydoKham.text{
            schedule.lydoKham = lydoKham.trim()
        }
        if let doctorTime = self.doctorTime, let currentDate = self.currentDate{
            let convertToStr = currentDate.toString(withFormat: "yyyy-MM-dd")
            schedule.ngayKham = convertToStr
            schedule.gioHen = doctorTime
        }
        
        currentBookingDotorRequest?.cancel()
        let progress = self.showProgress()
        currentBookingDotorRequest = Schedule.bookAnAppointmentDoctor(parameter: schedule.toJSON(), success: { (data) in
            self.hideProgress(progress)
            if data?.general != nil {
                self.hideProgress(progress)
                if let result = data?.general?.mmagen{
                    let title = "Bạn đã đặt lịch bác sĩ thành công với mã đặt lịch là \(result). Bạn có thể vào chức năng 'Lịch sử lịch hẹn' để xem lại thông tin chi tiết. Xin cảm ơn!"
                    let alert = UIAlertController(title: "Thông báo", message: title, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Lịch sử lịch hẹn", style: UIAlertActionStyle.default, handler: { alertAction in
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let scheduleAnAppointmentVC = storyboard.instantiateViewController(withIdentifier: "ViewAppointmentListVC") as! ViewAppointmentListViewController
                        self.tabBarController?.tabBar.isHidden = true
                        self.navigationController?.pushViewController(scheduleAnAppointmentVC, animated: true)
                    }))
                    alert.addAction(UIAlertAction(title: "Đóng", style: UIAlertActionStyle.cancel, handler: { alertAction in
                        self.lydoKham.text = ""
                        self.view.setNeedsLayout()
                        self.view.layoutIfNeeded()
                    }))
                    self.present(alert, animated: true, completion: nil)
                }else{
                    self.hideProgress(progress)
                    if let result = data?.mstringResult{
                        Toast(text: result).show()
                    }
                }
            }else{
                self.hideProgress(progress)
                if let result = data?.mstringResult{
                    Toast(text: result).show()
                }
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func autoGenHSSK() {
        
        medicalProfileObj?.patientName = fullNameTextField.text!
        medicalProfileObj?.gender = maleRadioButton.isSelected ? "M":"F"
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd-MM-yyyy"
        medicalProfileObj?.birthDay = dateFormater.string(from: birthdayDatePicker.currentDate)
        medicalProfileObj?.address = addressTextField.text!
        medicalProfileObj?.mobilePhone = phoneNumberTextField.text!
        
        let medicalHealthFactoryUser = MedicalHealthFactoryUser()
        medicalHealthFactoryUser.medicalProfile = MedicalProfile()
        medicalHealthFactoryUser.medicalHealFactory = self.medicalHealFactoryObj
        medicalHealthFactoryUser.medicalProfile? = self.medicalProfileObj!
        medicalHealthFactoryUser.medicalFor = (listMedicalFor?[currentMedicalForIndex!])!
        medicalHealthFactoryUser.medicalVaccinsTreEm = self.medicalVaccin1!
        medicalHealthFactoryUser.medicalVaccinsTCMR = self.medicalVaccin2!
        medicalHealthFactoryUser.medicalVaccinsUonVan = self.medicalVaccin3!
        medicalHealthFactoryUser.medicalCilinicals = self.medicalProfiles!
        if currentUser != nil {
            if let user = currentUser {
                medicalHealthFactoryUser.userID = user.id
            }
        }
        
        let progress = self.showProgress()
        medicalHealthFactoryUser.addEditMedicalHealthFactory(success: {(result) in
            if result != nil {
                self.hideProgress(progress)
                return
            } else {
                //                showAlertView(title: "Không thể tạo mới hồ sơ sức khoẻ!", view: self)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == birthdayTextField{
//            guard let text = textField.text else { return true }
//            let newLength = text.count + string.count - range.length
//            return newLength <= 4
//        }else
        if textField == fullNameTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == addressTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 100
        }else if textField == phoneNumberTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 12
        }else if textField == fullNameDoctorTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }
//        if textField == birthdayTextField{
//            guard let text = textField.text else { return true }
//            let newLength = text.count + string.count - range.length
//            return newLength <= 4
//        }else
        if textField == genderTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else{
            return true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = lydoKham.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        return changedText.count <= 250
    }
    
    //Keyboard handle
    @objc private func keyboardWillShow(aNotification:Notification) {
        if let curTextField = currentTextField {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            scrollView.contentInset = contentInset
            scrollView.scrollIndicatorInsets = contentInset
            let rect = curTextField.frame
            let test = view.bounds.height - (rect.size.height + rect.origin.y + scrollView.contentInset.top) - 60.0
            let different = keyboardFrame.size.height - test
            if different > 0 {
                scrollView.contentOffset.y = different - self.scrollView.contentInset.top
            }
        }
    }
    
    @objc private func keyboardWillHide(aNotification:Notification) {
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = 0
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5, animations: {
            self.btnRegisterSchedule.alpha = 1
        }, completion: {
            finished in
            self.btnRegisterSchedule.isHidden = false
        })
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5, animations: {
            self.btnRegisterSchedule.alpha = 0
        }, completion: {
            finished in
            self.btnRegisterSchedule.isHidden = true
        })
    }
}
