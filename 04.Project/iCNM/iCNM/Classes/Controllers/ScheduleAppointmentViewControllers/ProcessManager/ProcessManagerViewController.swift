//
//  ProcessManagerViewController.swift
//  iCNM
//
//  Created by Thanh Huyen on 8/21/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import Toaster
import FontAwesome_swift

class ProcessManagerViewController: BaseViewControllerNoSearchBar, UITableViewDelegate, UITableViewDataSource, UIViewControllerTransitioningDelegate, RatingVC3Delegate {
    
    @IBOutlet weak var tableView: UITableView!
    var processManager:ProcessManager? = nil
    let arrayImages = ["xacnhan.jpg","nguoilaymau.jpg","ketqua.jpg","support.jpg","bacsi.jpg"]
    let arrayTitle = ["Lịch hẹn khám","Khám tại nhà/Lấy mẫu","Kết quả khám","Tư vấn kết quả","Lịch hẹn tái khám"]
    var arrayContent:[String]? = nil
    var arrayOriginStatus:[String]? = nil
    var scheduleID:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.arrayContent = [String]()
        self.arrayOriginStatus = [String]()
    }
    
    func getDataProcessManager(scheduleID:Int){
        let progress = self.showProgress()
        self.scheduleID = scheduleID
        ProcessManager().getAllProcessManager(scheduleID:scheduleID, success: { (data) in
            self.hideProgress(progress)
            self.processManager = data
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            if let process = self.processManager?.process{
                if let ngayDatLich = process.ngayDatLich, let khungGioDL = process.khungGio{
                    let ngayDatLichStr = dateFormatter.string(from: ngayDatLich)
                    if let ngayXacnhan = process.ngayXacNhan, let kgXacnhan = process.khungGioXacNhan, let tgXacNhan = process.tGXacNhan{
                        let ngayXacnhanStr = dateFormatter.string(from: ngayXacnhan)
                        let kgXacnhanStr = dateFormatter.string(from: tgXacNhan)
                        if !ngayXacnhanStr.isEmpty && !kgXacnhanStr.isEmpty && !kgXacnhan.isEmpty{
                            self.arrayContent?.append("\(Constant.DEFIND_ORGANIZE)\nThời gian khám: \(ngayXacnhanStr) \nKhung giờ khám: \(kgXacnhan)")
                            self.arrayOriginStatus?.append("Xác nhận lịch hẹn")
                        }else{
                            self.arrayContent?.append("\(Constant.DEFIND_ORGANIZE)\nThời gian khám: \(ngayDatLichStr) \nKhung giờ khám: \(khungGioDL)")
                            self.arrayOriginStatus?.append("Đang chờ xác nhận")
                        }
                    }else{
                        self.arrayContent?.append("\(Constant.DEFIND_ORGANIZE)\nThời gian khám: \(ngayDatLichStr) \nKhung giờ khám: \(khungGioDL)")
                        self.arrayOriginStatus?.append("Đang chờ xác nhận")
                    }
                }
                
                if let userLab = self.processManager?.userLab{
                    if let userID = userLab.userID, let userName = userLab.userName, let phone = userLab.phone{
                        if phone.trim().isEmpty{
                            self.arrayContent?.append("Mã nhân viên: \(userID)\nNhân viên: \(userName)\nSố điện thoại:\n\(Constant.DEFIND_ORGANIZE)")
                        }else{
                            self.arrayContent?.append("Mã nhân viên: \(userID)\nNhân viên: \(userName)\nSố điện thoại: \(phone)\n\(Constant.DEFIND_ORGANIZE)")
                        }
                    }else{
                        self.arrayContent?.append("Nhân viên: Chưa cập nhật\nSố điện thoại: Chưa cập nhật\n")
                    }
                    if let SID = process.sID{
                        if !SID.isEmpty{
                            self.arrayOriginStatus?.append("Hoàn thành lấy mẫu")
                        }else{
                            self.arrayOriginStatus?.append("Chờ lấy mẫu")
                        }
                    }else{
                        self.arrayOriginStatus?.append("Chờ lấy mẫu")
                    }
                    
                }else{
                    self.arrayContent?.append("")
                    self.arrayOriginStatus?.append("")
                }
                
                if let SID = process.sID{
                    if let fullResult = process.fullResult{
                        if fullResult{
                            self.arrayContent?.append("Mã tra cứu: \(SID)")
                            self.arrayOriginStatus?.append("Đã đủ kết quả")
                        }
                    }else{
                        if let tGNhanMau = process.tGNhanMau{
                            print(tGNhanMau)
                            self.arrayContent?.append("Mã tra cứu:")
                            self.arrayOriginStatus?.append("Đã nhận đủ mẫu - Đang chạy XN")
                        }else{
                            self.arrayContent?.append("Mã tra cứu:")
                            self.arrayOriginStatus?.append("Đang chờ nhận mẫu")
                        }
                    }
                }else{
                    self.arrayContent?.append("")
                    self.arrayOriginStatus?.append("")
                }
                
                if let tuVan = process.tuVan{
                    if tuVan{
                        self.arrayContent?.append(Constant.ADVISORY)
                        self.arrayOriginStatus?.append("")
                    }
                }else{
                    if let SID = process.sID{
                        if !SID.isEmpty{
                            self.arrayContent?.append(Constant.ADVISORY)
                            self.arrayOriginStatus?.append("")
                        }
                    }else{
                        self.arrayContent?.append("")
                        self.arrayOriginStatus?.append("")
                    }
                }
                
                if let taiKham = process.tGHenTaiKham{
                    if !taiKham.timeAgoSinceNow().isEmpty{
                        let ngayhenTK = dateFormatter.string(from: taiKham)
                        self.arrayContent?.append(Constant.RE_EXAMINATION + " \(ngayhenTK) " + Constant.RE_EXAMINATION_SUB)
                        self.arrayOriginStatus?.append("Đã hẹn tái khám")
                    }else{
                        self.arrayContent?.append(Constant.RE_NO_EXAMINATION)
                        self.arrayOriginStatus?.append("Chưa có hẹn tái khám")
                    }
                }else{
                    self.arrayContent?.append("")
                    self.arrayOriginStatus?.append("")
                }
            }
            
            self.tableView.reloadData()
            
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayContent!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProcessCell") as! ProcessTableViewCell
        
        cell.imgProcess.image = UIImage(named: arrayImages[indexPath.row])
        cell.lblTitle.text = arrayTitle[indexPath.row]
        if let process_Content = arrayContent?[indexPath.row]{
            if process_Content.isEmpty{
                cell.coverView.isHidden = true
            }else{
                cell.coverView.isHidden = false
            }
            cell.lblContent.text = process_Content
        }
        if let process_Status = arrayOriginStatus?[indexPath.row]{
            cell.lblStatus.text = process_Status
        }
        
        switch indexPath.row {
        case 1:
            cell.btnDone.isHidden = true
            if let result = arrayContent?[indexPath.row]{
                let myAttributedString = setTextAttributedString(result: result)
                cell.lblContent.attributedText = myAttributedString
            }
            if (arrayOriginStatus?[indexPath.row].contains("Hoàn thành lấy mẫu"))!{
                if let sumRate = self.processManager?.process?.diemDanhGia{
                    if sumRate != 0{
                        cell.btnDone.isHidden = true
                        cell.sumRating.isHidden = false
                        cell.sumRating.isUserInteractionEnabled = false
                        cell.sumRating.rating = Double(sumRate)
                    }else{
                        cell.sumRating.isHidden = true
                        cell.btnDone.isHidden = false
                        cell.btnDone.setTitle("Đánh giá", for: UIControlState.normal)
                        cell.btnDone.tag = indexPath.row
                        cell.btnDone.addTarget(self, action: #selector(ProcessManagerViewController.handleRatingEmployee(_:)), for: UIControlEvents.touchUpInside)
                    }
                }else{
                    cell.sumRating.isHidden = true
                    cell.btnDone.isHidden = false
                    cell.btnDone.setTitle("Đánh giá", for: UIControlState.normal)
                    cell.btnDone.tag = indexPath.row
                    cell.btnDone.addTarget(self, action: #selector(ProcessManagerViewController.handleRatingEmployee(_:)), for: UIControlEvents.touchUpInside)
                }
            }else{
                cell.btnDone.isHidden = true
            }
        case 0,2,3:
            if indexPath.row == 2 || indexPath.row == 3{
                if let result = arrayContent?[indexPath.row]{
                    let myAttributedString = setTextAttributedString(result: result)
                    cell.lblContent.attributedText = myAttributedString
                }
            }
            cell.btnDone.isHidden = true
        case 4:
            if let result = arrayContent?[indexPath.row]{
                let myAttributedString = setTextAttributedString(result: result)
                cell.lblContent.attributedText = myAttributedString
            }
            cell.btnDone.isHidden = true
            cell.lineView.isHidden = true
        default:
            cell.btnDone.isHidden = false
        }
        
        return cell
    }
    
    @IBAction func handleRatingEmployee(_ sender: Any){
        //Rating
        let ratingVC = RatingVC3(nibName: "RatingVC3", bundle: nil)
        ratingVC.delegate = self
        ratingVC.scheduleID = scheduleID
        ratingVC.modalPresentationStyle = .custom
        ratingVC.transitioningDelegate = self
        self.present(ratingVC, animated: true, completion: nil)
    }
    
    func reloadRatingUser(scheduleID: Int, noidung: String, sumRate: Double) {
        let progress = self.showProgress()
        ProcessManager.customRatingEmployee(scheduleID:scheduleID, sumRate: Float(sumRate), noiDung: noidung, success: { (data) in
            Toast(text: "Đánh giá thành công").show()
            self.arrayContent = [String]()
            self.arrayOriginStatus = [String]()
            self.processManager = nil
            self.getDataProcessManager(scheduleID: scheduleID)
            self.hideProgress(progress)
        }, fail: { (error, response) in
            
        })
        self.goBackProcessManager()
    }
    
    func goBackProcessManager()
    {
        self .dismiss(animated: true) {
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1{
            if let userLab = self.processManager?.userLab{
                if let phone = userLab.phone{
                    if !phone.trim().isEmpty{
                        phone.trim().makeCallPhone()
                    }
                }
            }
        }
        else if indexPath.row == 2{
            if let process = self.processManager?.process{
                if let sID = process.sID?.trim(){
                    if let viewController = UIStoryboard(name: "DetailLookUp", bundle: nil).instantiateViewController(withIdentifier: "DetailLookUpResultVC") as? DetailLookUpResultVC {
                        self.tabBarController?.tabBar.isHidden = true
                        viewController.title = sID
                        viewController.getListLookUpResultBySID(sid: sID, pID: "", organizeID:"1")
                        self.navigationController!.pushViewController(viewController, animated: true)
                    }
                }else{
                    showAlertView(title: "Bạn chưa có kết quả. Hệ thống đang trong quá trình xử lý mẫu!", view: self)
                }
            }else{
                showAlertView(title: "Bạn chưa có kết quả. Hệ thống đang trong quá trình xử lý mẫu!", view: self)
            }
        }else if indexPath.row == 3{
            Constant.HOTLINE.makeCallPhone()
        }
        else if indexPath.row == 4{
            if let taiKham = self.processManager?.process?.tGHenTaiKham{
                if !taiKham.timeAgoSinceNow().isEmpty{
                    let createQuestionVC = StoryboardScene.Main.scheduleAppointmentVC.instantiate()
                    self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.pushViewController(createQuestionVC, animated: true)
                }else{
                    showAlertView(title: Constant.RE_NO_EXAMINATION, view: self)
                }
            }else{
                showAlertView(title: Constant.RE_NO_EXAMINATION, view: self)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let result_content = arrayContent?[indexPath.row]
        if !(result_content?.isEmpty)!{
            return UITableViewAutomaticDimension
        }else{
            return 120
        }
    }
}

