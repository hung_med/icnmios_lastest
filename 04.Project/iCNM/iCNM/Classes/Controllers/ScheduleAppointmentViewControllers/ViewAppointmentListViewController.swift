//
//  ViewAppointmentListViewController.swift
//  iCNM
//
//  Created by Medlatec on 8/3/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import FontAwesome_swift
import Toaster

class ViewAppointmentListViewController: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var typeSegment: UISegmentedControl!
    @IBOutlet weak var appointmentTableView: UITableView!
    @IBOutlet weak var addAppointmentButton: UIBarButtonItem!
    @IBOutlet weak var noDataView: UIView!
    private var currentStatus = 1
    
    private var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    private var currentRequest:Request?
    private var currentUserNotificationToken:NotificationToken?
    private var scheduleDataArray:[Schedule]? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        noDataView.isHidden = true
       
        appointmentTableView.estimatedRowHeight = 100
        appointmentTableView.rowHeight = UITableViewAutomaticDimension
        appointmentTableView.tableFooterView = UIView()
        let fontAwesomeFontAdd = FontFamily.FontAwesome.regular.font(size: 17.0)
        addAppointmentButton.setTitleTextAttributes([NSFontAttributeName:fontAwesomeFontAdd!], for: .normal)
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        // Setup the Search Controller
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(ViewAppointmentListViewController.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        if currentUser == nil {
            self.navigationController?.dismiss(animated: true, completion: nil)
            return
        }
        
        if let nvc = self.navigationController {
            if nvc.viewControllers.count == 1 {
                self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Đóng", style: UIBarButtonItemStyle.plain, target: self, action: #selector(touchCloseButton(_:)));
            }
        }
        
        currentUserNotificationToken = currentUser?.observe({[weak self] (change) in
            switch change {
            case .change:
                print("Thay doi")
            case .error(let error):
                print("Co loi \(error)")
            case .deleted:
                print("bi xoa")
                self?.navigationController?.dismiss(animated: true, completion: nil)
            }
        })
        appointmentTableView.dataSource = self
        appointmentTableView.delegate = self
        requestData()

        // Do any additional setup after loading the view.
    }

    override func requestData() {
        currentRequest?.cancel()
        let progress = self.showProgress()
        self.scheduleDataArray = [Schedule]()
        if let userID = currentUser?.id {
            let stringRequest = "\(currentStatus),\(userID)"
            currentRequest = Schedule.getSchedule(stringRequest: stringRequest, success: { (scheduleArray) in
                for i in 0..<Int((scheduleArray.count)){
                    let schedule = scheduleArray[i] as Schedule
                    if schedule.ngayHuyLich == nil{
                        self.scheduleDataArray?.append(schedule)
                    }
                }
                if self.scheduleDataArray!.count == 0 {
                    self.noDataView.isHidden = false
                } else {
                    self.noDataView.isHidden = true
                }
                self.appointmentTableView.reloadData()
                self.hideProgress(progress)
            }) { (error, response) in
                self.hideProgress(progress)
                self.handleNetworkError(error: error, responseData: response.data)
            }
        }
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deleteActionSchedule(sender: AnyObject) {
        self.appointmentTableView.setEditing(true, animated: false)
    }
    
    private func tableView(tableView: UITableView,
                           editingStyleForRowAtIndexPath indexPath: IndexPath)
        -> UITableViewCellEditingStyle {
            return UITableViewCellEditingStyle.init(rawValue: 3)!
    }
    
    private func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            var optionMenu:UIAlertController? = nil
            if IS_IPAD{
                optionMenu = UIAlertController(title: "Thông báo", message: "Bạn có chắc chắn muốn huỷ lịch này không?", preferredStyle: .alert)
            }else{
                optionMenu = UIAlertController(title: "Thông báo", message: "Bạn có chắc chắn muốn huỷ lịch này không?", preferredStyle: .alert)
            }
            optionMenu?.addAction(UIAlertAction(title: "Đồng ý", style: .default, handler: { _ in
                let scheduleID = self.scheduleDataArray![indexPath.row].id
                self.currentRequest?.cancel()
                let progress = self.showProgress()
                self.currentRequest = Schedule.deleteSchedule(scheduleID: scheduleID, success: { (result) in
                    self.hideProgress(progress)
                    if result == "ok"{
                        self.scheduleDataArray?.remove(at: indexPath.row)
                        self.appointmentTableView.reloadData()
                        Toast(text: "Bạn đã huỷ lịch thành công!").show()
                    }else{
                        Toast(text: "Huỷ lịch không thành công!").show()
                    }
                }) { (error, response) in
                    self.hideProgress(progress)
                    self.handleNetworkError(error: error, responseData: response.data)
                }
            }))
            optionMenu?.addAction(UIAlertAction(title: "Thoát", style: UIAlertActionStyle.cancel, handler: {(action:UIAlertAction!) in
                
            }))
            
            optionMenu?.popoverPresentationController?.sourceView = self.view
            self.present(optionMenu!, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return scheduleDataArray!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = appointmentTableView.dequeueReusableCell(withIdentifier: "AppointmentCell", for: indexPath) as! AppointmentTableViewCell
        let schedule = scheduleDataArray![indexPath.row]
        cell.backgroundColor = .clear
        cell.myGroupView.layer.cornerRadius = 4
        cell.myGroupView.layer.borderWidth = 1
        cell.myGroupView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell.myGroupView.layer.masksToBounds = true
        cell.myGroupView.layer.shadowOffset = CGSize(width: 0, height:2)
        cell.myGroupView.layer.shadowColor = UIColor.groupTableViewBackground.cgColor
        cell.myGroupView.layer.shadowOpacity = 0.23
        cell.myGroupView.layer.shadowRadius = 2
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        var dateString = ""
        if schedule.typeID == 1 || schedule.typeID == 2{
            if schedule.typeID == 1{
                cell.iconRight.isHidden = false
            }else{
                cell.iconRight.isHidden = true
            }
            cell.spaceV1Constraint.constant = 12
            if let scheduleDate = schedule.dateSchedule {
                dateString = dateFormatter.string(from:scheduleDate)
                dateString = convertToDays(weekDay: dateString)
                dateFormatter.dateFormat = "dd/MM/yyyy"
                dateString += "\n"
                if let timeString = schedule.scheduleTime?.timeString {
                    dateString += timeString
                    dateString += "\n"
                }
                dateString += dateFormatter.string(from:scheduleDate)
            }
            var typeString = ""
            if let organizeName = schedule.organize?.organizeName, let userName =  schedule.userName{
                cell.unitName.text = userName + "\n\(organizeName)"
                cell.unitName.lineBreakMode = .byWordWrapping
                cell.unitName.numberOfLines = 0
            }else{
                cell.unitName.text = "Chưa cập nhật dữ liệu"
            }
            if let scheduleTypeName = schedule.scheduleType?.name {
                typeString += "\n\(schedule.phone ?? "")"
                typeString += "\n\(scheduleTypeName)"
                cell.typeLabel.text = typeString
            }
            cell.dateLabel.text = dateString
            cell.btnCodeGen.isHidden = true
            cell.btnCodeGen.setTitle("", for: .normal)
            
        }else{
            cell.iconRight.isHidden = true
            cell.spaceV1Constraint.constant = 18
            if let scheduleDate = schedule.dateSchedule {
                dateString = dateFormatter.string(from:scheduleDate)
                dateString = convertToDays(weekDay: dateString)
                dateFormatter.dateFormat = "dd/MM/yyyy"
                dateString += "\n"
                if let timeString = schedule.gioHen {
                    dateString += timeString
                    dateString += "\n"
                }
                dateString += dateFormatter.string(from:scheduleDate)
                cell.dateLabel.text = dateString
            }
            var typeString = ""
            if let organizeName = schedule.organize?.organizeName, let userName =  schedule.userName {
                cell.unitName.text = userName + "\n\(organizeName)"
                cell.unitName.lineBreakMode = .byWordWrapping
                cell.unitName.numberOfLines = 0
            }else{
                cell.unitName.text = "Chưa cập nhật dữ liệu"
            }
            if let scheduleTypeName = schedule.scheduleType?.name {
                typeString += "\n\(schedule.phone ?? "")"
                typeString += "\n\(scheduleTypeName)"
                cell.typeLabel.text = typeString
                cell.typeLabel.halfTextColorChange(fullText: typeString, changeText: scheduleTypeName)
            }
            
            if let maGen = schedule.maGen {
                cell.btnCodeGen.isHidden = false
                cell.btnCodeGen.layer.cornerRadius = 4.0
                cell.btnCodeGen.layer.masksToBounds = true
                cell.btnCodeGen.backgroundColor = UIColor(hex:"1976D2")
                cell.btnCodeGen.titleLabel?.font = UIFont.fontAwesome(ofSize: 18.0)
                let iconStr = " " + maGen + " " + String.fontAwesomeIcon(name: .questionCircle) + " "
                cell.btnCodeGen.setTitle(iconStr, for: .normal)
                cell.btnCodeGen.tag = indexPath.row
                cell.btnCodeGen.addTarget(self, action:#selector(handleTapCodeGen), for: .touchUpInside)
            }else{
                cell.btnCodeGen.isHidden = true
                cell.btnCodeGen.setTitle("", for: .normal)
            }
        }
        
        if let maGG = schedule.maGG{
            //cell.btnGift.isHidden = false
            cell.btnGift.titleLabel?.font = UIFont.fontAwesome(ofSize: 13)
            let title = String.fontAwesomeIcon(name: .gift)
            cell.btnGift.setTitle(title, for: .normal)
            cell.btnGift.accessibilityHint = maGG
            cell.btnGift.addTarget(self, action:#selector(handleGift), for: .touchUpInside)
        }else{
            //cell.btnGift.isHidden = true
        }
        if IS_IPHONE_5{
            cell.unitName.font = UIFont.systemFont(ofSize: 13.0)
            cell.dateLabel.font = UIFont.systemFont(ofSize: 13.0)
            cell.typeLabel.font = UIFont.fontAwesome(ofSize: 13.0)
            cell.btnGift.titleLabel?.font = UIFont.fontAwesome(ofSize: 13.0)
            cell.btnCodeGen.titleLabel?.font = UIFont.fontAwesome(ofSize: 16.0)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let scheduleObj = self.scheduleDataArray![indexPath.row]
        let scheduleType = scheduleObj.typeID
        if scheduleType == 1{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let processManagerVC = storyboard.instantiateViewController(withIdentifier: "ProcessManagerVC") as! ProcessManagerViewController
            self.tabBarController?.tabBar.isHidden = true
                processManagerVC.getDataProcessManager(scheduleID: scheduleObj.id)
            self.navigationController?.pushViewController(processManagerVC, animated: true)
        }
    }
    
    func handleGift(sender: UIButton){
        let btn = sender as UIButton
        if let maGG = btn.accessibilityHint{
            let title = "Mã giảm giá của bạn là: \(maGG)"
            showAlertView(title: title, view: self)
        }
    }
    
    func handleTapCodeGen(sender: UIButton){
        let tag = sender.tag
        let schedule = scheduleDataArray![tag]
        if let maGen = schedule.maGen{
            let title = "Mã lịch hẹn của bạn là: \(maGen). Bạn vui lòng cung cấp mã khi sử dụng dịch vụ. Xin cảm ơn!"
            showAlertView(title: title, view: self)
        }
    }
    
    @IBAction func typeSegmentValueChanged(_ sender: Any) {
        if let segment = sender as? UISegmentedControl {
            switch(segment.selectedSegmentIndex) {
            case 0:
                currentStatus = 1
            case 1:
                currentStatus = 0
            default:
                break;
            }
            requestData()
        }
    }
    
    deinit {
        currentUserNotificationToken?.invalidate()
    }
    
    @IBAction func touchCloseButton(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func touchAddAppointmentButton(_ sender: Any) {
        if currentUser == nil{
            confirmLogin(myView: self)
        }
        
        var optionMenu:UIAlertController? = nil
        if IS_IPAD{
            optionMenu = UIAlertController(title: nil, message: "Tuỳ chọn", preferredStyle: .alert)
        }else{
            optionMenu = UIAlertController(title: nil, message: "Tuỳ chọn", preferredStyle: .actionSheet)
        }
        
        let action1 = UIAlertAction(title: "Đặt lịch khám ngay", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let sAppointmentVC = StoryboardScene.Main.scheduleAppointmentVC.instantiate()
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(sAppointmentVC, animated: true)
        })
        let action2 = UIAlertAction(title: "Đặt lịch với Bác sĩ", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let scheduleAppointmentVC = FormScheduleAppointmentVC(nibName: "FormScheduleAppointmentVC", bundle: nil)
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(scheduleAppointmentVC, animated: true)
        })
        
        let cancelAction = UIAlertAction(title: "Huỷ", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        let image_doctor =  UIImage.resizeImage(image: UIImage(named: "user-circle")!, toWidth: 20)?.maskWithColor(color: UIColor(hexColor: 0x1976D2, alpha: 1.0))
        let image_normal =  UIImage.resizeImage(image: UIImage(named: "user-md")!, toWidth: 20)?.maskWithColor(color: UIColor(hexColor: 0xFF1E17, alpha: 1.0))
        action2.setValue(image_normal?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
        action1.setValue(image_doctor?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
        
        optionMenu?.addAction(action1)
        optionMenu?.addAction(action2)
        optionMenu?.addAction(cancelAction)
        
        // show action sheet
        optionMenu?.popoverPresentationController?.sourceView = self.view
        self.present(optionMenu!, animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Quản lý lịch hẹn"
    }

}
