//
//  AppointmentTableViewCell.swift
//  iCNM
//
//  Created by Balua on 8/14/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class AppointmentTableViewCell: UITableViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var unitName: UILabel!
    @IBOutlet weak var myGroupView: UIView!
    @IBOutlet weak var btnGift: UIButton!
    @IBOutlet weak var btnCodeGen: UIButton!
    @IBOutlet weak var iconRight: UIImageView!
   // @IBOutlet weak var spaceVConstraint: NSLayoutConstraint!
    @IBOutlet weak var spaceV1Constraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let f = contentView.frame
        let fr = UIEdgeInsetsInsetRect(f, UIEdgeInsetsMake(10, 10, 10, 15))
        contentView.frame = fr
        addShadow(cell: self)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func addShadow(cell:UITableViewCell) {
        cell.layer.cornerRadius = 4
        cell.layer.masksToBounds = true
        
        cell.layer.masksToBounds = false
        cell.layer.shadowOffset = CGSize(width: 0, height:0)
        cell.layer.shadowColor = UIColor.groupTableViewBackground.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowRadius = 2
    }
}
