//
//  ListDictMedicalVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class ListDictMedicalVC: BaseTableViewController, UIViewControllerTransitioningDelegate,UISearchBarDelegate {
    
    @IBOutlet weak var myTableView: UITableView!
    var dictMedical: [DictMedical]?
    var listKeywords: [String]?
    var listNames:[String]?
    var listMedicalNames: [String: [DictMedical]] = [:]
    
    var filteredNames:[DictMedical]?
//    let searchController = UISearchController(searchResultsController: nil)
//    var searchBarButton:UIBarButtonItem? = nil
    var newBackButton:UIBarButtonItem? = nil
    var isSearch:Bool? = false
    
    var searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 250, height: 18))
    var searchOn : UIBarButtonItem!
    var searchOff : UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        myTableView.delegate = self
        myTableView.dataSource = self
        
        myTableView.estimatedRowHeight = 70
        myTableView.rowHeight = UITableViewAutomaticDimension
        self.navigationItem.hidesBackButton = true
        newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ListDictMedicalVC.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        // Setup the Search Controller
//        searchController.searchBar.delegate = self
//        searchController.searchResultsUpdater = self
//        searchController.searchBar.placeholder = "Tìm theo Tên"
//        searchController.searchBar.frame = CGRect(x: 0, y: 0, width: 250, height: 18)
//        searchController.hidesNavigationBarDuringPresentation = false
//        searchController.delegate = self as? UISearchControllerDelegate
//        searchController.dimsBackgroundDuringPresentation = false
        
//        searchBarButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action:  #selector(ListLookUpResultVC.searchBarButtonHandler))
//        self.navigationItem.setRightBarButtonItems([searchBarButton!], animated: true)
        
        // create right bar button item
        searchOn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(isSearchOn))
        searchOff = UIBarButtonItem(title: "Hủy", style: .plain, target: self, action: #selector(isSearchOff))
        
        self.navigationItem.rightBarButtonItems = [self.searchOn]
        
        // create search bar
        self.tabBarController?.tabBar.isHidden = true
        searchBar.placeholder = "Tìm kiếm danh mục xét nghiệm.."
        searchBar.tintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        searchBar.isHidden = true
        searchBar.delegate = self
        
        title = "Từ điển xét nghiệm"
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        
        let nib = UINib(nibName: "CustomMedicalCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        getListDictMedical()
    }
    
    func isSearchOn() {
        self.navigationItem.setRightBarButtonItems([self.searchOff], animated: false)
        self.navigationItem.titleView = searchBar
        searchBar.isHidden = false
        searchBar.becomeFirstResponder()
    }
    
    func isSearchOff() {
        searchBar.text = nil
        self.navigationItem.setRightBarButtonItems([self.searchOn], animated: false)
        self.navigationItem.titleView = nil
        if searchBar.text == "" {
            isSearch = false
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        searchController.isActive = true
//    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
//    func searchBarButtonHandler(button: UIBarButtonItem)
//    {
//        // show UISearchBar
//        self.navigationItem.rightBarButtonItems = nil
//        self.navigationItem.leftBarButtonItem = nil
//        self.navigationItem.titleView = searchController.searchBar
//        self.navigationItem.titleView?.frame = searchController.searchBar.frame
//        if #available(iOS 11.0, *) {
//            searchController.searchBar.heightAnchor.constraint(equalToConstant: 40).isActive = true
//        }
//        searchController.searchBar.becomeFirstResponder()
//        searchController.searchBar.showsBookmarkButton = false
//        searchController.hidesNavigationBarDuringPresentation = false
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if searchBar.text != "" {
            isSearchOn()
        }
    }
    
    func getListDictMedical(){
        let progress = self.showProgress()
        self.dictMedical = [DictMedical]()
        self.listKeywords = [String]()
        self.listNames = [String]()
        DictMedical.getAllDictMedical(success: { (data) in
            if data.count > 0 {
                self.dictMedical?.append(contentsOf: data)
                for i in 0..<Int((self.dictMedical?.count)!)
                {
                    let dictMedicalObj = self.dictMedical?[i].testName
                    //self.listNames?.append(dictMedicalObj!)
                    
                    let index = dictMedicalObj?.index((dictMedicalObj?.startIndex)!, offsetBy: 1)
                    let result = dictMedicalObj?.substring(to: index!)
                    if !(self.listKeywords!.contains(result!)) {
                        self.listKeywords?.append(result!)
                    }
                }
                self.myTableView.reloadData()
                self.hideProgress(progress)
            } else {
                //self?.tableView.es_noticeNoMoreData()
                let lblWarning = createUILabel()
                lblWarning.center = self.view.center
                self.myTableView.addSubview(lblWarning)
                self.hideProgress(progress)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return self.listKeywords //Side Section title
    }
    
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int
    {
        return index
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if searchBar.text != "" {
            return 1
        }else{
            return self.listKeywords!.count
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchBar.text != "" {
            return self.filteredNames!.count
        }
        listMedicalNames = filterKeywords(listKeywords: self.dictMedical!)
        return listMedicalNames[self.listKeywords![section]]!.count
    }
    
    // Section Title
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
        if searchBar.text != "" {
            if let count = self.filteredNames?.count{
                return "Kết quả tìm kiếm" + " (\(count))"
            }else{
                return "Kết quả tìm kiếm" + " (0)"
            }
        }else{
            return self.listKeywords?[section]
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header : UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.red
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomMedicalCell
        let medicalObj: DictMedical?
        if searchBar.text != "" {
            medicalObj = (self.filteredNames?[indexPath.row])!
            cell.lblNameTest.text = medicalObj?.testName
            let cost = medicalObj?.price
            if cost == 0{
                cell.lblCost.text = "0đ"
            }else{
                let format = formatString(value: cost!)
                cell.lblCost.text = format
            }
            
            cell.lblNameTest.lineBreakMode = .byWordWrapping
            cell.lblNameTest.numberOfLines = 0
            cell.lblNameTest.adjustsFontSizeToFitWidth = true
        } else {
            if let medicalObj = listMedicalNames[(self.listKeywords?[indexPath.section])!]?[indexPath.row]{
                cell.lblNameTest.text = medicalObj.testName
                cell.lblNameTest.lineBreakMode = .byWordWrapping
                cell.lblNameTest.numberOfLines = 0
                cell.lblNameTest.adjustsFontSizeToFitWidth = true
                let cost = medicalObj.price
                if cost == 0{
                    cell.lblCost.text = "0đ"
                }else{
                    let format = formatString(value: cost)
                    cell.lblCost.text = format
                }
            }
        }
        return cell
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            isSearch = false
            view.endEditing(true)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        else {
            isSearch = true
            filterContentForSearchText(searchText)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredNames = dictMedical?.filter({( medical : DictMedical) -> Bool in
            return (medical.testName.lowercased().contains(searchText.lowercased()))
        })
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let detailDictMedicalVC = DetailDictMedicalVC(nibName: "DetailDictMedicalVC", bundle: nil)
        if searchBar.text != "" {
            isSearch = true
            if let medicalObj = filteredNames?[indexPath.row]{
                detailDictMedicalVC.headerString = medicalObj.testName
                detailDictMedicalVC.meaning1 = medicalObj.meaning1
                detailDictMedicalVC.meaning2 = medicalObj.meaning2
                self.navigationController?.pushViewController(detailDictMedicalVC, animated: true)
            }
        }else{
            isSearch = false
            if let medicalObj = listMedicalNames[(self.listKeywords?[indexPath.section])!]?[indexPath.row]{
                detailDictMedicalVC.headerString = medicalObj.testName
                detailDictMedicalVC.meaning1 = medicalObj.meaning1
                detailDictMedicalVC.meaning2 = medicalObj.meaning2
                self.navigationController?.pushViewController(detailDictMedicalVC, animated: true)
            }
        }
    }
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension//Choose your custom row height
    }
    
//    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
//        filterContentForSearchText(searchBar.text!)
//    }
//    func updateSearchResults(for searchController: UISearchController) {
//        filterContentForSearchText(searchController.searchBar.text!)
//
//        if searchController.isActive{
//            self.navigationItem.rightBarButtonItems = nil
//            self.navigationItem.leftBarButtonItem = nil
//        }else{
//            self.navigationItem.setRightBarButtonItems([searchBarButton!], animated: true)
//            self.navigationItem.leftBarButtonItem = newBackButton
//        }
//    }
    
//    func searchBarCancelButtonClicked(_ searchBar:UISearchBar) {
//        self.navigationItem.titleView = nil
//        self.navigationItem.setRightBarButtonItems([searchBarButton!], animated: true)
//        self.navigationItem.leftBarButtonItem = newBackButton
//    }
//
//    func didPresentSearchController(searchController: UISearchController) {
//        searchController.searchBar.becomeFirstResponder()
//    }
//
//    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
//        searchController.searchBar.tintColor = UIColor.gray
//        return true
//    }
}
