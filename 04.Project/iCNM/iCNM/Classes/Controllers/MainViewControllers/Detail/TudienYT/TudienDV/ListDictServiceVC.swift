//
//  ListDictDiseaseVC.swift
//  iCNM
//
//  Created by Mac osx on 11/6/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import FontAwesome_swift
import Toaster
import PhotoSlider

struct SpecialistServicesGroups {
    var section: String
    var items: [SpecialistServicesGroup]
    var collapsed: Bool
    
    public init(section: String, items: [SpecialistServicesGroup], collapsed: Bool = false) {
        self.section = section
        self.items = items
        self.collapsed = collapsed
    }
}

class ListDictServiceVC: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate, CustomMedicalBodyHeaderDelegate {
    
    @IBOutlet weak var myTableView: UITableView!
    var specialistServices:[SpecialistServices]?
    var specialistServicesGroup:[SpecialistServicesGroup]?
    var sectionsData: [SpecialistServicesGroups]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        title = "Từ điển Dịch vụ"
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(ListDictDiseaseVC.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 200
        myTableView.rowHeight = UITableViewAutomaticDimension
        self.sectionsData = [SpecialistServicesGroups]()
        
        let nib_header = UINib(nibName: "CustomMedicalBodyHeader", bundle: nil)
        myTableView.register(nib_header, forHeaderFooterViewReuseIdentifier: "headerCell")
        
        let nib = UINib(nibName: "DetailMedicalBodyCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        
        getSpecialistServices()
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func getSpecialistServices(){
        let progress = self.showProgress()
        self.specialistServices = [SpecialistServices]()
        SpecialistServices.getSpecialistServices(success: { (data) in
            self.specialistServices?.append(contentsOf: data)
            for i in 0..<Int((self.specialistServices?.count)!){
                let specialistServiceObj = self.specialistServices![i]
                let specialistID = specialistServiceObj.specialistID
                let specialName = specialistServiceObj.specialName
                SpecialistServicesGroup.getSpecialistServicesGroup(specialistID:specialistID, success: { (data) in
                    self.specialistServicesGroup = [SpecialistServicesGroup]()
                    if !self.specialistServicesGroup!.contains(where: { $0.name == specialName}) {
                        self.specialistServicesGroup?.append(contentsOf: data)
                        self.specialistServicesGroup?.sort {
                            $0.name.trimmingCharacters(in: .whitespaces).localizedCaseInsensitiveCompare($1.name.trimmingCharacters(in: .whitespaces)) == ComparisonResult.orderedAscending
                        }
                    }
                    self.sectionsData?.append(SpecialistServicesGroups(section: specialName, items: self.specialistServicesGroup!, collapsed: true))
                    DispatchQueue.main.async(execute: {
                        if self.sectionsData?.count == self.specialistServices?.count{
                            self.hideProgress(progress)
                            self.myTableView.reloadData()
                        }
                    })
                }, fail: { (error, response) in
                    self.hideProgress(progress)
                    self.handleNetworkError(error: error, responseData: response.data)
                })
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
       return (self.sectionsData?.count)!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionsData![section].collapsed ? 0 : sectionsData![section].items.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 40
    }
    
    func toggleSection(_ header: CustomMedicalBodyHeader, section: Int) {
        let collapsed = !(self.sectionsData?[section].collapsed)!
        self.sectionsData?[section].collapsed = collapsed
        header.setCollapsed(collapsed)
        myTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerCell") as? CustomMedicalBodyHeader ?? CustomMedicalBodyHeader(reuseIdentifier: "headerCell")
        
        if let name = self.sectionsData?[section].section, let items = self.sectionsData?[section].items{
            headerView.lblNameSection.text = name + " (\(items.count))"
        }
        
        headerView.setCollapsed((sectionsData?[section].collapsed)!)
        headerView.lblArrowSection.font = UIFont.fontAwesome(ofSize: 18.0)
        if self.sectionsData?[section].items.count != 0{
            headerView.lblArrowSection.text = String.fontAwesomeIcon(name: .angleRight)
        }else{
            headerView.lblArrowSection.text = ""
        }
        headerView.section = section
        headerView.delegate = self
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DetailMedicalBodyCell
        let medicalDetailObj = self.sectionsData![indexPath.section].items[indexPath.row]
        cell.lblTitle.text = medicalDetailObj.name
        if let mean = try! medicalDetailObj.name.convertHtmlSymbols(){
            cell.lblDesc.text = mean
            cell.lblDesc.lineBreakMode = .byWordWrapping
            cell.lblDesc.numberOfLines = 0
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 85
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let medicalDetailObj = self.sectionsData![indexPath.section].items[indexPath.row]
        let detailDictServices = DetailDictServices(nibName: "DetailDictServices", bundle: nil)
        detailDictServices.titleServices = medicalDetailObj.name
        detailDictServices.getDetailDictServicesGroup(specialistServicesObj: medicalDetailObj)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(detailDictServices, animated: true)
    }
}
