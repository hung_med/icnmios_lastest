//
//  DetailDictMedicalVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class DetailDictMedicalVC: BaseTableViewController {

    var sectionsData: [SectionGroup]?
    @IBOutlet weak var myTableView: UITableView!
    var meaning1:String? = nil
    var meaning2:String? = nil
    var headerString:String? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        if #available(iOS 11.0, *) {
            UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
            // Setup the Search Controller
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(DetailDictMedicalVC.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        myTableView.delegate = self
        myTableView.dataSource = self
        
        myTableView.estimatedRowHeight = 70
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        let nib = UINib(nibName: "DetailMedicalBodyCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        title = headerString
        let nib_header = UINib(nibName: "CustomMedicalBodyHeader", bundle: nil)
        myTableView.register(nib_header, forHeaderFooterViewReuseIdentifier: "headerCell")
        sectionsData = [SectionGroup]()
        
        self.sectionsData?.append(SectionGroup(name: "", items: [
            ItemInSection(name: headerString!)]))
    
        self.sectionsData?.append(SectionGroup(name: "Ý nghĩa chung", items: [
            ItemInSection(name: meaning1!)]))
        
        self.sectionsData?.append(SectionGroup(name: "Dành cho bác sỹ", items: [
            ItemInSection(name: meaning2!)]))
        
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    // MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerCell") as? CustomMedicalBodyHeader ?? CustomMedicalBodyHeader(reuseIdentifier: "headerCell")
        
        headerView.lblNameSection.text = sectionsData?[section].name
        headerView.lblNameSection.textColor = UIColor.white
        //headerView.setCollapsed((sectionsData?[section].collapsed)!)
        headerView.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        
        headerView.section = section
       // headerView.delegate = self
        return headerView
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsData!.count
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if section == 0{
            return 0
        }
        return 40
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionsData![section].items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item: ItemInSection = sectionsData![indexPath.section].items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DetailMedicalBodyCell
        
        cell.lblTitle.isHidden = true
        if indexPath.section == 0{
            cell.lblDesc.font = UIFont.boldSystemFont(ofSize: 16.0)
        }
        if let name = try! item.name.convertHtmlSymbols(){
            if !name.isEmpty{
                cell.lblDesc.text = name
            }else{
                cell.lblDesc.text = "Đang cập nhật dữ liệu"
            }
        }else{
            
        }
        cell.lblDesc.lineBreakMode = .byWordWrapping
        cell.lblDesc.numberOfLines = 0
        cell.lblDesc.adjustsFontSizeToFitWidth = true
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension//Choose your custom row height
    }
}


