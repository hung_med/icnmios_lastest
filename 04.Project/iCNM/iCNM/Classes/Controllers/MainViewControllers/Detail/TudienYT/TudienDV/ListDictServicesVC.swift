//
//  NewsDetailViewController.swift
//  iCNM
//
//  Created by Mac osx on 7/12/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class ListDictServicesVC: UIViewController, DictServicesVCDelegate {

    var dictServicesVC:DictServicesVC?
    var dictServicesGroupVC:DictServicesGroupVC?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(ListDictServicesVC.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        dictServicesVC = DictServicesVC(nibName: "DictServicesVC", bundle: nil)
        dictServicesVC?.delegate = self
        dictServicesVC?.view.layer.borderColor = UIColor.lightGray.cgColor
        dictServicesVC?.view.layer.borderWidth = 1.0
        self.view.addSubview((dictServicesVC?.view)!)
        self.addChildViewController(dictServicesVC!)
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func loadServiceMeaningGroup(specialistID:Int){
        dictServicesGroupVC = DictServicesGroupVC(nibName: "DictServicesGroupVC", bundle: nil)
        dictServicesGroupVC?.getSpecialistServicesGroup(specialistID: specialistID)
        if dictServicesGroupVC?.view != nil{
            dictServicesGroupVC?.view.removeFromSuperview()
            dictServicesGroupVC?.removeFromParentViewController()
        }
        self.view.addSubview((dictServicesGroupVC?.view)!)
        self.addChildViewController(dictServicesGroupVC!)
    }
    
}
