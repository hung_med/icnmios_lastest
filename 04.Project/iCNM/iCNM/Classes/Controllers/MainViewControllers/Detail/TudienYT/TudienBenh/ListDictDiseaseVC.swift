//
//  ListDictDiseaseVC.swift
//  iCNM
//
//  Created by Mac osx on 11/6/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import FontAwesome_swift
import Toaster
import PhotoSlider

struct ListDictDiseaseGroup {
    var section: String
    var items: [MedicalDetailsByGroups]
    var collapsed: Bool
    
    public init(section: String, items: [MedicalDetailsByGroups], collapsed: Bool = false) {
        self.section = section
        self.items = items
        self.collapsed = collapsed
    }
}

struct ListMedicalSymptonByLocationGroup {
    var section: String
    var items: [MedicalSymptonByLocationBody]
    var collapsed: Bool
    
    public init(section: String, items: [MedicalSymptonByLocationBody], collapsed: Bool = false) {
        self.section = section
        self.items = items
        self.collapsed = collapsed
    }
}

class ListDictDiseaseVC: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate, CustomMedicalBodyHeaderDelegate {
   
    var scopeSelected = ScrollableSegmentedControl()
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myView: UIView!
    let segmented = YSSegmentedControl(frame: .zero, titles: [])
    var dictDiseaseNames: [String]?
    var listDictDisease:[DictDiseaseGroups]?
    var medicalDetails:[MedicalDetailsByGroups]?
    var sectionsData: [ListDictDiseaseGroup]?
    var medicalLocationBody:[MedicalLocationBody]?
    var medicalSymptonByLocationBodys:[MedicalSymptonByLocationBody]?
    var sectionsData2: [ListMedicalSymptonByLocationGroup]?
    var currentIndex:Int = 0
    let scrollView = UIScrollView()
   
    @IBOutlet weak var topConstr: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        title = "Từ điển bệnh"
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(ListDictDiseaseVC.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }

        dictDiseaseNames = ["Nhóm bệnh", "Triệu chứng"]
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 200
        myTableView.rowHeight = UITableViewAutomaticDimension
        
        let nib_header = UINib(nibName: "CustomMedicalBodyHeader", bundle: nil)
        myTableView.register(nib_header, forHeaderFooterViewReuseIdentifier: "headerCell")
        
        let nib = UINib(nibName: "DetailMedicalBodyCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        self.reloadSegmentController(names: self.dictDiseaseNames!, currentTab: 0)
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
  
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if IS_IPAD{
            if #available(iOS 11.0, *) {
                topConstr.constant = 0
            }else{
                self.myView.frame = CGRect(x: 0, y: 0, width: myView.frame.size.width, height: myView.frame.size.height)
                self.myTableView.frame = CGRect(x: 0, y: 40, width: myTableView.frame.size.width, height: myTableView.frame.size.height+76)
            }
            
        }else{
            if #available(iOS 11.0, *) {
                
            }
            else{
                self.myView.frame = CGRect(x: 0, y: 0, width: myView.frame.size.width, height: myView.frame.size.height)
                self.myTableView.frame = CGRect(x: 0, y: 40, width: myTableView.frame.size.width, height: myTableView.frame.size.height+76)
            }
            
            if IS_IPHONE_X{
                self.myView.frame = CGRect(x: 0, y: 20, width: myView.frame.size.width, height: myView.frame.size.height)
                self.myTableView.frame = CGRect(x: 0, y: 108, width: myTableView.frame.size.width, height: myTableView.frame.size.height)
            }
        }
    }
    
    func reloadSegmentController(names:[String], currentTab:Int){
        if #available(iOS 11.0, *) {
            scopeSelected.frame = CGRect(x: 0, y: 38, width: screenSizeWidth/2*CGFloat(names.count), height: 38)
        }
        else{
            scopeSelected.frame = CGRect(x: 0, y: 34, width: screenSizeWidth/2*CGFloat(names.count), height: 30)
        }
        
        var indexCate: Int = 0
        for copeTitle in names {
            scopeSelected.insertSegment(withTitle: copeTitle, at: indexCate)
            indexCate += 1
        }
        
        scopeSelected.addTarget(self, action: #selector(ListDictDiseaseVC.getAllDictDisease), for: .valueChanged)
        scopeSelected.segmentContentColor = UIColor.lightGray
        scopeSelected.underlineSelected = true
        scopeSelected.segmentStyle = .textOnly
        scopeSelected.backgroundColor = UIColor.clear
        scopeSelected.tintColor = UIColor(hex:"1976D2")
        // add it in a scrollView, because ill got too much categories here. Just if you need that:
        
        let scrollView  = UIScrollView()
        if #available(iOS 11.0, *) {
            scrollView.frame = CGRect(x:0, y: 34, width: screenSizeWidth, height: 100)
        }
        else{
            scrollView.frame = CGRect(x:0, y: 45, width: screenSizeWidth, height: 100)
        }
        scrollView.contentSize = CGSize(width: scopeSelected.frame.size.width + 16,height: scopeSelected.frame.size.height-1)
        scrollView.showsHorizontalScrollIndicator = false
        // set first category
        self.sectionsData = [ListDictDiseaseGroup]()
        self.sectionsData2 = [ListMedicalSymptonByLocationGroup]()
        scopeSelected.selectedSegmentIndex = 0
        scrollView.addSubview(scopeSelected)
        
        myView?.addSubview(scrollView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        title = "Từ điển bệnh"
        super.viewDidAppear(animated)
        if IS_IPAD{
            topConstr.constant = 0
        }else{
            if IS_IPHONE_X{
                self.myView.frame = CGRect(x: 0, y: 20, width: myView.frame.size.width, height: myView.frame.size.height)
                self.myTableView.frame = CGRect(x: 0, y: 108, width: myTableView.frame.size.width, height: myTableView.frame.size.height)
            }
        }
    }
    
    func getAllDictDisease(_ sender: ScrollableSegmentedControl){
        let index = scopeSelected.selectedSegmentIndex
        currentIndex = index
        if index == 0{
            if self.sectionsData?.count != 0{
                self.myTableView.reloadData()
            }else{
                let progress = self.showProgress()
                self.listDictDisease = [DictDiseaseGroups]()
                DictDiseaseGroups.getDictDiseaseGroups(success: { (data) in
                    self.listDictDisease?.append(contentsOf: data)
                    for i in 0..<Int((self.listDictDisease?.count)!){
                        let dictDiseaseObj = self.listDictDisease![i]
                        let diseaseGroupID = dictDiseaseObj.diseaseGroupID
                        let dGName = dictDiseaseObj.dGName
                        self.getMedicalDetailsByGroups(diseaseGroupID: diseaseGroupID, dGName:dGName)
                    }
                    self.hideProgress(progress)
                }, fail: { (error, response) in
                    self.hideProgress(progress)
                    self.handleNetworkError(error: error, responseData: response.data)
                })
            }
        }else{
            if self.sectionsData2?.count != 0{
                self.myTableView.reloadData()
            }else{
                let progress = self.showProgress()
                self.medicalLocationBody = [MedicalLocationBody]()
                MedicalLocationBody.getMedicalLocationBody(success: { (data) in
                    self.medicalLocationBody?.append(contentsOf: data)
                    for i in 0..<Int((self.medicalLocationBody?.count)!){
                        let medicalLocationObj = self.medicalLocationBody![i]
                        let locationBodyID = medicalLocationObj.locationBodyID
                        let locationBodyName = medicalLocationObj.locationBodyName
                        self.getMedicalSymptonByLocationBody(locationBodyID: locationBodyID, locationBodyName:locationBodyName)
                    }
                    self.hideProgress(progress)
                }, fail: { (error, response) in
                    self.hideProgress(progress)
                    self.handleNetworkError(error: error, responseData: response.data)
                })
            }
        }
    }
    
    func getMedicalDetailsByGroups(diseaseGroupID:Int, dGName:String){
        let progress = self.showProgress()
        MedicalDetailsByGroups().getMedicalDetailsByGroups(diseaseGroupID:diseaseGroupID, success: { (data) in
            self.medicalDetails = [MedicalDetailsByGroups]()
            self.medicalDetails?.append(contentsOf: data)
            self.medicalDetails?.sort {
                $0.name.trimmingCharacters(in: .whitespaces).localizedCaseInsensitiveCompare($1.name.trimmingCharacters(in: .whitespaces)) == ComparisonResult.orderedAscending
            }
            self.sectionsData?.append(ListDictDiseaseGroup(section: dGName,
                    items: self.medicalDetails!, collapsed: true))
            
            self.hideProgress(progress)
            DispatchQueue.main.async(execute: {
                if self.sectionsData?.count == self.listDictDisease?.count{
                    self.myTableView.reloadData()
                }
            })
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func getMedicalSymptonByLocationBody(locationBodyID:Int, locationBodyName:String){
        let progress = self.showProgress()
        MedicalSymptonByLocationBody().getMedicalSymptonByLocationBody(locationBodyID:locationBodyID, success: { (data) in
            self.medicalSymptonByLocationBodys = [MedicalSymptonByLocationBody]()
            self.medicalSymptonByLocationBodys?.append(contentsOf: data)
            self.medicalSymptonByLocationBodys?.sort {
                $0.name.trimmingCharacters(in: .whitespaces).localizedCaseInsensitiveCompare($1.name.trimmingCharacters(in: .whitespaces)) == ComparisonResult.orderedAscending
            }
            self.sectionsData2?.append(ListMedicalSymptonByLocationGroup(section: locationBodyName, items: self.medicalSymptonByLocationBodys!, collapsed: true))
            self.hideProgress(progress)
            DispatchQueue.main.async(execute: {
                if self.sectionsData2?.count == self.medicalLocationBody?.count{
                    self.myTableView.reloadData()
                }
            })
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func switchChanged(_ sender: ScrollableSegmentedControl){
        // currentIndex = sender.selectedSegmentIndex
        self.myTableView .reloadData()
    }
  
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if self.currentIndex == 0{
            return (self.sectionsData?.count)!
        }else{
            return (self.sectionsData2?.count)!
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.currentIndex == 0{
            return sectionsData![section].collapsed ? 0 : sectionsData![section].items.count
        }else{
            return sectionsData2![section].collapsed ? 0 : sectionsData2![section].items.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 40
    }
    
    func toggleSection(_ header: CustomMedicalBodyHeader, section: Int) {
        if self.currentIndex == 0{
            if sectionsData?.count != 0{
                let collapsed = !(self.sectionsData?[section].collapsed)!
                self.sectionsData?[section].collapsed = collapsed
                header.setCollapsed(collapsed)
                myTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
            }
        }else{
            if sectionsData2?.count != 0{
                let collapsed = !(self.sectionsData2?[section].collapsed)!
                self.sectionsData2?[section].collapsed = collapsed
                header.setCollapsed(collapsed)
                myTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
            }
        }
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerCell") as? CustomMedicalBodyHeader ?? CustomMedicalBodyHeader(reuseIdentifier: "headerCell")
        
        if self.currentIndex == 0{
            if let name = self.sectionsData?[section].section, let items = self.sectionsData?[section].items{
                headerView.lblNameSection.text = name + " (\(items.count))"
            }
            
            headerView.setCollapsed((sectionsData?[section].collapsed)!)
            headerView.lblArrowSection.font = UIFont.fontAwesome(ofSize: 18.0)
            if self.sectionsData?[section].items.count != 0{
                headerView.lblArrowSection.text = String.fontAwesomeIcon(name: .angleRight)
            }else{
                headerView.lblArrowSection.text = ""
            }
            headerView.section = section
            headerView.delegate = self
        }else{
            if let name = self.sectionsData2?[section].section, let items = self.sectionsData2?[section].items{
                headerView.lblNameSection.text = name + " (\(items.count))"
            }
            
            headerView.setCollapsed((sectionsData2?[section].collapsed)!)
            headerView.lblArrowSection.font = UIFont.fontAwesome(ofSize: 18.0)
            if self.sectionsData2?[section].items.count != 0{
                headerView.lblArrowSection.text = String.fontAwesomeIcon(name: .angleRight)
            }else{
                headerView.lblArrowSection.text = ""
            }
            headerView.section = section
            headerView.delegate = self
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DetailMedicalBodyCell
        if self.currentIndex == 0{
            let medicalDetailObj = self.sectionsData![indexPath.section].items[indexPath.row]
            cell.lblTitle.text = medicalDetailObj.name
            if let mean = try! medicalDetailObj.mean.convertHtmlSymbols(){
                cell.lblDesc.text = mean
                cell.lblDesc.lineBreakMode = .byWordWrapping
                cell.lblDesc.numberOfLines = 0
            }
        }else{
            let medicalDetailObj = self.sectionsData2![indexPath.section].items[indexPath.row]
            cell.lblTitle.text = medicalDetailObj.name
            if let mean = try! medicalDetailObj.mean.convertHtmlSymbols(){
                cell.lblDesc.text = mean
                cell.lblDesc.lineBreakMode = .byWordWrapping
                cell.lblDesc.numberOfLines = 0
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 85
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.currentIndex == 0{
            let medicalDetailObj = self.sectionsData![indexPath.section].items[indexPath.row]
            let detailDictionGroupVC = DetailDictionGroupVC(nibName: "DetailDictionGroupVC", bundle: nil)
            let medicalDicID = medicalDetailObj.medicalDicID
            detailDictionGroupVC.titleDiction = medicalDetailObj.name
            detailDictionGroupVC.getMedicalDictionaryDetail(medicalDicID:medicalDicID)
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(detailDictionGroupVC, animated: true)
        }else{
            let medicalDetailObj = self.sectionsData2![indexPath.section].items[indexPath.row]
            let detailDictionGroupVC = DetailMedicalBodyGroup(nibName: "DetailDictionGroupVC", bundle: nil)
            let medicalSymptonID = medicalDetailObj.medicalSymptonID
            detailDictionGroupVC.titleMedicalBody = medicalDetailObj.name
            detailDictionGroupVC.getMedicalSymptonByID(symptonByID: medicalSymptonID)
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(detailDictionGroupVC, animated: true)
        }
    }
}
