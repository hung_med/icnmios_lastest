//
//  ListLookUpResultVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class DetailMedicalBodyGroup: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate, CustomMedicalBodyHeaderDelegate {

    @IBOutlet weak var myTableView: UITableView!
    var medicalSymptonByIDs:[MedicalSymptonByID]?
    var sectionsData: [SectionGroup]?
    var listImageURL:[String]?
    var listDescriptions:[String]?
    var titleMedicalBody:String? = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(DetailMedicalBodyGroup.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        title = titleMedicalBody
        
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 70
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.none

        let nib1 = UINib(nibName: "CustomSlideTableViewCell", bundle: nil)
        myTableView.register(nib1, forCellReuseIdentifier: "Cell3")
        
        let nib = UINib(nibName: "DetailMedicalBodyCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        
        let nib2 = UINib(nibName: "DetailMedicalBodyCell2", bundle: nil)
        myTableView.register(nib2, forCellReuseIdentifier: "Cell2")
        
        let nib_header = UINib(nibName: "CustomMedicalBodyHeader", bundle: nil)
        myTableView.register(nib_header, forHeaderFooterViewReuseIdentifier: "headerCell")
        
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func getMedicalSymptonByID(symptonByID:Int){
        let progress = self.showProgress()
        listImageURL = [String]()
        listDescriptions = [String]()
        sectionsData = [SectionGroup]()
        medicalSymptonByIDs = [MedicalSymptonByID]()
        let medicalSymptonByID = MedicalSymptonByID()
        medicalSymptonByID.getMedicalSymptonByID(symptonID:symptonByID, success: { (data) in
            if let medicalSymptonObj = data {
                
                self.medicalSymptonByIDs?.append(medicalSymptonObj)
               
                if (medicalSymptonObj.medicalSympton?.mean.utf8.count)! >= 0{
                    if let mean = try! medicalSymptonObj.medicalSympton?.mean.convertHtmlSymbols(){
                        self.sectionsData?.append(SectionGroup(name: "Định nghĩa", items: [
                            ItemInSection(name: mean)], collapsed: false))
                    }
                }
                if (medicalSymptonObj.medicalSympton?.description.utf8.count)! >= 0{
                    if let description = try! medicalSymptonObj.medicalSympton?.description.convertHtmlSymbols(){
                        self.sectionsData?.append(SectionGroup(name: "Mô tả", items: [
                            ItemInSection(name: description)], collapsed: false))
                    }
                }
                if (medicalSymptonObj.medicalSympton?.reason.utf8.count)! >= 0{
                    if let reason = try! medicalSymptonObj.medicalSympton?.reason.convertHtmlSymbols(){
                        self.sectionsData?.append(SectionGroup(name: "Nguyên nhân", items: [
                            ItemInSection(name: reason)], collapsed: false))
                    }
                }
                if (medicalSymptonObj.medicalSympton?.diagnosis.utf8.count)! >= 0{
                    if let diagnosis = try! medicalSymptonObj.medicalSympton?.diagnosis.convertHtmlSymbols(){
                        self.sectionsData?.append(SectionGroup(name: "Chuẩn đoán", items: [
                            ItemInSection(name: diagnosis)], collapsed: false))
                    }
                }
                
                self.medicalSymptonByIDs?.append(medicalSymptonObj)
                let existImageURL = medicalSymptonObj.medicalSympton?.pictures
                let descriptionPictures = medicalSymptonObj.medicalSympton?.descriptionPictures
                
                let urlString = existImageURL?.components(separatedBy: ",")
                for url in urlString! {
                    self.listImageURL?.append(url)
                }
                
                let descPictures = descriptionPictures?.components(separatedBy: ",")
                for desc in descPictures! {
                    self.listDescriptions?.append(desc)
                }
                
                self.sectionsData?.append(SectionGroup(name: "Bệnh liên quan", items: [
                    ItemInSection(name: "")]))
                
                self.myTableView.reloadData()
                self.hideProgress(progress)
            } else {
                let lblWarning = createUILabel()
                self.myTableView.addSubview(lblWarning)
            }
            
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerCell") as? CustomMedicalBodyHeader ?? CustomMedicalBodyHeader(reuseIdentifier: "headerCell")
        
        headerView.lblNameSection.text = sectionsData?[section].name
        headerView.setCollapsed((sectionsData?[section].collapsed)!)
        headerView.contentView.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        headerView.lblNameSection.textColor = UIColor.white
        headerView.lblNameSection.font = UIFont.boldSystemFont(ofSize: 16)
        
        headerView.lblArrowSection.font = UIFont.fontAwesome(ofSize: 18)
        headerView.lblArrowSection.text = String.fontAwesomeIcon(name: .angleRight)
        headerView.section = section
        headerView.delegate = self
        return headerView
    }
    
    func toggleSection(_ header: CustomMedicalBodyHeader, section: Int) {
        let collapsed = !(self.sectionsData?[section].collapsed)!
        
        // Toggle collapse
        self.sectionsData?[section].collapsed = collapsed
        header.setCollapsed(collapsed)
        
        myTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsData!.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == sectionsData!.count-1{
            let medicalSymptonObj = self.medicalSymptonByIDs?[0]
            return (medicalSymptonObj!.medicalDictionary?.count)!
        }else{
            return sectionsData![section].collapsed ? 0 : sectionsData![section].items.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var medicalSymptonObj:MedicalSymptonByID? = nil
        if (self.medicalSymptonByIDs?.count)! > 0{
            medicalSymptonObj = self.medicalSymptonByIDs?[0]
        }
        
        if indexPath.section == ((self.sectionsData?.count)!-1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath) as! DetailMedicalBodyCell2
            let medicalSympton = medicalSymptonObj!.medicalDictionary?[indexPath.row]
            cell.lblTitle?.text = medicalSympton?.mean
            cell.lblTitle.lineBreakMode = .byWordWrapping
            cell.lblTitle.numberOfLines = 0
            cell.lblTitle.adjustsFontSizeToFitWidth = true
            
            let existImageURL = medicalSympton?.pictures
            let urlString = existImageURL?.components(separatedBy: ",")
            
            for url in urlString! {
                cell.imageThumb.contentMode = .scaleToFill
                let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                cell.imageThumb.loadImageUsingUrlString(urlString: urlString!)
            }
            
            return cell
        }else{
            let item: ItemInSection = sectionsData![indexPath.section].items[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DetailMedicalBodyCell
            
            if !item.name.trim().isEmpty{
                cell.lblDesc.text = item.name
            }else{
                cell.lblDesc.text = "Đang cập nhật dữ liệu"
            }
            cell.lblDesc.lineBreakMode = .byWordWrapping
            cell.lblDesc.numberOfLines = 0
            cell.lblDesc.adjustsFontSizeToFitWidth = true
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == ((self.sectionsData?.count)!-1){
            let detailDictionGroupVC = DetailDictionGroupVC(nibName: "DetailDictionGroupVC", bundle: nil)
            let medicalSymptonsObj = self.medicalSymptonByIDs?[0]
            let medicalSympton = medicalSymptonsObj?.medicalDictionary?[indexPath.row]
            if medicalSympton?.medicalDicID != 0{
                detailDictionGroupVC.getMedicalDictionaryDetail(medicalDicID:(medicalSympton?.medicalDicID)!)
                self.navigationController?.pushViewController(detailDictionGroupVC, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
}
