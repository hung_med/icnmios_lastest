//
//  ListLookUpResultVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class DetailDictionGroupVC: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate, CustomMedicalBodyHeaderDelegate {
    
    @IBOutlet weak var myTableView: UITableView!
    var medicalDictDetails:[MedicalDictionaryDetail]?
    var listImageURL:[String]?
    var listDescriptions:[String]?
    var sectionsData: [SectionGroup]?
    var titleDiction:String? = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(DetailDictionGroupVC.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        title = titleDiction
        myTableView.delegate = self
        myTableView.dataSource = self
        
        myTableView.estimatedRowHeight = 70
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.none
       
        let nib = UINib(nibName: "DetailMedicalBodyCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        
        let nib2 = UINib(nibName: "DetailMedicalBodyCell2", bundle: nil)
        myTableView.register(nib2, forCellReuseIdentifier: "Cell2")
        
        let nib_header = UINib(nibName: "CustomMedicalBodyHeader", bundle: nil)
        myTableView.register(nib_header, forHeaderFooterViewReuseIdentifier: "headerCell")
        
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func getMedicalDictionaryDetail(medicalDicID:Int){
        let progress = self.showProgress()
        listImageURL = [String]()
        listDescriptions = [String]()
        sectionsData = [SectionGroup]()
        self.medicalDictDetails = [MedicalDictionaryDetail]()
        MedicalDictionaryDetail().getMedicalDictionaryDetail(medicalDicID:medicalDicID, success: { (data) in
            if let medicalDictDetailObj = data {
                self.medicalDictDetails?.append(medicalDictDetailObj)
        
                if (medicalDictDetailObj.mean.utf8.count) >= 0{
                    if let mean = try! medicalDictDetailObj.mean.convertHtmlSymbols(){
                        self.sectionsData?.append(SectionGroup(name: "Định nghĩa", items: [
                            ItemInSection(name: mean)], collapsed: false))
                    }
                }
                if (medicalDictDetailObj.overview.utf8.count) >= 0{
                    if let overview = try! medicalDictDetailObj.overview.convertHtmlSymbols(){
                        self.sectionsData?.append(SectionGroup(name: "Tổng quan", items: [
                            ItemInSection(name: overview)], collapsed: false))
                    }
                }
                if (medicalDictDetailObj.reason.utf8.count) >= 0{
                    if let reason = try! medicalDictDetailObj.reason.convertHtmlSymbols(){
                        self.sectionsData?.append(SectionGroup(name: "Nguyên nhân", items: [
                            ItemInSection(name: reason)], collapsed: false))
                    }
                }
                if (medicalDictDetailObj.symptom.utf8.count) >= 0{
                    if let symptom = try! medicalDictDetailObj.symptom.convertHtmlSymbols(){
                        self.sectionsData?.append(SectionGroup(name: "Triệu chứng", items: [
                            ItemInSection(name: symptom)], collapsed: false))
                    }
                }
                if (medicalDictDetailObj.classify.utf8.count) >= 0{
                    if let classify = try! medicalDictDetailObj.classify.convertHtmlSymbols(){
                        self.sectionsData?.append(SectionGroup(name: "Phân loại", items: [
                            ItemInSection(name: classify)], collapsed: false))
                    }
                }
                if (medicalDictDetailObj.prevention.utf8.count) >= 0{
                    if let prevention = try! medicalDictDetailObj.prevention.convertHtmlSymbols(){
                        self.sectionsData?.append(SectionGroup(name: "Phòng bệnh và điều trị", items: [
                            ItemInSection(name: prevention)], collapsed: false))
                    }
                }
                
                self.sectionsData?.append(SectionGroup(name: "Triệu chứng khác", items: [
                    ItemInSection(name: "")]))
                
                let existImageURL = medicalDictDetailObj.pictures
                let descriptionPictures = medicalDictDetailObj.descriptionPictures
                
                let urlString = existImageURL.components(separatedBy: ",")
                for url in urlString {
                    self.listImageURL?.append(url)
                }
                
                let descPictures = descriptionPictures.components(separatedBy: ",")
                for desc in descPictures {
                    self.listDescriptions?.append(desc)
                }
                
                self.myTableView.reloadData()
                self.hideProgress(progress)
            }else {
                //self?.tableView.es_noticeNoMoreData()
                let lblWarning = createUILabel()
                lblWarning.center = self.view.center
                self.myTableView.addSubview(lblWarning)
                self.hideProgress(progress)
            }
            
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerCell") as? CustomMedicalBodyHeader ?? CustomMedicalBodyHeader(reuseIdentifier: "headerCell")
        headerView.lblNameSection.text = sectionsData?[section].name
        headerView.lblNameSection.textColor = UIColor.white
        headerView.lblNameSection.font = UIFont.boldSystemFont(ofSize: 16)
        headerView.setCollapsed((sectionsData?[section].collapsed)!)
        headerView.contentView.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        headerView.lblArrowSection.isHidden = false
        headerView.lblArrowSection.font = UIFont.fontAwesome(ofSize: 18.0)
        headerView.lblArrowSection.text = String.fontAwesomeIcon(name: .angleRight)
        headerView.section = section
        headerView.delegate = self
        
        return headerView
    }
    
    func toggleSection(_ header: CustomMedicalBodyHeader, section: Int) {
        let collapsed = !(self.sectionsData?[section].collapsed)!
        
        // Toggle collapse
        self.sectionsData?[section].collapsed = collapsed
        header.setCollapsed(collapsed)
        
        myTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsData!.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == sectionsData!.count-1{
            let medicalDetailsObj = self.medicalDictDetails?[0]
            return medicalDetailsObj!.medicalSymptons.count
        }else{
            return sectionsData![section].collapsed ? 0 : sectionsData![section].items.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let medicalDetailsObj = self.medicalDictDetails?[0]
        if indexPath.section == ((self.sectionsData?.count)!-1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath) as! DetailMedicalBodyCell2
            let medicalSympton = medicalDetailsObj!.medicalSymptons[indexPath.row]
            cell.lblTitle?.text = medicalSympton.description
            
            let existImageURL = medicalSympton.pictures
            let urlString = existImageURL.components(separatedBy: ",")
            
            for url in urlString {
                cell.imageThumb.contentMode = .scaleToFill
                let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                cell.imageThumb.loadImageUsingUrlString(urlString: urlString!)
            }
            return cell
        }else{
            let item: ItemInSection = sectionsData![indexPath.section].items[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DetailMedicalBodyCell
            
            if !item.name.trim().isEmpty{
                cell.lblDesc.text = item.name
            }else{
                cell.lblDesc.text = "Đang cập nhật dữ liệu"
            }
            cell.lblDesc.lineBreakMode = .byWordWrapping
            cell.lblDesc.numberOfLines = 0
            cell.lblDesc.adjustsFontSizeToFitWidth = true
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == ((self.sectionsData?.count)!-1){
            let detailMedicalSymptonVC = DetailMedicalBodyGroup(nibName: "DetailMedicalBodyGroup", bundle: nil)
            let medicalDetailsObj = self.medicalDictDetails?[0]
            let medicalSympton = medicalDetailsObj!.medicalSymptons[indexPath.row]
            if medicalSympton.medicalSymptonID != 0{
                detailMedicalSymptonVC.titleMedicalBody = medicalSympton.name
            detailMedicalSymptonVC.getMedicalSymptonByID(symptonByID:medicalSympton.medicalSymptonID)
                self.navigationController?.pushViewController(detailMedicalSymptonVC, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
}


