//
//  DetailMedicalBodyCell2.swift
//  iCNM
//
//  Created by Mac osx on 8/14/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class DetailMedicalBodyCell2: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageThumb: CustomImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
