//
//  CustomDoctorCollectionViewCell.swift
//  iCNM
//
//  Created by Quang Hung on 7/7/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Cosmos
class CustomDoctorCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageThumb: CustomImageView!
    @IBOutlet weak var isOnlineBtn: UIButton!
    @IBOutlet weak var cosmosStar: CosmosView!
    @IBOutlet weak var lbSpecialize: UILabel!
    @IBOutlet weak var lblDoctorName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
