//
//  CustomNewsCollectionViewCell.swift
//  iCNM
//
//  Created by Quang Hung on 7/7/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class CustomQACollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var view1: UIView!
    
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var lblQuestionContent: UILabel!
    @IBOutlet weak var lblCreateDate: UILabel!
    @IBOutlet weak var lblFullname: UILabel!
    @IBOutlet weak var userAvatar: CustomImageView!
    @IBOutlet weak var view_Title: UIView!
    @IBOutlet weak var doctorAvatar: CustomImageView!
    @IBOutlet weak var userNameDoctor: UILabel!
    @IBOutlet weak var lblQuestionTitle: UILabel!
    @IBOutlet weak var lblCreateDate2: UILabel!
    @IBOutlet weak var lblDoctorAnswer: UILabel!
    @IBOutlet weak var lblSpecialName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
