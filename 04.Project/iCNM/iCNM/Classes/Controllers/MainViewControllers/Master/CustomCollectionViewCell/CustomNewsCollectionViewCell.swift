//
//  CustomNewsCollectionViewCell.swift
//  iCNM
//
//  Created by Quang Hung on 7/7/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class CustomNewsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageThumb: CustomImageView!
    @IBOutlet weak var lblTitleNews: UILabel!
    @IBOutlet weak var lblDepartment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
