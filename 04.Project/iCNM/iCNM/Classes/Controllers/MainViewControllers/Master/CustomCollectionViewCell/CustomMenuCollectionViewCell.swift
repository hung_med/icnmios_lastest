//
//  CustomMenuCollectionViewCell.swift
//  iCNM
//
//  Created by Quang Hung on 7/7/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class CustomMenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgWidthConstr: NSLayoutConstraint!
    @IBOutlet weak var imgHeightConstr: NSLayoutConstraint!

    @IBOutlet weak var viewHeightConstr: NSLayoutConstraint!
    @IBOutlet weak var viewWidthConstr: NSLayoutConstraint!
    
    @IBOutlet weak var lblBottomConstr: NSLayoutConstraint!
    @IBOutlet weak var imageThumb: UIImageView!
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

}
