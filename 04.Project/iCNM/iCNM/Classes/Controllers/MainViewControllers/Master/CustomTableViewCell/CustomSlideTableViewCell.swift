//
//  CustomSlideTableViewCell.swift
//  iCNM
//
//  Created by Mac osx on 8/7/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
@objc protocol CustomSlideTableViewCellDelegate {
    @objc optional func presentServicePackage(serviceID:Int, serviceName:String)
    @objc optional func presentScheduleAppointmentVC()
}
class CustomSlideTableViewCell: UITableViewCell, UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    var countImage:Int = 0
    var listDesc:[String]?
    var listServiceID:[Int]?
    var delegate:CustomSlideTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        activityIndicator.isHidden = true
        listDesc = [String]()
        listServiceID = [Int]()
        Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
    }
    
    func showSlideAnimation(isCheck:Bool, listImages:[String], desc:[String]){
        listDesc = desc
        lblTitle.lineBreakMode = .byWordWrapping
        lblTitle.numberOfLines = 0
        self.scrollView.frame = CGRect(x:0, y:0, width:self.frame.width, height:self.frame.height)
        let scrollViewWidth:Int = Int(self.scrollView.frame.width)
        let scrollViewHeight:Int = Int(self.scrollView.frame.height)
        for i in 0..<Int(listImages.count){
            
            let imageName = listImages[i] as String
            let image = CustomImageView(frame: CGRect(x:scrollViewWidth*i, y:0, width:scrollViewWidth, height:scrollViewHeight))
            
            let url = imageName.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            image.loadImageUsingUrlString(urlString: url!)
            self.scrollView.addSubview(image)
            image.tag = i
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            image.isUserInteractionEnabled = true
            image.addGestureRecognizer(tapGestureRecognizer)
            lblTitle.text = self.listDesc?[0]
        }
        
        self.scrollView.contentSize = CGSize(width:self.scrollView.frame.width * CGFloat(listImages.count), height:self.scrollView.frame.height)
        self.scrollView.delegate = self
        self.pageControl.currentPage = 0
        //activityIndicator.stopAnimating()
        //activityIndicator.hidesWhenStopped = true
    }
    
    func moveToNextPage (){
        if listDesc!.count > 0{
            let pageWidth:Int = Int(self.scrollView.frame.width)
            let maxWidth:Int = pageWidth * countImage
            let contentOffset:Int = Int(self.scrollView.contentOffset.x)
            
            var slideToX = contentOffset + pageWidth
            if  contentOffset + pageWidth == maxWidth{
                slideToX = 0
            }
            self.scrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:Int(self.scrollView.frame.height)), animated: true)
        }
    }
    
    //MARK: UIScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage+1)
        if self.pageControl.currentPage >= (self.listDesc?.count)!{
            return
        }
        lblTitle.text = self.listDesc?[Int(currentPage+1)]
        lblTitle.lineBreakMode = .byWordWrapping
        lblTitle.numberOfLines = 0
    }
    
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        let tag = tappedImage.tag
        if self.listServiceID?.count != 0{
            let serviceID = self.listServiceID?[tag]
            if serviceID != 0{
                let serviceName = self.listDesc?[tag]
                self.delegate?.presentServicePackage!(serviceID: serviceID!, serviceName: serviceName!)
            }else{
                self.delegate?.presentScheduleAppointmentVC!()
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
