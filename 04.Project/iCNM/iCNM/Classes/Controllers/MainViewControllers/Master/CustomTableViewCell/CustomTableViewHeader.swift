//
//  DetailLookUpCustomHeader.swift
//  iCNM
//
//  Created by Mac osx on 7/18/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class CustomTableViewHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var titleHeader: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var imageArrow: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       // addShadow(cell: self)
    }
}
