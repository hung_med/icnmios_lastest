//
//  PopupBannerViewController.swift
//  iCNM
//
//  Created by Mac osx on 9/7/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import Toaster

class PopupBannerViewController: BaseViewControllerNoSearchBar {
    var currentUser:User? = nil
    @IBOutlet weak var popupBanner: CustomImageView!
    @IBOutlet weak var btnClosePopup: UIButton!
    var urlPopupBanner:String? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        btnClosePopup.isHidden = true
        self.popupBanner.loadImageUsingUrlString(urlString: urlPopupBanner!)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        btnClosePopup.isHidden = false
    }

    @IBAction func closePopupBanner(_ sender: Any) {
        if let userID = self.currentUser?.id{
            self.sendCharity(userID: userID)
        }
    }
    
    func sendCharity(userID:Int){
        let progress = self.showProgress()
        Charity().sendCharity(userID:userID, success: { (data) in
            self.hideProgress(progress)
            self.dismiss(animated: true) {
                Toast(text: "Cảm ơn quý khách hàng đã cài đặt và trải nghiệm ứng dụng iCNM!").show()
            }
            
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }

}
