//
//  CustomLookUpResultCell.swift
//  iCNM
//
//  Created by Mac osx on 7/14/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class CustomMenuCell: UITableViewCell {

    @IBOutlet weak var lblMenuName: UILabel!
    @IBOutlet weak var icon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
