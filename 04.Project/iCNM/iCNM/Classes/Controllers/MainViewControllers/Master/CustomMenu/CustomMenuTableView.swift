//
//  CustomComboxBoxList.swift
//  iCNM
//
//  Created by Mac osx on 8/4/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
protocol CustomMenuTableViewDelegate {
    func goBackMainVC(index:Int)
}

class CustomMenuTableView: UITableViewController {
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    @IBOutlet var myTableView: UITableView!
    var delegate:CustomMenuTableViewDelegate?
    var listMenus:[String]?
    var listImages:[String]?
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(handleLoginSuccess), name: Constant.NotificationMessage.loginSuccess, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleLogout), name: Constant.NotificationMessage.logout, object: nil)
        
        myTableView.delegate = self
        myTableView.dataSource = self
        let nib = UINib(nibName: "CustomMenuCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        
        listMenus = ["Tra cứu kết quả", "Gửi câu hỏi tư vấn", "Đặt lịch hẹn khám"]
        listImages = ["list-alt", "question-circle", "calendar-plus-o"]
        self.myTableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listMenus!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomMenuCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        if let name_menu = listMenus?[indexPath.row]{
            cell.lblMenuName.text = name_menu
        }
        
        if let icon_menu = listImages?[indexPath.row]{
            if indexPath.row == 0{
                let color = UIColor.white
                cell.icon.image = UIImage(named: icon_menu)?.maskWithColor(color: color)
            }else{
                let color = UIColor(hexColor: 0xFF4500, alpha: 1.0)
                cell.icon.image = UIImage(named: icon_menu)?.maskWithColor(color: color)
            }
        }
        
        if indexPath.row == 0{
            cell.backgroundColor = UIColor(hexColor: 0xFF4500, alpha: 1.0)
            cell.lblMenuName.textColor = UIColor.white
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0{
            if currentUser == nil{
                confirmLogin(myView: self)
                return
            }
        }
        self.delegate?.goBackMainVC(index:indexPath.row)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 45
    }
    
    func handleLoginSuccess(aNotification:Notification) {
        let realm = try! Realm()
        currentUser = realm.currentUser()
        let userDefault = UserDefaults.standard
        userDefault.set(true, forKey: "agreedTerms")
        userDefault.synchronize()
    }
    
    func handleLogout(aNotification:Notification) {
        currentUser = nil
        let userDefault = UserDefaults.standard
        if userDefault.bool(forKey: "agreedTerms") {
            userDefault.removeObject(forKey: "agreedTerms")
        }
    }
}
