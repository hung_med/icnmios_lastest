//
//  NewsCommentListViewController.swift
//  iCNM
//
//  Created by Len Pham on 8/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import ESPullToRefresh
import NextGrowingTextView
import RealmSwift
import SideMenu
import DZNEmptyDataSet
import Toaster

class NewsCommentListViewController: BaseViewControllerNoSearchBar{

    @IBOutlet weak var addNewCommentBtn: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputContainerView: UIView!
    @IBOutlet weak var inputContainerViewBottom: NSLayoutConstraint!
    @IBOutlet weak var growingTextView: NextGrowingTextView!
    
    fileprivate var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate var pageNumber = 1
    public var newsModelInput: NewsModel = NewsModel()
    
    var listCommentsData = [UserNewsCommentModel]()
    
    fileprivate var headerScrollView = ESRefreshHeaderAnimator(frame:CGRect.zero)
    fileprivate var footerScrollView = ESRefreshFooterAnimator(frame:CGRect.zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(NewsCommentListViewController.back(sender:)), for: UIControlEvents.touchUpInside)
            
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        // tabbar
        self.tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        title = "Tất cả bình luận"
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        //tableview cell height
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension

        // get data from server
        self.requestData()
        
        // add refresh and load more control
        // add loading text
        headerScrollView.pullToRefreshDescription = "Kéo để làm mới"
        headerScrollView.releaseToRefreshDescription = "Thả để làm mới"
        headerScrollView.loadingDescription = "Đang tải..."
        
        footerScrollView.loadingDescription = "Đang tải..."
        footerScrollView.noMoreDataDescription = ""

        self.tableView.es.addInfiniteScrolling(animator:footerScrollView) { [weak self] in

            if let weakSelf = self {
                weakSelf.pageNumber = weakSelf.pageNumber + 1

                UserNewsCommentModel.getListCommentOfNews(newsId: weakSelf.newsModelInput.newsID, pageNumber: weakSelf.pageNumber, countNumber: 10, success: { (userNewsCommentModelList) in
                    weakSelf.tableView.es.stopLoadingMore()
                    if userNewsCommentModelList.count > 0 {
                        weakSelf.listCommentsData.append(contentsOf: userNewsCommentModelList)
                        weakSelf.tableView.reloadData()
                    } else {
                        weakSelf.tableView.es.noticeNoMoreData()
                    }

                }, fail: { (error, response) in
                    weakSelf.tableView.es.stopLoadingMore()
                    weakSelf.handleNetworkError(error: error, responseData: response.data)
                })
            }
        }

        self.tableView.es.addPullToRefresh(animator: headerScrollView) { [weak self] in

            if let weakSelf = self {
                weakSelf.pageNumber = 1;
                weakSelf.requestData()
            }
        }
       tableView.tableFooterView = UIView()
        
        // inputContainerView
        
        NotificationCenter.default.addObserver(self, selector: #selector(NewsCommentListViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(NewsCommentListViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.growingTextView.layer.cornerRadius = 4
        self.growingTextView.backgroundColor = UIColor(white: 0.9, alpha: 1)
        self.growingTextView.textView.textContainerInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0)
        self.growingTextView.layer.borderColor = UIColor.gray.cgColor
        self.growingTextView.layer.borderWidth = 0.5
        
        self.navigationItem.setRightBarButtonItems([self.addNewCommentBtn], animated: true)
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        currentUser = {
            let realm = try! Realm()
            return realm.currentUser()
        }()
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
   
    override func requestData() {
        self.listCommentsData.removeAll()
        self.tableView.reloadData()
        // show progress
        let activityIndicator = self.showProgress()
        // group api
        UserNewsCommentModel.getListCommentOfNews(newsId: self.newsModelInput.newsID, pageNumber: self.pageNumber, countNumber: 10, success: { (userNewsCommentModelList) in
            self.hideProgress(activityIndicator)

            self.listCommentsData = userNewsCommentModelList
            self.tableView.es.stopPullToRefresh()
            self.tableView.es.stopLoadingMore()
            self.tableView.reloadData()
           
            
        }) { (error, response) in
            self.hideProgress(activityIndicator)

            self.tableView.es.stopPullToRefresh()
            self.tableView.es.stopLoadingMore()

            self.handleNetworkError(error: error, responseData: response.data)
        }
    }
}

extension NewsCommentListViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listCommentsData.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let strIdentifier : String = "NewsCommentTableViewCellId"
        let cell = tableView.dequeueReusableCell(withIdentifier: strIdentifier, for: indexPath) as! NewsCommentTableViewCell
        let userNewsCommentModel : UserNewsCommentModel? = listCommentsData[indexPath.row]
        cell.thumbImageView.layer.borderColor = UIColor(hexColor: 0x1D6EDC, alpha: 1.0).cgColor
        cell.thumbImageView.layer.borderWidth = 2.0
        
        let avatar = userNewsCommentModel?.user?.avatar
        if avatar == nil{
            cell.thumbImageView.image = UIImage(named:"avatar_default")
        }else{
            if let commentUser = userNewsCommentModel?.user, let avatarURL = commentUser.getAvatarURL() {
                cell.thumbImageView.loadImageUsingUrlString(urlString: avatarURL.absoluteString)
                cell.thumbImageView.contentMode = .scaleAspectFill
            }
        }
        
        if let tempName = userNewsCommentModel?.user?.name {
            cell.nameLabel.text = tempName
        }
        
        if let tmpDateTime = userNewsCommentModel?.newsCommentModel?.dateCreate {
            //get time to server
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            var date: Date? = dateFormatter.date(from: tmpDateTime)
            
            if date == nil {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                date = dateFormatter.date(from: tmpDateTime)
            }
            
            if let dateTime = date?.timeAgoSinceNow(){
                cell.timesLabel.text  = "\(dateTime)"
            }
        }
        
        if let tempContent = userNewsCommentModel?.newsCommentModel?.comment {
            cell.commentLabel.text = tempContent
        }

        //custom UI
        cell.thumbImageView.layer.cornerRadius = cell.thumbImageView.frame.size.width/2;
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userNewsCommentModel: UserNewsCommentModel? = listCommentsData[indexPath.row]
        let userInfo:UserInfo = (userNewsCommentModel?.user)!
        self.showProfileCurrentUser(user: userInfo )
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 5
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func showProfileCurrentUser(user:UserInfo){
        let profileDoctorVC = ProfileDoctorVC(nibName: "ProfileDoctorVC", bundle: nil)
        let featuredDoctor = FeaturedDoctor()
        featuredDoctor.userInfo = user
        featuredDoctor.doctor = nil
        featuredDoctor.specialists = nil
        profileDoctorVC.featuredDoctor = featuredDoctor
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(profileDoctorVC, animated: true)
    }
}

extension NewsCommentListViewController: UITextViewDelegate {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func handleSendButton(_ sender: AnyObject) {
      
       if let user = currentUser {
            if self.growingTextView.textView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty {
                let alertViewController = UIAlertController(title: "Thông báo", message: "Bạn chưa nhập nội dung bình luận", preferredStyle: .alert)
                let noAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                alertViewController.addAction(noAction)
                self.present(alertViewController, animated: true, completion: nil)
                return
            }
        
            UserNewsCommentModel.addNewComment(newsId: self.newsModelInput.newsID, userId:user.id, contentComment:self.growingTextView.textView.text
                    , success: { (isSuccess) in
                if isSuccess {
                    self.pageNumber = 1
                    Toast(text: "Đã bình luận thành công").show()
                    self.requestData()
                }
            }, fail: { (error, response) in
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
        else {
            self.showRemindGoToLogin()
        }
    
        //reset textview & hide keyboard
        self.growingTextView.textView.text = ""
        self.view.endEditing(true)
    }
    
    @IBAction func handleCancelButton(_ sender: AnyObject) {
        self.growingTextView.textView.text = ""
        self.view.endEditing(true)
    }
    
    // Dispose of any resources that can be recreated.
    @IBAction func touchOnWriteNewComment(_ sender: UIBarButtonItem) {
        if  currentUser != nil {
            self.growingTextView.delegate = self;
            self.growingTextView.becomeFirstResponder()
        }
        else {
            self.showRemindGoToLogin()
        }
    
    }
    
    func keyboardWillHide(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let _ = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                self.inputContainerViewBottom.constant =  -1000
                UIView.animate(withDuration: 0.25, animations: { () -> Void in self.view.layoutIfNeeded() })
            }
        }
    }
    
    
    func keyboardWillShow(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                self.inputContainerViewBottom.constant = keyboardHeight
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    func removeTrailingSpaces(aString: String) -> String{
        
        var spaceCount = 0
        for characters in aString{
            if characters == " "{
                spaceCount = spaceCount + 1
            }else{
                break;
            }
        }
        
        var finalString = ""
        let duplicateString = aString.replacingOccurrences(of: " ", with: "")
        while spaceCount != 0 {
            finalString = finalString + " "
            spaceCount = spaceCount - 1
        }
        
        return (finalString + duplicateString)
        
        
    }

}
extension NewsCommentListViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    func backgroundColor(forEmptyDataSet scrollView: UIScrollView) -> UIColor  {
        return UIColor.white
    }

    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {

        let para = NSMutableAttributedString.init(string: "Hiện tại chưa có bình luận nào cho bài viết này")
        let paragraphStyle = NSMutableParagraphStyle.init()
        paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
        paragraphStyle.alignment = NSTextAlignment.center;
        para.addAttributes([NSParagraphStyleAttributeName: paragraphStyle], range: NSRange(location: 0,length: para.length))

        return para
    }

    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let font =  UIFont.systemFont(ofSize:18.0)
        let textFont = [NSFontAttributeName:font, NSForegroundColorAttributeName:UIColor.white]
        let attrString = NSAttributedString(string: "Để lại bình luận ngay", attributes:textFont)
        return attrString
    }

    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> UIImage! {
        guard let image = UIImage.init(named: "button_background_foursquare_highlight")
        else {
            return nil
        }
        let capInsets = UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0);
        return image.resizableImage(withCapInsets: capInsets, resizingMode: UIImageResizingMode.stretch)
    }

    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        if  currentUser != nil {
            self.growingTextView.delegate = self;
            self.growingTextView.becomeFirstResponder()
        }
        else {
            self.showRemindGoToLogin()
        }
    }
}

 


