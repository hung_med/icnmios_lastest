//
//  NewsDetailViewController.swift
//  iCNM
//
//  Created by Mac osx on 7/12/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import NextGrowingTextView
import ESPullToRefresh
import SideMenu
import Social
import RealmSwift
import FontAwesomeKit
import Toaster

class NewsDetailViewController: BaseViewControllerNoSearchBar, UIWebViewDelegate, UINavigationControllerDelegate {
    // MARK: UI
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var shareViewForTap: UIView!
    @IBOutlet weak var likeViewForTap: UIView!
    @IBOutlet weak var commentViewForTap: UIView!
    @IBOutlet weak var replyViewForTap: UIView!
    
    @IBOutlet weak var inputContainerView: UIView!
    @IBOutlet weak var inputContainerViewBottom: NSLayoutConstraint!
    @IBOutlet weak var growingTextView: NextGrowingTextView!
    
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentsButton: UIButton!
    @IBOutlet weak var replyButton: UIButton!
    @IBOutlet weak var bannerAds: UIButton!
    fileprivate var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    // MARK:  data
    private var selectedIndex = -1
    public var newsModelInput = NewsModel()
    public var newsCategoryInput = NewsCategory()
    public var newsSearch = NewsCategories()
    fileprivate var newsIsLike: Bool = false
    fileprivate var numberSections: Int = 0

    fileprivate var newsModelDetail = NewsModel()
    fileprivate var newsRelatedArr = [NewsModel]()
    fileprivate var newsCommentArr =  [UserNewsCommentModel]()
    fileprivate var contentWebView =  NewsDetailWebView()
    var isCheck:Bool? = false
    // add loading text
    fileprivate var headerScrollView = ESRefreshHeaderAnimator(frame:CGRect.zero)
    var lastContentOffset: CGFloat = 0
    // MARK: life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // tabbar
        self.tabBarController?.tabBar.isHidden = true
        
        //naigation bar
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        
        // add action for bottom bar
        self.addActionForBottomBar()
        self.setUpUIForBottomBar()
        
        self.bannerAds.titleLabel?.font = UIFont.fontAwesome(ofSize: 18.0)
        let titleBtn = String.fontAwesomeIcon(name: .calendarPlusO) +  " Đặt lịch hẹn ngay"
        self.bannerAds.setTitle(titleBtn, for: .normal)
        self.bannerAds.isHidden = true
        //add loading text
        headerScrollView.pullToRefreshDescription = "Kéo để làm mới"
        headerScrollView.releaseToRefreshDescription = "Thả để làm mới"
        headerScrollView.loadingDescription = "Đang tải..."
        // add refresh control
        self.tableView.es.stopPullToRefresh()
        self.tableView.es.addPullToRefresh(animator: headerScrollView) { [weak self] in
            if let weakSelf = self {
                weakSelf.requestData()
            }
        }
        
        // automation dimension for table view
        self.tableView.estimatedRowHeight = 44
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.separatorStyle = .singleLine
        self.tableView.tableFooterView = UIView()
       // UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(NewsDetailViewController.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }else{
            let button: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            button.setImage(UIImage(named: "angle-left"), for: UIControlState.normal)
            button.addTarget(self, action: #selector(NewsDetailViewController.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        if let titleNav = newsCategoryInput.name{
            title = titleNav
        }
        
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleLoginSuccess), name: Constant.NotificationMessage.loginSuccess, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleLogout), name: Constant.NotificationMessage.logout, object: nil)
        
        //add notification keyboard hidden/show
        NotificationCenter.default.addObserver(self, selector: #selector(NewsDetailViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(NewsDetailViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.growingTextView.layer.cornerRadius = 4
        self.growingTextView.layer.borderColor = UIColor.gray.cgColor
        self.growingTextView.layer.borderWidth = 0.5
        self.growingTextView.backgroundColor = UIColor(white: 0.9, alpha: 1)
        self.growingTextView.textView.textContainerInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0)
       // self.getResultAds(typeID: 7)
        let categoryID = self.newsCategoryInput.id
        self.checkTrackingFeature(categoryID:categoryID)
      //  self.navigationItem.setRightBarButtonItems(nil, animated: true)
    }
    
    func checkTrackingFeature(categoryID:Int){
        if currentUser != nil{
            let userDefaults = UserDefaults.standard
            let token = userDefaults.object(forKey: "token") as! String
            TrackingFeature().checkTrackingFeature(fcm:token, featureName:Constant.NEWS_DETAIL, categoryID:categoryID,  success: {(result) in
                if result != nil{
                    print("flow track done:", Constant.NEWS_DETAIL)
                }else{
                    print("flow track fail:", Constant.NEWS_DETAIL)
                }
            }, fail: { (error, response) in
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }else{
            return
        }
    }
    
    func sendTrackingNewsID(userID:Int, newsID:Int, deviceType:String){
        let progress = self.showProgress()
        TrackingNewsID().sendTrackingNewsID(userID:userID, newsID:newsID, deviceType:deviceType, success: { (data) in
            self.hideProgress(progress)
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }

    @IBAction func handlerScheduleAppointment(_ sender: Any) {
        let checkNoti = Int(self.newsModelDetail.checkNoti.trim())
        if checkNoti == 0{
            //dat lich
            if currentUser != nil {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let scheduleAnAppointmentVC = storyboard.instantiateViewController(withIdentifier: "ScheduleAppointmentVC") as! SAViewController
                self.tabBarController?.tabBar.isHidden = true
                self.navigationController?.pushViewController(scheduleAnAppointmentVC, animated: true)
            }else{
                confirmLogin(myView: self)
                return
            }
        }else{
            //dat goi kham
            let servicePackageDetailVC = ServicePackageDetailVC(nibName: "ServicePackageDetailVC", bundle: nil)
            servicePackageDetailVC.isCheck = true
            servicePackageDetailVC.getAllServiceDetailGroups(serviceID:checkNoti!)
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(servicePackageDetailVC, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Chi tiết tin tức")
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        navigationController?.setNavigationBarHidden(false, animated: true)
        //load data
        self.requestData()
    }
    
    func back(sender: UIBarButtonItem) {
        let result = self.navigationController?.backToViewController(viewController: SearchVCViewController.self)
        if !result!{
            self.navigationController?.popToRootViewController(animated: true)
        }else{
            
        }
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        if self.isMovingFromParentViewController {
//
//        }
//    }
//
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.bannerAds.frame = CGRect(x: self.bannerAds.frame.origin.x, y: screenSizeHeight-44-116, width:self.bannerAds.frame.width, height: 44)
       // self.btnCloseAds.frame = CGRect(x: screenSizeWidth-20, y: bannerAds.frame.origin.y, width: 20, height: 20)
    }
    
    //MARK webview delegate
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        contentWebView.webViewHeightConstraint.constant = webView.scrollView.contentSize.height
        contentWebView.webView.layoutIfNeeded()
        contentWebView.frame.size.height =  webView.scrollView.contentSize.height + 30
        self.tableView.tableHeaderView = contentWebView
        self.tableView.es.stopPullToRefresh()
        self.numberSections = 2
        self.tableView.reloadData()
        
        let checkNoti = self.newsModelDetail.checkNoti
        if !checkNoti.trim().isEmpty{
            //show button Datlich
            self.bannerAds.isHidden = false
            if self.isCheck!{
                let userID = self.currentUser?.id
                let newsID = self.newsModelDetail.newsID
                self.sendTrackingNewsID(userID: userID!, newsID: newsID, deviceType: "IOS")
            }
        }else{
            //hidden button Datlich
            self.bannerAds.isHidden = true
            //   self.btnCloseAds.isHidden = false
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5, animations: {
            self.bannerAds.alpha = 1
        }, completion: {
            finished in
            self.bannerAds.isHidden = false
        })
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5, animations: {
            self.bannerAds.alpha = 0
        }, completion: {
            finished in
            self.bannerAds.isHidden = true
        })
    }

    // Refresh data
    override func requestData() {
        //refresh data
        self.newsModelDetail = NewsModel()
        self.newsRelatedArr.removeAll()
        self.newsCommentArr.removeAll()
        
        // show progress
        let activityIndicator = self.showProgress()
        // group api
        let callApiGroup = DispatchGroup()
        // get news detail
        callApiGroup.enter()
        NewsModel.getNewsDetail(newsId: newsModelInput.newsID, success: { (newsModel) in
            self.newsModelDetail = newsModel
            callApiGroup.leave()

        }, fail: { (error, response) in
            callApiGroup.leave()
            self.tableView.es.stopPullToRefresh()
            self.handleNetworkError(error: error, responseData: response.data)
        })
        
        // get relate news
        callApiGroup.enter()
        NewsModel.getRelatedNews(newsId: newsModelInput.newsID, specialistID: newsCategoryInput.id, success: { (newsRelateList) in
            self.newsRelatedArr = newsRelateList
            callApiGroup.leave()
        }, fail: { (error, response) in
            callApiGroup.leave()
            self.tableView.es.stopPullToRefresh()
            self.handleNetworkError(error: error, responseData: response.data)
        })
        
        // get list comment of news
        callApiGroup.enter()
        UserNewsCommentModel.getListCommentOfNews(newsId: newsModelInput.newsID, pageNumber: 1, countNumber: 5, success: { (userNewsCommentList) in
            self.newsCommentArr = userNewsCommentList
            callApiGroup.leave()
        }, fail: { (error, response) in
            callApiGroup.leave()
            self.tableView.es.stopPullToRefresh()
            self.handleNetworkError(error: error, responseData: response.data)
        })
        
        callApiGroup.enter()
        if let user = currentUser {
            NewsModel.getLikeNews(newsId: newsModelInput.newsID, userId: user.id, success: { (isLike) in
                self.newsIsLike = isLike
                callApiGroup.leave()
                
            }, fail: { (error, response) in
                callApiGroup.leave()
                self.tableView.es.stopPullToRefresh()
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
        else {
             callApiGroup.leave()
        }
        
        NewsModel.postUpdateRead(newsId: self.newsModelInput.newsID, success: { (result) in
            print("update number read: ", self.newsModelInput.newsID )
        }, fail: { (error, response) in
            self.tableView.es.stopPullToRefresh()
            self.handleNetworkError(error: error, responseData: response.data)
        })
        
        // when all api get complete
        callApiGroup.notify(queue: DispatchQueue.main, execute: {
            
            self.hideProgress(activityIndicator)
            
            // update like ui
            self.setUpUIForLike(isLike: self.newsIsLike)
            if IS_IPAD{
                self.reloadWebView(contentTextSize: 33, titlePageSize: 45)
            }else{
                self.reloadWebView(contentTextSize: 30, titlePageSize: 45)
            }
            
        })
    }
    
    
    func reloadWebView(contentTextSize:CGFloat, titlePageSize:CGFloat)  {
    //show webview
    //get time from server and convert to date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        var date = dateFormatter.date(from: self.newsModelDetail.datePhan)
        if date == nil {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            date = dateFormatter.date(from: self.newsModelDetail.datePhan)
        }
        
        if (date == nil) {
            return
        }
        
        dateFormatter.dateFormat = "EEEE-dd/MM/yyyy HH:mm:ss"
        dateFormatter.timeZone = TimeZone.ReferenceType.local
        let dateStr = dateFormatter.string(from: date!)
        
        let fr = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40);
        self.contentWebView = NewsDetailWebView.init(frame: fr)
        self.contentWebView.webView.delegate = self
        let pageHtml = "<html>\n <head>\n <style type=\"text/css\">\n titleText { font-size: \(titlePageSize)pt; color:#ec1313;}\n contentText { font-size: \(contentTextSize)pt;}\n .fixed-ratio-resize {width: 100%;height: auto;}\n body{font-family: '-apple-system','OpenSans';font-size: \(contentTextSize)pt;}\n </style>\n </head></br> <body><contentText>\(dateStr)<contentText></br> <titleText><b>\(self.newsModelDetail.title)</b></titleText></br><contentText><contentText>\(self.newsModelDetail.newsContent)<contentText></body> </html>"
        self.contentWebView.htmlContent = pageHtml

    }
    
    
    @IBAction func clickOnChangeFontSizeBarBtn(_ sender: Any) {
       var alert:UIAlertController? = nil
        if IS_IPAD{
            alert = UIAlertController(title: "Tuỳ chọn", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        }else{
            alert = UIAlertController(title: "Tuỳ chọn", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        }
        
        // add the actions (buttons)
        alert?.addAction(UIAlertAction(title: "Phóng to cỡ chữ", style: UIAlertActionStyle.default, handler: { action in
            if IS_IPAD{
                self.reloadWebView(contentTextSize: 48, titlePageSize: 67)
            }else{
                self.reloadWebView(contentTextSize: 45, titlePageSize: 67)
            }
            
            
        }))
        
        alert?.addAction(UIAlertAction(title: "Thu nhỏ cỡ chữ", style: UIAlertActionStyle.default, handler: { action in
            if IS_IPAD{
                self.reloadWebView(contentTextSize: 33, titlePageSize: 45)
            }else{
                self.reloadWebView(contentTextSize: 30, titlePageSize: 45)
            }
        }))
        
        alert?.addAction(UIAlertAction(title: "Huỷ", style: UIAlertActionStyle.destructive, handler: { action in
            
        }))
        
        
        self.present(alert!, animated: true, completion: nil)
    }
    
    func handleLoginSuccess(aNotification:Notification) {
        let realm = try! Realm()
        currentUser = realm.currentUser()
    }
    
    func handleLogout(aNotification:Notification) {
        currentUser = nil
    }
}

extension NewsDetailViewController:UITableViewDelegate, UITableViewDataSource {

    // MARK: - Table view data source
     func numberOfSections(in tableView: UITableView) -> Int {
        return self.numberSections
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
      
        case 0:
            return newsRelatedArr.count
        case 1:
            return newsCommentArr.count
        default:
            return 0
        }
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var strIdentifier : String = String()
        
        switch indexPath.section {
            case 0:
                strIdentifier = "NewsRelatedTableViewCell"
                let cell = tableView.dequeueReusableCell(withIdentifier: strIdentifier, for: indexPath) as! NewsRelatedTableViewCell
                
                if self.newsRelatedArr.count != 0{
                    let newsModel : NewsModel? = self.newsRelatedArr[indexPath.row]
                    
                    if let tmpImageName = newsModel?.img {
                        let urlString = tmpImageName.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                        cell.thumbImageView.contentMode = .scaleAspectFill
                        cell.thumbImageView.loadImageUsingUrlString(urlString: urlString!)
                    }
                    
                    if let tmpTitle = newsModel?.title {
                        cell.titleLabel.text = tmpTitle
                    }
                    
                    if let tmpDesc = newsModel?.description {
                        cell.descriptionLabel.text = tmpDesc
                    }
                    
                    if let tmpDateTime = newsModel?.datePhan {
                        //get time to server
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                        var date = dateFormatter.date(from: tmpDateTime)
                        if date == nil {
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                            date = dateFormatter.date(from: tmpDateTime)
                        }
                        
                        if let dateTime = date?.timeAgoSinceNow(){
                            cell.timesLabel.text  = "\(dateTime)"
                        }
                        
                    }
                    
                    cell.viewForTap.onTap({ (gesture) in
                        let newsDetailVC = StoryboardScene.Main.newsDetailViewController.instantiate()
                        newsDetailVC.newsCategoryInput = self.newsCategoryInput
                        newsDetailVC.newsModelInput = newsModel!
                        self.navigationController?.pushViewController(newsDetailVC, animated: true)
                    })
                }
              
                return cell
         
        case 1:
            
            strIdentifier = "NewsCommentTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: strIdentifier, for: indexPath) as! NewsCommentTableViewCell
            //custom UI
            cell.thumbImageView.layer.cornerRadius = cell.thumbImageView.frame.size.width/2;
            cell.thumbImageView.layer.borderColor = UIColor(hexColor: 0x1D6EDC, alpha: 1.0).cgColor
            cell.thumbImageView.layer.borderWidth = 2.0
            // fill cell data
            let userNewsCommentModel : UserNewsCommentModel? = self.newsCommentArr[indexPath.row]
            let avatar = userNewsCommentModel?.user?.avatar
            if avatar == nil{
                cell.thumbImageView.image = UIImage(named:"avatar_default")
            }else{
                if let commentUser = userNewsCommentModel?.user, let avatarURL = commentUser.getAvatarURL() {
                    cell.thumbImageView.loadImageUsingUrlString(urlString: avatarURL.absoluteString)
                    cell.thumbImageView.contentMode = .scaleAspectFill
                }
            }

            if let tempName = userNewsCommentModel?.user?.name {
                cell.nameLabel.text = tempName
            }
            
            if let tmpDateTime = userNewsCommentModel?.newsCommentModel?.dateCreate {
                //get time to server
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                var date = dateFormatter.date(from: tmpDateTime)
                if date == nil {
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                    date = dateFormatter.date(from: tmpDateTime)
                }
                
                if let dateTime = date?.timeAgoSinceNow(){
                    cell.timesLabel.text  = "\(dateTime)"
                }
            }
            
            if let tempContent = userNewsCommentModel?.newsCommentModel?.comment {
                cell.commentLabel.text = tempContent
            }
            
            return cell
        default:
            return  UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
 
        if indexPath.section == 0 {
            return 101
        }
        else {
            return UITableViewAutomaticDimension
        }
    }
    
    // Set the spacing between sections
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 77
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.newsCommentArr.count != 0{
            let comment = self.newsCommentArr[indexPath.row]
            if let userInfo = comment.user{
                self.showProfileCurrentUser(userInfo: userInfo)
            }
        }
    }
    // Make the background color show through
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            let strIdentifier = "TitleOtherNewsCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: strIdentifier)
            return cell
        }
        
        else if section == 1 {
            let strIdentifier = "TitleCommentsCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: strIdentifier)
            return cell
        }
        else  {
            return UIView()
        }
    }
    
    func showProfileCurrentUser(userInfo:UserInfo){
        let profileDoctorVC = ProfileDoctorVC(nibName: "ProfileDoctorVC", bundle: nil)
        let featuredDoctor = FeaturedDoctor()
        featuredDoctor.userInfo = userInfo
        featuredDoctor.doctor = nil
        featuredDoctor.specialists = nil
        profileDoctorVC.featuredDoctor = featuredDoctor
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(profileDoctorVC, animated: true)
    }
 }

//MARK bottom view actions
extension NewsDetailViewController {
    
    func setUpUIForBottomBar() -> Void {
        self.setUpUIForLike(isLike: false)
        self.setUpUIForShare()
        self.setUpUIForComment()
        self.setUpUIForReply()
    }
    
    func setUpUIForLike(isLike: Bool) -> Void {

        if isLike {
            self.likeButton.setImage(UIImage(named: "icon_liked"), for: UIControlState.normal)
        }
        else {
            self.likeButton.setImage(UIImage(named: "icon_unlike"), for: UIControlState.normal)
        }
    }
    
    func setUpUIForShare() -> Void {

        self.shareButton.setAttributedTitle(nil, for: .normal)
        self.shareButton.setTitleColor(UIColor(hex:"9D9D9D"), for: .normal)
        self.shareButton.setTitle("\u{f064}", for: .normal)
    }
    
    func setUpUIForComment() -> Void {
        
        self.commentsButton.setAttributedTitle(nil, for: .normal)
        self.commentsButton.setTitleColor(UIColor(hex:"9D9D9D"), for: .normal)
        self.commentsButton.setTitle("\u{f0e5}", for: .normal)
    }
    
    func setUpUIForReply() -> Void {
        
        self.replyButton.setAttributedTitle(nil, for: .normal)
        self.replyButton.setTitleColor(UIColor(hex:"9D9D9D"), for: .normal)
        self.replyButton.setTitle("\u{f044}", for: .normal)
    }
    
    func addActionForBottomBar() -> Void {
        
        // tap on like view
        likeViewForTap.onTap { (gesture) in
            // call api like this news
            self.callApiLikeThisNews()
            // change like button state
        }
        
        // tap on share view
        shareViewForTap.onTap { (gesture) in
            self.shareOnFacebookApp()
        }
        
        // tap on reply view
        replyViewForTap.onTap { (gesture) in
            // show up view for user enter comment
            self.showViewForUserEnterComment()
        }
        
        // tap on comments view
        commentViewForTap.onTap { (gesture) in
            // go to all comments view
            self.showAllCommentVC()
        }
    }
    
    func shareOnFacebookApp() -> Void {
       
        if let composeVC = SLComposeViewController(forServiceType: SLServiceTypeFacebook) {
            let linkFB = newsModelDetail.linkfb
            let urlApp = URL(string: linkFB)
            composeVC.add(urlApp)
            composeVC.setInitialText("Viết gì đó...")
            self.present(composeVC, animated: true, completion: nil)
        }
    }

    func callApiLikeThisNews() -> Void {
        
        if let user = currentUser {
            NewsModel.postLikeNews(newsId: newsModelInput.newsID, userId: user.id, success: { (isLike) in
                //update UI for btn like = selected or unselected
                if isLike{
                    Toast(text: "Đã thích bài viết").show()
                }else{
                    Toast(text: "Đã ngừng thích bài viết").show()
                }
                self.setUpUIForLike(isLike:isLike)
            }, fail: { (error, response) in
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
        else {
            self.showRemindGoToLogin()
        }
    }
    
    func showViewForUserEnterComment() -> Void {
        self.touchOnWriteNewComment()
    }
    
    func showAllCommentVC() -> Void {
        let commentListVC = StoryboardScene.Main.newsCommentListViewController.instantiate()
        commentListVC.newsModelInput = self.newsModelInput
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(commentListVC, animated: true)
    }
}

//MARK comment action
extension NewsDetailViewController: UITextViewDelegate {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func handleSendButton(_ sender: AnyObject) {
        
       if let user = currentUser {
            // validate comment content
            if self.growingTextView.textView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty {
                
                let alertViewController = UIAlertController(title: "Thông báo", message: "Bạn chưa nhập nội dung bình luận", preferredStyle: .alert)
                let noAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                alertViewController.addAction(noAction)
                self.present(alertViewController, animated: true, completion: nil)
                
                return
            }
            
            // call api add comment
            UserNewsCommentModel.addNewComment(newsId: self.newsModelInput.newsID, userId: user.id, contentComment:self.growingTextView.textView.text , success: { (isSuccess) in
                
            // reload section or show msg thong bao sent thanh  cong
               self.requestData()
                
            }, fail: { (error, response) in
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
        else {
            self.showRemindGoToLogin()
        }
    
        self.growingTextView.textView.text = ""
        self.view.endEditing(true)
    }
    
    
    @IBAction func handleCancelButton(_ sender: AnyObject) {
        self.growingTextView.textView.text = ""
        self.view.endEditing(true)
    }
    
    // Dispose of any resources that can be recreated.
   fileprivate func touchOnWriteNewComment() {
       if  currentUser != nil {
            self.growingTextView.delegate = self;
            self.growingTextView.becomeFirstResponder()
        }
        else {
            self.showRemindGoToLogin()
        }
    }
    
    func keyboardWillHide(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let _ = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                self.inputContainerViewBottom.constant =  -1000
                UIView.animate(withDuration: 0.25, animations: { () -> Void in self.view.layoutIfNeeded() })
            }
        }
    }
    
    func keyboardWillShow(_ sender: Notification) {
        if let userInfo = (sender as NSNotification).userInfo {
            if let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                self.inputContainerViewBottom.constant = keyboardHeight
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                })
            }
        }
    }

}


