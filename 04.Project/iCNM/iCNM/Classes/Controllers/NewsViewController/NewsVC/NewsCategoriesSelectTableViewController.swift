//
//  NewsCategoriesSelectTableViewController.swift
//  iCNM
//
//  Created by Len Pham on 9/2/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import FontAwesome_swift

class NewsCategoriesSelectTableViewController: UITableViewController, UIGestureRecognizerDelegate {
    private var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    var notificationToken: NotificationToken? = nil
    @IBOutlet weak var myTableView:UITableView!
    @IBOutlet weak var myView:UIView!
    @IBOutlet weak var myView2:UIView!
    @IBOutlet weak var lblSuggestTag:UILabel!
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var checkBoxCategories: UIButton!
    var isCheck = false
    var lblDesc:UILabel? = nil
    let categories:Results<NewsCategoryModel> = {
        let realm = try! Realm()
        var userId = 0;
        if let user = realm.currentUser() {
            userId = user.id
        }
        let strPredicate = String(format:"userID == %d",userId)
        return realm.objects(NewsCategoryModel.self).filter(strPredicate)
    }()
    
    @IBAction func btnCategories(_ sender: Any) {
        if isCheck {
            checkBoxCategories.setImage(UIImage(named: "CheckBoxOutline"), for: .normal)
            for category in categories {
                let realm = try! Realm()
                try! realm.write {
                    category.selected = false
                }
            }
            isCheck = false
        } else {
            checkBoxCategories.setImage(UIImage(named: "CheckBoxChecked"), for: .normal)
            for category in categories {
                let realm = try! Realm()
                try! realm.write {
                    category.selected = true
                }
            }
            isCheck = true
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableViewAutomaticDimension
        self.navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.white,
             NSFontAttributeName: UIFont.systemFont(ofSize: 16)]
        NotificationCenter.default.addObserver(self, selector: #selector(handleLoginSuccess), name: Constant.NotificationMessage.loginSuccess, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleLogout), name: Constant.NotificationMessage.logout, object: nil)
        
        notificationToken = categories.observe { [weak self] (changes) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                tableView.reloadData()
                
            case .update(_, let deletions, let insertions, let modifications):
                // Query results have changed, so apply them to the UITableView
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.endUpdates()
            case .error(let error):
                fatalError("\(error)")
            }
        }
        
        // set uncheck box if data nil or check if data not nill
        if categories.count > 0 {
            checkBoxCategories.setImage(UIImage(named: "CheckBoxChecked"), for: .normal)
            isCheck = true
        } else {
            checkBoxCategories.setImage(UIImage(named: "CheckBoxOutline"), for: .normal)
            isCheck = false
        }
        
        lblSuggestTag.font = UIFont.fontAwesome(ofSize: 14.0)
        let tagLabel = String.fontAwesomeIcon(name: .users) + "  Nhập chủ đề bạn quan tâm..."
        lblSuggestTag.text = tagLabel
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(NewsCategoriesSelectTableViewController.selectTagSpecialist))
        gestureRecognizer.delegate = self
        lblSuggestTag.isUserInteractionEnabled = true
        lblSuggestTag.addGestureRecognizer(gestureRecognizer)
    }
    
    func selectTagSpecialist(){
        if self.currentUser != nil{
            let tagSpecialistVC = TagSpecialistVC(nibName: "TagSpecialistVC", bundle: nil)
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(tagSpecialistVC, animated: true)
        }else{
            confirmLogin(myView: self)
        }
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidDisappear(_ animated:Bool){
        super.viewDidDisappear(false)
    }
    
    override func viewWillDisappear(_ animated:Bool){
        super.viewWillDisappear(false)
        let noti = Notification.init(name: Notification.Name("categoryClosed"))
        NotificationCenter.default.post(noti)
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SpecialistCell", for: indexPath) as! SpecialistSelectTableViewCell
        let category = categories[indexPath.row]
        cell.specialistTitle.text = category.name
        if category.selected {
            cell.selectButton.setImage(UIImage(asset: Asset.checkBoxChecked), for: .normal)
        } else {
            cell.selectButton.setImage(UIImage(asset: Asset.checkBoxOutline), for: .normal)
        }
        cell.selectButton.tag = indexPath.row
        cell.selectButton.onTap { (gesture) in
            let realm = try! Realm()
            try! realm.write {
                category.selected = !category.selected
            }
        }
        
        return cell
    }
    
    deinit {
        notificationToken?.invalidate()
    }
    
    func handleLoginSuccess(aNotification:Notification) {
        let realm = try! Realm()
        currentUser = realm.currentUser()
        let userDefault = UserDefaults.standard
        userDefault.set(true, forKey: "agreedTerms")
        userDefault.synchronize()
        
    }
    
    func handleLogout(aNotification:Notification) {
        currentUser = nil
        let userDefault = UserDefaults.standard
        if userDefault.bool(forKey: "agreedTerms") {
            userDefault.removeObject(forKey: "agreedTerms")
        }
    }
    
}
