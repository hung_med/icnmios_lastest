//
//  NewsCategoryListItemsViewController.swift
//  iCNM
//
//  Created by Len Pham on 8/2/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import ESPullToRefresh
import GestureRecognizerClosures
import RealmSwift

class NewsCategoryListItemsViewController: BaseTableViewController {

    private var pageNumber = 1
    var newsCategory:NewsCategory = NewsCategory()
    
    var listNewsData = [NewsModel]()
    
    private var headerScrollView = ESRefreshHeaderAnimator(frame:CGRect.zero)
    private var footerScrollView = ESRefreshFooterAnimator(frame:CGRect.zero)
    
    fileprivate var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //tabbar
        self.tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        title = newsCategory.name
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        // tableview height
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.tableFooterView = UIView()
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(NewsCategoryListItemsViewController.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }else{
            let button: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            button.setImage(UIImage(named: "angle-left"), for: UIControlState.normal)
            button.addTarget(self, action: #selector(NewsCategoryListItemsViewController.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        // get data from server
        self.requestData()
        let categoryID = self.newsCategory.id
        self.checkTrackingFeature(categoryID:categoryID)
        
        footerScrollView.loadingDescription = "Đang tải..."
        footerScrollView.noMoreDataDescription = ""

        self.tableView.es.addInfiniteScrolling(animator:footerScrollView) { [weak self] in
            
            if let weakSelf = self {
                weakSelf.pageNumber = weakSelf.pageNumber + 1
                
                NewsModel.getListNewsOfCategory(pageNumber: weakSelf.pageNumber, countNumber: 5, specialistID: weakSelf.newsCategory.id, success: { (listNewsModel) in
                    weakSelf.tableView.es.stopLoadingMore()
                    if listNewsModel.count > 0 {
                        weakSelf.listNewsData.append(contentsOf: listNewsModel)
                        weakSelf.tableView.reloadData()
                    } else {
                    }

                }, fail: { (error, response) in
                    weakSelf.tableView.es.stopLoadingMore()
                    weakSelf.handleNetworkError(error: error, responseData: response.data)
                })
            }
        }

        tableView.tableFooterView = UIView()
    }
    
    func checkTrackingFeature(categoryID:Int){
        if currentUser != nil{
            let userDefaults = UserDefaults.standard
            let token = userDefaults.object(forKey: "token") as! String
            TrackingFeature().checkTrackingFeature(fcm:token, featureName:Constant.NEWS_DETAIL, categoryID:categoryID,  success: {(result) in
                if result != nil{
                    print("flow track done:", Constant.NEWS_DETAIL)
                }else{
                    print("flow track fail:", Constant.NEWS_DETAIL)
                }
            }, fail: { (error, response) in
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }else{
            return
        }
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func requestData() {
        let activityIndicator = self.showProgress()
        self.pageNumber = 1
        self.listNewsData = [NewsModel]()
        self.tableView.reloadData()
        NewsModel.getListNewsOfCategory(pageNumber: self.pageNumber, countNumber: 5, specialistID: self.newsCategory.id, success: { (listNewsModel) in
            self.hideProgress(activityIndicator)
            self.tableView.es.stopPullToRefresh()
            if listNewsModel.count > 0 {
                self.listNewsData = listNewsModel
                self.tableView.reloadData()

            }
            
        }) { (error, response) in
            self.hideProgress(activityIndicator)
            self.tableView.es.stopPullToRefresh()
            self.tableView.reloadData()
            self.handleNetworkError(error: error, responseData: response.data)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listNewsData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var strIdentifier : String = String()
        
        if indexPath.row == 0 {
            strIdentifier = "NewsCategotyListItemsLargeCell"
        }
        else {
            strIdentifier = "NewsCategotyListItemsThumbailCell"
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: strIdentifier, for: indexPath) as! NewsCategotyListItemsCell
        if indexPath.row == 0 {
            if IS_IPHONE_5{
                cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
            }else{
                cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 17.0)
            }
            cell.titleLabel.layer.shadowColor = UIColor.darkGray.cgColor
            cell.titleLabel.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
            cell.titleLabel.layer.shadowOpacity = 0.8
        }

        let newsModel : NewsModel? = listNewsData[indexPath.row]
        
        if let tmpImageName = newsModel?.img {
            let urlString = tmpImageName.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            cell.thumbImageView.contentMode = .scaleAspectFill
            cell.thumbImageView.loadImageUsingUrlString(urlString: urlString!)
        }
        
        if let tmpTitle = newsModel?.title {
            cell.titleLabel.text = tmpTitle
            cell.titleLabel.lineBreakMode = .byWordWrapping
            cell.titleLabel.numberOfLines = 0
        }
        
        if let tmpContent = newsModel?.description {
            cell.descriptionLabel.text = tmpContent
        }
        
        if let tmpDateTime = newsModel?.datePhan {
            //get time to server
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            var date = dateFormatter.date(from: tmpDateTime)
            
            if date == nil {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                date = dateFormatter.date(from: tmpDateTime)
            }
            
            if let dateTime = date?.timeAgoSinceNow(){
                cell.timesLabel.text = "\(dateTime)"
            }else if let tmpDateTime = newsModel?.dateCreate{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                var date = dateFormatter.date(from: tmpDateTime)
                
                if date == nil {
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    date = dateFormatter.date(from: tmpDateTime)
                }
                
                if let dateTime = date?.timeAgoSinceNow(){
                    cell.timesLabel.text = "\(dateTime)"
                }
            }
        }
        
        cell.viewForTap.onTap { (gestures) in
            let detailNewsVC = StoryboardScene.Main.newsDetailViewController.instantiate()
            detailNewsVC.newsModelInput = newsModel!
            detailNewsVC.newsCategoryInput = self.newsCategory
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(detailNewsVC, animated: true)
        }
        
        return cell

    }
    
    
   override  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableViewAutomaticDimension
        }
        else {
            return 101
        }
    }


}
