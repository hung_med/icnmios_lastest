//
//  NewsHomeViewController.swift
//  iCNM
//
//  Created by Len Pham on 7/28/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import ESPullToRefresh
import GestureRecognizerClosures
import RealmSwift
import SideMenu


 class NewsHomeViewController: BaseViewController, UIScrollViewDelegate{
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var chooseSpeacialistBtn: UIBarButtonItem!
    
    fileprivate var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    fileprivate let reuseIdentifier = "NewsHomeCollectionViewCell"
    fileprivate let sectionInsets = UIEdgeInsets(top: 0, left: 10.0, bottom: 0, right: 10.0)
    
    fileprivate var listCategories : [NewsCategory] = [NewsCategory]()
    
    fileprivate let itemsPerRow: CGFloat = 2
    fileprivate var headerScrollView = ESRefreshHeaderAnimator(frame:CGRect.zero)
    
    var notificationToken: NotificationToken? = nil
    let newsCategorySelectVC = StoryboardScene.Main.newsCategoriesMenuNavigationController.instantiate()
    
    let selectedNewsCategories:Results<NewsCategoryModel> = {
        let realm = try! Realm()
        var userId = 0;
        if let user = realm.currentUser() {
            userId = user.id
        }
        
        let strPredicateUser = String(format:"userID == %d",userId)
        let strPredicateSelected = String(format:"selected == true")
        let strPredicate = NSCompoundPredicate(type: .and, subpredicates: [NSPredicate(format: strPredicateUser), NSPredicate(format: strPredicateSelected)])

        return realm.objects(NewsCategoryModel.self).filter(strPredicate)
    }()

    let newsCategories:Results<NewsCategoryModel> = {
        let realm = try! Realm()
        var userId = 0;
        if let user = realm.currentUser() {
            userId = user.id
        }
        let strPredicateUser = String(format:"userID == %d",userId)
        return realm.objects(NewsCategoryModel.self).filter(strPredicateUser)
    }()
    
    
     //MARK lycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // add noti
        NotificationCenter.default.addObserver(self, selector: #selector(self.postUserCategoryToServer), name: Notification.Name(rawValue: "categoryClosed"), object: nil)
        // show tabbar
        self.tabBarController?.tabBar.isHidden = false
        //navigation bar
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        // get data from server at first time
        self.checkData()
        
        // add loading text
        headerScrollView.pullToRefreshDescription = "Kéo để làm mới"
        headerScrollView.releaseToRefreshDescription = "Thả để làm mới"
        headerScrollView.loadingDescription = "Đang tải..."
        
        // add refresh control
        self.collectionView?.es.addPullToRefresh(animator: headerScrollView) { [weak self] in
            
            if let weakSelf = self {
                weakSelf.checkData()
            }
        }
        
        self.navigationItem.setRightBarButtonItems([self.chooseSpeacialistBtn], animated: true)
        self.checkTrackingFeature()
    }
    
    func checkTrackingFeature(){
        if currentUser != nil{
            let userDefaults = UserDefaults.standard
            let token = userDefaults.object(forKey: "token") as! String
            TrackingFeature().checkTrackingFeature(fcm:token, featureName:Constant.NEWS, categoryID: 0, success: {(result) in
                if result != nil{
                    print("flow track done:", Constant.NEWS)
                }else{
                    print("flow track fail:", Constant.NEWS)
                }
            }, fail: { (error, response) in
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }else{
            return
        }
    }
    
    func checkData() -> Void {
        if self.newsCategories.count == 0 {
            let realm = try! Realm()
            var userId = 0;
            if let user = realm.currentUser() {
                userId = user.id
            }
            
            UserNewsCategories.getAllNewsCategory(userId: userId, success: { (isSuccess) in
                self.requestData()
                
            }, fail: { (error, response) in
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
        else {
            self.requestData()
        }
    }
    
    @IBAction func openLeftMenu(_ sender: UIBarButtonItem) {
        self.openLeftmenu()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Tin tức")
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        //right menu
        SideMenuManager.default.menuRightNavigationController = newsCategorySelectVC
        
        self.tabBarController?.tabBar.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK call api get data from server
    
    override func requestData() {
        listCategories.removeAll()
        self.collectionView.reloadData()
        let activityIndicator = self.showProgress()
        if self.selectedNewsCategories.count == 0  {
            self.hideProgress(activityIndicator)
            self.collectionView.es.stopPullToRefresh()
            self.collectionView.reloadData()
            
            return
        }
        
        let newsCategoryIDs:[Int] = self.selectedNewsCategories.map { $0.id }
        NewsCategory.getListNewsOfAllCategory(categoryIds: newsCategoryIDs, success: { (categories) in
            //add new category
            self.listCategories = categories.sorted(by: { $0.orderNum < $1.orderNum})
            self.hideProgress(activityIndicator)
            self.collectionView.reloadData()
            self.collectionView.es.stopPullToRefresh()
        }, fail: { (error, response) in
            //log error
            Loader.removeLoaderFrom(self.collectionView)
            self.hideProgress(activityIndicator)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
}

// MARK: - UICollectionViewDataSource
extension NewsHomeViewController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return listCategories.count
    }
    
     func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return (listCategories[section].listNews.count - 1)
    }
    
     func collectionView(_ collectionView: UICollectionView,
                                 viewForSupplementaryElementOfKind kind: String,
                                 at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionHeader {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: "NewsHomeHeaderView",
                                                                             for: indexPath) as! NewsHomeHeaderView
            
            headerView.categoryTitleLabel.text = listCategories[indexPath.section].name
            
            let newsCategory: NewsCategory = listCategories[indexPath.section]
            
            if newsCategory.listNews.count > 0 {
                headerView.newDetailForTap.isHidden = false

                let newsModel : NewsModel? = newsCategory.listNews[0]
                
                if let tmpImageName = newsModel?.img {
                    let urlString = tmpImageName.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                    headerView.newsImage.contentMode = .scaleAspectFill
                    headerView.newsImage.layer.cornerRadius = 4.0
                    headerView.newsImage.layer.masksToBounds = true
                    headerView.newsImage.loadImageUsingUrlString(urlString: urlString!)
                }
                
                if let tmpTitle = newsModel?.title {
                    if IS_IPHONE_5{
                        headerView.newsTitleLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
                    }else{
                        headerView.newsTitleLabel.font = UIFont.boldSystemFont(ofSize: 17.0)
                    }
                    headerView.newsTitleLabel.text = tmpTitle
                }

                if let tmpDateTime = newsModel?.datePhan {
                    //get time to server
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    var date = dateFormatter.date(from: tmpDateTime)
                    if date == nil {
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                        date = dateFormatter.date(from: tmpDateTime)
                    }
                    
                    if let dateTime = date?.timeAgoSinceNow(){
                        headerView.newsTimeLabel.text = "\(dateTime)"
                    }
                }
                headerView.newDetailForTap.onTap({ (tapGesture) in
                    let newsDetailVC = StoryboardScene.Main.newsDetailViewController.instantiate()
                    newsDetailVC.newsCategoryInput = newsCategory
                    newsDetailVC.newsModelInput = newsModel!
                    self.navigationController?.pushViewController(newsDetailVC, animated: true)
                })
            }
            else {
                headerView.newDetailForTap.isHidden = true
            }
            
            //add action tap to view more go to NewsCategoryListItemsViewController
            headerView.headerViewForTap.onTap({ (tapGesture) in
                let newsCategoryListItemsVC = StoryboardScene.Main.newsCategoryListItemsViewController.instantiate()
                newsCategoryListItemsVC.newsCategory = newsCategory
                self.navigationController?.pushViewController(newsCategoryListItemsVC, animated: true)
            })
            
            return headerView
        }
        else {
            return UIView() as! UICollectionReusableView
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
    }
    
     func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: reuseIdentifier, for: indexPath) as! NewsHomeCollectionViewCell
        cell.contentView.layer.cornerRadius = 4.0
        cell.contentView.layer.masksToBounds = true
        cell.contentView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell.contentView.layer.borderWidth = 1.0
        
        cell.contentView.layer.shadowOffset = CGSize(width: 0, height:4)
        cell.contentView.layer.shadowColor = UIColor.groupTableViewBackground.cgColor
        cell.contentView.layer.shadowOpacity = 0.5
        cell.contentView.layer.shadowRadius = 2
        if #available(iOS 10, *) {
            // use an api that requires the minimum version to be 10
        } else {
            collectionView.collectionViewLayout.invalidateLayout()
            collectionView.layoutSubviews()
        }
        cell.thumbImageView.layer.cornerRadius = 4.0
        cell.thumbImageView.layer.masksToBounds = true
        cell.thumbImageView.layer.borderColor = UIColor.white.cgColor
        cell.thumbImageView.layer.borderWidth = 1.0
        
        let newsCategory: NewsCategory = listCategories[indexPath.section];
        let newsModel : NewsModel? = newsCategory.listNews[indexPath.row + 1]
        
        if let tmpImageName = newsModel?.img {
            let urlString = tmpImageName.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            cell.thumbImageView.contentMode = .scaleAspectFill
            cell.thumbImageView.loadImageUsingUrlString(urlString: urlString!)
        }
        
        if let tmpTitle = newsModel?.title {
            cell.titleLabel.text = tmpTitle
        }
        
        if let tmpDateTime = newsModel?.datePhan {
            //get time to server
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            var date = dateFormatter.date(from: tmpDateTime)
            
            if date == nil {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                date = dateFormatter.date(from: tmpDateTime)
            }

            if let dateTime = date?.timeAgoSinceNow(){
                cell.timesLabel.text = "\(dateTime)"
            }
        }
        
        cell.viewForTap.onTap({ (tapGesture) in
            let newsDetailVC = StoryboardScene.Main.newsDetailViewController.instantiate()
            newsDetailVC.newsCategoryInput = newsCategory
            newsDetailVC.newsModelInput = newsModel!
            self.navigationController?.pushViewController(newsDetailVC, animated: true)
        })
        
        return cell
    }
    
    @IBAction func touchOnChangeCategory(_ sender: Any) {
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    func postUserCategoryToServer() {
        let realm = try! Realm()
        if let user = realm.currentUser() {
            let newsCategoryIDs:[Int] = self.selectedNewsCategories.map { $0.id }
            let newsCategoryIDsStr = String(format: "%@",(newsCategoryIDs.map{String($0)}).joined(separator: ","))
            
            UserNewsCategories.postUserCategories(userId: user.id, categoryIds: newsCategoryIDsStr, success: { (isSuccess) in
                
            }, fail: { (error, response) in
                //log error
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
        self.checkData()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5, animations: {
            self.tabBarController?.tabBar.isHidden = true
        }, completion: {
            finished in
            self.tabBarController?.tabBar.isHidden = false
        })
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5, animations: {
            self.tabBarController?.tabBar.isHidden = false
        }, completion: {
            finished in
            self.tabBarController?.tabBar.isHidden = true
        })
    }
}

// MARK: - UICollectionViewDelegate
extension NewsHomeViewController: UICollectionViewDelegate {
     func collectionView(_ collectionView: UICollectionView,
                                 didSelectItemAt indexPath: IndexPath) {
    }
}

extension NewsHomeViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if listCategories[indexPath.section].listNews.count > 0 {
            let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
            let availableWidth = view.frame.width - paddingSpace
            let widthPerItem = availableWidth / itemsPerRow
            if IS_IPHONE_5{
                return CGSize(width: widthPerItem, height: widthPerItem + 20)
            }
            return CGSize(width: widthPerItem, height: widthPerItem)
            
        }
        else {
            return CGSize(width: 0, height: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        // ofSize should be the same size of the headerView's label size:
        let newsCategory: NewsCategory = listCategories[section]
        if newsCategory.listNews.count > 0 {
            return CGSize.init(width: collectionView.frame.size.width, height: 400)
        }
        else {
            return CGSize.init(width: collectionView.frame.size.width, height: 60)
        }
    }
}

