//
//  TagSpecialistVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
//protocol TagSpecialistVCDelegate {
//    //func gobackAppointmentListVC(listSelectMedicalSchedule:[TestMedicalSchedule]?, medicalSchedule:[TestMedicalSchedule]?)
//}

class TagSpecialistVC: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate, UIViewControllerTransitioningDelegate,UISearchBarDelegate, UISearchResultsUpdating {
    
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myTableView2: UITableView!
    var listUserTags:[KeyWordTag] = [KeyWordTag]()
    var newBackButton:UIBarButtonItem? = nil
    var filteredTagNames:[KeyWordTag] = [KeyWordTag]()
//    let searchController = UISearchController(searchResultsController: nil)
//    var searchBarButton:UIBarButtonItem? = nil
    var isSearch:Bool? = false
    @IBOutlet weak var myTableviewHeightConstraint: NSLayoutConstraint!
    var listSelectResult:[KeyWordTag] = [KeyWordTag]()
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    var searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 250, height: 18))
    var searchOn : UIBarButtonItem!
    var searchOff : UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.isHidden = true
        title = "Danh sách từ khoá quan tâm"
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 70
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView2.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        myTableView2.scrollIndicatorInsets = UIEdgeInsetsMake(64, 0, 0, 0)
//        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        // Setup the Search Controller
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(TagSpecialistVC.back(sender:)), for: UIControlEvents.touchUpInside)
            newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }else{
            myTableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
            let button: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            button.setImage(UIImage(named: "angle-left"), for: UIControlState.normal)
            button.addTarget(self, action: #selector(TagSpecialistVC.back(sender:)), for: UIControlEvents.touchUpInside)
            newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
//        // Setup the Search Controller
//        searchController.searchBar.delegate = self
//        searchController.searchResultsUpdater = self
//        searchController.searchBar.placeholder = "Tìm theo từ khoá quan tâm"
//        searchController.hidesNavigationBarDuringPresentation = false
//        searchController.delegate = self as? UISearchControllerDelegate
//        searchController.dimsBackgroundDuringPresentation = false
//        myTableviewHeightConstraint.constant = 0
//
//        // show UISearchBar
//        self.navigationItem.titleView = searchController.searchBar
//        self.navigationItem.titleView?.frame = searchController.searchBar.frame
//        searchController.searchBar.becomeFirstResponder()
//        searchController.searchBar.showsBookmarkButton = false
//        searchController.hidesNavigationBarDuringPresentation = false
        
        // create right bar button item
        searchOn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(isSearchOn))
        searchOff = UIBarButtonItem(title: "Xong", style: .plain, target: self, action: #selector(isSearchOff))
        
        self.navigationItem.rightBarButtonItems = [self.searchOn]
        
        // create search bar
        self.tabBarController?.tabBar.isHidden = true
        searchBar.placeholder = "Tìm theo từ khoá quan tâm"
        searchBar.tintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        searchBar.isHidden = true
        searchBar.delegate = self
       
        if let user = self.currentUser{
            self.myTableView.isHidden = true
            self.myTableView2.isHidden = false
            self.getUserTagByUserID(userID: user.id)
        }
    }
    
    func isSearchOn() {
        self.navigationItem.setRightBarButtonItems([self.searchOff], animated: false)
        self.navigationItem.titleView = searchBar
        searchBar.isHidden = false
        searchBar.becomeFirstResponder()
    }
    
    func isSearchOff() {
        searchBar.text = nil
        self.navigationItem.setRightBarButtonItems([self.searchOn], animated: false)
        self.navigationItem.titleView = nil
        if searchBar.text == "" {
            isSearch = false
            self.navigationItem.leftBarButtonItem = newBackButton
            self.myTableView.isHidden = true
            self.myTableView2.isHidden = false
            //call api updateUserTag
            var listUserTags:String? = ""
            if self.listSelectResult.count != 0{
                for item in self.listSelectResult{
                    let keyWordID = item.keyWordID
                    if keyWordID != 0{
                        listUserTags = listUserTags! + keyWordID.description + ","
                    }
                }
            }
            
            self.updateUserTags(userTags: listUserTags!)
            DispatchQueue.main.async {
                self.myTableView2.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        searchController.isActive = true
//        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "Xong"
        if #available(iOS 11.0, *) {
//            let height = (self.navigationController?.navigationBar.frame.size.height)
//            searchController.searchBar.heightAnchor.constraint(equalToConstant: height!-4).isActive = true
//            myTableView.contentInset = UIEdgeInsetsMake(8, 0, 0, 0)
        }
    }
    
    func getUserTagByUserID(userID:Int){
        let progress = self.showProgress()
        UserTag.getUserTagByUserIDFix(userID:userID, success: {  (result) in
            if result != nil{
                DispatchQueue.main.async(execute: {
                    let keywordTag = result?.keywordTag
                    self.listUserTags = keywordTag!
                    self.listUserTags = self.listUserTags.sorted(by: { $0.keyName < $1.keyName })
                    let keywordTagUser = result?.userKeywordTags
                    
                    self.showTagByUserID(tag:keywordTag!, userTag:keywordTagUser!)
                    self.hideProgress(progress)
                })
            }else{
                let lblWarning = createUILabel()
                self.myTableView.addSubview(lblWarning)
            }
        }, fail: { (error, response) in
            
        })
    }
    
    func showTagByUserID(tag:[KeyWordTag], userTag:[UserKeyWord]){
        for i in 0..<tag.count {
            let tagObj = tag[i] as KeyWordTag
            if userTag.count > 0{
                if userTag.contains( where: { $0.tagID == tagObj.keyWordID } ) {
                    self.listSelectResult.append(tagObj)
                    myTableviewHeightConstraint.constant = 0
                    myTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
                    if #available(iOS 11.0, *) {
                        self.myTableView2.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
                    }else{
                        self.myTableView2.contentInset = UIEdgeInsetsMake(62, 0, 0, 0)
                    }
                    
                    self.myTableView2.setEditing(true, animated: false)
                    self.myTableView2.reloadData()
                }
            }else{
                
            }
        }
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredTagNames = listUserTags.filter({( tagObj : KeyWordTag) -> Bool in
            return tagObj.keyName.lowercased().contains(searchText.lowercased())
        })
        
        if searchText.isEmpty{
            self.myTableView.isHidden = true
        }
        if filteredTagNames.count != 0 && !searchText.isEmpty{
            self.myTableView.setEditing(true, animated: false)
            self.myTableView.isHidden = false
            myTableviewHeightConstraint.constant = 0
            if #available(iOS 11.0, *) {
                myTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
            }else{
                myTableView.contentInset = UIEdgeInsetsMake(62, 0, 0, 0)
            }
            
            self.myTableView.reloadData()
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // code to check for a particular tableView.
        if tableView == myTableView{
            if self.filteredTagNames.count != 0 {
                return self.filteredTagNames.count
            }
            return self.listUserTags.count
        }else{
            return self.listSelectResult.count
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == myTableView{
            if self.filteredTagNames.count != 0 {
                return 1
            }else{
                return 0
            }
        }else{
            if self.listSelectResult.count != 0{
                return 1
            }else{
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header : UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.backgroundView?.backgroundColor = UIColor.groupTableViewBackground
        //header.textLabel?.textColor = UIColor.red
    }
    
    // Section Title
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
        if tableView == myTableView{
            let count = self.filteredTagNames.count
            return "Kết quả tìm kiếm" + " (\(count))"
        }else{
            if self.listSelectResult.count != 0{
                return "Từ khoá bạn quan tâm: (\(self.listSelectResult.count))"
            }else{
                return ""
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if (cell == nil) {
            cell = UITableViewCell(style:UITableViewCellStyle.subtitle, reuseIdentifier:"Cell")
        }
        if tableView == myTableView{
            var tagObj: KeyWordTag?
            DispatchQueue.main.async(execute: {
                if self.filteredTagNames.count != 0{
                    tagObj = (self.filteredTagNames[indexPath.row])
                    cell?.textLabel?.text = tagObj?.keyName
                    cell?.textLabel?.font = UIFont.fontAwesome(ofSize: 14.0)
                    cell?.textLabel?.lineBreakMode = .byWordWrapping
                    cell?.textLabel?.numberOfLines = 0
                    cell?.textLabel?.adjustsFontSizeToFitWidth = true
                }else{
                    tagObj = self.listUserTags[indexPath.row]
                    cell?.textLabel?.text = tagObj?.keyName
                    cell?.textLabel?.font = UIFont.fontAwesome(ofSize: 14.0)
                    cell?.textLabel?.lineBreakMode = .byWordWrapping
                    cell?.textLabel?.numberOfLines = 0
                }
            })
        }else{
            let tagObj = self.listSelectResult[indexPath.row]
            cell?.textLabel?.text = tagObj.keyName
            cell?.textLabel?.font = UIFont.fontAwesome(ofSize: 14.0)
            cell?.textLabel?.lineBreakMode = .byWordWrapping
            cell?.textLabel?.numberOfLines = 0
            cell?.textLabel?.adjustsFontSizeToFitWidth = true
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isSearch = true
        if self.filteredTagNames.count != 0{
            let tagObj = self.filteredTagNames[indexPath.row]
            if !self.listSelectResult.contains(where: { $0.keyWordID == tagObj.keyWordID}) {
                self.listSelectResult.append(tagObj)
            }else{
                tableView.deselectRow(at: indexPath, animated: false)
                showAlertView(title: "Từ khoá này đã chọn", view: self)
                return
            }
        }
        myTableviewHeightConstraint.constant = 0
        if #available(iOS 11.0, *) {
            self.myTableView2.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
            myTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        }else{
            self.myTableView2.contentInset = UIEdgeInsetsMake(62, 0, 0, 0)
            myTableView.contentInset = UIEdgeInsetsMake(62, 0, 0, 0)
        }
        self.myTableView2.setEditing(true, animated: false)
        self.myTableView2.reloadData()
        
        DispatchQueue.main.async {
            if self.listSelectResult.count != 0{
                let indexPath = IndexPath(row: self.listSelectResult.count-1, section: 0)
                self.myTableView2.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    
    private func tableView(tableView: UITableView,
                           editingStyleForRowAtIndexPath indexPath: IndexPath)
        -> UITableViewCellEditingStyle {
            return UITableViewCellEditingStyle.init(rawValue: 3)!
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if tableView == myTableView2{
            if editingStyle == .delete{
               // let tagObj = self.listSelectResult[indexPath.row]
               // self.filteredTagNames.insert(tagObj, at: 0)
                
                self.listSelectResult.remove(at: indexPath.row)
                if self.listSelectResult.count == 0{
                    myTableviewHeightConstraint.constant = 0
                    if #available(iOS 11.0, *) {
                        myTableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
                    }else{
                        myTableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
                    }
                }
                self.myTableView2.reloadData()
                //self.myTableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension//Choose your custom row height
    }

    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            isSearch = false
            view.endEditing(true)
            DispatchQueue.main.async {
                self.myTableView2.reloadData()
            }
        }
        else {
            isSearch = true
            filterContentForSearchText(searchText)
            DispatchQueue.main.async {
                self.myTableView2.reloadData()
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!)
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
        
        if searchController.isActive{
            self.navigationItem.leftBarButtonItem = nil
        }else{
            self.navigationItem.leftBarButtonItem = newBackButton
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar:UISearchBar) {

    }
    
    func updateUserTags(userTags:String){
        let progress = self.showProgress()
        if let user = currentUser{
            UserTag().updateUserTags(userID:user.id, userTag: userTags, success: {(result) in
                if result != nil{
                    self.hideProgress(progress)
                }else{
                    self.hideProgress(progress)
                }
            }, fail: { (error, response) in
                self.hideProgress(progress)
            })
        }
    }
    
    func didPresentSearchController(searchController: UISearchController) {
        searchController.searchBar.becomeFirstResponder()
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
//        searchController.searchBar.tintColor = UIColor.gray
        return true
    }
}

