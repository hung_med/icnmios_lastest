//
//  NewHomeHeaderView.swift
//  iCNM
//
//  Created by Len Pham on 7/28/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class NewsHomeHeaderView: UICollectionReusableView {
    //Header view
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var categoryTitleLabel: UILabel!
    @IBOutlet weak var viewMoreButton: UIButton!
    @IBOutlet weak var headerViewForTap: UIView!
    //News view
    @IBOutlet weak var NewsView: UIView!
    @IBOutlet weak var newDetailForTap: UIView!
    
    //image
    @IBOutlet weak var newsImage: CustomImageView!
    
    //news UILabel
    @IBOutlet weak var newsTitleLabel: UILabel!
   // @IBOutlet weak var newsDescriptionLabel: UILabel!
    @IBOutlet weak var newsTimeLabel: UILabel!
}
