//
//  NewsContentHtmlTableViewCell.swift
//  iCNM
//
//  Created by Len Pham on 8/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class NewsContentHtmlTableViewCell: UITableViewCell {
    @IBOutlet weak var newContentWebView: UIWebView!
    
    public var htmlContent: String = String() {
        didSet {
            let pageHtml = htmlContent;
            newContentWebView.loadHTMLString(pageHtml,
                                     baseURL: nil)
            
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
