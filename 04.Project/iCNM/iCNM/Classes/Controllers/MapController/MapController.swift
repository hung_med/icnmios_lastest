//
//  MapController.swift
//  iCNM
//
//  Created by Nguyen Van Dung on 7/24/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import SideMenu
import RealmSwift
import Toaster

class MapController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: GMSMapView!
    //var clusterManager: GMUClusterManager!
    @IBOutlet weak var listView: UIView!
    @IBOutlet weak var viewOverlay: UIView!
    //Combobox
    @IBOutlet weak var specialistCombobox: FWComboBox!
    @IBOutlet weak var distanceCombobox: FWComboBox!
    @IBOutlet weak var sortCombobox: FWComboBox!
    @IBOutlet weak var specialistMoreCombobox: FWComboBox!
    
    @IBOutlet weak var btnSearchArea: UIButton!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var imgThumnailMedicalUnit: UIImageView!
    @IBOutlet weak var lbNameMedicalUnit: UILabel!
    @IBOutlet weak var lbCommentCount: UILabel!
    @IBOutlet weak var lbAddress: UILabel!
    @IBOutlet weak var lbDistance: UILabel!
    
    @IBOutlet weak var imgStar1: UIImageView!
    @IBOutlet weak var imgStar2: UIImageView!
    @IBOutlet weak var imgStar3: UIImageView!
    @IBOutlet weak var imgStar4: UIImageView!
    @IBOutlet weak var imgStar5: UIImageView!
    
    @IBOutlet weak var constraintTop: NSLayoutConstraint!
    
    //Filter
    @IBOutlet weak var btnHospital: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var moreView: UIView!
   
    //var
    var locationManager = CLLocationManager()
    var listMedicalUnitNearBy:[LocationNearBy]!
    var selectedLocation:LocationNearBy?
    var savedCameraPosition:GMSCameraPosition?
    
    var selectedMarker:GMSMarker!
    var saveImageMarker:UIImage!
    
    //-----
    var specialist:String = "Tất cả chuyên khoa"
    var typeMedicalUnit:String = "1-2-3-4-5"
    var distance:Int = 1
    var rate:Int = 2
    var sortType:String = "Chọn đánh giá"//or Từ cao đến thấp
    //count of swipe down
    var countSwipeDown:Int = 0
    var userDefaults = UserDefaults.standard
    
    //Datasource
    var specialistArr:[String]?
    
    let arrDistance = ["1 km", "2 km", "5 km", "10 km", "20 km", "50 km"]
    let arrSort = ["Từ thấp đến cao", "Từ cao đến thấp", "Chọn đánh giá"]
    let supportFeedBackVC = StoryboardScene.Main.supportCategoriesMenuNavigationController.instantiate()
    
    private var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set layout for UI
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        self.lbDistance.layer.masksToBounds = true
        self.lbDistance.layer.cornerRadius = 3
        self.btnSearchArea.layer.cornerRadius = 5
        self.btnSearchArea.layer.masksToBounds = true
        specialistCombobox.layer.cornerRadius = 5
        specialistCombobox.layer.masksToBounds = true
        specialistCombobox.layer.borderWidth = 0.5
        specialistCombobox.layer.borderColor = UIColor.lightGray.cgColor
        self.btnHospital.layer.cornerRadius = 15
//
        //Combobox Delegate
        specialistCombobox.delegate = self
        distanceCombobox.delegate = self
        sortCombobox.delegate = self
        specialistMoreCombobox.delegate = self
        
        //get all specialist
        self.specialistArr = [String]()
        self.getSpecialList()
        
        //button search
        self.btnSearchArea.isHidden = true
        //tableview
        tableView.register(UINib.init(nibName: "UnitInforCell", bundle: Bundle.main), forCellReuseIdentifier: "UnitInforCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = UIColor.lightGray
        
        self.listMedicalUnitNearBy = [LocationNearBy]()
    
        //location manager - get user location
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        // Set up the cluster manager with the supplied icon generator and
        // renderer.
//        let iconGenerator = GMUDefaultClusterIconGenerator()
//        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
//        let renderer = GMUDefaultClusterRenderer(mapView: mapView,
//                                                 clusterIconGenerator: iconGenerator)
//        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm,
//                                           renderer: renderer)
//        clusterManager.setDelegate(self, mapDelegate: self)
//        // Generate and add random items to the cluster manager.
//        generateClusterItems()
//
//        // Call cluster() after items have been added to perform the clustering
//        // and rendering on map.
//        clusterManager.cluster()
        
        //Setup Swipe Action
        self.setupSwipAction()
        
        self.btnHospital.backgroundColor = UIColor.init(hex: "0084FF")
        self.checkTrackingFeature()
    }
    
    func getSpecialList() {
        Specialist.getAllSpecialist(success: { (results) in
            if !results.isEmpty {
                for result in results {
                    if let specialistName = result.name{
                        self.specialistArr?.append(specialistName)
                    }
                }
                //init combobox
                self.initialComboboxDatasource()
            } else {
                
            }
        }, fail: { (error, response) in
            
        })
    }
    
    /// Randomly generates cluster items within some extent of the camera and
    /// adds them to the cluster manager.
//    private func generateClusterItems() {
//        let extent = 0.2
//        for index in 1...20 {
//            let lat = (self.savedCameraPosition?.target.latitude)! + extent * randomScale()
//            let lng = (self.savedCameraPosition?.target.longitude)! + extent * randomScale()
//            let name = "Item \(index)"
//            let item =
//                POIItem(position: CLLocationCoordinate2DMake(lat, lng), name: name)
//            clusterManager.add(item)
//        }
//    }
    
    /// Returns a random value between -1.0 and 1.0.
//    private func randomScale() -> Double {
//        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
//    }
    
    func checkTrackingFeature(){
        if currentUser != nil{
            let userDefaults = UserDefaults.standard
            let token = userDefaults.object(forKey: "token") as! String
            TrackingFeature().checkTrackingFeature(fcm:token, featureName:Constant.MAPS, categoryID: 0, success: {(result) in
                if result != nil{
                    print("flow track done:", Constant.MAPS)
                }else{
                    print("flow track fail:", Constant.MAPS)
                }
            }, fail: { (error, response) in
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }else{
            return
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.userAllowAccessLocationAction()
        
        //Notification
        let reloadListLocation = Notification.Name("ReloadListLocation")
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadListData), name: reloadListLocation, object: nil)
    }
    @IBAction func openMenu(_ sender: UIBarButtonItem) {
        self.openLeftmenu()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Bản đồ")
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        //right menu
        SideMenuManager.default.menuRightNavigationController = supportFeedBackVC
        self.tabBarController?.tabBar.isHidden = false
        requestData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchOnChangeSupportFeedback(_ sender: Any) {
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    /*
     * initial combox datasource
     */
    func initialComboboxDatasource() {
        //
        distanceCombobox.dataSource = self.arrDistance
        let distanceStr = "\(self.distance) km"
        let indexOfSelectedDistance = self.specialistArr?.index(of: distanceStr)
        specialistCombobox.selectRow(at: indexOfSelectedDistance)
        
        //
        sortCombobox.dataSource = self.arrSort
        let indexOfSelectedSort = self.arrSort.index(of: self.sortType)
        sortCombobox.selectRow(at: indexOfSelectedSort)
        
        specialistMoreCombobox.dataSource = self.specialistArr!
        let indexOfSelectedMoreSpecialist = self.specialistArr?.index(of: self.specialist)
        specialistMoreCombobox.selectRow(at: indexOfSelectedMoreSpecialist)
        
        specialistCombobox.dataSource = self.specialistArr!
        let indexOfSelectedSpecialist = self.specialistArr?.index(of: self.specialist)
        specialistCombobox.selectRow(at: indexOfSelectedSpecialist)
        //specialistCombobox.selectRow(at: indexOfSelectedSort)
    }
    
    /*
     * Set up swipe gesture for view
     */
    func setupSwipAction() -> Void {
        let swipeUp = UISwipeGestureRecognizer.init(target: self, action: #selector(self.swipeUp))
        swipeUp.direction = UISwipeGestureRecognizerDirection.up;
        self.listView.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer.init(target: self, action: #selector(self.swipeDown))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down;
        self.listView.addGestureRecognizer(swipeDown)
    }
    
    // MARK: Google Map
    
    /*
     * Set up marker for Google Map
     * param: array LocationNearBy
     */
    func setMarkerForMap(locations: [LocationNearBy]) -> Void {
        
        let imgTypeDoctor = UIImage(named: "map_icon_doctor")
        let imgTypeHospital = UIImage(named: "map_icon_hospital")
        let imgTypeDrugstore = UIImage(named: "map_icon_medicin")
        
        //clear all marker before load again
        self.mapView.clear()
        
        for location in locations {
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: CLLocationDegrees(location.lat), longitude: CLLocationDegrees(location.long))
        
            //set image
            if (location.locationTypeID == 1) {
                marker.icon = imgTypeDoctor
            } else if (location.locationTypeID == 2 || location.locationTypeID == 3 || location.locationTypeID == 4) {
                marker.icon = imgTypeHospital
            } else if (location.locationTypeID == 5) {
                marker.icon = imgTypeDrugstore
            }
            
            marker.userData = location
            marker.map = mapView
        }
    }
    
    
    /**
     * Show information of medical unit when touch in marker of Google map
     * Param: medical unit
     * return: void
     */
    func showMedicalInfor(medicalUnit: LocationNearBy) -> Void {
        
        //set selection locationID
        self.selectedLocation = medicalUnit
        
        //fill data
        
        //name
        if (medicalUnit.name.count <= 50) {
            lbNameMedicalUnit.text = medicalUnit.name
        } else {
            let index = medicalUnit.name.index(medicalUnit.name.startIndex, offsetBy: 50)
            lbNameMedicalUnit.text = medicalUnit.name.substring(to: index) + "..."
        }
        
        Common.sharedInstance.setRatingStar(imgViewStar1: self.imgStar1, imgViewStar2: self.imgStar2, imgViewStar3: self.imgStar3, imgViewStar4: self.imgStar4, imgViewStar5: self.imgStar5, ratingScore: medicalUnit.averageRating)
        Common.sharedInstance.setTextLabelCommentCount(locationID: medicalUnit.locationID, label: self.lbCommentCount)
        
        lbAddress.text = medicalUnit.address
        
        let distanceKm = medicalUnit.distance/1000
        lbDistance.text = "\(Double(distanceKm).rounded(toPlaces: 2)) km"
        lbDistance.textColor = UIColor.white
        lbDistance.backgroundColor = UIColor(hex:"1976D2")
    
        var urlImage = API.baseURLImage + "iCNMImage/" + "LocationImage/" +
            "\(medicalUnit.locationID)/" + medicalUnit.imageStr
        let realImageUrl = urlImage.components(separatedBy: ";")[0]
        let result = Int(splitImageLocationToString(str: medicalUnit.imageStr))
        if result != medicalUnit.locationID{
            urlImage = API.baseURLImage + "iCNMImage/" + "LocationImage/" + medicalUnit.imageStr
            let realImageUrl = urlImage.components(separatedBy: ";")[0]
            imgThumnailMedicalUnit.sd_setImage(with: URL(string: realImageUrl), placeholderImage: UIImage(named: "default-thumbnail.png"))
        }else{
            imgThumnailMedicalUnit.sd_setImage(with: URL(string: realImageUrl), placeholderImage: UIImage(named: "default-thumbnail.png"))
        }
        self.popUpView.isHidden = false
        self.listView.isHidden = true
    }
   
   // MARK: -- SWIFT LEFT, UP, DOWN
    func swipeUp(gestureRecognizer: UISwipeGestureRecognizer) {
        
        UIView.animate(withDuration: 0.5) {
            self.constraintTop.constant = 0
            self.listView.layoutIfNeeded()
        }
    }

    func swipeDown(gestureRecognizer: UISwipeGestureRecognizer) {
        
        if self.constraintTop.constant == 0 {
            UIView.animate(withDuration: 0.5) {
                self.constraintTop.constant = 270
                self.listView.layoutIfNeeded()
            }
        } else if self.constraintTop.constant == 270 {
            let screenHeight = self.view.frame.size.height
            self.constraintTop.constant = screenHeight
            
            if (self.listMedicalUnitNearBy.count > 0) {
                
                let firstMedicalUnit = self.listMedicalUnitNearBy[0]
                self.showMedicalInfor(medicalUnit: firstMedicalUnit)
            }
        }
    }
    
    // MARK: IBAction
    @IBAction func btnClosePopupAction(_ sender: Any) {
        
        //hide popup infor
        self.popUpView.isHidden = true
        
        //show list unit
        self.constraintTop.constant = 270
        self.listView.isHidden = false
        
        //unhightlight marker on map
        if (self.selectedMarker != nil) {
            self.unhighlightMarker(marker: self.selectedMarker!)
        }
    }
    
    @IBAction func showUnitDetail(_ sender: Any) {
        let unitDetailVC = UnitDetailController(nibName: "UnitDetailController", bundle: nil)
        unitDetailVC.locationNearBy = self.selectedLocation
        unitDetailVC.savedCameraPosition = self.savedCameraPosition
        unitDetailVC.rate = self.rate
        self.navigationController?.pushViewController(unitDetailVC, animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        
        let progress = self.showProgress()
        
        let latStringOriginal = "\((self.savedCameraPosition?.target.latitude)!)"
        let latStr = latStringOriginal.replacingOccurrences(of: ".", with: "-")
        
        let longStringOriginal = "\((self.savedCameraPosition?.target.longitude)!)"
        let longStr = longStringOriginal.replacingOccurrences(of: ".", with: "-")
        
        LocationNearBy.getLocationNearBy(lat: latStr, long: longStr, specialist: self.specialist, distance: self.distance, pageNumber: 1, pageRow: Constant.MAX_ROW, type: self.typeMedicalUnit, rate:rate) { (data, error) in
            
            //set data
            self.listMedicalUnitNearBy.removeAll()
            self.listMedicalUnitNearBy.append(contentsOf: data!)
            
            //reload table
            self.tableView.reloadData()
            
            //set marker
            self.setMarkerForMap(locations: self.listMedicalUnitNearBy)
            
            //hide progess
            self.hideProgress(progress)
        }

        self.btnSearchArea.isHidden = true
        self.popUpView.isHidden = true
        
        //show list unit
        self.constraintTop.constant = 270
        self.listView.isHidden = false
    }
    
    @IBAction func selectTypeHospital(_ sender: Any) {
        self.btnHospital.backgroundColor = UIColor.init(hex: "0084FF")
        self.reloadListData()
    }
    
    @IBAction func moreAction(_ sender: Any) {
        self.viewOverlay.isHidden = false
        self.moreView.isHidden = false
    }
    
    @IBAction func btnExitAction(_ sender: Any) {
        self.viewOverlay.isHidden = true
        self.moreView.isHidden = true;
    }
    
    @IBAction func btnAgree(_ sender: Any) {
        self.viewOverlay.isHidden = true
        self.moreView.isHidden = true
        self.reloadListData()
    }
    
    // MARK: Reload list data
    func reloadListData() -> Void {
        
        //show progress
        let progress = self.showProgress()
        let latStringOriginal = "\((self.savedCameraPosition?.target.latitude)!)"
        let latStr = latStringOriginal.replacingOccurrences(of: ".", with: "-")
        
        let longStringOriginal = "\((self.savedCameraPosition?.target.longitude)!)"
        let longStr = longStringOriginal.replacingOccurrences(of: ".", with: "-")
        
        LocationNearBy.getLocationNearBy(lat: latStr, long: longStr, specialist: self.specialist, distance: self.distance, pageNumber: 1, pageRow: Constant.MAX_ROW, type: self.typeMedicalUnit, rate:rate) { (data, error) in
            if data?.count == 0{
                Toast(text: "Không có dữ liệu").show()
            }
            //set data
            self.listMedicalUnitNearBy.removeAll();
            self.listMedicalUnitNearBy.append(contentsOf: data!)
            
            //reload table
            self.tableView.reloadData()
            self.tableView.setContentOffset(CGPoint.zero, animated: true)
            //set marker
            self.setMarkerForMap(locations: self.listMedicalUnitNearBy)
            
            //hide progress
            self.hideProgress(progress)
        }
    }
    
    /*
     * Enable, disable button when user allow, not allow access location
    */
    func userAllowAccessLocationAction() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                
                self.btnSearchArea.isUserInteractionEnabled = false
                self.specialistCombobox.isUserInteractionEnabled = false
              //  self.btnDoctor.isUserInteractionEnabled = false
                self.btnHospital.isUserInteractionEnabled = false
              //  self.btnMedicalStore.isUserInteractionEnabled = false
                self.btnMore.isUserInteractionEnabled = false
           // openSettingApp(message:"Bạn vui lòng bật định vị khi dùng ứng dụng")

            case .authorizedAlways, .authorizedWhenInUse:
                
                self.btnSearchArea.isUserInteractionEnabled = true
                self.specialistCombobox.isUserInteractionEnabled = true
              //  self.btnDoctor.isUserInteractionEnabled = true
                self.btnHospital.isUserInteractionEnabled = true
              //  self.btnMedicalStore.isUserInteractionEnabled = true
                self.btnMore.isUserInteractionEnabled = true
            }
        } else {
            //openSettingApp(message:"Bạn vui lòng bật định vị khi dùng ứng dụng")

        }
    }
    
    //open location settings for app
    func openSettingApp(message: String) {
        let alertController = UIAlertController (title: "Thông báo", message:message , preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: NSLocalizedString("Thiết lập", comment: ""), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Huỷ", comment: ""), style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
}

/// Point of Interest Item which implements the GMUClusterItem protocol.
//class POIItem: NSObject, GMUClusterItem {
//    var position: CLLocationCoordinate2D
//    var name: String!
//
//    init(position: CLLocationCoordinate2D, name: String) {
//        self.position = position
//        self.name = name
//    }
//}

extension MapController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
       
        if (!self.listView.isHidden) {
             self.listView.isHidden = true
        } else {
            //show list unit
            self.constraintTop.constant = 270
            self.listView.isHidden = false
            self.popUpView.isHidden = true
        }
    }
    
    // MARK: - GMUClusterManagerDelegate
    
//    private func clusterManager(clusterManager: GMUClusterManager, didTapCluster cluster: GMUCluster) {
//        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
//                                                           zoom: mapView.camera.zoom + 1)
//        let update = GMSCameraUpdate.setCamera(newCamera)
//        mapView.moveCamera(update)
//    }
//
//    // MARK: - GMUMapViewDelegate
//
//    private func mapView(mapView: GMSMapView, didTapMarker marker: GMSMarker) -> Bool {
//        if let poiItem = marker.userData as? POIItem {
//            NSLog("Did tap marker for cluster item \(poiItem.name)")
//        } else {
//            NSLog("Did tap a normal marker")
//        }
//        return false
//    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        //mapview animation
        let camera = GMSCameraPosition.camera(withLatitude: marker.position.latitude,
                                              longitude: marker.position.longitude, zoom: 18.0)
        mapView.animate(to: camera)
        
        //show detail infor
        if (self.selectedMarker != nil) {
            self.unhighlightMarker(marker: self.selectedMarker!)
        }
        
        self.highlightMarker(marker: marker)
        self.selectedMarker = marker
       
        let location = marker.userData as! LocationNearBy
        showMedicalInfor(medicalUnit: location)
        return true
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.btnSearchArea.isHidden = false
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        self.savedCameraPosition = position
    }
    
    func highlightMarker(marker: GMSMarker) {
        self.saveImageMarker = marker.icon
        marker.icon = UIImage(named: "ic_marker_ticked.png")
    }
    
    func unhighlightMarker(marker: GMSMarker) {
        marker.icon = self.saveImageMarker
    }
}

// MARK: TableView Delegate, Datasource
extension MapController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listMedicalUnitNearBy.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UnitInforCell", for: indexPath) as! UnitInforCell
        cell.selectionStyle = .none
        
        let locationNearBy = self.listMedicalUnitNearBy[indexPath.row]
        var urlImage = API.baseURLImage + "iCNMImage/" + "LocationImage/" +
            "\(locationNearBy.locationID)/" + locationNearBy.imageStr
        let realImageUrl = urlImage.components(separatedBy: ";")[0]
        let result = Int(splitImageLocationToString(str: locationNearBy.imageStr))
        if result != locationNearBy.locationID{
            urlImage = API.baseURLImage + "iCNMImage/" + "LocationImage/" + locationNearBy.imageStr
            let realImageUrl = urlImage.components(separatedBy: ";")[0]
            cell.imgView.sd_setImage(with: URL(string: realImageUrl), placeholderImage: UIImage(named: "default-thumbnail.png") )
        }else{
            cell.imgView.sd_setImage(with: URL(string: realImageUrl), placeholderImage: UIImage(named: "default-thumbnail.png"))
        }
       
        if (locationNearBy.name.count <= 50) {
            
            cell.lbMedicalUnitName.text = locationNearBy.name
        } else {
            let index = locationNearBy.name.index(locationNearBy.name.startIndex, offsetBy: 50)
            cell.lbMedicalUnitName.text = locationNearBy.name.substring(to: index) + "..."
        }

        //Rating
        Common.sharedInstance.setRatingStar(imgViewStar1: cell.imgStar1, imgViewStar2: cell.imgStar2, imgViewStar3: cell.imgStar3, imgViewStar4: cell.imgStar4, imgViewStar5: cell.imgStar5, ratingScore: locationNearBy.averageRating)
        cell.setCommentCount(locationID: locationNearBy.locationID)
        
        cell.lbAddress.text = locationNearBy.address
        
        let distanceKm = locationNearBy.distance/1000
        cell.lbDistance.text = "\(Double(distanceKm).rounded(toPlaces: 2)) km"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let locationNearBy = self.listMedicalUnitNearBy[indexPath.row];
        let unitDetailVC = UnitDetailController(nibName: "UnitDetailController", bundle: nil)
        unitDetailVC.locationNearBy = locationNearBy
        unitDetailVC.savedCameraPosition = self.savedCameraPosition
        unitDetailVC.rate = self.rate
        self.navigationController?.pushViewController(unitDetailVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
}

// MARK: CLLocationManagerDelegate
extension MapController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations.last
        
        let center = CLLocationCoordinate2D(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
        
        let camera = GMSCameraPosition.camera(withLatitude: center.latitude,
                                              longitude: center.longitude, zoom: 16.0)
        
        mapView.animate(to: camera)
        mapView.isMyLocationEnabled = true
        mapView.delegate = self

        //save current GMSCameraPosition
        self.savedCameraPosition = camera
        
        let latStringOriginal = "\((self.savedCameraPosition?.target.latitude)!)"
        let latStr = latStringOriginal.replacingOccurrences(of: ".", with: "-")
        
        let longStringOriginal = "\((self.savedCameraPosition?.target.longitude)!)"
        let longStr = longStringOriginal.replacingOccurrences(of: ".", with: "-")
        
        if userDefaults.object(forKey: "LAT") != nil{
            userDefaults.removeObject(forKey: "LAT")
        }
        if userDefaults.object(forKey: "LONG") != nil{
            userDefaults.removeObject(forKey: "LONG")
        }
        
        userDefaults.set(latStringOriginal, forKey: "LAT")
        userDefaults.set(longStringOriginal, forKey: "LONG")
        userDefaults.synchronize()
        
        LocationNearBy.getLocationNearBy(lat: latStr, long: longStr, specialist:self.specialist, distance: self.distance, pageNumber: 1, pageRow: Constant.MAX_ROW, type: self.typeMedicalUnit, rate:rate) { (data, error) in
            
            //set data
            self.listMedicalUnitNearBy.removeAll()
            self.listMedicalUnitNearBy.append(contentsOf: data!)
            
            //reload table
            self.tableView.reloadData()
            //set marker
            self.setMarkerForMap(locations: self.listMedicalUnitNearBy)
        }
        locationManager.stopUpdatingLocation()
    }
}

extension MapController:FWComboBoxDelegate {
    func fwComboBox(comboBox: FWComboBox, didSelectAtIndex index: Int) {
        
        if (comboBox == specialistCombobox) {
            self.specialist = self.specialistArr![index]
            let indexOfSelectedSpecialist = self.specialistArr?.index(of: self.specialist)
            specialistMoreCombobox.selectRow(at: indexOfSelectedSpecialist)
            self.reloadListData()
        }
        
        if (comboBox == distanceCombobox) {
            let distanceStr = arrDistance[index].components(separatedBy: " ")[0]
            self.distance = Int(distanceStr)!
        }
        
        if (comboBox == specialistMoreCombobox) {
            self.specialist = self.specialistArr![index]
            let indexOfSelectedSpecialist = self.specialistArr?.index(of: self.specialist)
            specialistCombobox.selectRow(at: indexOfSelectedSpecialist)
            self.reloadListData()
        }
        
        if (comboBox == sortCombobox) {
            self.sortType = arrSort[index]
            rate = index
        }
    }
}

