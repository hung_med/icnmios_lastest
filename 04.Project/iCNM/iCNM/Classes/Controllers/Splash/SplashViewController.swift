//
//  SplashViewController.swift
//  iCNM
//
//  Created by Medlatec on 6/7/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    @IBOutlet weak var titleVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var iconVerticalConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        iconVerticalConstraint.constant = 0
        titleVerticalConstraint.constant = 51
        UIView.animate(withDuration: 1.5, animations: {
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.black
        }) { (finished) in
            if let window = UIApplication.shared.keyWindow {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    let userDefault = UserDefaults.standard
                    if userDefault.bool(forKey: "agreedTerms") {
                        let mainViewController =  StoryboardScene.Main.mainTabBarController.instantiate()
                        window.rootViewController = mainViewController
                    } else {
                        let onboardViewController = StoryboardScene.OnBoard.onBoardPageViewController.instantiate()
                        window.rootViewController = onboardViewController
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
