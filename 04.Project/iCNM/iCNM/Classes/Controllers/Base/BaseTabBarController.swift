//
//  BaseTabBarController.swift
//  iCNM
//
//  Created by Medlatec on 6/19/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import SideMenu

class BaseTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    @IBInspectable var defaultIndex:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let items = self.tabBar.items {
            if defaultIndex < items.count {
                selectedIndex = defaultIndex
            }
        }
        
        SideMenuManager.default.menuLeftNavigationController = StoryboardScene.Main.leftMenuNavigationController.instantiate()
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuAnimationBackgroundColor = UIColor(hex:"1d6edc")
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.view)
        SideMenuManager.default.menuAnimationFadeStrength = 0.5
        
        if IS_IPAD{
            UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 18.0)], for: UIControlState.normal)
        }else{
            if IS_IPHONE_5{
                UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 11.0)], for: UIControlState.normal)
            }else{
                UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 12.0)], for: UIControlState.normal)
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
