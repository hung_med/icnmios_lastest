//
//  BaseTableViewController.swift
//  ezCloudHotelManager
//
//  Created by Medlatec on 2/15/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class BaseTableViewController: UITableViewController {
    
    //    var loadingProgress:MKActivityIndicator?
    
    private let indicatorSize:CGFloat = 28.0
    private let loadingProgressLineWidth:CGFloat = 3.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(internetComeBack), name: Constant.NotificationMessage.internetComeBack, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(noInternetConnection), name: Constant.NotificationMessage.noInternetConnection, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleApplicationComesFromBackground), name:
            NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        // Do any additional setup after loading the view.
    }
    
    @objc func internetComeBack(aNotification: Notification) {
        requestData()
    }
    
    func requestData() {
        //Implement nothing
    }
    
    @objc func noInternetConnection (aNotification: Notification) {
    }
    
    @objc func handleApplicationComesFromBackground (aNotification: Notification) {
        if self.isViewLoaded && self.view.window != nil {
            requestData()
        }
    }
    
    func showProgress(color:UIColor? = nil) -> MKActivityIndicator? {
        //        if loadingProgress == nil {
        if let viewToAdd = self.navigationController?.view {
            let loadingProgress = MKActivityIndicator(frame: CGRect(x:0,y:0,width: indicatorSize,height:indicatorSize))
            if let color = color {
                loadingProgress.color = color
            }
            loadingProgress.center = self.view.center
            loadingProgress.lineWidth = loadingProgressLineWidth
            loadingProgress.autoresizingMask = [.flexibleTopMargin, .flexibleLeftMargin, .flexibleBottomMargin, .flexibleRightMargin]
            viewToAdd.addSubview(loadingProgress)
            loadingProgress.startAnimating()
            return loadingProgress
        }
        return nil
        //        }
    }
    
    func hideProgress(_ loadingProgress:MKActivityIndicator?) {
        loadingProgress?.stopAnimating()
        loadingProgress?.removeFromSuperview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
