//
//  BaseViewController.swift
//  ezCloudHotelManager
//
//  Created by Medlatec on 2/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import SideMenu

class BaseViewController: BaseViewControllerNoSearchBar, UISearchBarDelegate {
    
    var searchBar:UISearchBar = UISearchBar()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.placeholder = "Tìm kiếm"
        if #available(iOS 11.0, *) {
            searchBar.heightAnchor.constraint(equalToConstant: 43.7).isActive = true

        }
        searchBar.delegate = self
        self.navigationItem.titleView = searchBar
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        let controller = SearchVCViewController()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController!.pushViewController(controller, animated: true)
        return false
    }
}
