//
//  BaseViewControllerNoSearchBar.swift
//  iCNM
//
//  Created by Medlatec on 9/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import SideMenu

class BaseViewControllerNoSearchBar: UIViewController, LoginViewControllerDelegate {

    private let indicatorSize:CGFloat = 28.0
    private let loadingProgressLineWidth:CGFloat = 3.0

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(internetComeBack), name: Constant.NotificationMessage.internetComeBack, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(noInternetConnection), name: Constant.NotificationMessage.noInternetConnection, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleApplicationComesFromBackground), name:
            NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    @objc func internetComeBack(aNotification: Notification) {
        requestData()
    }
    
    func requestData() {
        //Implement nothing
    }
    
    @objc func noInternetConnection (aNotification: Notification) {
    }
    
    @objc func handleApplicationComesFromBackground (aNotification: Notification) {
        if self.isViewLoaded && self.view.window != nil {
            requestData()
        }
    }
    
    func showProgress(color:UIColor? = nil) -> MKActivityIndicator? {
        let loadingProgress = MKActivityIndicator(frame: CGRect(x:0,y:0,width: indicatorSize,height:indicatorSize))
        if let color = color {
            loadingProgress.color = color
        }
        loadingProgress.center = self.view.center
        loadingProgress.lineWidth = loadingProgressLineWidth
        loadingProgress.autoresizingMask = [.flexibleTopMargin, .flexibleLeftMargin, .flexibleBottomMargin, .flexibleRightMargin]
        view.addSubview(loadingProgress)
        loadingProgress.startAnimating()
        return loadingProgress
    }
    
    func hideProgress(_ loadingProgress:MKActivityIndicator?) {
        loadingProgress?.stopAnimating()
        loadingProgress?.removeFromSuperview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func openLeftmenu() {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    func openRight() {
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    func showRemindGoToLogin() {
        let alertViewController = UIAlertController(title: "Thông báo", message: "Bạn phải đăng nhập mới thực hiện được chức năng này", preferredStyle: .alert)
        
        alertViewController.addAction(UIAlertAction(title: "Bỏ qua", style: UIAlertActionStyle.cancel, handler: { action in
        }))
        alertViewController.addAction(UIAlertAction(title: "Đăng nhập", style: UIAlertActionStyle.default, handler: { action in
            self.callLogIn()
        }))
        
        self.present(alertViewController, animated: true, completion: nil)
    }
    
    func callLogIn() {
        let loginVC = StoryboardScene.LoginRegister.loginViewController.instantiate()
        loginVC.myDelegate = self
        self.navigationController?.pushViewController(loginVC, animated:false)
    }
    
    func backCurrentView(){
        navigationController?.popViewController(animated: true)
    }
}
