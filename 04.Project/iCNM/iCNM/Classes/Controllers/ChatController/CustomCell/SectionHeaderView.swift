//
//  SectionHeaderView.swift
//  iCNM
//
//  Created by Thanh Huyen on 5/2/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class SectionHeaderView: UICollectionReusableView {
    
    @IBOutlet weak var lbHeader: UILabel!
    @IBOutlet weak var lbNumMess: UILabel!
    
}

