//
//  ChatTableViewCell.swift
//  iCNM
//
//  Created by Thành Trung on 13/04/2018.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {
    
    // outlet connect
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var isOnlineBtn: UIButton!
    @IBOutlet weak var lbUser: UILabel!
    @IBOutlet weak var lbLastMess: UILabel!
    @IBOutlet weak var lbTime: UILabel!
    @IBOutlet weak var lbNumMess: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imgAvatar.layer.cornerRadius = 30
        imgAvatar.layer.masksToBounds = true
        imgAvatar.layer.borderWidth = 2.5
        imgAvatar.layer.borderColor = UIColor.init(hex:"1D6EDC").cgColor
        lbNumMess.isHidden = true
        lbNumMess.layer.cornerRadius = 10.5
        lbNumMess.layer.masksToBounds = true
        isOnlineBtn.layer.cornerRadius = 8.0
        isOnlineBtn.layer.masksToBounds = true
        isOnlineBtn.layer.borderWidth = 2.0
        isOnlineBtn.layer.borderColor = UIColor.white.cgColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

