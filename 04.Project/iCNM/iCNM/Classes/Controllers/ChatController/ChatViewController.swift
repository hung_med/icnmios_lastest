//
//  ChatViewController.swift
//  iCNM
//
//  Created by Thành Trung on 07/04/2018.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseCore
import FirebaseStorage
import JSQMessagesViewController
import MobileCoreServices
import AVKit
import SDWebImage
import RealmSwift
import Photos
import PhotoSlider
import Toaster
import FontAwesome_swift

class ChatViewController: JSQMessagesViewController, UIGestureRecognizerDelegate {
    
    // user variable
    var featuredDoctor:FeaturedDoctor?
    let databaseReference = Database.database().reference()
    var myView : UIView?
    
    @IBOutlet weak var btCallVideo: UIBarButtonItem!
    @IBOutlet weak var btFile: UIBarButtonItem!
    
    var senderAvatar : String?
    
    var receiverAvatar : String?
    var receiverName : String?
    var receiverId : Int?
    
    let mediaPicker = UIImagePickerController()
    var Array_Messages = [JSQMessage]()
    var ClassMessages = [Messages]()
    var loadMore_ArrayMessages = [JSQMessage]()
    var loadMore_ClassMessages = [Messages]()
    
    var keyFirebase = [String]()
    var totalKey = [String]()
    var lastKey = ""
    
    var receiverKeySearch = ""
    
    var totalMess = Int()
    var newMess = ""
    
    var savePhoto : URL?
    var saveVideo : URL?
    
    var isCheckStatus = ""
    var isDel = false
    
    var naviTitle = ""
    var isOnline = "Đã gửi"
    var userOnline = [Int]()
    
    var delMess = ""
    
    var btnCallVideo : UIBarButtonItem!
    
    var currentUser : User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()

    
    // media sent button
    @IBAction func btVideoCall(_ sender: Any) {
        showAlertView(title: "Chức năng này đang được xây dựng!", view: self)
    }
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }

//    func client(_ client: SINCallClient!, didReceiveIncomingCall call: SINCall!) {
//        let getcallvideo_vc = UIStoryboard.init(name: "Chat", bundle: nil).instantiateViewController(withIdentifier: "getcallvideo_vc") as! GetCallViewController
//        getcallvideo_vc.segueSinCall = call
//        self.navigationController?.pushViewController(getcallvideo_vc, animated: true)
//    }
//    
//    @IBAction func btMediaSent(_ sender: Any) {
//
//        showAlertView(title: "Chức năng đang được xây dựng", view: self)
//        return
//
//        isTyping = false
//
//        let mediaVC = StoryboardScene.Chat.mediaVc.instantiate()
//        self.tabBarController?.tabBar.isHidden = true
//        guard let passAvatar = receiverAvatar else { return }
//        mediaVC.receiverAvatar = passAvatar
//
//        guard let passReceiverId = receiverId else { return }
//        mediaVC.receiverId = passReceiverId
//
//        databaseReference.child("Helpers").child("Conversations").child(self.senderId).child("\(receiverId!)").updateChildValues(["isOnlineOnChat" : false])
//        self.navigationController?.pushViewController(mediaVC, animated: true)
//    }
    
    // back button -> home
    @IBAction func btBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        databaseReference.child("Helpers").child("Conversations").child(self.senderId).child("\(receiverId!)").updateChildValues(["isOnlineOnChat" : false])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        } else {
            collectionView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0)
        }
        
        self.inputToolbar.maximumHeight = 100
        
        mediaPicker.delegate = self
        
        myView = self.navigationController?.view
        
        //get the current user Id
        if let id = currentUser?.userDoctor?.userInfo?.id,
            let userName = currentUser?.userDoctor?.userInfo?.name {
            
            self.senderId = String(id)
            self.senderDisplayName = userName
            senderAvatar = currentUser?.userDoctor?.userInfo?.avatar
        }
        
        getData(numberMess: 10)
        
        // set avatar frame
        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize.init(width: 25, height: 25)
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize.init(width: 25, height: 25)
        
        // hide keyboard
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGestureRecognizer.delegate = self
        self.collectionView?.addGestureRecognizer(tapGestureRecognizer)
        
        collectionView.addSubview(refreshControl)
        
        // check online on chat
        if let fromId = self.senderId, let toId = receiverId {
            databaseReference.child("Helpers").child("Conversations").child(fromId).child("\(toId)").updateChildValues(["isOnlineOnChat" : true])
        }
        
        // get name and id, avatar doctor - display the doctor's name in navi_bar
        if receiverName != nil {
            naviTitle = receiverName!
        } else {
            databaseReference.child("Users").child("\(receiverId!)").observeSingleEvent(of: .value, with: { snapshot in
                if let dict = snapshot.value as? [String : Any] {
                    self.naviTitle = dict["Name"] as! String
                }
            })
        }
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        refreshControl.addTarget(self, action: #selector(ChatViewController.loadMore), for: .valueChanged)
        return refreshControl
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // get total number mess
        databaseReference.child("Messages").child(senderId).child("\(receiverId!)").observe(.value, with: { snapshot in
            self.totalMess = Int(snapshot.childrenCount)
        })

        databaseReference.child("Messages").child(senderId).child("\(receiverId!)").observe(.childAdded, with: { snapshot in
            self.totalKey.append(snapshot.key)
        })
        
        // delete data if it doesnt have
        if let id = senderId {
            databaseReference.child("Conversations").child(id).queryOrdered(byChild: "from").queryEqual(toValue: receiverId!).observe(.childAdded, with: { (snapshot) in
                if snapshot.childrenCount > 1 {
                    self.databaseReference.child("Conversations").child(id).child(snapshot.key).updateChildValues(["seen" : true])
                    self.databaseReference.child("Conversations").child(id).queryOrdered(byChild: "from").queryEqual(toValue: self.receiverId!).removeAllObservers()
                    self.databaseReference.child("Conversations").child(id).child(snapshot.key).removeAllObservers()
                }
            })
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
//        self.inputToolbar.contentView.textView.becomeFirstResponder()
        // call func
        checkUserIsTyping()
        
        self.databaseReference.child("Helpers").child("Users").child("\(receiverId!)").observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                if let dict = snapshot.value as? [String : Any] {
                    let isOnline = dict["isOnline"] as! Bool
                    if isOnline == true {
                        self.navigationItem.setTitle(title: self.naviTitle, subtitle: "Online")
                    } else {
                        self.navigationItem.setTitle(title: self.naviTitle, subtitle: "Offline")
                    }
                }
            } else {
                self.navigationItem.setTitle(title: self.naviTitle, subtitle: "Offline")
            }
        })
        
        // da xem da gui
        _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(checkData), userInfo: nil, repeats: true)
    }
    
    func checkData() {
        databaseReference.child("Helpers").child("Conversations").child("\(receiverId!)").child(self.senderId).observeSingleEvent(of: .value, with: { (snapshot) in
            if let dict = snapshot.value as? [String : Any] {
                let check = dict["isOnlineOnChat"] as! Bool
                if check {
                    self.isOnline = "Đã xem"
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                } else {
                    self.isOnline = "Đã gửi"
                }
            }
        })
    }
    
    func loadMore() {
        
        guard let lastOfTotalKey = totalKey[0] as? String else {return}
        
        if lastKey == lastOfTotalKey {
            refreshControl.removeFromSuperview()
            if #available(iOS 11.0, *) {
                collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
            } else {
                collectionView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0)
            }
        } else {
            keyFirebase.removeAll()
            loadMore_ArrayMessages.removeAll()
            loadMore_ClassMessages.removeAll()
            
            guard let fromId = self.senderId else { return }
            guard let toId = receiverId else { return }
            
            databaseReference.child("Messages").child(fromId).child("\(toId)").queryOrderedByKey().queryEnding(atValue: self.lastKey).queryLimited(toLast: 10).removeAllObservers()
            databaseReference.child("Messages").child(fromId).child("\(toId)").queryOrderedByKey().queryEnding(atValue: self.lastKey).queryLimited(toLast: 10).observe(.childAdded, with: { (snapshot) in
                
                self.lastKey = ""
                self.keyFirebase.insert(snapshot.key, at: self.keyFirebase.count)
                self.lastKey = self.keyFirebase[0]
                
                if let dict = snapshot.value as? [String : Any] {
                    let senderid = dict["from"] as? Int ?? 0
                    let time = dict["time"] as? NSNumber ?? 0
                    let mediatype = dict["type"] as? String ?? ""
                    //let sendername = dict["SenderName"] as? String ?? ""
                    //let receivername = dict["ReceiverName"] as? String ?? ""
                    
                    if mediatype == "text" {
                        let text = dict["message"] as! String
                        let newJSQMess = JSQMessage(senderId: "\(senderid)", displayName: self.senderDisplayName, text: text)
                        
                        let newMess = Messages(TextString: text, SenderIdString: "\(senderid)", ReceiverIdString: "\(toId)", TimeNumber: time)
                        
                        self.loadMore_ArrayMessages.append(newJSQMess!)
                        self.loadMore_ClassMessages.append(newMess)
                        
                        if newMess.SenderId == self.senderId! {
                            newMess.ChatId = newMess.ReceiverId
                        } else {
                            newMess.ChatId = newMess.SenderId
                        }
                        
                        if newMess.ChatId == String(self.receiverId!) {
                            
                            if self.loadMore_ArrayMessages.count < 10 {
                                self.Array_Messages.insert(newJSQMess!, at: self.loadMore_ArrayMessages.count - 1)
                                self.ClassMessages.insert(newMess, at: self.loadMore_ClassMessages.count - 1)
                            }
                            
                            //                            self.finishReceivingMessage()
                            DispatchQueue.main.async {
                                self.collectionView.reloadData()
                            }
                        }
                    }
                        
                    else if mediatype == "image" {
                        let photo = JSQPhotoMediaItem(image: nil)
                        let PhotoUrl = dict["message"] as? String ?? ""
                        
                        let downloader = SDWebImageDownloader.shared()
                        downloader.downloadImage(with: URL(string: PhotoUrl), options: [], progress: nil, completed: { (image, data, error, finished) in
                            
                            DispatchQueue.main.async(execute: {
                                photo?.image = image
                                self.collectionView.reloadData()
                            })
                        })
                        
                        let newJSQMess = JSQMessage(senderId: "\(senderid)", displayName: self.senderDisplayName, media: photo)
                        
                        let newMess = Messages(TextString: "", SenderIdString: "\(senderid)", ReceiverIdString: "\(toId)", TimeNumber: time)
                        
                        self.loadMore_ArrayMessages.append(newJSQMess!)
                        self.loadMore_ClassMessages.append(newMess)
                        
                        if newMess.SenderId == self.senderId! {
                            newMess.ChatId = newMess.ReceiverId
                        } else {
                            newMess.ChatId = newMess.SenderId
                        }
                        
                        if newMess.ChatId == String(self.receiverId!) {
                            
                            if self.loadMore_ArrayMessages.count < 10 {
                                self.Array_Messages.insert(newJSQMess!, at: self.loadMore_ArrayMessages.count - 1)
                                self.ClassMessages.insert(newMess, at: self.loadMore_ClassMessages.count - 1)
                            }
                            
                            //                            self.finishReceivingMessage()
                            DispatchQueue.main.async {
                                self.collectionView.reloadData()
                            }
                        }
                    }
                    
                    else if mediatype == "callvideo" {
                        let text = dict["message"] as! String
                        let newJSQMess = JSQMessage(senderId: "\(senderid)", displayName: self.senderDisplayName, text: text)
                        
                        let newMess = Messages(TextString: text, SenderIdString: "\(senderid)", ReceiverIdString: "\(toId)", TimeNumber: time)
                        
                        if newMess.SenderId == self.senderId! {
                            newMess.ChatId = newMess.ReceiverId
                        } else {
                            newMess.ChatId = newMess.SenderId
                        }
                        
                        if newMess.ChatId == String(self.receiverId!) {
                            self.Array_Messages.append(newJSQMess!)
                            self.ClassMessages.append(newMess)
                            self.finishReceivingMessage()
                            DispatchQueue.main.async {
                                self.collectionView.reloadData()
                            }
                        }
                    }
                        
                    else if mediatype == "video" {
                        let thumnailImage = dict["message"] as! String
                        let thumnailImageUrl = URL(string: thumnailImage)
                        let data = try? Data(contentsOf: thumnailImageUrl!)
                        
                        let videoUrl = dict["videoUrl"] as? String ?? ""
                        let video = URL(string: videoUrl)
                        
                        let videoItem = JSQVideoMediaItem(fileURL: video!, isReadyToPlay: true, thumbnailImage: UIImage.sd_image(with: data!))
                        
                        let newJSQMess = JSQMessage(senderId: "\(senderid)", displayName: self.senderDisplayName, media: videoItem)
                        
                        let newMess = Messages(TextString: "", SenderIdString: "\(senderid)", ReceiverIdString: "\(toId)", TimeNumber: time)
                        
                        self.loadMore_ArrayMessages.append(newJSQMess!)
                        self.loadMore_ClassMessages.append(newMess)
                        
                        if newMess.SenderId == self.senderId! {
                            newMess.ChatId = newMess.ReceiverId
                        } else {
                            newMess.ChatId = newMess.SenderId
                        }
                        
                        if newMess.ChatId == String(self.receiverId!) {
                            
                            if self.loadMore_ArrayMessages.count < 10 {
                                self.Array_Messages.insert(newJSQMess!, at: self.loadMore_ArrayMessages.count - 1)
                                self.ClassMessages.insert(newMess, at: self.loadMore_ClassMessages.count - 1)
                            }
                            
                            //                            self.finishReceivingMessage()
                            DispatchQueue.main.async {
                                self.collectionView.reloadData()
                            }
                        }
                    }
                }
            })
            refreshControl.endRefreshing()
        }
    }
    
    func gestureRecognizer(_: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith shouldRecognizeSimultaneouslyWithGestureRecognizer:UIGestureRecognizer) -> Bool {
        return true
    }
    func dismissKeyboard(gesture: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    // get data
    func getData(numberMess : Int) {
        
        guard let fromId = self.senderId else { return }
        guard let toId = receiverId else { return }
        
        let progress = showProgress(view: self.myView!)
        
        databaseReference.child("Messages").child(fromId).child("\(toId)").queryLimited(toLast: UInt(numberMess)).observe(.childAdded, with: { (snapshot) in
            
            self.keyFirebase.insert(snapshot.key, at: self.keyFirebase.count)
            self.lastKey = self.keyFirebase[0]
            
            if let dict = snapshot.value as? [String : Any] {
                let senderid = dict["from"] as? Int ?? 0
                let time = dict["time"] as? NSNumber ?? 0
                let mediatype = dict["type"] as? String ?? ""
                
                if mediatype == "text" {
                    let text = dict["message"] as! String
                    let newJSQMess = JSQMessage(senderId: "\(senderid)", displayName: self.senderDisplayName, text: text)
                    
                    let newMess = Messages(TextString: text, SenderIdString: "\(senderid)", ReceiverIdString: "\(toId)", TimeNumber: time)
                    
                    if newMess.SenderId == self.senderId! {
                        newMess.ChatId = newMess.ReceiverId
                    } else {
                        newMess.ChatId = newMess.SenderId
                    }
                    
                    if newMess.ChatId == String(self.receiverId!) {
                        self.Array_Messages.append(newJSQMess!)
                        self.ClassMessages.append(newMess)
                        self.finishReceivingMessage()
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                    }
                }
                    
                else if mediatype == "image" {
                    let photo = JSQPhotoMediaItem(image: nil)
                    let PhotoUrl = dict["message"] as? String ?? ""
                    
                    let downloader = SDWebImageDownloader.shared()
                    downloader.downloadImage(with: URL(string: PhotoUrl), options: [], progress: nil, completed: { (image, data, error, finished) in
                        
                        DispatchQueue.main.async(execute: {
                            photo?.image = image
                            self.collectionView.reloadData()
                        })
                    })
                    self.showMediaData(senderId: "\(senderid)", displayName: self.senderDisplayName, text: "", ReceiverId: toId, TimeNumber: time, media: photo!, PhotoUrlString: PhotoUrl, ThumnailUrlString: "", VideoUrlString: "")
                }
                    
                else if mediatype == "video" {
                    let thumnailImage = dict["message"] as! String
                    let thumnailImageUrl = URL(string: thumnailImage)
                    guard let data = try? Data(contentsOf: thumnailImageUrl!) else {return}
                    
                    let videoUrl = dict["videoUrl"] as? String ?? ""
                    let video = URL(string: videoUrl)
                    
                    let videoItem = JSQVideoMediaItem(fileURL: video!, isReadyToPlay: true, thumbnailImage: UIImage.sd_image(with: data))
                    
                    self.showMediaData(senderId: "\(senderid)", displayName: self.senderDisplayName, text: "", ReceiverId: toId, TimeNumber: time, media: videoItem, PhotoUrlString: "", ThumnailUrlString: thumnailImage, VideoUrlString: videoUrl)

                }
                else if mediatype == "callvideo" {
                    let text = dict["message"] as! String
                    let newJSQMess = JSQMessage(senderId: "\(senderid)", displayName: self.senderDisplayName, text: text)
                    
                    let newMess = Messages(TextString: text, SenderIdString: "\(senderid)", ReceiverIdString: "\(toId)", TimeNumber: time)
                    
                    if newMess.SenderId == self.senderId! {
                        newMess.ChatId = newMess.ReceiverId
                    } else {
                        newMess.ChatId = newMess.SenderId
                    }
                    
                    if newMess.ChatId == String(self.receiverId!) {
                        self.Array_Messages.append(newJSQMess!)
                        self.ClassMessages.append(newMess)
                        self.finishReceivingMessage()
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                    }
                }
            }
            hideProgress(progress)
        })
        
        databaseReference.child("Messages").child(fromId).child("\(toId)").queryLimited(toLast: UInt(numberMess)).observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                return
            } else {
                hideProgress(progress)
            }
        })
    }
    
    // show media in chat log
    func showMediaData(senderId: String, displayName: String, text: String, ReceiverId: Int, TimeNumber: NSNumber, media: JSQMediaItem, PhotoUrlString : String, ThumnailUrlString : String, VideoUrlString : String) {
        let newJSQMess = JSQMessage(senderId: senderId, displayName: displayName, media: media)
        let newMess = Messages(TextString: text, SenderIdString: senderId, ReceiverIdString: "\(ReceiverId)", TimeNumber: TimeNumber)
        
        if newMess.SenderId == self.senderId! {
            newMess.ChatId = newMess.ReceiverId
            media.appliesMediaViewMaskAsOutgoing = true
        } else {
            newMess.ChatId = newMess.SenderId
            media.appliesMediaViewMaskAsOutgoing = false
        }
        
        newMess.PhotoUrl = PhotoUrlString
        newMess.VideoUrl = VideoUrlString
        
        if newMess.ChatId == String(self.receiverId!) {
            self.Array_Messages.append(newJSQMess!)
            self.ClassMessages.append(newMess)
            self.finishReceivingMessage()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    // config collection view
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Array_Messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return Array_Messages[indexPath.item]
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        cell.cellTopLabel.textColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        
        if Array_Messages[indexPath.item].senderId == senderId {
            cell.textView?.textColor = UIColor.white
            //cell.textView?.isSelectable = false
            cell.cellBottomLabel.textInsets = UIEdgeInsetsMake(0, 0, 0, 35)
            let second = ClassMessages[indexPath.row].Time.doubleValue
            let time = Date(timeIntervalSince1970: second/1000)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            cell.cellTimeLabel.text = dateFormatter.string(from: time)
            cell.cellBottomLabel.text = isOnline

        } else {
            cell.textView?.textColor = UIColor.black
            //cell.textView?.isSelectable = false
            cell.cellBottomLabel.textInsets = UIEdgeInsetsMake(0, 35, 0, 0)
            let second = ClassMessages[indexPath.row].Time.doubleValue
            let time = Date(timeIntervalSince1970: second/1000)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            cell.cellTimeLabel.text = dateFormatter.string(from: time)
        }
        return cell
    }
    
    func showDeleteAlert() {
        
        let alert = UIAlertController(title: "Bạn chắc chắn muốn xóa tin nhắn ?", message: nil, preferredStyle: .actionSheet)
        
        let action1 = UIAlertAction(title: "Đồng ý", style: .destructive) { (action) in
            return
        }
        let action2 = UIAlertAction(title: "Huỷ", style: .cancel, handler: nil)
        alert.addAction(action1)
        alert.addAction(action2)
        self.present(alert, animated: true, completion: nil)
    }
    
    // when user press send button
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        isTyping = false
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        let time = Int((date.timeIntervalSince1970) * 1000)
        let table = databaseReference.child("Messages").child(senderId!).child("\(receiverId!)").child("\(time)")
        let table1 = databaseReference.child("Messages").child("\(receiverId!)").child(senderId!).child("\(time)")
//        let table2 = databaseReference.child("Messages").child(senderId!).child("\(receiverId!)").child("\(time-1)")
//        let table3 = databaseReference.child("Messages").child("\(receiverId!)").child(senderId!).child("\(time-1)")
        let senderKeySearch = currentUser?.userDoctor?.userInfo?.keySearchName
        
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd/MM/yyyy"
//        let dateString = dateFormatter.string(from: date)
        
//        let dateValue = ["from": Int(senderId) as Any, "message": dateString, "seen": false, "time": time, "type": "date"] as [String : Any]
        let value = ["from": Int(senderId) as Any, "message": text, "seen": false, "time": time, "type": "text"] as [String : Any]
        table.setValue(value)
        table1.setValue(value)
//        databaseReference.child("Messages").child(senderId!).child("\(receiverId!)").observeSingleEvent(of: .value, with: { (snapshot) in
//            if snapshot.exists() {
//                self.databaseReference.child("Messages").child(senderId!).child("\(self.receiverId!)").queryLimited(toLast: 1).observeSingleEvent(of: .childAdded, with: { (snapshot1) in
//                    self.databaseReference.child("Messages").child(senderId!).child("\(self.receiverId!)").child(snapshot1.key).observe(.value, with: { (snapshot2) in
//                        if let dict = snapshot2.value as? [String : Any] {
//                            let time = dict["time"] as! Double
//                            let timeDate = Date(timeIntervalSince1970: time/1000)
//                            let timeString = dateFormatter.string(from: timeDate)
//
//                            if dateString == timeString {
//                                table.setValue(value)
//                                table1.setValue(value)
//                            } else {
//                                table2.setValue(dateValue)
//                                table3.setValue(dateValue)
//
//                                table.setValue(value)
//                                table1.setValue(value)
//                            }
//                        }
//                    })
//                })
//
//            } else {
//                table2.setValue(dateValue)
//                table3.setValue(dateValue)
//
//                table.setValue(value)
//                table1.setValue(value)
//            }
//        })

        var value1 = [String : Any]()
        var value2 = [String : Any]()
        if let toAvatar = self.receiverAvatar {
            value1 = ["avatar" : "\(toAvatar)", "from" : Int(self.receiverId!), "message" : text, "name" : self.receiverName!,"searchKey" : self.receiverKeySearch as Any, "seen" : true, "status" : "receive", "timeStamp" : time, "orderTime" : 0 - time] as [String : Any]
        } else {
            value1 = ["avatar" : "nil", "from" : Int(self.receiverId!), "message" : text, "name" : self.receiverName!,"searchKey" : self.receiverKeySearch as Any, "seen" : true, "status" : "receive", "timeStamp" : time, "orderTime" : 0 - time] as [String : Any]
        }
        
        if let fromAvatar = self.currentUser?.userDoctor?.userInfo?.avatar {
            value2 = ["avatar" : "\(fromAvatar)", "from" : Int(senderId!) as Any, "message" : text, "name" : senderDisplayName!,"searchKey" : senderKeySearch as Any, "seen" : false, "status" : "receive", "timeStamp" : time, "orderTime" : 0 - time] as [String : Any]
        } else {
            value2 = ["avatar" : "nil", "from" : Int(senderId!) as Any, "message" : text, "name" : senderDisplayName!,"searchKey" : senderKeySearch as Any, "seen" : false, "status" : "receive", "timeStamp" : time, "orderTime" : 0 - time] as [String : Any]
        }
        
        databaseReference.child("Conversations").child(senderId!).child("\(time)").setValue(value1)
        databaseReference.child("Conversations").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.hasChild(senderId) {
                self.databaseReference.child("Conversations").child(senderId!).queryOrdered(byChild: "from").queryEqual(toValue: self.receiverId!).observe(.childAdded, with: { (snapshot) in
                    self.databaseReference.child("Conversations").child(senderId!).child(snapshot.key).removeValue()
                    self.databaseReference.child("Conversations").child(senderId!).queryOrdered(byChild: "from").queryEqual(toValue: self.receiverId!).removeAllObservers()
                    self.databaseReference.child("Conversations").child(senderId!).child(snapshot.key).removeAllObservers()
                    self.databaseReference.child("Conversations").child(senderId!).child("\(time)").setValue(value1)
                })
            } else {
                self.databaseReference.child("Conversations").child(senderId!).child("\(time)").setValue(value1)
            }
        })
        
        databaseReference.child("Conversations").child("\(self.receiverId!)").child("\(time)").setValue(value2)
        databaseReference.child("Conversations").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.hasChild("\(self.receiverId!)") {
                self.databaseReference.child("Conversations").child("\(self.receiverId!)").queryOrdered(byChild: "from").queryEqual(toValue: Int(senderId!)).observe(.childAdded, with: { (snapshot) in
                    self.databaseReference.child("Conversations").child("\(self.receiverId!)").child(snapshot.key).removeValue()
                    self.databaseReference.child("Conversations").child("\(self.receiverId!)").queryOrdered(byChild: "from").queryEqual(toValue: Int(senderId!)).removeAllObservers()
                    self.databaseReference.child("Conversations").child("\(self.receiverId!)").child(snapshot.key).removeAllObservers()
                    self.databaseReference.child("Conversations").child("\(self.receiverId!)").child("\(time)").setValue(value2)
                })
            }
            else {
                self.databaseReference.child("Conversations").child("\(self.receiverId!)").child("\(time)").setValue(value2)
            }
        })
        finishSendingMessage()
    }
    
    // when user press attach button
    override func didPressAccessoryButton(_ sender: UIButton!) {
        let alert = UIAlertController(title: "", message: "Chọn file", preferredStyle: .actionSheet)
        
        let action = UIAlertAction(title: "Chụp Ảnh", style: .default) { (action) in
            self.mediaPicker.sourceType = .camera
            //self.mediaPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
            self.present(self.mediaPicker, animated: true, completion: {
                self.mediaPicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .white
            })
        }
        
        let action1 = UIAlertAction(title: "Thư viện", style: .default) { (action) in
            self.mediaPicker.sourceType = .photoLibrary
            self.mediaPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.present(self.mediaPicker, animated: true, completion: {
                
                self.mediaPicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .white
                let toaster = Toast(text: "Vui lòng chọn video có dung lượng dưới 25Mb", delay: 0, duration: 3)
                toaster.show()
            })
        }
        
        let action2 = UIAlertAction(title: "Huỷ", style: .cancel, handler: nil)
        alert.addAction(action)
        alert.addAction(action1)
        alert.addAction(action2)
        self.present(alert, animated: true, completion: nil)
    }
    
    // create bubble
    lazy var outgoingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }()
    lazy var incomingBubble: JSQMessagesBubbleImage = {
        return JSQMessagesBubbleImageFactory()!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }()
    // show bubble
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        return Array_Messages[indexPath.item].senderId == senderId ? outgoingBubble : incomingBubble
    }
    
    func deleteMessage() {
    }
    
    // did tap bubble - play video
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = Array_Messages[indexPath.row]
        
        if message.isMediaMessage {
            
            if let videoItem = message.media as? JSQVideoMediaItem {
                
                let player = AVPlayer(url: videoItem.fileURL!)
                saveVideo = videoItem.fileURL!
                
                let longPress = UILongPressGestureRecognizer()
                longPress.addTarget(self, action: #selector(saveVideoToLibrary))
                longPress.cancelsTouchesInView = true
                cell.addGestureRecognizer(longPress)
                
                let playerViewCtrl = AVPlayerViewController()
                
                playerViewCtrl.player = player
                self.present(playerViewCtrl, animated: true, completion: nil)
            }
                
            else if message.media.isKind(of: JSQPhotoMediaItem.self) {
                
                let mediaItem = message.media
                if (mediaItem?.isKind(of: JSQPhotoMediaItem.self))! {
                    let photoItem : JSQPhotoMediaItem = mediaItem as! JSQPhotoMediaItem
                    
                    if let url = ClassMessages[indexPath.row].PhotoUrl {
                        savePhoto = URL(string: url)
                        let longPress = UILongPressGestureRecognizer()
                        longPress.addTarget(self, action: #selector(savePhotoToLibrary))
                        longPress.cancelsTouchesInView = true
                        cell.addGestureRecognizer(longPress)
                    }
                    if let images : UIImage = photoItem.image {
                        let photoSlider = PhotoSlider.ViewController(images: [images])
                        photoSlider.modalPresentationStyle = .overCurrentContext
                        photoSlider.modalTransitionStyle = .crossDissolve
                        present(photoSlider, animated: true, completion: nil)
                    }
                }
            }
        }
        else {
            let longPress = UILongPressGestureRecognizer()
            longPress.addTarget(self, action: #selector(savePhotoToLibrary))
            longPress.cancelsTouchesInView = true
            cell.textView.addGestureRecognizer(longPress)
        }
    }
    
    // when user longpress and save video to library
    @objc func saveVideoToLibrary() {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let action = UIAlertAction(title: "Lưu vào thư viện", style: .default) { (action) in
            let data = try? Data(contentsOf: self.saveVideo!)
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let filePath="\(documentsPath)/tempFile.mp4"
            DispatchQueue.main.async {
                try? data?.write(to: URL(fileURLWithPath: filePath), options: .atomic)
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                }) { saved, error in
                    if saved {
                        let alert = UIAlertController(title: "Lưu thành công", message: nil, preferredStyle: .alert)
                        let action = UIAlertAction(title: "Đồng ý", style: .default, handler: nil)
                        self.navigationItem.leftBarButtonItem?.isEnabled = true
                        alert.addAction(action)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        let action2 = UIAlertAction(title: "Huỷ", style: .cancel, handler: nil)
        alert.addAction(action)
        //alert.addAction(action1)
        alert.addAction(action2)
        self.present(alert, animated: true, completion: nil)
    }
    
    // when user longpress and save photo to library
    @objc func savePhotoToLibrary() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let action = UIAlertAction(title: "Lưu vào thư viện", style: .default) { (action) in
            let data = try? Data(contentsOf: self.savePhoto!)
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let filePath="\(documentsPath)/filename.jpg"
            DispatchQueue.main.async {
                try? data?.write(to: URL(fileURLWithPath: filePath), options: .atomic)
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAssetFromImage(atFileURL: URL(fileURLWithPath: filePath))
                }) { saved, error in
                    if saved {
                        let alert = UIAlertController(title: "Lưu thành công", message: nil, preferredStyle: .alert)
                        let action = UIAlertAction(title: "Đồng ý", style: .default, handler: nil)
                        self.navigationItem.leftBarButtonItem?.isEnabled = true
                        alert.addAction(action)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        
        let action2 = UIAlertAction(title: "Huỷ", style: .cancel, handler: nil)
        alert.addAction(action)
        //alert.addAction(action1)
        alert.addAction(action2)
        self.present(alert, animated: true, completion: nil)
    }
    
    lazy var outgoingAvatar: JSQMessagesAvatarImage = {
        if let avatar = self.senderAvatar {
            let url = API.baseURLImage + API.iCNMImage + "\(String(describing: self.senderId!))/\(avatar)".trim()
            if let data1 = try? Data(contentsOf: URL(string: url)!) {
                return JSQMessagesAvatarImageFactory.avatarImage(with: UIImage.sd_image(with: data1), diameter: 25)
            } else {
                return JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(named: "avatar_default"), diameter: 25)
            }
        }
        return JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(named: "avatar_default"), diameter: 25)
    }()
    
    lazy var incomingAvatar: JSQMessagesAvatarImage = {
        if let avatar = self.receiverAvatar {
            let url = API.baseURLImage + API.iCNMImage + "\(String(describing: self.receiverId!))/\(avatar)".trim()
            if let data2 = try? Data(contentsOf: URL(string: url)!) {
                return JSQMessagesAvatarImageFactory.avatarImage(with: UIImage.sd_image(with: data2), diameter: 25)
            } else {
                return JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(named: "avatar_default"), diameter: 25)
            }
        }
        return JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(named: "avatar_default"), diameter: 25)
    }()
    
    // avatar
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return Array_Messages[indexPath.item].senderId == senderId ? outgoingAvatar : incomingAvatar
    }
    
    // height between top label cell
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
        let dateMessage = ClassMessages[indexPath.row].Time.doubleValue
        var preDateMessage : Double?
        var preDate : Date?
        var preDateString : String?
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        if indexPath.item > 0 {
            preDateMessage = self.ClassMessages[indexPath.row - 1].Time.doubleValue
            preDate = Date(timeIntervalSince1970: preDateMessage!/1000)
            preDateString = dateFormatter.string(from: preDate!)
        }
        
        let date = Date(timeIntervalSince1970: dateMessage/1000)
        let dateString = dateFormatter.string(from: date)
        
        if dateString == preDateString {
            return 0
        } else {
            return 50
        }
    }
    
    // top label content of cell
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        
        // assign time label
        let second = ClassMessages[indexPath.row].Time.doubleValue
        let time = Date(timeIntervalSince1970: second/1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let string = dateFormatter.string(from: time)
        return NSAttributedString(string: string)
    }
    
    // bottom label height of cell
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        if isLastMess(indexpath: indexPath) {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        }
        return 0
    }
    
    //Check if it is the last message in all your messages
    //Check that the next message is not sent by the same person.
    func isLastMess(indexpath: IndexPath) -> Bool {
        if indexpath.item == ClassMessages.count - 1 {
            return true
        }
        else if ClassMessages[indexpath.item].SenderId == ClassMessages[indexpath.item + 1].SenderId {
            return false
        }
        else if ClassMessages[indexpath.item].SenderId != ClassMessages[indexpath.item + 1].ReceiverId {
            return false
        }
        return true
    }
    
    //    // bottom label content of cell
    //    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellBottomLabelAt indexPath: IndexPath!) -> NSAttributedString! {
    //
    //        let newMess = ClassMessages[indexPath.row]
    //        if newMess.SenderId == self.senderId!  {
    //            _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.checkIsOnline), userInfo: nil, repeats: false)
    //
    //            return NSAttributedString(string: isOnline)
    //        }
    //        return nil
    //    }
    
    // check when user typing
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        // If the text is not empty, the user is typing
        if textView.text != "" {
            isTyping = true
        } else {
            //databaseReference.child("TypingIndicator").child(self.senderId!).updateChildValues([receiverId! : false])
        }
    }
    lazy var userTypingReference = self.databaseReference.child("TypingIndicator").child(self.senderId!).child("\(self.receiverId!)")
    lazy var usersTypingQuery = self.databaseReference.child("TypingIndicator").child(self.senderId!).child("\(self.receiverId!)").queryOrderedByValue().queryEqual(toValue: true)
    private var localTyping = false
    var isTyping: Bool {
        get {
            return localTyping
        }
        set {
            localTyping = newValue
            databaseReference.child("TypingIndicator").child(self.senderId!).child("\(self.receiverId!)").setValue(newValue)
        }
    }
    private func checkUserIsTyping() {
        let typingIndicatorReference = databaseReference.child("TypingIndicator")
        userTypingReference = typingIndicatorReference.child(senderId!).child("\(self.receiverId!)")
        userTypingReference.onDisconnectRemoveValue()
        
        usersTypingQuery.observe(.value) { (data: DataSnapshot) in
            if data.childrenCount == 1 && self.isTyping {
                return
            }
            
            self.showTypingIndicator = data.childrenCount > 0
            self.scrollToBottom(animated: true)
        }
    }
    //    var getStatus = ""
    //    // check when user typing
    //    override func textViewDidChange(_ textView: UITextView) {
    //        super.textViewDidChange(textView)
    //        // If the text is not empty, the user is typing
    //        isTyping = textView.text != ""
    //    }
    //    private lazy var userTypingReference = self.databaseReference.child("TypingIndicator").child(self.senderId)
    //    private lazy var usersTypingQuery = self.databaseReference.child("TypingIndicator").queryOrderedByValue().queryEqual(toValue: true)
    //    private var localTyping = false
    //    var isTyping: Bool {
    //        get {
    //            return localTyping
    //        }
    //        set {
    //            localTyping = newValue
    //            userTypingReference.setValue(newValue)
    //        }
    //    }
    //    private func checkUserIsTyping() {
    //        let typingIndicatorReference = databaseReference.child("TypingIndicator")
    //        userTypingReference = typingIndicatorReference.child(senderId)
    //        userTypingReference.onDisconnectRemoveValue()
    //
    //        usersTypingQuery.observe(.value) { (data: DataSnapshot) in
    //            if data.childrenCount == 1 && self.isTyping {
    //                return
    //            }
    //
    //            self.showTypingIndicator = data.childrenCount > 0
    //            self.scrollToBottom(animated: true)
    //        }
    //    }
    
    // func when user send photo or video
    func sendMedia(photo : UIImage?, video : URL?) {
        
        // save photo to storage firebase
        if let photo = photo {
            
            JSQSystemSoundPlayer.jsq_playMessageSentSound()
            
            let filePath = "\(senderId!)/\(Date.timeIntervalSinceReferenceDate)"
            let data = UIImageJPEGRepresentation(photo, 0.3)
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpg"
            let progress = showProgress(view: myView!)
//            self.view.isUserInteractionEnabled = false
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            self.navigationItem.leftBarButtonItem?.isEnabled = false
            
            let uploadProgress = Storage.storage().reference().child(filePath).putData(data!, metadata: metadata) { (metadata, error) in
                
                if error != nil {
                    return
                }
                // save to database
                
                let PhotoUrl = metadata?.downloadURLs![0].absoluteString
                let date = Date()
                let time = Int((date.timeIntervalSince1970) * 1000)
                let table = self.databaseReference.child("Messages").child(self.senderId!).child("\(self.receiverId!)").child("\(time)")
                let table1 = self.databaseReference.child("Messages").child("\(self.receiverId!)").child(self.senderId!).child("\(time)")
                let senderKeySearch = self.currentUser?.userDoctor?.userInfo?.keySearchName
                
                let value = ["from": Int(self.senderId) as Any, "message": PhotoUrl as Any, "seen": false, "time": time, "type": "image"] as [String : Any]
                table.setValue(value)
                table1.setValue(value)
                
//                let table2 = self.databaseReference.child("MediaFiles").child(self.senderId!).child("\(self.receiverId!)").child("\(time)")
//                let table3 = self.databaseReference.child("MediaFiles").child("\(self.receiverId!)").child(self.senderId!).child("\(time)")
//                table2.setValue(value)
//                table3.setValue(value)
                
                var value1 = [String : Any]()
                var value2 = [String : Any]()
                if let toAvatar = self.receiverAvatar {
                    value1 = ["avatar" : "\(toAvatar)", "from" : Int(self.receiverId!), "message" : "Hình Ảnh", "name" : self.receiverName!,"searchKey" : self.receiverKeySearch as Any, "seen" : true, "status" : "receive", "timeStamp" : time, "orderTime" : 0 - time] as [String : Any]
                } else {
                    value1 = ["avatar" : "nil", "from" : Int(self.receiverId!), "message" : "Hình Ảnh", "name" : self.receiverName!,"searchKey" : self.receiverKeySearch as Any, "seen" : true, "status" : "receive", "timeStamp" : time, "orderTime" : 0 - time] as [String : Any]
                }
                
                if let fromAvatar = self.currentUser?.userDoctor?.userInfo?.avatar {
                    value2 = ["avatar" : "\(fromAvatar)", "from" : Int(self.senderId!) as Any, "message" : "Hình Ảnh", "name" : self.senderDisplayName!,"searchKey" : senderKeySearch as Any, "seen" : false, "status" : "receive", "timeStamp" : time, "orderTime" : 0 - time] as [String : Any]
                } else {
                    value2 = ["avatar" : "nil", "from" : Int(self.senderId!) as Any, "message" : "Hình Ảnh", "name" : self.senderDisplayName!,"searchKey" : senderKeySearch as Any, "seen" : false, "status" : "receive", "timeStamp" : time, "orderTime" : 0 - time] as [String : Any]
                }
                
                self.databaseReference.child("Conversations").child(self.senderId!).child("\(time)").setValue(value1)
                self.databaseReference.child("Conversations").child("\(self.receiverId!)").child("\(time)").setValue(value2)
                self.databaseReference.child("Conversations").observeSingleEvent(of: .value, with: { (snapshot) in
                    if snapshot.hasChild(self.senderId) {
                        self.databaseReference.child("Conversations").child(self.senderId!).queryOrdered(byChild: "from").queryEqual(toValue: self.receiverId!).observe(.childAdded, with: { (snapshot) in
                            self.databaseReference.child("Conversations").child(self.senderId!).child(snapshot.key).removeValue()
                            self.databaseReference.child("Conversations").child(self.senderId!).queryOrdered(byChild: "from").queryEqual(toValue: self.receiverId!).removeAllObservers()
                            self.databaseReference.child("Conversations").child(self.senderId!).child(snapshot.key).removeAllObservers()
                            self.databaseReference.child("Conversations").child(self.senderId!).child("\(time)").setValue(value1)
                        })
                        
                        self.databaseReference.child("Conversations").child("\(self.receiverId!)").queryOrdered(byChild: "from").queryEqual(toValue: self.receiverId!).observe(.childAdded, with: { (snapshot) in
                            self.databaseReference.child("Conversations").child("\(self.receiverId!)").child(snapshot.key).removeValue()
                            self.databaseReference.child("Conversations").child("\(self.receiverId!)").queryOrdered(byChild: "from").queryEqual(toValue: self.receiverId!).removeAllObservers()
                            self.databaseReference.child("Conversations").child("\(self.receiverId!)").child(snapshot.key).removeAllObservers()
                            self.databaseReference.child("Conversations").child("\(self.receiverId!)").child("\(time)").setValue(value2)
                        })
                    } else {
                        self.databaseReference.child("Conversations").child(self.senderId!).child("\(time)").setValue(value1)
                        self.databaseReference.child("Conversations").child("\(self.receiverId!)").child("\(time)").setValue(value2)
                    }
                })
                
                self.finishSendingMessage()
                
            }
            uploadProgress.observe(.success, handler: { (snpashot) in
                // finish load
                hideProgress(progress)
//                self.view.isUserInteractionEnabled = true
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                self.navigationItem.leftBarButtonItem?.isEnabled = true
            })
        }
        else if let video = video {
            
            JSQSystemSoundPlayer.jsq_playMessageSentSound()
            
            let filePath = "\((self.senderId)!)/\(Date.timeIntervalSinceReferenceDate)"
            let data = try? Data(contentsOf: video)
            let metadata = StorageMetadata()
            metadata.contentType = "video/mp4"
            let progress = showProgress(view: myView!)
            
//            self.view.isUserInteractionEnabled = false
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            self.navigationItem.leftBarButtonItem?.isEnabled = false
            
            Storage.storage().reference().child(filePath).putData(data!, metadata: metadata) { (metadata, error)
                in
                if error != nil {
                    return
                }
                else if (metadata?.size)! > 25000000 {
                    let sizeAlert = UIAlertController(title: "Thông báo", message: "Dung lượng video bạn gửi quá lớn. Vui lòng chọn video có độ dài dưới 1 phút", preferredStyle: .alert)
                    let sizeAction = UIAlertAction(title: "Đồng ý", style: .default, handler: { (action) in
                        Storage.storage().reference().child(filePath).delete(completion: nil)
                    })
                    sizeAlert.addAction(sizeAction)
                    self.present(sizeAlert, animated: true, completion: nil)
                    hideProgress(progress!)
                    
//                    self.view.isUserInteractionEnabled = true
                    self.navigationItem.rightBarButtonItem?.isEnabled = true
                    self.navigationItem.leftBarButtonItem?.isEnabled = true
                    
                }
                else {
                    let VideoUrl = metadata!.downloadURLs![0].absoluteString
                    
                    // get thumnail of video
                    let asset = AVAsset(url: URL(string: VideoUrl)!)
                    let assetImgGenerator = AVAssetImageGenerator(asset: asset)
                    assetImgGenerator.appliesPreferredTrackTransform = true
                    
                    let videoTime = CMTimeMake(1, 60)
                    let thumnailImg = try? assetImgGenerator.copyCGImage(at: videoTime, actualTime: nil)
                    
                    if let thumnailImg = thumnailImg {
                        let thumnailImmage = UIImage(cgImage: thumnailImg)
                        let filePath = "\(self.senderId!)/\(Date.timeIntervalSinceReferenceDate)"
                        let data = UIImageJPEGRepresentation(thumnailImmage, 0.1)
                        let metadata = StorageMetadata()
                        metadata.contentType = "image/jpg"
                        let uploadProgress = Storage.storage().reference().child(filePath).putData(data!, metadata: metadata) { (metadata, error) in
                            if error != nil {
                                hideProgress(progress)
                                return
                            }
                            // save to database with thumnail image
                            let thumnailImageUrl = metadata!.downloadURLs![0].absoluteString
                            let date = Date()
                            let time = Int((date.timeIntervalSince1970) * 1000)
                            let table = self.databaseReference.child("Messages").child(self.senderId!).child("\(self.receiverId!)").child("\(time)")
                            let table1 = self.databaseReference.child("Messages").child("\(self.receiverId!)").child(self.senderId!).child("\(time)")
                            let senderKeySearch = self.currentUser?.userDoctor?.userInfo?.keySearchName
                            
                            let value = ["from": Int(self.senderId) as Any, "videoUrl": VideoUrl, "seen": false, "time": time, "type": "video", "message" : thumnailImageUrl] as [String : Any]
                            
                            table.setValue(value)
                            table1.setValue(value)
                            
//                            let table2 = self.databaseReference.child("MediaFiles").child(self.senderId!).child("\(self.receiverId!)").child("\(time)")
//                            let table3 = self.databaseReference.child("MediaFiles").child("\(self.receiverId!)").child(self.senderId!).child("\(time)")
//                            table2.setValue(value)
//                            table3.setValue(value)
                            
                            var value1 = [String : Any]()
                            var value2 = [String : Any]()
                            if let toAvatar = self.receiverAvatar {
                                value1 = ["avatar" : "\(toAvatar)", "from" : Int(self.receiverId!), "message" : "Video", "name" : self.receiverName!,"searchKey" : self.receiverKeySearch as Any, "seen" : true, "status" : "receive", "timeStamp" : time, "orderTime" : 0 - time] as [String : Any]
                            } else {
                                value1 = ["avatar" : "nil", "from" : Int(self.receiverId!), "message" : "Video", "name" : self.receiverName!,"searchKey" : self.receiverKeySearch as Any, "seen" : true, "status" : "receive", "timeStamp" : time, "orderTime" : 0 - time] as [String : Any]
                            }
                            
                            if let fromAvatar = self.currentUser?.userDoctor?.userInfo?.avatar {
                                value2 = ["avatar" : "\(fromAvatar)", "from" : Int(self.senderId!) as Any, "message" : "Video", "name" : self.senderDisplayName!,"searchKey" : senderKeySearch as Any, "seen" : false, "status" : "receive", "timeStamp" : time, "orderTime" : 0 - time] as [String : Any]
                            } else {
                                value2 = ["avatar" : "nil", "from" : Int(self.senderId!) as Any, "message" : "Video", "name" : self.senderDisplayName!,"searchKey" : senderKeySearch as Any, "seen" : false, "status" : "receive", "timeStamp" : time, "orderTime" : 0 - time] as [String : Any]
                            }
                            
                            self.databaseReference.child("Conversations").child(self.senderId!).child("\(time)").setValue(value1)
                            self.databaseReference.child("Conversations").child("\(self.receiverId!)").child("\(time)").setValue(value2)
                            self.databaseReference.child("Conversations").observeSingleEvent(of: .value, with: { (snapshot) in
                                if snapshot.hasChild(self.senderId) {
                                    self.databaseReference.child("Conversations").child(self.senderId!).queryOrdered(byChild: "from").queryEqual(toValue: self.receiverId!).observe(.childAdded, with: { (snapshot) in
                                        self.databaseReference.child("Conversations").child(self.senderId!).child(snapshot.key).removeValue()
                                        self.databaseReference.child("Conversations").child(self.senderId!).queryOrdered(byChild: "from").queryEqual(toValue: self.receiverId!).removeAllObservers()
                                        self.databaseReference.child("Conversations").child(self.senderId!).child(snapshot.key).removeAllObservers()
                                        self.databaseReference.child("Conversations").child(self.senderId!).child("\(time)").setValue(value1)
                                    })
                                    
                                    self.databaseReference.child("Conversations").child("\(self.receiverId!)").queryOrdered(byChild: "from").queryEqual(toValue: self.receiverId!).observe(.childAdded, with: { (snapshot) in
                                        self.databaseReference.child("Conversations").child("\(self.receiverId!)").child(snapshot.key).removeValue()
                                        self.databaseReference.child("Conversations").child("\(self.receiverId!)").queryOrdered(byChild: "from").queryEqual(toValue: self.receiverId!).removeAllObservers()
                                        self.databaseReference.child("Conversations").child("\(self.receiverId!)").child(snapshot.key).removeAllObservers()
                                        self.databaseReference.child("Conversations").child("\(self.receiverId!)").child("\(time)").setValue(value2)
                                    })
                                } else {
                                    self.databaseReference.child("Conversations").child(self.senderId!).child("\(time)").setValue(value1)
                                    self.databaseReference.child("Conversations").child("\(self.receiverId!)").child("\(time)").setValue(value2)
                                }
                            })
                            
                            self.finishSendingMessage()
                            
                        }
                        uploadProgress.observe(.success, handler: { (snpashot) in
                            // finish load
                            hideProgress(progress)
//                            self.view.isUserInteractionEnabled = true
                            self.navigationItem.rightBarButtonItem?.isEnabled = true
                            self.navigationItem.leftBarButtonItem?.isEnabled = true
                        })
                    }
                }
            }
        }
    }
}

extension ChatViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let choosePhoto = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if #available(iOS 10.0, *) {
                choosePhoto.withHorizontallyFlippedOrientation()
                sendMedia(photo: choosePhoto, video: nil)
            } else {
                let photo = UIImage(cgImage: choosePhoto.cgImage!, scale: 1.0, orientation: .down)
                sendMedia(photo: photo, video: nil)
            }
        }
        else if let chooseVideo = info[UIImagePickerControllerMediaURL] as? URL {
            sendMedia(photo: nil, video: chooseVideo)
        }
        
        self.dismiss(animated: true, completion: nil)
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
