//
//  MediaSentViewController.swift
//  iCNM
//
//  Created by Thanh Huyen on 4/28/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import AVKit
import Photos
import Firebase
import FirebaseDatabase
import JSQMessagesViewController
import SDWebImage
import PhotoSlider
import RealmSwift

class MediaSentViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var avatarReceiver: UIImageView!
    
    var arrayImage = [Messages]()
    var arrayVideo = [Messages]()
    var arrayJSQImage = [JSQMessage]()
    var arrayJSQVideo = [JSQMessage]()
    
    var arrayAllImage = [Messages]()
    var arrayAllVideo = [Messages]()
    var arrayAllJSQImage = [JSQMessage]()
    var arrayAllJSQVideo = [JSQMessage]()
    
    var savePhoto : URL?
    var saveVideo : URL?
    
    var firstLoad = 6
    
    var myView : UIView?
    
    lazy var currentUser : User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    var senderId : String?
    var senderName : String?
    
    var receiverAvatar : String?
    var receiverId : Int?
    
    let databaseReference = Database.database().reference()
    
    @IBAction func btBack(_ sender: Any) {
        
        if let id = currentUser?.userDoctor?.userInfo?.id {
            senderId = String(id)
            
            databaseReference.child("Helpers").child("Conversations").child(senderId!).child("\(receiverId!)").updateChildValues(["isOnlineOnChat" : true])
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(MediaSentCollectionViewCell.self, forCellWithReuseIdentifier: "colllectionViewCell")
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        myView = self.navigationController?.view
        
        // assign value for image
        if let avatar = self.receiverAvatar {
            let url = API.baseURLImage + API.iCNMImage + "\(String(describing: self.receiverId!))/\(avatar)".trim()
            if let data = try? Data(contentsOf: URL(string: url)!) {
                avatarReceiver.image = UIImage(data: data)
            } else {
                avatarReceiver.image = UIImage(named: "avatar_default")
            }
        } else {
            avatarReceiver.image = UIImage(named: "avatar_default")
        }
        
        // call func
        getAllData()
        getData(numMess: firstLoad)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        arrayImage = [Messages]()
    }
    
    func getAllData() {
        guard let id = currentUser?.userDoctor?.userInfo?.id else {return}
        guard let userName = currentUser?.userDoctor?.userInfo?.name else {return}
        
        senderId = String(id)
        senderName = userName
        
        databaseReference.child("MediaFiles").child("\(senderId!)").child("\(receiverId!)").observe(.childAdded, with: { snapshot in
            if let dict = snapshot.value as? [String : Any] {
                let senderid = dict["from"] as? Int ?? 0
                let time = dict["time"] as? NSNumber ?? 0
                let mediatype = dict["type"] as? String ?? ""
                
                if mediatype == "image" {
                    
                    let photo = JSQPhotoMediaItem(image: nil)
                    let PhotoUrl = dict["message"] as? String ?? ""
                    
                    let downloader = SDWebImageDownloader.shared()
                    downloader.downloadImage(with: URL(string: PhotoUrl), options: [], progress: nil, completed: { (image, data, error, finished) in
                        
                        DispatchQueue.main.async(execute: {
                            photo?.image = image
                            self.collectionView.reloadData()
                        })
                    })
                    
                    let newJSQMedia = JSQMessage(senderId: "\(senderid)", displayName: self.senderName, media: photo)
                    
                    let newMess = Messages(TextString: "", SenderIdString: "\(senderid)", ReceiverIdString: "\(self.receiverId!)", TimeNumber: time)
                    
                    if newMess.SenderId == self.senderId! {
                        newMess.ChatId = newMess.ReceiverId
                    } else {
                        newMess.ChatId = newMess.SenderId
                    }
                    
                    newMess.PhotoUrl = PhotoUrl
                    
                    if newMess.ChatId == String(self.receiverId!) {
                        self.arrayAllImage.append(newMess)
                        self.arrayAllJSQImage.append(newJSQMedia!)
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                    }
                }
                    
                else if mediatype == "video" {
                    let thumnailImage = dict["message"] as! String
                    let thumnailImageUrl = URL(string: thumnailImage)
                    guard let data = try? Data(contentsOf: thumnailImageUrl!) else {return}
                    
                    let videoUrl = dict["videoUrl"] as? String ?? ""
                    let video = URL(string: videoUrl)
                    let videoItem = JSQVideoMediaItem(fileURL: video!, isReadyToPlay: true, thumbnailImage: UIImage(data: data))
                    
                    let newJSQMedia = JSQMessage(senderId: "\(senderid)", displayName: self.senderName, media: videoItem)
                    
                    let newMess = Messages(TextString: "", SenderIdString: "\(senderid)", ReceiverIdString: "\(self.receiverId!)", TimeNumber: time)
                    
                    if newMess.SenderId == self.senderId! {
                        newMess.ChatId = newMess.ReceiverId
                    } else {
                        newMess.ChatId = newMess.SenderId
                    }
                    
                    newMess.VideoUrl = videoUrl
                    newMess.ThumnailUrl = thumnailImage
                    
                    if newMess.ChatId == String(self.receiverId!) {
                        self.arrayAllVideo.append(newMess)
                        self.arrayAllJSQVideo.append(newJSQMedia!)
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                    }
                }
            }
        })
    }
    
    func getData(numMess: Int) {
        
        guard let id = currentUser?.userDoctor?.userInfo?.id else {return}
        guard let userName = currentUser?.userDoctor?.userInfo?.name else {return}

        senderId = String(id)
        senderName = userName
        
        let progress = showProgress(view: self.myView!)
        collectionView.isUserInteractionEnabled = false
        databaseReference.child("MediaFiles").child("\(senderId!)").child("\(receiverId!)").observe(.childAdded, with: { snapshot in
            //.queryLimited(toLast: UInt(numMess))
            if let dict = snapshot.value as? [String : Any] {
                let senderid = dict["from"] as? Int ?? 0
                let time = dict["time"] as? NSNumber ?? 0
                let mediatype = dict["type"] as? String ?? ""
                
                if mediatype == "image" {
                    
                    let photo = JSQPhotoMediaItem(image: nil)
                    let PhotoUrl = dict["message"] as? String ?? ""
                    
                    let downloader = SDWebImageDownloader.shared()
                    downloader.downloadImage(with: URL(string: PhotoUrl), options: [], progress: nil, completed: { (image, data, error, finished) in
                        
                        DispatchQueue.main.async(execute: {
                            photo?.image = image
                            self.collectionView.reloadData()
                        })
                    })
                    
                    let newJSQMedia = JSQMessage(senderId: "\(senderid)", displayName: self.senderName, media: photo)
                    
                    let newMess = Messages(TextString: "", SenderIdString: "\(senderid)", ReceiverIdString: "\(self.receiverId!)", TimeNumber: time)
                    
                    if newMess.SenderId == self.senderId! {
                        newMess.ChatId = newMess.ReceiverId
                    } else {
                        newMess.ChatId = newMess.SenderId
                    }
                    
                    newMess.PhotoUrl = PhotoUrl
                    
                    if newMess.ChatId == String(self.receiverId!) {
                        self.arrayImage.append(newMess)
                        self.arrayJSQImage.append(newJSQMedia!)
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                    }
                }
                    
                else if mediatype == "video" {
                    let thumnailImage = dict["message"] as! String
                    let thumnailImageUrl = URL(string: thumnailImage)
                    guard let data = try? Data(contentsOf: thumnailImageUrl!) else {return}
                    
                    let videoUrl = dict["videoUrl"] as? String ?? ""
                    let video = URL(string: videoUrl)
                    let videoItem = JSQVideoMediaItem(fileURL: video!, isReadyToPlay: true, thumbnailImage: UIImage(data: data))
                    
                    let newJSQMedia = JSQMessage(senderId: "\(senderid)", displayName: self.senderName, media: videoItem)
                    
                    let newMess = Messages(TextString: "", SenderIdString: "\(senderid)", ReceiverIdString: "\(self.receiverId!)", TimeNumber: time)
                    
                    if newMess.SenderId == self.senderId! {
                        newMess.ChatId = newMess.ReceiverId
                    } else {
                        newMess.ChatId = newMess.SenderId
                    }
                    
                    newMess.VideoUrl = videoUrl
                    newMess.ThumnailUrl = thumnailImage
                    
                    if newMess.ChatId == String(self.receiverId!) {
                        self.arrayVideo.append(newMess)
                        self.arrayJSQVideo.append(newJSQMedia!)
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                    }
                }
            }
            hideProgress(progress)
            self.collectionView.isUserInteractionEnabled = true
        })
        
        databaseReference.child("Messages").child("\(senderId!)").child("\(receiverId!)").observe(.value, with: { snapshot in
            if snapshot.exists() {
                return
            } else {
                hideProgress(progress)
            }
        })
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return arrayImage.count
        } else {
            return arrayVideo.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath) as! MediaSentCollectionViewCell
        if indexPath.section == 0 {
            if let url = URL(string: arrayImage[indexPath.row].PhotoUrl!) {
                DispatchQueue.main.async {
                     cell.imgMedia.sd_setImage(with: url, completed: nil)
                }
               
            }
            return cell
        } else {
            if let url = URL(string: arrayVideo[indexPath.row].ThumnailUrl!) {
                DispatchQueue.main.async {
                    cell.imgMedia.sd_setImage(with: url, completed: nil)
                }
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerTitle = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeaderView", for: indexPath) as! SectionHeaderView
        headerTitle.lbNumMess.layer.cornerRadius = 13
        headerTitle.lbNumMess.layer.masksToBounds = true
        
        if indexPath.section == 0 {
            headerTitle.lbHeader.text = "Ảnh"
            headerTitle.lbNumMess.text = String(arrayAllImage.count)
            
        } else {
            headerTitle.lbHeader.text = "Video"
            headerTitle.lbNumMess.text = String(arrayAllVideo.count)
            
        }
        return headerTitle
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        if indexPath.section == 0 {

                guard let url = URL(string: arrayImage[indexPath.row].PhotoUrl!) else {return}
                savePhoto = url
                
                let imageMess = arrayJSQImage[indexPath.row].media
                if (imageMess?.isKind(of: JSQPhotoMediaItem.self))! {
                    let photoItem = imageMess as! JSQPhotoMediaItem
                    if let images = photoItem.image {
                        let photoSlider = PhotoSlider.ViewController(images: [images])
                        photoSlider.modalPresentationStyle = .overCurrentContext
                        photoSlider.modalTransitionStyle = .crossDissolve
                        present(photoSlider, animated: true, completion: nil)
                    }
                }
                let longPress = UILongPressGestureRecognizer()
                longPress.addTarget(self, action: #selector(savePhotoToLibrary))
                cell?.addGestureRecognizer(longPress)
//            }
        } else {
            let videoMess = arrayJSQVideo[indexPath.row]
            if let videoItem = videoMess.media as? JSQVideoMediaItem {
                
                let player = AVPlayer(url: videoItem.fileURL!)
                saveVideo = videoItem.fileURL!
                let playerViewCtrl = AVPlayerViewController()
                playerViewCtrl.player = player
                self.present(playerViewCtrl, animated: true, completion: nil)
                
                let longPress = UILongPressGestureRecognizer()
                longPress.addTarget(self, action: #selector(saveVideoToLibrary))
                cell?.addGestureRecognizer(longPress)
                
            }
        }
    }
    
    @objc func saveVideoToLibrary() {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let action = UIAlertAction(title: "Lưu vào thư viện", style: .default) { (action) in
            self.navigationItem.leftBarButtonItem?.isEnabled = false
            let data = try? Data(contentsOf: self.saveVideo!)
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let filePath="\(documentsPath)/tempFile.mp4"
            DispatchQueue.main.async {
                try? data?.write(to: URL(fileURLWithPath: filePath), options: .atomic)
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                }) { saved, error in
                    if saved {
                        let alert = UIAlertController(title: "Lưu thành công", message: nil, preferredStyle: .alert)
                        let action = UIAlertAction(title: "Đồng ý", style: .default, handler: nil)
                        self.navigationItem.leftBarButtonItem?.isEnabled = true
                        alert.addAction(action)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        let action2 = UIAlertAction(title: "Huỷ", style: .cancel, handler: nil)
        alert.addAction(action)
        //alert.addAction(action1)
        alert.addAction(action2)
        self.present(alert, animated: true, completion: nil)
    }
    
    // when user longpress and save photo to library
    @objc func savePhotoToLibrary() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let action = UIAlertAction(title: "Lưu vào thư viện", style: .default) { (action) in
            self.navigationItem.leftBarButtonItem?.isEnabled = false
            let data = try? Data(contentsOf: self.savePhoto!)
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let filePath="\(documentsPath)/filename.jpg"
            DispatchQueue.main.async {
                try? data?.write(to: URL(fileURLWithPath: filePath), options: .atomic)
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAssetFromImage(atFileURL: URL(fileURLWithPath: filePath))
                }) { saved, error in
                    if saved {
                        let alert = UIAlertController(title: "Lưu thành công", message: nil, preferredStyle: .alert)
                        let action = UIAlertAction(title: "Đồng ý", style: .default, handler: nil)
                        self.navigationItem.leftBarButtonItem?.isEnabled = true
                        alert.addAction(action)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        let action2 = UIAlertAction(title: "Huỷ", style: .cancel, handler: nil)
        alert.addAction(action)
        //alert.addAction(action1)
        alert.addAction(action2)
        self.present(alert, animated: true, completion: nil)
    }
}

extension MediaSentViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
        return CGSize(width: collectionViewWidth/3, height: collectionViewWidth/3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}




