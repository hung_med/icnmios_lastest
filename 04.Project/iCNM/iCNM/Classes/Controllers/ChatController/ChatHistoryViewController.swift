//
//  ChatHistoryViewController.swift
//  iCNM
//
//  Created by Thành Trung on 13/04/2018.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import SDWebImage
import FirebaseDatabase
import RealmSwift
import ESPullToRefresh
import SVPullToRefresh

class ChatHistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    // outlet connect
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnLoadMoreDoctor: UIButton!
    @IBOutlet weak var lblMessage:UILabel!
    // user variable
    var Array_Messages = [Conversations]()
    var Array_MessDict = [String : Conversations]()
    
    var isSearching = false
    var filterData_Messages = [Conversations]()
    
    lazy var currentUser : User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    var senderid : Int?
    var senderName : String?
    var senderAvatar : String?
    
    var userOnline = [Int]()
    
    var loadPeople = 3
    var totalPeople = 0
    
    var myView : UIView?
    
    let databaseReference = Database.database().reference()
    var searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 250, height: 18))
    var searchOn : UIBarButtonItem!
    var searchOff : UIBarButtonItem!
    
    // button connect
    @IBAction func btBack(_ sender: Any) {
//        let mainvc = StoryboardScene.Main.mainTabBarController.instantiate()
//        let appDel = UIApplication.shared.delegate as! AppDelegate
//        appDel.window?.rootViewController = mainvc
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myView = self.navigationController?.view
        
        if #available(iOS 11.0, *) {
            tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        } else {
            tableView.contentInset = UIEdgeInsetsMake(-60, 0, 0, 0)
        }
        
        // create right bar button item
        searchOn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(isSearchOn))
        searchOff = UIBarButtonItem(title: "Hủy", style: .plain, target: self, action: #selector(isSearchOff))
        
        self.navigationItem.rightBarButtonItems = [self.searchOn]
        btnLoadMoreDoctor.layer.cornerRadius = 6.0
        btnLoadMoreDoctor.layer.masksToBounds = true
        btnLoadMoreDoctor.isHidden = true
        lblMessage.isHidden = true
        // create search bar
        self.tabBarController?.tabBar.isHidden = true
        searchBar.placeholder = "Tìm kiếm.."
        searchBar.tintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        searchBar.isHidden = true
        searchBar.delegate = self
        
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        getData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        DispatchQueue.main.async {
            self.tableView.reloadData()
            Loader.addLoaderTo(self.tableView)
            Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(ChatHistoryViewController.loaded), userInfo: nil, repeats: true)
        }
        if self.Array_Messages.count != 0{
            self.btnLoadMoreDoctor.isHidden = true
            self.lblMessage.isHidden = true
        }else{
            self.btnLoadMoreDoctor.isHidden = false
            self.lblMessage.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let id = senderid {
            databaseReference.child("Conversations").child("\(id)").observe(.childAdded, with: { (snapshot) in
                let key = snapshot.key
                self.databaseReference.child("Conversations").child("\(id)").child(key).observe(.value, with: { (snap) in
                    if snap.childrenCount == 1 {
                        self.databaseReference.child("Conversations").child("\(id)").child(snap.key).removeValue()
                    }
                })
            })
        }
    }
    
    func isSearchOn() {
        self.navigationItem.setRightBarButtonItems([self.searchOff], animated: false)
        self.navigationItem.titleView = searchBar
        searchBar.isHidden = false
        searchBar.becomeFirstResponder()
    }
    
    func isSearchOff() {
        searchBar.text = nil
        self.navigationItem.setRightBarButtonItems([self.searchOn], animated: false)
        self.navigationItem.titleView = nil
        if searchBar.text == "" {
            isSearching = false
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    
    // get data from user login
    func getData() {
        guard let id = currentUser?.userDoctor?.userInfo?.id else {return}
        guard let userName = currentUser?.userDoctor?.userInfo?.name else {return}
        
        senderid = id
        senderName = userName
        
        databaseReference.child("Conversations").child("\(senderid!)").observe(.childAdded, with: { (snapshot) in
            let key = snapshot.key
           
//            let progress = showProgress(view: self.myView!)
            self.databaseReference.child("Conversations").child("\(self.senderid!)").child(key).observeSingleEvent(of: .value, with: { (snapshot) in
                
                if let dict = snapshot.value as? [String : Any] {
                    let name = dict["name"] as? String ?? ""
                    let from = dict["from"] as? Int ?? 0
                    let avatar = dict["avatar"] as? String ?? "nil"
                    let message = dict["message"] as? String ?? ""
                    let keysearch = dict["searchKey"] as? String ?? ""
                    let seen = dict["seen"] as? Bool
                    let status = dict["status"] as? String ?? ""
                    let timestamp = dict["timeStamp"] as? NSNumber ?? 0
                    let ordertime = dict["orderTime"] as? NSNumber ?? 0
                    
                    let newConver = Conversations(avatarString: avatar, fromString: from, messageString: message, nameString: name, orderTimeNumber: ordertime, serachKeyString: keysearch, statusString: status, timeStampNumber: timestamp)
                    
                    newConver.seen = seen
                    
                    self.Array_Messages.append(newConver)
                    // group mess
                    
                    self.Array_MessDict[String(newConver.from)] = newConver
                    self.Array_Messages = Array(self.Array_MessDict.values)
                    
                    self.Array_Messages.sort(by: { (message1, message2) -> Bool in
                        return message1.timeStamp.int64Value > message2.timeStamp.int64Value
                    })
                }
//                hideProgress(progress)
            })
        })
    }
    
    func loaded()
    {
        Loader.removeLoaderFrom(self.tableView)
    }
    
    // table view protocol
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filterData_Messages.count
        }
        return Array_Messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell") as! ChatTableViewCell
        
        let newConver = Array_Messages[indexPath.row].seen
        if isSearching {
            
            if newConver == false {
                cell.lbUser.font = UIFont.boldSystemFont(ofSize: 18)
                //                cell.lbLastMess.font = UIFont.boldSystemFont(ofSize: 18)
                cell.lbLastMess.textColor = .black
            } else {
                cell.lbUser.font = UIFont.systemFont(ofSize: 15)
                cell.lbLastMess.font = UIFont.systemFont(ofSize: 15)
            }
            
            let id = filterData_Messages[indexPath.row].from
            self.databaseReference.child("Helpers").child("Users").child("\(id)").observe(.value, with: { (snapshot) in
                if snapshot.exists() {
                    if let dict = snapshot.value as? [String : Any] {
                        let isOnline = dict["isOnline"] as! Bool
                        if isOnline == true {
                            cell.isOnlineBtn.backgroundColor = UIColor.init(hex: "25D366")
                        } else {
                            cell.isOnlineBtn.backgroundColor = UIColor.init(hex: "AAAAAA")
                        }
                    }
                } else {
                    cell.isOnlineBtn.backgroundColor = UIColor.init(hex: "AAAAAA")
                }
            })
            
            let avatar = filterData_Messages[indexPath.row].avatar
            if avatar != "nil" {
                let urlString = API.baseURLImage + API.iCNMImage + "\(id)/\(filterData_Messages[indexPath.row].avatar)"
                
                if let url = URL(string: urlString.trim()) {
                    cell.imgAvatar.sd_setImage(with: url, completed: nil)
                }
            } else {
                cell.imgAvatar.image = UIImage(named: "avatar_default")
            }
            
            cell.lbUser.text! = filterData_Messages[indexPath.row].name
            cell.lbLastMess.text! = filterData_Messages[indexPath.row].message
            
            // assign time label
            let second = filterData_Messages[indexPath.row].timeStamp.doubleValue
            let time = Date(timeIntervalSince1970: second / 1000)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
            cell.lbTime.text! = time.timeAgoSinceNow()
            
        } else {
            if newConver == false {
                cell.lbUser.font = UIFont.boldSystemFont(ofSize: 18)
                cell.lbLastMess.textColor = .black
                
            } else {
                cell.lbUser.font = UIFont.systemFont(ofSize: 15)
                cell.lbLastMess.font = UIFont.systemFont(ofSize: 15)
            }
            
            let id = Array_Messages[indexPath.row].from
            self.databaseReference.child("Helpers").child("Users").child("\(id)").observe(.value, with: { (snapshot) in
                if snapshot.exists() {
                    if let dict = snapshot.value as? [String : Any] {
                        let isOnline = dict["isOnline"] as! Bool
                        if isOnline == true {
                            cell.isOnlineBtn.backgroundColor = UIColor.init(hex: "25D366")
                        } else {
                            cell.isOnlineBtn.backgroundColor = UIColor.init(hex: "AAAAAA")
                        }
                    }
                } else {
                    cell.isOnlineBtn.backgroundColor = UIColor.init(hex: "AAAAAA")
                }
            })
            
            let avatar = Array_Messages[indexPath.row].avatar
            if avatar != "nil" {
                let urlString = API.baseURLImage + API.iCNMImage + "\(id)/\(Array_Messages[indexPath.row].avatar)"
                if let url = URL(string: urlString.trim()) {
                    cell.imgAvatar.sd_setImage(with: url, completed: nil)
                }
            } else {
                cell.imgAvatar.image = UIImage(named: "avatar_default")
            }
            
            cell.lbUser.text! = Array_Messages[indexPath.row].name
            cell.lbLastMess.text! = Array_Messages[indexPath.row].message
            
            // assign time label
            let second = Array_Messages[indexPath.row].timeStamp.doubleValue
            let time = Date(timeIntervalSince1970: second / 1000)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
            cell.lbTime.text! = time.timeAgoSinceNow()
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        guard let id = currentUser?.userDoctor?.userInfo?.id else {return}
//        NotificationCenter.default.post(name: NSNotification.Name("UserDidLoginNotification"), object: nil, userInfo: ["userId": "\(String(describing: id))"])
        
        if isSearching {
            
            // get name and avatar
            let chatVC = StoryboardScene.Chat.chatVc.instantiate()
            chatVC.receiverId = filterData_Messages[(indexPath.row)].from
            chatVC.receiverName = filterData_Messages[indexPath.row].name
            chatVC.receiverAvatar = filterData_Messages[indexPath.row].avatar
            
            // get key search
            databaseReference.child("Users").child("\(filterData_Messages[(indexPath.row)].from)").observeSingleEvent(of: .value, with: { (snapshot) in
                if let dict = snapshot.value as? [String : Any] {
                    chatVC.receiverKeySearch = dict["KeySearch"] as! String
                }
            })
            
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(chatVC, animated: true)
            
        } else {
            // get name and avatar
            let chatVC = StoryboardScene.Chat.chatVc.instantiate()
            chatVC.receiverId = Array_Messages[(indexPath.row)].from
            chatVC.receiverName = Array_Messages[indexPath.row].name
            chatVC.receiverAvatar = Array_Messages[indexPath.row].avatar
            
            // get key search
            databaseReference.child("Users").child("\(Array_Messages[(indexPath.row)].from)").observeSingleEvent(of: .value, with: { (snapshot) in
                if let dict = snapshot.value as? [String : Any] {
                    chatVC.receiverKeySearch = dict["KeySearch"] as! String
                }
            })
            
            
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(chatVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let message = Array_Messages[indexPath.row]
            guard let fromId = senderid else {return}
            
            if let chatId = message.from as? Int {
                databaseReference.child("Conversations").child("\(fromId)").child("\(chatId)").removeValue(completionBlock: { (error, databaseRef) in
                    if error != nil {
                        print(error?.localizedDescription as Any)
                    }
                    self.Array_Messages.remove(at: indexPath.row)
                    self.tableView.deleteRows(at: [indexPath], with: .automatic)
                    
                    self.databaseReference.child("Conversations").child("\(fromId)").queryOrdered(byChild: "from").queryEqual(toValue: message.from).observe(.childAdded, with: { (snapshot) in
                        self.databaseReference.child("Conversations").child("\(fromId)").child(snapshot.key).removeValue()
                    })
                    
                    self.databaseReference.child("Messages").child("\(fromId)").child("\(message.from)").removeValue()
                })
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            isSearching = false
            view.endEditing(true)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        else {
            isSearching = true
            filterContentForSearchText(searchText)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filterData_Messages = Array_Messages.filter({(lookUp : Conversations) -> Bool in
            let receiverName = lookUp.name.folding(options: .diacriticInsensitive, locale: .autoupdatingCurrent)
            let keySearch = searchText.folding(options: .diacriticInsensitive, locale: .autoupdatingCurrent)
            return receiverName.lowercased().contains(keySearch.lowercased())
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func loadMoreDoctorHandler(_ sender: Any) {
        if currentUser != nil {
            let featuredDoctorVC = ListMoreFeaturedDoctorVC(nibName: "ListMoreFeaturedDoctorVC", bundle: nil)
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(featuredDoctorVC, animated: true)
        }else{
            confirmLogin(myView: self)
        }
    }
}

