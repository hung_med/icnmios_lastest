//
//  UnitDetailController.swift
//  MedlatecDemo
//
//  Created by Nguyen Van Dung on 7/25/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Toaster
import Cosmos
import GoogleMaps
import RealmSwift
import PopupDialog

protocol UnitDetailControllerDelegate {
    func savedCameraPositionDelegate(content:String)
}

class UnitDetailController: UIViewController, DoctorCellDelegate, UIViewControllerTransitioningDelegate, RatingVCDelegate {
    
    let indicatorSize:CGFloat = 28.0
    let loadingProgressLineWidth:CGFloat = 3.0
    
    var locationNearBy:LocationNearBy!
    var location:Location?
    var savedCameraPosition:GMSCameraPosition?
    var isHaveReview:Bool = false
    var delgate:UnitDetailControllerDelegate?
    //Reload comment list param
    var isExpandCommentList:Bool = false
    var numberOfComment:Int = 0
    var rate:Int = 2
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var cosmosViewCamNhanChung: CosmosView!
    @IBOutlet weak var cosmosViewChuyenMon: CosmosView!
    @IBOutlet weak var cosmosViewThaiDo: CosmosView!
    @IBOutlet weak var tvNoiDung: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set title
        self.title = self.locationNearBy.name
        self.tabBarController?.tabBar.isHidden = true
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        // Setup the Search Controller
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(UnitDetailController.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }else{
            let button: UIButton = UIButton(frame: CGRect(x: 40, y: 0, width: 40, height: 40))
            button.setImage(UIImage(named: "angle-left"), for: UIControlState.normal)
            button.addTarget(self, action: #selector(UnitDetailController.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        tableView.separatorColor = UIColor.clear
        
        //register nib
        registerNib()
        
        //notification
        let notiReloadRatingCell = Notification.Name("ReloadUserRatingCell")
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadUseRatingCell), name: notiReloadRatingCell, object: nil)
        
        let notificationShowRatingPopup = Notification.Name("showRatingPopup")
        NotificationCenter.default.addObserver(self, selector: #selector(self.showRatingForm), name: notificationShowRatingPopup, object: nil)
        
        let notificationShowProfileUser = Notification.Name("showProfileUser")
        NotificationCenter.default.addObserver(self, selector: #selector(self.showProfileUser), name: notificationShowProfileUser, object: nil)
 
        let showUserProfileNotification = Notification.Name("ShowUserProfileNotification")
        NotificationCenter.default.addObserver(self, selector: #selector(self.showUserProfileController), name: showUserProfileNotification, object: nil)
    }

    func back(sender: UIBarButtonItem) {
        let result = self.navigationController?.backToViewController(viewController: SearchVCViewController.self)
        if !result!{
            self.navigationController?.popToRootViewController(animated: true)
        }else{
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Location.getMedicalUnitDetail(locationID: (self.locationNearBy?.locationID)!) { (location, error) in
            self.location = location!
            
            let progress = self.showProgress()
            self.tableView.reloadData()
            self.hideProgress(progress)
        }
        //self.tabBarController?.tabBar.isHidden = false
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(handleLoginSuccess), name: Constant.NotificationMessage.loginSuccess, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleLogout), name: Constant.NotificationMessage.logout, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     * Show User profile controller
     */
    func showUserProfileController(notification: Notification) {
        if let userInfo = notification.object {
            self.showProfileCurrentUser(userInfo: userInfo as! UserInfo)
        }
    }
    
    func showProfileUser(notification: Notification){
        if let user = self.currentUser{
            self.showProfileCurrentUser(userInfo: (user.userDoctor?.userInfo)!)
        }
    }
    
    func showProfileCurrentUser(userInfo:UserInfo){
        let profileDoctorVC = ProfileDoctorVC(nibName: "ProfileDoctorVC", bundle: nil)
        let featuredDoctor = FeaturedDoctor()
        featuredDoctor.userInfo = userInfo
        featuredDoctor.doctor = nil
        featuredDoctor.specialists = nil
        profileDoctorVC.featuredDoctor = featuredDoctor
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(profileDoctorVC, animated: true)
    }
    
    /**
     * Function name: RegisterNib
     * Description: Register nib for tableview
     */
    func registerNib() -> Void {
        self.tableView.register(UINib(nibName: "CommonInforCell", bundle: Bundle.main), forCellReuseIdentifier: "CommonInforCell")
        
        self.tableView.register(UINib(nibName: "PhotoCell", bundle: Bundle.main), forCellReuseIdentifier: "PhotoCell")
        
        self.tableView.register(UINib(nibName: "DoctorCell", bundle: Bundle.main), forCellReuseIdentifier: "DoctorCell")
        
        tableView.register(UINib(nibName: "UserRatingCell", bundle: nil), forCellReuseIdentifier: "UserRatingCell")
        
        self.tableView.register(UINib(nibName: "CommentsCell", bundle: Bundle.main), forCellReuseIdentifier: "CommentsCell")
        
          self.tableView.register(UINib(nibName: "UserCommentCell", bundle: Bundle.main), forCellReuseIdentifier: "UserCommentCell")
        
        self.tableView.register(UINib(nibName: "NearByUnitCell", bundle: Bundle.main), forCellReuseIdentifier: "NearByUnitCell")
    }
    
    func reloadRatingUser(general: Double, ratingTechnique: Double, medicalEthics: Double, noidung: String, sumRate: Double) {
        let progress = self.showProgress()
        if self.currentUser != nil {
            if let user = self.currentUser {
                user.requestUserInformation(success: { (newUserInfo) in
                    
                    Review.userAddReview(rateContent: noidung, general: general, medicalEthics: medicalEthics, technique: ratingTechnique, userID: user.id, locationID: self.locationNearBy.locationID, sumRate: sumRate, success: { strRes in
                        
                        print(strRes)
                        let noti = Notification.Name("ChangeImageRated")
                        NotificationCenter.default.post(name: noti, object: nil)
                        
                        DispatchQueue.main.async {
                            Toast(text: "Cám ơn bạn đã đánh giá cho đơn vị y tế này").show()
                            self.isHaveReview = true
                            let notiPrivate = Notification.init(name: Notification.Name("ReloadUserRatingCell"))
                            self.postNotify(notification: notiPrivate)
                            
                            let reloadListLocation = Notification.init(name: Notification.Name("ReloadListLocation"))
                            self.postNotify(notification: reloadListLocation)
                            
                            self.hideProgress(progress)
                        }
                        
                    }, fail: { (error, response) in
                        print(error.localizedDescription)
                    })
                    
                    
                }, fail: { (error, response) in
                    print("\(error)")
                })
            }
        }
        
        self.goBackProfileDoctor()
    }
    
    func goBackProfileDoctor()
    {
        self .dismiss(animated: true) {
            
        }
    }
    
    /*
     * Show Rating Popup
     */
    func showRatingForm() {
        let ratingVC = RatingVC(nibName: "RatingVC", bundle: nil)
        ratingVC.delegate = self
        ratingVC.listUserComment = nil
        ratingVC.modalPresentationStyle = .custom
        ratingVC.transitioningDelegate = self
        self.present(ratingVC, animated: true, completion: nil)
    }
    
    /**
     * handleLoginSuccess
     */
    func handleLoginSuccess() {
        let realm = try! Realm()
        currentUser = realm.currentUser()
        
        self.tableView.reloadData()
    }
    
    func handleLogout(aNotification:Notification) {
        currentUser = nil
    }
}

extension UnitDetailController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {

        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section == 0) {
            let cellCommon = tableView.dequeueReusableCell(withIdentifier: "CommonInforCell", for: indexPath) as! CommonInforCell
            cellCommon.selectionStyle = .none
            cellCommon.delegate = self
            
            if let unit = self.location {
                self.fillDataForCellCommon(cell: cellCommon, medicalUnit: unit)
            }
            
            cellCommon.setMarkerForMap(lat: Float(self.locationNearBy.lat), long: Float(self.locationNearBy.long), name: self.locationNearBy.name, locationTypeID: self.locationNearBy.locationTypeID)
            
            cellCommon.layoutIfNeeded()
            return cellCommon
            
        } else if (indexPath.section == 1) {
            let photoCell = tableView.dequeueReusableCell(withIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
            photoCell.selectionStyle = .none
            if self.locationNearBy.imageStr != ""{
                let realImageUrl = self.locationNearBy.imageStr.components(separatedBy: ";") as [String]
                var result:[String]? = [String]()
                if !realImageUrl.isEmpty{
                    for url in realImageUrl {
                        if !url.isEmpty{
                            result?.append(url)
                        }
                    }
                    photoCell.showListImages(images: result!, locationID: self.locationNearBy.locationID)
                }
            }else{
                photoCell.showListImages(images: [""], locationID: self.locationNearBy.locationID)
            }
            return photoCell
        } else if (indexPath.section == 2) {
            
            let doctorCell = tableView.dequeueReusableCell(withIdentifier: "DoctorCell", for: indexPath) as! DoctorCell
            doctorCell.delegate = self
            doctorCell.getDataDoctor(locationID: self.locationNearBy.locationID)
            doctorCell.selectionStyle = .none
            return doctorCell
        } else if (indexPath.section == 3) {
            if (self.isHaveReview == true) {
                let userCommentCell = tableView.dequeueReusableCell(withIdentifier: "UserCommentCell", for: indexPath) as! UserCommentCell
                userCommentCell.selectionStyle = .none
                self.setDisplayRatingCell(cell: userCommentCell)
                return userCommentCell
            }
            
            let ratingCell = tableView.dequeueReusableCell(withIdentifier: "UserRatingCell", for: indexPath) as! UserRatingCell
            ratingCell.selectionStyle = .none
            ratingCell.currentUser = self.currentUser
            ratingCell.setAvatar()
            return ratingCell
            
        } else if (indexPath.section == 4) {
            let commentsCell = tableView.dequeueReusableCell(withIdentifier: "CommentsCell", for: indexPath) as! CommentsCell
            commentsCell.selectionStyle = .none
            commentsCell.delegate = self
            commentsCell.fillDataComment(locationID: self.locationNearBy.locationID)
            
            if (self.isExpandCommentList) {
                commentsCell.heightConstraintBtnViewAllComment.constant = 0
            }
            
            return commentsCell
            
        } else if (indexPath.section == 5) {
            let nearByUnitCell = tableView.dequeueReusableCell(withIdentifier: "NearByUnitCell", for: indexPath) as! NearByUnitCell
            nearByUnitCell.delegate = self
            nearByUnitCell.reloadListNearByUnit(lat: Float(self.locationNearBy.lat), long: Float(self.locationNearBy.long), rate:self.rate)
            
            return nearByUnitCell
        }
        
        return UITableViewCell();
    }
    
    func showProfileDoctor(doctor: Doctor) {
        /*let profileDoctorVC = ProfileDoctorVC(nibName: "ProfileDoctorVC", bundle: nil)
        let featuredDoctor = FeaturedDoctor()
        featuredDoctor.doctor = doctor
        featuredDoctor.userInfo = nil
        featuredDoctor.specialists = nil
        profileDoctorVC.featuredDoctor = featuredDoctor
        self.navigationController?.pushViewController(profileDoctorVC, animated: true)*/
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.section == 0) {
            return UITableViewAutomaticDimension
        } else if (indexPath.section == 1) {
            return 210
        } else if (indexPath.section == 2) {
            return 250
        } else if (indexPath.section == 3) {
            return 130
        } else if (indexPath.section == 4) {

            if (self.isExpandCommentList) {
                return CGFloat(110 * self.numberOfComment + 10)
            } else {
                return 155
            }
            
        } else if (indexPath.section == 5) {
            return 250
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.section == 0) {
            return UITableViewAutomaticDimension
        } else if (indexPath.section == 1) {
            return 210
        } else if (indexPath.section == 2) {
            return 250
        } else if (indexPath.section == 3) {
            return 130
        } else if (indexPath.section == 4) {
            if (self.isExpandCommentList) {
                return CGFloat(110 * self.numberOfComment + 10)
            } else {
                return 155
            }
        } else if (indexPath.section == 5) {
            return 250
        }
        
        return 0
    }
 

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if (section == 0) {
            return 0
        }
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (section == 1) {
            return "Hình ảnh"
        } else if (section == 2) {
            return "Bác sĩ"
        } else if (section == 3) {
            return "Đánh giá của bạn"
        } else if (section == 4) {
            return "Tất cả các đánh giá"
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if (section == 5) {
            let fr = CGRect(x: 0, y: 10, width: self.view.frame.size.width, height: 40)
            let headerButtonView = HeaderButtonView.init(frame: fr)
            
            headerButtonView.title.text = "Đơn vị y tế xung quanh";
            return headerButtonView;
        }
        
        return nil
    }
    
    // MARK: -- SET CELL DATA
    func fillDataForCellCommon(cell:CommonInforCell, medicalUnit: Location) -> Void {
        var urlImage = API.baseURLImage + "iCNMImage/" + "LocationImage/" +
            "\(medicalUnit.locationID)/" + medicalUnit.imageStr
        let realImageUrl = urlImage.components(separatedBy: ";")[0]
        let result = Int(splitImageLocationToString(str: medicalUnit.imageStr))
        if result != medicalUnit.locationID{
            urlImage = API.baseURLImage + "iCNMImage/" + "LocationImage/" + medicalUnit.imageStr
            let realImageUrl = urlImage.components(separatedBy: ";")[0]
            cell.imgMedicalUnit.sd_setImage(with: URL(string: realImageUrl), placeholderImage: UIImage(named: "default-thumbnail.png") )
            cell.imgMedicalUnit.contentMode = .scaleAspectFill
        }else{
            cell.imgMedicalUnit.sd_setImage(with: URL(string: realImageUrl), placeholderImage: UIImage(named: "default-thumbnail.png"))
            cell.imgMedicalUnit.contentMode = .scaleToFill
        }
        
        cell.lbMedicalName.text = medicalUnit.name
        cell.lbMedicalName.lineBreakMode = .byWordWrapping
        cell.lbMedicalName.numberOfLines = 0
        cell.lbRating.text = "\(Double(medicalUnit.averageRating).rounded(toPlaces: 1))"
        
        //rating star
        Common.sharedInstance.setRatingStar(imgViewStar1: cell.imgStar1, imgViewStar2: cell.imgStar2, imgViewStar3: cell.imgStar3, imgViewStar4: cell.imgStar4, imgViewStar5: cell.imgStar5, ratingScore: medicalUnit.averageRating)
        
        //set display count rating
        cell.setCountRating(locationID: self.locationNearBy.locationID)
        
        cell.lbAddress.text = medicalUnit.address
        cell.btnSchedule.addTarget(self, action: #selector(UnitDetailController.handlerSchedule(sender:)), for: UIControlEvents.touchUpInside)
        
        if (!medicalUnit.mobile.isEmpty) {
            cell.lbPhoneNumber.text = medicalUnit.mobile
            cell.btnCallPhone.tag = Int(medicalUnit.mobile)!
            cell.btnCallPhone.addTarget(self, action: #selector(UnitDetailController.handlerCallPhone(sender:)), for: UIControlEvents.touchUpInside)
        } else {
            cell.lbPhoneNumber.text = "Chưa cập nhật"
        }
        if (!medicalUnit.email.isEmpty) {
            cell.lbEmail.text = medicalUnit.email
        }else{
            cell.lbEmail.text = "Chưa cập nhật"
        }
        if (!medicalUnit.urlWebsite.isEmpty) {
            cell.lbWebsite.text = medicalUnit.urlWebsite
        }else{
            cell.lbWebsite.text = "Chưa cập nhật"
        }
 
        //working time
        if (!medicalUnit.workingTime.isEmpty) {
            cell.lbTimeOpen.text = medicalUnit.workingTime
        } else {
            cell.lbTimeOpen.text = "Chưa cập nhật"
        }
        
        //intro
        if let intro = try! medicalUnit.intro.convertHtmlSymbols(){
            cell.lbIntro.text = intro
        }else{
            cell.lbIntro.text = "Chưa cập nhật"
        }
        
        //distance
        if (self.locationNearBy?.distance != 0) {
            let distanceKm = self.locationNearBy.distance/1000
            cell.lbDistance.text = "\(Double(distanceKm).rounded(toPlaces: 2)) km"
        } else {
            cell.lbDistance.text = "Chưa cập nhật"
        }
        
        //display like status image
        if let user = currentUser {
            let progress = showProgress()
            user.requestUserInformation(success: { (newUserInfo) in
                self.hideProgress(progress)
                //Set image like status, review status
                Like.getLikeStatus(locationID: self.locationNearBy.locationID, userID: user.id) { (status, error) in
                    
                    if (status == 0 || status == 2) {
                        cell.imageBtnLike.image = UIImage(named: "like_btn_60")
                    } else if (status == 1) {
                        cell.imageBtnLike.image = UIImage(named: "ic_btn_favorited")
                    }
                    
                }
            }, fail: { (error, response) in
                self.hideProgress(progress)
            })
        }
        
        //Update Rated Status
        self.updateUserRatingStatus(imageView: cell.imageBtnReview)
    }
    
    func handlerCallPhone(sender: UIButton) {
        let phone = sender.tag
        let url: NSURL = URL(string: "TEL://\(phone)")! as NSURL
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    func handlerSchedule(sender: UIButton) {
        showAlertView(title: "Chức năng đang được xây dựng!", view: self)
        return
    }
    
    /**
     * Set data cell for comment
     */
    func setDisplayRatingCell(cell: UserCommentCell) {
        
        UserRating.getUserRating(locationID: self.locationNearBy.locationID) { (data, error) in
            
            if let user = self.currentUser {
                user.requestUserInformation(success: { (newUserInfo) in
                    for userRating in data! {
                        let userRated = userRating
                        
                        if (userRated.rating.userID == user.id) {
                            
                            //set user infor
                            cell.userInfo = userRated.user
                            
                            //*Start fill data
                            if userRating.user.avatar != nil{
                                let avatarStr = userRating.user.avatar!.trimmingCharacters(in: .whitespaces)
                                let avatarFullStringUrl = API.baseURLImage + "\(API.iCNMImage)" + "\(userRating.user.id)/\(avatarStr)"
                                
                                cell.imgUserAvatar.sd_setImage(with: URL(string: avatarFullStringUrl), placeholderImage: UIImage(named: "avatar_default"))
                            }else{
                                cell.imgUserAvatar.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "avatar_default"))
                            }
                            
                            //user
                            cell.lbNameOfUser.text = userRating.user.name
                            
                            //star
                            Common.sharedInstance.setRatingStar(imgViewStar1: cell.imgStar1, imgViewStar2: cell.imgStar2, imgViewStar3: cell.imgStar3, imgViewStar4: cell.imgStar4, imgViewStar5: cell.imgStar5, ratingScore: userRating.rating.sumRate)
                            
                            //rating
                            cell.lbTimeOpen.text = userRating.rating.dateCreate!.timeAgoSinceNow()
                            cell.lbCommentContent.text = userRating.rating.rateContent.trim()
                            //*End fill data
                        }
                        
                    }
                    
                }, fail: { (error, response) in
                    print("\(error)")
                })
            }
        }
        
    }
    
    
    // MARK: Reload section
    func reloadUseRatingCell() {
       
        Location.getMedicalUnitDetail(locationID: (self.locationNearBy?.locationID)!) { (location, error) in
            
            //renew location
            self.location = location!
            
            DispatchQueue.main.async {
                let progress = self.showProgress()
                self.tableView.reloadData()
                self.tableView.setContentOffset(CGPoint.zero, animated: true)
                self.hideProgress(progress)
            }
        }
        
    }
    
    // MARK: Update user rating status
    func updateUserRatingStatus(imageView: UIImageView) {
        UserRating.getUserRating(locationID: self.locationNearBy.locationID) { (data, error) in
            //self.listUserComment = data
            
            if self.currentUser != nil {
                if let user = self.currentUser {
                    user.requestUserInformation(success: { (newUserInfo) in
                        for userRating in data! {
                            let userRated = userRating
                            if (userRated.rating.userID == user.id) {
                                imageView.image = UIImage(named: "ic_btn_rated.png")
                                self.isHaveReview = true
                            }
                        }
                        
                    }, fail: { (error, response) in
                        print("\(error)")
                    })
                }
            }
        }
    }
    
    // MARK: Progress
    func showProgress(color:UIColor? = nil) -> MKActivityIndicator? {
        let loadingProgress = MKActivityIndicator(frame: CGRect(x:0, y:0, width: self.indicatorSize, height: self.indicatorSize))
        if let color = color {
            loadingProgress.color = color
        }
        loadingProgress.center = self.view.center
        loadingProgress.lineWidth = self.loadingProgressLineWidth
        loadingProgress.autoresizingMask = [.flexibleTopMargin, .flexibleLeftMargin, .flexibleBottomMargin, .flexibleRightMargin]
        view.addSubview(loadingProgress)
        loadingProgress.startAnimating()
        return loadingProgress
    }
    
    func hideProgress(_ loadingProgress:MKActivityIndicator?) {
        loadingProgress?.stopAnimating()
        loadingProgress?.removeFromSuperview()
    }
}

// MARK: NearByUnitCellDelegate
extension UnitDetailController:NearByUnitCellDelegate {
    func showMedicalUnitDetail(detailController: UnitDetailController) {
        self.navigationController?.pushViewController(detailController, animated: true)
    }
}

// MARK: 
extension UnitDetailController: CommonInforCellDelegate {
    func performLikeAction() {
        
        if self.currentUser == nil {
            confirmLogin(myView: self)
        } else {
            if let user = currentUser {
                let progress = showProgress()
                user.requestUserInformation(success: { (newUserInfo) in
                    self.hideProgress(progress)
                    
                    Like.getLikeStatus(locationID: self.locationNearBy.locationID, userID: user.id) { (status, error) in
                        
                        if (status == 2) {
                            Like.updateLikeStatus(locationID: self.locationNearBy.locationID, userID: user.id, completionHandler: { (status, error) in
                                //if status == 1 {
                                    DispatchQueue.main.async {
                                        Toast(text: "Đã yêu thích").show()
                                        
                                        let noti = Notification.init(name: Notification.Name("ChangeImageButtonLike"))
                                        self.postNotify(notification: noti)
                                    }
                                //}
                            })
                            
                        } else if (status == 0) {
                            Like.updateLikeStatus(locationID: self.locationNearBy.locationID, userID: user.id, completionHandler: { (status, error) in
                                if status == 1 {
                                    DispatchQueue.main.async {
                                        Toast(text: "Đã yêu thích").show()
                                        let noti = Notification.init(name: Notification.Name("ChangeImageButtonLike"))
                                        
                                        self.postNotify(notification: noti)
                                    }
                                }
                            })
                            
                        } else if (status == 1) {
                            Like.updateLikeStatus(locationID: self.locationNearBy.locationID, userID: user.id, completionHandler: { (status, error) in
                                if status == 0 {
                                    DispatchQueue.main.async {
                                        Toast(text: "Đã ngừng yêu thích").show()
                                        let noti = Notification.init(name: Notification.Name("ChangeImageButtonUnLike"))
                                        
                                        self.postNotify(notification: noti)
                                    }
                                }
                            })
                        }
                    }
                    
                }, fail: { (error, response) in
                    self.hideProgress(progress)
                })
            }
            
        }
        
    }
    
    //post notify
    func postNotify(notification: Notification) {
        let changeImageButtonLikeNoti = Notification.Name("ChangeImageButtonLike")
        let changeImageButtonUnLikeNoti = Notification.Name("ChangeImageButtonUnLike")
        let reloadSelectionNoti = Notification.Name("ReloadUserRatingCell")
        let reloadListLocationNoti = Notification.Name("ReloadListLocation")
       
        let changeImageRated = Notification.Name("ChangeImageRated")
        if (notification.name == changeImageButtonLikeNoti) {
            // Post notification
            NotificationCenter.default.post(name: changeImageButtonLikeNoti, object: nil)
        } else if (notification.name == changeImageButtonUnLikeNoti) {
            NotificationCenter.default.post(name: changeImageButtonUnLikeNoti, object: nil)
        } else if (notification.name == changeImageRated) {
            NotificationCenter.default.post(name: changeImageRated, object: nil)
        } else if (notification.name == reloadSelectionNoti) {
            NotificationCenter.default.post(name: reloadSelectionNoti, object: nil)
        } else if (notification.name == reloadListLocationNoti) {
            NotificationCenter.default.post(name: reloadListLocationNoti, object: nil)
        }
    }
    
    //Rating
    func performRatingAction() {
        if self.currentUser == nil {
            confirmLogin(myView: self)
        } else {
            if (self.isHaveReview == true) {
                Toast(text: "Bạn đã đánh giá đơn vị y tế này").show()
            } else {
                self.showRatingForm()
            }
        }
    }
    
    /**
     * Open Map Direction
     */
    func openMapDirection() {
        if savedCameraPosition != nil{
            let url = URL(string: "http://maps.apple.com/?saddr=\((savedCameraPosition?.target.latitude)!),\((savedCameraPosition?.target.longitude)!)&daddr=\(self.locationNearBy.lat),\(self.locationNearBy.long)")
            
            UIApplication.shared.openURL(url!)
        }else{
            showAlertView(title: "Không tìm được địa điểm này", view: self)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = tvNoiDung.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        return changedText.count <= 100
    }
}

extension UnitDetailController: CommentsCellDelegate {
    func reloadCommentList(numberOfComment: Int) {
        self.numberOfComment = numberOfComment
        self.isExpandCommentList = true
        tableView.reloadSections([4], with: .none)
    }
}
