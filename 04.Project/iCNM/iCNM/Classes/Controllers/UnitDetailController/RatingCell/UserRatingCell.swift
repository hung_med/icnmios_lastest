//
//  UserRatingCell.swift
//  MedlatecDemo
//
//  Created by Nguyen Van Dung on 7/28/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class UserRatingCell: UITableViewCell {
    
    @IBOutlet weak var imgAvatar: CustomImageView!
    @IBOutlet weak var imgChecked: UIImageView!
    @IBOutlet weak var viewStarRating: UIView!
    var currentUser:User? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        gestureRecognizer.numberOfTapsRequired = 1
        viewStarRating.addGestureRecognizer(gestureRecognizer)
        
        let gestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(self.handleTapAvatar))
        gestureRecognizer2.numberOfTapsRequired = 1
        imgAvatar.isUserInteractionEnabled = true
        imgAvatar.addGestureRecognizer(gestureRecognizer2)
    }
    
    func setAvatar(){
        self.imgAvatar.layer.masksToBounds = true
        self.imgAvatar.layer.cornerRadius = 25
        self.imgAvatar.layer.borderColor = UIColor(hexColor: 0x1D6EDC, alpha: 1.0).cgColor
        self.imgAvatar.layer.borderWidth = 2.0
        if let user = self.currentUser{
            if let avatar = user.userDoctor?.userInfo?.avatar{
                let avatarURL = API.baseURLImage + "\(API.iCNMImage)" + "\(user.id)" + "/\(avatar)"
                let urlString = avatarURL.trimmingCharacters(in: .whitespaces)
                imgAvatar.loadImageUsingUrlString(urlString: urlString)
            }else{
                imgAvatar.image = UIImage(named: "avatar_default")
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func handleTap(_ sender: UITapGestureRecognizer) {
        let notificationShowRatingPopup = Notification.Name("showRatingPopup")
        NotificationCenter.default.post(name: notificationShowRatingPopup , object: nil)
    }
    
    func handleTapAvatar(_ sender: UITapGestureRecognizer) {
        let notificationShowProfileUser = Notification.Name("showProfileUser")
        NotificationCenter.default.post(name: notificationShowProfileUser , object: nil)
    }
}
