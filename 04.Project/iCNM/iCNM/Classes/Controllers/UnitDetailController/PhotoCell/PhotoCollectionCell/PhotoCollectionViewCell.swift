//
//  PhotoCollectionViewCell.swift
//  MedlatecDemo
//
//  Created by Nguyen Van Dung on 7/26/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgPhoto: CustomImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imgPhoto.layer.masksToBounds = true
        imgPhoto.layer.cornerRadius = 5
    }

}
