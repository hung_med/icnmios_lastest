//
//  UserCommentCell.swift
//  MedlatecDemo
//
//  Created by Nguyen Van Dung on 7/28/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class UserCommentCell: UITableViewCell {
    
    @IBOutlet weak var imgUserAvatar: UIImageView!
    @IBOutlet weak var lbNameOfUser: UILabel!
        
    @IBOutlet weak var lbTimeOpen: UILabel!
    
    @IBOutlet weak var imgStar1: UIImageView!
    @IBOutlet weak var imgStar2: UIImageView!
    @IBOutlet weak var imgStar3: UIImageView!
    @IBOutlet weak var imgStar4: UIImageView!
    @IBOutlet weak var imgStar5: UIImageView!
    @IBOutlet weak var lbCommentContent: UILabel!
    
    var userInfo: UserInfo!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imgUserAvatar.layer.masksToBounds = true
        imgUserAvatar.layer.cornerRadius = 25
        imgUserAvatar.layer.borderColor = UIColor(hexColor: 0x1D6EDC, alpha: 1.0).cgColor
        imgUserAvatar.layer.borderWidth = 2.0
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageAvartarTapped))
        imgUserAvatar.isUserInteractionEnabled = true
        imgUserAvatar.addGestureRecognizer(tapGestureRecognizer)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func imageAvartarTapped() {
        let showUserProfileNotification = Notification.Name("ShowUserProfileNotification")
        
        //let userDict = ["userKey": self.userInfo]
        NotificationCenter.default.post(name: showUserProfileNotification, object: self.userInfo)
    }
    
}
