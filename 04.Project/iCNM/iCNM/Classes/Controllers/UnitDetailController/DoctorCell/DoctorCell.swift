//
//  DoctorCell.swift
//  MedlatecDemo
//
//  Created by Nguyen Van Dung on 7/27/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
protocol DoctorCellDelegate {
    func showProfileDoctor(doctor:Doctor)
}

class DoctorCell: UITableViewCell {

    var locationID:Int = 0
    var listDoctorNearBy:[Doctor]!
    var delegate:DoctorCellDelegate?
    @IBOutlet weak var collectionView: UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        //init list
        listDoctorNearBy = [Doctor]()
        
        self.collectionView.register(UINib(nibName: "DoctorCollectionCell", bundle: Bundle.main), forCellWithReuseIdentifier: "DoctorCollectionCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.emptyDataSetSource = self
        self.collectionView.emptyDataSetDelegate = self
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        self.collectionView.collectionViewLayout = layout
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //Get data
    func getDataDoctor(locationID:Int) -> Void {

        DoctorNearBy.getDoctorNearBy(locationID: locationID) { (data, error) in
            self.listDoctorNearBy = data
            self.collectionView.reloadData()
        }
    }
    
}

extension DoctorCell: UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listDoctorNearBy.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let doctor = self.listDoctorNearBy[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DoctorCollectionCell", for: indexPath) as! DoctorCollectionCell
        if let image = doctor.image{
            cell.imgDoctor.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "avatar_default"))
        }
        if let doctorName = doctor.doctorName{
            cell.lbDoctorName.text = doctorName
        }
        
        Common.sharedInstance.setRatingStar(imgViewStar1: cell.imgStar1, imgViewStar2: cell.imgStar2, imgViewStar3: cell.imgStar3, imgViewStar4: cell.imgStar4, imgViewStar5: cell.imgStar5, ratingScore: doctor.rating)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let doctor = self.listDoctorNearBy[indexPath.row]
        self.delegate?.showProfileDoctor(doctor: doctor)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 180, height: 250)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 6, bottom: 0, right: 6)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 6
    }
}

extension DoctorCell: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Không có bác sĩ nào"
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView) -> UIColor  {
        return UIColor.white
    }
}
