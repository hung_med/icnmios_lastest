//
//  DoctorCollectionCell.swift
//  MedlatecDemo
//
//  Created by Nguyen Van Dung on 7/27/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class DoctorCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imgDoctor: UIImageView!
    @IBOutlet weak var lbDoctorName: UILabel!
    
    @IBOutlet weak var imgStar1: UIImageView!
    @IBOutlet weak var imgStar2: UIImageView!
    @IBOutlet weak var imgStar3: UIImageView!
    @IBOutlet weak var imgStar4: UIImageView!
    @IBOutlet weak var imgStar5: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
