//
//  NearByUnitCell.swift
//  iCNM
//
//  Created by Nguyen Van Dung on 7/30/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import SDWebImage
import DZNEmptyDataSet

protocol NearByUnitCellDelegate {
    func showMedicalUnitDetail(detailController: UnitDetailController)
}

class NearByUnitCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var listNearByUnit:[LocationNearBy]!
    
    var delegate:NearByUnitCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.listNearByUnit = []
        
        self.collectionView.register(UINib(nibName: "CollectionNearByUnitCell", bundle: Bundle.main), forCellWithReuseIdentifier: "CollectionNearByUnitCell")
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.emptyDataSetSource = self
        self.collectionView.emptyDataSetDelegate = self
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        self.collectionView.collectionViewLayout = layout
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //-----
    func reloadListNearByUnit(lat: Float, long: Float, rate:Int) -> Void {
        
        let latStringOriginal = "\(lat)"
        let latStr = latStringOriginal.replacingOccurrences(of: ".", with: "-")
        
        let longStringOriginal = "\(long)"
        let longStr = longStringOriginal.replacingOccurrences(of: ".", with: "-")
        
        LocationNearBy.getLocationNearBy(lat: latStr, long: longStr, specialist: "Tất cả chuyên khoa", distance: 10, pageNumber: 1, pageRow: 10, type: "1-2-3-4-5", rate:rate) { (data, error) in
            
            self.listNearByUnit.removeAll()
            self.listNearByUnit.append(contentsOf: data!)
            self.collectionView.reloadData();
        }
    }
}

extension NearByUnitCell: UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listNearByUnit.count
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 180, height: 250)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 6, bottom: 0, right: 6)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 6
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let unit = self.listNearByUnit[indexPath.row];
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionNearByUnitCell", for: indexPath) as! CollectionNearByUnitCell
        var urlImage = API.baseURLImage + "iCNMImage/" + "LocationImage/" +
            "\(unit.locationID)/" + unit.imageStr
        let realImageUrl = urlImage.components(separatedBy: ";")[0]
        let result = Int(splitImageLocationToString(str: unit.imageStr))
        if result != unit.locationID{
            urlImage = API.baseURLImage + "iCNMImage/" + "LocationImage/" + unit.imageStr
            let realImageUrl = urlImage.components(separatedBy: ";")[0]
            cell.imgUnit.sd_setImage(with: URL(string: realImageUrl), placeholderImage: UIImage(named: "default-thumbnail.png") )
        }else{
            cell.imgUnit.sd_setImage(with: URL(string: realImageUrl), placeholderImage: UIImage(named: "default-thumbnail.png"))
        }
       
        //name
        cell.lbUnitName.text = unit.name
        cell.lbUnitName.lineBreakMode = .byWordWrapping
        cell.lbUnitName.numberOfLines = 0
        //location type name
        cell.lbLocationTypeName.text = unit.locationTypeName
        
        //location star
        Common.sharedInstance.setRatingStar(imgViewStar1: cell.imgStar1, imgViewStar2: cell.imgStar2, imgViewStar3: cell.imgStar3, imgViewStar4: cell.imgStar4, imgViewStar5: cell.imgStar5, ratingScore: unit.averageRating)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let location = self.listNearByUnit[indexPath.row];
        
        let unitDetailVC = UnitDetailController(nibName: "UnitDetailController", bundle: nil)
        unitDetailVC.locationNearBy = location
        unitDetailVC.title = location.name
        
        self.delegate?.showMedicalUnitDetail(detailController: unitDetailVC)
    }
}

extension NearByUnitCell: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Không có đơn vị y tế nào"
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView) -> UIColor  {
        return UIColor.white
    }
}

