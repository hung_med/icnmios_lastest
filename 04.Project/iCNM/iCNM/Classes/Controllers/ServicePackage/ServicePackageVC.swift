//
//  ServicePackageVC.swift
//  iCNM
//
//  Created by Mac osx on 11/6/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import FontAwesome_swift
class ServicePackageVC: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate, FilterServicePackageVCDelegate, UIPopoverPresentationControllerDelegate {
    var scopeSelected = ScrollableSegmentedControl()
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myView: UIView!
    var storedOffsets = [Int: CGFloat]()
    var serviceGroups: [ServiceGroups]?
    var serviceNames:[String]?
    var serviceByGroups: [ServiceByGroups]?
    var currentIndex:Int = 0
    let scrollView  = UIScrollView()
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    @IBOutlet weak var topConstr: NSLayoutConstraint!
    // MARK: Lifecycle
    
    let segmented = YSSegmentedControl(frame: .zero, titles: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        // Setup the Search Controller
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(ServicePackageVC.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        let btnButton =  UIButton(type: .custom)
        btnButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 15.0)
        let buttonName = String.fontAwesomeIcon(name: .filter)
        btnButton.setTitle(buttonName, for: .normal)
        btnButton.addTarget(self, action: #selector(ServicePackageVC.filterHandler), for: .touchUpInside)
        btnButton.frame = CGRect(x: screenSizeWidth-20, y: 0, width: 25, height:25)
        let filterBtnItem = UIBarButtonItem(customView: btnButton)
        self.navigationItem.setRightBarButtonItems([filterBtnItem], animated: true)
        
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        
        serviceNames = [String]()
        serviceGroups = [ServiceGroups]()
        serviceByGroups = [ServiceByGroups]()
        //self.reloadSegmentController(names: serviceNames!)
        //self.setupSwipAction()
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 200
        myTableView.rowHeight = UITableViewAutomaticDimension
        let nib = UINib(nibName: "ServicePackageCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        
        self.getAllServiceGroups()
        self.checkTrackingFeature()
    }
    
    /*
     * Set up swipe gesture for view
     */
    func setupSwipAction() -> Void {
        let swipeLeft = UISwipeGestureRecognizer.init(target: self, action: #selector(self.swipeLeftAndRight))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.myTableView.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer.init(target: self, action: #selector(self.swipeLeftAndRight))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.myTableView.addGestureRecognizer(swipeRight)
    }
    
    func swipeLeftAndRight(gestureRecognizer: UISwipeGestureRecognizer) {
        if gestureRecognizer.direction == .right{
            currentIndex -= 1
            if currentIndex < 0{
                currentIndex = 0
                return
            }
        }else{
            currentIndex += 1
            if currentIndex >= (self.serviceNames?.count)!{
                currentIndex = (self.serviceNames?.count)!
                return
            }
        }
        
        scopeSelected.selectedSegmentIndex = currentIndex
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func checkTrackingFeature(){
        if currentUser != nil{
            let userDefaults = UserDefaults.standard
            let token = userDefaults.object(forKey: "token") as! String
            TrackingFeature().checkTrackingFeature(fcm:token, featureName:Constant.PACKAGE_SERVICE, categoryID: 0, success: {(result) in
                if result != nil{
                    print("flow track done:", Constant.PACKAGE_SERVICE)
                }else{
                    print("flow track fail:", Constant.PACKAGE_SERVICE)
                }
            }, fail: { (error, response) in
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }else{
            return
        }
    }
    
    func filterHandler(_ sender: UIButton){
        let filterServicePackageVC = FilterServicePackageVC(nibName: "FilterServicePackageVC", bundle: nil)
        filterServicePackageVC.delegate = self
        filterServicePackageVC.modalPresentationStyle = .custom
        self.present(filterServicePackageVC, animated: true, completion: nil)
    }
    
    func gobackServicePackageVC(provinceID:Int, provinceName:String, gender:Int, age:Int, price:Int, priceName:String){
        let resultFilterServiceVC = ResultFilterServiceVC(nibName: "ResultFilterServiceVC", bundle: nil)
        resultFilterServiceVC.provinceVal = provinceID
        resultFilterServiceVC.provinceName = provinceName
        resultFilterServiceVC.gender = gender
        resultFilterServiceVC.ageVal = age
        resultFilterServiceVC.priceValue = price
        resultFilterServiceVC.priceName = priceName
        resultFilterServiceVC.resultFilterService(provinceID: provinceID, gender: gender, age: age, price: price)
        self.navigationController?.pushViewController(resultFilterServiceVC, animated: true)
    }

    
    func getAllServiceGroups(){
        ServiceGroups().getAllServiceGroups(success: { (data) in
            if data.count > 0 {
                self.serviceGroups?.append(contentsOf: data)
                for i in 0..<Int((self.serviceGroups?.count)!){
                    let name = data[i].name.uppercased()
                    self.serviceNames?.append(name)
                }
                self.reloadSegmentController(names: self.serviceNames!, currentTab: 0)
                self.myTableView.reloadData()
            }
        }, fail: { (error, response) in
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if IS_IPAD{
            topConstr.constant = 0
        }else{
            if #available(iOS 11.0, *) {
                
            }
            else{
                self.myView.frame = CGRect(x: 0, y: 0, width: myView.frame.size.width, height: myView.frame.size.height)
                self.myTableView.frame = CGRect(x: 0, y: 44, width: myTableView.frame.size.width, height: myTableView.frame.size.height+76)
            }
            
            if IS_IPHONE_X || IS_IPHONE_XS{
                self.myView.frame = CGRect(x: 0, y: 20, width: myView.frame.size.width, height: myView.frame.size.height)
                self.myTableView.frame = CGRect(x: 0, y: 108, width: myTableView.frame.size.width, height: myTableView.frame.size.height)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Gói khám/Dịch vụ")
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    func reloadSegmentController(names:[String], currentTab:Int){
       // let totalSize = self.calculateLongestTextWidth(names: names)
        if #available(iOS 11.0, *) {
            scopeSelected.frame = CGRect(x: 0, y: 38, width: screenSizeWidth, height: 38)
        }
        else{
            scopeSelected.frame = CGRect(x: 0, y: 34, width: screenSizeWidth, height: 30)
        }
        
        var indexCate: Int = 0
        for copeTitle in names {
            scopeSelected.insertSegment(withTitle: copeTitle, at: indexCate)
            indexCate += 1
        }
        scopeSelected.addTarget(self, action: #selector(ServicePackageVC.getServiceByGroups), for: .valueChanged)
        
        scopeSelected.segmentContentColor = UIColor.lightGray
        scopeSelected.underlineSelected = true
        scopeSelected.segmentStyle = .textOnly
        scopeSelected.backgroundColor = UIColor.clear
        scopeSelected.tintColor = UIColor(hex:"1976D2")
        // add it in a scrollView, because ill got too much categories here. Just if you need that:
        
        if #available(iOS 11.0, *) {
            scrollView.frame = CGRect(x:0, y: 34, width: screenSizeWidth, height: 100)
        }
        else{
            scrollView.frame = CGRect(x:0, y: 45, width: screenSizeWidth, height: 100)
        }
        
        scrollView.contentSize = CGSize(width: scopeSelected.frame.size.width + 16,height: scopeSelected.frame.size.height-1)
        
        scrollView.showsHorizontalScrollIndicator = false
        // set first category
        scopeSelected.selectedSegmentIndex = currentTab
        scrollView.addSubview(scopeSelected)
        myView?.addSubview(scrollView)
    }
    
    private func calculateLongestTextWidth(names:[String]?)->CGFloat {
        var totalSize:CGFloat = 0
        for str in names!{
            let fontAttributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
            let size = (str as NSString).size(attributes: fontAttributes)
            totalSize += size.width
        }
        return totalSize
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        title = "Gói khám/Dịch vụ"
        super.viewDidAppear(animated)
        if IS_IPAD{
            topConstr.constant = 0
        }else{
            if IS_IPHONE_X || IS_IPHONE_XS{
                self.myView.frame = CGRect(x: 0, y: 20, width: myView.frame.size.width, height: myView.frame.size.height)
                self.myTableView.frame = CGRect(x: 0, y: 108, width: myTableView.frame.size.width, height: myTableView.frame.size.height)
            }
        }
    }
    
    func switchChanged(_ sender: ScrollableSegmentedControl){
       // currentIndex = sender.selectedSegmentIndex
        self.myTableView .reloadData()
    }
    
    func getServiceByGroups(){
        let currentIndex = scopeSelected.selectedSegmentIndex
        self.reloadServiceFollowTab(index: currentIndex)
    }
    
    func reloadServiceFollowTab(index:Int){
        let serviceGroupObj = self.serviceGroups![index]
        let groupID = serviceGroupObj.serviceGroupID
        self.serviceByGroups = [ServiceByGroups]()
        self.myTableView.reloadData()
        let progress = self.showProgress()
        ServiceByGroups().getServiceByGroups(groupID: groupID, success: { (data) in
            if data.count > 0 {
                self.serviceByGroups?.append(contentsOf: data)
                self.myTableView.reloadData()
                self.hideProgress(progress)
            }else {
                self.hideProgress(progress)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (self.serviceByGroups?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ServicePackageCell
        cell.selectionStyle = .none
        
        let serviceByGroups = self.serviceByGroups![indexPath.row]
        
        if let serviceName = serviceByGroups.service?.serviceName{
            cell.lblServiceName.text = serviceName
        }
        
        if let serviceDesc = serviceByGroups.service?.serviceDes{
            cell.lblServiceDesc.text = serviceDesc
            cell.lblServiceDesc.lineBreakMode = .byWordWrapping
            cell.lblServiceDesc.numberOfLines = 0
        }
        
        let provinceName = serviceByGroups.provinceName
        cell.lblProvinceName.font = UIFont.fontAwesome(ofSize: 14.0)
        cell.lblProvinceName.text = String.fontAwesomeIcon(name: .mapMarker) + " \(provinceName)"
        cell.lblProvinceName.textColor = UIColor(hex:"1976D2")
        if let img = serviceByGroups.service?.img, let tmpServiceID = serviceByGroups.service?.serviceID{
            let serviceID = String(tmpServiceID)
            cell.imageThumb.contentMode = .scaleToFill
            let urlString = img.trimmingCharacters(in: .whitespaces)
            if urlString != "" || !urlString.isEmpty{
                let url = API.baseURLImage + "\(API.iCNMImage)" +
                    "/ServiceImage/\(serviceID)" + "/\(urlString)"
                cell.imageThumb.loadImageUsingUrlString(urlString: url)
            }
        }
        
        if let discount = serviceByGroups.service?.discount, let cost = serviceByGroups.service?.cost{
            if cost == 0 && discount == 0{
                cell.lblPercentDiscount.text = "0%"
            }else{
                let percent = Float(1.0 - Float (discount) / Float (cost))
                cell.lblPercentDiscount.font = UIFont.fontAwesome(ofSize: 13.0)
                cell.lblPercentDiscount.text = String.fontAwesomeIcon(name: .caretDown) + " " + String(format: "%.f", percent*100) + "%"
            }
        }
        
        if let sex = serviceByGroups.service?.sex{
            if sex == 0{
                cell.lblSex.font = UIFont.fontAwesome(ofSize: 14.0)
                cell.lblSex.text = String.fontAwesomeIcon(name: .female) + "  Nữ"
                cell.lblSex.textColor = UIColor(hex:"1976D2")
            }else if sex == 1{
                cell.lblSex.font = UIFont.fontAwesome(ofSize: 14.0)
                cell.lblSex.text = String.fontAwesomeIcon(name: .male) + "  Nam"
                cell.lblSex.textColor = UIColor(hex:"1976D2")
            }else if sex == 2{
                cell.lblSex.font = UIFont.fontAwesome(ofSize: 14.0)
                cell.lblSex.text = String.fontAwesomeIcon(name: .male) + "  Nam - " + String.fontAwesomeIcon(name: .female) + "  Nữ "
                cell.lblSex.textColor = UIColor(hex:"1976D2")
            }
        }
        
        if let costFe = serviceByGroups.service?.cost{
            let format = formatString(value: Float(costFe))
            cell.lblCountFe.text = "\(format)"
        }
        
        if let discostFe = serviceByGroups.service?.discount{
            let format = formatString(value: Float(discostFe))
            cell.lblDiscountFe.text = "\(format)"
        }
        
        if let costFe = serviceByGroups.service?.cost{
            let result = formatAttributeString(value:costFe)
            cell.lblCountFe.attributedText = result
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if serviceByGroups!.count > 0{
             return 170
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let serviceByGroupsObj = self.serviceByGroups![indexPath.row]
        let servicePackageDetailVC = ServicePackageDetailVC(nibName: "ServicePackageDetailVC", bundle: nil)
            if let serviceID = serviceByGroupsObj.service?.serviceID{
                servicePackageDetailVC.isCheck = false
                servicePackageDetailVC.serviceName = (serviceByGroupsObj.service?.serviceName)!
                servicePackageDetailVC.getAllServiceDetailGroups(serviceID:Int(serviceID))
            }
        self.navigationController?.pushViewController(servicePackageDetailVC, animated: true)
    }
}


