//
//  ResultFilterServiceVC.swift
//  iCNM
//
//  Created by Mac osx on 11/17/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class ResultFilterServiceVC: BaseViewControllerNoSearchBar, FilterServicePackageVCDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var lblProvince: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myView: UIView!
    var services: [ListCustomServiceDetail]?
    var storedOffsets = [Int: CGFloat]()
    var gender:Int = 0
    var priceValue:Int? = 0
    var priceName:String? = nil
    var ageVal:Int? = 0
    var provinceVal:Int? = 0
    var provinceName:String? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        services = [ListCustomServiceDetail]()
        self.tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        // Setup the Search Controller
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(ResultFilterServiceVC.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        let btnButton =  UIButton(type: .custom)
        btnButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 15.0)
        let buttonName = String.fontAwesomeIcon(name: .filter)
        btnButton.setTitle(buttonName, for: .normal)
        btnButton.addTarget(self, action: #selector(ResultFilterServiceVC.filterHandler), for: .touchUpInside)
        btnButton.frame = CGRect(x: screenSizeWidth-20, y: 0, width: 25, height:25)
        let filterBtnItem = UIBarButtonItem(customView: btnButton)
        self.navigationItem.setRightBarButtonItems([filterBtnItem], animated: true)
        
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 200
        myTableView.rowHeight = UITableViewAutomaticDimension
       // myTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        let nib = UINib(nibName: "ServicePackageCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        
        self.lblProvince.text = provinceName
        
        if self.gender == -1{
            self.lblGender.text = "Tất cả"
        }else if self.gender == 1{
            self.lblGender.text = "Nam"
        }else{
            self.lblGender.text = "Nữ"
        }
        if self.ageVal == -1{
            self.lblAge.text = "Tất cả"
        }else{
            self.lblAge.text = "\(self.ageVal!)"
        }
        if self.priceValue == -1{
            self.lblPrice.text = "Tất cả"
        }else{
            self.lblPrice.text = priceName
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if IS_IPHONE_X{
            self.myView.frame = CGRect(x: 0, y: 90, width: myView.frame.size.width, height: myView.frame.size.height)
            
            self.myTableView.frame = CGRect(x: 0, y: 143, width: myTableView.frame.size.width, height: myTableView.frame.size.height)
        }
    }
    
    func back(sender: UIBarButtonItem) {
        let result = self.navigationController?.backToViewController(viewController: ServicePackageVC.self)
        if !result!{
            print("back done")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        
        title = "Kết quả lọc"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // get previous view controller and set title to "" or any things You want
        if let viewControllers = self.navigationController?.viewControllers {
            let previousVC: UIViewController? = viewControllers.count >= 2 ? viewControllers[viewControllers.count - 2] : nil;
            previousVC?.title = ""
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func filterHandler(_ sender: UIButton){
        let filterServicePackageVC = FilterServicePackageVC(nibName: "FilterServicePackageVC", bundle: nil)
        filterServicePackageVC.delegate = self
        filterServicePackageVC.modalPresentationStyle = .custom
        self.present(filterServicePackageVC, animated: true, completion: nil)
    }
    
    func gobackServicePackageVC(provinceID: Int, provinceName: String, gender: Int, age: Int, price: Int, priceName: String) {
        let resultFilterServiceVC = ResultFilterServiceVC(nibName: "ResultFilterServiceVC", bundle: nil)
        resultFilterServiceVC.provinceVal = provinceID
        resultFilterServiceVC.provinceName = provinceName
        resultFilterServiceVC.gender = gender
        resultFilterServiceVC.ageVal = age
        resultFilterServiceVC.priceValue = price
        resultFilterServiceVC.priceName = priceName
        resultFilterServiceVC.resultFilterService(provinceID: provinceID, gender: gender, age: age, price: price)
        self.navigationController?.pushViewController(resultFilterServiceVC, animated: true)
    }
    
    func reloadTableView() {
        
    }
    
    func resultFilterService(provinceID:Int, gender:Int, age:Int, price:Int){
        let progress = self.showProgress()
        ListCustomServiceDetail().getFindService(provinceID: provinceID, gender: gender, age:age, price:price, success: { (data) in
            if data.count > 0{
                self.services = data
                self.myTableView.reloadData()
                self.hideProgress(progress)
            } else {
                self.hideProgress(progress)
                let lblWarning = createUILabel()
                self.myTableView.addSubview(lblWarning)
            }
            
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (self.services?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ServicePackageCell
        cell.selectionStyle = .none
        let serviceByGroups = self.services![indexPath.row]
        
        if let serviceName = serviceByGroups.service?.serviceName{
            cell.lblServiceName.text = serviceName
            cell.lblServiceName.lineBreakMode = .byWordWrapping
            cell.lblServiceName.numberOfLines = 0
        }
        
        if let serviceDesc = serviceByGroups.service?.serviceDes{
            cell.lblServiceDesc.text = serviceDesc
            cell.lblServiceDesc.lineBreakMode = .byWordWrapping
            cell.lblServiceDesc.numberOfLines = 0
        }
        
        let provinceName = serviceByGroups.provinceName
        cell.lblProvinceName.font = UIFont.fontAwesome(ofSize: 14.0)
        cell.lblProvinceName.text = String.fontAwesomeIcon(name: .mapMarker) + " \(provinceName)"
        cell.lblProvinceName.textColor = UIColor(hex:"1976D2")
        if let img = serviceByGroups.service?.img, let tmpServiceID = serviceByGroups.service?.serviceID{
            let serviceID = String(tmpServiceID)
            cell.imageThumb.contentMode = .scaleAspectFit
            let urlString = img.trimmingCharacters(in: .whitespaces)
            if urlString != "" || !urlString.isEmpty{
                let url = API.baseURLImage + "\(API.iCNMImage)" +
                    "/ServiceImage/\(serviceID)" + "/\(urlString)"
                cell.imageThumb.loadImageUsingUrlString(urlString: url)
            }
        }
        
        if let discount = serviceByGroups.service?.discount, let cost = serviceByGroups.service?.cost{
            if cost == 0 && discount == 0{
                cell.lblPercentDiscount.text = "0%"
            }else{
                let percent = Float(1.0 - Float (discount) / Float (cost))
                cell.lblPercentDiscount.font = UIFont.fontAwesome(ofSize: 13.0)
                cell.lblPercentDiscount.text = String.fontAwesomeIcon(name: .caretDown) + " " + String(format: "%.f", percent*100) + "%"
            }
        }
        
        if let sex = serviceByGroups.service?.sex{
            if sex == 0{
                cell.lblSex.font = UIFont.fontAwesome(ofSize: 14.0)
                cell.lblSex.text = String.fontAwesomeIcon(name: .female) + "  Nữ"
                cell.lblSex.textColor = UIColor(hex:"1976D2")
            }else if sex == 1{
                cell.lblSex.font = UIFont.fontAwesome(ofSize: 14.0)
                cell.lblSex.text = String.fontAwesomeIcon(name: .male) + "  Nam"
                cell.lblSex.textColor = UIColor(hex:"1976D2")
            }else if sex == 2{
                cell.lblSex.font = UIFont.fontAwesome(ofSize: 14.0)
                cell.lblSex.text = String.fontAwesomeIcon(name: .male) + "  Nam - " + String.fontAwesomeIcon(name: .female) + "  Nữ "
                cell.lblSex.textColor = UIColor(hex:"1976D2")
            }
        }
        
        if let costFe = serviceByGroups.service?.cost{
            let format = formatString(value: Float(costFe))
            cell.lblCountFe.text = "\(format)"
        }
        
        if let discostFe = serviceByGroups.service?.discount{
            let format = formatString(value: Float(discostFe))
            cell.lblDiscountFe.text = "\(format)"
        }
        
        if let costFe = serviceByGroups.service?.cost{
            let result = formatAttributeString(value:costFe)
            cell.lblCountFe.attributedText = result
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if self.services!.count > 0{
            return 170
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let serviceByGroupsObj = self.services![indexPath.row]
        let servicePackageDetailVC = ServicePackageDetailVC(nibName: "ServicePackageDetailVC", bundle: nil)
        if let serviceID = serviceByGroupsObj.service?.serviceID{
            servicePackageDetailVC.isCheck = false
            servicePackageDetailVC.serviceName = (serviceByGroupsObj.service?.serviceName)!
            servicePackageDetailVC.getAllServiceDetailGroups(serviceID:Int(serviceID))
        }
        self.navigationController?.pushViewController(servicePackageDetailVC, animated: true)
    }
}
