//
//  FilterServicePackageVC.swift
//  iCNM
//
//  Created by Mac osx on 11/17/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
protocol FilterServicePackageVCDelegate {
    func gobackServicePackageVC(provinceID:Int, provinceName:String, gender:Int, age:Int, price:Int, priceName:String)
}
class FilterServicePackageVC: UIViewController, FWComboBoxDelegate {

    @IBOutlet weak var radio_Nu: ISRadioButton!
    @IBOutlet weak var radio_Nam: ISRadioButton!
    @IBOutlet weak var sliderAge: UISlider!
    @IBOutlet weak var ageValue: UILabel!
    @IBOutlet weak var price1: ISRadioButton!
    @IBOutlet weak var price2: ISRadioButton!
    @IBOutlet weak var price3: ISRadioButton!
    @IBOutlet weak var price4: ISRadioButton!
    var delegate: FilterServicePackageVCDelegate?
    var gender:Int = -1
    var priceValue:Int? = -1
    var priceName:String? = "Tất cả"
    var ageVal:Int? = -1
    var provinceVal:Int? = -1
    var provinceName:String? = "Tất cả"
    @IBOutlet weak var provinceComboBox: FWComboBox!
    public var currentProvinceIndex:Int? = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        // Do any additional setup after loading the view.
        sliderAge.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
        sliderAge.addTarget(self, action: #selector(sliderDidEndSliding), for: [.touchUpInside, .touchUpOutside])
        provinceComboBox.delegate = self
        provinceComboBox.layer.borderWidth = 1
        provinceComboBox.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        provinceComboBox.layer.cornerRadius = 4
        provinceComboBox.layer.masksToBounds = true
        //provinceComboBox.defaultTitle = "Tỉnh/TP"
        requestData()
    }
    
    private var provinces:[Province]? {
        didSet {
            populateProvinceComboBox()
        }
    }

    func sliderDidEndSliding() {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func populateProvinceComboBox() {
        if let provinces = self.provinces {
            let activeProvinces = provinces.filter({ (province) -> Bool in
                return province.active
            })
            provinceComboBox.dataSource = activeProvinces.map({ (province) -> String in
                return province.name ?? ""
            })

            provinceComboBox.selectRow(at: currentProvinceIndex)

        } else {
            provinceComboBox.selectRow(at: nil)
            provinceComboBox.dataSource = [String]()
        }
    }
    
    @IBAction func touchGenderRadioButton(_ sender: ISRadioButton) {
        if sender == radio_Nam {
            gender = 1
            radio_Nam.isSelected = true
            radio_Nu.isSelected = false
        } else {
            gender = 0
            radio_Nu.isSelected = true
            radio_Nam.isSelected = false
        }
    }
    
    @IBAction func touchPriceRadioButton(_ sender: ISRadioButton) {
        if sender == price1 {
            priceValue = 1
            priceName = "< 1 triệu"
            price1.isSelected = true
            price2.isSelected = false
            price3.isSelected = false
            price4.isSelected = false
        } else if sender == price2 {
            priceValue = 12
            priceName = "Từ 1 đến 2 triệu"
            price1.isSelected = false
            price2.isSelected = true
            price3.isSelected = false
            price4.isSelected = false
        } else if sender == price3 {
            priceValue = 23
            priceName = "Từ 2 đến 3 triệu"
            price1.isSelected = false
            price2.isSelected = false
            price3.isSelected = true
            price4.isSelected = false
        } else{
            priceValue = 3
            priceName = "> 3 triệu"
            price1.isSelected = false
            price2.isSelected = false
            price3.isSelected = false
            price4.isSelected = true
        }
    }
    
    func requestData() {
        Province.requestAllProvinces(success: { (provinces) in
            self.provinces = provinces
            if let defaultName = provinces[0].name{
                self.provinceName = defaultName
            }
            
        }) { (error, response) in
            
        }
    }
    
    func fwComboBox(comboBox: FWComboBox, didSelectAtIndex index: Int) {
        if comboBox == provinceComboBox {
            currentProvinceIndex = index
            let valueStr = provinces![index].id?.trimmingCharacters(in: .whitespaces)
            provinceVal = Int(valueStr!)
            provinceName = provinces![index].name
        }
    }
    
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            if touchEvent.phase == .moved{
                ageVal = Int(slider.value)
                DispatchQueue.main.async(execute: {
                    self.ageValue.text = "\(Int(slider.value)) Tuổi"
                })
            }else if touchEvent.phase == .ended{
                
            }
        }
    }
    
    @IBAction func btnFilterHandler(_ sender: Any) {
        self .dismiss(animated: true) {
            self.delegate?.gobackServicePackageVC(provinceID:self.provinceVal!, provinceName:self.provinceName!, gender:self.gender, age:self.ageVal!, price:self.priceValue!, priceName:self.priceName!)
        }
    }
    
    @IBAction func btnExit(_ sender: Any) {
        self .dismiss(animated: true) {
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
