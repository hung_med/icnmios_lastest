//
//  LookupResultVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import FontAwesome_swift
import Toaster
import Alamofire

protocol RegisterScheduleVCDelegate {
    func gobackPackageDetailView(sevice:Service)
}

class RegisterScheduleVC: BaseViewControllerNoSearchBar, FWComboBoxDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate, UITextViewDelegate, FWDatePickerDelegate, UIPopoverPresentationControllerDelegate, UIViewControllerTransitioningDelegate {

    @IBOutlet weak var organizeLabel: UILabel!
    @IBOutlet weak var selectDateLabel: UILabel!
    @IBOutlet weak var timeRangeLabel: UILabel!
    @IBOutlet weak var timeRangeCollectionView: UICollectionView!
    @IBOutlet weak var organizeComboBox: FWComboBox!
    private var currentTextField:UITextField?
    @IBOutlet weak var appointmentDatePicker: FWDatePicker!
    private var currentOrganizeRequest:Request?
    private var currentScheduleTimeRequest:Request?
    @IBOutlet weak var timeRangeSpaceCollectionView: NSLayoutConstraint!
    @IBOutlet weak var typeSegment: UISegmentedControl!
    @IBOutlet weak var thumbnailService: CustomImageView!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblCost: UILabel!
    @IBOutlet weak var lblProvinceName: UILabel!
    @IBOutlet weak var txtuserName: FWFloatingLabelTextField!
    @IBOutlet weak var txtPhone: FWFloatingLabelTextField!
    @IBOutlet weak var txtAddress: FWFloatingLabelTextField!
    @IBOutlet weak var txtMaNV: FWFloatingLabelTextField!
    @IBOutlet weak var txtEmail: FWFloatingLabelTextField!

    @IBOutlet weak var buttonExit: UIButton!
    @IBOutlet weak var buttonDone: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!

    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var myScrollView:FWCustomScrollView!
    var service:Service? = nil
    var provinceName:String? = nil
    var confirm:Bool? = false
    var isOn:Bool? = false
    var userID:Int = 0
    var delegate:RegisterScheduleVCDelegate?
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    private var organizes:[Organize]? = [Organize]() {
        didSet {
            self.setupOrganizeComboBox()
        }
    }
    
    private var currentOrganize:Organize? {
        didSet {
            self.setupTimeRangeCollectionView()
            if let curOrganize = currentOrganize {
                currentScheduleTimeRequest?.cancel()
                let progress = self.showProgress()
                self.currentScheduleTimeRequest = curOrganize.getAllTimeRange(success: { (timeRanges) in
                    self.hideProgress(progress)
                    self.setupTimeRangeCollectionView()
                }, fail: { (error, response) in
                    self.hideProgress(progress)
                    self.handleNetworkError(error: error, responseData: response.data)
                })
            } else {
                firstSegmentCurrentTimeRange = nil
            }
        }
    }
    private var firstSegmentCurrentTimeRange:ScheduleTime?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        buttonDone.layer.cornerRadius = 4
        buttonDone.layer.masksToBounds = true
        buttonExit.layer.cornerRadius = 4
        buttonExit.layer.masksToBounds = true
        self.txtuserName.delegate = self
        self.txtPhone.delegate = self
        self.txtAddress.delegate = self
        self.txtMaNV.delegate = self
        self.txtEmail.delegate = self
        organizeComboBox.delegate = self
        myScrollView.delegate = self
        appointmentDatePicker.delegate = self
        appointmentDatePicker.minimumDate = Date()
        appointmentDatePicker.titleFont = UIFont.boldSystemFont(ofSize: 14.0)
        appointmentDatePicker.textColor = UIColor(hex: "1d6edc")
        appointmentDatePicker.contentHorizontalAlignment = .right
        organizeComboBox.layer.cornerRadius = 4.0
        organizeComboBox.layer.masksToBounds = true
        organizeComboBox.layer.borderColor = UIColor.lightGray.cgColor
        organizeComboBox.layer.borderWidth = 0.5
        
        timeRangeCollectionView.register(UINib(nibName: "SheduleTimeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TimeRangeCell")
        
        timeRangeCollectionView.dataSource = self
        timeRangeCollectionView.delegate = self
        timeRangeCollectionView.collectionViewLayout = SheduleTimeFlowLayout()
        timeRangeSpaceCollectionView.constant = 0
        registerForNotifications()
        
        self.confirm = false
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        if let img = service?.img, let serviceID = service?.serviceID{
            let urlString = img.trimmingCharacters(in: .whitespaces)
            if urlString != "" || !urlString.isEmpty{
                let url = API.baseURLImage + "\(API.iCNMImage)" +
                    "/ServiceImage/\(serviceID)" + "/\(urlString)"
                thumbnailService.loadImageUsingUrlString(urlString: url)
            }
        }
        if let serviceName = service?.serviceName{
            lblServiceName.text = serviceName.uppercased()
            lblServiceName.lineBreakMode = .byWordWrapping
            lblServiceName.numberOfLines = 0
        }
        if let costFe = service?.discount{
            let format = formatString(value: Float(costFe))
            lblCost.font = UIFont.fontAwesome(ofSize: 14.0)
            lblCost.text = format
        }
        if let provinceN = provinceName{
            lblProvinceName.font = UIFont.fontAwesome(ofSize: 14.0)
            lblProvinceName.text = String.fontAwesomeIcon(name: .mapMarker) + " \(provinceN)"
        }
        
        if let conditionDate = service?.conditionDate{
            appointmentDatePicker.conditionDate = conditionDate
        }
        
        if let serviceID = service?.serviceID{
            self.getOrganizeByID(serviceID: serviceID)
        }
        
        if let serviceType = service?.serviceType{
            //only BV
            if serviceType == 0{
                self.btnConfirm.isUserInteractionEnabled = false
                self.btnConfirm.alpha = 0.6
                organizeComboBox.alpha = 1.0
                organizeComboBox.isUserInteractionEnabled = true
            }
            // only TN
            else if serviceType == 1{
                self.btnConfirm.isUserInteractionEnabled = false
                self.btnConfirm.alpha = 1.0
                self.btnConfirm.setImage(UIImage(asset: Asset.checkBoxChecked), for: .normal)
                self.confirm = true
                organizeComboBox.alpha = 0.6
                organizeComboBox.isUserInteractionEnabled = false
            }
            // TN and BV
            else if serviceType == 2{
                self.btnConfirm.isUserInteractionEnabled = true
                organizeComboBox.isUserInteractionEnabled = true
            }
        }
        
        txtPhone.isUserInteractionEnabled = false
        txtPhone.addRegx(strRegx: "^[0-9]{10,11}$", errorMsg: "Số điện thoại không đúng định dạng")
        txtPhone.messageForValidatingLength = "Số điện thoại không được bỏ trống"
        txtuserName.messageForValidatingLength = "Tên không được bỏ trống"
        txtAddress.messageForValidatingLength = "Địa chỉ không được bỏ trống"
        if currentUser != nil{
            if let userName = currentUser?.userDoctor?.userInfo?.name{
                txtuserName.text = userName
            }
            if let phone = currentUser?.userDoctor?.userInfo?.phone{
                txtPhone.text = phone
            }
            if let address = currentUser?.userDoctor?.userInfo?.address{
                txtAddress.text = address
            }
            if let email = currentUser?.userDoctor?.userInfo?.email{
                txtEmail.text = email
            }
        }else{

        }
    }
    
    func getOrganizeByID(serviceID:Int){
        let progress = self.showProgress()
        self.currentOrganizeRequest = Organize.getOrganizeByID(serviceID:serviceID, success: { (organizes) in
            self.organizes = organizes
            if let curOrganize = self.currentOrganize {
                self.currentScheduleTimeRequest = curOrganize.getAllTimeRange(success: { (timeRanges) in
                    self.hideProgress(progress)
                    //self.setupTimeRangeCollectionView()
                    
                   // requestData()
                }, fail: { (error, response) in
                    self.hideProgress(progress)
                    self.handleNetworkError(error: error, responseData: response.data)
                })
            } else {
                self.hideProgress(progress)
            }
            
        }) { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        }
    }
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc private func keyboardWillShow(aNotification:Notification) {
        if let curTextField = currentTextField {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = myScrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            myScrollView.contentInset = contentInset
            myScrollView.scrollIndicatorInsets = contentInset
            if let rect = curTextField.superview?.convert(curTextField.frame, to: curTextField.superview?.superview) {
                let test = view.bounds.height - (rect.size.height + rect.origin.y + myScrollView.contentInset.top) - 60.0
                let different = keyboardFrame.size.height - test
                if different > 0 {
                    myScrollView.contentOffset.y = different - self.myScrollView.contentInset.top
                }
            }
        }
        
    }
    
    @objc private func keyboardWillHide(aNotification:Notification) {
        var contentInset:UIEdgeInsets = myScrollView.contentInset
        contentInset.bottom = 0
        myScrollView.contentInset = contentInset
        myScrollView.scrollIndicatorInsets = contentInset
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        self.isOn = !self.isOn!
        if isOn!{
            self.btnConfirm.setImage(UIImage(asset: Asset.checkBoxChecked), for: .normal)
            self.confirm = true
            //organizeComboBox.alpha = 0.6
            //organizeComboBox.isUserInteractionEnabled = false
        }else{
            self.btnConfirm.setImage(UIImage(asset: Asset.checkBoxOutline), for: .normal)
            self.confirm = false
            //organizeComboBox.alpha = 1.0
            //organizeComboBox.isUserInteractionEnabled = true
        }
    }
 
    @IBAction func btnDone(_ sender: Any) {
        let idGoi = self.service?.idGoi
        let maGoi = self.service?.maGoi
        let tenGoi = self.service?.serviceName
        let serviceID = self.service?.serviceID
        let personNumService = self.service?.personNumService
        let timeBefore = self.service?.timeBefore
        if let currentUserID = self.currentUser?.id{
            if currentUserID == 0{
                self.userID = 0
            }else{
                self.userID = currentUserID
            }
        }
        
        if self.txtuserName.validate() && self.txtPhone.validate() && self.txtAddress.validate(){
            
            if currentOrganize == nil && typeSegment.selectedSegmentIndex == 0 {
                let alertViewController = UIAlertController(title: "Thông báo", message: "Bạn chưa chọn nơi khám", preferredStyle: .alert)
                let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                alertViewController.addAction(noAction)
                self.present(alertViewController, animated: true, completion: nil)
                return
            }
            
            if let dayNumberOfWeek = appointmentDatePicker.currentDate.dayNumberOfWeek(){
                var convertStr = String(dayNumberOfWeek)
                if dayNumberOfWeek == 7{
                    convertStr = "0"
                }
                if let conditionDate = service?.conditionDate{
                    if (conditionDate.contains(convertStr)){
                        
                    }else{
                        if let conditionDate = service?.conditionDate{
                            let modified = conditionDate.replace(target: "0", withString: "7").replace(target: "1", withString: "Chủ nhật")
                            Toast(text: "Gói khám chỉ được đặt vào các ngày \(modified). Vui lòng chọn đúng ngày để đặt lịch").show()
                            return
                        }
                    }
                }
            }
            
            let currentTimeRange:ScheduleTime? = firstSegmentCurrentTimeRange
            if currentTimeRange == nil {
                let alertViewController = UIAlertController(title: "Thông báo", message: "Bạn chưa chọn khung giờ", preferredStyle: .alert)
                let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                alertViewController.addAction(noAction)
                self.present(alertViewController, animated: true, completion: nil)
                return
            }
        
            let noikham = self.currentOrganize?.organizeName
            let currentDate = self.appointmentDatePicker.currentDate
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let startTime = currentTimeRange?.startTime
            let endTime = currentTimeRange?.endTime
            let formatter2 = DateFormatter()
            formatter2.dateFormat = "HH:mm:ss"
            
            let ngaykham = formatter.string(from: currentDate)
            let splitStr = ngaykham.components(separatedBy: " ")[0] + " "
            let giohen1 = splitStr + formatter2.string(from: startTime!)
            let giohen2 = splitStr + formatter2.string(from: endTime!)
            
            if let userName = self.txtuserName.text, let phone = self.txtPhone.text, let address = self.txtAddress.text, let manv = self.txtMaNV.text, let email = self.txtEmail.text{
                self.sendRegisterSchedule(userID:self.userID, serviceID:serviceID!, idGoi: idGoi!, maGoi: maGoi!, tenGK:tenGoi!, name: userName, nguonban:"ICNM", phone: phone, address: address, maNV:manv, email:email, confirm:self.confirm!, noikham:noikham!, ngaykham:ngaykham, giohen1: giohen1, giohen2: giohen2, personNumService:personNumService!, timBefore:timeBefore!)
            }
        }
    }
    
    func sendRegisterSchedule(userID:Int, serviceID:Int, idGoi:Int, maGoi:String, tenGK:String, name:String, nguonban:String, phone:String, address:String, maNV:String, email:String, confirm:Bool, noikham:String, ngaykham:String, giohen1:String, giohen2:String, personNumService:Int, timBefore:Int){
        let progress = self.showProgress()
        if noikham.contains("MEDLATEC"){
            RegisterSchedule().sendRegisterSchedule(idGoi:idGoi, maGoi:maGoi, tenGK:tenGK, name:name, phone:phone, address:address, maNV:maNV, email:email, confirm: confirm, noikham:noikham, ngaykham:ngaykham, giohen1:giohen1, giohen2:giohen2, personNumService:personNumService, timeBefore:timBefore,  success: { (data) in
                if data?.general != nil {
                    self.hideProgress(progress)
                    if let result = data?.general?.mmagen{
                        let title = "Bạn đã đặt thành công \(tenGK). Mã gói khám là \(result). Bạn có thể vào chức năng Thông báo của tôi để xem lại thông tin mã gói khám."
                        let alert = UIAlertController(title: "Thông báo", message: title, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Đóng", style: .default, handler: { action in
                            self .dismiss(animated: true) {
                                self.sendNotificationSuccessService(userID:userID, serviceID:serviceID, magenGK:result)
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                } else {
                    self.hideProgress(progress)
                    if let result = data?.mstringResult{
                        Toast(text: result).show()
                    }
                }
            }, fail: { (error, response) in
                self.hideProgress(progress)
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }else{
            RegisterSchedule().sendRegisterSchedulePK(idGoi:idGoi, maGoi:maGoi, tenGK:tenGK, name:name, nguonban:nguonban, phone:phone, address:address, maNV:maNV, email:email, confirm: confirm, noikham:noikham, ngaykham:ngaykham, giohen1:giohen1, giohen2:giohen2, success: { (data) in
                if data != nil {
                    self.hideProgress(progress)
                    if let result = data?.mmagen{
                        let title = "Bạn đã đặt thành công \(tenGK). Mã gói khám là \(result). Bạn có thể vào chức năng Thông báo của tôi để xem lại thông tin mã gói khám."
                        let alert = UIAlertController(title: "Thông báo", message: title, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                            self .dismiss(animated: true) {
                                self.sendNotificationSuccessService(userID:userID, serviceID:serviceID, magenGK:result)
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                } else {
                    self.hideProgress(progress)
                    Toast(text: "Bạn không thể đặt gói khám!").show()
                }
            }, fail: { (error, response) in
                self.hideProgress(progress)
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
    }

    func sendNotificationSuccessService(userID:Int, serviceID:Int, magenGK:String){
        RegisterSchedule().sendNotificationSuccessService(serviceID: serviceID, userID: userID, magenGK: magenGK,  success: { (data) in
            if data != nil{

            }else {

            }
        }, fail: { (error, response) in
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }

    @IBAction func btnExit(_ sender: Any) {
        self .dismiss(animated: true) {

        }
    }

    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController!, sourceViewController source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController: presented, presenting: presentingViewController)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    override func requestData() {
        currentOrganizeRequest?.cancel()
        currentScheduleTimeRequest?.cancel()
        let progress = self.showProgress()
        if let curOrganize = self.currentOrganize {
            self.currentScheduleTimeRequest = curOrganize.getAllTimeRange(success: { (timeRanges) in
                self.hideProgress(progress)
                self.setupTimeRangeCollectionView()
            }, fail: { (error, response) in
                self.hideProgress(progress)
                self.handleNetworkError(error: error, responseData: response.data)
            })
        } else {
            self.hideProgress(progress)
        }
    }
    
    func setupOrganizeComboBox() {
        if let organizes = self.organizes {
            let activeOrganizes = organizes.filter({ (organizes) -> Bool in
                return organizes.active
            })
            organizeComboBox.dataSource = activeOrganizes.map({ (organizes) -> String in
                return organizes.organizeName!
            })

            organizeComboBox.selectRow(at: 0)
            currentOrganize = organizes[0]
        } else {
            organizeComboBox.selectRow(at: nil)
            organizeComboBox.dataSource = [String]()
        }
       
        if let noikham = currentOrganize?.organizeName{
            if noikham.contains("MEDLATEC"){
                self.txtMaNV.isUserInteractionEnabled = true
            }else{
                self.txtMaNV.isUserInteractionEnabled = false
            }
        }
    }
    
    func fwComboBox(comboBox: FWComboBox, didSelectAtIndex index: Int) {
        currentOrganize = organizes?[index]
    }
    
    //UICollectionView Datasource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = currentOrganize?.timeRanges.count ?? 0
        if count == 0 {
            timeRangeSpaceCollectionView.constant = 0
        } else {
            timeRangeSpaceCollectionView.constant = 14
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeRangeCell", for: indexPath) as! SheduleTimeCollectionViewCell
        let index = indexPath.row
        let timeRange = currentOrganize!.timeRanges[index]
        cell.timeButton.tag = index
        cell.timeButton.layer.cornerRadius = 14.0
        cell.timeButton.layer.borderWidth = 1.0
        let currentTimeRange:ScheduleTime? = firstSegmentCurrentTimeRange
        if let curTimeRange = currentTimeRange, timeRange.id == curTimeRange.id {
            cell.timeButton.layer.borderColor = UIColor(hex: "1d6edc").cgColor
            if let timeStr = timeRange.timeString {
                let attributedString = NSMutableAttributedString(string: " \(timeStr)")
                if IS_IPHONE_5{
                    attributedString.addAttribute(NSFontAttributeName, value:
                        UIFont.boldSystemFont(ofSize: 11.0), range: NSMakeRange(2, timeStr.count))
                }else{
                    attributedString.addAttribute(NSFontAttributeName, value:
                        UIFont.boldSystemFont(ofSize: 12.0), range: NSMakeRange(2, timeStr.count))
                }
                
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor(hex:"1d6edc"), range: NSMakeRange(2, timeStr.count))
                if IS_IPHONE_5{
                    attributedString.addAttribute(NSFontAttributeName, value: FontFamily.FontAwesome.regular.font(size: 11.0), range: NSMakeRange(0, 1))
                }else{
                    attributedString.addAttribute(NSFontAttributeName, value: FontFamily.FontAwesome.regular.font(size: 12.0), range: NSMakeRange(0, 1))
                }
                
                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor(hex:"39b54a"), range: NSMakeRange(0, 1))
                cell.timeButton.setAttributedTitle(attributedString, for: .normal)
            }
        } else {
            cell.timeButton.layer.borderColor = UIColor(hex: "cccccc").cgColor
            cell.timeButton.setAttributedTitle(nil, for: .normal)
            if IS_IPHONE_5{
                cell.timeButton.titleLabel?.font = UIFont.systemFont(ofSize: 11.0)
            }else{
                cell.timeButton.titleLabel?.font = UIFont.systemFont(ofSize: 12.0)
            }
            
            cell.timeButton.setTitle(timeRange.timeString, for: .normal)
        }
        cell.timeButton.addTarget(self, action: #selector(touchTimeRangeButton(sender:)), for: .touchUpInside)
        return cell
    }
    
    func touchTimeRangeButton(sender:UIButton) {
        firstSegmentCurrentTimeRange = currentOrganize?.timeRanges[sender.tag]
        timeRangeCollectionView.reloadData()
    }
    
    private func setupTimeRangeCollectionView() {
        let currentTimeRange:ScheduleTime? = nil
        
        if let curTimeRange = currentTimeRange {
            let index = currentOrganize?.timeRanges.index(where: { (timeRange) -> Bool in
                timeRange.id == curTimeRange.id
            })
            if index == nil {
                firstSegmentCurrentTimeRange = nil
            }
        }
        timeRangeCollectionView.reloadData()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtuserName{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == txtMaNV{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }else if textField == txtEmail{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == txtAddress{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 100
        }else if textField == txtPhone{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 12
        }else{
            return true
        }
    }
}
