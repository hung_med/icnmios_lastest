//
//  SheduleTimeFlowLayout.swift
//  iCNM
//
//  Created by Medlatec on 7/29/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class SheduleTimeFlowLayout: UICollectionViewFlowLayout {
    override init() {
        super.init()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    func setupLayout() {
        minimumInteritemSpacing = 10
        minimumLineSpacing = 10
        scrollDirection = .vertical
    }
    
    override var itemSize: CGSize {
        set {
            
        }
        get {
            let numberOfColumns: CGFloat = 3
            
            let itemWidth = (self.collectionView!.bounds.width - ((numberOfColumns - 1)*minimumInteritemSpacing)) / numberOfColumns
            return CGSize(width: floor(itemWidth), height: 30.0)
        }
    }
}
