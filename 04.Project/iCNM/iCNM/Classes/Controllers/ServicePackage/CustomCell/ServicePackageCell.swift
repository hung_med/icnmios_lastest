//
//  ServicePackageCell.swift
//  iCNM
//
//  Created by Mac osx on 11/6/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class ServicePackageCell: UITableViewCell {
    @IBOutlet weak var imageThumb:CustomImageView!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblServiceDesc: UILabel!
    @IBOutlet weak var lblCountFe: UILabel!
    @IBOutlet weak var lblDiscountFe: UILabel!
    @IBOutlet weak var lblSex: UILabel!
   
    @IBOutlet weak var lblProvinceName: UILabel!
    @IBOutlet weak var lblPercentDiscount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let f = contentView.frame
        let fr = UIEdgeInsetsInsetRect(f, UIEdgeInsetsMake(15, 15, 15, 15))
        contentView.frame = fr
        addShadow(cell: self)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func addShadow(cell:UITableViewCell) {
        cell.layer.cornerRadius = 4
        cell.layer.masksToBounds = true
        
        cell.layer.masksToBounds = false
        cell.layer.shadowOffset = CGSize(width: 0, height:0)
        cell.layer.shadowColor = UIColor.darkGray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowRadius = 2
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
