//
//  ServicePackageVC.swift
//  iCNM
//
//  Created by Mac osx on 11/6/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import FontAwesome_swift
import Cosmos
import Toaster
import GoogleMaps

class ServicePackageDetailVC: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate, UIScrollViewDelegate, ItemsServicePackageCellDelegate, UITextViewDelegate, RegisterScheduleVCDelegate, NotificationKhaosatVCDelegate, CommentServicePackageCellDelegate, RatingVC2Delegate, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var btnOrder: UIButton!
    var storedOffsets = [Int: CGFloat]()
    var serviceDetailGroups:[ServiceDetailGroups]? = nil
    var listImages: [String]? = nil
    var descPictures: [String]? = nil
    var serviceName:String? = nil
    var isCheck:Bool? = false
    var listTitleHeaders: [String]? = nil
    var cellHeightAuto = 0
    var currentTag = 0
    var lastContentOffset: CGFloat = 0
    var serviceRatings:[ServiceRatingFollowUser]? = nil
    var listSection = [Servey]()
    var listServey = [String : [Servey]]()
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Setup the Search Controller        
        serviceRatings = [ServiceRatingFollowUser]()
        serviceDetailGroups = [ServiceDetailGroups]()
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 200
        myTableView.rowHeight = UITableViewAutomaticDimension
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        NotificationCenter.default.addObserver(self, selector: #selector(handleLoginSuccess), name: Constant.NotificationMessage.loginSuccess, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleLogout), name: Constant.NotificationMessage.logout, object: nil)
        let notificationShowProfileUser = Notification.Name("showProfileUser2")
        NotificationCenter.default.addObserver(self, selector: #selector(self.showUserProfileController), name: notificationShowProfileUser, object: nil)
        
        // Setup the Search Controller
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(ServicePackageDetailVC.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }
       // title = serviceName
        let btnButton =  UIButton(type: .custom)
        btnButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 16.0)
        let buttonName = String.fontAwesomeIcon(name: .calendarPlusO)
        btnButton.setTitle(buttonName, for: .normal)
        btnButton.addTarget(self, action: #selector(ServicePackageDetailVC.registerScheduleHandle), for: .touchUpInside)
        btnButton.frame = CGRect(x: screenSizeWidth-40, y: 0, width: 24, height:24)
        let registerScheduleBtnItem = UIBarButtonItem(customView: btnButton)
        self.navigationItem.setRightBarButtonItems([registerScheduleBtnItem], animated: true)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        
        self.btnOrder.titleLabel?.font = UIFont.fontAwesome(ofSize: 20.0)
        let titleBtn = String.fontAwesomeIcon(name: .calendarPlusO) +  " Đặt lịch hẹn ngay"
        self.btnOrder.setTitle(titleBtn, for: .normal)
        self.btnOrder.addTarget(self, action:#selector(handleScheduleAnAppointment), for: .touchUpInside)
        self.btnOrder.isHidden = true
        self.btnOrder.isUserInteractionEnabled = false
        let nib0 = UINib(nibName: "InfoServicePackageCell", bundle: nil)
        myTableView.register(nib0, forCellReuseIdentifier: "InfoServiceCell")
        
        let nib1 = UINib(nibName: "CustomSlideTableViewCell", bundle: nil)
        myTableView.register(nib1, forCellReuseIdentifier: "Cell3")
        
        let nib2 = UINib(nibName: "ItemsServicePackageCell", bundle: nil)
        myTableView.register(nib2, forCellReuseIdentifier: "ItemsServiceCell")
        
        let nib3 = UINib(nibName: "LocationCell", bundle: nil)
        myTableView.register(nib3, forCellReuseIdentifier: "LocationCell")
      
        let nib4 = UINib(nibName: "ListTableItemsCell", bundle: nil)
        myTableView?.register(nib4, forCellReuseIdentifier: "ItemsCell")
        
        let nib5 = UINib(nibName: "SubDescriptionItemCell", bundle: nil)
        myTableView?.register(nib5, forCellReuseIdentifier: "DescCell")
        
        let nib6 = UINib(nibName: "CustomTableViewCell", bundle: nil)
        myTableView.register(nib6, forCellReuseIdentifier: "Cell")
        
        let nib7 = UINib(nibName: "CommentServicePackageCell", bundle: nil)
        myTableView.register(nib7, forCellReuseIdentifier: "CommentServiceCell")
        
        myTableView.register(UINib(nibName: "CustomHeaderServiceDetail", bundle: nil), forHeaderFooterViewReuseIdentifier: "headerServiceDetailCell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let user = self.currentUser{
            if self.serviceDetailGroups?.count != 0{
                if let service = self.serviceDetailGroups![0].service{
                    let serveyID = service.isServey
                    if serveyID != 0{
                        self.getAllServey(userID: user.id, serveyID: serveyID)
                    }else{
                        self.btnOrder.isUserInteractionEnabled = true
                    }
                }
            }else{
                self.btnOrder.isUserInteractionEnabled = true
            }
        }else{
            self.btnOrder.isUserInteractionEnabled = true
        }
    }
    
    func registerScheduleHandle(_ sender: UIButton){
        if currentUser == nil{
            confirmLogin(myView: self)
            return
        }else{
            self.showRegisterScheduleVC()
        }
    }
    
    func getAllServey(userID:Int, serveyID:Int){
        let progress = self.showProgress()
        ServeyObject().getAllServey(userID: userID, serveyQuestionID:serveyID, success: { (data) in
            if data?.listServey.count != 0 {
                for i in 0..<Int((data?.listServey.count)!){
                    let serveyObj = data?.listServey[i]
                    if let serveyTitle = serveyObj?.nServeyQuesContent{
                        
                        if !self.listSection.contains(where: {$0.nServeyQuesID == serveyObj?.nServeyQuesID}) {
                            self.listSection.append(serveyObj!)
                        }
                        
                        if self.listServey[serveyTitle] == nil {
                            self.listServey[serveyTitle] = [Servey]()
                        }
                        
                        self.listServey[serveyTitle]?.append(serveyObj!)
                    }
                }
                self.btnOrder.isUserInteractionEnabled = true
                self.myTableView.reloadData()
                self.hideProgress(progress)
            } else {
                self.hideProgress(progress)
                self.btnOrder.isUserInteractionEnabled = true
            }
        }, fail: { (error, response) in
            self.btnOrder.isUserInteractionEnabled = true
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func checkTrackingFeature(serviceID:Int){
        if currentUser != nil{
            let userDefaults = UserDefaults.standard
            let token = userDefaults.object(forKey: "token") as! String
            TrackingFeature().checkTrackingFeature(fcm:token, featureName:Constant.PACKAGE_SERVICE_DETAIL, categoryID: serviceID, success: {(result) in
                if result != nil{
                    print("flow track done:", Constant.PACKAGE_SERVICE)
                }else{
                    print("flow track fail:", Constant.PACKAGE_SERVICE)
                }
            }, fail: { (error, response) in
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }else{
            return
        }
    }
    
    func back(sender: UIBarButtonItem) {
        if self.isCheck!{
            self.navigationController?.popViewController(animated: true)
        }else{
            let result = self.navigationController?.backToViewController(viewController: ServicePackageVC.self)
            if !result!{
                self.navigationController?.popToRootViewController(animated: true)
            }else{
                
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Chi tiết Gói khám")
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        // get previous view controller and set title to "" or any things You want
    }
   
    func getAllServiceDetailGroups(serviceID:Int){
        let progress = self.showProgress()
        descPictures = [String]()
        listImages = [String]()
        self.listTitleHeaders = [String]()
        ServiceDetailGroups().getAllServiceDetailGroups(serviceID:serviceID, success: { (data) in
            if data != nil {
                self.serviceDetailGroups?.append(data!)
                if let img = data?.service?.img, let desImg = data?.service?.desImg, let serviceName = data?.service?.serviceName {
                    self.title = serviceName
                    self.descPictures?.append(desImg)
                    let urlString = img.trimmingCharacters(in: .whitespaces)
                    if urlString != "" || !urlString.isEmpty{
                        let url = API.baseURLImage + "\(API.iCNMImage)" +
                            "/ServiceImage/\(serviceID)" + "/\(urlString)"
                        self.listImages?.append(url)
                    }
                }
                DispatchQueue.main.async{
                    self.listTitleHeaders = ["", "", "ĐƠN VỊ CUNG CẤP", "CHI TIẾT", "", "", "GÓI KHÁM KHÁC"]
                    if self.serviceName == nil{
                        let serviceDetailObj = self.serviceDetailGroups![0]
                        self.title = serviceDetailObj.service?.serviceName
                    }
                    self.myTableView.reloadData()
                    self.hideProgress(progress)
                    self.btnOrder.isHidden = false
                    self.checkTrackingFeature(serviceID: serviceID)
                }
            } else {
                self.hideProgress(progress)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func getAllServiceRating(pageID:Int, serviceID:Int){
        let progress = self.showProgress()
        ServiceRatingFollowUser.getAllServiceRating(pageID:1, serviceID: serviceID, success: { (data) in
            if data.count > 0{
                self.serviceRatings = data
                self.myTableView.reloadData()
                self.hideProgress(progress)
            } else {
                self.serviceRatings = [ServiceRatingFollowUser]()
                self.myTableView.reloadData()
                self.hideProgress(progress)
            }
            
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return listTitleHeaders!.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerServiceDetailCell") as! CustomHeaderServiceDetail
        headerView.lblSectionName.tag = 0
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapSectionName))
        headerView.lblSectionName.isUserInteractionEnabled = true
        headerView.lblSectionName.addGestureRecognizer(tapGesture)
        if section == 3{
            if self.currentTag == 1{
                headerView.lblSectionName2.textColor =  UIColor(hex:"1976D2")
                headerView.lblSectionName.textColor = UIColor.lightGray
            }else{
                headerView.lblSectionName.textColor =  UIColor(hex:"1976D2")
                headerView.lblSectionName2.textColor = UIColor.lightGray
            }
            
            headerView.lblSectionName.dropShadow(color: .lightGray, opacity: 1, offSet: .zero, radius: 1)
            headerView.lblSectionName2.dropShadow(color: .lightGray, opacity: 1, offSet: .zero, radius: 1)
            headerView.lblSectionName.backgroundColor = UIColor.white
            headerView.lblSectionName?.font = UIFont.fontAwesome(ofSize: 16.0)
            if IS_IPHONE_5{
                headerView.lblSectionName?.font = UIFont.fontAwesome(ofSize: 14.0)
            }
            let infoTitle = String.fontAwesomeIcon(name: .list)
            headerView.lblSectionName.text = " " + infoTitle + "  CHI TIẾT"
            headerView.lblSectionName.textAlignment = NSTextAlignment.left
            headerView.lblSectionName2.backgroundColor = UIColor.white
            headerView.lblSectionName2.isHidden = false
            headerView.lblSectionName2?.font = UIFont.fontAwesome(ofSize: 16.0)
            headerView.lblSectionName2.textAlignment = NSTextAlignment.center
            
            if #available(iOS 10, *) {
                // use an api that requires the minimum version to be 10
            } else {
                headerView.lblSectionName.textAlignment = NSTextAlignment.left
                headerView.lblSectionName2.textAlignment = NSTextAlignment.left
            }
            
            let commentTitle = String.fontAwesomeIcon(name: .comment)
            if self.serviceDetailGroups?.count != 0{
                let serviceDetailObj = self.serviceDetailGroups![0]
                if let countRate = serviceDetailObj.service?.countRate{
                    headerView.lblSectionName2.text = commentTitle + "  ĐÁNH GIÁ (\(countRate))"
                }else{
                    headerView.lblSectionName2.text = commentTitle + "  ĐÁNH GIÁ"
                }
            }else{
                headerView.lblSectionName2.text = commentTitle + "  ĐÁNH GIÁ"
            }
            if IS_IPHONE_5{
                headerView.lblSectionName2?.font = UIFont.fontAwesome(ofSize: 14.0)
            }
            headerView.lblSectionName2.tag = 1
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapSectionName))
            headerView.lblSectionName2.isUserInteractionEnabled = true
            headerView.lblSectionName2.addGestureRecognizer(tapGesture)
        }else{
            if let titleHeader = listTitleHeaders?[section]{
                headerView.lblSectionName.text = " \(titleHeader)"
            }
            
            headerView.lblSectionName.dropShadow(color: .white, opacity: 0, offSet: .zero, radius: 0)
            headerView.lblSectionName2.backgroundColor = UIColor.clear
            headerView.lblSectionName.layer.borderWidth = 0
            headerView.lblSectionName2.isHidden = true
            headerView.lblSectionName.textColor = UIColor.lightGray
            headerView.lblSectionName.textAlignment = NSTextAlignment.left
            
            if #available(iOS 10, *) {
                // use an api that requires the minimum version to be 10
            } else {
                if section == 2{
                    headerView.lblSectionName.backgroundColor = UIColor.clear
                    headerView.lblSectionName2.backgroundColor = UIColor.white
                }
            }
        }
        headerView.contentView.backgroundColor = UIColor.white
        return headerView
    }

    func handleTapSectionName(sender: UITapGestureRecognizer) {
        let tag = (sender.view as? UILabel)?.tag
        self.currentTag = tag!
        let serviceDetailObj = self.serviceDetailGroups![0]
        if let serviceID = serviceDetailObj.service?.serviceID{
            self.getAllServiceRating(pageID:1, serviceID: serviceID)
        }
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        let sectionName = listTitleHeaders![section]
        if !sectionName.isEmpty{
            return 40
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return serviceDetailGroups!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let serviceDetailObj = self.serviceDetailGroups![0]
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3", for: indexPath) as!
            CustomSlideTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            
            cell.countImage = (self.listImages?.count)!
            if cell.countImage > 0{
                cell.showSlideAnimation(isCheck: false, listImages: self.listImages!, desc: self.descPictures!)
            }
            return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoServiceCell", for: indexPath) as! InfoServicePackageCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            if let serviceName = serviceDetailObj.service?.serviceName{
                cell.lblServiceName.text = serviceName
                cell.lblServiceName.font = UIFont.fontAwesome(ofSize: 20.0)
                cell.lblServiceName.lineBreakMode = .byWordWrapping
                cell.lblServiceName.numberOfLines = 0
            }
            
            if let serviceDesc = serviceDetailObj.service?.serviceDes{
                cell.lblServiceDes.text = serviceDesc
                cell.lblServiceDes.lineBreakMode = .byWordWrapping
                cell.lblServiceDes.numberOfLines = 0
            }
            
            if let discount = serviceDetailObj.service?.discount, let cost = serviceDetailObj.service?.cost{
                let percent = Float(1.0 - Float (discount) / Float (cost))
                cell.lblDisCountFe.font = UIFont.fontAwesome(ofSize: 16.0)
                cell.lblDisCountFe.text = String.fontAwesomeIcon(name: .caretDown) + " " + String(format: "%.f", percent*100) + "%"
            }
            
            if let discount = serviceDetailObj.service?.discount{
                let format = formatString(value: Float(discount))
                cell.lblDisCount.font = UIFont.boldSystemFont(ofSize: 20)
                cell.lblDisCount.text = "\(format)"
            }
            
            if let cost = serviceDetailObj.service?.cost{
                let result = formatAttributeString(value:cost)
                //cell.lblCost.font = UIFont.fontAwesome(ofSize: 18.0)
                cell.lblCost.attributedText = result
            }
            
            if let averageRating = serviceDetailObj.service?.averageRating{
                cell.cosmosRate.rating = Double(averageRating)
            }
            
            if let countRate = serviceDetailObj.service?.countRate{
                cell.lblNumberComment.text = "(\(countRate))"
            }
            
            if let averageRating = serviceDetailObj.service?.averageRating{
                cell.lblStar.text = "\(averageRating)/5"
                cell.lblStar.textColor = UIColor(red: 1, green: 149/255, blue: 0, alpha: 1)
            }
            
            return cell
        }
        else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell", for: indexPath) as! LocationCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            if let locationID = serviceDetailObj.location?.locationID{
                let urlImage = API.baseURLImage + "iCNMImage/" + "LocationImage/" + "\(locationID)/" + (serviceDetailObj.location?.imageStr)!
                let realImageUrl = urlImage.components(separatedBy: ";")[0]
                cell.imageThumb.loadImageUsingUrlString(urlString: realImageUrl)
            }
            
            if let name = serviceDetailObj.location?.name{
                cell.lblUnitName.text = name
                cell.lblUnitName.lineBreakMode = .byWordWrapping
                cell.lblUnitName.numberOfLines = 0
            }else{
                cell.lblUnitName.font = UIFont.fontAwesome(ofSize: 14.0)
                cell.lblUnitName.text = "Không có dữ liệu"
            }
            
            if let address = serviceDetailObj.location?.address{
                cell.lblAddress.lineBreakMode = .byWordWrapping
                cell.lblAddress.numberOfLines = 0
                cell.lblAddress.font = UIFont.fontAwesome(ofSize: 14.0)
                cell.lblAddress.text = String.fontAwesomeIcon(name: .mapMarker) + "   \(address)"
            }
            return cell
        }else if indexPath.section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemsCell", for: indexPath) as! ListTableItemsCell
            let listItems = serviceDetailObj.serviceDetail as [ServiceDetail]
            if currentTag == 1{
                cell.lblTestName.isHidden = true
            }else{
                cell.lblTestName.isHidden = false
                if listItems.count != 0{
                    cell.lblTestName.text = "\(listItems.count) Danh mục xét nghiệm"
                    cell.lblTestName.font = UIFont.boldSystemFont(ofSize: 15.0)
                }else{
                    cell.lblTestName.text = "0 Danh mục xét nghiệm"
                    cell.lblTestName.font = UIFont.boldSystemFont(ofSize: 15.0)
                }
            }
            cell.selectionStyle = .none
            return cell
        }else if indexPath.section == 4{
            if currentTag == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ItemsServiceCell", for: indexPath) as! ItemsServicePackageCell
                cell.delegate = self
                cell.setUpTable(cellHeightAuto: cellHeightAuto)
                cell.subMenuTable?.isScrollEnabled = false
                cell.subMenuTable?.bounces = false
                cell.subMenuTable?.alwaysBounceVertical = false
                let serviceDetail = serviceDetailObj.serviceDetail
                cell.serviceDetail = serviceDetail
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "CommentServiceCell", for: indexPath) as! CommentServicePackageCell
                let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapAvatar))
                gestureRecognizer.numberOfTapsRequired = 1
                cell.myAvatar.isUserInteractionEnabled = true
                cell.myAvatar.addGestureRecognizer(gestureRecognizer)
               
                cell.delegate = self
                cell.serviceRatings = self.serviceRatings
                cell.servicePackageObj = self.serviceDetailGroups![0]
                cell.currentUser = self.currentUser
                if let numberRate = self.serviceDetailGroups![0].service?.countRate{
                    if numberRate > 5{
                        let height = 128*CGFloat(serviceRatings!.count) + 215.0
                        cell.setUpTable(cellHeightAuto: Int(height))
                    }else{
                        let height = 128*CGFloat(serviceRatings!.count) + 174.0
                        cell.setUpTable(cellHeightAuto: Int(height))
                    }
                }else{
                    let height = 128*CGFloat(serviceRatings!.count) + 174.0
                    cell.setUpTable(cellHeightAuto: Int(height))
                }
                
                cell.myTableView?.isScrollEnabled = false
                cell.myTableView?.bounces = false
                cell.myTableView?.alwaysBounceVertical = false
                cell.myTableView?.reloadData()
                cell.btnRate.isUserInteractionEnabled = true
                cell.btnRate.addTarget(self, action: #selector(ServicePackageDetailVC.performRatingAction(_:)), for: UIControlEvents.touchUpInside)
                return cell
            }
            
        }else if indexPath.section == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DescCell", for: indexPath) as! SubDescriptionItemCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            if self.currentTag == 0{
                cell.lblDescription.text = "Lưu ý: Nhấn vào tên xét nghiệm để biết thêm ý nghĩa xét nghiệm"
            }else{
                cell.lblDescription.text = ""
            }
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomTableViewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.collectionView.register(UINib(nibName: "OtherServiceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "OtherServiceCell")
            cell.collectionView.isScrollEnabled = false
            cell.collectionView.bounces = false
            if let layout = cell.collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
                layout.scrollDirection = .vertical
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let serviceDetailObj = self.serviceDetailGroups![0]
        if indexPath.section == 0{
            if IS_IPAD{
                return screenSizeHeight/3
            }else{
                return screenSizeHeight/3
            }
        }else if indexPath.section == 1{
            return UITableViewAutomaticDimension
        }
        else if indexPath.section == 2{
            return 100
        }else if indexPath.section == 3{
            if currentTag == 0{
                return 44
            }else{
                return 0
            }
        }else if indexPath.section == 4{
            if currentTag == 0{
                let serviceDetail = serviceDetailObj.serviceDetail as [ServiceDetail]
                var count:Int = 0
                for i in 0..<Int(serviceDetail.count){
                    cellHeightAuto = Int(44*CGFloat(serviceDetail.count))
                    let groupTestName = serviceDetail[i].groupTestName
                    let testName = serviceDetail[i].testName
                    if !testName.isEmpty && !groupTestName.isEmpty{
                        count += 44
                    }
                }
                cellHeightAuto += count
                return CGFloat(cellHeightAuto)
            }else{
                if let numberRate = self.serviceDetailGroups![0].service?.countRate{
                    if numberRate > 5{
                         return 128*CGFloat(serviceRatings!.count) + 215.0
                    }else{
                        return 128*CGFloat(serviceRatings!.count) + 174.0
                    }
                }else{
                     return 174.0
                }
            }
        }else if indexPath.section == 5{
            if currentTag == 0{
                return 40
            }else{
                return 0
            }
        }else{
            let services = serviceDetailObj.listCustomServiceDetail
            if services.count > 0{
                let value = Int(screenSizeHeight/2.78)
                if IS_IPAD{
                    if services.count%2 == 0{
                        return CGFloat(240 + value*(services.count/2-1) + 300)
                    }else{
                        return CGFloat(240 + value*(services.count/2) + 300)
                    }
                }else{
                    if IS_IPHONE_5{
                        if services.count%2 == 0{
                            return CGFloat(240 + value*(services.count/2-1) + 175)
                        }else{
                            return CGFloat(240 + value*(services.count/2) + 175)
                        }
                    }else{
                        if services.count%2 == 0{
                            if IS_IPHONE_X{
                                return CGFloat(240 + value*(services.count/2-1) + 105)
                            }else{
                                return CGFloat(240 + value*(services.count/2-1) + 75)
                            }
                        }else{
                            if IS_IPHONE_X{
                                return CGFloat(240 + value*(services.count/2) + 105)
                            }else{
                                return CGFloat(240 + value*(services.count/2) + 75)
                            }
                        }
                        //return CGFloat(240 + value*(services.count/2)+10*(services.count/2+1)+40)
                    }
                }
            }else{
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2{
            let unitDetailVC = UnitDetailController(nibName: "UnitDetailController", bundle: nil)
            let location = self.serviceDetailGroups![0].location
            if location != nil{
                unitDetailVC.locationNearBy = location
                unitDetailVC.savedCameraPosition =
                    GMSCameraPosition.camera(withLatitude: (location?.lat)!,
                                             longitude: (location?.long)!, zoom: 16.0)
                self.navigationController?.pushViewController(unitDetailVC, animated: true)
            }
        }
    }
    
    func handleScheduleAnAppointment(sender: UIButton){
        if currentUser == nil{
            confirmLogin(myView: self)
            return
        }else{
            self.showRegisterScheduleVC()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 6{
            guard let tableViewCell = cell as? CustomTableViewCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 6{
            guard let tableViewCell = cell as? CustomTableViewCell else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }
    }
    
    func getInforDetailTestName(testCode:String){
        let param = testCode.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let progress = self.showProgress()
        Meanning().getMeaningByCode(testCode:param!, success: { (data) in
            if data != nil{
                self.hideProgress(progress)
                let descLookUpResultVC = DescLookUpResultVC(nibName: "DescLookUpResultVC", bundle: nil)
                if let testName = try! data?.testName.convertHtmlSymbols(){
                    descLookUpResultVC.testName = testName
                }
                if let meaning1 = try! data?.meaning1.convertHtmlSymbols(){
                    descLookUpResultVC.meaning1 = meaning1
                }
                if let meaning2 = try! data?.meaning2.convertHtmlSymbols(){
                    descLookUpResultVC.meaning2 = meaning2
                }
                descLookUpResultVC.modalPresentationStyle = .custom
                self.present(descLookUpResultVC, animated: true, completion: nil)
            } else {
                self.hideProgress(progress)
                let descLookUpResultVC = DescLookUpResultVC(nibName: "DescLookUpResultVC", bundle: nil)
                descLookUpResultVC.testName = "Đang cập nhật dữ liệu"
                descLookUpResultVC.meaning1 = "Đang cập nhật dữ liệu"
                descLookUpResultVC.meaning2 = "Đang cập nhật dữ liệu"
                descLookUpResultVC.modalPresentationStyle = .custom
                self.present(descLookUpResultVC, animated: true, completion: nil)
            }
            
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func showRegisterScheduleVC(){
        if self.listServey.count != 0{
            let notificationKhaosatVC = NotificationKhaosatVC(nibName: "NotificationKhaosatVC", bundle: nil)
            notificationKhaosatVC.delegate = self
            notificationKhaosatVC.modalPresentationStyle = .custom
            notificationKhaosatVC.transitioningDelegate = self
            notificationKhaosatVC.listSection = listSection
            notificationKhaosatVC.listServey = listServey
            self.present(notificationKhaosatVC, animated: true, completion: nil)
        }else{
            self.gobackRegisterScheduleVC()
        }
    }
    
    func destroyListServey() {
        listSection = [Servey]()
        listServey = [String : [Servey]]()
        self.gobackRegisterScheduleVC()
    }
    
    func gobackRegisterScheduleVC(){
        if self.serviceDetailGroups?.count != 0{
            let registerScheduleVC = RegisterScheduleVC(nibName: "RegisterScheduleVC", bundle: nil)
            registerScheduleVC.delegate = self
            registerScheduleVC.modalPresentationStyle = .custom
            registerScheduleVC.transitioningDelegate = self
            let provinceName = self.serviceDetailGroups![0].provinceName
            registerScheduleVC.provinceName = provinceName
            if let service = self.serviceDetailGroups![0].service{
                registerScheduleVC.service = service
            }
            self.present(registerScheduleVC, animated: true, completion: nil)
        }else{
            return
        }
    }
    
    func gobackPackageDetailView(sevice: Service) {
        let payViewController = VNPayViewController(nibName: "VNPayViewController", bundle: nil)
        self.tabBarController?.tabBar.isHidden = true
        payViewController.service = sevice
        self.navigationController?.pushViewController(payViewController, animated: true)
    }
    
    func performRatingAction(_ sender: Any) {
        let ratingVC = RatingVC2(nibName: "RatingVC2", bundle: nil)
        ratingVC.delegate = self
        ratingVC.serviceRatings = self.serviceRatings
        ratingVC.modalPresentationStyle = .custom
        ratingVC.transitioningDelegate = self
        self.present(ratingVC, animated: true, completion: nil)
    }
    
    func reloadRatingUser2(ratingNum: Double, noidung: String) {
        let serviceID = self.serviceDetailGroups![0].service?.serviceID
        let progress = self.showProgress()
        if self.currentUser != nil {
            if let user = self.currentUser {
                let userID = user.id
                var isUpdate:Bool = false
                var rateID:Int = 0
                for serviceRating in self.serviceRatings!{
                    if let userInfo = serviceRating.user{
                        if userInfo.userID == user.id{
                            isUpdate = true
                            rateID = (serviceRating.serviceRating?.serRateID)!
                        }
                    }
                }
                
                if isUpdate{
                    ServiceRatingFollowUser().putServiceRating(serRateID:rateID, serRateContent:noidung, serRateNum: Float(ratingNum), serviceID:serviceID!, userID:userID, success: {(result) in
                        if (result?.contains("Update"))!{
                            self.hideProgress(progress)
                            Toast(text: "Đánh giá thành công").show()
                            self.currentTag = 1
                            self.getAllServiceRating(pageID:1, serviceID: serviceID!)
                        }else{
                            self.hideProgress(progress)
                            Toast(text: "Đánh giá thất bại").show()
                        }
                    }, fail: { (error, response) in
                        
                    })
                }else{
                    ServiceRatingFollowUser().postServiceRating(serRateContent:noidung, serRateNum: Float(ratingNum), serviceID:serviceID!, userID:user.id, success: {(result) in
                        if (result?.contains("insert"))!{
                            self.hideProgress(progress)
                            Toast(text: "Đánh giá thành công").show()
                            self.currentTag = 1
                            self.getAllServiceRating(pageID:1, serviceID: serviceID!)
                        }else{
                            self.hideProgress(progress)
                            Toast(text: "Đánh giá thất bại").show()
                        }
                    }, fail: { (error, response) in
                        
                    })
                }
            }
        }
    
        self.goBackServicePackage()
    }
    
    func goBackServicePackage() {
        self .dismiss(animated: true) {
            
        }
    }
    
    func pushMoreCommentService() {
        let moreCommentServicePackage = MoreCommentServicePackage(nibName: "MoreCommentServicePackage", bundle: nil)
        let serviceDetailObj = self.serviceDetailGroups![0]
        if let serviceID = serviceDetailObj.service?.serviceID{
            moreCommentServicePackage.getAllServiceRating(pageID:1, serviceID: serviceID)
        }
        self.navigationController?.pushViewController(moreCommentServicePackage, animated: true)
    }
    
    func showUserProfileController(notification: Notification) {
        if let user = notification.object {
            let currentUser = user as! Users
            let userInfo = UserInfo()
            userInfo.id = currentUser.userID
            userInfo.name = currentUser.name
            userInfo.address = currentUser.address
            userInfo.email = currentUser.email
            userInfo.avatar = currentUser.avatar
            userInfo.aboutMe = currentUser.aboutMe
            userInfo.education = currentUser.education
            userInfo.gender = currentUser.gender
            userInfo.job = currentUser.job
            userInfo.phone = currentUser.phone
            userInfo.workplace = currentUser.workPlace
            userInfo.keySearchName = currentUser.keySearchName
            self.showProfileCurrentUser(user: userInfo)
        }
    }
    
    func handleLoginSuccess(aNotification:Notification) {
        let realm = try! Realm()
        currentUser = realm.currentUser()
        self.myTableView.reloadSections(IndexSet(integer: 4), with: .automatic)
    }
    
    func handleLogout(aNotification:Notification) {
        currentUser = nil
    }
    
    func handleTapAvatar(_ sender: UITapGestureRecognizer) {
        if let user = self.currentUser{
            self.showProfileCurrentUser(user: (user.userDoctor?.userInfo)!)
        }
    }
    
    func showProfileCurrentUser(user:UserInfo){
        let profileDoctorVC = ProfileDoctorVC(nibName: "ProfileDoctorVC", bundle: nil)
        let featuredDoctor = FeaturedDoctor()
        featuredDoctor.userInfo = user
        featuredDoctor.doctor = nil
        featuredDoctor.specialists = nil
        profileDoctorVC.featuredDoctor = featuredDoctor
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(profileDoctorVC, animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5, animations: {
            self.btnOrder.alpha = 1
        }, completion: {
            finished in
            self.btnOrder.isHidden = false
        })
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5, animations: {
            self.btnOrder.alpha = 0
        }, completion: {
            finished in
            self.btnOrder.isHidden = true
        })
    }
}

extension ServicePackageDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let serviceDetailObj = self.serviceDetailGroups![0]
        let services = serviceDetailObj.listCustomServiceDetail
        if services.count > 0{
            return services.count
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = getItemWidth(boundWidth: collectionView.bounds.size.width, column:2)
        if IS_IPHONE_5{
            let value = Int(screenSizeHeight/2.40)
            return CGSize(width: itemWidth, height: value)
        }else{
            let value = Int(screenSizeHeight/2.78)
            return CGSize(width: itemWidth, height: value)
        }
    }
    
    func getItemWidth(boundWidth: CGFloat, column:CGFloat) -> Int {
        let totalWidth = boundWidth - (Constant.offset + Constant.offset) - (column - 1) * Constant.minItemSpacing
        return Int(totalWidth / column)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OtherServiceCell", for: indexPath) as! OtherServiceCollectionViewCell
        cell.contentView.layer.cornerRadius = 4.0
        cell.contentView.layer.masksToBounds = true
        cell.contentView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell.contentView.layer.borderWidth = 1.0
        
        let serviceDetailObj = self.serviceDetailGroups![0]
        DispatchQueue.main.async(execute: {
            let listCustomServiceObj = serviceDetailObj.listCustomServiceDetail[indexPath.row]
            if let serviceName = listCustomServiceObj.service?.serviceName{
                cell.lblServiceName.text = serviceName
                cell.lblServiceName.font = UIFont.fontAwesome(ofSize: 18.0)
                if IS_IPHONE_5{
                    cell.lblServiceName.font = UIFont.fontAwesome(ofSize: 15.0)
                }
                cell.lblServiceName.lineBreakMode = .byWordWrapping
                cell.lblServiceName.numberOfLines = 0
            }
            
            if let img = listCustomServiceObj.service?.img, let tmpServiceID = listCustomServiceObj.service?.serviceID{
                let serviceID = String(tmpServiceID)
                cell.imageThumb.contentMode = .scaleToFill
                let urlString = img.trimmingCharacters(in: .whitespaces)
                if urlString != "" || !urlString.isEmpty{
                    let url = API.baseURLImage + "\(API.iCNMImage)" +
                        "/ServiceImage/\(serviceID)" + "/\(urlString)"
                    cell.imageThumb.loadImageUsingUrlString(urlString: url)
                }
            }
            
            if let discount = listCustomServiceObj.service?.discount, let cost = listCustomServiceObj.service?.cost{
                let percent = Float(1.0 - Float (discount) / Float (cost))
                cell.lblSaleOff.font = UIFont.fontAwesome(ofSize: 13.0)
                cell.lblSaleOff.text = String.fontAwesomeIcon(name: .caretDown) + " " + String(format: "%.f", percent*100) + "%"
            }
            
            if let discount = listCustomServiceObj.service?.discount{
                let format = formatString(value: Float(discount))
                cell.lblCost.font = UIFont.boldSystemFont(ofSize: 18.0)
                if IS_IPHONE_5{
                    cell.lblCost.font = UIFont.boldSystemFont(ofSize: 15.0)
                }
                cell.lblCost.text = "\(format)"
            }
            
            if let cost = listCustomServiceObj.service?.cost{
                let result = formatAttributeString(value:cost)
                // cell.lblDisCount.font = UIFont.fontAwesome(ofSize: 16.0)
                cell.lblDisCount.attributedText = result
            }
            
            if let averageRating = listCustomServiceObj.service?.averageRating{
                cell.cosmosRateStar.rating = Double(averageRating)
            }
            
            if IS_IPHONE_5{
                cell.lblNumberComment.textAlignment = NSTextAlignment.right
            }
            
            if let countRate = listCustomServiceObj.service?.countRate{
                cell.lblNumberComment.text = "(\(countRate))"
            }
        })
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let serviceDetailObj = self.serviceDetailGroups![0]
        let listCustomServiceObj = serviceDetailObj.listCustomServiceDetail[indexPath.row]
        let servicePackageDetailVC = ServicePackageDetailVC(nibName: "ServicePackageDetailVC", bundle: nil)
        if let serviceID = listCustomServiceObj.service?.serviceID{
            servicePackageDetailVC.serviceName = (listCustomServiceObj.service?.serviceName)!
            servicePackageDetailVC.getAllServiceDetailGroups(serviceID:Int(serviceID))
        }
        
        self.navigationController?.pushViewController(servicePackageDetailVC, animated: true)
    }
}
