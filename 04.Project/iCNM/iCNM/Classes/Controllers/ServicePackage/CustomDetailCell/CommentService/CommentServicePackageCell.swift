//
//  CommentServicePackageCell.swift
//  iCNM
//
//  Created by Mac osx on 11/9/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Cosmos
protocol CommentServicePackageCellDelegate {
    func pushMoreCommentService()
}
class CommentServicePackageCell: UITableViewCell, UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myAvatar: CustomImageView!
    @IBOutlet weak var numberPeopleRate: UILabel!
    @IBOutlet weak var numberOfRate: UILabel!
    @IBOutlet weak var btnRate: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var cosmosRate: CosmosView!
    var currentUser:User? = nil
    var serviceRatings:[ServiceRatingFollowUser]? = nil
    var servicePackageObj:ServiceDetailGroups? = nil
    var delegate:CommentServicePackageCellDelegate?
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style , reuseIdentifier: reuseIdentifier)
        //setUpTable()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //setUpTable()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //setUpTable()
    }
    
    func setUpTable(cellHeightAuto:Int){
        myAvatar.layer.cornerRadius = 30
        myAvatar.layer.masksToBounds = true
        myAvatar.layer.borderColor = UIColor(hexColor: 0x1D6EDC, alpha: 1.0).cgColor
        myAvatar.layer.borderWidth = 2.0
        btnMore.isHidden = false
        if let user = self.currentUser{
            if let avatar = user.userDoctor?.userInfo?.avatar{
                let avatarURL = API.baseURLImage + "\(API.iCNMImage)" + "\(user.id)" + "/\(avatar)"
                let urlString = avatarURL.trimmingCharacters(in: .whitespaces)
                myAvatar.loadImageUsingUrlString(urlString: urlString)
            }else{
                myAvatar.image = UIImage(named: "avatar_default")
            }
        }
        
        if let numberRate = self.servicePackageObj?.service?.countRate{
            numberPeopleRate.text = "Nói cho mọi người biết trải nghiệm của bạn!"
            if numberRate > 5{
                btnMore.isHidden = false
            }else{
                btnMore.isHidden = true
            }
            if numberRate == 0{
                numberPeopleRate.text = "Bạn hãy là người đầu tiên đánh giá gói khám này."
                numberOfRate.text = ""
                cosmosRate.rating = 0
            }else{
                for serviceRating in self.serviceRatings!{
                    if let userInfo = serviceRating.user{
                        if userInfo.userID == currentUser?.id{
                            let rating = serviceRating.serviceRating
                            if let serRateNum = rating?.serRateNum{
                                cosmosRate.rating = Double(serRateNum)
                                numberOfRate.text = "\(serRateNum)/5"
                                numberPeopleRate.text = rating?.serRateContent
                            }
                        }else{
                            
                        }
                    }
                }
            }
        }
        numberOfRate.textColor = UIColor(red: 1, green: 149/255, blue: 0, alpha: 1)
        
        myTableView?.delegate = self
        myTableView?.dataSource = self
        myTableView?.estimatedRowHeight = 200
        myTableView?.rowHeight = UITableViewAutomaticDimension
        let nib = UINib(nibName: "CommentServiceCell", bundle: nil)
        myTableView?.register(nib, forCellReuseIdentifier: "ServiceItemCell")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.serviceRatings!.count != 0{
            return self.serviceRatings!.count
        }else{
            return 0
        }
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceItemCell", for: indexPath) as! CommentServiceCell
        
        if self.serviceRatings?.count != 0{
            let serviceRatingObj = self.serviceRatings![indexPath.row]
            cell.userAvatar.layer.cornerRadius = 30
            cell.userAvatar.layer.masksToBounds = true
            cell.user = serviceRatingObj.user
            if let name = serviceRatingObj.user?.name{
                cell.lblUserName.text = name
            }
            
            if let avatar = serviceRatingObj.user?.avatar, let userID = serviceRatingObj.user?.userID{
                let avatarURL = API.baseURLImage + "\(API.iCNMImage)" + "\(userID)" + "/\(avatar)"
                let urlString = avatarURL.trimmingCharacters(in: .whitespaces)
                if avatar.isEmpty{
                    cell.userAvatar.image = UIImage(named: "avatar_default")
                }else{
                    cell.userAvatar.loadImageUsingUrlString(urlString: urlString)
                }
            }
            
            let serviceRateContent = serviceRatingObj.serviceRating?.serRateContent
            cell.lblDescription.text = serviceRateContent
            cell.lblDescription.lineBreakMode = .byWordWrapping
            cell.lblDescription.numberOfLines = 0
            
            if let dateCreate = serviceRatingObj.serviceRating?.dateCreate{
                cell.lblDateTime.text = dateCreate.timeAgoSinceNow()
            }
            
            if let serviceRateNum = serviceRatingObj.serviceRating?.serRateNum{
                cell.cosmosRate.rating = Double(serviceRateNum)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @IBAction func btnMoreComment(_ sender: Any) {
        self.delegate?.pushMoreCommentService()
    }
        
//    func handleTapTestName(sender: UITapGestureRecognizer) {
//        //let tag = (sender.view as? UILabel)?.tag
//        let testCode = (sender.view as? UILabel)?.accessibilityHint
//        delegate?.getInforDetailTestName(testCode: testCode!)
//    }
}
