//
//  CommentServiceCell.swift
//  iCNM
//
//  Created by Mac osx on 11/7/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Cosmos

class CommentServiceCell: UITableViewCell {
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var userAvatar: CustomImageView!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var cosmosRate: CosmosView!
    @IBOutlet weak var lblDescription: UILabel!
    var user:Users? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        userAvatar.layer.borderColor = UIColor(hexColor: 0x1D6EDC, alpha: 1.0).cgColor
        userAvatar.layer.borderWidth = 2.0
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapAvatar))
        gestureRecognizer.numberOfTapsRequired = 1
        userAvatar.isUserInteractionEnabled = true
        userAvatar.addGestureRecognizer(gestureRecognizer)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func handleTapAvatar(_ sender: UITapGestureRecognizer) {
        let notificationShowProfileUser = Notification.Name("showProfileUser2")
        NotificationCenter.default.post(name: notificationShowProfileUser , object: self.user)
    }
}
