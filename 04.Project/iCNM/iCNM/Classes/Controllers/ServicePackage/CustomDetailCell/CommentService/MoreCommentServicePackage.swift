//
//  MoreCommentServicePackage.swift
//  iCNM
//
//  Created by Mac osx on 11/6/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import FontAwesome_swift
import ESPullToRefresh

class MoreCommentServicePackage: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var myTableView: UITableView!
    var serviceRatings:[ServiceRatingFollowUser]? = nil
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    var serviceID:Int = 0
    fileprivate var pageNumber = 1
    fileprivate var footerScrollView = ESRefreshFooterAnimator(frame:CGRect.zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        title = "Danh sách đánh giá"
        let button: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        button.setImage(UIImage(named: "angle-left"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(MoreCommentServicePackage.back(sender:)), for: UIControlEvents.touchUpInside)
        let newBackButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.leftBarButtonItem = newBackButton
    
        serviceRatings = [ServiceRatingFollowUser]()
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 200
        myTableView.rowHeight = UITableViewAutomaticDimension
        let nib = UINib(nibName: "CommentServiceCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "ServiceItemCell")
        
        footerScrollView.loadingMoreDescription = "Tải thêm"
        self.myTableView.es.addInfiniteScrolling(animator:footerScrollView) { [weak self] in
            
            if let weakSelf = self {
                weakSelf.pageNumber = weakSelf.pageNumber + 1
                ServiceRatingFollowUser.getAllServiceRating(pageID:weakSelf.pageNumber, serviceID: (self?.serviceID)!, success: { (data) in
                    weakSelf.myTableView.es.stopLoadingMore()
                    if data.count > 0 {
                        for item in data {
                            self?.serviceRatings?.append(item)
                            self?.myTableView.reloadData()
                        }
                    } else {
                        self?.footerScrollView.loadingMoreDescription = "Đã hết dữ liệu"
                        if self?.serviceRatings?.count == 0{
                            self?.footerScrollView.loadingMoreDescription = "Không có bình luận nào"
                        }
                    }
                }, fail: { (error, response) in
                    
                })
            }
        }
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func getAllServiceRating(pageID:Int, serviceID:Int){
        self.serviceID = serviceID
        let progress = self.showProgress()
        ServiceRatingFollowUser.getAllServiceRating(pageID: pageID, serviceID: serviceID, success: { (data) in
            if data.count > 0{
                self.serviceRatings = data
                self.myTableView.reloadData()
                self.hideProgress(progress)
            } else {
                self.serviceRatings = [ServiceRatingFollowUser]()
                self.myTableView.reloadData()
                self.hideProgress(progress)
            }
            
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.serviceRatings!.count != 0{
            return self.serviceRatings!.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceItemCell", for: indexPath) as! CommentServiceCell
        if self.serviceRatings?.count != 0{
            let serviceRatingObj = self.serviceRatings![indexPath.row]
            cell.userAvatar.layer.cornerRadius = 30
            cell.userAvatar.layer.masksToBounds = true
            cell.user = serviceRatingObj.user
            if let name = serviceRatingObj.user?.name{
                cell.lblUserName.text = name
            }
            
            if let avatar = serviceRatingObj.user?.avatar, let userID = serviceRatingObj.user?.userID{
                let avatarURL = API.baseURLImage + "\(API.iCNMImage)" + "\(userID)" + "/\(avatar)"
                let urlString = avatarURL.trimmingCharacters(in: .whitespaces)
                if avatar.isEmpty{
                    cell.userAvatar.image = UIImage(named: "avatar_default")
                }else{
                    cell.userAvatar.loadImageUsingUrlString(urlString: urlString)
                }
            }
            
            let serviceRateContent = serviceRatingObj.serviceRating?.serRateContent
            cell.lblDescription.text = serviceRateContent
            cell.lblDescription.lineBreakMode = .byWordWrapping
            cell.lblDescription.numberOfLines = 0
            
            if let dateCreate = serviceRatingObj.serviceRating?.dateCreate{
                cell.lblDateTime.text = dateCreate.timeAgoSinceNow()
            }
            
            if let serviceRateNum = serviceRatingObj.serviceRating?.serRateNum{
                cell.cosmosRate.rating = Double(serviceRateNum)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
