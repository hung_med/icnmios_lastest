//
//  ListTableItemsCell.swift
//  iCNM
//
//  Created by Mac osx on 7/15/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class ListTableItemsCell: UITableViewCell {

    @IBOutlet weak var lblTestName: UILabel!
    @IBOutlet weak var lblCost: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
