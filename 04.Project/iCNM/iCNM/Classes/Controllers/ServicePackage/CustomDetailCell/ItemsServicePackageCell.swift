//
//  ItemsServicePackageCell.swift
//  iCNM
//
//  Created by Mac osx on 11/9/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
protocol ItemsServicePackageCellDelegate {
    func getInforDetailTestName(testCode:String)
}
class ItemsServicePackageCell: UITableViewCell, UITableViewDataSource,UITableViewDelegate {
    var subMenuTable:UITableView?
    var header:[String]? = nil
    var serviceDetail:[ServiceDetail] = []
    var delegate:ItemsServicePackageCellDelegate?
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style , reuseIdentifier: reuseIdentifier)
        //setUpTable()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //setUpTable()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //setUpTable()
    }
    
    func setUpTable(cellHeightAuto:Int){
        if subMenuTable == nil{
            subMenuTable = UITableView(frame: .init(x: 0, y: 0, width: screenSizeWidth, height: CGFloat(cellHeightAuto)), style:UITableViewStyle.plain)
            subMenuTable?.delegate = self
            subMenuTable?.dataSource = self
            subMenuTable?.estimatedRowHeight = 200
            subMenuTable?.rowHeight = UITableViewAutomaticDimension
            self.addSubview(subMenuTable!)
            let nib_header = UINib(nibName: "ListTableItemSection", bundle: nil)
            subMenuTable?.register(nib_header, forHeaderFooterViewReuseIdentifier: "HeaderCell")
            
            let nib = UINib(nibName: "ListTableItemsCell", bundle: nil)
            subMenuTable?.register(nib, forCellReuseIdentifier: "ItemsCell")
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 44
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderCell") as? ListTableItemSection
        let serviceGroups = self.serviceDetail[section]
        let groupTestName = serviceGroups.groupTestName
        
        if !groupTestName.isEmpty{
            headerView?.lblHeaderTestName.text = groupTestName
        }else{
            headerView?.lblHeaderTestName.text = serviceGroups.testName
        }
        
        headerView?.lblHeaderTestName.accessibilityHint = serviceGroups.testCode
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapTestName))
        headerView?.lblHeaderTestName.isUserInteractionEnabled = true
        headerView?.lblHeaderTestName.addGestureRecognizer(tapGesture)
        
        let cost = serviceGroups.price
        let format = formatString2(value: cost)
        headerView?.lblCost.text = format
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let serviceGroups = self.serviceDetail[section]
        let customGroupTest = serviceGroups.groupTestName
        if !customGroupTest.isEmpty{
            let testName = serviceGroups.testName
            if !testName.isEmpty{
                return 1
            }else{
                return 0
            }
        }else{
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return serviceDetail.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemsCell", for: indexPath) as! ListTableItemsCell
        let serviceGroups = self.serviceDetail[indexPath.section]
        let testName = serviceGroups.testName
        cell.lblTestName.accessibilityHint = serviceGroups.testCode
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapTestName))
        cell.lblTestName.isUserInteractionEnabled = true
        cell.lblTestName.addGestureRecognizer(tapGesture)
        
        if !testName.isEmpty{
            cell.lblTestName.text = testName
        }else{
            cell.lblTestName.text = ""
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func handleTapTestName(sender: UITapGestureRecognizer) {
        //let tag = (sender.view as? UILabel)?.tag
        let testCode = (sender.view as? UILabel)?.accessibilityHint
        delegate?.getInforDetailTestName(testCode: testCode!)
    }
}
