//
//  LocationCell.swift
//  iCNM
//
//  Created by Mac osx on 11/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class LocationCell: UITableViewCell {
    @IBOutlet weak var lblUnitName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var imageThumb: CustomImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
