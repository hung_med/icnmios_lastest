//
//  DetailAnswerDoctorVC.swift
//  iCNM
//
//  Created by Mac osx on 11/28/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Toaster
import RealmSwift
import PhotoSlider

protocol DetailAnswerDoctorVCVCDelegate {
    func reloadDataAnswerDoctor(currentTag:Int)
}

//@available(iOS 10.0, *)
class DetailAnswerDoctorVC: BaseViewControllerNoSearchBar, TemplateAnswerVCDelegate, OtherAnswerVCDelegate, UITextViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var avatarImageView: PASImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var specialistTag: FWTagLabel!
    @IBOutlet weak var questionTitleLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var questionContentTextView: UITextView!
    @IBOutlet weak var btnAddTemp: UIButton!
    @IBOutlet weak var btnViewContainer: UIButton!
    @IBOutlet weak var btnUpdateAnswer: UIButton!
    @IBOutlet weak var btnExit: UIButton!
    @IBOutlet weak var imgRecord: UIImageView!
    @IBOutlet weak var myCollectionView: UICollectionView!
    var delegate:DetailAnswerDoctorVCVCDelegate?
    var questionAnswerForDoctors:QuestionAnswerForDoctor? = nil
    var currentTag:Int = 0
    var resultTime:Int = 0
    var countTimer:Int = 0
    var timer:Timer? = nil
    var currentContent:String? = nil
    var userDefaults = UserDefaults.standard
    var listImages:[String]? = nil
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var heightConstr: NSLayoutConstraint!
    @IBOutlet weak var heightQuestionConstr: NSLayoutConstraint!
    
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        btnUpdateAnswer.layer.cornerRadius = 4
        btnUpdateAnswer.layer.masksToBounds = true
        contentTextView.layer.cornerRadius = 4
        contentTextView.layer.masksToBounds = true
        contentTextView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        contentTextView.layer.borderWidth = 1.0
        
        btnExit.layer.cornerRadius = 4
        btnExit.layer.masksToBounds = true
        
        contentTextView.delegate = self
        self.myView.layer.cornerRadius = 10
        self.myView.layer.borderWidth = 1
        self.myView.layer.masksToBounds = true
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 50, height: 50)
        layout.minimumInteritemSpacing = 2
        layout.minimumLineSpacing = 2
        self.myCollectionView.collectionViewLayout = layout
        self.myCollectionView.dataSource = self
        self.myCollectionView.delegate = self
        self.myCollectionView.register(UINib(nibName: "MyQuestionThumbCell", bundle: nil), forCellWithReuseIdentifier: "ThumbCell")

        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        title = "Trả lời"
        let btnButton =  UIButton(type: .custom)
        btnButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 16.0)
        let buttonName = String.fontAwesomeIcon(name: .sendO)
        btnButton.setTitle(buttonName, for: .normal)
        btnButton.addTarget(self, action: #selector(DetailAnswerDoctorVC.btnUpdateAnswerAction), for: .touchUpInside)
        btnButton.frame = CGRect(x: screenSizeWidth-20, y: 0, width: 30, height:30)
        let filterBtnItem = UIBarButtonItem(customView: btnButton)
        self.navigationItem.setRightBarButtonItems([filterBtnItem], animated: true)
        
        // Setup the Search Controller
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(DetailAnswerDoctorVC.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        //add notification keyboard hidden/show
        NotificationCenter.default.addObserver(self, selector: #selector(DetailAnswerDoctorVC.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(DetailAnswerDoctorVC.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.listImages = [String]()
        if questionAnswerForDoctors != nil{
            if let questionID = self.questionAnswerForDoctors?.id{
                if (userDefaults.object(forKey: "\(questionID)") != nil){
                    let content = userDefaults.object(forKey: "\(questionID)") as! String
                    self.currentContent = content
                    self.contentTextView.text = content
                }else{
                    if let answerContent = questionAnswerForDoctors?.answerContent
                    {
                        contentTextView.attributedText = answerContent.htmlAttributedString(fontSize: 14.0)
                    }
                }
            }
            
            if let title = questionAnswerForDoctors?.title
            {
                questionTitleLabel.text = title
                questionTitleLabel.lineBreakMode = .byWordWrapping
                questionTitleLabel.numberOfLines = 0
            }
            if let content = questionAnswerForDoctors?.content
            {
                questionContentTextView.text = content.components(separatedBy: "\n\n").filter { $0 != "" }.joined(separator: "\n")
                //questionContentTextView.lineBreakMode = .byWordWrapping
                //questionContentTextView.numberOfLines = 0
            }
            if let fullname = questionAnswerForDoctors?.fullname
            {
                nameLabel.text = fullname
            }
            if let dateCreate = questionAnswerForDoctors?.dateCreate
            {
                let strDatetime = dateCreate.timeAgoSinceNow()
                timeLabel.text = "Đã hỏi \(strDatetime)"
            }
            
            if let specialName = questionAnswerForDoctors?.specialName
            {
                specialistTag.text = specialName
            }
            
            if let avatar = questionAnswerForDoctors?.avatar, let userId = questionAnswerForDoctors?.userID {
                let urlString = avatar.trimmingCharacters(in: .whitespaces)
                let avatarURL = API.baseURLImage + "\(API.iCNMImage)" + "\(userId)" + "/\(urlString)"
                if let otherAvatarURL = URL(string: avatarURL) {
                    avatarImageView.imageURL(URL: otherAvatarURL)
                }
            }
            
            if let images = questionAnswerForDoctors?.questionImage, let questionID = questionAnswerForDoctors?.id{
                let urlString = images.components(separatedBy: ";")
                
                if urlString.count != 0{
                    heightConstr.constant = 50
                    if IS_IPAD{
                        heightConstr.constant = 80
                        heightQuestionConstr.constant = 50
                    }else{
                        if IS_IPHONE_6, IS_IPHONE_8 {
                            heightQuestionConstr.constant = 25
                        }
                    }
                    
                    for url in urlString {
                        if !url.isEmpty || url != ""{
                            if !url.isEmpty || url != ""{
                                let urlBase = API.baseURLImage + "\(API.iCNMImage)" +
                                    "QuestionImage/\(questionID)/" + "\(url)"
                                self.listImages?.append(urlBase)
                            }else{
                                self.listImages = [String]()
                            }
                            
                        }else{
                            if IS_IPAD{
                                heightConstr.constant = 80
                                heightQuestionConstr.constant = 50
                            }else{
                                heightConstr.constant = 0
                                if IS_IPHONE_5 {
                                    heightQuestionConstr.constant = 25
                                } else {
                                    heightQuestionConstr.constant = 50
                                }
                            }
                        }
                    }
                }else{
                    heightConstr.constant = 0
                    if IS_IPHONE_5 {
                        heightQuestionConstr.constant = 25
                    } else {
                        heightQuestionConstr.constant = 50
                    }
                }
            }
            
            if self.resultTime == 0{
                if let dateCreate = questionAnswerForDoctors?.dateCreate{
                    let currentSecond = Int(dateCreate.millisecondsSince1970)
                    let diff = self.resultTime - currentSecond
                    if diff < 0{
                        countTimer = 60 * 60 * 1000 - 1000
                        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(DetailAnswerDoctorVC.updateRealTimer), userInfo: nil, repeats: true)
                    }
                }
            }else{
                if let expireTime = questionAnswerForDoctors?.expireTime{
                    let currentSecond = Int(expireTime.millisecondsSince1970)
                    let diff = self.resultTime - currentSecond
                    if (diff > 60 * 60 * 1000) {
                        statusLabel.text = "Quá hạn trả lời"
                        statusLabel.textColor = UIColor.red
                        btnAddTemp.isUserInteractionEnabled = false
                        btnAddTemp.alpha = 0.5
                        btnViewContainer.isUserInteractionEnabled = false
                        btnViewContainer.alpha = 0.5
                        btnUpdateAnswer.isUserInteractionEnabled = false
                        btnUpdateAnswer.alpha = 0.5
                        self.navigationItem.rightBarButtonItems = nil
                    }else{
                        countTimer = 60 * 60 * 1000 - diff
                        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(DetailAnswerDoctorVC.updateRealTimer), userInfo: nil, repeats: true)
                    }
                }
            }
        }
        
//        speechRecognizer.delegate = self
        // record answer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(recordAnswer))
        imgRecord.addGestureRecognizer(tapGesture)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let frame = CGRect(x: screenSizeWidth/2 - self.myView.frame.width/2, y: screenSizeHeight/2 - self.myView.frame.height/2, width: self.myView.frame.width, height: self.myView.frame.height)
        self.myView.frame = frame
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func updateRealTimer() {
        if(countTimer > 0) {
            countTimer -= 1000
            let result = Common.sharedInstance.stringFromTimeInterval2(interval: countTimer)
            statusLabel.text = "\(result)"
            statusLabel.textColor = UIColor(hex:"1976D2")
            btnAddTemp.isUserInteractionEnabled = true
            btnAddTemp.alpha = 1.0
            btnViewContainer.isUserInteractionEnabled = true
            btnViewContainer.alpha = 1.0
            btnUpdateAnswer.isUserInteractionEnabled = true
            btnUpdateAnswer.alpha = 1.0
        }else{
            timer?.invalidate()
            timer = nil
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnAddTempAction(_ sender: Any) {
        var optionMenu:UIAlertController? = nil
        if IS_IPAD{
            optionMenu = UIAlertController(title: nil, message: "Chọn chuyên khoa", preferredStyle: .alert)
        }else{
            optionMenu = UIAlertController(title: nil, message: "Chọn chuyên khoa", preferredStyle: .actionSheet)
        }
        
        let action1 = UIAlertAction(title: "Sản khoa", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            if IS_IPAD{
                optionMenu = UIAlertController(title: nil, message: "Chọn loại câu hỏi", preferredStyle: .alert)
            }else{
                optionMenu = UIAlertController(title: nil, message: "Chọn loại câu hỏi", preferredStyle: .actionSheet)
            }
            let action1_1 = UIAlertAction(title: "Câu hỏi về Beta", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.showTemplateAnswerVC(content: Constant.questionBeta)
            })
            let action1_2 = UIAlertAction(title: "Câu hỏi về Triple Test", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.showTemplateAnswerVC(content: Constant.questionTripleTest)
            })
            let action1_3 = UIAlertAction(title: "Câu hỏi về Double Test", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.showTemplateAnswerVC(content: Constant.questionDoubleTest)
            })
            let action1_4 = UIAlertAction(title: "Một số chỉ số về nội tiết tố hay hỏi", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.showTemplateAnswerVC(content: Constant.questionOther)
            })
           
            optionMenu?.addAction(action1_1)
            optionMenu?.addAction(action1_2)
            optionMenu?.addAction(action1_3)
            optionMenu?.addAction(action1_4)
            // show action sheet
            optionMenu?.popoverPresentationController?.sourceView = self.view
            self.present(optionMenu!, animated: true, completion: nil)
            
        })
        let action2 = UIAlertAction(title: "Nội khoa", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.showTemplateAnswerVC(content: Constant.questionNoiKhoaOther)
        })
        let action3 = UIAlertAction(title: "Xét nghiệm", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if IS_IPAD{
                optionMenu = UIAlertController(title: nil, message: "Chọn loại Xét nghiệm", preferredStyle: .alert)
            }else{
                optionMenu = UIAlertController(title: nil, message: "Chọn loại Xét nghiệm", preferredStyle: .actionSheet)
            }
            let action1_1 = UIAlertAction(title: "Triple Test", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.showTemplateAnswerVC(content: Constant.questionXN_1)
            })
            let action1_2 = UIAlertAction(title: "Double Test", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.showTemplateAnswerVC(content: Constant.questionXN_2)
            })
            optionMenu?.addAction(action1_1)
            optionMenu?.addAction(action1_2)
            // show action sheet
            optionMenu?.popoverPresentationController?.sourceView = self.view
            self.present(optionMenu!, animated: true, completion: nil)
        })
        
        let action4 = UIAlertAction(title: "Khác", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let otherAnswerVC = OtherAnswerVC(nibName: "OtherAnswerVC", bundle: nil)
            otherAnswerVC.delegate = self
            if let userName = self.currentUser?.userDoctor?.userInfo?.name{
                otherAnswerVC.userName = userName
            }
            otherAnswerVC.modalPresentationStyle = .custom
            otherAnswerVC.view.frame = CGRect(x: screenSizeWidth/2-(screenSizeWidth-60)/2, y: screenSizeHeight/2-(screenSizeHeight - 228)/2, width: screenSizeWidth-60, height: screenSizeHeight - 228)
            self.present(otherAnswerVC, animated: true, completion: nil)
        })
        
        let action5 = UIAlertAction(title: "Huỷ", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
       
        optionMenu?.addAction(action1)
        optionMenu?.addAction(action2)
        optionMenu?.addAction(action3)
        optionMenu?.addAction(action4)
        optionMenu?.addAction(action5)
        // show action sheet
        optionMenu?.popoverPresentationController?.sourceView = self.view
        self.present(optionMenu!, animated: true, completion: nil)
    }
    
    func reloadDataOther(content:String){
        self.contentTextView.text.append(content)
        self.currentContent = self.contentTextView.text.components(separatedBy: "\n\n").filter { $0 != "" }.joined(separator: "\n")
    }
    
    @IBAction func btnViewContainerAction(_ sender: Any) {
        var optionMenu:UIAlertController? = nil
        if IS_IPAD{
            optionMenu = UIAlertController(title: nil, message: "Nhóm chuyên khoa", preferredStyle: .alert)
        }else{
            optionMenu = UIAlertController(title: nil, message: "Nhóm chuyên khoa", preferredStyle: .actionSheet)
        }
        
        let action1 = UIAlertAction(title: "Xét nghiệm", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            if IS_IPAD{
                optionMenu = UIAlertController(title: nil, message: "Xét nghiệm", preferredStyle: .alert)
            }else{
                optionMenu = UIAlertController(title: nil, message: "Xét nghiệm", preferredStyle: .actionSheet)
            }
            let action1_1 = UIAlertAction(title: "Hỏi về chỉ số xét nghiệm tăng, giảm nghĩa là sao?", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.showTemplateAnswerVC(content: Constant.container_QuestionXN1)
            })
            let action1_2 = UIAlertAction(title: "Hỏi về chỉ số xét nghiệm nghĩa là gì?", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.showTemplateAnswerVC(content: Constant.container_QuestionXN2)
            })
            let action1_3 = UIAlertAction(title: "Hỏi về kết quả xét nghiệm, từ đó xin tư vấn về chế độ ăn uống, điều trị?", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.showTemplateAnswerVC(content: Constant.container_QuestionXN3)
            })
            let action1_4 = UIAlertAction(title: "Trở lại", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
               // self.showTemplateAnswerVC(content: Constant.container_QuestionXN3)
            })
            
            optionMenu?.addAction(action1_1)
            optionMenu?.addAction(action1_2)
            optionMenu?.addAction(action1_3)
            optionMenu?.addAction(action1_4)
            // show action sheet
            optionMenu?.popoverPresentationController?.sourceView = self.view
            self.present(optionMenu!, animated: true, completion: nil)
            
        })
        let action2 = UIAlertAction(title: "Khám chuyên khoa và điều trị", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.showTemplateAnswerVC(content: Constant.container_Specalist)
        })
        let action3 = UIAlertAction(title: "Hỏi về dịch vụ MEDLATEC", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            if IS_IPAD{
                optionMenu = UIAlertController(title: nil, message: "Hỏi về dịch vụ MEDLATEC", preferredStyle: .alert)
            }else{
                optionMenu = UIAlertController(title: nil, message: "Hỏi về dịch vụ MEDLATEC", preferredStyle: .actionSheet)
            }
            let action1_1 = UIAlertAction(title: "Hỏi về các dịch vụ của MEDLATEC: Hỏi về các chuyên khoa, dịch vụ (BHYT, lấy mẫu xét nghiệm tại nhà, KSK,...)", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.showTemplateAnswerVC(content: Constant.container_ServiceMedlatec)
            })
            
            let action1_2 = UIAlertAction(title: "Trở lại", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                
            })
            
            optionMenu?.addAction(action1_1)
            optionMenu?.addAction(action1_2)
            // show action sheet
            optionMenu?.popoverPresentationController?.sourceView = self.view
            self.present(optionMenu!, animated: true, completion: nil)
        })
        
        let action4 = UIAlertAction(title: "Huỷ", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
       
        optionMenu?.addAction(action1)
        optionMenu?.addAction(action2)
        optionMenu?.addAction(action3)
        optionMenu?.addAction(action4)
        // show action sheet
        optionMenu?.popoverPresentationController?.sourceView = self.view
        self.present(optionMenu!, animated: true, completion: nil)
    }
    
    func showTemplateAnswerVC(content:String){
        let templateAnswerVC = TemplateAnswerVC(nibName: "TemplateAnswerVC", bundle: nil)
        templateAnswerVC.delegate = self
        templateAnswerVC.content = content
        templateAnswerVC.modalPresentationStyle = .custom
        templateAnswerVC.view.frame = CGRect(x: screenSizeWidth/2-(screenSizeWidth-60)/2, y: screenSizeHeight/2-(screenSizeHeight - 300)/2, width: screenSizeWidth-60, height: screenSizeHeight - 300)
        self.present(templateAnswerVC, animated: true, completion: nil)
    }
    
    func reloadDataTemplate(content: String) {
        self.contentTextView.text.append(content)
        self.currentContent = self.contentTextView.text.components(separatedBy: "\n\n").filter { $0 != "" }.joined(separator: "\n")
    }
    
    @IBAction func btnExitAction(_ sender: Any) {
        if let questionID = self.questionAnswerForDoctors?.id{
            let result = self.contentTextView.text.components(separatedBy: "\n\n").filter { $0 != "" }.joined(separator: "\n")
            self.userDefaults.set(result, forKey: "\(questionID)")
            self.userDefaults.synchronize()
        }
        self .dismiss(animated: true) {
            
        }
    }
    
    @IBAction func btnUpdateAnswerAction(_ sender: Any) {
        if contentTextView.text.count == 0{
            showAlertView(title: "Bạn vui lòng nhập nội dung bình luận", view: self)
            return
        }
        if contentTextView.text.count < 50{
            showAlertView(title: "Câu trả lời quá ngắn", view: self)
            return
        }
        let answer = Answer()
        answer.questionID = (questionAnswerForDoctors?.id)!
        answer.content = contentTextView.text.htmlAttributedString()?.string
        answer.doctorID = (currentUser?.doctorID)!
        answer.approver = (questionAnswerForDoctors?.approver)!
        answer.answerRate = 0
        answer.countLike = 0
        answer.countShare = 0
        answer.countThank = 0
        answer.approver = false
        answer.comment = "From_iCNM"
        answer.active = true
        
        self.postAnswerQuestion(answer: answer)
    }
    
    func postAnswerQuestion(answer: Answer){
        contentTextView?.resignFirstResponder()
        let progress = self.showProgress()
        QuestionAnswer().postAnswerQuestion(answer: answer, success: { (data) in
            if data != "" {
                self.hideProgress(progress)
                if (self.userDefaults.object(forKey: "\(answer.questionID)") != nil){
                    self.userDefaults.removeObject(forKey: "\(answer.questionID)")
                }
                self.navigationController?.popViewController(animated: true)
                self .dismiss(animated: true) {
                    self.delegate?.reloadDataAnswerDoctor(currentTag: self.currentTag)
                }
            } else {
                self.hideProgress(progress)
                Toast(text: "Cập nhật câu hỏi thất bại").show()
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    @nonobjc func textFieldShouldEndEditing(_ textField: UITextView) -> Bool {
        return true
    }
    
    func keyboardWillShow(_ sender: NSNotification) {
        
    }
    func keyboardWillHide(_ sender: NSNotification) {
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        return changedText.count <= 4000
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if let result = textView.text {
            self.currentContent = result
            if #available(iOS 10.0, *) {
                let convertVoice = ConvertVoiceVC()
                convertVoice.stopRecording()
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    @nonobjc func textFieldShouldReturn(textView: UITextView) -> Bool
    {
        textView.becomeFirstResponder()
        return true
    }
    
    //Number of views
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.listImages?.count)!
    }
    
    //Populate view
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        collectionView.collectionViewLayout.invalidateLayout()
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThumbCell", for: indexPath) as! MyQuestionThumbCell
        cell.contentView.layer.cornerRadius = 4.0
        cell.contentView.layer.masksToBounds = true
        cell.contentView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell.contentView.layer.borderWidth = 1.0
        cell.imageThumb.contentMode = .scaleToFill
        let imgStr = self.listImages![indexPath.row]
        if imgStr.range(of:".jpg") != nil {
            cell.imageThumb.loadImageUsingUrlString(urlString: imgStr)
        }else{
            cell.imageThumb.loadImageUsingUrlString(urlString: imgStr + ".jpg")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let urlString = self.listImages![indexPath.row]
        let imageView = CustomImageView()
        let url = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if url?.range(of:".jpg") != nil {
            imageView.loadImageUsingUrlString(urlString: url!)
        }else{
            imageView.loadImageUsingUrlString(urlString: url! + ".jpg")
        }
        
        if let images : UIImage = imageView.image {
            let photoSlider = PhotoSlider.ViewController(images: [images])
            photoSlider.modalPresentationStyle = .overCurrentContext
            photoSlider.modalTransitionStyle = .crossDissolve
            present(photoSlider, animated: true, completion: nil)
        }
        
//        let frame = CGRect(x: 0, y: 0, width: screenSizeWidth, height: screenSizeHeight)
//        imageView.frame = frame
//        imageView.contentMode = .scaleAspectFit
//        imageView.backgroundColor = .black
//        imageView.isUserInteractionEnabled = true
//        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
//        imageView.addGestureRecognizer(tap)
//        var height:CGFloat = 0.0
//        if IS_IPHONE_X{
//            height = 76.0
//        }else{
//            height = 64
//        }
//        let imageClose = UIImageView(frame: CGRect(x: screenSizeWidth-40, y: height+25, width: 25, height: 25))
//        let image = UIImage(named: "icon_close")?.maskWithColor(color: UIColor(hexColor: 0xFF1E17, alpha: 1.0))
//        imageClose.image = image
//        imageView.addSubview(imageClose)
//        self.navigationController?.navigationBar.layer.zPosition = -1
//        self.view .addSubview(imageView)
    }
    
    // Use to back from full mode
    func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let view = sender.view!
        view .removeFromSuperview()
        self.navigationController?.navigationBar.layer.zPosition = 0
    }
    
    func recordAnswer() {
        Toast(text: "Bắt đầu ghi âm ..").show()
        self.view.endEditing(true)
        if let result = contentTextView.text {
            self.currentContent = result
            if #available(iOS 10.0, *) {
                let convertVoice = ConvertVoiceVC()
                convertVoice.startRecording(textView: contentTextView, image: imgRecord, content: result)
            } else {
                // Fallback on earlier versions
            }
        }
    }
}


