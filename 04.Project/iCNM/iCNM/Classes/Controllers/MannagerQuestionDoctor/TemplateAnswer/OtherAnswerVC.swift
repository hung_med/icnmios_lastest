//
//  OtherAnswerVC
//  iCNM
//
//  Created by Mac osx on 11/02/17.
//  Copyright © 2017 Quang Hung. All rights reserved.
//

import UIKit
import FontAwesome_swift
protocol OtherAnswerVCDelegate {
    func reloadDataOther(content:String)
}
class OtherAnswerVC: UIViewController {

    @IBOutlet weak var myBtnExit: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var myView: UIView!
    var userName:String? = nil
    var delegate:OtherAnswerVCDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        myView.layer.cornerRadius = 10
        myView.layer.borderWidth = 1
        myView.layer.masksToBounds = true
        myBtnExit.layer.cornerRadius = 4
        myBtnExit.layer.masksToBounds = true
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        
        btn1.layer.cornerRadius = 1
        btn1.layer.masksToBounds = true
        btn1.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        btn1.layer.borderWidth = 1.0
        btn2.layer.cornerRadius = 1
        btn2.layer.masksToBounds = true
        btn2.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        btn2.layer.borderWidth = 1.0
        
        btn1.titleLabel?.font = UIFont.fontAwesome(ofSize: 14.0)
        btn2.titleLabel?.font = UIFont.fontAwesome(ofSize: 14.0)
        if let currentUserName = userName{
            let title = "Chào bạn \(currentUserName)!"
            let title2 = "Xin cảm ơn \(currentUserName)!"
            btn1.setTitle(title, for: UIControlState.normal)
            btn2.setTitle(title2, for: UIControlState.normal)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnExit(_ sender: Any) {
        self .dismiss(animated: true) {
            
        }
    }
    
    @IBAction func btnSelectItem(_ sender: Any) {
        let btn = sender as! UIButton
        self .dismiss(animated: true) {
            if let content = btn.titleLabel?.text{
                self.delegate?.reloadDataOther(content: content)
            }
        }
    }
}
