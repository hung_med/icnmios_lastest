//
//  TemplateAnswerVC
//  iCNM
//
//  Created by Mac osx on 11/02/17.
//  Copyright © 2017 Quang Hung. All rights reserved.
//

import UIKit
protocol TemplateAnswerVCDelegate {
    func reloadDataTemplate(content:String)
}
class TemplateAnswerVC: BaseViewController {
  
   @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContent: UITextView!
    @IBOutlet weak var myBtnExit: UIButton!
    @IBOutlet weak var myBtnSelect: UIButton!
    @IBOutlet weak var myView: UIView!
    var delegate:TemplateAnswerVCDelegate?
    var content:String? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        myView.layer.cornerRadius = 10
        myView.layer.borderWidth = 1
        myView.layer.masksToBounds = true
        myBtnExit.layer.cornerRadius = 4
        myBtnExit.layer.masksToBounds = true
        myBtnSelect.layer.cornerRadius = 4
        myBtnSelect.layer.masksToBounds = true
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        self.lblContent.text = content
        self.searchBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnExit(_ sender: Any) {
        self .dismiss(animated: true) {
            
        }
    }
    
    @IBAction func btnSelectTemplate(_ sender: Any) {
        self .dismiss(animated: true) {
            if let content = self.lblContent.text{
                self.delegate?.reloadDataTemplate(content: content)
            }
        }
    }
}
