//
//  Realm+Extensions.swift
//  iCNM
//
//  Created by Medlatec on 5/15/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import RealmSwift

extension Realm {
    func currentUser() -> User? {
        return objects(User.self).first
    }
}
