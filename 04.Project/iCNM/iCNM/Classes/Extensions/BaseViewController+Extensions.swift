//
//  UIViewController+Extensions.swift
//  ezCloudHotelManager
//
//  Created by Medlatec on 2/10/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import Alamofire

extension BaseViewControllerNoSearchBar {
    
    func showUnknownError() {
//        let alertViewController = UIAlertController(title: "Thông báo", message: "Có Lỗi xảy ra!", preferredStyle: .alert)
//        let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
//        alertViewController.addAction(noAction)
//        self.present(alertViewController, animated: true, completion: nil)
        print("Có Lỗi xảy ra")
    }
    
    func handleNetworkError(error:Error, responseData:Data?) {
        
        if let error = error as? AFError {
            switch error {
            case .invalidURL(let url):
                print("Invalid URL: \(url) - \(error.localizedDescription)")
                showUnknownError()
            case .parameterEncodingFailed(let reason):
                print("Parameter encoding failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
                showUnknownError()
            case .multipartEncodingFailed(let reason):
                print("Multipart encoding failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
                showUnknownError()
            case .responseValidationFailed(let reason):
                print("Response validation failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
                
                switch reason {
                case .dataFileNil, .dataFileReadFailed:
                    print("Downloaded file could not be read")
                    showUnknownError()
                case .missingContentType(let acceptableContentTypes):
                    print("Content Type Missing: \(acceptableContentTypes)")
                    showUnknownError()
                case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                    print("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                    showUnknownError()
                case .unacceptableStatusCode(let code):
                    //                    print("Response status code was unacceptable: \(code)")
                    switch code {
                    case 401:
                        let alertViewController = UIAlertController(title: "Thông báo", message: "Phiên làm việc hết hạn, bạn cần đăng nhập lại", preferredStyle: .alert)
                        let noAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                            NotificationCenter.default.post(name: Constant.NotificationMessage.prepareLogout, object: nil)
                            NotificationCenter.default.post(name: Constant.NotificationMessage.logout, object: nil)
                        })
                        alertViewController.addAction(noAction)
                        self.present(alertViewController, animated: true, completion: nil)
                    case 503:
                        let delegate = UIApplication.shared.delegate as! AppDelegate
                        delegate.showInternetSnackbar("Dịch vụ không khả dụng!", duration: .short)
                    default:
                        if let responseData = responseData {
                            do {
                                let dic = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: String]
                                if let error = dic?["error_description"] {
                                    let alertViewController = UIAlertController(title: "Thông báo", message: error, preferredStyle: .alert)
                                    let noAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                                    alertViewController.addAction(noAction)
                                    self.present(alertViewController, animated: true, completion: nil)
                                } else if let error = dic?["Message"] {
                                    let alertViewController = UIAlertController(title: "Thông báo", message: error, preferredStyle: .alert)
                                    let noAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                                    alertViewController.addAction(noAction)
                                    self.present(alertViewController, animated: true, completion: nil)
                                } else {
                                    showUnknownError()
                                }
                                return
                            } catch {
                                showUnknownError()
                                return
                            }
                        }
                        showUnknownError()
                    }
                }
            case .responseSerializationFailed(let reason):
                print("Response serialization failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
                showUnknownError()
            }
            
            print("Underlying error: \(String(describing: error.underlyingError))")
        } else if let error = error as? URLError {
            print("URLError occurred: \(error)")
            if error.code == URLError.notConnectedToInternet {
                //handled by AppDelegate
            } else if error.code == URLError.cannotFindHost {
                let delegate = UIApplication.shared.delegate as! AppDelegate
                delegate.showInternetSnackbar("Không tìm thấy máy chủ", duration: .short)
            } else if error.code == URLError.timedOut {
                let delegate = UIApplication.shared.delegate as! AppDelegate
                delegate.showInternetSnackbar("Kết nối vượt quá thời gian quy định!", duration: .short)
            } else if error.code != URLError.cancelled {
                showUnknownError()
            }
        } else {
            print("Unknown error: \(error)")
            showUnknownError()
        }
    }
}
